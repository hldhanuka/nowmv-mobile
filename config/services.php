<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID', '219086772851-3mfiag5h04k9j0r9dfoc0vn5f6f9tv8p.apps.googleusercontent.com'),  // Your Google App ID
        'client_secret' => env('GOOGLE_CLIENT_SECRET', '51uiTpVgrHlWgWXEPAqrrpqc'), // Your Google App Secret
        'redirect' => env('GOOGLE_CALLBACK_URL', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/google'),
    ],

    'google_ios' => [
        'client_id' => env('GOOGLE_IOS_CLIENT_ID', '219086772851-nmfgd16qd6pmnqc494f3q97bq8ci8muh.apps.googleusercontent.com'),  // Your Google IOS App ID,
    ],

    'google_new' => [
        'client_id' => env('GOOGLE_CLIENT_ID_NEW', '219086772851-3mfiag5h04k9j0r9dfoc0vn5f6f9tv8p.apps.googleusercontent.com'),  // Your Google App ID
        'client_secret' => env('GOOGLE_CLIENT_SECRET_NEW', '51uiTpVgrHlWgWXEPAqrrpqc'), // Your Google App Secret
        'redirect' => env('GOOGLE_CALLBACK_URL_NEW', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/google'),
    ],

    'google_ios_new' => [
        'client_id' => env('GOOGLE_IOS_CLIENT_ID_NEW', '219086772851-nmfgd16qd6pmnqc494f3q97bq8ci8muh.apps.googleusercontent.com'),  // Your Google IOS App ID,
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID', '2701740086569778'),  // Your Facebook App ID
        'client_secret' => env('FACEBOOK_CLIENT_SECRET', 'be14b7eb8d21a4ce2a5f399c453c1d85'), // Your Facebook App Secret
        'redirect' => env('FACEBOOK_CALLBACK_URL', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/facebook'),
    ],
    'facebook_new' => [
        'client_id' => env('FACEBOOK_CLIENT_ID_NEW', '2701740086569778'),  // Your Facebook App ID
        'client_secret' => env('FACEBOOK_CLIENT_SECRET_NEW', 'be14b7eb8d21a4ce2a5f399c453c1d85'), // Your Facebook App Secret
        'redirect' => env('FACEBOOK_CALLBACK_URL_NEW', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/facebook'),
    ],

    'twitter' => [
        'client_id' => env('TWITTER_CLIENT_ID', '20qeEOeCUpREKvIenuUTwDZY3'),  // Your Twitter Client ID
        'client_secret' => env('TWITTER_CLIENT_SECRET', 'kZLsRL703y808eE3zQTjj9FiPs73MBsbC2SOnTnFrvxjUPN02i'), // Your Twitter Client Secret
        'redirect' => env('TWITTER_CALLBACK_URL', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/twitter'),
    ],

    'twitter_new' => [
        'client_id' => env('TWITTER_CLIENT_ID', '20qeEOeCUpREKvIenuUTwDZY3'),  // Your Twitter Client ID
        'client_secret' => env('TWITTER_CLIENT_SECRET', 'kZLsRL703y808eE3zQTjj9FiPs73MBsbC2SOnTnFrvxjUPN02i'), // Your Twitter Client Secret
        'redirect' => env('TWITTER_CALLBACK_URL', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/twitter'),
    ],

//    'instagram' => [
//        'client_id' => env('INSTAGRAM_CLIENT_ID', '1254827598048038'),  // Your Instagram App ID
//        'client_secret' => env('INSTAGRAM_CLIENT_SECRET', '9a5985f5c365f7b10032a06d81e9027e'), // Your Instagram App Secret
//        'redirect' => env('INSTAGRAM_CALLBACK_URL', env('APP_URL', 'https://dhiraagu-now-mv.arimac.digital').'/api/social-login/callback/instagram'),
//        'scopes' => ['user_profile'],
//    ],

];
