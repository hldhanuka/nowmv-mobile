<?php
/**
 * Created by PhpStorm.
 * User: Dasun Dissanayake
 * Date: 2019-11-02
 * Time: 11:49 AM.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Environment
    |--------------------------------------------------------------------------
    |
    | Environment "production" or "staging"
    |
    */
    'environment' => env('DHIRAAGU_ENVIRONMENT', 'staging'),

    /*
    |--------------------------------------------------------------------------
    | username and password
    |--------------------------------------------------------------------------
    |
    | 'username' and 'password' are required when 'grant_type' is 'password'
    |
    */
    'username' => env('DHIRAAGU_USERNAME', ''),
    'password' => env('DHIRAAGU_PASSWORD', ''),

    /*
    |--------------------------------------------------------------------------
    | SMS username and SMS password
    |--------------------------------------------------------------------------
    |
    | 'sms_username' and 'sms_password' for SMS Notification send body
    |
    */
    'sms_username' => env('DHIRAAGU_SMS_USERNAME', ''),
    'sms_password' => env('DHIRAAGU_SMS_PASSWORD', ''),

    /*
    |--------------------------------------------------------------------------
    | Base URL and token generation parameters
    |--------------------------------------------------------------------------
    |
    | 'grant_type' must be 'refresh_token' or 'password'.
    | 'refresh_token' recommended for single node applications and
    | 'password' recommended for multi node applications.
    | 'username' and 'password' are provided by the DHIRAAGU team.
    |
    */
    'production' => [
        'base_url'        => 'https://api.dhiraagu.com.mv',
        'grant_type'      => 'password',
    ],

    'staging' => [
        'base_url'        => 'https://testapi.dhiraagu.com.mv',
        'grant_type'      => 'password',
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Enable
    |--------------------------------------------------------------------------
    |
    | Log enable must be true or false. true is recommended for safety reasons
    |
    */
    'log_enable' => true,

    /*
    |--------------------------------------------------------------------------
    | Log Path
    |--------------------------------------------------------------------------
    |
    | If 'log_enable' is true then log files generated in this path
    |
    */
    'log_path' => storage_path('logs/dhiraagu/logs'),

    /*
    |--------------------------------------------------------------------------
    | Log Request Headers
    |--------------------------------------------------------------------------
    |
    | If 'log_request_headers' is true then log request headers also
    | (Precondition - 'log_enable' => true)
    |
    */
    'log_request_headers' => true,

    /*
    |--------------------------------------------------------------------------
    | Bypass Service Call
    |--------------------------------------------------------------------------
    |
    | true for local developing environment
    | false for Live and Staging environments
    |
    */
    'bypass_service' => env('DHIRAAGU_BYPASS_SERVICE', true),

    /*
    |--------------------------------------------------------------------------
    | Bypass mobile numbers
    |--------------------------------------------------------------------------
    |
    */
    'bypass_mobile' => env('DHIRAAGU_BYPASS_MOBILE', ''),

    /*
    |--------------------------------------------------------------------------
    | REST Service
    |--------------------------------------------------------------------------
    |
    */
    'prayer_types' => [
        'fajr' => 0,
        'sunrise' => 1,
        'dhuhr' => 2,
        'asr' => 3,
        'maghrib' => 4,
        // 'sunset' => 5, // Remove
        'isha' => 6,
    ],

    'prayer_reminder_before' => 5, // 5 min

    'static_api_data' => [
        'clientReference' => "NowMv",
        'senderId' => "NowMv",
        'messageTemplate' => "Please enter this OTP code to sign in to Now MV App",
        'otpServiceDescription' => "OTP",

        'mfs_username' => env("DHIRAAGU_MFS_USERNAME","T1RQTUVSQ0hBTlQ="),
        'mfs_merchantKey' => env("DHIRAAGU_MFS_MERCHANT_KEY","a2s3ODgxeGM="),
        'mfs_originationNumber' => env("DHIRAAGU_MFS_ORIGINATION_NUMBER","7400004"),
        'mfs_zakat_originationNumber' => env("DHIRAAGU_MFS_ZAKAT_ORIGINATION_NUMBER","7400004"),

        'sms_sender' => env("DHIRAAGU_MFS_USERNAME","NowMv")
    ],

    'api' => [
        'generate_otp' => '/v1/2fa',
        'verify_otp' => '/v1/2fa/{ClientRefrerence}',
        'payment_otp_request' => '/v1/mfs/payment',
        'payment_otp_verify' => '/v1/mfs/otp/verify',
        'transaction_status' => '/v1/mfs/otp/status/{referenceId}',
        'e_directory' => '/v1/info/subscribers/{serviceNo}/dir',
        'sms_notification' => '/v1/notif/vasone/api',
    ],

    'flight_data' => [
        'image_base_url' => 'http://fis.com.mv',
        'arrivals' => 'http://fis.com.mv/cgi-bin/webfids.pl',
        'departures' => 'http://fis.com.mv/cgi-bin/webfids.pl?webfids_type=departures',
        'air_port' => 'Velana'
    ],

    'weather_data' => [
        'token' => '3p3rQ0nL2UQ0GKyPcCBGPwtt',
        'station_list' => 'http://mobile.codeworks.mv/api/v1/stations',
        'station_details' => 'http://mobile.codeworks.mv/api/v1/stations/{id}'
    ],

    'dhathuru_credentials' => [
        'api_username' => env('DHATHURU_API_USERNAME','test_dhiraagu'),
        'api_password' =>  env('DHATHURU_API_PASSWORD','jFYnrfRf4zIegyPISwrInV2ff3SmNv'),
    ],

    'dhathuru_api' => [
        'get_atoll_list' => 'https://www.dhathuru.mv/dhathuru/api2/get_atoll_list',
        'get_island_list' =>  'https://www.dhathuru.mv/dhathuru/api2/get_island_list',
        'get_island_schedules' =>  'https://www.dhathuru.mv/dhathuru/api2/get_island_schedules/{island_id}/{date}',
        'get_ticket_plan_details' =>  'https://www.dhathuru.mv/dhathuru/api2/get_ticket_plan_details/{plan_id}',
        'get_boat_details' =>  'https://www.dhathuru.mv/dhathuru/api2/get_boat_details/{vessel_id}',
        'get_island_details' =>  'https://www.dhathuru.mv/dhathuru/api2/get_island_details/{island_id}',
        'get_seating_layout_details' =>  'https://www.dhathuru.mv/dhathuru/api2/get_seating_layout_details/{vessel_id}',
    ],

    'mtcc_bus_service' => [6],


    'mtcc_ferry_service' => [1,2,5,7,9,11],

    'mtcc_logo' => env('APP_URL')."/uploads/travel/MTCC_logo.png",

    'mpl_logo' => env('APP_URL')."/uploads/travel/MPL_logo.png",

    'mpl_api' => [
        'get_all_stop' => 'https://api.transportlink.mv/api/stop',
        'get_schedule' => 'https://api.transportlink.mv/api/schedule',

    ]
];
