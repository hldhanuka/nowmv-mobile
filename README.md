 #Dhiraagu Now MV

Now MV mobile application of Dhiraagu focuses on introducing a dynamic and interactive user experience
to all mobile customers to provide them with a lifestyle platform that includes information about events
and activities that are happening in real time.

## Installation
copy .env file to root folder.

Make the database

```bash
composer install
php artisan migrate --seed
```

## Developed By
[Arimac Digital](https://arimac.digital/)
