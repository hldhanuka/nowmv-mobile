/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    // CUSTOM FONTS
    //config.contentsCss = '/ckeditor/custom-fonts/Faruma.ttf';
    //config.font_names = 'Faruma/faruma;' + config.font_names;
    //config.contentsCss = '/ckeditor/custom-fonts/MVFaseyha.ttf';
    //config.font_names = 'MVFaseyha/MVFaseyha;' + config.font_names;
    //config.contentsCss = '/ckeditor/custom-fonts/MVFaseyha.ttf';
    config.font_names = 'MV_Waheed/MV_Waheed;';// + config.font_names; // Remove other english fonts
    config.font_names = 'MV_Faseyha/MV_Faseyha;' + config.font_names;
    config.font_names = 'Faruma/Faruma;' + config.font_names;
    config.font_names = 'CircularStd-Book/CircularStd-Book;' + config.font_names;

    // DEFAULT FONT SIZE SELECTED
    //config.font_defaultLabel = 'faruma';
    //config.font_defaultLabel = 'MVFaseyha';
    config.font_defaultLabel = 'MV_Faseyha';
    config.fontSize_defaultLabel = '20px';
};

CKEDITOR.on( 'instanceCreated', function( e ){
    //e.editor.addCss("@font-face {font-family: 'MVFaseyha';src: url('./custom-fonts/MVFaseyha.eot');src: url('./custom-fonts/MVFaseyha.eot?#iefix') format('embedded-opentype'),url('./custom-fonts/MVFaseyha.woff2') format('woff2'),url('./custom-fonts/MVFaseyha.woff') format('woff'),url('./custom-fonts/MVFaseyha.ttf') format('truetype'),url('./custom-fonts/MVFaseyha.svg#MVFaseyha') format('svg');font-weight: normal;font-style: italic;}"  );
    e.editor.addCss("@font-face{font-family:'MV_Waheed';src:url('./custom-fonts/maldives/MV_Waheed.otf');}@font-face{font-family:'MV_Faseyha';src:url('./custom-fonts/maldives/MV_Faseyha_Mhr.otf');}@font-face{font-family:'Faruma';src:url('./custom-fonts/maldives/Faruma.otf');}@font-face{font-family:'CircularStd-Book';src:url('./custom-fonts/english/CircularStd-Book.otf');}"  );
});

// DEFAULT FONT SIZE APPEND
// CKEDITOR.on('instanceReady', function( ev ) {
//     ev.editor.dataProcessor.htmlFilter.addRules({
//         elements: {
//             p: function (e) {
//                 e.attributes.style = 'font-size:20px; font-family:MVFaseyha; line-height: 34px;';
//             }
//         }
//     });
// });

CKEDITOR.config.contentsCss = '/ckeditor/custom-fonts/fonts.css';
