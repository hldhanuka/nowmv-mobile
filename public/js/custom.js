$(document).ready(function () {
    //load upload image preview
    $(document).on("click", "#prev_img", function () {
        $("#custom-file-input").click();
    });

    $(document).on("change", "input[type=file]", function () {
        var currentEle = $(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                currentEle
                    .closest(".form-group")
                    .find("#prev_img")
                    .attr("src", e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
    });
    //load upload image preview
    $(document).on("click", ".prev_img2", function () {
        $("#custom-file-input2").click();
    });

    $(document).on("change", "input[type=file]", function () {
        var currentEle = $(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                currentEle
                    .closest(".form-group")
                    .find("#prev_img2")
                    .attr("src", e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
    });
});

function deleteListViewItem() {
    event.preventDefault(); // prevent form submit
    var form = event.target.form; // storing the form

    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to delete?",
        icon: 'warning',
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    }).then((result) => {
        if (result.value) {
            // Press Approve
            form.submit();
        }
    });
}

var inputs = document.querySelectorAll('.file-input');

for (var i = 0, len = inputs.length; i < len; i++) {
    customInput(inputs[i])
}

function customInput (el) {
    const fileInput = el.querySelector('[type="file"]');
    const label = el.querySelector('[data-js-label]');

    fileInput.onchange =
        fileInput.onmouseout = function () {
            if (!fileInput.value) return;

            var value = fileInput.value.replace(/^.*[\\\/]/, '');
            el.className += ' -chosen';
            label.innerText = value
        }
}
