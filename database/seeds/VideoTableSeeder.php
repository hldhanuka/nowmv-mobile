<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->truncate();
        DB::table('videos')->insert([
            [
                'title' => "15 Things You Didn't Know About The Maldives",
                'url' => "https://www.youtube.com/watch?v=DXpLx7PKiqU",
                'video_thumbnail' => "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/video/1605096811853847561517.jpeg",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => "Malediven (Maldives) Six Senses Laamu",
                'url' => "https://www.youtube.com/watch?v=bZkLuCgORi4",
                'video_thumbnail' => "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/video/1605096827686499382980.jpeg",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => "Maldives City Tour - Places to visit in Male', capital city | Maldives Vlog",
                'url' => "https://www.youtube.com/watch?v=rmqVrdqXFig",
                'video_thumbnail' => "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/video/1605096843173900795318.jpeg",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => "A ride around Male' City (Capital of the Maldives)",
                'url' => "https://www.youtube.com/watch?v=kY8OVmLsOMM",
                'video_thumbnail' => "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/video/1605096857554807670212.jpeg",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);
    }
}
