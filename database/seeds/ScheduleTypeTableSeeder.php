<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ScheduleTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedule_types')->truncate();
        DB::table('schedule_types')->insert([
            [
                'name' => "Default Buses",
                'status' => "1",
                'priority' => 5,
                'date' => '[ { "date": "2020-02-01", "title": "" }, { "date": "2020-02-02", "title": "" }, { "date": "2020-02-03", "title": "" }, { "date": "2020-02-04", "title": "" }, { "date": "2020-02-05", "title": "" }, { "date": "2020-02-06", "title": "" }, { "date": "2020-02-07", "title": "" }, { "date": "2020-02-08", "title": "" }, { "date": "2020-02-09", "title": "" }, { "date": "2020-02-10", "title": "" }, { "date": "2020-02-11", "title": "" }, { "date": "2020-02-12", "title": "" }, { "date": "2020-02-13", "title": "" }, { "date": "2020-02-14", "title": "" }, { "date": "2020-02-15", "title": "" }, { "date": "2020-02-16", "title": "" }, { "date": "2020-02-17", "title": "" }, { "date": "2020-02-18", "title": "" }, { "date": "2020-02-19", "title": "" }, { "date": "2020-02-20", "title": "" }, { "date": "2020-02-21", "title": "" }, { "date": "2020-02-22", "title": "" }, { "date": "2020-02-23", "title": "" }, { "date": "2020-02-24", "title": "" }, { "date": "2020-02-25", "title": "" }, { "date": "2020-02-26", "title": "" }, { "date": "2020-02-27", "title": "" }, { "date": "2020-02-28", "title": "" }, { "date": "2020-02-29", "title": "" }, { "date": "2020-03-01", "title": "" }, { "date": "2020-03-02", "title": "" } ]',
                'type' => "bus",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "Weekend Buses",
                'status' => "1",
                'priority' => 4,
                'date' => '[ { "date": "2020-02-01", "title": "" }, { "date": "2020-02-02", "title": "" }, { "date": "2020-02-08", "title": "" }, { "date": "2020-02-09", "title": "" }, { "date": "2020-02-15", "title": "" }, { "date": "2020-02-16", "title": "" }, { "date": "2020-02-22", "title": "" }, { "date": "2020-02-23", "title": "" }, { "date": "2020-02-29", "title": "" }, { "date": "2020-03-01", "title": "" }, { "date": "2020-03-07", "title": "" }, { "date": "2020-03-08", "title": "" }, { "date": "2020-03-14", "title": "" }, { "date": "2020-03-15", "title": "" }, { "date": "2020-03-21", "title": "" }, { "date": "2020-03-22", "title": "" }, { "date": "2020-03-28", "title": "" } ]',
                'type' => "bus",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "Holidays Buses",
                'status' => "1",
                'priority' => 3,
                'date' => '[{"date": "2020-01-04", "title": ""},{"date": "2020-01-08", "title": ""},{"date": "2020-01-21", "title": ""},{"date": "2020-02-09", "title": ""}]',
                'type' => "bus",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "Special Buses",
                'status' => "1",
                'priority' => 2,
                'date' => '[{"date": "2019-12-12", "title": ""},{"date": "2019-12-13", "title": ""},{"date": "2019-12-15", "title": ""},{"date": "2019-12-16", "title": ""},{"date": "2019-12-17", "title": ""},{"date": "2019-12-19", "title": ""},{"date": "2019-12-20", "title": ""},{"date": "2019-12-21", "title": ""},{"date": "2019-12-22", "title": ""},{"date": "2019-12-24", "title": ""},{"date": "2019-12-25", "title": ""}]',
                'type' => "bus",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "Ramazan Buses",
                'status' => "1",
                'priority' => 1,
                'date' => '[{"date": "2019-12-12", "title": ""},{"date": "2019-12-13", "title": ""},{"date": "2019-12-15", "title": ""},{"date": "2019-12-16", "title": ""},{"date": "2019-12-17", "title": ""},{"date": "2019-12-19", "title": ""},{"date": "2019-12-20", "title": ""},{"date": "2019-12-21", "title": ""},{"date": "2019-12-22", "title": ""},{"date": "2019-12-24", "title": ""},{"date": "2019-12-25", "title": ""}]',
                'type' => "bus",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "DHIRAAGU",
                'status' => "1",
                'priority' => 1,
                'date' => '[{"date": "2019-12-01", "title": "DHIRAAGU DAY 1"},{"date": "2019-12-15", "title": "DHIRAAGU DAY 2"},{"date": "2020-01-01", "title": "DHIRAAGU DAY 3"},{"date": "2020-01-15", "title": "DHIRAAGU DAY 4"}]',
                'type' => "calendar",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "YOUTH",
                'status' => "1",
                'priority' => 2,
                'date' => '[{"date": "2019-12-05", "title": "Youth Day 1"},{"date": "2019-12-23", "title": "Youth Day 2"},{"date": "2020-01-20", "title": "Youth Day 3"}]',
                'type' => "calendar",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "SPECIAL DAYS",
                'status' => "1",
                'priority' => 3,
                'date' => '[{"date": "2019-12-05", "title": "Special Day 0"},{"date": "2019-12-11", "title": "Special Day 1"},{"date": "2019-12-19", "title": "Special day 2"},{"date": "2019-12-20", "title": "Special day 3"},{"date": "2019-12-25", "title": "Christmas"},{"date": "2020-01-21", "title": "Special day 4"}]',
                'type' => "calendar",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "Default Ferries",
                'status' => "1",
                'priority' => 3,
                'date' => '[ { "date": "2020-02-01", "title": "" }, { "date": "2020-02-02", "title": "" }, { "date": "2020-02-03", "title": "" }, { "date": "2020-02-04", "title": "" }, { "date": "2020-02-05", "title": "" }, { "date": "2020-02-06", "title": "" }, { "date": "2020-02-07", "title": "" }, { "date": "2020-02-08", "title": "" }, { "date": "2020-02-09", "title": "" }, { "date": "2020-02-10", "title": "" }, { "date": "2020-02-11", "title": "" }, { "date": "2020-02-12", "title": "" }, { "date": "2020-02-13", "title": "" }, { "date": "2020-02-14", "title": "" }, { "date": "2020-02-15", "title": "" }, { "date": "2020-02-16", "title": "" }, { "date": "2020-02-17", "title": "" }, { "date": "2020-02-18", "title": "" }, { "date": "2020-02-19", "title": "" }, { "date": "2020-02-20", "title": "" }, { "date": "2020-02-21", "title": "" }, { "date": "2020-02-22", "title": "" }, { "date": "2020-02-23", "title": "" }, { "date": "2020-02-24", "title": "" }, { "date": "2020-02-25", "title": "" }, { "date": "2020-02-26", "title": "" }, { "date": "2020-02-27", "title": "" }, { "date": "2020-02-28", "title": "" }, { "date": "2020-02-29", "title": "" }, { "date": "2020-03-01", "title": "" }, { "date": "2020-03-02", "title": "" } ]',
                'type' => "ferry",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "Weekend Ferries",
                'status' => "1",
                'priority' => 2,
                'date' => '[ { "date": "2020-02-01", "title": "" }, { "date": "2020-02-02", "title": "" }, { "date": "2020-02-08", "title": "" }, { "date": "2020-02-09", "title": "" }, { "date": "2020-02-15", "title": "" }, { "date": "2020-02-16", "title": "" }, { "date": "2020-02-22", "title": "" }, { "date": "2020-02-23", "title": "" }, { "date": "2020-02-29", "title": "" }, { "date": "2020-03-01", "title": "" }, { "date": "2020-03-07", "title": "" }, { "date": "2020-03-08", "title": "" }, { "date": "2020-03-14", "title": "" }, { "date": "2020-03-15", "title": "" }, { "date": "2020-03-21", "title": "" }, { "date": "2020-03-22", "title": "" }, { "date": "2020-03-28", "title": "" } ]',
                'type' => "ferry",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => "RAMAZAN Ferries",
                'status' => "1",
                'priority' => 1,
                'date' => '[{"date": "2019-12-05", "title": "ferry_03 0"},{"date": "2019-12-11", "title": "ferry_03 1"},{"date": "2019-12-19", "title": "ferry_03 2"},{"date": "2019-12-20", "title": "ferry_03 3"},{"date": "2019-12-25", "title": "ferry_03"},{"date": "2020-01-21", "title": "ferry_03 4"}]',
                'type' => "ferry",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);
    }
}
