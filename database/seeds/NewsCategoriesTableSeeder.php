<?php

use Illuminate\Database\Seeder;

class NewsCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('news_categories')->delete();
        
        \DB::table('news_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'International Sports',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:03:49',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:03:49',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Entertainment',
                'status' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Gossip',
                'status' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Food',
                'status' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Business',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:04:29',
                'created_at' => '2020-01-09 10:04:19',
                'updated_at' => '2020-01-09 10:04:29',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Charity',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 22:00:23',
                'created_at' => '2020-01-09 12:26:56',
                'updated_at' => '2020-01-09 22:00:23',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Travel',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:59:57',
                'created_at' => '2020-01-09 15:45:10',
                'updated_at' => '2020-01-09 21:59:57',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}