<?php

use Illuminate\Database\Seeder;

class RestaurantRatingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('restaurant_ratings')->delete();
        
        \DB::table('restaurant_ratings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'restaurant_id' => 1,
                'rate' => 4.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'restaurant_id' => 2,
                'rate' => 4.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'restaurant_id' => 1,
                'rate' => 3.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'restaurant_id' => 2,
                'rate' => 1.0,
                'created_by' => 3,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-12 15:57:56',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'restaurant_id' => 3,
                'rate' => 4.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'restaurant_id' => 3,
                'rate' => 2.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'restaurant_id' => 4,
                'rate' => 4.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'restaurant_id' => 4,
                'rate' => 2.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'restaurant_id' => 1,
                'rate' => 1.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'restaurant_id' => 2,
                'rate' => 2.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'restaurant_id' => 3,
                'rate' => 1.0,
                'created_by' => 2,
                'created_at' => '2020-01-09 00:29:13',
                'updated_at' => '2020-01-09 00:29:13',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'restaurant_id' => 1,
                'rate' => 4.0,
                'created_by' => 3,
                'created_at' => '2020-01-10 17:34:53',
                'updated_at' => '2020-01-10 17:34:53',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'restaurant_id' => 1,
                'rate' => 5.0,
                'created_by' => 4,
                'created_at' => '2020-01-11 22:03:10',
                'updated_at' => '2020-01-11 22:03:10',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'restaurant_id' => 4,
                'rate' => 5.0,
                'created_by' => 4,
                'created_at' => '2020-01-11 22:04:27',
                'updated_at' => '2020-01-11 22:04:27',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'restaurant_id' => 3,
                'rate' => 5.0,
                'created_by' => 5,
                'created_at' => '2020-01-12 12:09:08',
                'updated_at' => '2020-01-12 12:10:09',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'restaurant_id' => 5,
                'rate' => 5.0,
                'created_by' => 5,
                'created_at' => '2020-01-12 15:08:44',
                'updated_at' => '2020-01-12 15:09:08',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'restaurant_id' => 2,
                'rate' => 5.0,
                'created_by' => 5,
                'created_at' => '2020-01-12 16:25:55',
                'updated_at' => '2020-01-12 16:25:55',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}