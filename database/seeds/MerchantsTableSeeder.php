<?php

use Illuminate\Database\Seeder;

class MerchantsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('merchants')->delete();

        \DB::table('merchants')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Mac Donals',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'KFC',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Pizza Hut',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Test',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 14:39:55',
                'created_at' => '2020-01-09 14:39:51',
                'updated_at' => '2020-01-09 14:39:55',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Diana Shoes',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:20:30',
                'created_at' => '2020-01-09 21:19:24',
                'updated_at' => '2020-01-09 21:20:30',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Fashion Bugs',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:31:26',
                'created_at' => '2020-01-09 21:25:37',
                'updated_at' => '2020-01-09 21:31:26',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'Thilakawardhana textiles',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:34:34',
                'created_at' => '2020-01-09 21:34:25',
                'updated_at' => '2020-01-09 21:34:34',
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'Abans',
                'key' => '7400004',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:37:23',
                'created_at' => '2020-01-09 21:36:03',
                'updated_at' => '2020-01-09 21:37:23',
                'deleted_at' => NULL,
            ),
        ));


    }
}
