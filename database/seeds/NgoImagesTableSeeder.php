<?php

use Illuminate\Database\Seeder;

class NgoImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ngo_images')->delete();

        \DB::table('ngo_images')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'ngo_id' => 1,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/ael0kRcPrLQDpGH2pRm2OCBqNv7cjb1CSMG8g7MY.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'ngo_id' => 1,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/aHv1McSGYZ1A5no69xmyEDQo06LnfZQAYakdeFWO.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'ngo_id' => 2,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/nQmAnV2KbddRR7K7ECDnLOqAn0f2ZHqla5lKA1cd.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'ngo_id' => 3,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/fI7CofWcmJkcuJTBnPshuJtK5Dq59gGM9xsa5xTF.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'ngo_id' => 4,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/wxPO9wplhb6mIzGHeBJkjuGY7iyJdDMIlXi2bmzM.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'ngo_id' => 5,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/rEPnG9jacviET5Pt7zdj65ME8xaAaJUvET2u7gF4.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'ngo_id' => 6,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/ngo_images/Yry3wQe1mHmF0vyx0aZCwr5Uyf97Ww9dyxHlK83v.jpeg',
                    'order' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
        ));
    }
}
