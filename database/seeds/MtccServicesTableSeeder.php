<?php

use Illuminate\Database\Seeder;

class MtccServicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mtcc_services')->delete();
        
        \DB::table('mtcc_services')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Airport express',
                'description' => 'Cover the gap between you and airport within just 5 minutes via our speedboats',
                'summary' => 'Discover our speedboat service from between Male City and the Hulhule\' Airport.',
                'status' => 0,
                'enable_route_link' => 1,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Premium Link',
                'description' => 'Premium ferries operating between Male\' and Hulhumale\', equipped for comfort with a dedicated waiting area at both Fenna and Meyna Gimatha. Premium ferries are operated every 20 – 30 minutes scheduled  from 0530hrs in the morning until 0030hrs midnight. On Fridays, this service will be discontinued for Friday prayers from 1130hrs to 1330hrs.',
                'summary' => 'Scheduled Premium Air-Conditioned ferry service between Male’ and Hulhumale’ ',
                'status' => 1,
                'enable_route_link' => 1,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 5,
                'name' => 'Cargo services',
                'description' => 'We offer scheduled cargo ferry service between Male’, Villingili, Thilafushi and Gulhifalhu, tailored to transfer heavy packages and cargo.<br/><br/>The fare charged for the cargo depends on the size of the package.<div class="row"><div class="col-sm-auto text-center"><span class="lead">Small - medium</span><br><span class="h2" style="line-height: 3rem;">05-10</span><br><span>MVR</span></div><div class="col-sm-auto text-center"><span class="lead">Medium - large</span><br><span class="h2" style="line-height: 3rem;">10-20</span><br><span>MVR</span></div><div class="col-sm-auto text-center"><span class="lead">Large - extra-large</span><br><span class="h2" style="line-height: 3rem;">20-25</span><br><span>MVR</span></div></div>',
                'summary' => 'Explore our Cargo Ferry Services between Male’ and Villingili, Thilafushi and Gulhifalhu',
                'status' => 1,
                'enable_route_link' => 1,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 6,
                'name' => 'Metro Link',
                'description' => 'We offer scheduled metro bus service within Hulhumale’ and shuttle bus service between Hulhumale’ and Velana International Airport.',
                'summary' => 'Explore our bus services',
                'status' => 1,
                'enable_route_link' => 1,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 7,
                'name' => 'Ferry Link',
                'description' => 'Explore our Ferry Services between Male’ and VillimaleMTCC\'s classic ferry service with enhanced ventilation, seating and other onboard services.',
                'summary' => 'Explore our Ferry Services between Male’ and Villimale',
                'status' => 1,
                'enable_route_link' => 1,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 8,
                'name' => 'Private Hire & Sea Charters',
                'description' => 'We offer bus and car hire service in Male\', Hulhumale’ and Velana International Airport. We also offer speedboat charter service allowing customers to enjoy excursion and cruising trips to anywhere in the Maldives.',
                'summary' => 'Explore our private hire and sea charter services',
                'status' => 1,
                'enable_route_link' => 0,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 9,
                'name' => 'Rashu Link',
            'description' => 'We provide intra and inter atoll ferry service across the Maldives in Zone 1 (Atolls Ha, Hdh, Sh), Zone 2 (Atolls N,R,B), Zone 3 (Atolls K, AA, ADh, V), Zone 4 (Atolls M, F, Dh), Zone 5(Atolls Th, L) and Zone 6 (Atolls Ga, Gdh).',
                'summary' => 'Explore our inter atoll ferry services provided in the six zones of Maldives',
                'status' => 1,
                'enable_route_link' => 1,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 10,
                'name' => 'Dhathuru Card',
                'description' => 'Apply for your contactless card to pay for your travels on MTCC services.',
                'summary' => 'Apply for Dhathuru card online',
                'status' => 1,
                'enable_route_link' => 0,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 11,
                'name' => 'North Speed',
                'description' => NULL,
                'summary' => 'Explore our speed-boat ferry services in the HA & HDh atolls of Maldives.',
                'status' => 1,
                'enable_route_link' => 0,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 12,
                'name' => 'Other services',
                'description' => NULL,
                'summary' => 'Explore additional services provided by us',
                'status' => 1,
                'enable_route_link' => 0,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}