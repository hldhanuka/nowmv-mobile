<?php

use Illuminate\Database\Seeder;

class NgoBadgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \DB::table('ngo_badges')->delete();

        \DB::table('ngo_badges')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'ngo_id' => 1,
                    'badge_id' => 10,
                    'winning_limit' => 2000.0,
                    'status' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:56:00',
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'ngo_id' => 2,
                    'badge_id' => 11,
                    'winning_limit' => 1000.0,
                    'status' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:55:49',
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'ngo_id' => 3,
                    'badge_id' => 12,
                    'winning_limit' => 5000.0,
                    'status' => 1,
                    'created_at' => '2020-01-10 04:49:38',
                    'updated_at' => '2020-01-10 10:58:03',
                    'deleted_at' => NULL,
                ),
        ));

    }
}
