<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FerryRouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ferry_routes')->truncate();
        DB::table('ferry_routes')->insert([
            [
                'id' => 1,
                'name' => "Male to Airport (VIA)",
                'from' => 1,
                'to' => 193,
                'type' => "public",
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 2,
                'name' => "Airport (VIA) to Male",
                'from' => 193,
                'to' => 1,
                'type' => "public",
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
