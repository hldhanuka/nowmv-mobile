<?php

use Illuminate\Database\Seeder;

class OfferCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('offer_categories')->delete();
        
        \DB::table('offer_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Restaurants',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:42:23',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:42:23',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Entertainment',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:42:43',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:42:43',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Electronics',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:43:03',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:43:03',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Clothing and Accessories',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:44:34',
                'created_at' => '2020-01-09 13:29:17',
                'updated_at' => '2020-01-09 21:44:34',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Household goods',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:45:03',
                'created_at' => '2020-01-09 14:36:47',
                'updated_at' => '2020-01-09 21:45:03',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}