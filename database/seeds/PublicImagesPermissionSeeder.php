<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PublicImagesPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $public_images_list_permission = Permission::create(['name' => 'public_images.list']);
        $public_images_create_permission = Permission::create(['name' => 'public_images.create']);
        $public_images_delete_permission = Permission::create(['name' => 'public_images.delete']);

        $admin_role = Role::findByName('admin');
        $admin_role->givePermissionTo($public_images_list_permission);
        $admin_role->givePermissionTo($public_images_create_permission);
        $admin_role->givePermissionTo($public_images_delete_permission);

        $news_agent_role = Role::findByName('news_agent');
        $news_agent_role->givePermissionTo($public_images_list_permission);
        $news_agent_role->givePermissionTo($public_images_create_permission);
        $news_agent_role->givePermissionTo($public_images_delete_permission);

        $merchant_role = Role::findByName('merchant');
        $merchant_role->givePermissionTo($public_images_list_permission);
        $merchant_role->givePermissionTo($public_images_create_permission);
        $merchant_role->givePermissionTo($public_images_delete_permission);

        $ngo_merchant_role = Role::findByName('ngo_merchant');
        $ngo_merchant_role->givePermissionTo($public_images_list_permission);
        $ngo_merchant_role->givePermissionTo($public_images_create_permission);
        $ngo_merchant_role->givePermissionTo($public_images_delete_permission);

    }
}
