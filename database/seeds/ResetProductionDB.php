<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ResetProductionDB extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_log')->truncate();
        DB::table('ads')->truncate();
        DB::table('badges')->truncate();
        DB::table('bus_halt_schedules')->truncate();
        DB::table('bus_locations')->truncate();
        DB::table('bus_routes')->truncate();
        DB::table('bus_route_schedules')->truncate();
        DB::table('bus_types')->truncate();
        DB::table('donations')->truncate();
        DB::table('donation_badges')->truncate();
        DB::table('events')->truncate();
        DB::table('event_invitees')->truncate();
        DB::table('event_messages')->truncate();
        DB::table('event_tags')->truncate();
        DB::table('featured_meals')->truncate();
        DB::table('ferry_locations')->truncate();
        DB::table('ferry_routes')->truncate();
        DB::table('ferry_route_schedules')->truncate();
        DB::table('ferry_terminal_schedules')->truncate();
        DB::table('interests')->truncate();
        DB::table('meals')->truncate();
        DB::table('meal_categories')->truncate();
        DB::table('meal_ratings')->truncate();
        DB::table('meal_reviews')->truncate();
        DB::table('merchants')->truncate();
        DB::table('merchant_users')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('news')->truncate();
        DB::table('news_agencies')->truncate();
        DB::table('news_agency_users')->truncate();
        DB::table('news_categories')->truncate();
        DB::table('news_tags')->truncate();
        DB::table('ngos')->truncate();
        DB::table('ngo_merchants')->truncate();
        DB::table('ngo_merchant_users')->truncate();
        DB::table('offers')->truncate();
        DB::table('offer_categories')->truncate();
        DB::table('prayer_tips')->truncate();
        DB::table('push_notifications')->truncate();
        DB::table('recipes')->truncate();
        DB::table('recipe_images')->truncate();
        DB::table('recipe_ratings')->truncate();
        DB::table('recipe_reviews')->truncate();
        DB::table('reminder_notifications')->truncate();
        DB::table('restaurants')->truncate();
        DB::table('restaurant_ratings')->truncate();
        DB::table('restaurant_reviews')->truncate();
        DB::table('schedule_types')->truncate();
        DB::table('short_links')->truncate();
        DB::table('social_accounts')->truncate();
        DB::table('support_teams')->truncate();
        DB::table('users')->truncate();
        DB::table('user_badges')->truncate();
        DB::table('user_cooked_recipes')->truncate();
        DB::table('user_donations')->truncate();
        DB::table('user_fastings')->truncate();
        DB::table('user_favourite_events')->truncate();
        DB::table('user_favourite_recipes')->truncate();
        DB::table('user_interests')->truncate();
        DB::table('user_notification_settings')->truncate();
        DB::table('user_offers')->truncate();
        DB::table('user_prayer_times')->truncate();
        DB::table('user_weather_locations')->truncate();
        DB::table('videos')->truncate();
        DB::table('zakat_payments')->truncate();

        DB::table('users')->insert([
            [
                'id' => 1,
                'first_name' => "admin",
                'last_name' => "nowmv",
                'email' => "admin@nowmv.com",
                'mobile' => "764422994",
                'password' => bcrypt('admin@nowmv123!'),
                'status' => "1",
                //'role' => "admin",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 2,
                'first_name' => "arimac",
                'last_name' => "nowmv",
                'email' => "arimac_admin@nowmv.com",
                'mobile' => "710474824",
                'password' => bcrypt('arimac_admin@nowmv123!'),
                'status' => "1",
                //'role' => "admin",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);

        \App\User::find(1)->assignRole("admin");
        \App\User::find(2)->assignRole("admin");
    }
}
