<?php

use Illuminate\Database\Seeder;

class SupportTeamsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('support_teams')->delete();

        \DB::table('support_teams')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Jane Fedric',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096521053591286707.jpeg',
                'location' => 'Male, Maldives',
                'email' => 'Jane@gmail.com',
                'phone' => '0777996633',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:17',
                'updated_at' => '2020-01-09 10:07:49',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Anna Petersz',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096658111135089731.jpeg',
                'location' => 'Maaffanu, Maldives',
                'email' => 'anna@gmail.com',
                'phone' => '0776897888',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:17',
                'updated_at' => '2020-01-09 10:09:11',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Risla Fazal',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096555531414805967.jpeg',
                'location' => 'Hulhumale Island, Maldives',
                'email' => 'risla@gmail.com',
                'phone' => '0777885522',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:17',
                'updated_at' => '2020-01-09 10:10:14',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Jerrom Fernando',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096616316631103776.jpeg',
                'location' => 'Utheemu, Maldives',
                'email' => 'jerrom@gmail.com',
                'phone' => '0777778855',
                'status' => 1,
                'created_at' => '2020-01-09 10:11:37',
                'updated_at' => '2020-01-09 10:11:37',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Yuwein Fernando',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096677875830732800.jpeg',
                'location' => 'Feydhoo, Maldives',
                'email' => 'yuwein@gmail.com',
                'phone' => '077755221122',
                'status' => 1,
                'created_at' => '2020-01-09 10:12:55',
                'updated_at' => '2020-01-09 10:12:55',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'aaa',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096702767879179901.jpeg',
                'location' => NULL,
                'email' => 'admin@nowmv.com',
                'phone' => '111111111',
                'status' => 1,
                'created_at' => '2020-01-09 14:54:51',
                'updated_at' => '2020-01-09 14:54:51',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'Test',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/support_teams/1605096734365433751203.jpeg',
                'location' => NULL,
                'email' => 'aaaaaaaaaaa',
                'phone' => '111111111111',
                'status' => 1,
                'created_at' => '2020-01-09 14:55:22',
                'updated_at' => '2020-01-09 14:55:22',
                'deleted_at' => NULL,
            ),
        ));


    }
}
