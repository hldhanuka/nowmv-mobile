<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        if (config('app.env') != 'production') {

            // OLD EXISTING SEEDS
            $this->call(SettingsTableSeeder::class);
            $this->call(UserTableSeeder::class);
            $this->call(InterestTableSeeder::class);
            $this->call(PrayerTipTableSeeder::class);
            $this->call(NotificationTypeTableSeeder::class);
            $this->call(PrayerTimesTableSeeder::class);
            $this->call(PrayerLocationsTableSeeder::class);
            $this->call(BusTypeTableSeeder::class);
            $this->call(ScheduleTypeTableSeeder::class);
            $this->call(VideoTableSeeder::class);
            $this->call(BusLocationTableSeeder::class);
            $this->call(FerryLocationTableSeeder::class);
            $this->call(FerryRouteTableSeeder::class);
            $this->call(FerryRouteSchedulesTableSeeder::class);
            $this->call(FerryTerminalScheduleTableSeeder::class);
            $this->call(BusRouteScheduleTableSeeder::class);
            $this->call(BusRouteTableSeeder::class);
            $this->call(BusHaltScheduleTableSeeder::class);
            $this->call(NewsAgencyTableSeeder::class);
            $this->call(NewsAgencyUserTableSeeder::class);
            $this->call(EventMessagesTableSeeder::class);
            $this->call(MerchantUserTableSeeder::class);
            $this->call(RoleAndPermissionSeeder::class);

            // NEW ISEEDS
            $this->call(AdsTableSeeder::class);
            $this->call(BadgesTableSeeder::class);
            $this->call(DonationsTableSeeder::class);
            $this->call(DonationBadgesTableSeeder::class);
            $this->call(NgoBadgeTableSeeder::class);
            $this->call(NgoImagesTableSeeder::class);
            $this->call(EventsTableSeeder::class);
            $this->call(EventInviteesTableSeeder::class);
            $this->call(EventTagsTableSeeder::class);
            $this->call(FeaturedMealsTableSeeder::class);
            $this->call(MealsTableSeeder::class);
            $this->call(MealCategoriesTableSeeder::class);
            $this->call(MealRatingsTableSeeder::class);
            $this->call(NewsTableSeeder::class);
            $this->call(NewsCategoriesTableSeeder::class);
            $this->call(NewsTagsTableSeeder::class);
            $this->call(OffersTableSeeder::class);
            $this->call(OfferCategoriesTableSeeder::class);
            $this->call(MerchantsTableSeeder::class);
            $this->call(PrayerTipsTableSeeder::class);
            $this->call(RecipesTableSeeder::class);
            $this->call(RecipeRatingsTableSeeder::class);
            $this->call(RecipeReviewsTableSeeder::class);
            $this->call(RestaurantsTableSeeder::class);
            $this->call(RestaurantRatingsTableSeeder::class);
            $this->call(RestaurantReviewsTableSeeder::class);
            $this->call(SupportTeamsTableSeeder::class);
            $this->call(PushNotificationsTableSeeder::class);
            $this->call(NgoTableSeeder ::class);
            $this->call(MtccMapLocationsTableSeeder::class);
            $this->call(MtccIslandsTableSeeder::class);
            $this->call(MtccServicesTableSeeder::class);
            $this->call(MtccServicePointsTableSeeder::class);
            $this->call(MtccRoutesTableSeeder::class);
            $this->call(MtccRoutePathsTableSeeder::class);
            $this->call(MtccRouteStopsTableSeeder::class);
            $this->call(MtccRouteStopMultipointsTableSeeder::class);
            $this->call(MtccRouteStopDaysTableSeeder::class);
            $this->call(MtccRouteStopTimesTableSeeder::class);
            $this->call(MtccCalendarFixedHolidaysTableSeeder::class);
            $this->call(MtccCalendarAnnualHolidaysTableSeeder::class);
            $this->call(ZakatCommoditySeeder::class);
            $this->call(ZakatIslandsTableSeeder::class);
            $this->call(ZakatAtollsTableSeeder::class);
            $this->call(NgoMerchantRoleAndPermissionSeeder::class);
            $this->call(CalendarNotificationSettingsSeeder::class);

            // POST LIVE Seeds
            $this->call(CustomRoleCreateSeeder::class);
            $this->call(MomsngoUserCreateSeeder::class);

            $this->call(PublicImagesPermissionSeeder::class);

            $this->call(QuranRecitationsTableSeeder::class);
    }
    }
}
