<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'first_name' => "admin",
                'last_name' => "nowmv",
                'email' => "admin@nowmv.com",
                'mobile' => "769943467",
                'password' => bcrypt('admin@nowmv123!'),
                'status' => "1",
                //'role' => "admin",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'first_name' => "arimac",
                'last_name' => "nowmv",
                'email' => "arimac_admin@nowmv.com",
                'mobile' => "764429394",
                'password' => bcrypt('arimac_admin@nowmv123!'),
                'status' => "1",
                //'role' => "admin",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);
        DB::table('users')->insert([
            [
                'first_name' => "Lahiru",
                'last_name' => "Munasinghe",
                'mobile' => "718717300",
                'email' => "abc@gmail.com",
                "birthday" => "1992-05-01",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'first_name' => "Pramodya",
                'last_name' => "Abeysinghe",
                'mobile' => "719990807",
                'email' => "pra@gmail.com",
                "birthday" => "1992-05-01",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'first_name' => "Kasun",
                'last_name' => "Somadasa",
                'mobile' => "768853567",
                'email' => "kasun@gmail.com",
                "birthday" => "1992-05-01",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
        DB::table('users')->insert([
            [
                'email' => "newsagent1@nowmv.com",
                'mobile' => "newsagent1@nowmv.com",
                'password' => bcrypt('qazwsxxzsawq'),
                'status' => "1",
                //'role' => "news_agent",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'email' => "newsagent2@nowmv.com",
                'mobile' => "newsagent2@nowmv.com",
                'password' => bcrypt('edcrfvvcfdre'),
                'status' => "1",
                //'role' => "news_agent",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);
        DB::table('users')->insert([
            [
                'email' => "merchant1@nowmv.com",
                'mobile' => "merchant1@nowmv.com",
                'password' => bcrypt('qazwsxxzsawq'),
                'status' => "1",
                //'role' => "merchant",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'email' => "merchant2@nowmv.com",
                'mobile' => "merchant2@nowmv.com",
                'password' => bcrypt('edcrfvvcfdre'),
                'status' => "1",
                //'role' => "merchant",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);

        DB::table('users')->insert([
            [
                'first_name' => "Manura",
                'last_name' => "Deelaka",
                'mobile' => "767789056",
                'email' => "manu@gmail.com",
                "birthday" => "1997-05-10",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'first_name' => "Ravindu",
                'last_name' => "Deelaka",
                'mobile' => "7167756478",
                'email' => "ravij@gmail.com",
                "birthday" => "1997-02-28",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'first_name' => "Kevin",
                'last_name' => "wji",
                'mobile' => "782324431",
                'email' => "kev@gmail.com",
                "birthday" => "1997-02-28",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'first_name' => "Gune",
                'last_name' => "jaya",
                'mobile' => "769684655",
                'email' => "gune@gmail.com",
                "birthday" => "1997-02-28",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'first_name' => "Hashan",
                'last_name' => "jaya",
                'mobile' => "715567889",
                'email' => "gune@gmail.com",
                "birthday" => "1997-02-28",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'first_name' => "Vinu",
                'last_name' => "jaya",
                'mobile' => "764421360",
                'email' => "vinu@gmail.com",
                "birthday" => "1997-02-28",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "male",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'first_name' => "Mahisha",
                'last_name' => "Nizam",
                'mobile' => "717786486",
                'email' => "mahisha@arimaclanka.com",
                "birthday" => "1992-02-28",
                "address" => "No 2, 6th Lane, Colombo 03",
                "gender" => "female",
                'status' => "1",
                'image' => "https://dhiraagu-now-mv.arimac.digital/uploads/profile/default.jpg",
                //'role' => "customer",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

        ]);
    }
}
