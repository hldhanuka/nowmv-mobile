<?php

use Illuminate\Database\Seeder;

class RestaurantsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('restaurants')->delete();

        \DB::table('restaurants')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Ithaa Undersea Restaurant',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090691514666573843.jpeg',
                'latitude' => '6.91144337704399',
                'longitude' => '79.85789560314947',
                'location_name' => 'Conrad Maldives, Rangali Island, Maldives',
                'content' => 'To dine below sea level, there’s nowhere quite like the Ithaa Undersea Restaurant. Part of the Conrad Maldives Rangali Island resort, this is a simply exquisite venue specialising in contemporary European dishes of the finest quality. Guests can enjoy a luxurious six-course dinner whilst revelling in the unique opportunity to watch reef sharks and manta rays sail on past them, without the need to don a snorkel. Dishes on offer include delectable malossol imperial caviar, and yellowtail kingfish with saffron champagne risotto. Ithaa certainly is a pricey restaurant, so for those with a tight budget, enjoy a mid-morning cocktail or a four-course lunch instead of the extravagant dinner option.',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 11:42:04',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 11:42:04',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Seagull Café House',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090711474388704176.jpeg',
                'latitude' => '6.927078600000002',
                'longitude' => '79.86124300000006',
                'location_name' => 'Seagull Café House, Faradhee Magu, Male.',
                'content' => 'The Seagull’s take on the British classic of fish and chips, Tuscan shrimp with garden vegetables and potato wedges, snapper in a coconut curry sauce, or a dhal and spinach curry, with an ice cold glass of papaya juice. No meal is complete without sampling at least one of the thirty ice cream flavours on offer, from pineapple to coconut, blackberry to bubble-gum.',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:13:01',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-12 15:13:01',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Salt Café & Restaurant',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090773436212410193.jpeg',
                'latitude' => '6.862830287397758',
                'longitude' => '79.8809944557587',
                'location_name' => '6th & 7th Floor Shinetree Building, Mohamed Ismail Didi Goalhi, Malé,',
                'content' => 'ENVIRONMENT
A chatty and very inviting atmosphere. Spacious although can be a little warm because of the steam from the platters served. A family based restaurant, so it’s likely to seek more attention of customers. Table arrangements are fairly large to position the platter in order to provide sufficient eating space for guests. This may seem a minor detail but it is very well thought out. Try eating from a sizzling hot plate while rubbing shoulders with the person next to you and you will know what I mean. Expect to go home with your hair and clothes smelling like you\'ve spent the night in a kitchen, but that\'s to be expected when dining in a restaurant of this nature, with sizzling hot plates filling the air with a plethora of aromas.

SERVICE
Another unique feature of The Sizzle is that each chair is numbered so that the waiter knows exactly what you ordered. Once again minor detail, but speaks volumes about the restaurants professionalism. Overall, the service is swift and the staff are very friendly. What I feel is most important with regards to service quality is the fact that the owner, Mr. Amalean, is at the restaurant at most times, personally checking on you and your well being. He leaves no stone unturned and this to me is the primary reason for its success.

Parking is a bit of a problem as the street is quite narrow. However, two valets with their luminous vests are always standing on the road waiting to show you where to park. Extremely efficient and they somehow manage to find you am empty parking slot.

FOOD
The food has never been a problem, everything is served hot and sizzling! The meat on the platters look so succulent defining a tingling sensation! A mouth watering aroma, with a variety of choices in exactly how the customer wants their food served. The desserts are a must try. Believe it or not, some of the desserts are served sizzling hot too. Who thought ice cream could be served in a piping hot dish. Try the brownie and ice cream sizzling dessert, it will blow your mind.

VALUE FOR MONEY
All in all very reasonably priced for the quality you get.

OVERALL
If you want it served hot, The Sizzles the place! Clean, friendly, reasonable priced with a slight parking problem, but you will not go home disappointed',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:13:08',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-12 15:13:08',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'Jazz Café',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090791318573171918.jpeg',
                'latitude' => '4.1754695',
                'longitude' => '73.5093531',
                'location_name' => 'Haveeree Hingun, Malé 20282, Maldives',
                'content' => NULL,
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:06:05',
                'created_at' => '2020-01-09 14:52:02',
                'updated_at' => '2020-01-12 15:06:05',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'title' => 'Symphony Restaurant',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090812463575934653.jpeg',
                'latitude' => '4.1754695',
                'longitude' => '73.5093531',
                'location_name' => 'M.a Male Faru, Athamaa Goalhi, 20196, Maldives',
                'content' => 'Quiet and nice restaurant. Attentive employees. Nice dessert.',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:06:10',
                'created_at' => '2020-01-12 14:46:10',
                'updated_at' => '2020-01-12 15:06:10',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'title' => 'REETHI RESTAURANT',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090833354395412107.jpeg',
                'latitude' => '4.1754695',
                'longitude' => '73.5093531',
                'location_name' => 'Maldives',
                'content' => 'Gorgeous place with some amazing views and exquisite food, you cannot fault this gorgeous island place, a must visit for sure. The setvice was outstanding, smiles all around the impeccable quality shows!',
                'status' => 1,
                'merchant_id' => 2,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:06:17',
                'created_at' => '2020-01-12 14:46:51',
                'updated_at' => '2020-01-12 15:06:17',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'title' => 'Sala Thai',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090894394695177206.jpeg',
                'latitude' => '4.1754695',
                'longitude' => '73.5093531',
                'location_name' => 'Buruneege Hithaffinivaa Magu, Malé, Maldives',
                'content' => 'Had lunch on 22nd Dec; the food, the ambiance and staff were great. Quite overpriced though',
                'status' => 1,
                'merchant_id' => 2,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:06:22',
                'created_at' => '2020-01-12 14:48:07',
                'updated_at' => '2020-01-12 15:06:22',
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'title' => 'Farivalhu Restaurant',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090929001200632787.jpeg',
                'latitude' => '4.1754695',
                'longitude' => '73.5093531',
                'location_name' => 'Maldives',
                'content' => 'Best buffet restaurant in meeru Island and have good hospitality.',
                'status' => 1,
                'merchant_id' => 2,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:06:36',
                'created_at' => '2020-01-12 14:49:51',
                'updated_at' => '2020-01-12 15:06:36',
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'title' => 'Mariyaadh Coffee Club',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/restaurants/1605090948863215755344.jpeg',
                'latitude' => '4.1754695',
                'longitude' => '73.5093531',
                'location_name' => 'Maldives',
                'content' => 'Mariyaadh Coffee Club',
                'status' => 2,
                'merchant_id' => 2,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:06:29',
                'created_at' => '2020-01-12 14:51:45',
                'updated_at' => '2020-01-12 15:06:29',
                'deleted_at' => NULL,
            ),
        ));


    }
}
