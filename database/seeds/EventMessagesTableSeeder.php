<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EventMessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('event_messages')->delete();

        \DB::table('event_messages')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'event_id' => 1,
                    'from' => 0,
                    'seen' => 0,
                    'message' => 'event_1 created',
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'event_id' => 1,
                    'from' => 3,
                    'seen' => 0,
                    'message' => '1st sample message',
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'event_id' => 2,
                    'from' => 0,
                    'seen' => 0,
                    'message' => 'event_2 created',
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'event_id' => 2,
                    'from' => 4,
                    'seen' => 0,
                    'message' => '1st sample message',
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'deleted_at' => NULL,
                ),
        ));
    }
}
