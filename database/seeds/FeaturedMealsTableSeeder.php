<?php

use Illuminate\Database\Seeder;

class FeaturedMealsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('featured_meals')->delete();

        \DB::table('featured_meals')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'restaurant_id' => 1,
                    'meal_id' => 2,
                    'created_by' => 1,
                    'authorised_by' => 1,
                    'published_at' => '2020-01-09 00:29:13',
                    'status' => 1,
                    'created_at' => '2020-01-09 00:29:13',
                    'updated_at' => '2020-01-09 13:33:42',
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'restaurant_id' => 1,
                    'meal_id' => 3,
                    'created_by' => 1,
                    'authorised_by' => 1,
                    'published_at' => '2020-01-09 00:29:13',
                    'status' => 1,
                    'created_at' => '2020-01-09 00:29:13',
                    'updated_at' => '2020-01-09 00:29:13',
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'restaurant_id' => 1,
                    'meal_id' => 4,
                    'created_by' => 1,
                    'authorised_by' => 1,
                    'published_at' => '2020-01-09 00:29:13',
                    'status' => 0,
                    'created_at' => '2020-01-09 00:29:13',
                    'updated_at' => '2020-01-09 13:30:54',
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'restaurant_id' => 1,
                    'meal_id' => 5,
                    'created_by' => 1,
                    'authorised_by' => 1,
                    'published_at' => '2020-01-09 00:29:13',
                    'status' => 0,
                    'created_at' => '2020-01-09 00:29:13',
                    'updated_at' => '2020-01-09 13:35:38',
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'restaurant_id' => 1,
                    'meal_id' => 6,
                    'created_by' => 1,
                    'authorised_by' => 1,
                    'published_at' => '2020-01-09 00:29:13',
                    'status' => 1,
                    'created_at' => '2020-01-09 00:29:13',
                    'updated_at' => '2020-01-09 00:29:13',
                    'deleted_at' => NULL,
                ),
        ));


    }
}
