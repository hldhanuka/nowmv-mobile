<?php

use Illuminate\Database\Seeder;

class EventTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        DB::table('event_tags')->truncate();
        DB::table('event_tags')->insert([
            [
                'title' => 'Art',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Causes',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Comedy',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Crafts',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Dance',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Drinks',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Film',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Fitness',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Food',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Games',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Gardening',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Health',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Home',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Literature',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Music',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Networking',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Other',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Party',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Religion',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Shopping',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Sports',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Theater',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],
            [
                'title' => 'Wellness',
                'status' => 1,
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:04:52',
                'deleted_at' => NULL,
            ],

        ]);


    }
}
