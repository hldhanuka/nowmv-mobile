<?php

use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('offers')->delete();

        \DB::table('offers')->insert(array (
            0 =>
            array (
                'id' => 1,
                'category_id' => 1,
                'merchant_id' => 2,
                'title' => 'Great Indian Festival',
                'content' => 'Test Content of Offer 1',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605250966357858331208.jpeg',
                'is_best' => 1,
                'amount' => '250.99',
                'status' => 1,
                'has_redeem' => 0,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:46:09',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:46:09',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'category_id' => 2,
                'merchant_id' => 2,
                'title' => 'Save earth',
                'content' => 'Test Content of Offer 2',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605250992794620960508.jpeg',
                'is_best' => 0,
                'amount' => '1500.00',
                'status' => 1,
                'has_redeem' => 0,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:46:46',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:46:46',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'category_id' => 3,
                'merchant_id' => 3,
                'title' => 'Mega Electronic sale',
                'content' => 'Test Content of Offer 10% off',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605251409174115692126.jpeg',
                'is_best' => 0,
                'amount' => '1500.00',
                'status' => 1,
                'has_redeem' => 0,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:39:26',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:39:26',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'category_id' => 1,
                'merchant_id' => 2,
                'title' => 'Card Offers',
                'content' => 'Test Content of Offer 15% off',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605251430139561397354.jpeg',
                'is_best' => 0,
                'amount' => '1000.00',
                'status' => 1,
                'has_redeem' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:23:16',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 21:23:16',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'category_id' => 3,
                'merchant_id' => 1,
                'title' => 'Super sinhala new year sale',
                'content' => NULL,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605251458361773113472.jpeg',
                'is_best' => 1,
                'amount' => '1500.00',
                'status' => 1,
                'has_redeem' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:35:17',
                'created_at' => '2020-01-09 10:56:56',
                'updated_at' => '2020-01-09 21:35:17',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'category_id' => 1,
                'merchant_id' => 3,
                'title' => 'Buy one get one free',
                'content' => NULL,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605251481949942385555.jpeg',
                'is_best' => 1,
                'amount' => '100.00',
                'status' => 1,
                'has_redeem' => 0,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:22:47',
                'created_at' => '2020-01-09 10:57:12',
                'updated_at' => '2020-01-09 21:22:47',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'category_id' => 1,
                'merchant_id' => 1,
                'title' => 'Diana year end sale',
                'content' => NULL,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/offers/1605251534962223886076.jpeg',
                'is_best' => 1,
                'amount' => '1000.00',
                'status' => 1,
                'has_redeem' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:22:40',
                'created_at' => '2020-01-09 14:39:27',
                'updated_at' => '2020-01-09 21:22:40',
                'deleted_at' => NULL,
            ),
        ));


    }
}
