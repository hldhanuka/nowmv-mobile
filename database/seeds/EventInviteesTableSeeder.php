<?php

use Illuminate\Database\Seeder;

class EventInviteesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('event_invitees')->delete();
        
        \DB::table('event_invitees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 4,
                'event_id' => 12,
                'invitee_id' => 3,
                'status' => 2,
                'created_at' => '2020-01-27 14:36:47',
                'updated_at' => '2020-01-27 15:44:44',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 4,
                'event_id' => 12,
                'invitee_id' => 5,
                'status' => 2,
                'created_at' => '2020-01-27 14:36:49',
                'updated_at' => '2020-01-27 14:36:49',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}