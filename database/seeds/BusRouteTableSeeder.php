<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class BusRouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bus_routes')->truncate();
        DB::table('bus_routes')->insert([
            [
                'id' => 1,
                'name' => "Male to Hulhumale",
                'from' => 1,
                'to' => 2,
                'bus_type_id' => 1,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 2,
                'name' => "Malee to VIA",
                'from' => 2,
                'to' => 4,
                'bus_type_id' => 1,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 3,
                'name' => "VIA to Male",
                'from' => 4,
                'to' => 2,
                'bus_type_id' => 1,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 4,
                'name' => "Hulhumale to Male",
                'from' => 2,
                'to' => 1,
                'bus_type_id' => 1,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
