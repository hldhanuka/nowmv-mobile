<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NewsAgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_agencies')->truncate();
        DB::table('news_agencies')->insert([
            [
                'name' => "News Agency 1",
                'logo' => "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/news_agencies/1605089116285739928964.jpeg",
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],[
                'name' => "News Agency 2",
                'logo' => "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/news_agencies/1605089139849927273310.jpeg",
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ]
        ]);
    }
}
