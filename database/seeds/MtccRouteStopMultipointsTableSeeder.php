<?php

use Illuminate\Database\Seeder;

class MtccRouteStopMultipointsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mtcc_route_stop_multipoints')->delete();
        
        \DB::table('mtcc_route_stop_multipoints')->insert(array (
            0 => 
            array (
                'id' => 63,
                'route_id' => 106,
                'sequence' => 1,
                'originating_service_point' => 18,
                'destination_service_point' => 42,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 64,
                'route_id' => 106,
                'sequence' => 2,
                'originating_service_point' => 42,
                'destination_service_point' => 43,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 65,
                'route_id' => 106,
                'sequence' => 3,
                'originating_service_point' => 43,
                'destination_service_point' => 23,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 66,
                'route_id' => 106,
                'sequence' => 4,
                'originating_service_point' => 23,
                'destination_service_point' => 33,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 67,
                'route_id' => 106,
                'sequence' => 5,
                'originating_service_point' => 33,
                'destination_service_point' => 34,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 68,
                'route_id' => 106,
                'sequence' => 6,
                'originating_service_point' => 34,
                'destination_service_point' => 35,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 69,
                'route_id' => 106,
                'sequence' => 7,
                'originating_service_point' => 35,
                'destination_service_point' => 36,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 70,
                'route_id' => 106,
                'sequence' => 8,
                'originating_service_point' => 36,
                'destination_service_point' => 37,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 71,
                'route_id' => 106,
                'sequence' => 9,
                'originating_service_point' => 37,
                'destination_service_point' => 44,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 72,
                'route_id' => 106,
                'sequence' => 10,
                'originating_service_point' => 44,
                'destination_service_point' => 16,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 73,
                'route_id' => 106,
                'sequence' => 11,
                'originating_service_point' => 16,
                'destination_service_point' => 18,
                'duration' => 3,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 78,
                'route_id' => 101,
                'sequence' => 1,
                'originating_service_point' => 49,
                'destination_service_point' => 48,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 79,
                'route_id' => 101,
                'sequence' => 2,
                'originating_service_point' => 48,
                'destination_service_point' => 23,
                'duration' => 13,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 80,
                'route_id' => 101,
                'sequence' => 3,
                'originating_service_point' => 23,
                'destination_service_point' => 46,
                'duration' => 2,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 81,
                'route_id' => 101,
                'sequence' => 4,
                'originating_service_point' => 46,
                'destination_service_point' => 49,
                'duration' => 14,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 86,
                'route_id' => 100,
                'sequence' => 1,
                'originating_service_point' => 46,
                'destination_service_point' => 47,
                'duration' => 2,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 87,
                'route_id' => 100,
                'sequence' => 2,
                'originating_service_point' => 47,
                'destination_service_point' => 48,
                'duration' => 13,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 88,
                'route_id' => 100,
                'sequence' => 3,
                'originating_service_point' => 48,
                'destination_service_point' => 49,
                'duration' => 1,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 89,
                'route_id' => 100,
                'sequence' => 4,
                'originating_service_point' => 49,
                'destination_service_point' => 46,
                'duration' => 14,
                'wait' => NULL,
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}