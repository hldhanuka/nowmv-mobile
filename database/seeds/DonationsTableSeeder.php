<?php

use Illuminate\Database\Seeder;

class DonationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('donations')->delete();

        \DB::table('donations')->insert(array (
            0 =>
            array (
                'id' => 1,
                'ngo_id' => 1,
                'title' => 'Mining billionaire Andrew Forrest pledges $70 million bushfire relief and recovery donation',
                'max_limit' => '20000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605086098705524281049.jpeg',
                'tag' => 'Save Child',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Through his Minderoo Foundation, Mr Forrest said he would provide $10 million to build a volunteer army of more than 1,200 people drawn from the mining and agriculture sectors to deploy to fire zones to assist in the rebuild of devastated communities.
Another $10 million would be spent in communities who need support in collaboration with the Australian Red Cross and the Salvation Army.
A further $50 million will be spent on a "national blueprint" for fire and disaster resilience to develop new approaches to mitigate the threat of bushfires, with a focus on climate change.
Another $10 million would be spent in communities who need support in collaboration with the Australian Red Cross and the Salvation Army.
A further $50 million will be spent on a "national blueprint" for fire and disaster resilience to develop new approaches to mitigate the threat of bushfires, with a focus on climate change.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:52:51',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-10 09:52:51',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'ngo_id' => 1,
                'title' => 'Mining billionaire Andrew Forrest pledges $70 million bushfire relief and recovery donation',
                'max_limit' => '2000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087711871512719755.jpeg',
                'tag' => 'Elders',
                'status' => 3,
                'type' => 'campaign',
                'content' => 'Through his Minderoo Foundation, Mr Forrest said he would provide $10 million to build a volunteer army of more than 1,200 people drawn from the mining and agriculture sectors to deploy to fire zones to assist in the rebuild of devastated communities.
Another $10 million would be spent in communities who need support in collaboration with the Australian Red Cross and the Salvation Army.
A further $50 million will be spent on a "national blueprint" for fire and disaster resilience to develop new approaches to mitigate the threat of bushfires, with a focus on climate change.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:03',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-10 13:56:32',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'ngo_id' => 2,
                'title' => 'Narooma residents leave properties as bushfires turn the sky orange in NSW South Coast town',
                'max_limit' => '2500.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087729036248981881.jpeg',
                'tag' => 'Save Animals',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Australia\'s animal population is unquestionably special. Many of its best-known species, including kangaroos and koalas, are native to the country, and many animals there can\'t be found in any other nations outside zoos.
In the massive bushfires sweeping the country, Australia\'s wildlife is helpless and populations are being devastated. Some of the most heartbreaking images and videos coming out of the crisis involve Australia\'s animals -- koalas shielding their babies from smoke, kangaroos being comforted, Patsy the Wonder Dog saving a herd of sheep, piles of burn cream and bandages stacked up, ready to treat injured wildlife.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:10',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-10 09:53:10',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'ngo_id' => 1,
                'title' => 'SCHOOL BUILDING FUND',
                'max_limit' => '5000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087751328143581424.jpeg',
                'tag' => 'Save Child',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Snow is falling in St. Joseph, Michigan. On this December day, the overcast sky, swirling flakes, and twinkling bulbs of holiday decorations have created a festive, almost Capraesque atmosphere along the brick-paved streets of this community, which sits on a bluff overlooking Lake Michigan. Across the street from the Boulevard Inn, stairs lead down to Silver Beach, a 1,600-foot-long expanse of sand that is the town’s main attraction in warmer months.
Inside the inn’s restaurant, Jackie Huie sits at a corner table explaining the student mentoring program that the Rotary Club of St. Joseph & Benton Harbor started a decade ago – a program that has helped more than 400 local high school students learn more about their dream careers by connecting them with professionals in those fields.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:20',
                'created_at' => '2020-01-09 11:44:30',
                'updated_at' => '2020-01-10 09:53:20',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'ngo_id' => 1,
                'title' => 'Your bushfire donation may not reach Australia for months   Read more:',
                'max_limit' => '1500.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087775411116453755.jpeg',
                'tag' => 'Fire',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Millions have been given, with £25 million sent to the NSW Rural Fire Service through comedian Celeste Barber’s campaign alone. But some who donated through the PayPal Giving Fund got an unwelcome surprise when they realised it would take anywhere from 15 to 90 days for their donation to reach relief teams. On its website, the non-for-profit auxiliary states that charities enrolled with PayPal Giving Fund will receive funds raised within 45 days of the original donation on Facebook and payouts are issued around the end of each month. However, organisations that aren’t enrolled with PayPal Giving Fund will have the money distributed to them by online payment or check within 90 days of receiving a donation. Although the policy is clear on donation pages, it has still spurred astonishment among some donors who had hoped their money would make a positive impact immediately.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:27',
                'created_at' => '2020-01-10 08:55:37',
                'updated_at' => '2020-01-10 09:53:27',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'ngo_id' => 1,
                'title' => 'SCHOOL TEXTBOOKS',
                'max_limit' => '4000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087803689734207481.jpeg',
                'tag' => 'Books',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Every child deserves a happy, healthy childhood and the opportunity to build a brighter future. School textbooks are the most effective way to improve students\' learning. Providing one textbook per pupil increases literacy scores by 45%.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:37',
                'created_at' => '2020-01-10 09:48:34',
                'updated_at' => '2020-01-10 09:53:37',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'ngo_id' => 3,
                'title' => 'EDUCATION STATIONARY',
                'max_limit' => '3000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087936053269583528.jpeg',
                'tag' => 'Save Child, Books',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Donate an education station in your name or a loved one today and provide a secure future to thousands of children in the years to come. Your donation will provide a dedicated learning space for one child which includes a school desk and chair.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:45',
                'created_at' => '2020-01-10 09:50:14',
                'updated_at' => '2020-01-10 09:53:45',
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'ngo_id' => 4,
                'title' => '400 million children do not have desks',
                'max_limit' => '5000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605087970454800571233.jpeg',
                'tag' => 'Save Child, Books',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Education is one of the most important routes out of extreme poverty. In the third world, education can enable individuals to lift themselves and their families out of the poverty trap and help them build a stable and secure livelihood.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:53:55',
                'created_at' => '2020-01-10 09:51:17',
                'updated_at' => '2020-01-10 09:53:55',
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'ngo_id' => 5,
                'title' => '50 Million living in conflict zones',
                'max_limit' => '5000.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605088003178360851521.jpeg',
                'tag' => 'Help Kids',
                'status' => 1,
                'type' => 'campaign',
                'content' => 'Working with parents and teachers, we overcome the barriers of poverty and inequality, helping parents to earn a secure living, communities to value education and tackle discrimination against girls and marginalised children so that no child is left behind. We construct and renovate classrooms, libraries and important facilities, whilst improving access to hygienic toilets and clean drinking water; creating a safe and engaging environment for children.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 09:54:02',
                'created_at' => '2020-01-10 09:52:44',
                'updated_at' => '2020-01-10 09:54:02',
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'ngo_id' => 6,
                'title' => 'Donors will soon be able to see where their blood is used through Google Maps',
                'max_limit' => '0.00',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/donations/1605088046576246983601.jpeg',
                'tag' => 'Bood',
                'status' => 1,
                'type' => 'campaign',
            'content' => 'As per the NBTC/NACO and Drugs and Cosmetics Rules, 1945 (122-EA, Part-XB), blood donation should be purely voluntary and the organisers must not gift anything to anyone visiting their camps.
Albeit, a lot of people arrived at the camp to voluntarily donate blood, a large number of people descended with the hope of receiving the sumptuous gift of Hilsa.
“For the last four years, we are organising blood donation camps. In 2016, we gifted ceiling fan and rain coat, while in 2017 we gifted mobile phones. In 2018, we gifted mixer grinders and this year we gave induction ovens and ‘Ilish Machh’(Hilsa fish),” Secretary of the organising committee Sanjay Nandi, said.',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:46:58',
                'created_at' => '2020-01-12 15:46:35',
                'updated_at' => '2020-01-12 15:46:58',
                'deleted_at' => NULL,
            ),
        ));


    }
}
