<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class MomsngoUserCreateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Get custom role
        $custom_role = Role::findByName('custom');

        // Create or Get User
        $new_custom_user = \App\User::where('email','momsngo@nowmv.com')->first();
        if($new_custom_user==null){
            $new_custom_user = new \App\User();
            $new_custom_user->email = "momsngo@nowmv.com";
            $new_custom_user->mobile = "momsngo@nowmv.com";
            $new_custom_user->password = bcrypt('tgbnbhgyt');
            $new_custom_user->status = 1;
            $new_custom_user->save();
            $new_custom_user->assignRole($custom_role);
        }

        // Create or Get NgoMerchant
        $new_ngo_merchant = \App\NgoMerchant::where('name','MOMSNGO')->first();
        if($new_ngo_merchant==null){
            $new_ngo_merchant = new \App\NgoMerchant();
            $new_ngo_merchant->name = "MOMSNGO";
            $new_ngo_merchant->key = "7400004";
            $new_ngo_merchant->status = 1;
            $new_ngo_merchant->created_by = 1;
            $new_ngo_merchant->authorized_by = 1;
            $new_ngo_merchant->published_at = \Carbon\Carbon::now()->toDateTimeString();
            $new_ngo_merchant->save();
        }

        // Create or Get NewsAgency
        $new_news_agency = \App\NewsAgency::where('name','MOMSNGO')->first();
        if($new_news_agency==null){
            $new_news_agency = new \App\NewsAgency();
            $new_news_agency->name = "MOMSNGO";
            $new_news_agency->logo = "https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/news_agencies/1605089151954761318345.jpeg";
            $new_news_agency->status = 1;
            $new_news_agency->created_by = 1;
            $new_news_agency->authorized_by = 1;
            $new_news_agency->published_at = \Carbon\Carbon::now()->toDateTimeString();
            $new_news_agency->save();
        }

        // Update or Create NgoMerchantUser
        $new_ngo_merchant_user = \App\NgoMerchantUser::updateOrCreate(['user_id'=>$new_custom_user->id],[
            'ngo_merchant_id' => $new_ngo_merchant->id,
            'status' => 1,
            'created_by' => 1,
            'authorized_by' => 1,
            'published_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Update or Create NewsAgencyUser
        $new_news_agency_user = \App\NewsAgencyUser::updateOrCreate(['user_id'=>$new_custom_user->id],[
            'news_agency_id' => $new_news_agency->id,
            'status' => 1,
            'created_by' => 1,
            'authorized_by' => 1,
            'published_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        $new_custom_user->givePermissionTo(
            [
                'cms_login',
                'donations.list',
                'donations.create',
                'donations.update',
                'donations.delete',
                'badges.list',
                'badges.create',
                'badges.update',
                'badges.delete',
                'events.list',
                'events.create',
                'events.update',
                'events.delete',
                'news_tags.list',
                'news_tags.create',
                'news_tags.update',
                'news_tags.delete',
                'agent_news.list',
                'agent_news.create',
                'agent_news.update',
                'agent_news.delete',
                'videos.list',
                'videos.create',
                'videos.update',
                'videos.delete',
            ]
        );
    }
}
