<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class FerryRouteSchedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ferry_route_schedules')->truncate();
        DB::table('ferry_route_schedules')->insert([
            [
                'schedule_type_id' => 9,
                'ferry_route_id' => 1,
                'starting_time' => '00:00:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 10,
                'ferry_route_id' => 1,
                'starting_time' => '00:00:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 11,
                'ferry_route_id' => 1,
                'starting_time' => '00:00:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 9,
                'ferry_route_id' => 1,
                'starting_time' => '13:15:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 10,
                'ferry_route_id' => 1,
                'starting_time' => '13:15:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 11,
                'ferry_route_id' => 1,
                'starting_time' => '13:15:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 9,
                'ferry_route_id' => 1,
                'starting_time' => '17:30:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 10,
                'ferry_route_id' => 1,
                'starting_time' => '17:30:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 11,
                'ferry_route_id' => 1,
                'starting_time' => '17:30:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 9,
                'ferry_route_id' => 1,
                'starting_time' => '20:00:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 10,
                'ferry_route_id' => 1,
                'starting_time' => '20:00:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'schedule_type_id' => 11,
                'ferry_route_id' => 1,
                'starting_time' => '20:00:00',
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
