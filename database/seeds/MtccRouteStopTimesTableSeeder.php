<?php

use Illuminate\Database\Seeder;

class MtccRouteStopTimesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mtcc_route_stop_times')->delete();
        
        \DB::table('mtcc_route_stop_times')->insert(array (
            0 => 
            array (
                'id' => 2020,
                'route_day_id' => 198,
                'departure_time' => '00:00:00',
                'arrival_time' => '00:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2021,
                'route_day_id' => 198,
                'departure_time' => '00:10:00',
                'arrival_time' => '00:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 2022,
                'route_day_id' => 198,
                'departure_time' => '00:20:00',
                'arrival_time' => '00:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 2023,
                'route_day_id' => 198,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 2024,
                'route_day_id' => 198,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 2025,
                'route_day_id' => 198,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 2026,
                'route_day_id' => 198,
                'departure_time' => '01:30:00',
                'arrival_time' => '01:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 2027,
                'route_day_id' => 198,
                'departure_time' => '02:00:00',
                'arrival_time' => '02:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 2028,
                'route_day_id' => 198,
                'departure_time' => '03:00:00',
                'arrival_time' => '03:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 2029,
                'route_day_id' => 198,
                'departure_time' => '04:00:00',
                'arrival_time' => '04:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 2030,
                'route_day_id' => 198,
                'departure_time' => '05:00:00',
                'arrival_time' => '05:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 2031,
                'route_day_id' => 198,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 2032,
                'route_day_id' => 198,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 2033,
                'route_day_id' => 198,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 2034,
                'route_day_id' => 198,
                'departure_time' => '06:10:00',
                'arrival_time' => '06:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 2035,
                'route_day_id' => 198,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 2036,
                'route_day_id' => 198,
                'departure_time' => '06:25:00',
                'arrival_time' => '06:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 2037,
                'route_day_id' => 198,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 2038,
                'route_day_id' => 198,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 2039,
                'route_day_id' => 198,
                'departure_time' => '06:38:00',
                'arrival_time' => '06:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 2040,
                'route_day_id' => 198,
                'departure_time' => '06:40:00',
                'arrival_time' => '06:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 2041,
                'route_day_id' => 198,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 2042,
                'route_day_id' => 198,
                'departure_time' => '06:50:00',
                'arrival_time' => '06:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 2043,
                'route_day_id' => 198,
                'departure_time' => '06:55:00',
                'arrival_time' => '07:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 2044,
                'route_day_id' => 198,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 2045,
                'route_day_id' => 198,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 2046,
                'route_day_id' => 198,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 2047,
                'route_day_id' => 198,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 2048,
                'route_day_id' => 198,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 2049,
                'route_day_id' => 198,
                'departure_time' => '07:24:00',
                'arrival_time' => '07:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 2050,
                'route_day_id' => 198,
                'departure_time' => '07:27:00',
                'arrival_time' => '07:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 2051,
                'route_day_id' => 198,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 2052,
                'route_day_id' => 198,
                'departure_time' => '07:35:00',
                'arrival_time' => '07:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 2053,
                'route_day_id' => 198,
                'departure_time' => '07:40:00',
                'arrival_time' => '07:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 2054,
                'route_day_id' => 198,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 2055,
                'route_day_id' => 198,
                'departure_time' => '07:50:00',
                'arrival_time' => '07:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 2056,
                'route_day_id' => 198,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 2057,
                'route_day_id' => 198,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 2058,
                'route_day_id' => 198,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 2059,
                'route_day_id' => 198,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 2060,
                'route_day_id' => 198,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 2061,
                'route_day_id' => 198,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 2062,
                'route_day_id' => 198,
                'departure_time' => '08:40:00',
                'arrival_time' => '08:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 2063,
                'route_day_id' => 198,
                'departure_time' => '08:50:00',
                'arrival_time' => '08:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 2064,
                'route_day_id' => 198,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 2065,
                'route_day_id' => 198,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 2066,
                'route_day_id' => 198,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 2067,
                'route_day_id' => 198,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 2068,
                'route_day_id' => 198,
                'departure_time' => '09:40:00',
                'arrival_time' => '09:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 2069,
                'route_day_id' => 198,
                'departure_time' => '09:50:00',
                'arrival_time' => '09:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 2070,
                'route_day_id' => 198,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 2071,
                'route_day_id' => 198,
                'departure_time' => '10:10:00',
                'arrival_time' => '10:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 2072,
                'route_day_id' => 198,
                'departure_time' => '10:20:00',
                'arrival_time' => '10:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 2073,
                'route_day_id' => 198,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 2074,
                'route_day_id' => 198,
                'departure_time' => '10:40:00',
                'arrival_time' => '10:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 2075,
                'route_day_id' => 198,
                'departure_time' => '10:50:00',
                'arrival_time' => '10:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 2076,
                'route_day_id' => 198,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 2077,
                'route_day_id' => 198,
                'departure_time' => '11:10:00',
                'arrival_time' => '11:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 2078,
                'route_day_id' => 198,
                'departure_time' => '11:20:00',
                'arrival_time' => '11:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 2079,
                'route_day_id' => 198,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 2080,
                'route_day_id' => 198,
                'departure_time' => '11:40:00',
                'arrival_time' => '11:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 2081,
                'route_day_id' => 198,
                'departure_time' => '11:50:00',
                'arrival_time' => '11:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 2082,
                'route_day_id' => 198,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 2083,
                'route_day_id' => 198,
                'departure_time' => '12:10:00',
                'arrival_time' => '12:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 2084,
                'route_day_id' => 198,
                'departure_time' => '12:15:00',
                'arrival_time' => '12:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 2085,
                'route_day_id' => 198,
                'departure_time' => '12:20:00',
                'arrival_time' => '12:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 2086,
                'route_day_id' => 198,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 2087,
                'route_day_id' => 198,
                'departure_time' => '12:35:00',
                'arrival_time' => '12:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 2088,
                'route_day_id' => 198,
                'departure_time' => '12:40:00',
                'arrival_time' => '12:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 2089,
                'route_day_id' => 198,
                'departure_time' => '12:45:00',
                'arrival_time' => '12:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 2090,
                'route_day_id' => 198,
                'departure_time' => '12:50:00',
                'arrival_time' => '12:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 2091,
                'route_day_id' => 198,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 2092,
                'route_day_id' => 198,
                'departure_time' => '13:10:00',
                'arrival_time' => '13:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 2093,
                'route_day_id' => 198,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 2094,
                'route_day_id' => 198,
                'departure_time' => '13:20:00',
                'arrival_time' => '13:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 2095,
                'route_day_id' => 198,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 2096,
                'route_day_id' => 198,
                'departure_time' => '13:40:00',
                'arrival_time' => '13:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 2097,
                'route_day_id' => 198,
                'departure_time' => '13:50:00',
                'arrival_time' => '13:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 2098,
                'route_day_id' => 198,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 2099,
                'route_day_id' => 198,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 2100,
                'route_day_id' => 198,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 2101,
                'route_day_id' => 198,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 2102,
                'route_day_id' => 198,
                'departure_time' => '14:40:00',
                'arrival_time' => '14:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 2103,
                'route_day_id' => 198,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 2104,
                'route_day_id' => 198,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 2105,
                'route_day_id' => 198,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 2106,
                'route_day_id' => 198,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 2107,
                'route_day_id' => 198,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 2108,
                'route_day_id' => 198,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 2109,
                'route_day_id' => 198,
                'departure_time' => '15:50:00',
                'arrival_time' => '15:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 2110,
                'route_day_id' => 198,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 2111,
                'route_day_id' => 198,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 2112,
                'route_day_id' => 198,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 2113,
                'route_day_id' => 198,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 2114,
                'route_day_id' => 198,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 2115,
                'route_day_id' => 198,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 2116,
                'route_day_id' => 198,
                'departure_time' => '16:50:00',
                'arrival_time' => '16:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 2117,
                'route_day_id' => 198,
                'departure_time' => '16:55:00',
                'arrival_time' => '17:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 2118,
                'route_day_id' => 198,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 2119,
                'route_day_id' => 198,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 2120,
                'route_day_id' => 198,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 2121,
                'route_day_id' => 198,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 2122,
                'route_day_id' => 198,
                'departure_time' => '17:25:00',
                'arrival_time' => '17:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 2123,
                'route_day_id' => 198,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 2124,
                'route_day_id' => 198,
                'departure_time' => '17:35:00',
                'arrival_time' => '17:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 2125,
                'route_day_id' => 198,
                'departure_time' => '17:40:00',
                'arrival_time' => '17:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 2126,
                'route_day_id' => 198,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 2127,
                'route_day_id' => 198,
                'departure_time' => '17:55:00',
                'arrival_time' => '18:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 2128,
                'route_day_id' => 198,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 2129,
                'route_day_id' => 198,
                'departure_time' => '18:05:00',
                'arrival_time' => '18:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 2130,
                'route_day_id' => 198,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 2131,
                'route_day_id' => 198,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 2132,
                'route_day_id' => 198,
                'departure_time' => '18:20:00',
                'arrival_time' => '18:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 2133,
                'route_day_id' => 198,
                'departure_time' => '18:25:00',
                'arrival_time' => '18:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 2134,
                'route_day_id' => 198,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 2135,
                'route_day_id' => 198,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 2136,
                'route_day_id' => 198,
                'departure_time' => '18:40:00',
                'arrival_time' => '18:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 2137,
                'route_day_id' => 198,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 2138,
                'route_day_id' => 198,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 2139,
                'route_day_id' => 198,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 2140,
                'route_day_id' => 198,
                'departure_time' => '19:10:00',
                'arrival_time' => '19:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 2141,
                'route_day_id' => 198,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 2142,
                'route_day_id' => 198,
                'departure_time' => '19:20:00',
                'arrival_time' => '19:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 2143,
                'route_day_id' => 198,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 2144,
                'route_day_id' => 198,
                'departure_time' => '19:40:00',
                'arrival_time' => '19:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 2145,
                'route_day_id' => 198,
                'departure_time' => '19:50:00',
                'arrival_time' => '19:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 2146,
                'route_day_id' => 198,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 2147,
                'route_day_id' => 198,
                'departure_time' => '20:10:00',
                'arrival_time' => '20:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 2148,
                'route_day_id' => 198,
                'departure_time' => '20:20:00',
                'arrival_time' => '20:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 2149,
                'route_day_id' => 198,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 2150,
                'route_day_id' => 198,
                'departure_time' => '20:40:00',
                'arrival_time' => '20:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 2151,
                'route_day_id' => 198,
                'departure_time' => '20:50:00',
                'arrival_time' => '20:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 2152,
                'route_day_id' => 198,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 2153,
                'route_day_id' => 198,
                'departure_time' => '21:10:00',
                'arrival_time' => '21:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 2154,
                'route_day_id' => 198,
                'departure_time' => '21:20:00',
                'arrival_time' => '21:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 2155,
                'route_day_id' => 198,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 2156,
                'route_day_id' => 198,
                'departure_time' => '21:40:00',
                'arrival_time' => '21:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 2157,
                'route_day_id' => 198,
                'departure_time' => '21:50:00',
                'arrival_time' => '21:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 2158,
                'route_day_id' => 198,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 2159,
                'route_day_id' => 198,
                'departure_time' => '22:10:00',
                'arrival_time' => '22:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 2160,
                'route_day_id' => 198,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 2161,
                'route_day_id' => 198,
                'departure_time' => '22:20:00',
                'arrival_time' => '22:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 2162,
                'route_day_id' => 198,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 2163,
                'route_day_id' => 198,
                'departure_time' => '22:40:00',
                'arrival_time' => '22:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 2164,
                'route_day_id' => 198,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 2165,
                'route_day_id' => 198,
                'departure_time' => '22:50:00',
                'arrival_time' => '22:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 2166,
                'route_day_id' => 198,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 2167,
                'route_day_id' => 198,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 2168,
                'route_day_id' => 198,
                'departure_time' => '23:20:00',
                'arrival_time' => '23:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 2169,
                'route_day_id' => 198,
                'departure_time' => '23:30:00',
                'arrival_time' => '23:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 2170,
                'route_day_id' => 198,
                'departure_time' => '23:40:00',
                'arrival_time' => '23:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 2171,
                'route_day_id' => 198,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 2172,
                'route_day_id' => 199,
                'departure_time' => '00:00:00',
                'arrival_time' => '00:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 2173,
                'route_day_id' => 199,
                'departure_time' => '00:10:00',
                'arrival_time' => '00:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 2174,
                'route_day_id' => 199,
                'departure_time' => '00:20:00',
                'arrival_time' => '00:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 2175,
                'route_day_id' => 199,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 2176,
                'route_day_id' => 199,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 2177,
                'route_day_id' => 199,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 2178,
                'route_day_id' => 199,
                'departure_time' => '01:30:00',
                'arrival_time' => '01:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 2179,
                'route_day_id' => 199,
                'departure_time' => '02:00:00',
                'arrival_time' => '02:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 2180,
                'route_day_id' => 199,
                'departure_time' => '03:00:00',
                'arrival_time' => '03:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 2181,
                'route_day_id' => 199,
                'departure_time' => '04:00:00',
                'arrival_time' => '04:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 2182,
                'route_day_id' => 199,
                'departure_time' => '05:00:00',
                'arrival_time' => '05:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 2183,
                'route_day_id' => 199,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 2185,
                'route_day_id' => 199,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 2186,
                'route_day_id' => 199,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 2187,
                'route_day_id' => 199,
                'departure_time' => '06:10:00',
                'arrival_time' => '06:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 2188,
                'route_day_id' => 199,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 2189,
                'route_day_id' => 199,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 2190,
                'route_day_id' => 199,
                'departure_time' => '06:40:00',
                'arrival_time' => '06:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 2191,
                'route_day_id' => 199,
                'departure_time' => '06:50:00',
                'arrival_time' => '06:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 2192,
                'route_day_id' => 199,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 2193,
                'route_day_id' => 199,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 2194,
                'route_day_id' => 199,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 2195,
                'route_day_id' => 199,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 2196,
                'route_day_id' => 199,
                'departure_time' => '07:40:00',
                'arrival_time' => '07:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 2197,
                'route_day_id' => 199,
                'departure_time' => '07:50:00',
                'arrival_time' => '07:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 2198,
                'route_day_id' => 199,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 2199,
                'route_day_id' => 199,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 2200,
                'route_day_id' => 199,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 2201,
                'route_day_id' => 199,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 2202,
                'route_day_id' => 199,
                'departure_time' => '08:40:00',
                'arrival_time' => '08:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 2203,
                'route_day_id' => 199,
                'departure_time' => '08:50:00',
                'arrival_time' => '08:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 2204,
                'route_day_id' => 199,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 2205,
                'route_day_id' => 199,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 2206,
                'route_day_id' => 199,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 2207,
                'route_day_id' => 199,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 2208,
                'route_day_id' => 199,
                'departure_time' => '09:40:00',
                'arrival_time' => '09:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 2209,
                'route_day_id' => 199,
                'departure_time' => '09:50:00',
                'arrival_time' => '09:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 2210,
                'route_day_id' => 199,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 2211,
                'route_day_id' => 199,
                'departure_time' => '10:10:00',
                'arrival_time' => '10:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 2212,
                'route_day_id' => 199,
                'departure_time' => '10:20:00',
                'arrival_time' => '10:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 2213,
                'route_day_id' => 199,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 2214,
                'route_day_id' => 199,
                'departure_time' => '10:35:00',
                'arrival_time' => '10:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 2215,
                'route_day_id' => 199,
                'departure_time' => '10:40:00',
                'arrival_time' => '10:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 2216,
                'route_day_id' => 199,
                'departure_time' => '10:50:00',
                'arrival_time' => '10:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 2217,
                'route_day_id' => 199,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 2218,
                'route_day_id' => 199,
                'departure_time' => '11:05:00',
                'arrival_time' => '11:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 2219,
                'route_day_id' => 199,
                'departure_time' => '11:10:00',
                'arrival_time' => '11:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 2220,
                'route_day_id' => 199,
                'departure_time' => '11:20:00',
                'arrival_time' => '11:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 2221,
                'route_day_id' => 199,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 2222,
                'route_day_id' => 199,
                'departure_time' => '11:40:00',
                'arrival_time' => '11:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 2223,
                'route_day_id' => 199,
                'departure_time' => '11:50:00',
                'arrival_time' => '11:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 2224,
                'route_day_id' => 199,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 2225,
                'route_day_id' => 199,
                'departure_time' => '12:10:00',
                'arrival_time' => '12:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 2226,
                'route_day_id' => 199,
                'departure_time' => '12:20:00',
                'arrival_time' => '12:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 2227,
                'route_day_id' => 199,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 2228,
                'route_day_id' => 199,
                'departure_time' => '12:40:00',
                'arrival_time' => '12:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 2229,
                'route_day_id' => 199,
                'departure_time' => '12:50:00',
                'arrival_time' => '12:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 2230,
                'route_day_id' => 199,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 2231,
                'route_day_id' => 199,
                'departure_time' => '13:10:00',
                'arrival_time' => '13:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 2232,
                'route_day_id' => 199,
                'departure_time' => '13:20:00',
                'arrival_time' => '13:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 2233,
                'route_day_id' => 199,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 2234,
                'route_day_id' => 199,
                'departure_time' => '13:40:00',
                'arrival_time' => '13:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 2235,
                'route_day_id' => 199,
                'departure_time' => '13:50:00',
                'arrival_time' => '13:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 2236,
                'route_day_id' => 199,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 2237,
                'route_day_id' => 199,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 2238,
                'route_day_id' => 199,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 2239,
                'route_day_id' => 199,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 2240,
                'route_day_id' => 199,
                'departure_time' => '14:40:00',
                'arrival_time' => '14:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 2241,
                'route_day_id' => 199,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 2242,
                'route_day_id' => 199,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 2243,
                'route_day_id' => 199,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 2244,
                'route_day_id' => 199,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 2245,
                'route_day_id' => 199,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 2246,
                'route_day_id' => 199,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 2247,
                'route_day_id' => 199,
                'departure_time' => '15:50:00',
                'arrival_time' => '15:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 2248,
                'route_day_id' => 199,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 2249,
                'route_day_id' => 199,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 2250,
                'route_day_id' => 199,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 2252,
                'route_day_id' => 199,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 2253,
                'route_day_id' => 199,
                'departure_time' => '16:25:00',
                'arrival_time' => '16:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 2254,
                'route_day_id' => 199,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 2255,
                'route_day_id' => 199,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 2256,
                'route_day_id' => 199,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 2257,
                'route_day_id' => 199,
                'departure_time' => '16:50:00',
                'arrival_time' => '16:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 2258,
                'route_day_id' => 199,
                'departure_time' => '16:55:00',
                'arrival_time' => '17:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 2259,
                'route_day_id' => 199,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 2260,
                'route_day_id' => 199,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 2261,
                'route_day_id' => 199,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 2262,
                'route_day_id' => 199,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 2263,
                'route_day_id' => 199,
                'departure_time' => '17:25:00',
                'arrival_time' => '17:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 2264,
                'route_day_id' => 199,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 2265,
                'route_day_id' => 199,
                'departure_time' => '17:40:00',
                'arrival_time' => '17:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 2266,
                'route_day_id' => 199,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 2267,
                'route_day_id' => 199,
                'departure_time' => '17:50:00',
                'arrival_time' => '17:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 2268,
                'route_day_id' => 199,
                'departure_time' => '17:55:00',
                'arrival_time' => '18:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 2269,
                'route_day_id' => 199,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 2270,
                'route_day_id' => 199,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 2271,
                'route_day_id' => 199,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 2272,
                'route_day_id' => 199,
                'departure_time' => '18:20:00',
                'arrival_time' => '18:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 2273,
                'route_day_id' => 199,
                'departure_time' => '18:25:00',
                'arrival_time' => '18:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 2274,
                'route_day_id' => 199,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 2275,
                'route_day_id' => 199,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 2276,
                'route_day_id' => 199,
                'departure_time' => '18:40:00',
                'arrival_time' => '18:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 2277,
                'route_day_id' => 199,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 2278,
                'route_day_id' => 199,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 2279,
                'route_day_id' => 199,
                'departure_time' => '18:55:00',
                'arrival_time' => '19:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 2280,
                'route_day_id' => 199,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 2281,
                'route_day_id' => 199,
                'departure_time' => '19:05:00',
                'arrival_time' => '19:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 2282,
                'route_day_id' => 199,
                'departure_time' => '19:10:00',
                'arrival_time' => '19:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 2283,
                'route_day_id' => 199,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 2284,
                'route_day_id' => 199,
                'departure_time' => '19:20:00',
                'arrival_time' => '19:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 2285,
                'route_day_id' => 199,
                'departure_time' => '19:25:00',
                'arrival_time' => '19:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 2286,
                'route_day_id' => 199,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 2287,
                'route_day_id' => 199,
                'departure_time' => '19:35:00',
                'arrival_time' => '19:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 2288,
                'route_day_id' => 199,
                'departure_time' => '19:40:00',
                'arrival_time' => '19:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 2289,
                'route_day_id' => 199,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 2290,
                'route_day_id' => 199,
                'departure_time' => '19:50:00',
                'arrival_time' => '19:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 2291,
                'route_day_id' => 199,
                'departure_time' => '19:55:00',
                'arrival_time' => '20:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 2292,
                'route_day_id' => 199,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 2293,
                'route_day_id' => 199,
                'departure_time' => '20:10:00',
                'arrival_time' => '20:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 2294,
                'route_day_id' => 199,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 2295,
                'route_day_id' => 199,
                'departure_time' => '20:20:00',
                'arrival_time' => '20:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 2296,
                'route_day_id' => 199,
                'departure_time' => '20:25:00',
                'arrival_time' => '20:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 2297,
                'route_day_id' => 199,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 2298,
                'route_day_id' => 199,
                'departure_time' => '20:40:00',
                'arrival_time' => '20:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 2299,
                'route_day_id' => 199,
                'departure_time' => '20:45:00',
                'arrival_time' => '20:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 2300,
                'route_day_id' => 199,
                'departure_time' => '20:50:00',
                'arrival_time' => '20:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 2301,
                'route_day_id' => 199,
                'departure_time' => '20:55:00',
                'arrival_time' => '21:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 2302,
                'route_day_id' => 199,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 2303,
                'route_day_id' => 199,
                'departure_time' => '21:10:00',
                'arrival_time' => '21:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 2304,
                'route_day_id' => 199,
                'departure_time' => '21:15:00',
                'arrival_time' => '21:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 2305,
                'route_day_id' => 199,
                'departure_time' => '21:20:00',
                'arrival_time' => '21:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 2306,
                'route_day_id' => 199,
                'departure_time' => '21:25:00',
                'arrival_time' => '21:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 2307,
                'route_day_id' => 199,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 2308,
                'route_day_id' => 199,
                'departure_time' => '21:40:00',
                'arrival_time' => '21:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 2309,
                'route_day_id' => 199,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 2310,
                'route_day_id' => 199,
                'departure_time' => '21:50:00',
                'arrival_time' => '21:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 2311,
                'route_day_id' => 199,
                'departure_time' => '21:55:00',
                'arrival_time' => '22:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 2312,
                'route_day_id' => 199,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 2313,
                'route_day_id' => 199,
                'departure_time' => '22:10:00',
                'arrival_time' => '22:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 2314,
                'route_day_id' => 199,
                'departure_time' => '22:20:00',
                'arrival_time' => '22:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 2315,
                'route_day_id' => 199,
                'departure_time' => '22:25:00',
                'arrival_time' => '22:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 2316,
                'route_day_id' => 199,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 2317,
                'route_day_id' => 199,
                'departure_time' => '22:40:00',
                'arrival_time' => '22:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 2318,
                'route_day_id' => 199,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 2319,
                'route_day_id' => 199,
                'departure_time' => '22:50:00',
                'arrival_time' => '22:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 2320,
                'route_day_id' => 199,
                'departure_time' => '22:55:00',
                'arrival_time' => '23:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 2321,
                'route_day_id' => 199,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 2322,
                'route_day_id' => 199,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 2323,
                'route_day_id' => 199,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 2324,
                'route_day_id' => 199,
                'departure_time' => '23:20:00',
                'arrival_time' => '23:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 2325,
                'route_day_id' => 199,
                'departure_time' => '23:30:00',
                'arrival_time' => '23:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 2326,
                'route_day_id' => 199,
                'departure_time' => '23:35:00',
                'arrival_time' => '23:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 2327,
                'route_day_id' => 199,
                'departure_time' => '23:40:00',
                'arrival_time' => '23:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 2328,
                'route_day_id' => 199,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 2329,
                'route_day_id' => 201,
                'departure_time' => '00:05:00',
                'arrival_time' => '00:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 2330,
                'route_day_id' => 201,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 2331,
                'route_day_id' => 201,
                'departure_time' => '00:25:00',
                'arrival_time' => '00:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 2332,
                'route_day_id' => 201,
                'departure_time' => '00:35:00',
                'arrival_time' => '00:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 2333,
                'route_day_id' => 201,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 2334,
                'route_day_id' => 201,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 2335,
                'route_day_id' => 201,
                'departure_time' => '01:15:00',
                'arrival_time' => '01:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 2336,
                'route_day_id' => 201,
                'departure_time' => '01:45:00',
                'arrival_time' => '01:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 2337,
                'route_day_id' => 201,
                'departure_time' => '02:15:00',
                'arrival_time' => '02:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 2338,
                'route_day_id' => 201,
                'departure_time' => '03:15:00',
                'arrival_time' => '03:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 2339,
                'route_day_id' => 201,
                'departure_time' => '04:15:00',
                'arrival_time' => '04:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 2340,
                'route_day_id' => 201,
                'departure_time' => '05:15:00',
                'arrival_time' => '05:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 2341,
                'route_day_id' => 201,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 2342,
                'route_day_id' => 201,
                'departure_time' => '06:25:00',
                'arrival_time' => '06:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 2343,
                'route_day_id' => 201,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 2344,
                'route_day_id' => 201,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 2345,
                'route_day_id' => 201,
                'departure_time' => '06:55:00',
                'arrival_time' => '07:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 2346,
                'route_day_id' => 201,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 2347,
                'route_day_id' => 201,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 2348,
                'route_day_id' => 201,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 2349,
                'route_day_id' => 201,
                'departure_time' => '07:35:00',
                'arrival_time' => '07:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 2350,
                'route_day_id' => 201,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 2351,
                'route_day_id' => 201,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 2352,
                'route_day_id' => 201,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 2353,
                'route_day_id' => 201,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 2354,
                'route_day_id' => 201,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 2355,
                'route_day_id' => 201,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 2356,
                'route_day_id' => 201,
                'departure_time' => '08:45:00',
                'arrival_time' => '08:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 2357,
                'route_day_id' => 201,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 2358,
                'route_day_id' => 201,
                'departure_time' => '09:05:00',
                'arrival_time' => '09:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 2359,
                'route_day_id' => 201,
                'departure_time' => '09:15:00',
                'arrival_time' => '09:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 2360,
                'route_day_id' => 201,
                'departure_time' => '09:25:00',
                'arrival_time' => '09:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 2361,
                'route_day_id' => 201,
                'departure_time' => '09:35:00',
                'arrival_time' => '09:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 2362,
                'route_day_id' => 201,
                'departure_time' => '09:45:00',
                'arrival_time' => '09:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 2363,
                'route_day_id' => 201,
                'departure_time' => '09:55:00',
                'arrival_time' => '10:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 2364,
                'route_day_id' => 201,
                'departure_time' => '10:05:00',
                'arrival_time' => '10:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 2365,
                'route_day_id' => 201,
                'departure_time' => '10:15:00',
                'arrival_time' => '10:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 2366,
                'route_day_id' => 201,
                'departure_time' => '10:25:00',
                'arrival_time' => '10:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 2367,
                'route_day_id' => 201,
                'departure_time' => '10:35:00',
                'arrival_time' => '10:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 2368,
                'route_day_id' => 201,
                'departure_time' => '10:45:00',
                'arrival_time' => '10:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 2369,
                'route_day_id' => 201,
                'departure_time' => '10:50:00',
                'arrival_time' => '10:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 2370,
                'route_day_id' => 201,
                'departure_time' => '10:55:00',
                'arrival_time' => '11:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 2392,
                'route_day_id' => 201,
                'departure_time' => '11:05:00',
                'arrival_time' => '11:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 2393,
                'route_day_id' => 201,
                'departure_time' => '11:15:00',
                'arrival_time' => '11:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 2394,
                'route_day_id' => 201,
                'departure_time' => '11:20:00',
                'arrival_time' => '11:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 2395,
                'route_day_id' => 201,
                'departure_time' => '11:25:00',
                'arrival_time' => '11:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 2396,
                'route_day_id' => 201,
                'departure_time' => '11:35:00',
                'arrival_time' => '11:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 2397,
                'route_day_id' => 201,
                'departure_time' => '11:45:00',
                'arrival_time' => '11:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 2398,
                'route_day_id' => 201,
                'departure_time' => '11:55:00',
                'arrival_time' => '12:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 2399,
                'route_day_id' => 201,
                'departure_time' => '12:05:00',
                'arrival_time' => '12:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 2400,
                'route_day_id' => 201,
                'departure_time' => '12:15:00',
                'arrival_time' => '12:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 2401,
                'route_day_id' => 201,
                'departure_time' => '12:25:00',
                'arrival_time' => '12:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 2402,
                'route_day_id' => 201,
                'departure_time' => '12:35:00',
                'arrival_time' => '12:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 2403,
                'route_day_id' => 201,
                'departure_time' => '12:45:00',
                'arrival_time' => '12:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 2404,
                'route_day_id' => 201,
                'departure_time' => '12:55:00',
                'arrival_time' => '13:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 2405,
                'route_day_id' => 201,
                'departure_time' => '13:05:00',
                'arrival_time' => '13:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 2406,
                'route_day_id' => 201,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 2407,
                'route_day_id' => 201,
                'departure_time' => '13:25:00',
                'arrival_time' => '13:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 2408,
                'route_day_id' => 201,
                'departure_time' => '13:35:00',
                'arrival_time' => '13:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 2409,
                'route_day_id' => 201,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 2410,
                'route_day_id' => 201,
                'departure_time' => '13:55:00',
                'arrival_time' => '14:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 2411,
                'route_day_id' => 201,
                'departure_time' => '14:05:00',
                'arrival_time' => '14:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 2412,
                'route_day_id' => 201,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 2414,
                'route_day_id' => 201,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 2415,
                'route_day_id' => 201,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 2416,
                'route_day_id' => 201,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 2417,
                'route_day_id' => 201,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 2418,
                'route_day_id' => 201,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 2419,
                'route_day_id' => 201,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 2420,
                'route_day_id' => 201,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 2421,
                'route_day_id' => 201,
                'departure_time' => '15:35:00',
                'arrival_time' => '15:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 2422,
                'route_day_id' => 201,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 2423,
                'route_day_id' => 201,
                'departure_time' => '15:55:00',
                'arrival_time' => '16:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 2424,
                'route_day_id' => 201,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 2425,
                'route_day_id' => 201,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 2426,
                'route_day_id' => 201,
                'departure_time' => '16:25:00',
                'arrival_time' => '16:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 2427,
                'route_day_id' => 201,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 2428,
                'route_day_id' => 201,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 2429,
                'route_day_id' => 201,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 2430,
                'route_day_id' => 201,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 2431,
                'route_day_id' => 201,
                'departure_time' => '16:55:00',
                'arrival_time' => '17:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 2432,
                'route_day_id' => 201,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 2433,
                'route_day_id' => 201,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 2434,
                'route_day_id' => 201,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 2435,
                'route_day_id' => 201,
                'departure_time' => '17:15:00',
                'arrival_time' => '18:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 2436,
                'route_day_id' => 201,
                'departure_time' => '17:25:00',
                'arrival_time' => '18:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 2437,
                'route_day_id' => 201,
                'departure_time' => '17:30:00',
                'arrival_time' => '18:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 2438,
                'route_day_id' => 201,
                'departure_time' => '17:35:00',
                'arrival_time' => '18:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 2439,
                'route_day_id' => 201,
                'departure_time' => '17:40:00',
                'arrival_time' => '18:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 2440,
                'route_day_id' => 201,
                'departure_time' => '17:45:00',
                'arrival_time' => '18:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 2441,
                'route_day_id' => 201,
                'departure_time' => '17:55:00',
                'arrival_time' => '18:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 2442,
                'route_day_id' => 201,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 2443,
                'route_day_id' => 201,
                'departure_time' => '18:05:00',
                'arrival_time' => '18:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 2444,
                'route_day_id' => 201,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 2445,
                'route_day_id' => 201,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 2446,
                'route_day_id' => 201,
                'departure_time' => '18:25:00',
                'arrival_time' => '18:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 2447,
                'route_day_id' => 201,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 2448,
                'route_day_id' => 201,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 2449,
                'route_day_id' => 201,
                'departure_time' => '18:40:00',
                'arrival_time' => '18:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 2450,
                'route_day_id' => 201,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 2451,
                'route_day_id' => 201,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 2452,
                'route_day_id' => 201,
                'departure_time' => '18:55:00',
                'arrival_time' => '19:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 2453,
                'route_day_id' => 201,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 2454,
                'route_day_id' => 201,
                'departure_time' => '19:05:00',
                'arrival_time' => '19:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 2455,
                'route_day_id' => 201,
                'departure_time' => '19:10:00',
                'arrival_time' => '19:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 2456,
                'route_day_id' => 201,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 2457,
                'route_day_id' => 201,
                'departure_time' => '19:20:00',
                'arrival_time' => '19:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 2458,
                'route_day_id' => 201,
                'departure_time' => '19:25:00',
                'arrival_time' => '19:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 2459,
                'route_day_id' => 201,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 2460,
                'route_day_id' => 201,
                'departure_time' => '19:35:00',
                'arrival_time' => '19:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 2461,
                'route_day_id' => 201,
                'departure_time' => '19:40:00',
                'arrival_time' => '19:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 2462,
                'route_day_id' => 201,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 2463,
                'route_day_id' => 201,
                'departure_time' => '19:50:00',
                'arrival_time' => '19:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 2464,
                'route_day_id' => 201,
                'departure_time' => '19:55:00',
                'arrival_time' => '20:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 2465,
                'route_day_id' => 201,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 2466,
                'route_day_id' => 201,
                'departure_time' => '20:05:00',
                'arrival_time' => '20:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 2467,
                'route_day_id' => 201,
                'departure_time' => '20:10:00',
                'arrival_time' => '20:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 2468,
                'route_day_id' => 201,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 2469,
                'route_day_id' => 201,
                'departure_time' => '20:25:00',
                'arrival_time' => '20:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 2470,
                'route_day_id' => 201,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 2471,
                'route_day_id' => 201,
                'departure_time' => '20:35:00',
                'arrival_time' => '20:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 2472,
                'route_day_id' => 201,
                'departure_time' => '20:40:00',
                'arrival_time' => '20:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 2473,
                'route_day_id' => 201,
                'departure_time' => '20:45:00',
                'arrival_time' => '20:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 2474,
                'route_day_id' => 201,
                'departure_time' => '20:55:00',
                'arrival_time' => '21:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 2475,
                'route_day_id' => 201,
                'departure_time' => '21:05:00',
                'arrival_time' => '21:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 2476,
                'route_day_id' => 201,
                'departure_time' => '21:10:00',
                'arrival_time' => '21:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 2477,
                'route_day_id' => 201,
                'departure_time' => '21:15:00',
                'arrival_time' => '21:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 2478,
                'route_day_id' => 201,
                'departure_time' => '21:25:00',
                'arrival_time' => '21:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 2479,
                'route_day_id' => 201,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 2480,
                'route_day_id' => 201,
                'departure_time' => '21:35:00',
                'arrival_time' => '21:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 2481,
                'route_day_id' => 201,
                'departure_time' => '21:40:00',
                'arrival_time' => '21:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 2482,
                'route_day_id' => 201,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 2483,
                'route_day_id' => 201,
                'departure_time' => '21:55:00',
                'arrival_time' => '22:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 2484,
                'route_day_id' => 201,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 2485,
                'route_day_id' => 201,
                'departure_time' => '22:05:00',
                'arrival_time' => '22:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 2486,
                'route_day_id' => 201,
                'departure_time' => '22:10:00',
                'arrival_time' => '22:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 2487,
                'route_day_id' => 201,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 2488,
                'route_day_id' => 201,
                'departure_time' => '22:25:00',
                'arrival_time' => '22:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 2489,
                'route_day_id' => 201,
                'departure_time' => '22:35:00',
                'arrival_time' => '22:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 2490,
                'route_day_id' => 201,
                'departure_time' => '22:40:00',
                'arrival_time' => '22:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 2491,
                'route_day_id' => 201,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 2492,
                'route_day_id' => 201,
                'departure_time' => '22:55:00',
                'arrival_time' => '23:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 2493,
                'route_day_id' => 201,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 2494,
                'route_day_id' => 201,
                'departure_time' => '23:05:00',
                'arrival_time' => '23:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 2495,
                'route_day_id' => 201,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 2496,
                'route_day_id' => 201,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 2497,
                'route_day_id' => 201,
                'departure_time' => '23:25:00',
                'arrival_time' => '23:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 2498,
                'route_day_id' => 201,
                'departure_time' => '23:30:00',
                'arrival_time' => '23:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 2499,
                'route_day_id' => 201,
                'departure_time' => '23:35:00',
                'arrival_time' => '23:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 2500,
                'route_day_id' => 201,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 2501,
                'route_day_id' => 201,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 2502,
                'route_day_id' => 201,
                'departure_time' => '23:55:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 2654,
                'route_day_id' => 223,
                'departure_time' => '00:00:00',
                'arrival_time' => '00:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 2655,
                'route_day_id' => 223,
                'departure_time' => '00:10:00',
                'arrival_time' => '00:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 2656,
                'route_day_id' => 223,
                'departure_time' => '00:20:00',
                'arrival_time' => '00:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 2657,
                'route_day_id' => 223,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 2658,
                'route_day_id' => 223,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 2659,
                'route_day_id' => 223,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 2660,
                'route_day_id' => 223,
                'departure_time' => '01:30:00',
                'arrival_time' => '01:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 2661,
                'route_day_id' => 223,
                'departure_time' => '02:00:00',
                'arrival_time' => '02:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 2662,
                'route_day_id' => 223,
                'departure_time' => '03:00:00',
                'arrival_time' => '03:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 2663,
                'route_day_id' => 223,
                'departure_time' => '04:00:00',
                'arrival_time' => '04:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 2664,
                'route_day_id' => 223,
                'departure_time' => '05:00:00',
                'arrival_time' => '05:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 2665,
                'route_day_id' => 223,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 2666,
                'route_day_id' => 223,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 2667,
                'route_day_id' => 223,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 2668,
                'route_day_id' => 223,
                'departure_time' => '06:10:00',
                'arrival_time' => '06:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 2669,
                'route_day_id' => 223,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 2670,
                'route_day_id' => 223,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 2671,
                'route_day_id' => 223,
                'departure_time' => '06:40:00',
                'arrival_time' => '06:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 2672,
                'route_day_id' => 223,
                'departure_time' => '06:50:00',
                'arrival_time' => '06:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 2673,
                'route_day_id' => 223,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 2674,
                'route_day_id' => 223,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 2675,
                'route_day_id' => 223,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 2676,
                'route_day_id' => 223,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 2677,
                'route_day_id' => 223,
                'departure_time' => '07:40:00',
                'arrival_time' => '07:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 2678,
                'route_day_id' => 223,
                'departure_time' => '07:50:00',
                'arrival_time' => '07:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 2679,
                'route_day_id' => 223,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 2680,
                'route_day_id' => 223,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 2681,
                'route_day_id' => 223,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 2682,
                'route_day_id' => 223,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 2683,
                'route_day_id' => 223,
                'departure_time' => '08:40:00',
                'arrival_time' => '08:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 2684,
                'route_day_id' => 223,
                'departure_time' => '08:50:00',
                'arrival_time' => '08:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 2685,
                'route_day_id' => 223,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 2686,
                'route_day_id' => 223,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 2687,
                'route_day_id' => 223,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 2688,
                'route_day_id' => 223,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 2689,
                'route_day_id' => 223,
                'departure_time' => '09:40:00',
                'arrival_time' => '09:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 2690,
                'route_day_id' => 223,
                'departure_time' => '09:50:00',
                'arrival_time' => '09:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 2691,
                'route_day_id' => 223,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 2692,
                'route_day_id' => 223,
                'departure_time' => '10:10:00',
                'arrival_time' => '10:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 2693,
                'route_day_id' => 223,
                'departure_time' => '10:20:00',
                'arrival_time' => '10:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 2694,
                'route_day_id' => 223,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_times')->insert(array (
            0 => 
            array (
                'id' => 2695,
                'route_day_id' => 223,
                'departure_time' => '10:40:00',
                'arrival_time' => '10:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2696,
                'route_day_id' => 223,
                'departure_time' => '10:50:00',
                'arrival_time' => '10:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 2697,
                'route_day_id' => 223,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 2698,
                'route_day_id' => 223,
                'departure_time' => '11:10:00',
                'arrival_time' => '11:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 2699,
                'route_day_id' => 223,
                'departure_time' => '11:20:00',
                'arrival_time' => '11:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 2700,
                'route_day_id' => 223,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 2701,
                'route_day_id' => 223,
                'departure_time' => '11:40:00',
                'arrival_time' => '11:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 2702,
                'route_day_id' => 223,
                'departure_time' => '11:50:00',
                'arrival_time' => '11:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 2703,
                'route_day_id' => 223,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 2704,
                'route_day_id' => 223,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 2705,
                'route_day_id' => 223,
                'departure_time' => '13:35:00',
                'arrival_time' => '13:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 2706,
                'route_day_id' => 223,
                'departure_time' => '13:40:00',
                'arrival_time' => '13:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 2707,
                'route_day_id' => 223,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 2708,
                'route_day_id' => 223,
                'departure_time' => '13:50:00',
                'arrival_time' => '13:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 2709,
                'route_day_id' => 223,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 2710,
                'route_day_id' => 223,
                'departure_time' => '14:05:00',
                'arrival_time' => '14:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 2711,
                'route_day_id' => 223,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 2712,
                'route_day_id' => 223,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 2713,
                'route_day_id' => 223,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 2714,
                'route_day_id' => 223,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 2715,
                'route_day_id' => 223,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 2716,
                'route_day_id' => 223,
                'departure_time' => '14:40:00',
                'arrival_time' => '14:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 2717,
                'route_day_id' => 223,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 2718,
                'route_day_id' => 223,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 2719,
                'route_day_id' => 223,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 2720,
                'route_day_id' => 223,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 2721,
                'route_day_id' => 223,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 2722,
                'route_day_id' => 223,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 2723,
                'route_day_id' => 223,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 2724,
                'route_day_id' => 223,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 2725,
                'route_day_id' => 223,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 2726,
                'route_day_id' => 223,
                'departure_time' => '15:35:00',
                'arrival_time' => '15:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 2727,
                'route_day_id' => 223,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 2728,
                'route_day_id' => 223,
                'departure_time' => '15:50:00',
                'arrival_time' => '15:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 2729,
                'route_day_id' => 223,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 2730,
                'route_day_id' => 223,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 2731,
                'route_day_id' => 223,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 2732,
                'route_day_id' => 223,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 2733,
                'route_day_id' => 223,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 2734,
                'route_day_id' => 223,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 2735,
                'route_day_id' => 223,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 2736,
                'route_day_id' => 223,
                'departure_time' => '16:40:00',
                'arrival_time' => '19:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 2737,
                'route_day_id' => 223,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 2738,
                'route_day_id' => 223,
                'departure_time' => '16:50:00',
                'arrival_time' => '19:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 2739,
                'route_day_id' => 223,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 2740,
                'route_day_id' => 223,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 2741,
                'route_day_id' => 223,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 2742,
                'route_day_id' => 223,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 2743,
                'route_day_id' => 223,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 2744,
                'route_day_id' => 223,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 2745,
                'route_day_id' => 223,
                'departure_time' => '17:35:00',
                'arrival_time' => '17:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 2746,
                'route_day_id' => 223,
                'departure_time' => '17:40:00',
                'arrival_time' => '17:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 2747,
                'route_day_id' => 223,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 2748,
                'route_day_id' => 223,
                'departure_time' => '17:50:00',
                'arrival_time' => '17:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 2749,
                'route_day_id' => 223,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 2750,
                'route_day_id' => 223,
                'departure_time' => '18:05:00',
                'arrival_time' => '18:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 2751,
                'route_day_id' => 223,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 2752,
                'route_day_id' => 223,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 2753,
                'route_day_id' => 223,
                'departure_time' => '18:20:00',
                'arrival_time' => '18:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 2754,
                'route_day_id' => 223,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 2755,
                'route_day_id' => 223,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 2756,
                'route_day_id' => 223,
                'departure_time' => '18:40:00',
                'arrival_time' => '18:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 2757,
                'route_day_id' => 223,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 2758,
                'route_day_id' => 223,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 2759,
                'route_day_id' => 223,
                'departure_time' => '19:05:00',
                'arrival_time' => '19:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 2760,
                'route_day_id' => 223,
                'departure_time' => '19:10:00',
                'arrival_time' => '19:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 2761,
                'route_day_id' => 223,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 2762,
                'route_day_id' => 223,
                'departure_time' => '19:20:00',
                'arrival_time' => '19:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 2763,
                'route_day_id' => 223,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 2764,
                'route_day_id' => 223,
                'departure_time' => '19:35:00',
                'arrival_time' => '19:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 2765,
                'route_day_id' => 223,
                'departure_time' => '19:40:00',
                'arrival_time' => '19:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 2766,
                'route_day_id' => 223,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 2767,
                'route_day_id' => 223,
                'departure_time' => '19:50:00',
                'arrival_time' => '19:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 2768,
                'route_day_id' => 223,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 2769,
                'route_day_id' => 223,
                'departure_time' => '20:05:00',
                'arrival_time' => '20:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 2770,
                'route_day_id' => 223,
                'departure_time' => '20:10:00',
                'arrival_time' => '20:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 2771,
                'route_day_id' => 223,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 2772,
                'route_day_id' => 223,
                'departure_time' => '20:20:00',
                'arrival_time' => '20:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 2773,
                'route_day_id' => 223,
                'departure_time' => '20:25:00',
                'arrival_time' => '20:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 2774,
                'route_day_id' => 223,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 2775,
                'route_day_id' => 223,
                'departure_time' => '20:35:00',
                'arrival_time' => '20:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 2776,
                'route_day_id' => 223,
                'departure_time' => '20:40:00',
                'arrival_time' => '20:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 2777,
                'route_day_id' => 223,
                'departure_time' => '20:50:00',
                'arrival_time' => '20:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 2778,
                'route_day_id' => 223,
                'departure_time' => '20:55:00',
                'arrival_time' => '21:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 2779,
                'route_day_id' => 223,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 2780,
                'route_day_id' => 223,
                'departure_time' => '21:05:00',
                'arrival_time' => '21:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 2781,
                'route_day_id' => 223,
                'departure_time' => '21:10:00',
                'arrival_time' => '21:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 2782,
                'route_day_id' => 223,
                'departure_time' => '21:20:00',
                'arrival_time' => '21:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 2783,
                'route_day_id' => 223,
                'departure_time' => '21:25:00',
                'arrival_time' => '21:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 2784,
                'route_day_id' => 223,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 2785,
                'route_day_id' => 223,
                'departure_time' => '21:40:00',
                'arrival_time' => '21:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 2786,
                'route_day_id' => 223,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 2787,
                'route_day_id' => 223,
                'departure_time' => '21:50:00',
                'arrival_time' => '21:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 2788,
                'route_day_id' => 223,
                'departure_time' => '21:55:00',
                'arrival_time' => '22:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 2789,
                'route_day_id' => 223,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 2790,
                'route_day_id' => 223,
                'departure_time' => '22:10:00',
                'arrival_time' => '22:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 2791,
                'route_day_id' => 223,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 2792,
                'route_day_id' => 223,
                'departure_time' => '22:20:00',
                'arrival_time' => '22:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 2793,
                'route_day_id' => 223,
                'departure_time' => '22:25:00',
                'arrival_time' => '22:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 2794,
                'route_day_id' => 223,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 2795,
                'route_day_id' => 223,
                'departure_time' => '22:40:00',
                'arrival_time' => '22:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 2796,
                'route_day_id' => 223,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 2797,
                'route_day_id' => 223,
                'departure_time' => '22:50:00',
                'arrival_time' => '22:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 2798,
                'route_day_id' => 223,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 2799,
                'route_day_id' => 223,
                'departure_time' => '23:05:00',
                'arrival_time' => '23:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 2800,
                'route_day_id' => 223,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 2801,
                'route_day_id' => 223,
                'departure_time' => '23:20:00',
                'arrival_time' => '23:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 2802,
                'route_day_id' => 223,
                'departure_time' => '23:30:00',
                'arrival_time' => '23:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 2803,
                'route_day_id' => 223,
                'departure_time' => '23:35:00',
                'arrival_time' => '23:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 2804,
                'route_day_id' => 223,
                'departure_time' => '23:40:00',
                'arrival_time' => '23:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 2805,
                'route_day_id' => 223,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 2806,
                'route_day_id' => 224,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 2807,
                'route_day_id' => 224,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 2808,
                'route_day_id' => 225,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 2809,
                'route_day_id' => 225,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 2810,
                'route_day_id' => 225,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 2811,
                'route_day_id' => 226,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 2812,
                'route_day_id' => 226,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 2813,
                'route_day_id' => 226,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 2814,
                'route_day_id' => 226,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 2815,
                'route_day_id' => 226,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 2816,
                'route_day_id' => 226,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 2817,
                'route_day_id' => 226,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 2818,
                'route_day_id' => 226,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 2819,
                'route_day_id' => 226,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 2820,
                'route_day_id' => 226,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 2821,
                'route_day_id' => 226,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 2822,
                'route_day_id' => 227,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 2823,
                'route_day_id' => 227,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 2824,
                'route_day_id' => 227,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 2825,
                'route_day_id' => 227,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 2826,
                'route_day_id' => 227,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 2827,
                'route_day_id' => 227,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 2828,
                'route_day_id' => 227,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 2829,
                'route_day_id' => 227,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 2830,
                'route_day_id' => 227,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 2831,
                'route_day_id' => 227,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 2832,
                'route_day_id' => 227,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 2833,
                'route_day_id' => 227,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 2834,
                'route_day_id' => 227,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 2835,
                'route_day_id' => 228,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 2836,
                'route_day_id' => 228,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 2837,
                'route_day_id' => 228,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 2838,
                'route_day_id' => 228,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 2839,
                'route_day_id' => 228,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 2840,
                'route_day_id' => 228,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 2841,
                'route_day_id' => 228,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 2842,
                'route_day_id' => 228,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 2843,
                'route_day_id' => 228,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 2844,
                'route_day_id' => 228,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 2845,
                'route_day_id' => 228,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 2846,
                'route_day_id' => 229,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 2847,
                'route_day_id' => 229,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 2848,
                'route_day_id' => 229,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 2849,
                'route_day_id' => 229,
                'departure_time' => '13:20:00',
                'arrival_time' => '13:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 2850,
                'route_day_id' => 229,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 2851,
                'route_day_id' => 229,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 2852,
                'route_day_id' => 229,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 2853,
                'route_day_id' => 229,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 2854,
                'route_day_id' => 229,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 2855,
                'route_day_id' => 229,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 2856,
                'route_day_id' => 229,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 2857,
                'route_day_id' => 229,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 2858,
                'route_day_id' => 229,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 2859,
                'route_day_id' => 230,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 2860,
                'route_day_id' => 231,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 2861,
                'route_day_id' => 232,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 2862,
                'route_day_id' => 233,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 2863,
                'route_day_id' => 234,
                'departure_time' => '08:35:00',
                'arrival_time' => '09:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 2864,
                'route_day_id' => 235,
                'departure_time' => '13:00:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 2865,
                'route_day_id' => 236,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 2866,
                'route_day_id' => 237,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 2867,
                'route_day_id' => 238,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 2868,
                'route_day_id' => 239,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 2869,
                'route_day_id' => 240,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 2870,
                'route_day_id' => 241,
                'departure_time' => '06:25:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 2871,
                'route_day_id' => 242,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 2872,
                'route_day_id' => 243,
                'departure_time' => '07:15:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 2873,
                'route_day_id' => 244,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 2874,
                'route_day_id' => 245,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 2875,
                'route_day_id' => 246,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 2876,
                'route_day_id' => 247,
                'departure_time' => '15:55:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 2883,
                'route_day_id' => 254,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 2884,
                'route_day_id' => 255,
                'departure_time' => '06:25:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 2885,
                'route_day_id' => 256,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 2886,
                'route_day_id' => 257,
                'departure_time' => '07:15:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 2887,
                'route_day_id' => 258,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 2888,
                'route_day_id' => 259,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 2889,
                'route_day_id' => 260,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 2890,
                'route_day_id' => 261,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 2891,
                'route_day_id' => 262,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 2892,
                'route_day_id' => 263,
                'departure_time' => '15:40:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 2893,
                'route_day_id' => 264,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 2894,
                'route_day_id' => 265,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 2895,
                'route_day_id' => 266,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 2896,
                'route_day_id' => 267,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 2897,
                'route_day_id' => 268,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 2898,
                'route_day_id' => 269,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 2899,
                'route_day_id' => 270,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 2900,
                'route_day_id' => 271,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 2901,
                'route_day_id' => 272,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 2902,
                'route_day_id' => 273,
                'departure_time' => '07:30:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 2903,
                'route_day_id' => 274,
                'departure_time' => '13:00:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 2904,
                'route_day_id' => 275,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 2905,
                'route_day_id' => 276,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 2906,
                'route_day_id' => 277,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 2907,
                'route_day_id' => 278,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 2908,
                'route_day_id' => 279,
                'departure_time' => '06:55:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 2909,
                'route_day_id' => 280,
                'departure_time' => '07:25:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 2910,
                'route_day_id' => 281,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 2911,
                'route_day_id' => 282,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 2912,
                'route_day_id' => 283,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 2913,
                'route_day_id' => 284,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 2914,
                'route_day_id' => 285,
                'departure_time' => '14:45:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 2915,
                'route_day_id' => 286,
                'departure_time' => '15:35:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 2916,
                'route_day_id' => 287,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 2917,
                'route_day_id' => 288,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 2918,
                'route_day_id' => 289,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 2919,
                'route_day_id' => 290,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 2920,
                'route_day_id' => 291,
                'departure_time' => '07:50:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 2921,
                'route_day_id' => 292,
                'departure_time' => '08:15:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 2922,
                'route_day_id' => 293,
                'departure_time' => '13:30:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 2923,
                'route_day_id' => 294,
                'departure_time' => '14:45:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 2924,
                'route_day_id' => 295,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 2925,
                'route_day_id' => 296,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 2926,
                'route_day_id' => 297,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 2927,
                'route_day_id' => 298,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 2928,
                'route_day_id' => 299,
                'departure_time' => '06:25:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 2929,
                'route_day_id' => 300,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 2930,
                'route_day_id' => 301,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 2931,
                'route_day_id' => 302,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 2932,
                'route_day_id' => 303,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 2933,
                'route_day_id' => 304,
                'departure_time' => '14:40:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 2934,
                'route_day_id' => 305,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 2935,
                'route_day_id' => 306,
                'departure_time' => '15:30:00',
                'arrival_time' => '16:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 2936,
                'route_day_id' => 307,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 2937,
                'route_day_id' => 308,
                'departure_time' => '06:00:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 2938,
                'route_day_id' => 309,
                'departure_time' => '07:50:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 2939,
                'route_day_id' => 310,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 2940,
                'route_day_id' => 311,
                'departure_time' => '13:30:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 2941,
                'route_day_id' => 312,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 2942,
                'route_day_id' => 313,
                'departure_time' => '14:35:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 2943,
                'route_day_id' => 314,
                'departure_time' => '06:15:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 2944,
                'route_day_id' => 315,
                'departure_time' => '06:45:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 2945,
                'route_day_id' => 316,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 2946,
                'route_day_id' => 317,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 2947,
                'route_day_id' => 318,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 2948,
                'route_day_id' => 319,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 2949,
                'route_day_id' => 320,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 2950,
                'route_day_id' => 321,
                'departure_time' => '06:15:00',
                'arrival_time' => '07:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 2951,
                'route_day_id' => 322,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 2954,
                'route_day_id' => 325,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 2955,
                'route_day_id' => 325,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 2956,
                'route_day_id' => 326,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 2957,
                'route_day_id' => 326,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 2958,
                'route_day_id' => 327,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 2959,
                'route_day_id' => 328,
                'departure_time' => '08:30:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 2960,
                'route_day_id' => 329,
                'departure_time' => '13:15:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 2961,
                'route_day_id' => 330,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 2962,
                'route_day_id' => 331,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 2963,
                'route_day_id' => 332,
                'departure_time' => '15:40:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 2964,
                'route_day_id' => 333,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 2965,
                'route_day_id' => 334,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 2966,
                'route_day_id' => 335,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 2967,
                'route_day_id' => 336,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 2968,
                'route_day_id' => 337,
                'departure_time' => '08:40:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 2969,
                'route_day_id' => 338,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 2970,
                'route_day_id' => 339,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 2971,
                'route_day_id' => 340,
                'departure_time' => '13:45:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 2972,
                'route_day_id' => 341,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 2973,
                'route_day_id' => 342,
                'departure_time' => '15:00:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 2974,
                'route_day_id' => 343,
                'departure_time' => '16:05:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 2975,
                'route_day_id' => 344,
                'departure_time' => '06:00:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 2976,
                'route_day_id' => 345,
                'departure_time' => '07:05:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 2977,
                'route_day_id' => 346,
                'departure_time' => '08:30:00',
                'arrival_time' => '09:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 2978,
                'route_day_id' => 347,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 2979,
                'route_day_id' => 348,
                'departure_time' => '14:00:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 2980,
                'route_day_id' => 349,
                'departure_time' => '15:25:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 2981,
                'route_day_id' => 350,
                'departure_time' => '05:50:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 2982,
                'route_day_id' => 351,
                'departure_time' => '06:55:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 2983,
                'route_day_id' => 352,
                'departure_time' => '07:55:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 2984,
                'route_day_id' => 353,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 2985,
                'route_day_id' => 354,
                'departure_time' => '15:05:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 2986,
                'route_day_id' => 355,
                'departure_time' => '16:05:00',
                'arrival_time' => '17:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 2987,
                'route_day_id' => 356,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 2988,
                'route_day_id' => 357,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 2989,
                'route_day_id' => 358,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 2990,
                'route_day_id' => 359,
                'departure_time' => '14:00:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 2991,
                'route_day_id' => 360,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 2992,
                'route_day_id' => 361,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 2993,
                'route_day_id' => 362,
                'departure_time' => '05:30:00',
                'arrival_time' => '06:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 2994,
                'route_day_id' => 363,
                'departure_time' => '06:15:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 2995,
                'route_day_id' => 364,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 2996,
                'route_day_id' => 365,
                'departure_time' => '07:55:00',
                'arrival_time' => '09:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 2997,
                'route_day_id' => 366,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 2998,
                'route_day_id' => 367,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 2999,
                'route_day_id' => 368,
                'departure_time' => '16:00:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 3000,
                'route_day_id' => 369,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 3001,
                'route_day_id' => 370,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 3002,
                'route_day_id' => 371,
                'departure_time' => '06:45:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 3003,
                'route_day_id' => 372,
                'departure_time' => '07:20:00',
                'arrival_time' => '08:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 3004,
                'route_day_id' => 373,
                'departure_time' => '08:20:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 3005,
                'route_day_id' => 374,
                'departure_time' => '13:30:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 3006,
                'route_day_id' => 375,
                'departure_time' => '14:45:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 3007,
                'route_day_id' => 376,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 3008,
                'route_day_id' => 377,
                'departure_time' => '16:20:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 3009,
                'route_day_id' => 378,
                'departure_time' => '06:00:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 3010,
                'route_day_id' => 379,
                'departure_time' => '14:00:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 3011,
                'route_day_id' => 380,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 3012,
                'route_day_id' => 381,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 3013,
                'route_day_id' => 382,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 3014,
                'route_day_id' => 383,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 3015,
                'route_day_id' => 384,
                'departure_time' => '07:45:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 3016,
                'route_day_id' => 385,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 3017,
                'route_day_id' => 386,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 3018,
                'route_day_id' => 387,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 3019,
                'route_day_id' => 388,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 3020,
                'route_day_id' => 389,
                'departure_time' => '14:35:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 3021,
                'route_day_id' => 390,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 3022,
                'route_day_id' => 391,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 3023,
                'route_day_id' => 392,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 3024,
                'route_day_id' => 393,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 3025,
                'route_day_id' => 394,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 3026,
                'route_day_id' => 395,
                'departure_time' => '07:40:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 3027,
                'route_day_id' => 396,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 3028,
                'route_day_id' => 397,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 3029,
                'route_day_id' => 398,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 3030,
                'route_day_id' => 399,
                'departure_time' => '09:25:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 3031,
                'route_day_id' => 400,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 3032,
                'route_day_id' => 401,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 3033,
                'route_day_id' => 402,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 3034,
                'route_day_id' => 403,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 3035,
                'route_day_id' => 404,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 3036,
                'route_day_id' => 405,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 3037,
                'route_day_id' => 406,
                'departure_time' => '06:10:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 3038,
                'route_day_id' => 407,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 3039,
                'route_day_id' => 408,
                'departure_time' => '07:35:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 3040,
                'route_day_id' => 409,
                'departure_time' => '07:45:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 3041,
                'route_day_id' => 410,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 3042,
                'route_day_id' => 411,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 3043,
                'route_day_id' => 412,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 3044,
                'route_day_id' => 413,
                'departure_time' => '14:50:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 3045,
                'route_day_id' => 414,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 3046,
                'route_day_id' => 415,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 3047,
                'route_day_id' => 416,
                'departure_time' => '15:50:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 3048,
                'route_day_id' => 417,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 3049,
                'route_day_id' => 418,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 3050,
                'route_day_id' => 419,
                'departure_time' => '06:35:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 3051,
                'route_day_id' => 420,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 3052,
                'route_day_id' => 421,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 3053,
                'route_day_id' => 422,
                'departure_time' => '07:35:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 3054,
                'route_day_id' => 423,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 3055,
                'route_day_id' => 424,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 3056,
                'route_day_id' => 425,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 3057,
                'route_day_id' => 426,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 3058,
                'route_day_id' => 427,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 3059,
                'route_day_id' => 428,
                'departure_time' => '14:50:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 3060,
                'route_day_id' => 429,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 3061,
                'route_day_id' => 430,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 3062,
                'route_day_id' => 431,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 3063,
                'route_day_id' => 432,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 3064,
                'route_day_id' => 433,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 3065,
                'route_day_id' => 434,
                'departure_time' => '16:50:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 3066,
                'route_day_id' => 435,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 3067,
                'route_day_id' => 436,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 3068,
                'route_day_id' => 437,
                'departure_time' => '07:30:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 3069,
                'route_day_id' => 438,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 3070,
                'route_day_id' => 439,
                'departure_time' => '08:45:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 3071,
                'route_day_id' => 440,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 3072,
                'route_day_id' => 441,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 3073,
                'route_day_id' => 442,
                'departure_time' => '09:45:00',
                'arrival_time' => '09:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 3074,
                'route_day_id' => 443,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 3075,
                'route_day_id' => 444,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 3076,
                'route_day_id' => 445,
                'departure_time' => '14:45:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 3077,
                'route_day_id' => 446,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 3078,
                'route_day_id' => 447,
                'departure_time' => '15:35:00',
                'arrival_time' => '16:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 3079,
                'route_day_id' => 448,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 3080,
                'route_day_id' => 449,
                'departure_time' => '16:45:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 3081,
                'route_day_id' => 450,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 3082,
                'route_day_id' => 451,
                'departure_time' => '08:40:00',
                'arrival_time' => '09:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 3083,
                'route_day_id' => 452,
                'departure_time' => '09:15:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 3084,
                'route_day_id' => 453,
                'departure_time' => '14:15:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 3085,
                'route_day_id' => 454,
                'departure_time' => '15:40:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 3086,
                'route_day_id' => 455,
                'departure_time' => '16:15:00',
                'arrival_time' => '17:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 3087,
                'route_day_id' => 456,
                'departure_time' => '06:30:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 3088,
                'route_day_id' => 457,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 3089,
                'route_day_id' => 458,
                'departure_time' => '07:35:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 3090,
                'route_day_id' => 459,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 3091,
                'route_day_id' => 460,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 3092,
                'route_day_id' => 461,
                'departure_time' => '09:40:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 3093,
                'route_day_id' => 462,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 3094,
                'route_day_id' => 463,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 3095,
                'route_day_id' => 464,
                'departure_time' => '14:40:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 3096,
                'route_day_id' => 465,
                'departure_time' => '15:05:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 3097,
                'route_day_id' => 466,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 3098,
                'route_day_id' => 467,
                'departure_time' => '16:35:00',
                'arrival_time' => '17:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 3099,
                'route_day_id' => 468,
                'departure_time' => '06:30:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 3100,
                'route_day_id' => 469,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 3101,
                'route_day_id' => 470,
                'departure_time' => '08:35:00',
                'arrival_time' => '09:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 3102,
                'route_day_id' => 471,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 3103,
                'route_day_id' => 472,
                'departure_time' => '09:55:00',
                'arrival_time' => '10:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 3104,
                'route_day_id' => 473,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 3105,
                'route_day_id' => 474,
                'departure_time' => '13:50:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 3106,
                'route_day_id' => 475,
                'departure_time' => '14:25:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 3107,
                'route_day_id' => 476,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 3108,
                'route_day_id' => 477,
                'departure_time' => '15:35:00',
                'arrival_time' => '17:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 3109,
                'route_day_id' => 478,
                'departure_time' => '06:30:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 3110,
                'route_day_id' => 479,
                'departure_time' => '07:45:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 3111,
                'route_day_id' => 480,
                'departure_time' => '09:45:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 3112,
                'route_day_id' => 481,
                'departure_time' => '11:00:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 3120,
                'route_day_id' => 489,
                'departure_time' => '10:45:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 3127,
                'route_day_id' => 496,
                'departure_time' => '10:00:00',
                'arrival_time' => '11:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 3128,
                'route_day_id' => 497,
                'departure_time' => '11:30:00',
                'arrival_time' => '13:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 3129,
                'route_day_id' => 498,
                'departure_time' => '13:30:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 3130,
                'route_day_id' => 499,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 3131,
                'route_day_id' => 500,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 3132,
                'route_day_id' => 501,
                'departure_time' => '15:30:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 3145,
                'route_day_id' => 514,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 3146,
                'route_day_id' => 515,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 3147,
                'route_day_id' => 516,
                'departure_time' => '07:50:00',
                'arrival_time' => '09:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 3148,
                'route_day_id' => 517,
                'departure_time' => '15:00:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 3149,
                'route_day_id' => 518,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 3150,
                'route_day_id' => 519,
                'departure_time' => '16:40:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 3151,
                'route_day_id' => 520,
                'departure_time' => '06:00:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 3152,
                'route_day_id' => 521,
                'departure_time' => '07:25:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 3153,
                'route_day_id' => 522,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 3154,
                'route_day_id' => 523,
                'departure_time' => '08:40:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 3155,
                'route_day_id' => 524,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 3156,
                'route_day_id' => 525,
                'departure_time' => '09:50:00',
                'arrival_time' => '10:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 3157,
                'route_day_id' => 526,
                'departure_time' => '10:25:00',
                'arrival_time' => '10:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 3158,
                'route_day_id' => 527,
                'departure_time' => '13:20:00',
                'arrival_time' => '13:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 3159,
                'route_day_id' => 528,
                'departure_time' => '13:45:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 3160,
                'route_day_id' => 529,
                'departure_time' => '14:20:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 3161,
                'route_day_id' => 530,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 3162,
                'route_day_id' => 531,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 3163,
                'route_day_id' => 532,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 3164,
                'route_day_id' => 533,
                'departure_time' => '16:40:00',
                'arrival_time' => '18:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 3166,
                'route_day_id' => 535,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 3167,
                'route_day_id' => 536,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 3168,
                'route_day_id' => 537,
                'departure_time' => '08:50:00',
                'arrival_time' => '09:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 3169,
                'route_day_id' => 538,
                'departure_time' => '09:45:00',
                'arrival_time' => '10:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 3170,
                'route_day_id' => 539,
                'departure_time' => '10:10:00',
                'arrival_time' => '10:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 3171,
                'route_day_id' => 540,
                'departure_time' => '10:55:00',
                'arrival_time' => '11:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 3172,
                'route_day_id' => 541,
                'departure_time' => '11:35:00',
                'arrival_time' => '12:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 3173,
                'route_day_id' => 542,
                'departure_time' => '13:20:00',
                'arrival_time' => '13:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 3174,
                'route_day_id' => 543,
                'departure_time' => '13:40:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 3175,
                'route_day_id' => 544,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 3176,
                'route_day_id' => 545,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 3177,
                'route_day_id' => 546,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 3179,
                'route_day_id' => 548,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 3180,
                'route_day_id' => 549,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 3181,
                'route_day_id' => 550,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 3182,
                'route_day_id' => 551,
                'departure_time' => '08:40:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 3187,
                'route_day_id' => 556,
                'departure_time' => '14:30:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 3188,
                'route_day_id' => 557,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 3189,
                'route_day_id' => 558,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 3190,
                'route_day_id' => 559,
                'departure_time' => '15:55:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 3191,
                'route_day_id' => 560,
                'departure_time' => '05:30:00',
                'arrival_time' => '07:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 3192,
                'route_day_id' => 561,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 3193,
                'route_day_id' => 562,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 3194,
                'route_day_id' => 563,
                'departure_time' => '07:35:00',
                'arrival_time' => '09:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 3195,
                'route_day_id' => 564,
                'departure_time' => '09:05:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 3196,
                'route_day_id' => 565,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 3197,
                'route_day_id' => 566,
                'departure_time' => '15:05:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 3198,
                'route_day_id' => 567,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 3199,
                'route_day_id' => 568,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 3200,
                'route_day_id' => 569,
                'departure_time' => '17:00:00',
                'arrival_time' => '18:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 3201,
                'route_day_id' => 570,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 3202,
                'route_day_id' => 571,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 3203,
                'route_day_id' => 572,
                'departure_time' => '07:30:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 3204,
                'route_day_id' => 573,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 3205,
                'route_day_id' => 574,
                'departure_time' => '08:40:00',
                'arrival_time' => '09:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 3206,
                'route_day_id' => 575,
                'departure_time' => '13:45:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 3207,
                'route_day_id' => 576,
                'departure_time' => '14:50:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 3208,
                'route_day_id' => 577,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 3209,
                'route_day_id' => 578,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 3210,
                'route_day_id' => 579,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 3211,
                'route_day_id' => 580,
                'departure_time' => '06:00:00',
                'arrival_time' => '07:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 3212,
                'route_day_id' => 581,
                'departure_time' => '07:10:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 3213,
                'route_day_id' => 582,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 3214,
                'route_day_id' => 583,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 3215,
                'route_day_id' => 584,
                'departure_time' => '08:50:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 3216,
                'route_day_id' => 585,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 3217,
                'route_day_id' => 586,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 3218,
                'route_day_id' => 587,
                'departure_time' => '13:50:00',
                'arrival_time' => '14:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 3219,
                'route_day_id' => 588,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 3220,
                'route_day_id' => 589,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 3221,
                'route_day_id' => 590,
                'departure_time' => '14:45:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 3222,
                'route_day_id' => 591,
                'departure_time' => '15:50:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 3223,
                'route_day_id' => 592,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 3224,
                'route_day_id' => 593,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 3225,
                'route_day_id' => 594,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 3227,
                'route_day_id' => 596,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 3230,
                'route_day_id' => 599,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 3232,
                'route_day_id' => 601,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 3233,
                'route_day_id' => 602,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 3234,
                'route_day_id' => 603,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 3235,
                'route_day_id' => 604,
                'departure_time' => '05:30:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 3236,
                'route_day_id' => 605,
                'departure_time' => '07:15:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 3237,
                'route_day_id' => 606,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_times')->insert(array (
            0 => 
            array (
                'id' => 3238,
                'route_day_id' => 607,
                'departure_time' => '15:05:00',
                'arrival_time' => '16:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 3239,
                'route_day_id' => 608,
                'departure_time' => '05:30:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3240,
                'route_day_id' => 609,
                'departure_time' => '07:15:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 3241,
                'route_day_id' => 610,
                'departure_time' => '08:50:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 3242,
                'route_day_id' => 611,
                'departure_time' => '13:30:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 3243,
                'route_day_id' => 612,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 3244,
                'route_day_id' => 613,
                'departure_time' => '14:50:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 3245,
                'route_day_id' => 614,
                'departure_time' => '05:30:00',
                'arrival_time' => '06:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 3246,
                'route_day_id' => 615,
                'departure_time' => '06:15:00',
                'arrival_time' => '06:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 3247,
                'route_day_id' => 616,
                'departure_time' => '06:40:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 3248,
                'route_day_id' => 617,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 3249,
                'route_day_id' => 618,
                'departure_time' => '07:40:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 3250,
                'route_day_id' => 619,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 3251,
                'route_day_id' => 620,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 3252,
                'route_day_id' => 621,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 3253,
                'route_day_id' => 622,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 3254,
                'route_day_id' => 623,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 3255,
                'route_day_id' => 624,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 3256,
                'route_day_id' => 625,
                'departure_time' => '14:50:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 3257,
                'route_day_id' => 626,
                'departure_time' => '15:35:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 3258,
                'route_day_id' => 627,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 3261,
                'route_day_id' => 628,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 3262,
                'route_day_id' => 628,
                'departure_time' => '06:50:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 3263,
                'route_day_id' => 628,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 3264,
                'route_day_id' => 628,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 3265,
                'route_day_id' => 628,
                'departure_time' => '07:50:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 3266,
                'route_day_id' => 628,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 3267,
                'route_day_id' => 628,
                'departure_time' => '08:50:00',
                'arrival_time' => '09:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 3268,
                'route_day_id' => 628,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 3269,
                'route_day_id' => 628,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 3270,
                'route_day_id' => 628,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 3271,
                'route_day_id' => 628,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 3272,
                'route_day_id' => 628,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 3273,
                'route_day_id' => 628,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 3274,
                'route_day_id' => 628,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 3275,
                'route_day_id' => 628,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 3276,
                'route_day_id' => 628,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 3277,
                'route_day_id' => 628,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 3278,
                'route_day_id' => 628,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 3279,
                'route_day_id' => 628,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 3280,
                'route_day_id' => 628,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 3281,
                'route_day_id' => 628,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 3282,
                'route_day_id' => 628,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 3283,
                'route_day_id' => 628,
                'departure_time' => '23:30:00',
                'arrival_time' => '23:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 3286,
                'route_day_id' => 629,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 3287,
                'route_day_id' => 629,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 3288,
                'route_day_id' => 629,
                'departure_time' => '06:40:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 3289,
                'route_day_id' => 629,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 3290,
                'route_day_id' => 629,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 3291,
                'route_day_id' => 629,
                'departure_time' => '07:40:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 3293,
                'route_day_id' => 629,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 3294,
                'route_day_id' => 629,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 3296,
                'route_day_id' => 629,
                'departure_time' => '08:40:00',
                'arrival_time' => '09:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 3297,
                'route_day_id' => 629,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 3300,
                'route_day_id' => 629,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 3303,
                'route_day_id' => 629,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 3306,
                'route_day_id' => 629,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 3309,
                'route_day_id' => 629,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 3312,
                'route_day_id' => 629,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 3315,
                'route_day_id' => 629,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 3318,
                'route_day_id' => 629,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 3319,
                'route_day_id' => 629,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 3321,
                'route_day_id' => 629,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 3322,
                'route_day_id' => 629,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 3323,
                'route_day_id' => 629,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 3324,
                'route_day_id' => 629,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 3325,
                'route_day_id' => 629,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 3326,
                'route_day_id' => 629,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 3369,
                'route_day_id' => 630,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 3370,
                'route_day_id' => 631,
                'departure_time' => '05:55:00',
                'arrival_time' => '06:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 3371,
                'route_day_id' => 632,
                'departure_time' => '06:40:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 3372,
                'route_day_id' => 633,
                'departure_time' => '06:55:00',
                'arrival_time' => '09:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 3373,
                'route_day_id' => 634,
                'departure_time' => '13:30:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 3374,
                'route_day_id' => 635,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 3375,
                'route_day_id' => 636,
                'departure_time' => '16:30:00',
                'arrival_time' => '17:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 3376,
                'route_day_id' => 637,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 3377,
                'route_day_id' => 638,
                'departure_time' => '05:00:00',
                'arrival_time' => '05:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 3378,
                'route_day_id' => 639,
                'departure_time' => '05:45:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 3379,
                'route_day_id' => 640,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 3380,
                'route_day_id' => 641,
                'departure_time' => '07:40:00',
                'arrival_time' => '10:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 3381,
                'route_day_id' => 642,
                'departure_time' => '13:30:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 3382,
                'route_day_id' => 643,
                'departure_time' => '15:55:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 3383,
                'route_day_id' => 644,
                'departure_time' => '16:30:00',
                'arrival_time' => '17:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 3384,
                'route_day_id' => 645,
                'departure_time' => '17:50:00',
                'arrival_time' => '18:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 3385,
                'route_day_id' => 646,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 3386,
                'route_day_id' => 647,
                'departure_time' => '05:55:00',
                'arrival_time' => '06:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 3387,
                'route_day_id' => 648,
                'departure_time' => '06:30:00',
                'arrival_time' => '07:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 3388,
                'route_day_id' => 649,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 3389,
                'route_day_id' => 650,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 3390,
                'route_day_id' => 651,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 3391,
                'route_day_id' => 652,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 3392,
                'route_day_id' => 653,
                'departure_time' => '13:55:00',
                'arrival_time' => '14:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 3393,
                'route_day_id' => 654,
                'departure_time' => '14:05:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 3394,
                'route_day_id' => 655,
                'departure_time' => '14:20:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 3395,
                'route_day_id' => 656,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 3396,
                'route_day_id' => 657,
                'departure_time' => '15:55:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 3397,
                'route_day_id' => 658,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 3398,
                'route_day_id' => 658,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 3399,
                'route_day_id' => 658,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 3400,
                'route_day_id' => 658,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 3401,
                'route_day_id' => 658,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 3402,
                'route_day_id' => 658,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 3403,
                'route_day_id' => 659,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 3404,
                'route_day_id' => 659,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 3405,
                'route_day_id' => 659,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 3406,
                'route_day_id' => 659,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 3407,
                'route_day_id' => 659,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 3408,
                'route_day_id' => 659,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 3409,
                'route_day_id' => 660,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 4828,
                'route_day_id' => 1763,
                'departure_time' => '00:05:00',
                'arrival_time' => '00:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 4829,
                'route_day_id' => 1764,
                'departure_time' => '00:06:00',
                'arrival_time' => '00:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 4830,
                'route_day_id' => 1765,
                'departure_time' => '00:07:00',
                'arrival_time' => '00:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 4831,
                'route_day_id' => 1766,
                'departure_time' => '00:08:00',
                'arrival_time' => '00:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 4832,
                'route_day_id' => 1767,
                'departure_time' => '00:09:00',
                'arrival_time' => '00:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 4833,
                'route_day_id' => 1768,
                'departure_time' => '00:10:00',
                'arrival_time' => '00:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 4834,
                'route_day_id' => 1769,
                'departure_time' => '00:11:00',
                'arrival_time' => '00:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 4835,
                'route_day_id' => 1770,
                'departure_time' => '00:12:00',
                'arrival_time' => '00:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 4836,
                'route_day_id' => 1771,
                'departure_time' => '00:13:00',
                'arrival_time' => '00:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 4837,
                'route_day_id' => 1772,
                'departure_time' => '00:14:00',
                'arrival_time' => '00:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 4838,
                'route_day_id' => 1773,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 4839,
                'route_day_id' => 1774,
                'departure_time' => '00:25:00',
                'arrival_time' => '00:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 4840,
                'route_day_id' => 1775,
                'departure_time' => '00:26:00',
                'arrival_time' => '00:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 4841,
                'route_day_id' => 1776,
                'departure_time' => '00:27:00',
                'arrival_time' => '00:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 4842,
                'route_day_id' => 1777,
                'departure_time' => '00:28:00',
                'arrival_time' => '00:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 4843,
                'route_day_id' => 1778,
                'departure_time' => '00:29:00',
                'arrival_time' => '00:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 4844,
                'route_day_id' => 1779,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 4845,
                'route_day_id' => 1780,
                'departure_time' => '00:31:00',
                'arrival_time' => '00:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 4846,
                'route_day_id' => 1781,
                'departure_time' => '00:32:00',
                'arrival_time' => '00:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 4847,
                'route_day_id' => 1782,
                'departure_time' => '00:33:00',
                'arrival_time' => '00:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 4848,
                'route_day_id' => 1783,
                'departure_time' => '00:34:00',
                'arrival_time' => '00:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 4849,
                'route_day_id' => 1784,
                'departure_time' => '00:35:00',
                'arrival_time' => '00:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 4850,
                'route_day_id' => 1785,
                'departure_time' => '00:55:00',
                'arrival_time' => '00:56:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 4851,
                'route_day_id' => 1786,
                'departure_time' => '00:56:00',
                'arrival_time' => '00:57:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 4852,
                'route_day_id' => 1787,
                'departure_time' => '00:57:00',
                'arrival_time' => '00:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 4853,
                'route_day_id' => 1788,
                'departure_time' => '00:58:00',
                'arrival_time' => '00:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 4854,
                'route_day_id' => 1789,
                'departure_time' => '00:59:00',
                'arrival_time' => '01:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 4855,
                'route_day_id' => 1790,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 4856,
                'route_day_id' => 1791,
                'departure_time' => '01:01:00',
                'arrival_time' => '01:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 4857,
                'route_day_id' => 1792,
                'departure_time' => '01:02:00',
                'arrival_time' => '01:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 4858,
                'route_day_id' => 1793,
                'departure_time' => '01:03:00',
                'arrival_time' => '01:04:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 4859,
                'route_day_id' => 1794,
                'departure_time' => '01:04:00',
                'arrival_time' => '01:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 4860,
                'route_day_id' => 1795,
                'departure_time' => '01:05:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 4861,
                'route_day_id' => 1796,
                'departure_time' => '05:35:00',
                'arrival_time' => '05:36:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 4862,
                'route_day_id' => 1797,
                'departure_time' => '05:36:00',
                'arrival_time' => '05:37:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 4863,
                'route_day_id' => 1798,
                'departure_time' => '05:37:00',
                'arrival_time' => '05:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 4864,
                'route_day_id' => 1799,
                'departure_time' => '05:38:00',
                'arrival_time' => '05:39:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 4865,
                'route_day_id' => 1800,
                'departure_time' => '05:39:00',
                'arrival_time' => '05:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 4866,
                'route_day_id' => 1801,
                'departure_time' => '05:40:00',
                'arrival_time' => '05:41:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 4867,
                'route_day_id' => 1802,
                'departure_time' => '05:41:00',
                'arrival_time' => '05:42:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 4868,
                'route_day_id' => 1803,
                'departure_time' => '05:42:00',
                'arrival_time' => '05:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 4869,
                'route_day_id' => 1804,
                'departure_time' => '05:43:00',
                'arrival_time' => '05:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 4870,
                'route_day_id' => 1805,
                'departure_time' => '05:44:00',
                'arrival_time' => '05:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 4871,
                'route_day_id' => 1806,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 4872,
                'route_day_id' => 1807,
                'departure_time' => '05:55:00',
                'arrival_time' => '05:56:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 4873,
                'route_day_id' => 1808,
                'departure_time' => '05:56:00',
                'arrival_time' => '05:57:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 4874,
                'route_day_id' => 1809,
                'departure_time' => '05:57:00',
                'arrival_time' => '05:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 4875,
                'route_day_id' => 1810,
                'departure_time' => '05:58:00',
                'arrival_time' => '05:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 4876,
                'route_day_id' => 1811,
                'departure_time' => '05:59:00',
                'arrival_time' => '06:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 4877,
                'route_day_id' => 1812,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 4878,
                'route_day_id' => 1813,
                'departure_time' => '06:01:00',
                'arrival_time' => '06:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 4879,
                'route_day_id' => 1814,
                'departure_time' => '06:02:00',
                'arrival_time' => '06:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 4880,
                'route_day_id' => 1815,
                'departure_time' => '06:03:00',
                'arrival_time' => '06:04:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 4881,
                'route_day_id' => 1816,
                'departure_time' => '06:04:00',
                'arrival_time' => '06:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 4882,
                'route_day_id' => 1817,
                'departure_time' => '06:05:00',
                'arrival_time' => '06:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 4883,
                'route_day_id' => 1818,
                'departure_time' => '06:25:00',
                'arrival_time' => '06:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 4884,
                'route_day_id' => 1819,
                'departure_time' => '06:26:00',
                'arrival_time' => '06:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 4885,
                'route_day_id' => 1820,
                'departure_time' => '06:27:00',
                'arrival_time' => '06:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 4886,
                'route_day_id' => 1821,
                'departure_time' => '06:28:00',
                'arrival_time' => '06:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 4887,
                'route_day_id' => 1822,
                'departure_time' => '06:29:00',
                'arrival_time' => '06:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 4888,
                'route_day_id' => 1823,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 4889,
                'route_day_id' => 1824,
                'departure_time' => '06:31:00',
                'arrival_time' => '06:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 4890,
                'route_day_id' => 1825,
                'departure_time' => '06:32:00',
                'arrival_time' => '06:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 4891,
                'route_day_id' => 1826,
                'departure_time' => '06:33:00',
                'arrival_time' => '06:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 4892,
                'route_day_id' => 1827,
                'departure_time' => '06:34:00',
                'arrival_time' => '06:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 4893,
                'route_day_id' => 1828,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 4894,
                'route_day_id' => 1829,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 4895,
                'route_day_id' => 1830,
                'departure_time' => '06:46:00',
                'arrival_time' => '06:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 4896,
                'route_day_id' => 1831,
                'departure_time' => '06:47:00',
                'arrival_time' => '06:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 4897,
                'route_day_id' => 1832,
                'departure_time' => '06:48:00',
                'arrival_time' => '06:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 4898,
                'route_day_id' => 1833,
                'departure_time' => '06:49:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 4899,
                'route_day_id' => 1834,
                'departure_time' => '06:50:00',
                'arrival_time' => '06:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 4900,
                'route_day_id' => 1835,
                'departure_time' => '06:51:00',
                'arrival_time' => '06:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 4901,
                'route_day_id' => 1836,
                'departure_time' => '06:52:00',
                'arrival_time' => '06:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 4902,
                'route_day_id' => 1837,
                'departure_time' => '06:53:00',
                'arrival_time' => '06:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 4903,
                'route_day_id' => 1838,
                'departure_time' => '06:54:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 4904,
                'route_day_id' => 1839,
                'departure_time' => '06:55:00',
                'arrival_time' => '06:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 4905,
                'route_day_id' => 1840,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 4906,
                'route_day_id' => 1841,
                'departure_time' => '07:06:00',
                'arrival_time' => '07:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 4907,
                'route_day_id' => 1842,
                'departure_time' => '07:07:00',
                'arrival_time' => '07:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 4908,
                'route_day_id' => 1843,
                'departure_time' => '07:08:00',
                'arrival_time' => '07:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 4909,
                'route_day_id' => 1844,
                'departure_time' => '07:09:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 4910,
                'route_day_id' => 1845,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 4911,
                'route_day_id' => 1846,
                'departure_time' => '07:11:00',
                'arrival_time' => '07:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 4912,
                'route_day_id' => 1847,
                'departure_time' => '07:12:00',
                'arrival_time' => '07:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 4913,
                'route_day_id' => 1848,
                'departure_time' => '07:13:00',
                'arrival_time' => '07:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 4914,
                'route_day_id' => 1849,
                'departure_time' => '07:14:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 4915,
                'route_day_id' => 1850,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 4916,
                'route_day_id' => 1851,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 4917,
                'route_day_id' => 1852,
                'departure_time' => '07:26:00',
                'arrival_time' => '07:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 4918,
                'route_day_id' => 1853,
                'departure_time' => '07:27:00',
                'arrival_time' => '07:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 4919,
                'route_day_id' => 1854,
                'departure_time' => '07:28:00',
                'arrival_time' => '07:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 4920,
                'route_day_id' => 1855,
                'departure_time' => '07:29:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 4921,
                'route_day_id' => 1856,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 4922,
                'route_day_id' => 1857,
                'departure_time' => '07:31:00',
                'arrival_time' => '07:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 4923,
                'route_day_id' => 1858,
                'departure_time' => '07:32:00',
                'arrival_time' => '07:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 4924,
                'route_day_id' => 1859,
                'departure_time' => '07:33:00',
                'arrival_time' => '07:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 4925,
                'route_day_id' => 1860,
                'departure_time' => '07:34:00',
                'arrival_time' => '07:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 4926,
                'route_day_id' => 1861,
                'departure_time' => '07:35:00',
                'arrival_time' => '07:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 4927,
                'route_day_id' => 1862,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 4928,
                'route_day_id' => 1863,
                'departure_time' => '07:46:00',
                'arrival_time' => '07:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 4929,
                'route_day_id' => 1864,
                'departure_time' => '07:47:00',
                'arrival_time' => '07:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 4930,
                'route_day_id' => 1865,
                'departure_time' => '07:48:00',
                'arrival_time' => '07:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 4931,
                'route_day_id' => 1866,
                'departure_time' => '07:49:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 4932,
                'route_day_id' => 1867,
                'departure_time' => '07:50:00',
                'arrival_time' => '07:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 4933,
                'route_day_id' => 1868,
                'departure_time' => '07:51:00',
                'arrival_time' => '07:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 4934,
                'route_day_id' => 1869,
                'departure_time' => '07:52:00',
                'arrival_time' => '07:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 4935,
                'route_day_id' => 1870,
                'departure_time' => '07:53:00',
                'arrival_time' => '07:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 4936,
                'route_day_id' => 1871,
                'departure_time' => '07:54:00',
                'arrival_time' => '07:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 4937,
                'route_day_id' => 1872,
                'departure_time' => '07:55:00',
                'arrival_time' => '07:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 4938,
                'route_day_id' => 1873,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 4939,
                'route_day_id' => 1874,
                'departure_time' => '08:06:00',
                'arrival_time' => '08:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 4940,
                'route_day_id' => 1875,
                'departure_time' => '08:07:00',
                'arrival_time' => '08:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 4941,
                'route_day_id' => 1876,
                'departure_time' => '08:08:00',
                'arrival_time' => '08:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 4942,
                'route_day_id' => 1877,
                'departure_time' => '08:09:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 4943,
                'route_day_id' => 1878,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 4944,
                'route_day_id' => 1879,
                'departure_time' => '08:11:00',
                'arrival_time' => '08:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 4945,
                'route_day_id' => 1880,
                'departure_time' => '08:12:00',
                'arrival_time' => '08:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 4946,
                'route_day_id' => 1881,
                'departure_time' => '08:13:00',
                'arrival_time' => '08:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 4947,
                'route_day_id' => 1882,
                'departure_time' => '08:14:00',
                'arrival_time' => '08:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 4948,
                'route_day_id' => 1883,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 4949,
                'route_day_id' => 1884,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 4950,
                'route_day_id' => 1885,
                'departure_time' => '08:26:00',
                'arrival_time' => '08:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 4951,
                'route_day_id' => 1886,
                'departure_time' => '08:27:00',
                'arrival_time' => '08:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 4952,
                'route_day_id' => 1887,
                'departure_time' => '08:28:00',
                'arrival_time' => '08:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 4953,
                'route_day_id' => 1888,
                'departure_time' => '08:29:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 4954,
                'route_day_id' => 1889,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 4955,
                'route_day_id' => 1890,
                'departure_time' => '08:31:00',
                'arrival_time' => '08:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 4956,
                'route_day_id' => 1891,
                'departure_time' => '08:32:00',
                'arrival_time' => '08:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 4957,
                'route_day_id' => 1892,
                'departure_time' => '08:33:00',
                'arrival_time' => '08:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 4958,
                'route_day_id' => 1893,
                'departure_time' => '08:34:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 4959,
                'route_day_id' => 1894,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 4960,
                'route_day_id' => 1895,
                'departure_time' => '08:45:00',
                'arrival_time' => '08:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 4961,
                'route_day_id' => 1896,
                'departure_time' => '08:46:00',
                'arrival_time' => '08:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 4962,
                'route_day_id' => 1897,
                'departure_time' => '08:47:00',
                'arrival_time' => '08:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 4963,
                'route_day_id' => 1898,
                'departure_time' => '08:48:00',
                'arrival_time' => '08:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 4964,
                'route_day_id' => 1899,
                'departure_time' => '08:49:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 4965,
                'route_day_id' => 1900,
                'departure_time' => '08:50:00',
                'arrival_time' => '08:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 4966,
                'route_day_id' => 1901,
                'departure_time' => '08:51:00',
                'arrival_time' => '08:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 4967,
                'route_day_id' => 1902,
                'departure_time' => '08:52:00',
                'arrival_time' => '08:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 4968,
                'route_day_id' => 1903,
                'departure_time' => '08:53:00',
                'arrival_time' => '08:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 4969,
                'route_day_id' => 1904,
                'departure_time' => '08:54:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 4970,
                'route_day_id' => 1905,
                'departure_time' => '08:55:00',
                'arrival_time' => '08:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 4971,
                'route_day_id' => 1906,
                'departure_time' => '09:05:00',
                'arrival_time' => '09:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 4972,
                'route_day_id' => 1907,
                'departure_time' => '09:06:00',
                'arrival_time' => '09:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 4973,
                'route_day_id' => 1908,
                'departure_time' => '09:07:00',
                'arrival_time' => '09:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 4974,
                'route_day_id' => 1909,
                'departure_time' => '09:08:00',
                'arrival_time' => '09:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 4975,
                'route_day_id' => 1910,
                'departure_time' => '09:09:00',
                'arrival_time' => '09:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 4976,
                'route_day_id' => 1911,
                'departure_time' => '09:10:00',
                'arrival_time' => '09:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 4977,
                'route_day_id' => 1912,
                'departure_time' => '09:11:00',
                'arrival_time' => '09:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 4978,
                'route_day_id' => 1913,
                'departure_time' => '09:12:00',
                'arrival_time' => '09:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 4979,
                'route_day_id' => 1914,
                'departure_time' => '09:13:00',
                'arrival_time' => '09:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 4980,
                'route_day_id' => 1915,
                'departure_time' => '09:14:00',
                'arrival_time' => '09:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 4981,
                'route_day_id' => 1916,
                'departure_time' => '09:15:00',
                'arrival_time' => '09:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 4982,
                'route_day_id' => 1917,
                'departure_time' => '09:25:00',
                'arrival_time' => '09:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 4983,
                'route_day_id' => 1918,
                'departure_time' => '09:26:00',
                'arrival_time' => '09:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 4984,
                'route_day_id' => 1919,
                'departure_time' => '09:27:00',
                'arrival_time' => '09:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 4985,
                'route_day_id' => 1920,
                'departure_time' => '09:28:00',
                'arrival_time' => '09:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 4986,
                'route_day_id' => 1921,
                'departure_time' => '09:29:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 4987,
                'route_day_id' => 1922,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 4988,
                'route_day_id' => 1923,
                'departure_time' => '09:31:00',
                'arrival_time' => '09:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 4989,
                'route_day_id' => 1924,
                'departure_time' => '09:32:00',
                'arrival_time' => '09:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 4990,
                'route_day_id' => 1925,
                'departure_time' => '09:33:00',
                'arrival_time' => '09:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 4991,
                'route_day_id' => 1926,
                'departure_time' => '09:34:00',
                'arrival_time' => '09:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 4992,
                'route_day_id' => 1927,
                'departure_time' => '09:35:00',
                'arrival_time' => '09:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 4993,
                'route_day_id' => 1928,
                'departure_time' => '09:45:00',
                'arrival_time' => '09:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 4994,
                'route_day_id' => 1929,
                'departure_time' => '09:46:00',
                'arrival_time' => '09:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 4995,
                'route_day_id' => 1930,
                'departure_time' => '09:47:00',
                'arrival_time' => '09:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 4996,
                'route_day_id' => 1931,
                'departure_time' => '09:48:00',
                'arrival_time' => '09:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 4997,
                'route_day_id' => 1932,
                'departure_time' => '09:49:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 4998,
                'route_day_id' => 1933,
                'departure_time' => '09:50:00',
                'arrival_time' => '09:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 4999,
                'route_day_id' => 1934,
                'departure_time' => '09:51:00',
                'arrival_time' => '09:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 5000,
                'route_day_id' => 1935,
                'departure_time' => '09:52:00',
                'arrival_time' => '09:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 5001,
                'route_day_id' => 1936,
                'departure_time' => '09:53:00',
                'arrival_time' => '09:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 5002,
                'route_day_id' => 1937,
                'departure_time' => '09:54:00',
                'arrival_time' => '09:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 5003,
                'route_day_id' => 1938,
                'departure_time' => '09:55:00',
                'arrival_time' => '09:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 5004,
                'route_day_id' => 1939,
                'departure_time' => '10:05:00',
                'arrival_time' => '10:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 5005,
                'route_day_id' => 1940,
                'departure_time' => '10:06:00',
                'arrival_time' => '10:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 5006,
                'route_day_id' => 1941,
                'departure_time' => '10:07:00',
                'arrival_time' => '10:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 5007,
                'route_day_id' => 1942,
                'departure_time' => '10:08:00',
                'arrival_time' => '10:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 5008,
                'route_day_id' => 1943,
                'departure_time' => '10:09:00',
                'arrival_time' => '10:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 5009,
                'route_day_id' => 1944,
                'departure_time' => '10:10:00',
                'arrival_time' => '10:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 5010,
                'route_day_id' => 1945,
                'departure_time' => '10:11:00',
                'arrival_time' => '10:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 5011,
                'route_day_id' => 1946,
                'departure_time' => '10:12:00',
                'arrival_time' => '10:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 5012,
                'route_day_id' => 1947,
                'departure_time' => '10:13:00',
                'arrival_time' => '10:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 5013,
                'route_day_id' => 1948,
                'departure_time' => '10:14:00',
                'arrival_time' => '10:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 5014,
                'route_day_id' => 1949,
                'departure_time' => '10:15:00',
                'arrival_time' => '10:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 5015,
                'route_day_id' => 1950,
                'departure_time' => '10:25:00',
                'arrival_time' => '10:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 5016,
                'route_day_id' => 1951,
                'departure_time' => '10:26:00',
                'arrival_time' => '10:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 5017,
                'route_day_id' => 1952,
                'departure_time' => '10:27:00',
                'arrival_time' => '10:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 5018,
                'route_day_id' => 1953,
                'departure_time' => '10:28:00',
                'arrival_time' => '10:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 5019,
                'route_day_id' => 1954,
                'departure_time' => '10:29:00',
                'arrival_time' => '10:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 5020,
                'route_day_id' => 1955,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 5021,
                'route_day_id' => 1956,
                'departure_time' => '10:31:00',
                'arrival_time' => '10:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 5022,
                'route_day_id' => 1957,
                'departure_time' => '10:32:00',
                'arrival_time' => '10:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 5023,
                'route_day_id' => 1958,
                'departure_time' => '10:33:00',
                'arrival_time' => '10:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 5024,
                'route_day_id' => 1959,
                'departure_time' => '10:34:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 5025,
                'route_day_id' => 1960,
                'departure_time' => '10:35:00',
                'arrival_time' => '10:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 5026,
                'route_day_id' => 1961,
                'departure_time' => '10:45:00',
                'arrival_time' => '10:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 5027,
                'route_day_id' => 1962,
                'departure_time' => '10:46:00',
                'arrival_time' => '10:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 5028,
                'route_day_id' => 1963,
                'departure_time' => '10:47:00',
                'arrival_time' => '10:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 5029,
                'route_day_id' => 1964,
                'departure_time' => '10:48:00',
                'arrival_time' => '10:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 5030,
                'route_day_id' => 1965,
                'departure_time' => '10:49:00',
                'arrival_time' => '10:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 5031,
                'route_day_id' => 1966,
                'departure_time' => '10:50:00',
                'arrival_time' => '10:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 5032,
                'route_day_id' => 1967,
                'departure_time' => '10:51:00',
                'arrival_time' => '10:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 5033,
                'route_day_id' => 1968,
                'departure_time' => '10:52:00',
                'arrival_time' => '10:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 5034,
                'route_day_id' => 1969,
                'departure_time' => '10:53:00',
                'arrival_time' => '10:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 5035,
                'route_day_id' => 1970,
                'departure_time' => '10:54:00',
                'arrival_time' => '10:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 5036,
                'route_day_id' => 1971,
                'departure_time' => '10:55:00',
                'arrival_time' => '10:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 5037,
                'route_day_id' => 1972,
                'departure_time' => '11:05:00',
                'arrival_time' => '11:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 5038,
                'route_day_id' => 1973,
                'departure_time' => '11:06:00',
                'arrival_time' => '11:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 5039,
                'route_day_id' => 1974,
                'departure_time' => '11:07:00',
                'arrival_time' => '11:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 5040,
                'route_day_id' => 1975,
                'departure_time' => '11:08:00',
                'arrival_time' => '11:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 5041,
                'route_day_id' => 1976,
                'departure_time' => '11:09:00',
                'arrival_time' => '11:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 5042,
                'route_day_id' => 1977,
                'departure_time' => '11:10:00',
                'arrival_time' => '11:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 5043,
                'route_day_id' => 1978,
                'departure_time' => '11:11:00',
                'arrival_time' => '11:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 5044,
                'route_day_id' => 1979,
                'departure_time' => '11:12:00',
                'arrival_time' => '11:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 5045,
                'route_day_id' => 1980,
                'departure_time' => '11:13:00',
                'arrival_time' => '11:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 5046,
                'route_day_id' => 1981,
                'departure_time' => '11:14:00',
                'arrival_time' => '11:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 5047,
                'route_day_id' => 1982,
                'departure_time' => '11:15:00',
                'arrival_time' => '11:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 5048,
                'route_day_id' => 1983,
                'departure_time' => '11:25:00',
                'arrival_time' => '11:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 5049,
                'route_day_id' => 1984,
                'departure_time' => '11:26:00',
                'arrival_time' => '11:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 5050,
                'route_day_id' => 1985,
                'departure_time' => '11:27:00',
                'arrival_time' => '11:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 5051,
                'route_day_id' => 1986,
                'departure_time' => '11:28:00',
                'arrival_time' => '11:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 5052,
                'route_day_id' => 1987,
                'departure_time' => '11:29:00',
                'arrival_time' => '11:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 5053,
                'route_day_id' => 1988,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 5054,
                'route_day_id' => 1989,
                'departure_time' => '11:31:00',
                'arrival_time' => '11:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 5055,
                'route_day_id' => 1990,
                'departure_time' => '11:32:00',
                'arrival_time' => '11:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 5056,
                'route_day_id' => 1991,
                'departure_time' => '11:33:00',
                'arrival_time' => '11:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 5057,
                'route_day_id' => 1992,
                'departure_time' => '11:34:00',
                'arrival_time' => '11:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 5058,
                'route_day_id' => 1993,
                'departure_time' => '11:35:00',
                'arrival_time' => '11:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 5059,
                'route_day_id' => 1994,
                'departure_time' => '11:45:00',
                'arrival_time' => '11:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 5060,
                'route_day_id' => 1995,
                'departure_time' => '11:46:00',
                'arrival_time' => '11:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 5061,
                'route_day_id' => 1996,
                'departure_time' => '11:47:00',
                'arrival_time' => '11:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 5062,
                'route_day_id' => 1997,
                'departure_time' => '11:48:00',
                'arrival_time' => '11:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 5063,
                'route_day_id' => 1998,
                'departure_time' => '11:49:00',
                'arrival_time' => '11:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 5064,
                'route_day_id' => 1999,
                'departure_time' => '11:50:00',
                'arrival_time' => '11:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 5065,
                'route_day_id' => 2000,
                'departure_time' => '11:51:00',
                'arrival_time' => '11:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 5066,
                'route_day_id' => 2001,
                'departure_time' => '11:52:00',
                'arrival_time' => '11:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 5067,
                'route_day_id' => 2002,
                'departure_time' => '11:53:00',
                'arrival_time' => '11:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 5068,
                'route_day_id' => 2003,
                'departure_time' => '11:54:00',
                'arrival_time' => '11:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 5069,
                'route_day_id' => 2004,
                'departure_time' => '11:55:00',
                'arrival_time' => '11:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 5070,
                'route_day_id' => 2005,
                'departure_time' => '12:05:00',
                'arrival_time' => '12:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 5071,
                'route_day_id' => 2006,
                'departure_time' => '12:06:00',
                'arrival_time' => '12:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 5072,
                'route_day_id' => 2007,
                'departure_time' => '12:07:00',
                'arrival_time' => '12:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 5073,
                'route_day_id' => 2008,
                'departure_time' => '12:08:00',
                'arrival_time' => '12:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 5074,
                'route_day_id' => 2009,
                'departure_time' => '12:09:00',
                'arrival_time' => '12:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 5075,
                'route_day_id' => 2010,
                'departure_time' => '12:10:00',
                'arrival_time' => '12:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 5076,
                'route_day_id' => 2011,
                'departure_time' => '12:11:00',
                'arrival_time' => '12:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 5077,
                'route_day_id' => 2012,
                'departure_time' => '12:12:00',
                'arrival_time' => '12:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 5078,
                'route_day_id' => 2013,
                'departure_time' => '12:13:00',
                'arrival_time' => '12:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 5079,
                'route_day_id' => 2014,
                'departure_time' => '12:14:00',
                'arrival_time' => '12:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 5080,
                'route_day_id' => 2015,
                'departure_time' => '12:15:00',
                'arrival_time' => '12:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 5081,
                'route_day_id' => 2016,
                'departure_time' => '12:25:00',
                'arrival_time' => '12:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 5082,
                'route_day_id' => 2017,
                'departure_time' => '12:26:00',
                'arrival_time' => '12:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 5083,
                'route_day_id' => 2018,
                'departure_time' => '12:27:00',
                'arrival_time' => '12:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 5084,
                'route_day_id' => 2019,
                'departure_time' => '12:28:00',
                'arrival_time' => '12:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 5085,
                'route_day_id' => 2020,
                'departure_time' => '12:29:00',
                'arrival_time' => '12:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 5086,
                'route_day_id' => 2021,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 5087,
                'route_day_id' => 2022,
                'departure_time' => '12:31:00',
                'arrival_time' => '12:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 5088,
                'route_day_id' => 2023,
                'departure_time' => '12:32:00',
                'arrival_time' => '12:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 5089,
                'route_day_id' => 2024,
                'departure_time' => '12:33:00',
                'arrival_time' => '12:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 5090,
                'route_day_id' => 2025,
                'departure_time' => '12:34:00',
                'arrival_time' => '12:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 5091,
                'route_day_id' => 2026,
                'departure_time' => '12:35:00',
                'arrival_time' => '12:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 5092,
                'route_day_id' => 2027,
                'departure_time' => '12:45:00',
                'arrival_time' => '12:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 5093,
                'route_day_id' => 2028,
                'departure_time' => '12:46:00',
                'arrival_time' => '12:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 5094,
                'route_day_id' => 2029,
                'departure_time' => '12:47:00',
                'arrival_time' => '12:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 5095,
                'route_day_id' => 2030,
                'departure_time' => '12:48:00',
                'arrival_time' => '12:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 5096,
                'route_day_id' => 2031,
                'departure_time' => '12:49:00',
                'arrival_time' => '12:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 5097,
                'route_day_id' => 2032,
                'departure_time' => '12:50:00',
                'arrival_time' => '12:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 5098,
                'route_day_id' => 2033,
                'departure_time' => '12:51:00',
                'arrival_time' => '12:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 5099,
                'route_day_id' => 2034,
                'departure_time' => '12:52:00',
                'arrival_time' => '12:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 5100,
                'route_day_id' => 2035,
                'departure_time' => '12:53:00',
                'arrival_time' => '12:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 5101,
                'route_day_id' => 2036,
                'departure_time' => '12:54:00',
                'arrival_time' => '12:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 5102,
                'route_day_id' => 2037,
                'departure_time' => '12:55:00',
                'arrival_time' => '12:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 5103,
                'route_day_id' => 2038,
                'departure_time' => '13:05:00',
                'arrival_time' => '13:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 5104,
                'route_day_id' => 2039,
                'departure_time' => '13:06:00',
                'arrival_time' => '13:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 5105,
                'route_day_id' => 2040,
                'departure_time' => '13:07:00',
                'arrival_time' => '13:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 5106,
                'route_day_id' => 2041,
                'departure_time' => '13:08:00',
                'arrival_time' => '13:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 5107,
                'route_day_id' => 2042,
                'departure_time' => '13:09:00',
                'arrival_time' => '13:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 5108,
                'route_day_id' => 2043,
                'departure_time' => '13:10:00',
                'arrival_time' => '13:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 5109,
                'route_day_id' => 2044,
                'departure_time' => '13:11:00',
                'arrival_time' => '13:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 5110,
                'route_day_id' => 2045,
                'departure_time' => '13:12:00',
                'arrival_time' => '13:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 5111,
                'route_day_id' => 2046,
                'departure_time' => '13:13:00',
                'arrival_time' => '13:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 5112,
                'route_day_id' => 2047,
                'departure_time' => '13:14:00',
                'arrival_time' => '13:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 5113,
                'route_day_id' => 2048,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 5114,
                'route_day_id' => 2049,
                'departure_time' => '13:25:00',
                'arrival_time' => '13:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 5115,
                'route_day_id' => 2050,
                'departure_time' => '13:26:00',
                'arrival_time' => '13:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 5116,
                'route_day_id' => 2051,
                'departure_time' => '13:27:00',
                'arrival_time' => '13:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 5117,
                'route_day_id' => 2052,
                'departure_time' => '13:28:00',
                'arrival_time' => '13:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 5118,
                'route_day_id' => 2053,
                'departure_time' => '13:29:00',
                'arrival_time' => '13:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 5119,
                'route_day_id' => 2054,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 5120,
                'route_day_id' => 2055,
                'departure_time' => '13:31:00',
                'arrival_time' => '13:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 5121,
                'route_day_id' => 2056,
                'departure_time' => '13:32:00',
                'arrival_time' => '13:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 5122,
                'route_day_id' => 2057,
                'departure_time' => '13:33:00',
                'arrival_time' => '13:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 5123,
                'route_day_id' => 2058,
                'departure_time' => '13:34:00',
                'arrival_time' => '13:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 5124,
                'route_day_id' => 2059,
                'departure_time' => '13:35:00',
                'arrival_time' => '13:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 5125,
                'route_day_id' => 2060,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 5126,
                'route_day_id' => 2061,
                'departure_time' => '13:46:00',
                'arrival_time' => '13:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 5127,
                'route_day_id' => 2062,
                'departure_time' => '13:47:00',
                'arrival_time' => '13:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 5128,
                'route_day_id' => 2063,
                'departure_time' => '13:48:00',
                'arrival_time' => '13:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 5129,
                'route_day_id' => 2064,
                'departure_time' => '13:49:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 5130,
                'route_day_id' => 2065,
                'departure_time' => '13:50:00',
                'arrival_time' => '13:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 5131,
                'route_day_id' => 2066,
                'departure_time' => '13:51:00',
                'arrival_time' => '13:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 5132,
                'route_day_id' => 2067,
                'departure_time' => '13:52:00',
                'arrival_time' => '13:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 5133,
                'route_day_id' => 2068,
                'departure_time' => '13:53:00',
                'arrival_time' => '13:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 5134,
                'route_day_id' => 2069,
                'departure_time' => '13:54:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 5135,
                'route_day_id' => 2070,
                'departure_time' => '13:55:00',
                'arrival_time' => '13:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 5136,
                'route_day_id' => 2071,
                'departure_time' => '14:05:00',
                'arrival_time' => '14:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 5137,
                'route_day_id' => 2072,
                'departure_time' => '14:06:00',
                'arrival_time' => '14:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 5138,
                'route_day_id' => 2073,
                'departure_time' => '14:07:00',
                'arrival_time' => '14:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 5139,
                'route_day_id' => 2074,
                'departure_time' => '14:08:00',
                'arrival_time' => '14:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 5140,
                'route_day_id' => 2075,
                'departure_time' => '14:09:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 5141,
                'route_day_id' => 2076,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 5142,
                'route_day_id' => 2077,
                'departure_time' => '14:11:00',
                'arrival_time' => '14:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 5143,
                'route_day_id' => 2078,
                'departure_time' => '14:12:00',
                'arrival_time' => '14:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 5144,
                'route_day_id' => 2079,
                'departure_time' => '14:13:00',
                'arrival_time' => '14:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 5145,
                'route_day_id' => 2080,
                'departure_time' => '14:14:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 5146,
                'route_day_id' => 2081,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 5147,
                'route_day_id' => 2082,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 5148,
                'route_day_id' => 2083,
                'departure_time' => '14:26:00',
                'arrival_time' => '14:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 5149,
                'route_day_id' => 2084,
                'departure_time' => '14:27:00',
                'arrival_time' => '14:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 5150,
                'route_day_id' => 2085,
                'departure_time' => '14:28:00',
                'arrival_time' => '14:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 5151,
                'route_day_id' => 2086,
                'departure_time' => '14:29:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 5152,
                'route_day_id' => 2087,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 5153,
                'route_day_id' => 2088,
                'departure_time' => '14:31:00',
                'arrival_time' => '14:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 5154,
                'route_day_id' => 2089,
                'departure_time' => '14:32:00',
                'arrival_time' => '14:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 5155,
                'route_day_id' => 2090,
                'departure_time' => '14:33:00',
                'arrival_time' => '14:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 5156,
                'route_day_id' => 2091,
                'departure_time' => '14:34:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 5157,
                'route_day_id' => 2092,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 5158,
                'route_day_id' => 2093,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 5159,
                'route_day_id' => 2094,
                'departure_time' => '14:46:00',
                'arrival_time' => '14:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 5160,
                'route_day_id' => 2095,
                'departure_time' => '14:47:00',
                'arrival_time' => '14:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 5161,
                'route_day_id' => 2096,
                'departure_time' => '14:48:00',
                'arrival_time' => '14:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 5162,
                'route_day_id' => 2097,
                'departure_time' => '14:49:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 5163,
                'route_day_id' => 2098,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 5164,
                'route_day_id' => 2099,
                'departure_time' => '14:51:00',
                'arrival_time' => '14:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 5165,
                'route_day_id' => 2100,
                'departure_time' => '14:52:00',
                'arrival_time' => '14:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 5166,
                'route_day_id' => 2101,
                'departure_time' => '14:53:00',
                'arrival_time' => '14:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 5167,
                'route_day_id' => 2102,
                'departure_time' => '14:54:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 5168,
                'route_day_id' => 2103,
                'departure_time' => '14:55:00',
                'arrival_time' => '14:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 5169,
                'route_day_id' => 2104,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 5170,
                'route_day_id' => 2105,
                'departure_time' => '15:06:00',
                'arrival_time' => '15:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 5171,
                'route_day_id' => 2106,
                'departure_time' => '15:07:00',
                'arrival_time' => '15:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 5172,
                'route_day_id' => 2107,
                'departure_time' => '15:08:00',
                'arrival_time' => '15:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 5173,
                'route_day_id' => 2108,
                'departure_time' => '15:09:00',
                'arrival_time' => '15:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 5174,
                'route_day_id' => 2109,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 5175,
                'route_day_id' => 2110,
                'departure_time' => '15:11:00',
                'arrival_time' => '15:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 5176,
                'route_day_id' => 2111,
                'departure_time' => '15:12:00',
                'arrival_time' => '15:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 5177,
                'route_day_id' => 2112,
                'departure_time' => '15:13:00',
                'arrival_time' => '15:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 5178,
                'route_day_id' => 2113,
                'departure_time' => '15:14:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 5179,
                'route_day_id' => 2114,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 5180,
                'route_day_id' => 2115,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 5181,
                'route_day_id' => 2116,
                'departure_time' => '15:26:00',
                'arrival_time' => '15:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 5182,
                'route_day_id' => 2117,
                'departure_time' => '15:27:00',
                'arrival_time' => '15:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 5183,
                'route_day_id' => 2118,
                'departure_time' => '15:28:00',
                'arrival_time' => '15:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 5184,
                'route_day_id' => 2119,
                'departure_time' => '15:29:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 5185,
                'route_day_id' => 2120,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 5186,
                'route_day_id' => 2121,
                'departure_time' => '15:31:00',
                'arrival_time' => '15:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 5187,
                'route_day_id' => 2122,
                'departure_time' => '15:32:00',
                'arrival_time' => '15:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 5188,
                'route_day_id' => 2123,
                'departure_time' => '15:33:00',
                'arrival_time' => '15:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 5189,
                'route_day_id' => 2124,
                'departure_time' => '15:34:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 5190,
                'route_day_id' => 2125,
                'departure_time' => '15:35:00',
                'arrival_time' => '15:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 5191,
                'route_day_id' => 2126,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 5192,
                'route_day_id' => 2127,
                'departure_time' => '15:46:00',
                'arrival_time' => '15:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 5193,
                'route_day_id' => 2128,
                'departure_time' => '15:47:00',
                'arrival_time' => '15:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 5194,
                'route_day_id' => 2129,
                'departure_time' => '15:48:00',
                'arrival_time' => '15:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 5195,
                'route_day_id' => 2130,
                'departure_time' => '15:49:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 5196,
                'route_day_id' => 2131,
                'departure_time' => '15:50:00',
                'arrival_time' => '15:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 5197,
                'route_day_id' => 2132,
                'departure_time' => '15:51:00',
                'arrival_time' => '15:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 5198,
                'route_day_id' => 2133,
                'departure_time' => '15:52:00',
                'arrival_time' => '15:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 5199,
                'route_day_id' => 2134,
                'departure_time' => '15:53:00',
                'arrival_time' => '15:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 5200,
                'route_day_id' => 2135,
                'departure_time' => '15:54:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 5201,
                'route_day_id' => 2136,
                'departure_time' => '15:55:00',
                'arrival_time' => '15:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 5202,
                'route_day_id' => 2137,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 5203,
                'route_day_id' => 2138,
                'departure_time' => '16:06:00',
                'arrival_time' => '16:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 5204,
                'route_day_id' => 2139,
                'departure_time' => '16:07:00',
                'arrival_time' => '16:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 5205,
                'route_day_id' => 2140,
                'departure_time' => '16:08:00',
                'arrival_time' => '16:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 5206,
                'route_day_id' => 2141,
                'departure_time' => '16:09:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 5207,
                'route_day_id' => 2142,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 5208,
                'route_day_id' => 2143,
                'departure_time' => '16:11:00',
                'arrival_time' => '16:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 5209,
                'route_day_id' => 2144,
                'departure_time' => '16:12:00',
                'arrival_time' => '16:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 5210,
                'route_day_id' => 2145,
                'departure_time' => '16:13:00',
                'arrival_time' => '16:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 5211,
                'route_day_id' => 2146,
                'departure_time' => '16:14:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 5212,
                'route_day_id' => 2147,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 5213,
                'route_day_id' => 2148,
                'departure_time' => '16:25:00',
                'arrival_time' => '16:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 5214,
                'route_day_id' => 2149,
                'departure_time' => '16:26:00',
                'arrival_time' => '16:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 5215,
                'route_day_id' => 2150,
                'departure_time' => '16:27:00',
                'arrival_time' => '16:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 5216,
                'route_day_id' => 2151,
                'departure_time' => '16:28:00',
                'arrival_time' => '16:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 5217,
                'route_day_id' => 2152,
                'departure_time' => '16:29:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 5218,
                'route_day_id' => 2153,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_times')->insert(array (
            0 => 
            array (
                'id' => 5219,
                'route_day_id' => 2154,
                'departure_time' => '16:31:00',
                'arrival_time' => '16:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5220,
                'route_day_id' => 2155,
                'departure_time' => '16:32:00',
                'arrival_time' => '16:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 5221,
                'route_day_id' => 2156,
                'departure_time' => '16:33:00',
                'arrival_time' => '16:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5222,
                'route_day_id' => 2157,
                'departure_time' => '16:34:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5223,
                'route_day_id' => 2158,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 5224,
                'route_day_id' => 2159,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 5225,
                'route_day_id' => 2160,
                'departure_time' => '16:46:00',
                'arrival_time' => '16:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 5226,
                'route_day_id' => 2161,
                'departure_time' => '16:47:00',
                'arrival_time' => '16:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 5227,
                'route_day_id' => 2162,
                'departure_time' => '16:48:00',
                'arrival_time' => '16:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 5228,
                'route_day_id' => 2163,
                'departure_time' => '16:49:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 5229,
                'route_day_id' => 2164,
                'departure_time' => '16:50:00',
                'arrival_time' => '16:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 5230,
                'route_day_id' => 2165,
                'departure_time' => '16:51:00',
                'arrival_time' => '16:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 5231,
                'route_day_id' => 2166,
                'departure_time' => '16:52:00',
                'arrival_time' => '16:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 5232,
                'route_day_id' => 2167,
                'departure_time' => '16:53:00',
                'arrival_time' => '16:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 5233,
                'route_day_id' => 2168,
                'departure_time' => '16:54:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 5234,
                'route_day_id' => 2169,
                'departure_time' => '16:55:00',
                'arrival_time' => '16:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 5235,
                'route_day_id' => 2170,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 5236,
                'route_day_id' => 2171,
                'departure_time' => '17:06:00',
                'arrival_time' => '17:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 5237,
                'route_day_id' => 2172,
                'departure_time' => '17:07:00',
                'arrival_time' => '17:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 5238,
                'route_day_id' => 2173,
                'departure_time' => '17:08:00',
                'arrival_time' => '17:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 5239,
                'route_day_id' => 2174,
                'departure_time' => '17:09:00',
                'arrival_time' => '17:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 5240,
                'route_day_id' => 2175,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 5241,
                'route_day_id' => 2176,
                'departure_time' => '17:11:00',
                'arrival_time' => '17:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 5242,
                'route_day_id' => 2177,
                'departure_time' => '17:12:00',
                'arrival_time' => '17:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 5243,
                'route_day_id' => 2178,
                'departure_time' => '17:13:00',
                'arrival_time' => '17:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 5244,
                'route_day_id' => 2179,
                'departure_time' => '17:14:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 5245,
                'route_day_id' => 2180,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 5246,
                'route_day_id' => 2181,
                'departure_time' => '17:25:00',
                'arrival_time' => '17:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 5247,
                'route_day_id' => 2182,
                'departure_time' => '17:26:00',
                'arrival_time' => '17:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 5248,
                'route_day_id' => 2183,
                'departure_time' => '17:27:00',
                'arrival_time' => '17:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 5249,
                'route_day_id' => 2184,
                'departure_time' => '17:28:00',
                'arrival_time' => '17:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 5250,
                'route_day_id' => 2185,
                'departure_time' => '17:29:00',
                'arrival_time' => '17:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 5251,
                'route_day_id' => 2186,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 5252,
                'route_day_id' => 2187,
                'departure_time' => '17:31:00',
                'arrival_time' => '17:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 5253,
                'route_day_id' => 2188,
                'departure_time' => '17:32:00',
                'arrival_time' => '17:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 5254,
                'route_day_id' => 2189,
                'departure_time' => '17:33:00',
                'arrival_time' => '17:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 5255,
                'route_day_id' => 2190,
                'departure_time' => '17:34:00',
                'arrival_time' => '17:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 5256,
                'route_day_id' => 2191,
                'departure_time' => '17:35:00',
                'arrival_time' => '17:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 5257,
                'route_day_id' => 2192,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 5258,
                'route_day_id' => 2193,
                'departure_time' => '17:46:00',
                'arrival_time' => '17:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 5259,
                'route_day_id' => 2194,
                'departure_time' => '17:47:00',
                'arrival_time' => '17:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 5260,
                'route_day_id' => 2195,
                'departure_time' => '17:48:00',
                'arrival_time' => '17:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 5261,
                'route_day_id' => 2196,
                'departure_time' => '17:49:00',
                'arrival_time' => '17:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 5262,
                'route_day_id' => 2197,
                'departure_time' => '17:50:00',
                'arrival_time' => '17:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 5263,
                'route_day_id' => 2198,
                'departure_time' => '17:51:00',
                'arrival_time' => '17:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 5264,
                'route_day_id' => 2199,
                'departure_time' => '17:52:00',
                'arrival_time' => '17:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 5265,
                'route_day_id' => 2200,
                'departure_time' => '17:53:00',
                'arrival_time' => '17:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 5266,
                'route_day_id' => 2201,
                'departure_time' => '17:54:00',
                'arrival_time' => '17:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 5267,
                'route_day_id' => 2202,
                'departure_time' => '17:55:00',
                'arrival_time' => '17:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 5268,
                'route_day_id' => 2203,
                'departure_time' => '18:05:00',
                'arrival_time' => '18:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 5269,
                'route_day_id' => 2204,
                'departure_time' => '18:06:00',
                'arrival_time' => '18:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 5270,
                'route_day_id' => 2205,
                'departure_time' => '18:07:00',
                'arrival_time' => '18:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 5271,
                'route_day_id' => 2206,
                'departure_time' => '18:08:00',
                'arrival_time' => '18:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 5272,
                'route_day_id' => 2207,
                'departure_time' => '18:09:00',
                'arrival_time' => '18:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 5273,
                'route_day_id' => 2208,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 5274,
                'route_day_id' => 2209,
                'departure_time' => '18:11:00',
                'arrival_time' => '18:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 5275,
                'route_day_id' => 2210,
                'departure_time' => '18:12:00',
                'arrival_time' => '18:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 5276,
                'route_day_id' => 2211,
                'departure_time' => '18:13:00',
                'arrival_time' => '18:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 5277,
                'route_day_id' => 2212,
                'departure_time' => '18:14:00',
                'arrival_time' => '18:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 5278,
                'route_day_id' => 2213,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 5279,
                'route_day_id' => 2214,
                'departure_time' => '18:25:00',
                'arrival_time' => '18:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 5280,
                'route_day_id' => 2215,
                'departure_time' => '18:26:00',
                'arrival_time' => '18:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 5281,
                'route_day_id' => 2216,
                'departure_time' => '18:27:00',
                'arrival_time' => '18:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 5282,
                'route_day_id' => 2217,
                'departure_time' => '18:28:00',
                'arrival_time' => '18:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 5283,
                'route_day_id' => 2218,
                'departure_time' => '18:29:00',
                'arrival_time' => '18:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 5284,
                'route_day_id' => 2219,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 5285,
                'route_day_id' => 2220,
                'departure_time' => '18:31:00',
                'arrival_time' => '18:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 5286,
                'route_day_id' => 2221,
                'departure_time' => '18:32:00',
                'arrival_time' => '18:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 5287,
                'route_day_id' => 2222,
                'departure_time' => '18:33:00',
                'arrival_time' => '18:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 5288,
                'route_day_id' => 2223,
                'departure_time' => '18:34:00',
                'arrival_time' => '18:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 5289,
                'route_day_id' => 2224,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 5290,
                'route_day_id' => 2225,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 5291,
                'route_day_id' => 2226,
                'departure_time' => '18:46:00',
                'arrival_time' => '18:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 5292,
                'route_day_id' => 2227,
                'departure_time' => '18:47:00',
                'arrival_time' => '18:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 5293,
                'route_day_id' => 2228,
                'departure_time' => '18:48:00',
                'arrival_time' => '18:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 5294,
                'route_day_id' => 2229,
                'departure_time' => '18:49:00',
                'arrival_time' => '18:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 5295,
                'route_day_id' => 2230,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 5296,
                'route_day_id' => 2231,
                'departure_time' => '18:51:00',
                'arrival_time' => '18:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 5297,
                'route_day_id' => 2232,
                'departure_time' => '18:52:00',
                'arrival_time' => '18:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 5298,
                'route_day_id' => 2233,
                'departure_time' => '18:53:00',
                'arrival_time' => '18:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 5299,
                'route_day_id' => 2234,
                'departure_time' => '18:54:00',
                'arrival_time' => '18:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 5300,
                'route_day_id' => 2235,
                'departure_time' => '18:55:00',
                'arrival_time' => '18:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 5301,
                'route_day_id' => 2236,
                'departure_time' => '19:05:00',
                'arrival_time' => '19:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 5302,
                'route_day_id' => 2237,
                'departure_time' => '19:06:00',
                'arrival_time' => '19:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 5303,
                'route_day_id' => 2238,
                'departure_time' => '19:07:00',
                'arrival_time' => '19:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 5304,
                'route_day_id' => 2239,
                'departure_time' => '19:08:00',
                'arrival_time' => '19:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 5305,
                'route_day_id' => 2240,
                'departure_time' => '19:09:00',
                'arrival_time' => '19:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 5306,
                'route_day_id' => 2241,
                'departure_time' => '19:10:00',
                'arrival_time' => '19:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 5307,
                'route_day_id' => 2242,
                'departure_time' => '19:11:00',
                'arrival_time' => '19:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 5308,
                'route_day_id' => 2243,
                'departure_time' => '19:12:00',
                'arrival_time' => '19:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 5309,
                'route_day_id' => 2244,
                'departure_time' => '19:13:00',
                'arrival_time' => '19:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 5310,
                'route_day_id' => 2245,
                'departure_time' => '19:14:00',
                'arrival_time' => '19:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 5311,
                'route_day_id' => 2246,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 5312,
                'route_day_id' => 2247,
                'departure_time' => '19:25:00',
                'arrival_time' => '19:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 5313,
                'route_day_id' => 2248,
                'departure_time' => '19:26:00',
                'arrival_time' => '19:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 5314,
                'route_day_id' => 2249,
                'departure_time' => '19:27:00',
                'arrival_time' => '19:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 5315,
                'route_day_id' => 2250,
                'departure_time' => '19:28:00',
                'arrival_time' => '19:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 5316,
                'route_day_id' => 2251,
                'departure_time' => '19:29:00',
                'arrival_time' => '19:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 5317,
                'route_day_id' => 2252,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 5318,
                'route_day_id' => 2253,
                'departure_time' => '19:31:00',
                'arrival_time' => '19:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 5319,
                'route_day_id' => 2254,
                'departure_time' => '19:32:00',
                'arrival_time' => '19:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 5320,
                'route_day_id' => 2255,
                'departure_time' => '19:33:00',
                'arrival_time' => '19:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 5321,
                'route_day_id' => 2256,
                'departure_time' => '19:34:00',
                'arrival_time' => '19:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 5322,
                'route_day_id' => 2257,
                'departure_time' => '19:35:00',
                'arrival_time' => '19:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 5323,
                'route_day_id' => 2258,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 5324,
                'route_day_id' => 2259,
                'departure_time' => '19:46:00',
                'arrival_time' => '19:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 5325,
                'route_day_id' => 2260,
                'departure_time' => '19:47:00',
                'arrival_time' => '19:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 5326,
                'route_day_id' => 2261,
                'departure_time' => '19:48:00',
                'arrival_time' => '19:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 5327,
                'route_day_id' => 2262,
                'departure_time' => '19:49:00',
                'arrival_time' => '19:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 5328,
                'route_day_id' => 2263,
                'departure_time' => '19:50:00',
                'arrival_time' => '19:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 5329,
                'route_day_id' => 2264,
                'departure_time' => '19:51:00',
                'arrival_time' => '19:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 5330,
                'route_day_id' => 2265,
                'departure_time' => '19:52:00',
                'arrival_time' => '19:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 5331,
                'route_day_id' => 2266,
                'departure_time' => '19:53:00',
                'arrival_time' => '19:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 5332,
                'route_day_id' => 2267,
                'departure_time' => '19:54:00',
                'arrival_time' => '19:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 5333,
                'route_day_id' => 2268,
                'departure_time' => '19:55:00',
                'arrival_time' => '19:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 5334,
                'route_day_id' => 2269,
                'departure_time' => '20:05:00',
                'arrival_time' => '20:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 5335,
                'route_day_id' => 2270,
                'departure_time' => '20:06:00',
                'arrival_time' => '20:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 5336,
                'route_day_id' => 2271,
                'departure_time' => '20:07:00',
                'arrival_time' => '20:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 5337,
                'route_day_id' => 2272,
                'departure_time' => '20:08:00',
                'arrival_time' => '20:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 5338,
                'route_day_id' => 2273,
                'departure_time' => '20:09:00',
                'arrival_time' => '20:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 5339,
                'route_day_id' => 2274,
                'departure_time' => '20:10:00',
                'arrival_time' => '20:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 5340,
                'route_day_id' => 2275,
                'departure_time' => '20:11:00',
                'arrival_time' => '20:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 5341,
                'route_day_id' => 2276,
                'departure_time' => '20:12:00',
                'arrival_time' => '20:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 5342,
                'route_day_id' => 2277,
                'departure_time' => '20:13:00',
                'arrival_time' => '20:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 5343,
                'route_day_id' => 2278,
                'departure_time' => '20:14:00',
                'arrival_time' => '20:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 5344,
                'route_day_id' => 2279,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 5345,
                'route_day_id' => 2280,
                'departure_time' => '20:25:00',
                'arrival_time' => '20:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 5346,
                'route_day_id' => 2281,
                'departure_time' => '20:26:00',
                'arrival_time' => '20:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 5347,
                'route_day_id' => 2282,
                'departure_time' => '20:27:00',
                'arrival_time' => '20:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 5348,
                'route_day_id' => 2283,
                'departure_time' => '20:28:00',
                'arrival_time' => '20:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 5349,
                'route_day_id' => 2284,
                'departure_time' => '20:29:00',
                'arrival_time' => '20:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 5350,
                'route_day_id' => 2285,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 5351,
                'route_day_id' => 2286,
                'departure_time' => '20:31:00',
                'arrival_time' => '20:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 5352,
                'route_day_id' => 2287,
                'departure_time' => '20:32:00',
                'arrival_time' => '20:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 5353,
                'route_day_id' => 2288,
                'departure_time' => '20:33:00',
                'arrival_time' => '20:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 5354,
                'route_day_id' => 2289,
                'departure_time' => '20:34:00',
                'arrival_time' => '20:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 5355,
                'route_day_id' => 2290,
                'departure_time' => '20:35:00',
                'arrival_time' => '20:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 5356,
                'route_day_id' => 2291,
                'departure_time' => '20:45:00',
                'arrival_time' => '20:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 5357,
                'route_day_id' => 2292,
                'departure_time' => '20:46:00',
                'arrival_time' => '20:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 5358,
                'route_day_id' => 2293,
                'departure_time' => '20:47:00',
                'arrival_time' => '20:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 5359,
                'route_day_id' => 2294,
                'departure_time' => '20:48:00',
                'arrival_time' => '20:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 5360,
                'route_day_id' => 2295,
                'departure_time' => '20:49:00',
                'arrival_time' => '20:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 5361,
                'route_day_id' => 2296,
                'departure_time' => '20:50:00',
                'arrival_time' => '20:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 5362,
                'route_day_id' => 2297,
                'departure_time' => '20:51:00',
                'arrival_time' => '20:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 5363,
                'route_day_id' => 2298,
                'departure_time' => '20:52:00',
                'arrival_time' => '20:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 5364,
                'route_day_id' => 2299,
                'departure_time' => '20:53:00',
                'arrival_time' => '20:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 5365,
                'route_day_id' => 2300,
                'departure_time' => '20:54:00',
                'arrival_time' => '20:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 5366,
                'route_day_id' => 2301,
                'departure_time' => '20:55:00',
                'arrival_time' => '20:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 5367,
                'route_day_id' => 2302,
                'departure_time' => '21:05:00',
                'arrival_time' => '21:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 5368,
                'route_day_id' => 2303,
                'departure_time' => '21:06:00',
                'arrival_time' => '21:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 5369,
                'route_day_id' => 2304,
                'departure_time' => '21:07:00',
                'arrival_time' => '21:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 5370,
                'route_day_id' => 2305,
                'departure_time' => '21:08:00',
                'arrival_time' => '21:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 5371,
                'route_day_id' => 2306,
                'departure_time' => '21:09:00',
                'arrival_time' => '21:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 5372,
                'route_day_id' => 2307,
                'departure_time' => '21:10:00',
                'arrival_time' => '21:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 5373,
                'route_day_id' => 2308,
                'departure_time' => '21:11:00',
                'arrival_time' => '21:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 5374,
                'route_day_id' => 2309,
                'departure_time' => '21:12:00',
                'arrival_time' => '21:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 5375,
                'route_day_id' => 2310,
                'departure_time' => '21:13:00',
                'arrival_time' => '21:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 5376,
                'route_day_id' => 2311,
                'departure_time' => '21:14:00',
                'arrival_time' => '21:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 5377,
                'route_day_id' => 2312,
                'departure_time' => '21:15:00',
                'arrival_time' => '21:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 5378,
                'route_day_id' => 2313,
                'departure_time' => '21:25:00',
                'arrival_time' => '21:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 5379,
                'route_day_id' => 2314,
                'departure_time' => '21:26:00',
                'arrival_time' => '21:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 5380,
                'route_day_id' => 2315,
                'departure_time' => '21:27:00',
                'arrival_time' => '21:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 5381,
                'route_day_id' => 2316,
                'departure_time' => '21:28:00',
                'arrival_time' => '21:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 5382,
                'route_day_id' => 2317,
                'departure_time' => '21:29:00',
                'arrival_time' => '21:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 5383,
                'route_day_id' => 2318,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 5384,
                'route_day_id' => 2319,
                'departure_time' => '21:31:00',
                'arrival_time' => '21:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 5385,
                'route_day_id' => 2320,
                'departure_time' => '21:32:00',
                'arrival_time' => '21:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 5386,
                'route_day_id' => 2321,
                'departure_time' => '21:33:00',
                'arrival_time' => '21:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 5387,
                'route_day_id' => 2322,
                'departure_time' => '21:34:00',
                'arrival_time' => '21:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 5388,
                'route_day_id' => 2323,
                'departure_time' => '21:35:00',
                'arrival_time' => '21:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 5389,
                'route_day_id' => 2324,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 5390,
                'route_day_id' => 2325,
                'departure_time' => '21:46:00',
                'arrival_time' => '21:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 5391,
                'route_day_id' => 2326,
                'departure_time' => '21:47:00',
                'arrival_time' => '21:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 5392,
                'route_day_id' => 2327,
                'departure_time' => '21:48:00',
                'arrival_time' => '21:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 5393,
                'route_day_id' => 2328,
                'departure_time' => '21:49:00',
                'arrival_time' => '21:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 5394,
                'route_day_id' => 2329,
                'departure_time' => '21:50:00',
                'arrival_time' => '21:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 5395,
                'route_day_id' => 2330,
                'departure_time' => '21:51:00',
                'arrival_time' => '21:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 5396,
                'route_day_id' => 2331,
                'departure_time' => '21:52:00',
                'arrival_time' => '21:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 5397,
                'route_day_id' => 2332,
                'departure_time' => '21:53:00',
                'arrival_time' => '21:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 5398,
                'route_day_id' => 2333,
                'departure_time' => '21:54:00',
                'arrival_time' => '21:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 5399,
                'route_day_id' => 2334,
                'departure_time' => '21:55:00',
                'arrival_time' => '21:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 5400,
                'route_day_id' => 2335,
                'departure_time' => '22:05:00',
                'arrival_time' => '22:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 5401,
                'route_day_id' => 2336,
                'departure_time' => '22:06:00',
                'arrival_time' => '22:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 5402,
                'route_day_id' => 2337,
                'departure_time' => '22:07:00',
                'arrival_time' => '22:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 5403,
                'route_day_id' => 2338,
                'departure_time' => '22:08:00',
                'arrival_time' => '22:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 5404,
                'route_day_id' => 2339,
                'departure_time' => '22:09:00',
                'arrival_time' => '22:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 5405,
                'route_day_id' => 2340,
                'departure_time' => '22:10:00',
                'arrival_time' => '22:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 5406,
                'route_day_id' => 2341,
                'departure_time' => '22:11:00',
                'arrival_time' => '22:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 5407,
                'route_day_id' => 2342,
                'departure_time' => '22:12:00',
                'arrival_time' => '22:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 5408,
                'route_day_id' => 2343,
                'departure_time' => '22:13:00',
                'arrival_time' => '22:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 5409,
                'route_day_id' => 2344,
                'departure_time' => '22:14:00',
                'arrival_time' => '22:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 5410,
                'route_day_id' => 2345,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 5411,
                'route_day_id' => 2346,
                'departure_time' => '22:25:00',
                'arrival_time' => '22:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 5412,
                'route_day_id' => 2347,
                'departure_time' => '22:26:00',
                'arrival_time' => '22:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 5413,
                'route_day_id' => 2348,
                'departure_time' => '22:27:00',
                'arrival_time' => '22:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 5414,
                'route_day_id' => 2349,
                'departure_time' => '22:28:00',
                'arrival_time' => '22:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 5415,
                'route_day_id' => 2350,
                'departure_time' => '22:29:00',
                'arrival_time' => '22:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 5416,
                'route_day_id' => 2351,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 5417,
                'route_day_id' => 2352,
                'departure_time' => '22:31:00',
                'arrival_time' => '22:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 5418,
                'route_day_id' => 2353,
                'departure_time' => '22:32:00',
                'arrival_time' => '22:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 5419,
                'route_day_id' => 2354,
                'departure_time' => '22:33:00',
                'arrival_time' => '22:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 5420,
                'route_day_id' => 2355,
                'departure_time' => '22:34:00',
                'arrival_time' => '22:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 5421,
                'route_day_id' => 2356,
                'departure_time' => '22:35:00',
                'arrival_time' => '22:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 5422,
                'route_day_id' => 2357,
                'departure_time' => '23:05:00',
                'arrival_time' => '23:06:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 5423,
                'route_day_id' => 2358,
                'departure_time' => '23:06:00',
                'arrival_time' => '23:07:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 5424,
                'route_day_id' => 2359,
                'departure_time' => '23:07:00',
                'arrival_time' => '23:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 5425,
                'route_day_id' => 2360,
                'departure_time' => '23:08:00',
                'arrival_time' => '23:09:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 5426,
                'route_day_id' => 2361,
                'departure_time' => '23:09:00',
                'arrival_time' => '23:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 5427,
                'route_day_id' => 2362,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:11:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 5428,
                'route_day_id' => 2363,
                'departure_time' => '23:11:00',
                'arrival_time' => '23:12:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 5429,
                'route_day_id' => 2364,
                'departure_time' => '23:12:00',
                'arrival_time' => '23:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 5430,
                'route_day_id' => 2365,
                'departure_time' => '23:13:00',
                'arrival_time' => '23:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 5431,
                'route_day_id' => 2366,
                'departure_time' => '23:14:00',
                'arrival_time' => '23:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 5432,
                'route_day_id' => 2367,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 5433,
                'route_day_id' => 2368,
                'departure_time' => '23:25:00',
                'arrival_time' => '23:26:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 5434,
                'route_day_id' => 2369,
                'departure_time' => '23:26:00',
                'arrival_time' => '23:27:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 5435,
                'route_day_id' => 2370,
                'departure_time' => '23:27:00',
                'arrival_time' => '23:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 5436,
                'route_day_id' => 2371,
                'departure_time' => '23:28:00',
                'arrival_time' => '23:29:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 5437,
                'route_day_id' => 2372,
                'departure_time' => '23:29:00',
                'arrival_time' => '23:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 5438,
                'route_day_id' => 2373,
                'departure_time' => '23:30:00',
                'arrival_time' => '23:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 5439,
                'route_day_id' => 2374,
                'departure_time' => '23:31:00',
                'arrival_time' => '23:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 5440,
                'route_day_id' => 2375,
                'departure_time' => '23:32:00',
                'arrival_time' => '23:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 5441,
                'route_day_id' => 2376,
                'departure_time' => '23:33:00',
                'arrival_time' => '23:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 5442,
                'route_day_id' => 2377,
                'departure_time' => '23:34:00',
                'arrival_time' => '23:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 5443,
                'route_day_id' => 2378,
                'departure_time' => '23:35:00',
                'arrival_time' => '23:38:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 5444,
                'route_day_id' => 2379,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 5445,
                'route_day_id' => 2380,
                'departure_time' => '23:46:00',
                'arrival_time' => '23:47:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 5446,
                'route_day_id' => 2381,
                'departure_time' => '23:47:00',
                'arrival_time' => '23:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 5447,
                'route_day_id' => 2382,
                'departure_time' => '23:48:00',
                'arrival_time' => '23:49:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 5448,
                'route_day_id' => 2383,
                'departure_time' => '23:49:00',
                'arrival_time' => '23:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 5449,
                'route_day_id' => 2384,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:51:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 5450,
                'route_day_id' => 2385,
                'departure_time' => '23:51:00',
                'arrival_time' => '23:52:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 5451,
                'route_day_id' => 2386,
                'departure_time' => '23:52:00',
                'arrival_time' => '23:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 5452,
                'route_day_id' => 2387,
                'departure_time' => '23:53:00',
                'arrival_time' => '23:54:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 5453,
                'route_day_id' => 2388,
                'departure_time' => '23:54:00',
                'arrival_time' => '23:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 5454,
                'route_day_id' => 2389,
                'departure_time' => '23:55:00',
                'arrival_time' => '23:58:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 5631,
                'route_day_id' => 2566,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 5632,
                'route_day_id' => 2566,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 5633,
                'route_day_id' => 2566,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 5634,
                'route_day_id' => 2566,
                'departure_time' => '15:45:00',
                'arrival_time' => '16:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 5635,
                'route_day_id' => 2566,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 5636,
                'route_day_id' => 2566,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 5683,
                'route_day_id' => 2568,
                'departure_time' => '00:00:00',
                'arrival_time' => '00:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 5684,
                'route_day_id' => 2568,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 5685,
                'route_day_id' => 2568,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 5686,
                'route_day_id' => 2568,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 5687,
                'route_day_id' => 2568,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 5688,
                'route_day_id' => 2568,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 5689,
                'route_day_id' => 2568,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 5690,
                'route_day_id' => 2568,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 5691,
                'route_day_id' => 2568,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 5692,
                'route_day_id' => 2568,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 5693,
                'route_day_id' => 2568,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 5694,
                'route_day_id' => 2568,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 5695,
                'route_day_id' => 2568,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 5696,
                'route_day_id' => 2568,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 5697,
                'route_day_id' => 2568,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 5698,
                'route_day_id' => 2568,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 5699,
                'route_day_id' => 2568,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 5700,
                'route_day_id' => 2568,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 5701,
                'route_day_id' => 2568,
                'departure_time' => '14:40:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 5702,
                'route_day_id' => 2568,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 5703,
                'route_day_id' => 2568,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 5704,
                'route_day_id' => 2568,
                'departure_time' => '15:40:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 5705,
                'route_day_id' => 2568,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 5706,
                'route_day_id' => 2568,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 5707,
                'route_day_id' => 2568,
                'departure_time' => '16:40:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 5708,
                'route_day_id' => 2568,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 5709,
                'route_day_id' => 2568,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 5710,
                'route_day_id' => 2568,
                'departure_time' => '17:40:00',
                'arrival_time' => '18:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 5711,
                'route_day_id' => 2568,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 5712,
                'route_day_id' => 2568,
                'departure_time' => '18:20:00',
                'arrival_time' => '18:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 5713,
                'route_day_id' => 2568,
                'departure_time' => '18:40:00',
                'arrival_time' => '19:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 5714,
                'route_day_id' => 2568,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 5715,
                'route_day_id' => 2568,
                'departure_time' => '19:20:00',
                'arrival_time' => '19:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 5716,
                'route_day_id' => 2568,
                'departure_time' => '19:40:00',
                'arrival_time' => '20:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 5717,
                'route_day_id' => 2568,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 5718,
                'route_day_id' => 2568,
                'departure_time' => '20:20:00',
                'arrival_time' => '20:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 5719,
                'route_day_id' => 2568,
                'departure_time' => '20:40:00',
                'arrival_time' => '21:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 5720,
                'route_day_id' => 2568,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 5721,
                'route_day_id' => 2568,
                'departure_time' => '21:20:00',
                'arrival_time' => '21:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 5722,
                'route_day_id' => 2568,
                'departure_time' => '21:40:00',
                'arrival_time' => '22:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 5723,
                'route_day_id' => 2568,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 5724,
                'route_day_id' => 2568,
                'departure_time' => '22:20:00',
                'arrival_time' => '22:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 5725,
                'route_day_id' => 2568,
                'departure_time' => '22:40:00',
                'arrival_time' => '23:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 5726,
                'route_day_id' => 2568,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 5727,
                'route_day_id' => 2568,
                'departure_time' => '23:20:00',
                'arrival_time' => '23:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 5728,
                'route_day_id' => 2568,
                'departure_time' => '23:40:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 5731,
                'route_day_id' => 2571,
                'departure_time' => '11:00:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 5732,
                'route_day_id' => 2572,
                'departure_time' => '14:20:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 5733,
                'route_day_id' => 2573,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 5734,
                'route_day_id' => 2574,
                'departure_time' => '08:50:00',
                'arrival_time' => '12:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 5735,
                'route_day_id' => 2575,
                'departure_time' => '06:30:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 5736,
                'route_day_id' => 2576,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 5737,
                'route_day_id' => 2577,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 5738,
                'route_day_id' => 2578,
                'departure_time' => '08:30:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 5739,
                'route_day_id' => 2579,
                'departure_time' => '14:30:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 5740,
                'route_day_id' => 2580,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 5741,
                'route_day_id' => 2581,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 5742,
                'route_day_id' => 2582,
                'departure_time' => '16:40:00',
                'arrival_time' => '17:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 5743,
                'route_day_id' => 2583,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 5744,
                'route_day_id' => 2584,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 5745,
                'route_day_id' => 2585,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 5746,
                'route_day_id' => 2586,
                'departure_time' => '08:40:00',
                'arrival_time' => '10:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 5747,
                'route_day_id' => 2587,
                'departure_time' => '10:30:00',
                'arrival_time' => '12:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 5748,
                'route_day_id' => 2588,
                'departure_time' => '12:15:00',
                'arrival_time' => '13:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 5749,
                'route_day_id' => 2589,
                'departure_time' => '09:00:00',
                'arrival_time' => '13:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 5750,
                'route_day_id' => 2590,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 5751,
                'route_day_id' => 2591,
                'departure_time' => '15:10:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 5752,
                'route_day_id' => 2592,
                'departure_time' => '09:00:00',
                'arrival_time' => '12:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 5753,
                'route_day_id' => 2593,
                'departure_time' => '12:15:00',
                'arrival_time' => '13:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 5754,
                'route_day_id' => 2594,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 5755,
                'route_day_id' => 2595,
                'departure_time' => '15:10:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 5756,
                'route_day_id' => 2596,
                'departure_time' => '00:05:00',
                'arrival_time' => '00:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 5757,
                'route_day_id' => 2596,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 5758,
                'route_day_id' => 2596,
                'departure_time' => '00:25:00',
                'arrival_time' => '00:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 5759,
                'route_day_id' => 2596,
                'departure_time' => '00:35:00',
                'arrival_time' => '00:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 5760,
                'route_day_id' => 2596,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 5761,
                'route_day_id' => 2596,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 5762,
                'route_day_id' => 2596,
                'departure_time' => '01:15:00',
                'arrival_time' => '01:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 5763,
                'route_day_id' => 2596,
                'departure_time' => '01:45:00',
                'arrival_time' => '01:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 5764,
                'route_day_id' => 2596,
                'departure_time' => '02:15:00',
                'arrival_time' => '02:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 5765,
                'route_day_id' => 2596,
                'departure_time' => '03:15:00',
                'arrival_time' => '03:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 5766,
                'route_day_id' => 2596,
                'departure_time' => '04:15:00',
                'arrival_time' => '04:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 5767,
                'route_day_id' => 2596,
                'departure_time' => '05:15:00',
                'arrival_time' => '05:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 5768,
                'route_day_id' => 2596,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 5769,
                'route_day_id' => 2596,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 5770,
                'route_day_id' => 2596,
                'departure_time' => '06:10:00',
                'arrival_time' => '06:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 5771,
                'route_day_id' => 2596,
                'departure_time' => '06:20:00',
                'arrival_time' => '06:28:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 5772,
                'route_day_id' => 2596,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 5773,
                'route_day_id' => 2596,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 5774,
                'route_day_id' => 2596,
                'departure_time' => '06:40:00',
                'arrival_time' => '06:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 5775,
                'route_day_id' => 2596,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:48:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 5776,
                'route_day_id' => 2596,
                'departure_time' => '06:48:00',
                'arrival_time' => '06:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 5777,
                'route_day_id' => 2596,
                'departure_time' => '06:50:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 5778,
                'route_day_id' => 2596,
                'departure_time' => '06:55:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 5779,
                'route_day_id' => 2596,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 5780,
                'route_day_id' => 2596,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 5781,
                'route_day_id' => 2596,
                'departure_time' => '07:10:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 5782,
                'route_day_id' => 2596,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:18:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 5783,
                'route_day_id' => 2596,
                'departure_time' => '07:18:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 5784,
                'route_day_id' => 2596,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 5785,
                'route_day_id' => 2596,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 5786,
                'route_day_id' => 2596,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:34:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 5787,
                'route_day_id' => 2596,
                'departure_time' => '07:34:00',
                'arrival_time' => '07:37:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 5788,
                'route_day_id' => 2596,
                'departure_time' => '07:37:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 5789,
                'route_day_id' => 2596,
                'departure_time' => '07:40:00',
                'arrival_time' => '07:42:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 5790,
                'route_day_id' => 2596,
                'departure_time' => '07:42:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 5791,
                'route_day_id' => 2596,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 5792,
                'route_day_id' => 2596,
                'departure_time' => '07:50:00',
                'arrival_time' => '07:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 5793,
                'route_day_id' => 2596,
                'departure_time' => '07:53:00',
                'arrival_time' => '07:57:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 5794,
                'route_day_id' => 2596,
                'departure_time' => '07:57:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 5795,
                'route_day_id' => 2596,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 5797,
                'route_day_id' => 2596,
                'departure_time' => '08:10:00',
                'arrival_time' => '08:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 5798,
                'route_day_id' => 2596,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 5799,
                'route_day_id' => 2596,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 5800,
                'route_day_id' => 2596,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 5801,
                'route_day_id' => 2596,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 5802,
                'route_day_id' => 2596,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 5803,
                'route_day_id' => 2596,
                'departure_time' => '08:40:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 5804,
                'route_day_id' => 2596,
                'departure_time' => '08:45:00',
                'arrival_time' => '08:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 5805,
                'route_day_id' => 2596,
                'departure_time' => '08:50:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 5806,
                'route_day_id' => 2596,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 5808,
                'route_day_id' => 2596,
                'departure_time' => '09:05:00',
                'arrival_time' => '09:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 5810,
                'route_day_id' => 2596,
                'departure_time' => '09:15:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 5811,
                'route_day_id' => 2596,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 5812,
                'route_day_id' => 2596,
                'departure_time' => '09:25:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 5814,
                'route_day_id' => 2596,
                'departure_time' => '09:35:00',
                'arrival_time' => '09:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 5816,
                'route_day_id' => 2596,
                'departure_time' => '09:45:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 5817,
                'route_day_id' => 2596,
                'departure_time' => '09:50:00',
                'arrival_time' => '09:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 5818,
                'route_day_id' => 2596,
                'departure_time' => '09:55:00',
                'arrival_time' => '10:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 5820,
                'route_day_id' => 2596,
                'departure_time' => '10:05:00',
                'arrival_time' => '10:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 5822,
                'route_day_id' => 2596,
                'departure_time' => '10:15:00',
                'arrival_time' => '10:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 5824,
                'route_day_id' => 2596,
                'departure_time' => '10:25:00',
                'arrival_time' => '10:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 5826,
                'route_day_id' => 2596,
                'departure_time' => '10:35:00',
                'arrival_time' => '10:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 5828,
                'route_day_id' => 2596,
                'departure_time' => '10:45:00',
                'arrival_time' => '10:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 5830,
                'route_day_id' => 2596,
                'departure_time' => '10:55:00',
                'arrival_time' => '11:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 5832,
                'route_day_id' => 2596,
                'departure_time' => '11:05:00',
                'arrival_time' => '11:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 5834,
                'route_day_id' => 2596,
                'departure_time' => '11:15:00',
                'arrival_time' => '11:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 5836,
                'route_day_id' => 2596,
                'departure_time' => '11:25:00',
                'arrival_time' => '11:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 5838,
                'route_day_id' => 2596,
                'departure_time' => '11:35:00',
                'arrival_time' => '11:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 5840,
                'route_day_id' => 2596,
                'departure_time' => '11:45:00',
                'arrival_time' => '11:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 5842,
                'route_day_id' => 2596,
                'departure_time' => '11:55:00',
                'arrival_time' => '12:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 5844,
                'route_day_id' => 2596,
                'departure_time' => '12:05:00',
                'arrival_time' => '12:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 5846,
                'route_day_id' => 2596,
                'departure_time' => '12:15:00',
                'arrival_time' => '12:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 5848,
                'route_day_id' => 2596,
                'departure_time' => '12:25:00',
                'arrival_time' => '12:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 5849,
                'route_day_id' => 2596,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 5850,
                'route_day_id' => 2596,
                'departure_time' => '12:35:00',
                'arrival_time' => '12:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 5851,
                'route_day_id' => 2596,
                'departure_time' => '12:40:00',
                'arrival_time' => '12:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 5852,
                'route_day_id' => 2596,
                'departure_time' => '12:45:00',
                'arrival_time' => '12:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 5853,
                'route_day_id' => 2596,
                'departure_time' => '12:50:00',
                'arrival_time' => '12:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 5854,
                'route_day_id' => 2596,
                'departure_time' => '12:55:00',
                'arrival_time' => '13:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 5855,
                'route_day_id' => 2596,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 5856,
                'route_day_id' => 2596,
                'departure_time' => '13:05:00',
                'arrival_time' => '13:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 5857,
                'route_day_id' => 2596,
                'departure_time' => '13:10:00',
                'arrival_time' => '13:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 5858,
                'route_day_id' => 2596,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 5860,
                'route_day_id' => 2596,
                'departure_time' => '13:25:00',
                'arrival_time' => '13:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 5861,
                'route_day_id' => 2596,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 5862,
                'route_day_id' => 2596,
                'departure_time' => '13:35:00',
                'arrival_time' => '13:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 5863,
                'route_day_id' => 2596,
                'departure_time' => '13:40:00',
                'arrival_time' => '13:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 5864,
                'route_day_id' => 2596,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 5866,
                'route_day_id' => 2596,
                'departure_time' => '13:55:00',
                'arrival_time' => '14:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 5868,
                'route_day_id' => 2596,
                'departure_time' => '14:05:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 5869,
                'route_day_id' => 2596,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 5870,
                'route_day_id' => 2596,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 5872,
                'route_day_id' => 2596,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 5874,
                'route_day_id' => 2596,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 5875,
                'route_day_id' => 2596,
                'departure_time' => '14:40:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 5876,
                'route_day_id' => 2596,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 5878,
                'route_day_id' => 2596,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 5880,
                'route_day_id' => 2596,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 5881,
                'route_day_id' => 2596,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 5882,
                'route_day_id' => 2596,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 5884,
                'route_day_id' => 2596,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 5886,
                'route_day_id' => 2596,
                'departure_time' => '15:35:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 5887,
                'route_day_id' => 2596,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 5888,
                'route_day_id' => 2596,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 5890,
                'route_day_id' => 2596,
                'departure_time' => '15:55:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 5892,
                'route_day_id' => 2596,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 5893,
                'route_day_id' => 2596,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 5894,
                'route_day_id' => 2596,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 5896,
                'route_day_id' => 2596,
                'departure_time' => '16:25:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 5898,
                'route_day_id' => 2596,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 5900,
                'route_day_id' => 2596,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 5901,
                'route_day_id' => 2596,
                'departure_time' => '16:50:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 5902,
                'route_day_id' => 2596,
                'departure_time' => '16:55:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 5904,
                'route_day_id' => 2596,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 5905,
                'route_day_id' => 2596,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 5906,
                'route_day_id' => 2596,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 5907,
                'route_day_id' => 2596,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 5908,
                'route_day_id' => 2596,
                'departure_time' => '17:25:00',
                'arrival_time' => '17:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 5910,
                'route_day_id' => 2596,
                'departure_time' => '17:35:00',
                'arrival_time' => '17:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 5911,
                'route_day_id' => 2596,
                'departure_time' => '17:40:00',
                'arrival_time' => '17:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 5912,
                'route_day_id' => 2596,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 5913,
                'route_day_id' => 2596,
                'departure_time' => '17:50:00',
                'arrival_time' => '17:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 5914,
                'route_day_id' => 2596,
                'departure_time' => '17:55:00',
                'arrival_time' => '18:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 5915,
                'route_day_id' => 2596,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 5916,
                'route_day_id' => 2596,
                'departure_time' => '18:05:00',
                'arrival_time' => '18:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 5917,
                'route_day_id' => 2596,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 5918,
                'route_day_id' => 2596,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 5919,
                'route_day_id' => 2596,
                'departure_time' => '18:20:00',
                'arrival_time' => '18:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 5920,
                'route_day_id' => 2596,
                'departure_time' => '18:25:00',
                'arrival_time' => '18:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 5921,
                'route_day_id' => 2596,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 5922,
                'route_day_id' => 2596,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 5923,
                'route_day_id' => 2596,
                'departure_time' => '18:40:00',
                'arrival_time' => '18:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 5924,
                'route_day_id' => 2596,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 5925,
                'route_day_id' => 2596,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 5926,
                'route_day_id' => 2596,
                'departure_time' => '18:55:00',
                'arrival_time' => '19:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 5927,
                'route_day_id' => 2596,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 5928,
                'route_day_id' => 2596,
                'departure_time' => '19:05:00',
                'arrival_time' => '19:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 5930,
                'route_day_id' => 2596,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 5932,
                'route_day_id' => 2596,
                'departure_time' => '19:25:00',
                'arrival_time' => '19:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 5933,
                'route_day_id' => 2596,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 5934,
                'route_day_id' => 2596,
                'departure_time' => '19:35:00',
                'arrival_time' => '19:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 5935,
                'route_day_id' => 2596,
                'departure_time' => '19:40:00',
                'arrival_time' => '19:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 5936,
                'route_day_id' => 2596,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 5938,
                'route_day_id' => 2596,
                'departure_time' => '19:55:00',
                'arrival_time' => '20:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 5940,
                'route_day_id' => 2596,
                'departure_time' => '20:05:00',
                'arrival_time' => '20:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 5942,
                'route_day_id' => 2596,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 5944,
                'route_day_id' => 2596,
                'departure_time' => '20:25:00',
                'arrival_time' => '20:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 5946,
                'route_day_id' => 2596,
                'departure_time' => '20:35:00',
                'arrival_time' => '20:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 5948,
                'route_day_id' => 2596,
                'departure_time' => '20:45:00',
                'arrival_time' => '20:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 5949,
                'route_day_id' => 2596,
                'departure_time' => '20:50:00',
                'arrival_time' => '20:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 5950,
                'route_day_id' => 2596,
                'departure_time' => '20:55:00',
                'arrival_time' => '21:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 5952,
                'route_day_id' => 2596,
                'departure_time' => '21:05:00',
                'arrival_time' => '21:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 5954,
                'route_day_id' => 2596,
                'departure_time' => '21:15:00',
                'arrival_time' => '21:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 5955,
                'route_day_id' => 2596,
                'departure_time' => '21:20:00',
                'arrival_time' => '21:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 5956,
                'route_day_id' => 2596,
                'departure_time' => '21:25:00',
                'arrival_time' => '21:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 5958,
                'route_day_id' => 2596,
                'departure_time' => '21:35:00',
                'arrival_time' => '21:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 5960,
                'route_day_id' => 2596,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 5961,
                'route_day_id' => 2596,
                'departure_time' => '21:50:00',
                'arrival_time' => '21:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 5962,
                'route_day_id' => 2596,
                'departure_time' => '21:55:00',
                'arrival_time' => '22:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 5964,
                'route_day_id' => 2596,
                'departure_time' => '22:05:00',
                'arrival_time' => '22:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 5966,
                'route_day_id' => 2596,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 5967,
                'route_day_id' => 2596,
                'departure_time' => '22:20:00',
                'arrival_time' => '22:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 5968,
                'route_day_id' => 2596,
                'departure_time' => '22:25:00',
                'arrival_time' => '22:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 5969,
                'route_day_id' => 2596,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 5970,
                'route_day_id' => 2596,
                'departure_time' => '22:35:00',
                'arrival_time' => '22:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 5971,
                'route_day_id' => 2596,
                'departure_time' => '22:40:00',
                'arrival_time' => '22:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 5972,
                'route_day_id' => 2596,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 5973,
                'route_day_id' => 2596,
                'departure_time' => '22:50:00',
                'arrival_time' => '22:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 5974,
                'route_day_id' => 2596,
                'departure_time' => '22:55:00',
                'arrival_time' => '23:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 5975,
                'route_day_id' => 2596,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 5976,
                'route_day_id' => 2596,
                'departure_time' => '23:05:00',
                'arrival_time' => '23:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 5977,
                'route_day_id' => 2596,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 5978,
                'route_day_id' => 2596,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 5979,
                'route_day_id' => 2596,
                'departure_time' => '23:20:00',
                'arrival_time' => '23:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 5980,
                'route_day_id' => 2596,
                'departure_time' => '23:25:00',
                'arrival_time' => '23:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 5982,
                'route_day_id' => 2596,
                'departure_time' => '23:35:00',
                'arrival_time' => '23:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 5984,
                'route_day_id' => 2596,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 5985,
                'route_day_id' => 2596,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 5986,
                'route_day_id' => 2596,
                'departure_time' => '23:55:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 5987,
                'route_day_id' => 2597,
                'departure_time' => '00:05:00',
                'arrival_time' => '00:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 5988,
                'route_day_id' => 2597,
                'departure_time' => '00:10:00',
                'arrival_time' => '00:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 5989,
                'route_day_id' => 2597,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 5990,
                'route_day_id' => 2597,
                'departure_time' => '00:20:00',
                'arrival_time' => '00:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 5991,
                'route_day_id' => 2597,
                'departure_time' => '00:25:00',
                'arrival_time' => '00:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 5992,
                'route_day_id' => 2597,
                'departure_time' => '00:35:00',
                'arrival_time' => '00:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 5993,
                'route_day_id' => 2597,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 5994,
                'route_day_id' => 2597,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_times')->insert(array (
            0 => 
            array (
                'id' => 5995,
                'route_day_id' => 2597,
                'departure_time' => '01:15:00',
                'arrival_time' => '01:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5996,
                'route_day_id' => 2597,
                'departure_time' => '01:45:00',
                'arrival_time' => '01:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 5997,
                'route_day_id' => 2597,
                'departure_time' => '02:15:00',
                'arrival_time' => '02:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5998,
                'route_day_id' => 2597,
                'departure_time' => '03:15:00',
                'arrival_time' => '03:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5999,
                'route_day_id' => 2597,
                'departure_time' => '04:15:00',
                'arrival_time' => '04:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6000,
                'route_day_id' => 2597,
                'departure_time' => '05:15:00',
                'arrival_time' => '05:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 6001,
                'route_day_id' => 2597,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 6002,
                'route_day_id' => 2597,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:08:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 6003,
                'route_day_id' => 2597,
                'departure_time' => '06:15:00',
                'arrival_time' => '06:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 6004,
                'route_day_id' => 2597,
                'departure_time' => '06:25:00',
                'arrival_time' => '06:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 6005,
                'route_day_id' => 2597,
                'departure_time' => '06:35:00',
                'arrival_time' => '06:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 6006,
                'route_day_id' => 2597,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 6007,
                'route_day_id' => 2597,
                'departure_time' => '06:55:00',
                'arrival_time' => '07:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 6008,
                'route_day_id' => 2597,
                'departure_time' => '07:05:00',
                'arrival_time' => '07:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 6009,
                'route_day_id' => 2597,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 6010,
                'route_day_id' => 2597,
                'departure_time' => '07:25:00',
                'arrival_time' => '07:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 6011,
                'route_day_id' => 2597,
                'departure_time' => '07:35:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 6012,
                'route_day_id' => 2597,
                'departure_time' => '07:40:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 6013,
                'route_day_id' => 2597,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 6014,
                'route_day_id' => 2597,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 6015,
                'route_day_id' => 2597,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 6016,
                'route_day_id' => 2597,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 6017,
                'route_day_id' => 2597,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 6018,
                'route_day_id' => 2597,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 6019,
                'route_day_id' => 2597,
                'departure_time' => '08:45:00',
                'arrival_time' => '08:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 6020,
                'route_day_id' => 2597,
                'departure_time' => '08:55:00',
                'arrival_time' => '09:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 6021,
                'route_day_id' => 2597,
                'departure_time' => '09:05:00',
                'arrival_time' => '09:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 6022,
                'route_day_id' => 2597,
                'departure_time' => '09:15:00',
                'arrival_time' => '09:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 6023,
                'route_day_id' => 2597,
                'departure_time' => '09:25:00',
                'arrival_time' => '09:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 6024,
                'route_day_id' => 2597,
                'departure_time' => '09:35:00',
                'arrival_time' => '09:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 6025,
                'route_day_id' => 2597,
                'departure_time' => '09:45:00',
                'arrival_time' => '09:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 6026,
                'route_day_id' => 2597,
                'departure_time' => '09:55:00',
                'arrival_time' => '10:03:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 6027,
                'route_day_id' => 2597,
                'departure_time' => '10:05:00',
                'arrival_time' => '10:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 6028,
                'route_day_id' => 2597,
                'departure_time' => '10:15:00',
                'arrival_time' => '10:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 6029,
                'route_day_id' => 2597,
                'departure_time' => '10:25:00',
                'arrival_time' => '10:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 6030,
                'route_day_id' => 2597,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 6031,
                'route_day_id' => 2597,
                'departure_time' => '10:35:00',
                'arrival_time' => '10:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 6032,
                'route_day_id' => 2597,
                'departure_time' => '10:45:00',
                'arrival_time' => '10:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 6033,
                'route_day_id' => 2597,
                'departure_time' => '10:55:00',
                'arrival_time' => '11:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 6034,
                'route_day_id' => 2597,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 6035,
                'route_day_id' => 2597,
                'departure_time' => '11:05:00',
                'arrival_time' => '11:13:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 6036,
                'route_day_id' => 2597,
                'departure_time' => '11:15:00',
                'arrival_time' => '11:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 6037,
                'route_day_id' => 2597,
                'departure_time' => '11:25:00',
                'arrival_time' => '11:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 6038,
                'route_day_id' => 2597,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 6039,
                'route_day_id' => 2597,
                'departure_time' => '11:35:00',
                'arrival_time' => '11:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 6040,
                'route_day_id' => 2597,
                'departure_time' => '11:45:00',
                'arrival_time' => '11:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 6041,
                'route_day_id' => 2597,
                'departure_time' => '11:55:00',
                'arrival_time' => '12:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 6042,
                'route_day_id' => 2597,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 6043,
                'route_day_id' => 2597,
                'departure_time' => '12:05:00',
                'arrival_time' => '12:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 6044,
                'route_day_id' => 2597,
                'departure_time' => '13:35:00',
                'arrival_time' => '13:43:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 6045,
                'route_day_id' => 2597,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 6046,
                'route_day_id' => 2597,
                'departure_time' => '13:50:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 6047,
                'route_day_id' => 2597,
                'departure_time' => '13:55:00',
                'arrival_time' => '14:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 6048,
                'route_day_id' => 2597,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 6049,
                'route_day_id' => 2597,
                'departure_time' => '14:05:00',
                'arrival_time' => '14:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 6050,
                'route_day_id' => 2597,
                'departure_time' => '14:10:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 6051,
                'route_day_id' => 2597,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 6052,
                'route_day_id' => 2597,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 6053,
                'route_day_id' => 2597,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 6054,
                'route_day_id' => 2597,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 6055,
                'route_day_id' => 2597,
                'departure_time' => '14:35:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 6056,
                'route_day_id' => 2597,
                'departure_time' => '14:40:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 6057,
                'route_day_id' => 2597,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 6058,
                'route_day_id' => 2597,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 6059,
                'route_day_id' => 2597,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 6060,
                'route_day_id' => 2597,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 6061,
                'route_day_id' => 2597,
                'departure_time' => '15:05:00',
                'arrival_time' => '15:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 6062,
                'route_day_id' => 2597,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 6063,
                'route_day_id' => 2597,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 6064,
                'route_day_id' => 2597,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 6065,
                'route_day_id' => 2597,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 6066,
                'route_day_id' => 2597,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 6067,
                'route_day_id' => 2597,
                'departure_time' => '15:35:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 6068,
                'route_day_id' => 2597,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 6069,
                'route_day_id' => 2597,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 6070,
                'route_day_id' => 2597,
                'departure_time' => '15:50:00',
                'arrival_time' => '15:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 6071,
                'route_day_id' => 2597,
                'departure_time' => '15:55:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 6072,
                'route_day_id' => 2597,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 6073,
                'route_day_id' => 2597,
                'departure_time' => '16:05:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 6074,
                'route_day_id' => 2597,
                'departure_time' => '16:10:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 6075,
                'route_day_id' => 2597,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 6076,
                'route_day_id' => 2597,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 6077,
                'route_day_id' => 2597,
                'departure_time' => '16:25:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 6078,
                'route_day_id' => 2597,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 6079,
                'route_day_id' => 2597,
                'departure_time' => '16:35:00',
                'arrival_time' => '16:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 6080,
                'route_day_id' => 2597,
                'departure_time' => '16:40:00',
                'arrival_time' => '16:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 6081,
                'route_day_id' => 2597,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 6082,
                'route_day_id' => 2597,
                'departure_time' => '16:50:00',
                'arrival_time' => '16:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 6083,
                'route_day_id' => 2597,
                'departure_time' => '16:55:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 6084,
                'route_day_id' => 2597,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 6085,
                'route_day_id' => 2597,
                'departure_time' => '17:05:00',
                'arrival_time' => '17:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 6086,
                'route_day_id' => 2597,
                'departure_time' => '17:10:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 6087,
                'route_day_id' => 2597,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 6088,
                'route_day_id' => 2597,
                'departure_time' => '17:20:00',
                'arrival_time' => '17:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 6089,
                'route_day_id' => 2597,
                'departure_time' => '17:25:00',
                'arrival_time' => '17:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 6090,
                'route_day_id' => 2597,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 6091,
                'route_day_id' => 2597,
                'departure_time' => '17:35:00',
                'arrival_time' => '17:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 6092,
                'route_day_id' => 2597,
                'departure_time' => '17:40:00',
                'arrival_time' => '17:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 6093,
                'route_day_id' => 2597,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 6094,
                'route_day_id' => 2597,
                'departure_time' => '17:50:00',
                'arrival_time' => '17:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 6095,
                'route_day_id' => 2597,
                'departure_time' => '17:55:00',
                'arrival_time' => '18:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 6096,
                'route_day_id' => 2597,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 6097,
                'route_day_id' => 2597,
                'departure_time' => '18:05:00',
                'arrival_time' => '18:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 6098,
                'route_day_id' => 2597,
                'departure_time' => '18:10:00',
                'arrival_time' => '18:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 6099,
                'route_day_id' => 2597,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 6100,
                'route_day_id' => 2597,
                'departure_time' => '18:20:00',
                'arrival_time' => '18:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 6101,
                'route_day_id' => 2597,
                'departure_time' => '18:25:00',
                'arrival_time' => '18:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 6102,
                'route_day_id' => 2597,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 6103,
                'route_day_id' => 2597,
                'departure_time' => '18:35:00',
                'arrival_time' => '18:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 6104,
                'route_day_id' => 2597,
                'departure_time' => '18:40:00',
                'arrival_time' => '18:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 6105,
                'route_day_id' => 2597,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 6106,
                'route_day_id' => 2597,
                'departure_time' => '18:50:00',
                'arrival_time' => '18:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 6107,
                'route_day_id' => 2597,
                'departure_time' => '18:55:00',
                'arrival_time' => '19:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 6108,
                'route_day_id' => 2597,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 6109,
                'route_day_id' => 2597,
                'departure_time' => '19:05:00',
                'arrival_time' => '19:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 6110,
                'route_day_id' => 2597,
                'departure_time' => '19:10:00',
                'arrival_time' => '19:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 6111,
                'route_day_id' => 2597,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 6112,
                'route_day_id' => 2597,
                'departure_time' => '19:20:00',
                'arrival_time' => '19:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 6113,
                'route_day_id' => 2597,
                'departure_time' => '19:25:00',
                'arrival_time' => '19:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 6114,
                'route_day_id' => 2597,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 6115,
                'route_day_id' => 2597,
                'departure_time' => '19:35:00',
                'arrival_time' => '19:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 6116,
                'route_day_id' => 2597,
                'departure_time' => '19:40:00',
                'arrival_time' => '19:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 6117,
                'route_day_id' => 2597,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 6118,
                'route_day_id' => 2597,
                'departure_time' => '19:50:00',
                'arrival_time' => '19:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 6119,
                'route_day_id' => 2597,
                'departure_time' => '19:55:00',
                'arrival_time' => '20:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 6120,
                'route_day_id' => 2597,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 6121,
                'route_day_id' => 2597,
                'departure_time' => '20:05:00',
                'arrival_time' => '20:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 6122,
                'route_day_id' => 2597,
                'departure_time' => '20:10:00',
                'arrival_time' => '20:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 6123,
                'route_day_id' => 2597,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 6124,
                'route_day_id' => 2597,
                'departure_time' => '20:20:00',
                'arrival_time' => '20:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 6125,
                'route_day_id' => 2597,
                'departure_time' => '20:25:00',
                'arrival_time' => '20:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 6126,
                'route_day_id' => 2597,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 6127,
                'route_day_id' => 2597,
                'departure_time' => '20:35:00',
                'arrival_time' => '20:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 6128,
                'route_day_id' => 2597,
                'departure_time' => '20:40:00',
                'arrival_time' => '20:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 6129,
                'route_day_id' => 2597,
                'departure_time' => '20:45:00',
                'arrival_time' => '20:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 6130,
                'route_day_id' => 2597,
                'departure_time' => '20:50:00',
                'arrival_time' => '20:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 6131,
                'route_day_id' => 2597,
                'departure_time' => '20:55:00',
                'arrival_time' => '21:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 6132,
                'route_day_id' => 2597,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 6133,
                'route_day_id' => 2597,
                'departure_time' => '21:05:00',
                'arrival_time' => '21:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 6134,
                'route_day_id' => 2597,
                'departure_time' => '21:10:00',
                'arrival_time' => '21:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 6135,
                'route_day_id' => 2597,
                'departure_time' => '21:15:00',
                'arrival_time' => '21:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 6136,
                'route_day_id' => 2597,
                'departure_time' => '21:20:00',
                'arrival_time' => '21:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 6137,
                'route_day_id' => 2597,
                'departure_time' => '21:25:00',
                'arrival_time' => '21:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 6138,
                'route_day_id' => 2597,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 6139,
                'route_day_id' => 2597,
                'departure_time' => '21:35:00',
                'arrival_time' => '21:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 6140,
                'route_day_id' => 2597,
                'departure_time' => '21:40:00',
                'arrival_time' => '21:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 6141,
                'route_day_id' => 2597,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 6142,
                'route_day_id' => 2597,
                'departure_time' => '21:55:00',
                'arrival_time' => '22:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 6143,
                'route_day_id' => 2597,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 6144,
                'route_day_id' => 2597,
                'departure_time' => '22:05:00',
                'arrival_time' => '22:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 6145,
                'route_day_id' => 2597,
                'departure_time' => '22:10:00',
                'arrival_time' => '22:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 6146,
                'route_day_id' => 2597,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:23:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 6147,
                'route_day_id' => 2597,
                'departure_time' => '22:25:00',
                'arrival_time' => '22:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 6148,
                'route_day_id' => 2597,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 6149,
                'route_day_id' => 2597,
                'departure_time' => '22:35:00',
                'arrival_time' => '22:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 6150,
                'route_day_id' => 2597,
                'departure_time' => '22:40:00',
                'arrival_time' => '22:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 6151,
                'route_day_id' => 2597,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:53:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 6152,
                'route_day_id' => 2597,
                'departure_time' => '22:55:00',
                'arrival_time' => '23:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 6153,
                'route_day_id' => 2597,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 6154,
                'route_day_id' => 2597,
                'departure_time' => '23:05:00',
                'arrival_time' => '23:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 6155,
                'route_day_id' => 2597,
                'departure_time' => '23:10:00',
                'arrival_time' => '23:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 6156,
                'route_day_id' => 2597,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 6157,
                'route_day_id' => 2597,
                'departure_time' => '23:20:00',
                'arrival_time' => '23:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 6158,
                'route_day_id' => 2597,
                'departure_time' => '23:25:00',
                'arrival_time' => '23:33:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 6159,
                'route_day_id' => 2597,
                'departure_time' => '23:35:00',
                'arrival_time' => '23:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 6160,
                'route_day_id' => 2597,
                'departure_time' => '23:40:00',
                'arrival_time' => '23:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 6161,
                'route_day_id' => 2597,
                'departure_time' => '23:45:00',
                'arrival_time' => '23:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 6162,
                'route_day_id' => 2597,
                'departure_time' => '23:50:00',
                'arrival_time' => '23:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 6163,
                'route_day_id' => 2597,
                'departure_time' => '23:55:00',
                'arrival_time' => '23:59:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 6164,
                'route_day_id' => 2598,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 6165,
                'route_day_id' => 2599,
                'departure_time' => '06:45:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 6166,
                'route_day_id' => 2600,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 6167,
                'route_day_id' => 2601,
                'departure_time' => '07:50:00',
                'arrival_time' => '07:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 6168,
                'route_day_id' => 2602,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 6169,
                'route_day_id' => 2603,
                'departure_time' => '08:20:00',
                'arrival_time' => '08:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 6170,
                'route_day_id' => 2604,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 6171,
                'route_day_id' => 2605,
                'departure_time' => '14:50:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 6172,
                'route_day_id' => 2606,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 6173,
                'route_day_id' => 2607,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 6174,
                'route_day_id' => 2608,
                'departure_time' => '15:50:00',
                'arrival_time' => '16:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 6175,
                'route_day_id' => 2609,
                'departure_time' => '16:25:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 6184,
                'route_day_id' => 2618,
                'departure_time' => '00:00:00',
                'arrival_time' => '00:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 6185,
                'route_day_id' => 2619,
                'departure_time' => '00:01:00',
                'arrival_time' => '00:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 6186,
                'route_day_id' => 2620,
                'departure_time' => '00:14:00',
                'arrival_time' => '00:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 6187,
                'route_day_id' => 2621,
                'departure_time' => '00:16:00',
                'arrival_time' => '00:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 6188,
                'route_day_id' => 2622,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 6189,
                'route_day_id' => 2623,
                'departure_time' => '00:31:00',
                'arrival_time' => '00:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 6190,
                'route_day_id' => 2624,
                'departure_time' => '00:44:00',
                'arrival_time' => '00:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 6191,
                'route_day_id' => 2625,
                'departure_time' => '00:46:00',
                'arrival_time' => '01:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 6192,
                'route_day_id' => 2626,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 6193,
                'route_day_id' => 2627,
                'departure_time' => '01:01:00',
                'arrival_time' => '01:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 6194,
                'route_day_id' => 2628,
                'departure_time' => '01:14:00',
                'arrival_time' => '01:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 6195,
                'route_day_id' => 2629,
                'departure_time' => '01:16:00',
                'arrival_time' => '01:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 6196,
                'route_day_id' => 2630,
                'departure_time' => '01:30:00',
                'arrival_time' => '01:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 6197,
                'route_day_id' => 2631,
                'departure_time' => '01:31:00',
                'arrival_time' => '01:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 6198,
                'route_day_id' => 2632,
                'departure_time' => '01:44:00',
                'arrival_time' => '01:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 6199,
                'route_day_id' => 2633,
                'departure_time' => '01:46:00',
                'arrival_time' => '02:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 6200,
                'route_day_id' => 2634,
                'departure_time' => '02:00:00',
                'arrival_time' => '02:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 6201,
                'route_day_id' => 2635,
                'departure_time' => '02:01:00',
                'arrival_time' => '02:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 6202,
                'route_day_id' => 2636,
                'departure_time' => '02:14:00',
                'arrival_time' => '02:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 6203,
                'route_day_id' => 2637,
                'departure_time' => '02:16:00',
                'arrival_time' => '02:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 6204,
                'route_day_id' => 2638,
                'departure_time' => '02:30:00',
                'arrival_time' => '02:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 6205,
                'route_day_id' => 2639,
                'departure_time' => '02:31:00',
                'arrival_time' => '02:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 6206,
                'route_day_id' => 2640,
                'departure_time' => '02:44:00',
                'arrival_time' => '02:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 6207,
                'route_day_id' => 2641,
                'departure_time' => '02:46:00',
                'arrival_time' => '03:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 6208,
                'route_day_id' => 2642,
                'departure_time' => '03:00:00',
                'arrival_time' => '03:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 6209,
                'route_day_id' => 2643,
                'departure_time' => '03:01:00',
                'arrival_time' => '03:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 6210,
                'route_day_id' => 2644,
                'departure_time' => '03:14:00',
                'arrival_time' => '03:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 6211,
                'route_day_id' => 2645,
                'departure_time' => '03:16:00',
                'arrival_time' => '03:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 6212,
                'route_day_id' => 2646,
                'departure_time' => '03:30:00',
                'arrival_time' => '03:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 6213,
                'route_day_id' => 2647,
                'departure_time' => '03:31:00',
                'arrival_time' => '03:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 6214,
                'route_day_id' => 2648,
                'departure_time' => '03:44:00',
                'arrival_time' => '03:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 6215,
                'route_day_id' => 2649,
                'departure_time' => '03:46:00',
                'arrival_time' => '04:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 6216,
                'route_day_id' => 2650,
                'departure_time' => '04:00:00',
                'arrival_time' => '04:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 6217,
                'route_day_id' => 2651,
                'departure_time' => '04:01:00',
                'arrival_time' => '04:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 6218,
                'route_day_id' => 2652,
                'departure_time' => '04:14:00',
                'arrival_time' => '04:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 6219,
                'route_day_id' => 2653,
                'departure_time' => '04:16:00',
                'arrival_time' => '04:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 6220,
                'route_day_id' => 2654,
                'departure_time' => '04:30:00',
                'arrival_time' => '04:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 6221,
                'route_day_id' => 2655,
                'departure_time' => '04:31:00',
                'arrival_time' => '04:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 6222,
                'route_day_id' => 2656,
                'departure_time' => '04:44:00',
                'arrival_time' => '04:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 6223,
                'route_day_id' => 2657,
                'departure_time' => '04:46:00',
                'arrival_time' => '05:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 6224,
                'route_day_id' => 2658,
                'departure_time' => '05:00:00',
                'arrival_time' => '05:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 6225,
                'route_day_id' => 2659,
                'departure_time' => '05:01:00',
                'arrival_time' => '05:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 6226,
                'route_day_id' => 2660,
                'departure_time' => '05:14:00',
                'arrival_time' => '05:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 6227,
                'route_day_id' => 2661,
                'departure_time' => '05:16:00',
                'arrival_time' => '05:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 6228,
                'route_day_id' => 2662,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 6229,
                'route_day_id' => 2663,
                'departure_time' => '05:31:00',
                'arrival_time' => '05:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 6230,
                'route_day_id' => 2664,
                'departure_time' => '05:44:00',
                'arrival_time' => '05:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 6231,
                'route_day_id' => 2665,
                'departure_time' => '05:46:00',
                'arrival_time' => '06:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 6232,
                'route_day_id' => 2666,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 6233,
                'route_day_id' => 2667,
                'departure_time' => '06:01:00',
                'arrival_time' => '06:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 6234,
                'route_day_id' => 2668,
                'departure_time' => '06:14:00',
                'arrival_time' => '06:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 6235,
                'route_day_id' => 2669,
                'departure_time' => '06:16:00',
                'arrival_time' => '06:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 6236,
                'route_day_id' => 2670,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 6237,
                'route_day_id' => 2671,
                'departure_time' => '06:31:00',
                'arrival_time' => '06:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 6238,
                'route_day_id' => 2672,
                'departure_time' => '06:44:00',
                'arrival_time' => '06:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 6239,
                'route_day_id' => 2673,
                'departure_time' => '06:46:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 6240,
                'route_day_id' => 2674,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 6241,
                'route_day_id' => 2675,
                'departure_time' => '07:01:00',
                'arrival_time' => '07:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 6242,
                'route_day_id' => 2676,
                'departure_time' => '07:14:00',
                'arrival_time' => '07:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 6243,
                'route_day_id' => 2677,
                'departure_time' => '07:16:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 6244,
                'route_day_id' => 2678,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 6245,
                'route_day_id' => 2679,
                'departure_time' => '07:31:00',
                'arrival_time' => '07:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 6246,
                'route_day_id' => 2680,
                'departure_time' => '07:44:00',
                'arrival_time' => '07:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 6247,
                'route_day_id' => 2681,
                'departure_time' => '07:46:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 6248,
                'route_day_id' => 2682,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 6249,
                'route_day_id' => 2683,
                'departure_time' => '08:01:00',
                'arrival_time' => '08:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 6250,
                'route_day_id' => 2684,
                'departure_time' => '08:14:00',
                'arrival_time' => '08:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 6251,
                'route_day_id' => 2685,
                'departure_time' => '08:16:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 6252,
                'route_day_id' => 2686,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 6253,
                'route_day_id' => 2687,
                'departure_time' => '08:31:00',
                'arrival_time' => '08:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 6254,
                'route_day_id' => 2688,
                'departure_time' => '08:44:00',
                'arrival_time' => '08:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 6255,
                'route_day_id' => 2689,
                'departure_time' => '08:46:00',
                'arrival_time' => '09:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 6256,
                'route_day_id' => 2690,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 6257,
                'route_day_id' => 2691,
                'departure_time' => '09:01:00',
                'arrival_time' => '09:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 6258,
                'route_day_id' => 2692,
                'departure_time' => '09:14:00',
                'arrival_time' => '09:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 6259,
                'route_day_id' => 2693,
                'departure_time' => '09:16:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 6260,
                'route_day_id' => 2694,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 6261,
                'route_day_id' => 2695,
                'departure_time' => '09:31:00',
                'arrival_time' => '09:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 6262,
                'route_day_id' => 2696,
                'departure_time' => '09:44:00',
                'arrival_time' => '09:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 6263,
                'route_day_id' => 2697,
                'departure_time' => '09:46:00',
                'arrival_time' => '10:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 6264,
                'route_day_id' => 2698,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 6265,
                'route_day_id' => 2699,
                'departure_time' => '10:01:00',
                'arrival_time' => '10:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 6266,
                'route_day_id' => 2700,
                'departure_time' => '10:14:00',
                'arrival_time' => '10:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 6267,
                'route_day_id' => 2701,
                'departure_time' => '10:16:00',
                'arrival_time' => '10:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 6268,
                'route_day_id' => 2702,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 6269,
                'route_day_id' => 2703,
                'departure_time' => '10:31:00',
                'arrival_time' => '10:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 6270,
                'route_day_id' => 2704,
                'departure_time' => '10:44:00',
                'arrival_time' => '10:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 6271,
                'route_day_id' => 2705,
                'departure_time' => '10:46:00',
                'arrival_time' => '11:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 6272,
                'route_day_id' => 2706,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 6273,
                'route_day_id' => 2707,
                'departure_time' => '11:01:00',
                'arrival_time' => '11:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 6274,
                'route_day_id' => 2708,
                'departure_time' => '11:14:00',
                'arrival_time' => '11:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 6275,
                'route_day_id' => 2709,
                'departure_time' => '11:16:00',
                'arrival_time' => '11:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 6276,
                'route_day_id' => 2710,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 6277,
                'route_day_id' => 2711,
                'departure_time' => '11:31:00',
                'arrival_time' => '11:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 6278,
                'route_day_id' => 2712,
                'departure_time' => '11:44:00',
                'arrival_time' => '11:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 6279,
                'route_day_id' => 2713,
                'departure_time' => '11:46:00',
                'arrival_time' => '12:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 6280,
                'route_day_id' => 2714,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 6281,
                'route_day_id' => 2715,
                'departure_time' => '12:01:00',
                'arrival_time' => '12:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 6282,
                'route_day_id' => 2716,
                'departure_time' => '12:14:00',
                'arrival_time' => '12:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 6283,
                'route_day_id' => 2717,
                'departure_time' => '12:16:00',
                'arrival_time' => '12:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 6284,
                'route_day_id' => 2718,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 6285,
                'route_day_id' => 2719,
                'departure_time' => '12:31:00',
                'arrival_time' => '12:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 6286,
                'route_day_id' => 2720,
                'departure_time' => '12:44:00',
                'arrival_time' => '12:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 6287,
                'route_day_id' => 2721,
                'departure_time' => '12:46:00',
                'arrival_time' => '13:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 6288,
                'route_day_id' => 2722,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 6289,
                'route_day_id' => 2723,
                'departure_time' => '13:01:00',
                'arrival_time' => '13:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 6290,
                'route_day_id' => 2724,
                'departure_time' => '13:14:00',
                'arrival_time' => '13:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 6291,
                'route_day_id' => 2725,
                'departure_time' => '13:16:00',
                'arrival_time' => '13:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 6292,
                'route_day_id' => 2726,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 6293,
                'route_day_id' => 2727,
                'departure_time' => '13:31:00',
                'arrival_time' => '13:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 6294,
                'route_day_id' => 2728,
                'departure_time' => '13:44:00',
                'arrival_time' => '13:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 6295,
                'route_day_id' => 2729,
                'departure_time' => '13:46:00',
                'arrival_time' => '14:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 6296,
                'route_day_id' => 2730,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 6297,
                'route_day_id' => 2731,
                'departure_time' => '14:01:00',
                'arrival_time' => '14:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 6298,
                'route_day_id' => 2732,
                'departure_time' => '14:14:00',
                'arrival_time' => '14:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 6299,
                'route_day_id' => 2733,
                'departure_time' => '14:16:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 6300,
                'route_day_id' => 2734,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 6301,
                'route_day_id' => 2735,
                'departure_time' => '14:31:00',
                'arrival_time' => '14:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 6302,
                'route_day_id' => 2736,
                'departure_time' => '14:44:00',
                'arrival_time' => '14:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 6303,
                'route_day_id' => 2737,
                'departure_time' => '14:46:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 6304,
                'route_day_id' => 2738,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 6305,
                'route_day_id' => 2739,
                'departure_time' => '15:01:00',
                'arrival_time' => '15:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 6306,
                'route_day_id' => 2740,
                'departure_time' => '15:14:00',
                'arrival_time' => '15:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 6307,
                'route_day_id' => 2741,
                'departure_time' => '15:16:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 6308,
                'route_day_id' => 2742,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 6309,
                'route_day_id' => 2743,
                'departure_time' => '15:31:00',
                'arrival_time' => '15:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 6310,
                'route_day_id' => 2744,
                'departure_time' => '15:44:00',
                'arrival_time' => '15:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 6311,
                'route_day_id' => 2745,
                'departure_time' => '15:46:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 6312,
                'route_day_id' => 2746,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 6313,
                'route_day_id' => 2747,
                'departure_time' => '16:01:00',
                'arrival_time' => '16:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 6314,
                'route_day_id' => 2748,
                'departure_time' => '16:14:00',
                'arrival_time' => '16:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 6315,
                'route_day_id' => 2749,
                'departure_time' => '16:16:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 6316,
                'route_day_id' => 2750,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 6317,
                'route_day_id' => 2751,
                'departure_time' => '16:31:00',
                'arrival_time' => '16:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 6318,
                'route_day_id' => 2752,
                'departure_time' => '16:44:00',
                'arrival_time' => '16:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 6319,
                'route_day_id' => 2753,
                'departure_time' => '16:46:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 6320,
                'route_day_id' => 2754,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 6321,
                'route_day_id' => 2755,
                'departure_time' => '17:01:00',
                'arrival_time' => '17:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 6322,
                'route_day_id' => 2756,
                'departure_time' => '17:14:00',
                'arrival_time' => '17:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 6323,
                'route_day_id' => 2757,
                'departure_time' => '17:16:00',
                'arrival_time' => '17:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 6324,
                'route_day_id' => 2758,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 6325,
                'route_day_id' => 2759,
                'departure_time' => '17:31:00',
                'arrival_time' => '17:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 6326,
                'route_day_id' => 2760,
                'departure_time' => '17:44:00',
                'arrival_time' => '17:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 6327,
                'route_day_id' => 2761,
                'departure_time' => '17:46:00',
                'arrival_time' => '18:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 6328,
                'route_day_id' => 2762,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 6329,
                'route_day_id' => 2763,
                'departure_time' => '18:01:00',
                'arrival_time' => '18:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 6330,
                'route_day_id' => 2764,
                'departure_time' => '18:14:00',
                'arrival_time' => '18:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 6331,
                'route_day_id' => 2765,
                'departure_time' => '18:16:00',
                'arrival_time' => '18:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 6332,
                'route_day_id' => 2766,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 6333,
                'route_day_id' => 2767,
                'departure_time' => '18:31:00',
                'arrival_time' => '18:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 6334,
                'route_day_id' => 2768,
                'departure_time' => '18:44:00',
                'arrival_time' => '18:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 6335,
                'route_day_id' => 2769,
                'departure_time' => '18:46:00',
                'arrival_time' => '19:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 6336,
                'route_day_id' => 2770,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 6337,
                'route_day_id' => 2771,
                'departure_time' => '19:01:00',
                'arrival_time' => '19:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 6338,
                'route_day_id' => 2772,
                'departure_time' => '19:14:00',
                'arrival_time' => '19:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 6339,
                'route_day_id' => 2773,
                'departure_time' => '19:16:00',
                'arrival_time' => '19:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 6340,
                'route_day_id' => 2774,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 6341,
                'route_day_id' => 2775,
                'departure_time' => '19:31:00',
                'arrival_time' => '19:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 6342,
                'route_day_id' => 2776,
                'departure_time' => '19:44:00',
                'arrival_time' => '19:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 6343,
                'route_day_id' => 2777,
                'departure_time' => '19:46:00',
                'arrival_time' => '20:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 6344,
                'route_day_id' => 2778,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 6345,
                'route_day_id' => 2779,
                'departure_time' => '20:01:00',
                'arrival_time' => '20:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 6346,
                'route_day_id' => 2780,
                'departure_time' => '20:14:00',
                'arrival_time' => '20:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 6347,
                'route_day_id' => 2781,
                'departure_time' => '20:16:00',
                'arrival_time' => '20:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 6348,
                'route_day_id' => 2782,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 6349,
                'route_day_id' => 2783,
                'departure_time' => '20:31:00',
                'arrival_time' => '20:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 6350,
                'route_day_id' => 2784,
                'departure_time' => '20:44:00',
                'arrival_time' => '20:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 6351,
                'route_day_id' => 2785,
                'departure_time' => '20:46:00',
                'arrival_time' => '21:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 6352,
                'route_day_id' => 2786,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 6353,
                'route_day_id' => 2787,
                'departure_time' => '21:01:00',
                'arrival_time' => '21:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 6354,
                'route_day_id' => 2788,
                'departure_time' => '21:14:00',
                'arrival_time' => '21:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 6355,
                'route_day_id' => 2789,
                'departure_time' => '21:16:00',
                'arrival_time' => '21:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 6356,
                'route_day_id' => 2790,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 6357,
                'route_day_id' => 2791,
                'departure_time' => '21:31:00',
                'arrival_time' => '21:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 6358,
                'route_day_id' => 2792,
                'departure_time' => '21:44:00',
                'arrival_time' => '21:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 6359,
                'route_day_id' => 2793,
                'departure_time' => '21:46:00',
                'arrival_time' => '22:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 6360,
                'route_day_id' => 2794,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 6361,
                'route_day_id' => 2795,
                'departure_time' => '22:01:00',
                'arrival_time' => '22:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 6362,
                'route_day_id' => 2796,
                'departure_time' => '22:14:00',
                'arrival_time' => '22:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 6363,
                'route_day_id' => 2797,
                'departure_time' => '22:16:00',
                'arrival_time' => '22:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 6364,
                'route_day_id' => 2798,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:31:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 6365,
                'route_day_id' => 2799,
                'departure_time' => '22:31:00',
                'arrival_time' => '22:44:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 6366,
                'route_day_id' => 2800,
                'departure_time' => '22:44:00',
                'arrival_time' => '22:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 6367,
                'route_day_id' => 2801,
                'departure_time' => '22:46:00',
                'arrival_time' => '23:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 6368,
                'route_day_id' => 2802,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:01:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 6369,
                'route_day_id' => 2803,
                'departure_time' => '23:01:00',
                'arrival_time' => '23:14:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 6370,
                'route_day_id' => 2804,
                'departure_time' => '23:14:00',
                'arrival_time' => '23:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 6371,
                'route_day_id' => 2805,
                'departure_time' => '23:16:00',
                'arrival_time' => '23:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 6372,
                'route_day_id' => 2806,
                'departure_time' => '00:00:00',
                'arrival_time' => '00:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 6373,
                'route_day_id' => 2807,
                'departure_time' => '00:02:00',
                'arrival_time' => '00:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 6374,
                'route_day_id' => 2808,
                'departure_time' => '00:15:00',
                'arrival_time' => '00:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 6375,
                'route_day_id' => 2809,
                'departure_time' => '00:16:00',
                'arrival_time' => '00:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 6376,
                'route_day_id' => 2810,
                'departure_time' => '00:30:00',
                'arrival_time' => '00:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 6377,
                'route_day_id' => 2811,
                'departure_time' => '00:32:00',
                'arrival_time' => '00:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 6378,
                'route_day_id' => 2812,
                'departure_time' => '00:45:00',
                'arrival_time' => '00:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 6379,
                'route_day_id' => 2813,
                'departure_time' => '00:46:00',
                'arrival_time' => '01:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 6380,
                'route_day_id' => 2814,
                'departure_time' => '01:00:00',
                'arrival_time' => '01:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 6381,
                'route_day_id' => 2815,
                'departure_time' => '01:02:00',
                'arrival_time' => '01:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 6382,
                'route_day_id' => 2816,
                'departure_time' => '01:15:00',
                'arrival_time' => '01:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 6383,
                'route_day_id' => 2817,
                'departure_time' => '01:16:00',
                'arrival_time' => '01:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 6384,
                'route_day_id' => 2818,
                'departure_time' => '01:30:00',
                'arrival_time' => '01:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 6385,
                'route_day_id' => 2819,
                'departure_time' => '01:32:00',
                'arrival_time' => '01:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 6386,
                'route_day_id' => 2820,
                'departure_time' => '01:45:00',
                'arrival_time' => '01:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 6387,
                'route_day_id' => 2821,
                'departure_time' => '01:46:00',
                'arrival_time' => '02:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 6388,
                'route_day_id' => 2822,
                'departure_time' => '02:00:00',
                'arrival_time' => '02:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 6389,
                'route_day_id' => 2823,
                'departure_time' => '02:02:00',
                'arrival_time' => '02:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 6390,
                'route_day_id' => 2824,
                'departure_time' => '02:15:00',
                'arrival_time' => '02:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 6391,
                'route_day_id' => 2825,
                'departure_time' => '02:16:00',
                'arrival_time' => '02:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 6392,
                'route_day_id' => 2826,
                'departure_time' => '02:30:00',
                'arrival_time' => '02:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 6393,
                'route_day_id' => 2827,
                'departure_time' => '02:32:00',
                'arrival_time' => '02:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 6394,
                'route_day_id' => 2828,
                'departure_time' => '02:45:00',
                'arrival_time' => '02:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 6395,
                'route_day_id' => 2829,
                'departure_time' => '02:46:00',
                'arrival_time' => '03:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 6396,
                'route_day_id' => 2830,
                'departure_time' => '03:00:00',
                'arrival_time' => '03:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 6397,
                'route_day_id' => 2831,
                'departure_time' => '03:02:00',
                'arrival_time' => '03:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 6398,
                'route_day_id' => 2832,
                'departure_time' => '03:15:00',
                'arrival_time' => '03:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 6399,
                'route_day_id' => 2833,
                'departure_time' => '03:16:00',
                'arrival_time' => '03:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 6400,
                'route_day_id' => 2834,
                'departure_time' => '03:30:00',
                'arrival_time' => '03:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 6401,
                'route_day_id' => 2835,
                'departure_time' => '03:32:00',
                'arrival_time' => '03:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 6402,
                'route_day_id' => 2836,
                'departure_time' => '03:45:00',
                'arrival_time' => '03:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 6403,
                'route_day_id' => 2837,
                'departure_time' => '03:46:00',
                'arrival_time' => '04:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 6404,
                'route_day_id' => 2838,
                'departure_time' => '04:00:00',
                'arrival_time' => '04:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 6405,
                'route_day_id' => 2839,
                'departure_time' => '04:02:00',
                'arrival_time' => '04:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 6406,
                'route_day_id' => 2840,
                'departure_time' => '04:15:00',
                'arrival_time' => '04:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 6407,
                'route_day_id' => 2841,
                'departure_time' => '04:16:00',
                'arrival_time' => '04:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 6408,
                'route_day_id' => 2842,
                'departure_time' => '04:30:00',
                'arrival_time' => '04:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 6409,
                'route_day_id' => 2843,
                'departure_time' => '04:32:00',
                'arrival_time' => '04:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 6410,
                'route_day_id' => 2844,
                'departure_time' => '04:45:00',
                'arrival_time' => '04:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 6411,
                'route_day_id' => 2845,
                'departure_time' => '04:46:00',
                'arrival_time' => '05:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 6412,
                'route_day_id' => 2846,
                'departure_time' => '05:00:00',
                'arrival_time' => '05:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 6413,
                'route_day_id' => 2847,
                'departure_time' => '05:02:00',
                'arrival_time' => '05:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 6414,
                'route_day_id' => 2848,
                'departure_time' => '05:15:00',
                'arrival_time' => '05:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 6415,
                'route_day_id' => 2849,
                'departure_time' => '05:16:00',
                'arrival_time' => '05:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 6416,
                'route_day_id' => 2850,
                'departure_time' => '05:30:00',
                'arrival_time' => '05:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 6417,
                'route_day_id' => 2851,
                'departure_time' => '05:32:00',
                'arrival_time' => '05:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 6418,
                'route_day_id' => 2852,
                'departure_time' => '05:45:00',
                'arrival_time' => '05:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 6419,
                'route_day_id' => 2853,
                'departure_time' => '05:46:00',
                'arrival_time' => '06:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 6420,
                'route_day_id' => 2854,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 6421,
                'route_day_id' => 2855,
                'departure_time' => '06:02:00',
                'arrival_time' => '06:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 6422,
                'route_day_id' => 2856,
                'departure_time' => '06:15:00',
                'arrival_time' => '06:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 6423,
                'route_day_id' => 2857,
                'departure_time' => '06:16:00',
                'arrival_time' => '06:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 6424,
                'route_day_id' => 2858,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 6425,
                'route_day_id' => 2859,
                'departure_time' => '06:32:00',
                'arrival_time' => '06:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 6426,
                'route_day_id' => 2860,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 6427,
                'route_day_id' => 2861,
                'departure_time' => '06:46:00',
                'arrival_time' => '07:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 6428,
                'route_day_id' => 2862,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 6429,
                'route_day_id' => 2863,
                'departure_time' => '07:02:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 6430,
                'route_day_id' => 2864,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 6431,
                'route_day_id' => 2865,
                'departure_time' => '07:16:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 6432,
                'route_day_id' => 2866,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 6433,
                'route_day_id' => 2867,
                'departure_time' => '07:32:00',
                'arrival_time' => '07:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 6434,
                'route_day_id' => 2868,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 6435,
                'route_day_id' => 2869,
                'departure_time' => '07:46:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 6436,
                'route_day_id' => 2870,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 6437,
                'route_day_id' => 2871,
                'departure_time' => '08:02:00',
                'arrival_time' => '08:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 6438,
                'route_day_id' => 2872,
                'departure_time' => '08:15:00',
                'arrival_time' => '08:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 6439,
                'route_day_id' => 2873,
                'departure_time' => '08:16:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 6440,
                'route_day_id' => 2874,
                'departure_time' => '08:30:00',
                'arrival_time' => '08:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 6441,
                'route_day_id' => 2875,
                'departure_time' => '08:32:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 6442,
                'route_day_id' => 2876,
                'departure_time' => '08:45:00',
                'arrival_time' => '08:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 6443,
                'route_day_id' => 2877,
                'departure_time' => '08:46:00',
                'arrival_time' => '09:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 6444,
                'route_day_id' => 2878,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 6445,
                'route_day_id' => 2879,
                'departure_time' => '09:02:00',
                'arrival_time' => '09:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 6446,
                'route_day_id' => 2880,
                'departure_time' => '09:15:00',
                'arrival_time' => '09:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 6447,
                'route_day_id' => 2881,
                'departure_time' => '09:16:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 6448,
                'route_day_id' => 2882,
                'departure_time' => '09:30:00',
                'arrival_time' => '09:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 6449,
                'route_day_id' => 2883,
                'departure_time' => '09:32:00',
                'arrival_time' => '09:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 6450,
                'route_day_id' => 2884,
                'departure_time' => '09:45:00',
                'arrival_time' => '09:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 6451,
                'route_day_id' => 2885,
                'departure_time' => '09:46:00',
                'arrival_time' => '10:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 6452,
                'route_day_id' => 2886,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 6453,
                'route_day_id' => 2887,
                'departure_time' => '10:02:00',
                'arrival_time' => '10:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 6454,
                'route_day_id' => 2888,
                'departure_time' => '10:15:00',
                'arrival_time' => '10:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 6455,
                'route_day_id' => 2889,
                'departure_time' => '10:16:00',
                'arrival_time' => '10:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 6456,
                'route_day_id' => 2890,
                'departure_time' => '10:30:00',
                'arrival_time' => '10:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 6457,
                'route_day_id' => 2891,
                'departure_time' => '10:32:00',
                'arrival_time' => '10:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 6458,
                'route_day_id' => 2892,
                'departure_time' => '10:45:00',
                'arrival_time' => '10:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 6459,
                'route_day_id' => 2893,
                'departure_time' => '10:46:00',
                'arrival_time' => '11:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 6460,
                'route_day_id' => 2894,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 6461,
                'route_day_id' => 2895,
                'departure_time' => '11:02:00',
                'arrival_time' => '11:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 6462,
                'route_day_id' => 2896,
                'departure_time' => '11:15:00',
                'arrival_time' => '11:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 6463,
                'route_day_id' => 2897,
                'departure_time' => '11:16:00',
                'arrival_time' => '11:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 6464,
                'route_day_id' => 2898,
                'departure_time' => '11:30:00',
                'arrival_time' => '11:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 6465,
                'route_day_id' => 2899,
                'departure_time' => '11:32:00',
                'arrival_time' => '11:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 6466,
                'route_day_id' => 2900,
                'departure_time' => '11:45:00',
                'arrival_time' => '11:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 6467,
                'route_day_id' => 2901,
                'departure_time' => '11:46:00',
                'arrival_time' => '12:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 6468,
                'route_day_id' => 2902,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 6469,
                'route_day_id' => 2903,
                'departure_time' => '12:02:00',
                'arrival_time' => '12:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 6470,
                'route_day_id' => 2904,
                'departure_time' => '12:15:00',
                'arrival_time' => '12:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 6471,
                'route_day_id' => 2905,
                'departure_time' => '12:16:00',
                'arrival_time' => '12:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 6472,
                'route_day_id' => 2906,
                'departure_time' => '12:30:00',
                'arrival_time' => '12:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 6473,
                'route_day_id' => 2907,
                'departure_time' => '12:32:00',
                'arrival_time' => '12:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 6474,
                'route_day_id' => 2908,
                'departure_time' => '12:45:00',
                'arrival_time' => '12:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 6475,
                'route_day_id' => 2909,
                'departure_time' => '12:46:00',
                'arrival_time' => '13:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 6476,
                'route_day_id' => 2910,
                'departure_time' => '13:00:00',
                'arrival_time' => '13:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 6477,
                'route_day_id' => 2911,
                'departure_time' => '13:02:00',
                'arrival_time' => '13:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 6478,
                'route_day_id' => 2912,
                'departure_time' => '13:15:00',
                'arrival_time' => '13:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 6479,
                'route_day_id' => 2913,
                'departure_time' => '13:16:00',
                'arrival_time' => '13:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 6480,
                'route_day_id' => 2914,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 6481,
                'route_day_id' => 2915,
                'departure_time' => '13:32:00',
                'arrival_time' => '13:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 6482,
                'route_day_id' => 2916,
                'departure_time' => '13:45:00',
                'arrival_time' => '13:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 6483,
                'route_day_id' => 2917,
                'departure_time' => '13:46:00',
                'arrival_time' => '14:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 6484,
                'route_day_id' => 2918,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 6485,
                'route_day_id' => 2919,
                'departure_time' => '14:02:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 6486,
                'route_day_id' => 2920,
                'departure_time' => '14:15:00',
                'arrival_time' => '14:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 6487,
                'route_day_id' => 2921,
                'departure_time' => '14:16:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 6488,
                'route_day_id' => 2922,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 6489,
                'route_day_id' => 2923,
                'departure_time' => '14:32:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 6490,
                'route_day_id' => 2924,
                'departure_time' => '14:45:00',
                'arrival_time' => '14:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 6491,
                'route_day_id' => 2925,
                'departure_time' => '14:46:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 6492,
                'route_day_id' => 2926,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 6493,
                'route_day_id' => 2927,
                'departure_time' => '15:02:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 6494,
                'route_day_id' => 2928,
                'departure_time' => '15:15:00',
                'arrival_time' => '15:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 6495,
                'route_day_id' => 2929,
                'departure_time' => '15:16:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 6496,
                'route_day_id' => 2930,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 6497,
                'route_day_id' => 2931,
                'departure_time' => '15:32:00',
                'arrival_time' => '15:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 6498,
                'route_day_id' => 2932,
                'departure_time' => '15:45:00',
                'arrival_time' => '15:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 6499,
                'route_day_id' => 2933,
                'departure_time' => '15:46:00',
                'arrival_time' => '16:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 6500,
                'route_day_id' => 2934,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 6501,
                'route_day_id' => 2935,
                'departure_time' => '16:02:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 6502,
                'route_day_id' => 2936,
                'departure_time' => '16:15:00',
                'arrival_time' => '16:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_times')->insert(array (
            0 => 
            array (
                'id' => 6503,
                'route_day_id' => 2937,
                'departure_time' => '16:16:00',
                'arrival_time' => '16:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 6504,
                'route_day_id' => 2938,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 6505,
                'route_day_id' => 2939,
                'departure_time' => '16:32:00',
                'arrival_time' => '16:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 6506,
                'route_day_id' => 2940,
                'departure_time' => '16:45:00',
                'arrival_time' => '16:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6507,
                'route_day_id' => 2941,
                'departure_time' => '16:46:00',
                'arrival_time' => '17:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6508,
                'route_day_id' => 2942,
                'departure_time' => '17:00:00',
                'arrival_time' => '17:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 6509,
                'route_day_id' => 2943,
                'departure_time' => '17:02:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 6510,
                'route_day_id' => 2944,
                'departure_time' => '17:15:00',
                'arrival_time' => '17:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 6511,
                'route_day_id' => 2945,
                'departure_time' => '17:16:00',
                'arrival_time' => '17:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 6512,
                'route_day_id' => 2946,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 6513,
                'route_day_id' => 2947,
                'departure_time' => '17:32:00',
                'arrival_time' => '17:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 6514,
                'route_day_id' => 2948,
                'departure_time' => '17:45:00',
                'arrival_time' => '17:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 6515,
                'route_day_id' => 2949,
                'departure_time' => '17:46:00',
                'arrival_time' => '18:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 6516,
                'route_day_id' => 2950,
                'departure_time' => '18:00:00',
                'arrival_time' => '18:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 6517,
                'route_day_id' => 2951,
                'departure_time' => '18:02:00',
                'arrival_time' => '18:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 6518,
                'route_day_id' => 2952,
                'departure_time' => '18:15:00',
                'arrival_time' => '18:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 6519,
                'route_day_id' => 2953,
                'departure_time' => '18:16:00',
                'arrival_time' => '18:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 6520,
                'route_day_id' => 2954,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 6521,
                'route_day_id' => 2955,
                'departure_time' => '18:32:00',
                'arrival_time' => '18:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 6522,
                'route_day_id' => 2956,
                'departure_time' => '18:45:00',
                'arrival_time' => '18:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 6523,
                'route_day_id' => 2957,
                'departure_time' => '18:46:00',
                'arrival_time' => '19:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 6524,
                'route_day_id' => 2958,
                'departure_time' => '19:00:00',
                'arrival_time' => '19:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 6525,
                'route_day_id' => 2959,
                'departure_time' => '19:02:00',
                'arrival_time' => '19:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 6526,
                'route_day_id' => 2960,
                'departure_time' => '19:15:00',
                'arrival_time' => '19:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 6527,
                'route_day_id' => 2961,
                'departure_time' => '19:16:00',
                'arrival_time' => '19:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 6528,
                'route_day_id' => 2962,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 6529,
                'route_day_id' => 2963,
                'departure_time' => '19:32:00',
                'arrival_time' => '19:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 6530,
                'route_day_id' => 2964,
                'departure_time' => '19:45:00',
                'arrival_time' => '19:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 6531,
                'route_day_id' => 2965,
                'departure_time' => '19:46:00',
                'arrival_time' => '20:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 6532,
                'route_day_id' => 2966,
                'departure_time' => '20:00:00',
                'arrival_time' => '20:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 6533,
                'route_day_id' => 2967,
                'departure_time' => '20:02:00',
                'arrival_time' => '20:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 6534,
                'route_day_id' => 2968,
                'departure_time' => '20:15:00',
                'arrival_time' => '20:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 6535,
                'route_day_id' => 2969,
                'departure_time' => '20:16:00',
                'arrival_time' => '20:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 6536,
                'route_day_id' => 2970,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 6537,
                'route_day_id' => 2971,
                'departure_time' => '20:32:00',
                'arrival_time' => '20:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 6538,
                'route_day_id' => 2972,
                'departure_time' => '20:45:00',
                'arrival_time' => '20:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 6539,
                'route_day_id' => 2973,
                'departure_time' => '20:46:00',
                'arrival_time' => '21:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 6540,
                'route_day_id' => 2974,
                'departure_time' => '21:00:00',
                'arrival_time' => '21:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 6541,
                'route_day_id' => 2975,
                'departure_time' => '21:02:00',
                'arrival_time' => '21:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 6542,
                'route_day_id' => 2976,
                'departure_time' => '21:15:00',
                'arrival_time' => '21:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 6543,
                'route_day_id' => 2977,
                'departure_time' => '21:16:00',
                'arrival_time' => '21:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 6544,
                'route_day_id' => 2978,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 6545,
                'route_day_id' => 2979,
                'departure_time' => '21:32:00',
                'arrival_time' => '21:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 6546,
                'route_day_id' => 2980,
                'departure_time' => '21:45:00',
                'arrival_time' => '21:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 6547,
                'route_day_id' => 2981,
                'departure_time' => '21:46:00',
                'arrival_time' => '22:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 6548,
                'route_day_id' => 2982,
                'departure_time' => '22:00:00',
                'arrival_time' => '22:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 6549,
                'route_day_id' => 2983,
                'departure_time' => '22:02:00',
                'arrival_time' => '22:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 6550,
                'route_day_id' => 2984,
                'departure_time' => '22:15:00',
                'arrival_time' => '22:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 6551,
                'route_day_id' => 2985,
                'departure_time' => '22:16:00',
                'arrival_time' => '22:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 6552,
                'route_day_id' => 2986,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:32:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 6553,
                'route_day_id' => 2987,
                'departure_time' => '22:32:00',
                'arrival_time' => '22:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 6554,
                'route_day_id' => 2988,
                'departure_time' => '22:45:00',
                'arrival_time' => '22:46:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 6555,
                'route_day_id' => 2989,
                'departure_time' => '22:46:00',
                'arrival_time' => '23:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 6556,
                'route_day_id' => 2990,
                'departure_time' => '23:00:00',
                'arrival_time' => '23:02:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 6557,
                'route_day_id' => 2991,
                'departure_time' => '23:02:00',
                'arrival_time' => '23:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 6558,
                'route_day_id' => 2992,
                'departure_time' => '23:15:00',
                'arrival_time' => '23:16:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 6559,
                'route_day_id' => 2993,
                'departure_time' => '23:16:00',
                'arrival_time' => '23:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 6560,
                'route_day_id' => 2994,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 6561,
                'route_day_id' => 2995,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 6562,
                'route_day_id' => 2996,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 6563,
                'route_day_id' => 2997,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 6564,
                'route_day_id' => 2998,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 6565,
                'route_day_id' => 2999,
                'departure_time' => '09:40:00',
                'arrival_time' => '09:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 6566,
                'route_day_id' => 3000,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 6567,
                'route_day_id' => 3001,
                'departure_time' => '13:40:00',
                'arrival_time' => '13:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 6568,
                'route_day_id' => 3002,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 6569,
                'route_day_id' => 3003,
                'departure_time' => '14:20:00',
                'arrival_time' => '15:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 6570,
                'route_day_id' => 3004,
                'departure_time' => '15:20:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 6571,
                'route_day_id' => 3005,
                'departure_time' => '16:00:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 6572,
                'route_day_id' => 3006,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 6573,
                'route_day_id' => 3007,
                'departure_time' => '07:20:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 6574,
                'route_day_id' => 3008,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 6575,
                'route_day_id' => 3009,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 6576,
                'route_day_id' => 3010,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 6577,
                'route_day_id' => 3011,
                'departure_time' => '10:20:00',
                'arrival_time' => '10:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 6578,
                'route_day_id' => 3012,
                'departure_time' => '10:40:00',
                'arrival_time' => '12:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 6579,
                'route_day_id' => 3013,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 6580,
                'route_day_id' => 3014,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 6581,
                'route_day_id' => 3015,
                'departure_time' => '08:50:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 6582,
                'route_day_id' => 3016,
                'departure_time' => '09:15:00',
                'arrival_time' => '09:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 6583,
                'route_day_id' => 3017,
                'departure_time' => '09:35:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 6584,
                'route_day_id' => 3018,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 6585,
                'route_day_id' => 3019,
                'departure_time' => '13:50:00',
                'arrival_time' => '14:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 6586,
                'route_day_id' => 3020,
                'departure_time' => '14:20:00',
                'arrival_time' => '14:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 6587,
                'route_day_id' => 3021,
                'departure_time' => '14:40:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 6588,
                'route_day_id' => 3022,
                'departure_time' => '15:05:00',
                'arrival_time' => '16:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 6589,
                'route_day_id' => 3023,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 6590,
                'route_day_id' => 3024,
                'departure_time' => '08:35:00',
                'arrival_time' => '08:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 6591,
                'route_day_id' => 3025,
                'departure_time' => '08:50:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 6592,
                'route_day_id' => 3026,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 6593,
                'route_day_id' => 3027,
                'departure_time' => '10:20:00',
                'arrival_time' => '11:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 6594,
                'route_day_id' => 3028,
                'departure_time' => '11:20:00',
                'arrival_time' => '11:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 6595,
                'route_day_id' => 3029,
                'departure_time' => '12:00:00',
                'arrival_time' => '12:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 6596,
                'route_day_id' => 3030,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 6597,
                'route_day_id' => 3031,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 6598,
                'route_day_id' => 3032,
                'departure_time' => '08:45:00',
                'arrival_time' => '10:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 6599,
                'route_day_id' => 3033,
                'departure_time' => '14:00:00',
                'arrival_time' => '15:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 6600,
                'route_day_id' => 3034,
                'departure_time' => '15:35:00',
                'arrival_time' => '16:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 6601,
                'route_day_id' => 3035,
                'departure_time' => '16:15:00',
                'arrival_time' => '17:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 6602,
                'route_day_id' => 3036,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 6604,
                'route_day_id' => 3038,
                'departure_time' => '07:35:00',
                'arrival_time' => '09:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 6605,
                'route_day_id' => 3039,
                'departure_time' => '09:20:00',
                'arrival_time' => '09:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 6606,
                'route_day_id' => 3040,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:30:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 6608,
                'route_day_id' => 3042,
                'departure_time' => '14:55:00',
                'arrival_time' => '16:15:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 6609,
                'route_day_id' => 3043,
                'departure_time' => '16:20:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 6610,
                'route_day_id' => 3044,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 6611,
                'route_day_id' => 3045,
                'departure_time' => '07:45:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 6613,
                'route_day_id' => 3047,
                'departure_time' => '07:55:00',
                'arrival_time' => '08:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 6615,
                'route_day_id' => 3049,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:45:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 6616,
                'route_day_id' => 3050,
                'departure_time' => '14:50:00',
                'arrival_time' => '14:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 6617,
                'route_day_id' => 3051,
                'departure_time' => '15:00:00',
                'arrival_time' => '15:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 6618,
                'route_day_id' => 3052,
                'departure_time' => '07:00:00',
                'arrival_time' => '08:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 6619,
                'route_day_id' => 3053,
                'departure_time' => '08:05:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 6620,
                'route_day_id' => 3054,
                'departure_time' => '08:25:00',
                'arrival_time' => '08:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 6621,
                'route_day_id' => 3055,
                'departure_time' => '08:45:00',
                'arrival_time' => '09:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 6622,
                'route_day_id' => 3056,
                'departure_time' => '14:00:00',
                'arrival_time' => '14:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 6623,
                'route_day_id' => 3057,
                'departure_time' => '14:25:00',
                'arrival_time' => '14:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 6624,
                'route_day_id' => 3058,
                'departure_time' => '14:45:00',
                'arrival_time' => '15:00:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 6625,
                'route_day_id' => 3059,
                'departure_time' => '15:05:00',
                'arrival_time' => '16:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 6626,
                'route_day_id' => 3060,
                'departure_time' => '06:30:00',
                'arrival_time' => '06:40:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 6627,
                'route_day_id' => 3061,
                'departure_time' => '06:45:00',
                'arrival_time' => '06:55:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 6628,
                'route_day_id' => 3062,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:10:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 6629,
                'route_day_id' => 3063,
                'departure_time' => '07:15:00',
                'arrival_time' => '07:25:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 6630,
                'route_day_id' => 3064,
                'departure_time' => '07:30:00',
                'arrival_time' => '07:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 6631,
                'route_day_id' => 3065,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 6632,
                'route_day_id' => 3066,
                'departure_time' => '14:55:00',
                'arrival_time' => '15:05:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 6633,
                'route_day_id' => 3067,
                'departure_time' => '15:10:00',
                'arrival_time' => '15:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 6634,
                'route_day_id' => 3068,
                'departure_time' => '15:25:00',
                'arrival_time' => '15:35:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 6635,
                'route_day_id' => 3069,
                'departure_time' => '15:40:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 6636,
                'route_day_id' => 2567,
                'departure_time' => '06:00:00',
                'arrival_time' => '06:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 6637,
                'route_day_id' => 2567,
                'departure_time' => '07:00:00',
                'arrival_time' => '07:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 6638,
                'route_day_id' => 2567,
                'departure_time' => '08:00:00',
                'arrival_time' => '08:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 6639,
                'route_day_id' => 2567,
                'departure_time' => '09:00:00',
                'arrival_time' => '09:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 6640,
                'route_day_id' => 2567,
                'departure_time' => '10:00:00',
                'arrival_time' => '10:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 6641,
                'route_day_id' => 2567,
                'departure_time' => '11:00:00',
                'arrival_time' => '11:20:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 6642,
                'route_day_id' => 2567,
                'departure_time' => '13:30:00',
                'arrival_time' => '13:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 6643,
                'route_day_id' => 2567,
                'departure_time' => '14:30:00',
                'arrival_time' => '14:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 6644,
                'route_day_id' => 2567,
                'departure_time' => '15:30:00',
                'arrival_time' => '15:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 6645,
                'route_day_id' => 2567,
                'departure_time' => '16:30:00',
                'arrival_time' => '16:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 6646,
                'route_day_id' => 2567,
                'departure_time' => '17:30:00',
                'arrival_time' => '17:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 6647,
                'route_day_id' => 2567,
                'departure_time' => '18:30:00',
                'arrival_time' => '18:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 6648,
                'route_day_id' => 2567,
                'departure_time' => '19:30:00',
                'arrival_time' => '19:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 6649,
                'route_day_id' => 2567,
                'departure_time' => '20:30:00',
                'arrival_time' => '20:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 6650,
                'route_day_id' => 2567,
                'departure_time' => '21:30:00',
                'arrival_time' => '21:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 6651,
                'route_day_id' => 2567,
                'departure_time' => '22:30:00',
                'arrival_time' => '22:50:00',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}