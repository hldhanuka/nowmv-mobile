<?php

use Illuminate\Database\Seeder;

class NgoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ngos')->delete();

        \DB::table('ngos')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'title' => 'ngo_1',
                    'ngo_merchant_id' => 1,
                    'logo' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ngos/1605087314930344269189.jpeg',
                    'content' => 'This is the content',
                    'max_limit' => '20000.00',
                    'tag' => 'Save Child',
                    'status' => 1,
                    'type' => 'ngo',
                    'created_at' => '2020-02-06 00:29:17',
                    'updated_at' => '2020-02-06 09:43:23',
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'title' => 'ngo_2',
                    'ngo_merchant_id' => 1,
                    'logo' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ngos/1605087372002711605336.jpeg',
                    'content' => 'This is the content',
                    'max_limit' => '2000.00',
                    'tag' => 'Save Animal',
                    'status' => 1,
                    'type' => 'ngo',
                    'created_at' => '2020-02-06 00:29:17',
                    'updated_at' => '2020-02-06 09:43:23',
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'title' => 'ngo_3',
                    'ngo_merchant_id' => 1,
                    'logo' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ngos/1605087580312962008932.jpeg',
                    'content' => 'This is the content',
                    'max_limit' => '5000.00',
                    'tag' => 'Save Child',
                    'status' => 1,
                    'type' => 'ngo',
                    'created_at' => '2020-02-06 00:29:17',
                    'updated_at' => '2020-02-06 09:43:23',
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'title' => 'ngo_4',
                    'ngo_merchant_id' => 1,
                    'logo' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ngos/1605087607432639375819.jpeg',
                    'content' => 'This is the content',
                    'max_limit' => '10000.00',
                    'tag' => 'Save Child',
                    'status' => 1,
                    'type' => 'ngo',
                    'created_at' => '2020-02-06 00:29:17',
                    'updated_at' => '2020-02-06 09:43:23',
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'title' => 'ngo_5',
                    'ngo_merchant_id' => 1,
                    'logo' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ngos/1605087636934545777838.jpeg',
                    'content' => 'This is the content',
                    'max_limit' => '10000.00',
                    'tag' => 'Save Child',
                    'status' => 1,
                    'type' => 'ngo',
                    'created_at' => '2020-02-06 00:29:17',
                    'updated_at' => '2020-02-06 09:43:23',
                    'deleted_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'title' => 'ngo_6',
                    'ngo_merchant_id' => 1,
                    'logo' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ngos/1605087668233541833388.jpeg',
                    'content' => 'This is the content',
                    'max_limit' => '1000.00',
                    'tag' => 'Save Child',
                    'status' => 1,
                    'type' => 'ngo',
                    'created_at' => '2020-02-06 00:29:17',
                    'updated_at' => '2020-02-06 09:43:23',
                    'deleted_at' => NULL,
                ),
        ));


    }

}
