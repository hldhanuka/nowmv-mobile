<?php

use Illuminate\Database\Seeder;
use \Carbon\Carbon;

class MerchantUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('merchant_users')->delete();

        \DB::table('merchant_users')->insert([
                [
                    'id' => 1,
                    'user_id' => 8,
                    'merchant_id' => 1,
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => Carbon::now()->toDateTimeString(),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'deleted_at' => NULL,
                ],
                [
                    'id' => 2,
                    'user_id' => 9,
                    'merchant_id' => 2,
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => Carbon::now()->toDateTimeString(),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    'deleted_at' => NULL,
                ]
        ]);
    }
}
