<?php

use Illuminate\Database\Seeder;

class MtccCalendarFixedHolidaysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mtcc_calendar_fixed_holidays')->delete();
        
        \DB::table('mtcc_calendar_fixed_holidays')->insert(array (
            0 => 
            array (
                'id' => 1,
                'date' => '2017-05-27 00:00:00',
                'description' => 'First day of ramazan',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'date' => '2017-06-25 00:00:00',
                'description' => 'Fitr Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'date' => '2017-06-26 00:00:00',
                'description' => 'Fitr Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'date' => '2017-08-31 00:00:00',
                'description' => 'Hajj Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'date' => '2017-09-01 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'date' => '2017-09-02 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'date' => '2017-09-03 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'date' => '2017-09-04 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'date' => '2017-09-21 00:00:00',
                'description' => 'Islamic New Year',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'date' => '2017-11-30 00:00:00',
            'description' => 'Prophet Mulhameds (SAW) Birthday',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'date' => '2018-06-15 00:00:00',
                'description' => 'Fitr Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'date' => '2018-06-16 00:00:00',
                'description' => 'Fitr Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'date' => '2018-06-17 00:00:00',
                'description' => 'Fitr Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'date' => '2018-07-27 00:00:00',
                'description' => 'Independance Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'date' => '2018-08-20 00:00:00',
                'description' => 'Hajj Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'date' => '2018-08-21 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'date' => '2018-08-22 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'date' => '2018-08-23 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'date' => '2018-08-24 00:00:00',
                'description' => 'Alhaa Eid',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'date' => '2018-09-11 00:00:00',
                'description' => 'Hijri New Year',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'date' => '2018-11-09 00:00:00',
                'description' => 'National Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'date' => '2018-11-20 00:00:00',
            'description' => 'Prophet Mulhameds (SAW) Birthday',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'date' => '2018-05-16 00:00:00',
                'description' => 'First day of ramazan',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}