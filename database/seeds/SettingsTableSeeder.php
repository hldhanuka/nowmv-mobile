<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            [
                'id' => 1,
                'title' => 'oauth_clients',
                'text' => '[{"name": "android", "secret": "AYzKlJH3TVE8JxUSWI5ujTcMjqvyMml0yzDkhCam", "version":1, "status":1},{"name": "ios", "secret": "I14KWLkxmK2g6uw3WV2moM4O5GKoFDS7fmUkVYYs", "version":1, "status":1},{"name": "dhiraagu_service", "secret": "Lu0L4AjQSr27ywfu7kZskJT2CeAQYLaLila8TuNV", "version":1, "status":1}]',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 2,
                'title' => 'app_settings',
                'text' => '{"android": {"new_version": "1.0.0","update_required": 0},"ios": {"new_version": "1.0.0","update_required": 0}}',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 3,
                'title' => 'radius',
                'text' => '20',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 4,
                'title' => 'greeting',
                'text' => 'Hi',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 5,
                'title' => 'is_fasting_enable',
                'text' => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 6,
                'title' => 'fasting_start_date',
                'text' => '2020-01-31',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 7,
                'title' => 'fasting_end_date',
                'text' => '2020-02-09',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 8,
                'title' => 'reset_password',
                'text' => 'nowmv123',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'id' => 9,
                'title' => 'ad_position',
                'text' => '{"donation_position":2,"food_position":2,"news_position":2,"offer_position": 2}',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'id' => 10,
                'title' => 'android_dhiraagu_pay_url',
                'text' => 'https://play.google.com/store/apps/details?id=mv.com.dhiraagu.dhiraagupay',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],

            [
                'id' => 11,
                'title' => 'ios_dhiraagu_pay_url',
                'text' => 'https://apps.apple.com/vc/app/dhiraagu-pay/id1257628924',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);
    }
}
