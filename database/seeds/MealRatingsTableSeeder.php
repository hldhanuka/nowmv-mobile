<?php

use Illuminate\Database\Seeder;

class MealRatingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('meal_ratings')->delete();
        
        \DB::table('meal_ratings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'restaurant_id' => 1,
                'meal_id' => 3,
                'rate' => 5.0,
                'created_by' => '3',
                'created_at' => '2020-01-10 17:40:54',
                'updated_at' => '2020-01-10 17:40:55',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'restaurant_id' => 1,
                'meal_id' => 4,
                'rate' => 5.0,
                'created_by' => '3',
                'created_at' => '2020-01-10 17:41:07',
                'updated_at' => '2020-01-10 17:41:13',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'restaurant_id' => 1,
                'meal_id' => 5,
                'rate' => 1.0,
                'created_by' => '3',
                'created_at' => '2020-01-10 17:41:18',
                'updated_at' => '2020-01-10 17:41:19',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'restaurant_id' => 1,
                'meal_id' => 2,
                'rate' => 4.0,
                'created_by' => '3',
                'created_at' => '2020-01-11 22:01:47',
                'updated_at' => '2020-01-11 22:01:54',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'restaurant_id' => 1,
                'meal_id' => 1,
                'rate' => 3.0,
                'created_by' => '3',
                'created_at' => '2020-01-11 22:02:04',
                'updated_at' => '2020-01-11 22:02:04',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'restaurant_id' => 1,
                'meal_id' => 1,
                'rate' => 5.0,
                'created_by' => '4',
                'created_at' => '2020-01-12 19:44:50',
                'updated_at' => '2020-01-12 19:44:50',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}