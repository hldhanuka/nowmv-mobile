<?php

use Illuminate\Database\Seeder;

class DonationBadgesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('donation_badges')->delete();
        
        \DB::table('donation_badges')->insert(array (
            0 => 
            array (
                'id' => 1,
                'donation_id' => 1,
                'badge_id' => 1,
                'winning_limit' => 2000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:56:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'donation_id' => 3,
                'badge_id' => 2,
                'winning_limit' => 1000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:55:49',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'donation_id' => 4,
                'badge_id' => 3,
                'winning_limit' => 5000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:58:03',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'donation_id' => 5,
                'badge_id' => 4,
                'winning_limit' => 2500.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:58:22',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'donation_id' => 6,
                'badge_id' => 5,
                'winning_limit' => 5000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:58:45',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'donation_id' => 7,
                'badge_id' => 6,
                'winning_limit' => 3000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:59:02',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'donation_id' => 1,
                'badge_id' => 7,
                'winning_limit' => 10000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:59:27',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'donation_id' => 9,
                'badge_id' => 8,
                'winning_limit' => 4000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 10:59:59',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'donation_id' => 6,
                'badge_id' => 9,
                'winning_limit' => 15000.0,
                'status' => 1,
                'created_at' => '2020-01-10 04:49:38',
                'updated_at' => '2020-01-10 11:00:20',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}