<?php

use App\Helpers\APIHelper;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Truncate tables
        DB::table('role_has_permissions')->delete();
        DB::table('model_has_permissions')->delete();
        DB::table('model_has_roles')->delete();
        DB::table('permissions')->delete();
        DB::table('roles')->delete();

        // Create Roles
        $admin_role = Role::create(['name' => 'admin']);
        $customer_role = Role::create(['name' => 'customer']);
        $news_agent_role = Role::create(['name' => 'news_agent']);
        $merchant_role = Role::create(['name' => 'merchant']);
        // TODO - Create new roles here

        // CMS PERMISSIONS
        $cms_login_permission = Permission::create(['name' => 'cms_login']);

        // settings
        $settings_update_permission = Permission::create(['name' => 'settings.update']);

        // users
        $users_list_permission = Permission::create(['name' => 'users.list']);
        $users_create_permission = Permission::create(['name' => 'users.create']);
        $users_update_permission = Permission::create(['name' => 'users.update']);
        $users_delete_permission = Permission::create(['name' => 'users.delete']);
        $users_approve_permission = Permission::create(['name' => 'users.approve']);

        // ads
        $ads_list_permission = Permission::create(['name' => 'ads.list']);
        $ads_create_permission = Permission::create(['name' => 'ads.create']);
        $ads_update_permission = Permission::create(['name' => 'ads.update']);
        $ads_delete_permission = Permission::create(['name' => 'ads.delete']);
        $ads_approve_permission = Permission::create(['name' => 'ads.approve']);

        // badges
        $badges_list_permission = Permission::create(['name' => 'badges.list']);
        $badges_create_permission = Permission::create(['name' => 'badges.create']);
        $badges_update_permission = Permission::create(['name' => 'badges.update']);
        $badges_delete_permission = Permission::create(['name' => 'badges.delete']);
        $badges_approve_permission = Permission::create(['name' => 'badges.approve']);

        // donations
        $donations_list_permission = Permission::create(['name' => 'donations.list']);
        $donations_create_permission = Permission::create(['name' => 'donations.create']);
        $donations_update_permission = Permission::create(['name' => 'donations.update']);
        $donations_delete_permission = Permission::create(['name' => 'donations.delete']);
        $donations_approve_permission = Permission::create(['name' => 'donations.approve']);

        // events
        $events_list_permission = Permission::create(['name' => 'events.list']);
        $events_create_permission = Permission::create(['name' => 'events.create']);
        $events_update_permission = Permission::create(['name' => 'events.update']);
        $events_delete_permission = Permission::create(['name' => 'events.delete']);
        $events_approve_permission = Permission::create(['name' => 'events.approve']);

        // ngos
        $ngos_list_permission = Permission::create(['name' => 'ngos.list']);
        $ngos_create_permission = Permission::create(['name' => 'ngos.create']);
        $ngos_update_permission = Permission::create(['name' => 'ngos.update']);
        $ngos_delete_permission = Permission::create(['name' => 'ngos.delete']);

        // event_tags
        $event_tags_list_permission = Permission::create(['name' => 'event_tags.list']);
        $event_tags_create_permission = Permission::create(['name' => 'event_tags.create']);
        $event_tags_update_permission = Permission::create(['name' => 'event_tags.update']);
        $event_tags_delete_permission = Permission::create(['name' => 'event_tags.delete']);

        // interests
        $interests_list_permission = Permission::create(['name' => 'interests.list']);
        $interests_create_permission = Permission::create(['name' => 'interests.create']);
        $interests_update_permission = Permission::create(['name' => 'interests.update']);
        $interests_delete_permission = Permission::create(['name' => 'interests.delete']);
        $interests_approve_permission = Permission::create(['name' => 'interests.approve']);

        // meal_categories
        $meal_categories_list_permission = Permission::create(['name' => 'meal_categories.list']);
        $meal_categories_create_permission = Permission::create(['name' => 'meal_categories.create']);
        $meal_categories_update_permission = Permission::create(['name' => 'meal_categories.update']);
        $meal_categories_delete_permission = Permission::create(['name' => 'meal_categories.delete']);
        $meal_categories_approve_permission = Permission::create(['name' => 'meal_categories.approve']);

        // meals
        $meals_list_permission = Permission::create(['name' => 'meals.list']);
        $meals_create_permission = Permission::create(['name' => 'meals.create']);
        $meals_update_permission = Permission::create(['name' => 'meals.update']);
        $meals_delete_permission = Permission::create(['name' => 'meals.delete']);
        $meals_approve_permission = Permission::create(['name' => 'meals.approve']);

        // meal_reviews
        $meal_reviews_list_permission = Permission::create(['name' => 'meal_reviews.list']);
        $meal_reviews_delete_permission = Permission::create(['name' => 'meal_reviews.delete']);

        // news_categories
        $news_categories_list_permission = Permission::create(['name' => 'news_categories.list']);
        $news_categories_create_permission = Permission::create(['name' => 'news_categories.create']);
        $news_categories_update_permission = Permission::create(['name' => 'news_categories.update']);
        $news_categories_delete_permission = Permission::create(['name' => 'news_categories.delete']);
        $news_categories_approve_permission = Permission::create(['name' => 'news_categories.approve']);

        // news_tags
        $news_tags_list_permission = Permission::create(['name' => 'news_tags.list']);
        $news_tags_create_permission = Permission::create(['name' => 'news_tags.create']);
        $news_tags_update_permission = Permission::create(['name' => 'news_tags.update']);
        $news_tags_delete_permission = Permission::create(['name' => 'news_tags.delete']);
        $news_tags_approve_permission = Permission::create(['name' => 'news_tags.approve']);

        // news
        $news_list_permission = Permission::create(['name' => 'news.list']);
        $news_create_permission = Permission::create(['name' => 'news.create']);
        $news_update_permission = Permission::create(['name' => 'news.update']);
        $news_delete_permission = Permission::create(['name' => 'news.delete']);
        $news_approve_permission = Permission::create(['name' => 'news.approve']);

        // news_agencies
        $news_agencies_list_permission = Permission::create(['name' => 'news_agencies.list']);
        $news_agencies_create_permission = Permission::create(['name' => 'news_agencies.create']);
        $news_agencies_update_permission = Permission::create(['name' => 'news_agencies.update']);
        $news_agencies_delete_permission = Permission::create(['name' => 'news_agencies.delete']);
        $news_agencies_approve_permission = Permission::create(['name' => 'news_agencies.approve']);

        // offer_categories
        $offer_categories_list_permission = Permission::create(['name' => 'offer_categories.list']);
        $offer_categories_create_permission = Permission::create(['name' => 'offer_categories.create']);
        $offer_categories_update_permission = Permission::create(['name' => 'offer_categories.update']);
        $offer_categories_delete_permission = Permission::create(['name' => 'offer_categories.delete']);
        $offer_categories_approve_permission = Permission::create(['name' => 'offer_categories.approve']);

        // offers
        $offers_list_permission = Permission::create(['name' => 'offers.list']);
        $offers_create_permission = Permission::create(['name' => 'offers.create']);
        $offers_update_permission = Permission::create(['name' => 'offers.update']);
        $offers_delete_permission = Permission::create(['name' => 'offers.delete']);
        $offers_approve_permission = Permission::create(['name' => 'offers.approve']);

        // merchants
        $merchants_list_permission = Permission::create(['name' => 'merchants.list']);
        $merchants_create_permission = Permission::create(['name' => 'merchants.create']);
        $merchants_update_permission = Permission::create(['name' => 'merchants.update']);
        $merchants_delete_permission = Permission::create(['name' => 'merchants.delete']);
        $merchants_approve_permission = Permission::create(['name' => 'merchants.approve']);

        // recipes
        $recipes_list_permission = Permission::create(['name' => 'recipes.list']);
        $recipes_create_permission = Permission::create(['name' => 'recipes.create']);
        $recipes_update_permission = Permission::create(['name' => 'recipes.update']);
        $recipes_delete_permission = Permission::create(['name' => 'recipes.delete']);
        $recipes_approve_permission = Permission::create(['name' => 'recipes.approve']);

        // recipe_reviews
        $recipe_reviews_list_permission = Permission::create(['name' => 'recipe_reviews.list']);
        $recipe_reviews_delete_permission = Permission::create(['name' => 'recipe_reviews.delete']);

        // restaurants
        $restaurants_list_permission = Permission::create(['name' => 'restaurants.list']);
        $restaurants_create_permission = Permission::create(['name' => 'restaurants.create']);
        $restaurants_update_permission = Permission::create(['name' => 'restaurants.update']);
        $restaurants_delete_permission = Permission::create(['name' => 'restaurants.delete']);
        $restaurants_approve_permission = Permission::create(['name' => 'restaurants.approve']);

        // restaurant_reviews
        $restaurant_reviews_list_permission = Permission::create(['name' => 'restaurant_reviews.list']);
        $restaurant_reviews_delete_permission = Permission::create(['name' => 'restaurant_reviews.delete']);

        // videos
        $videos_list_permission = Permission::create(['name' => 'videos.list']);
        $videos_create_permission = Permission::create(['name' => 'videos.create']);
        $videos_update_permission = Permission::create(['name' => 'videos.update']);
        $videos_delete_permission = Permission::create(['name' => 'videos.delete']);
        $videos_approve_permission = Permission::create(['name' => 'videos.approve']);

        // prayer_tips
        $prayer_tips_list_permission = Permission::create(['name' => 'prayer_tips.list']);
        $prayer_tips_create_permission = Permission::create(['name' => 'prayer_tips.create']);
        $prayer_tips_update_permission = Permission::create(['name' => 'prayer_tips.update']);
        $prayer_tips_delete_permission = Permission::create(['name' => 'prayer_tips.delete']);
        $prayer_tips_approve_permission = Permission::create(['name' => 'prayer_tips.approve']);

        // bus_routes
        $bus_routes_list_permission = Permission::create(['name' => 'bus_routes.list']);
        $bus_routes_create_permission = Permission::create(['name' => 'bus_routes.create']);
        $bus_routes_update_permission = Permission::create(['name' => 'bus_routes.update']);
        $bus_routes_delete_permission = Permission::create(['name' => 'bus_routes.delete']);

        // ferry_routes
        $ferry_routes_list_permission = Permission::create(['name' => 'ferry_routes.list']);
        $ferry_routes_create_permission = Permission::create(['name' => 'ferry_routes.create']);
        $ferry_routes_update_permission = Permission::create(['name' => 'ferry_routes.update']);
        $ferry_routes_delete_permission = Permission::create(['name' => 'ferry_routes.delete']);

        // bus_locations
        $bus_locations_list_permission = Permission::create(['name' => 'bus_locations.list']);
        $bus_locations_create_permission = Permission::create(['name' => 'bus_locations.create']);
        $bus_locations_update_permission = Permission::create(['name' => 'bus_locations.update']);
        $bus_locations_delete_permission = Permission::create(['name' => 'bus_locations.delete']);

        // bus_halt_schedules
        $bus_halt_schedules_list_permission = Permission::create(['name' => 'bus_halt_schedules.list']);
        $bus_halt_schedules_create_permission = Permission::create(['name' => 'bus_halt_schedules.create']);
        $bus_halt_schedules_update_permission = Permission::create(['name' => 'bus_halt_schedules.update']);
        $bus_halt_schedules_delete_permission = Permission::create(['name' => 'bus_halt_schedules.delete']);

        // ferry_terminal_schedules
        $ferry_terminal_schedules_list_permission = Permission::create(['name' => 'ferry_terminal_schedules.list']);
        $ferry_terminal_schedules_create_permission = Permission::create(['name' => 'ferry_terminal_schedules.create']);
        $ferry_terminal_schedules_update_permission = Permission::create(['name' => 'ferry_terminal_schedules.update']);
        $ferry_terminal_schedules_delete_permission = Permission::create(['name' => 'ferry_terminal_schedules.delete']);


        // bus_route_schedules
        $bus_route_schedules_list_permission = Permission::create(['name' => 'bus_route_schedules.list']);
        $bus_route_schedules_create_permission = Permission::create(['name' => 'bus_route_schedules.create']);
        $bus_route_schedules_update_permission = Permission::create(['name' => 'bus_route_schedules.update']);
        $bus_route_schedules_delete_permission = Permission::create(['name' => 'bus_route_schedules.delete']);

        // ferry_route_schedules
        $ferry_route_schedules_list_permission = Permission::create(['name' => 'ferry_route_schedules.list']);
        $ferry_route_schedules_create_permission = Permission::create(['name' => 'ferry_route_schedules.create']);
        $ferry_route_schedules_update_permission = Permission::create(['name' => 'ferry_route_schedules.update']);
        $ferry_route_schedules_delete_permission = Permission::create(['name' => 'ferry_route_schedules.delete']);

        // ferry_locations
        $ferry_locations_list_permission = Permission::create(['name' => 'ferry_locations.list']);
        $ferry_locations_create_permission = Permission::create(['name' => 'ferry_locations.create']);
        $ferry_locations_update_permission = Permission::create(['name' => 'ferry_locations.update']);
        $ferry_locations_delete_permission = Permission::create(['name' => 'ferry_locations.delete']);

        // schedule_types
        $schedule_types_list_permission = Permission::create(['name' => 'schedule_types.list']);
        $schedule_types_create_permission = Permission::create(['name' => 'schedule_types.create']);
        $schedule_types_update_permission = Permission::create(['name' => 'schedule_types.update']);
        $schedule_types_delete_permission = Permission::create(['name' => 'schedule_types.delete']);

        // support_teams
        $support_teams_list_permission = Permission::create(['name' => 'support_teams.list']);
        $support_teams_create_permission = Permission::create(['name' => 'support_teams.create']);
        $support_teams_update_permission = Permission::create(['name' => 'support_teams.update']);
        $support_teams_delete_permission = Permission::create(['name' => 'support_teams.delete']);

        $push_notification_create_permission = Permission::create(['name' => 'push_notifications.create']);

        // agent_news (special)
        $agent_news_list_permission = Permission::create(['name' => 'agent_news.list']);
        $agent_news_create_permission = Permission::create(['name' => 'agent_news.create']);
        $agent_news_update_permission = Permission::create(['name' => 'agent_news.update']);
        $agent_news_delete_permission = Permission::create(['name' => 'agent_news.delete']);

        // merchant_offer (special)
        $merchant_offer_list_permission = Permission::create(['name' => 'merchant_offer.list']);
        $merchant_offer_create_permission = Permission::create(['name' => 'merchant_offer.create']);
        $merchant_offer_update_permission = Permission::create(['name' => 'merchant_offer.update']);
        $merchant_offer_delete_permission = Permission::create(['name' => 'merchant_offer.delete']);

        // zakat_payments
        $zakat_payments_list_permission = Permission::create(['name' => 'zakat_payments.list']);
        $zakat_payments_csv_permission = Permission::create(['name' => 'zakat_payments.csv']);

        $merchant_offer_category_list_permission = Permission::create(['name' => 'merchant_offer_category.list']);

        // Give Admin Permissions
        $admin_role->givePermissionTo($cms_login_permission);
        $admin_role->givePermissionTo($settings_update_permission);
        $admin_role->givePermissionTo($users_list_permission);
        $admin_role->givePermissionTo($users_create_permission);
        $admin_role->givePermissionTo($users_update_permission);
        $admin_role->givePermissionTo($users_delete_permission);
        $admin_role->givePermissionTo($users_approve_permission);
        $admin_role->givePermissionTo($ads_list_permission);
        $admin_role->givePermissionTo($ads_create_permission);
        $admin_role->givePermissionTo($ads_update_permission);
        $admin_role->givePermissionTo($ads_delete_permission);
        $admin_role->givePermissionTo($ads_approve_permission);
        $admin_role->givePermissionTo($badges_list_permission);
        $admin_role->givePermissionTo($badges_create_permission);
        $admin_role->givePermissionTo($badges_update_permission);
        $admin_role->givePermissionTo($badges_delete_permission);
        $admin_role->givePermissionTo($badges_approve_permission);
        $admin_role->givePermissionTo($donations_list_permission);
        $admin_role->givePermissionTo($donations_create_permission);
        $admin_role->givePermissionTo($donations_update_permission);
        $admin_role->givePermissionTo($donations_delete_permission);
        $admin_role->givePermissionTo($donations_approve_permission);
        $admin_role->givePermissionTo($events_list_permission);
        $admin_role->givePermissionTo($events_create_permission);
        $admin_role->givePermissionTo($events_update_permission);
        $admin_role->givePermissionTo($events_delete_permission);
        $admin_role->givePermissionTo($events_approve_permission);
        $admin_role->givePermissionTo($ngos_list_permission);
        $admin_role->givePermissionTo($ngos_create_permission);
        $admin_role->givePermissionTo($ngos_update_permission);
        $admin_role->givePermissionTo($ngos_delete_permission);
        $admin_role->givePermissionTo($event_tags_list_permission);
        $admin_role->givePermissionTo($event_tags_create_permission);
        $admin_role->givePermissionTo($event_tags_update_permission);
        $admin_role->givePermissionTo($event_tags_delete_permission);
        $admin_role->givePermissionTo($interests_list_permission);
        $admin_role->givePermissionTo($interests_create_permission);
        $admin_role->givePermissionTo($interests_update_permission);
        $admin_role->givePermissionTo($interests_delete_permission);
        $admin_role->givePermissionTo($interests_approve_permission);
        $admin_role->givePermissionTo($meal_categories_list_permission);
        $admin_role->givePermissionTo($meal_categories_create_permission);
        $admin_role->givePermissionTo($meal_categories_update_permission);
        $admin_role->givePermissionTo($meal_categories_delete_permission);
        $admin_role->givePermissionTo($meal_categories_approve_permission);
        $admin_role->givePermissionTo($meals_list_permission);
        $admin_role->givePermissionTo($meals_create_permission);
        $admin_role->givePermissionTo($meals_update_permission);
        $admin_role->givePermissionTo($meals_delete_permission);
        $admin_role->givePermissionTo($meals_approve_permission);
        $admin_role->givePermissionTo($meal_reviews_list_permission);
        $admin_role->givePermissionTo($meal_reviews_delete_permission);
        $admin_role->givePermissionTo($news_categories_list_permission);
        $admin_role->givePermissionTo($news_categories_create_permission);
        $admin_role->givePermissionTo($news_categories_update_permission);
        $admin_role->givePermissionTo($news_categories_delete_permission);
        $admin_role->givePermissionTo($news_categories_approve_permission);
        $admin_role->givePermissionTo($news_list_permission);
        $admin_role->givePermissionTo($news_create_permission);
        $admin_role->givePermissionTo($news_update_permission);
        $admin_role->givePermissionTo($news_delete_permission);
        $admin_role->givePermissionTo($news_approve_permission);
        $admin_role->givePermissionTo($news_tags_list_permission);
        $admin_role->givePermissionTo($news_tags_create_permission);
        $admin_role->givePermissionTo($news_tags_update_permission);
        $admin_role->givePermissionTo($news_tags_delete_permission);
        $admin_role->givePermissionTo($news_tags_approve_permission);
        $admin_role->givePermissionTo($news_agencies_list_permission);
        $admin_role->givePermissionTo($news_agencies_create_permission);
        $admin_role->givePermissionTo($news_agencies_update_permission);
        $admin_role->givePermissionTo($news_agencies_delete_permission);
        $admin_role->givePermissionTo($news_agencies_approve_permission);
        $admin_role->givePermissionTo($offer_categories_list_permission);
        $admin_role->givePermissionTo($offer_categories_create_permission);
        $admin_role->givePermissionTo($offer_categories_update_permission);
        $admin_role->givePermissionTo($offer_categories_delete_permission);
        $admin_role->givePermissionTo($offer_categories_approve_permission);
        $admin_role->givePermissionTo($offers_list_permission);
        $admin_role->givePermissionTo($offers_create_permission);
        $admin_role->givePermissionTo($offers_update_permission);
        $admin_role->givePermissionTo($offers_delete_permission);
        $admin_role->givePermissionTo($offers_approve_permission);
        $admin_role->givePermissionTo($merchants_list_permission);
        $admin_role->givePermissionTo($merchants_create_permission);
        $admin_role->givePermissionTo($merchants_update_permission);
        $admin_role->givePermissionTo($merchants_delete_permission);
        $admin_role->givePermissionTo($merchants_approve_permission);
        $admin_role->givePermissionTo($recipes_list_permission);
        $admin_role->givePermissionTo($recipes_create_permission);
        $admin_role->givePermissionTo($recipes_update_permission);
        $admin_role->givePermissionTo($recipes_delete_permission);
        $admin_role->givePermissionTo($recipes_approve_permission);
        $admin_role->givePermissionTo($recipe_reviews_list_permission);
        $admin_role->givePermissionTo($recipe_reviews_delete_permission);
        $admin_role->givePermissionTo($restaurants_list_permission);
        $admin_role->givePermissionTo($restaurants_create_permission);
        $admin_role->givePermissionTo($restaurants_update_permission);
        $admin_role->givePermissionTo($restaurants_delete_permission);
        $admin_role->givePermissionTo($restaurants_approve_permission);
        $admin_role->givePermissionTo($restaurant_reviews_list_permission);
        $admin_role->givePermissionTo($restaurant_reviews_delete_permission);
        $admin_role->givePermissionTo($videos_list_permission);
        $admin_role->givePermissionTo($videos_create_permission);
        $admin_role->givePermissionTo($videos_update_permission);
        $admin_role->givePermissionTo($videos_delete_permission);
        $admin_role->givePermissionTo($videos_approve_permission);
        $admin_role->givePermissionTo($prayer_tips_list_permission);
        $admin_role->givePermissionTo($prayer_tips_create_permission);
        $admin_role->givePermissionTo($prayer_tips_update_permission);
        $admin_role->givePermissionTo($prayer_tips_delete_permission);
        $admin_role->givePermissionTo($prayer_tips_approve_permission);
        $admin_role->givePermissionTo($bus_routes_list_permission);
        $admin_role->givePermissionTo($bus_routes_create_permission);
        $admin_role->givePermissionTo($bus_routes_update_permission);
        $admin_role->givePermissionTo($bus_routes_delete_permission);
        $admin_role->givePermissionTo($bus_halt_schedules_list_permission);
        $admin_role->givePermissionTo($bus_halt_schedules_create_permission);
        $admin_role->givePermissionTo($bus_halt_schedules_update_permission);
        $admin_role->givePermissionTo($bus_halt_schedules_delete_permission);
        $admin_role->givePermissionTo($ferry_terminal_schedules_list_permission);
        $admin_role->givePermissionTo($ferry_terminal_schedules_create_permission);
        $admin_role->givePermissionTo($ferry_terminal_schedules_update_permission);
        $admin_role->givePermissionTo($ferry_terminal_schedules_delete_permission);
        $admin_role->givePermissionTo($bus_route_schedules_list_permission);
        $admin_role->givePermissionTo($bus_route_schedules_create_permission);
        $admin_role->givePermissionTo($bus_route_schedules_update_permission);
        $admin_role->givePermissionTo($bus_route_schedules_delete_permission);
        $admin_role->givePermissionTo($ferry_route_schedules_list_permission);
        $admin_role->givePermissionTo($ferry_route_schedules_create_permission);
        $admin_role->givePermissionTo($ferry_route_schedules_update_permission);
        $admin_role->givePermissionTo($ferry_route_schedules_delete_permission);
        $admin_role->givePermissionTo($ferry_routes_list_permission);
        $admin_role->givePermissionTo($ferry_routes_create_permission);
        $admin_role->givePermissionTo($ferry_routes_update_permission);
        $admin_role->givePermissionTo($ferry_routes_delete_permission);
        $admin_role->givePermissionTo($bus_locations_list_permission);
        $admin_role->givePermissionTo($bus_locations_create_permission);
        $admin_role->givePermissionTo($bus_locations_update_permission);
        $admin_role->givePermissionTo($bus_locations_delete_permission);
        $admin_role->givePermissionTo($ferry_locations_list_permission);
        $admin_role->givePermissionTo($ferry_locations_create_permission);
        $admin_role->givePermissionTo($ferry_locations_update_permission);
        $admin_role->givePermissionTo($ferry_locations_delete_permission);
        $admin_role->givePermissionTo($schedule_types_list_permission);
        $admin_role->givePermissionTo($schedule_types_create_permission);
        $admin_role->givePermissionTo($schedule_types_update_permission);
        $admin_role->givePermissionTo($schedule_types_delete_permission);
        $admin_role->givePermissionTo($support_teams_list_permission);
        $admin_role->givePermissionTo($support_teams_create_permission);
        $admin_role->givePermissionTo($support_teams_update_permission);
        $admin_role->givePermissionTo($support_teams_delete_permission);
        $admin_role->givePermissionTo($push_notification_create_permission);
        $admin_role->givePermissionTo($zakat_payments_list_permission);
        $admin_role->givePermissionTo($zakat_payments_csv_permission);

        // Give News Agent Permissions
        $news_agent_role->givePermissionTo($cms_login_permission);
        $news_agent_role->givePermissionTo($news_categories_list_permission);
        $news_agent_role->givePermissionTo($news_tags_list_permission);
        $news_agent_role->givePermissionTo($agent_news_list_permission);
        $news_agent_role->givePermissionTo($agent_news_create_permission);
        $news_agent_role->givePermissionTo($agent_news_update_permission);
        $news_agent_role->givePermissionTo($agent_news_delete_permission);

        // Give Merchant Permissions
        $merchant_role->givePermissionTo($cms_login_permission);
        $merchant_role->givePermissionTo($merchant_offer_list_permission);
        $merchant_role->givePermissionTo($merchant_offer_create_permission);
        $merchant_role->givePermissionTo($merchant_offer_update_permission);
        $merchant_role->givePermissionTo($merchant_offer_delete_permission);
        $merchant_role->givePermissionTo($merchant_offer_category_list_permission);
        $merchant_role->givePermissionTo($meal_categories_list_permission);
        $merchant_role->givePermissionTo($meal_categories_create_permission);
        $merchant_role->givePermissionTo($meal_categories_update_permission);
        $merchant_role->givePermissionTo($meal_categories_delete_permission);
        $merchant_role->givePermissionTo($meal_categories_approve_permission);
        $merchant_role->givePermissionTo($meals_list_permission);
        $merchant_role->givePermissionTo($meals_create_permission);
        $merchant_role->givePermissionTo($meals_update_permission);
        $merchant_role->givePermissionTo($meals_delete_permission);
        $merchant_role->givePermissionTo($meals_approve_permission);
        $merchant_role->givePermissionTo($meal_reviews_list_permission);
        $merchant_role->givePermissionTo($meal_reviews_delete_permission);
        $merchant_role->givePermissionTo($recipes_list_permission);
        $merchant_role->givePermissionTo($recipes_create_permission);
        $merchant_role->givePermissionTo($recipes_update_permission);
        $merchant_role->givePermissionTo($recipes_delete_permission);
        $merchant_role->givePermissionTo($recipes_approve_permission);
        $merchant_role->givePermissionTo($recipe_reviews_list_permission);
        $merchant_role->givePermissionTo($recipe_reviews_delete_permission);
        $merchant_role->givePermissionTo($restaurants_list_permission);
        $merchant_role->givePermissionTo($restaurants_create_permission);
        $merchant_role->givePermissionTo($restaurants_update_permission);
        $merchant_role->givePermissionTo($restaurants_delete_permission);
        $merchant_role->givePermissionTo($restaurants_approve_permission);
        $merchant_role->givePermissionTo($restaurant_reviews_list_permission);
        $merchant_role->givePermissionTo($restaurant_reviews_delete_permission);

        // TODO - remove users when goes live
        \App\User::find(1)->assignRole($admin_role);
        \App\User::find(2)->assignRole($admin_role);
        \App\User::find(3)->assignRole($customer_role);
        \App\User::find(4)->assignRole($customer_role);
        \App\User::find(5)->assignRole($customer_role);
        \App\User::find(6)->assignRole($news_agent_role);
        \App\User::find(7)->assignRole($news_agent_role);
        \App\User::find(8)->assignRole($merchant_role);
        \App\User::find(9)->assignRole($merchant_role);
        \App\User::find(10)->assignRole($customer_role);
        \App\User::find(11)->assignRole($customer_role);
        \App\User::find(12)->assignRole($customer_role);
        \App\User::find(13)->assignRole($customer_role);
        \App\User::find(14)->assignRole($customer_role);
        \App\User::find(15)->assignRole($customer_role);
        \App\User::find(16)->assignRole($customer_role);

        APIHelper::setDefaultNotificationSettings(3);
        APIHelper::setDefaultHomeScreenOrder(3);
        APIHelper::setDefaultPrayerReminder(3);
        APIHelper::setDefaultNotificationSettings(4);
        APIHelper::setDefaultHomeScreenOrder(4);
        APIHelper::setDefaultPrayerReminder(4);
        APIHelper::setDefaultNotificationSettings(5);
        APIHelper::setDefaultHomeScreenOrder(5);
        APIHelper::setDefaultPrayerReminder(5);
        APIHelper::setDefaultNotificationSettings(10);
        APIHelper::setDefaultHomeScreenOrder(10);
        APIHelper::setDefaultPrayerReminder(10);
        APIHelper::setDefaultNotificationSettings(11);
        APIHelper::setDefaultHomeScreenOrder(11);
        APIHelper::setDefaultPrayerReminder(11);
        APIHelper::setDefaultNotificationSettings(12);
        APIHelper::setDefaultHomeScreenOrder(12);
        APIHelper::setDefaultPrayerReminder(12);
        APIHelper::setDefaultNotificationSettings(13);
        APIHelper::setDefaultHomeScreenOrder(13);
        APIHelper::setDefaultPrayerReminder(13);
        APIHelper::setDefaultNotificationSettings(14);
        APIHelper::setDefaultHomeScreenOrder(14);
        APIHelper::setDefaultPrayerReminder(14);
        APIHelper::setDefaultNotificationSettings(15);
        APIHelper::setDefaultHomeScreenOrder(15);
        APIHelper::setDefaultPrayerReminder(15);
        APIHelper::setDefaultNotificationSettings(16);
        APIHelper::setDefaultHomeScreenOrder(16);
        APIHelper::setDefaultPrayerReminder(16);
    }
}
