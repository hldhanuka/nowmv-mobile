<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('news')->delete();

        \DB::table('news')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'category_id' => 5,
                    'title' => 'Oil prices shed gains as alarm over Iran rocket strike fades - for now',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605088924351949402568.jpeg',
                    'video' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/news1.mp4',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089645869282988342.jpeg',
                    'content' => 'Brent oil futures were up on Wednesday but well below peaks hit in frenzied early trading after a rocket attack by Iran on American forces in Iraq raised the spectre of a spiralling Middle East conflict and disruption to crude flows.

Prices gave up most of their early gains as analysts said market tensions could ease as long as oil production facilities remain unaffected by attacks.

Tweets by U.S. President Donald Trump and Iran\'s foreign minister also appeared to signal a period of calm - for now.

Brent crude futures were up 21 cents, or 0.31%, at $68.48 by around 1200 GMT, after earlier rising to their highest since mid-September at $71.75.

West Texas Intermediate crude futures were down 11 cents, or 0.18%, at $62.76 a barrel. They earlier hit $65.85, the highest since late April last year.

Iran\'s missile attack on U.S.-led forces in Iraq came early on Wednesday, hours after the funeral of Qassem Soleimani, the commander of the country\'s elite Quds Force killed in a U.S. drone strike on Jan. 3.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://www.indiatoday.in/world/story/iran-us-standoff-oil-prices-shed-gains-alarm-iran-rocket-strike-fades-1635069-2020-01-08',
                    'tags' => '[1,2,4]',
                    'status' => 1,
                    'html_content' => '<h4>Oil prices shed gains as alarm over Iran rocket strike fades - for now</h4><p>Brent oil futures were up on Wednesday but well below peaks hit in frenzied early trading after a rocket attack by Iran on American forces in Iraq raised the spectre of a spiralling Middle East conflict and disruption to crude flows.

Prices gave up most of their early gains as analysts said market tensions could ease as long as oil production facilities remain unaffected by attacks.

Tweets by U.S. President Donald Trump and Iran\'s foreign minister also appeared to signal a period of calm - for now.

Brent crude futures were up 21 cents, or 0.31%, at $68.48 by around 1200 GMT, after earlier rising to their highest since mid-September at $71.75.

West Texas Intermediate crude futures were down 11 cents, or 0.18%, at $62.76 a barrel. They earlier hit $65.85, the highest since late April last year.

Iran\'s missile attack on U.S.-led forces in Iraq came early on Wednesday, hours after the funeral of Qassem Soleimani, the commander of the country\'s elite Quds Force killed in a U.S. drone strike on Jan. 3.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:51:59',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:51:59',
                    'deleted_at' => NULL,
                ),
            1 =>
                array(
                    'id' => 2,
                    'category_id' => 3,
                    'title' => 'Australians urged to evacuate as monster bushfires regenerate',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089576583441635484.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=rJ8YFlPh-sc',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089587651664290041.jpeg',
                    'content' => 'The Maldives has been known as ‘The Sunny Side of Life’ for decades now, owing to the monsoon climate and the pristine beaches and clear blue waters. This image of Maldives as a haven for those who love sandy white beaches and being one with nature in the sea has put Maldives on the forefront of the tourism sector as a luxury destination on millions of bucket-lists.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[2,4]',
                    'status' => 1,
                    'html_content' => '<h4>Australians urged to evacuate as monster bushfires regenerate</h4><p>The Maldives has been known as
‘The Sunny Side of Life’ for decades now, owing to the monsoon climate and the pristine beaches and clear blue waters.
 This image of Maldives as a haven for those who love sandy white beaches and being one with nature in the sea has put
 Maldives on the forefront of the tourism sector as a luxury destination on millions of bucket-lists.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 12:33:25',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 12:33:25',
                    'deleted_at' => NULL,
                ),
            2 =>
                array(
                    'id' => 3,
                    'category_id' => 2,
                    'title' => 'ATP Cup quarter-final: Australia beat Great Britain on match tie-break – as it happened',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089916924266363729.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=JtVvuyavveM',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089941923366275017.jpeg',
                    'content' => 'The Maldives has been known as ‘The Sunny Side of Life’ for decades now, owing to the monsoon climate and the pristine beaches and clear blue waters. This image of Maldives as a haven for those who love sandy white beaches and being one with nature in the sea has put Maldives on the forefront of the tourism sector as a luxury destination on millions of bucket-lists.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[2]',
                    'status' => 1,
                    'html_content' => '<h4>ATP Cup quarter-final: Australia beat Great Britain on match tie-break –
 as it happened</h4><p>The Maldives has been known as ‘The Sunny Side of Life’ for decades now, owing to the monsoon
  climate and the pristine beaches and clear blue waters. This image of Maldives as a haven for those who love sandy
   white beaches and being one with nature in the sea has put Maldives on the forefront of the tourism sector as a
   luxury destination on millions of bucket-lists.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 12:33:46',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 12:33:46',
                    'deleted_at' => NULL,
                ),
            3 =>
                array(
                    'id' => 4,
                    'category_id' => 2,
                    'title' => 'Liam Gallagher: My brother called me begging for Oasis reunion in 2022',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089974848435391619.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=JtVvuyavveM',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605089986908227426847.jpeg',
                    'content' => 'The Maldives has been known as ‘The Sunny Side of Life’ for decades now, owing to the monsoon climate and the pristine beaches and clear blue waters. This image of Maldives as a haven for those who love sandy white beaches and being one with nature in the sea has put Maldives on the forefront of the tourism sector as a luxury destination on millions of bucket-lists.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[1,3,4]',
                    'status' => 1,
                    'html_content' => '<h4>Liam Gallagher: My brother called me begging for Oasis reunion in 2022</h4>
<p>The Maldives has been known as ‘The Sunny Side of Life’ for decades now, owing to the monsoon climate and the pristine
beaches and clear blue waters. This image of Maldives as a haven for those who love sandy white beaches and being one with
nature in the sea has put Maldives on the forefront of the tourism sector as a luxury destination on millions of bucket-lists.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 12:38:12',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 12:38:12',
                    'deleted_at' => NULL,
                ),
            4 =>
                array(
                    'id' => 5,
                    'category_id' => 7,
                    'title' => 'The reef life: Here a host of activities opens up the colourful underwater world in the Maldives',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090008509253503403.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=P9Z6okLJDv0',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090025728540331979.jpeg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Kasun Silva',
                    'link' => 'https://www.thehindu.com/life-and-style/travel/the-reef-life/article30512863.ece',
                    'tags' => '[1,6]',
                    'status' => 1,
                    'html_content' => '<h4>The reef life: Here a host of activities opens up the colourful underwater world in the Maldives</h4>
<p>As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors
. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development
built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of
 one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:03:28',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:03:28',
                    'deleted_at' => NULL,
                ),
            5 =>
                array(
                    'id' => 6,
                    'category_id' => 3,
                    'title' => 'Payments due to courts to be accepted by all courts in Maldives',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090054681987939024.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=6N9PpMtGY30',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090069228928640214.jpeg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/76196',
                    'tags' => '[1]',
                    'status' => 1,
                    'html_content' => '<h4>Payments due to courts to be accepted by all courts in Maldives</h4>
<p>As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism
 to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the
 perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With
 an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture,
 heritage, and history into the tourism sector.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:18:35',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:18:35',
                    'deleted_at' => NULL,
                ),
            6 =>
                array(
                    'id' => 7,
                    'category_id' => 6,
                    'title' => 'Pres inaugurates new mosque in Kendhikulhudhoo',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090131541934167818.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=E2gcmB3cMz8',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090160003481168943.jpeg',
                    'content' => 'President Ibrahim Mohamed Solih on Tuesday inaugurated ‘Masjid Al-Rahmath’ mosque in Kendhikulhudhoo Island of Noonu Atoll.

The President was warmly welcomed by members of the island council and the people of Kendhikulhudhoo upon his arrival at the island. Minister of Islamic Affairs Dr. Ahmed Zahir and MP for Kendhikulhudhoo constituency, Ahmed Eesa attended the inauguration ceremony alongside the president.

Speaking at the ceremony, President Solih emphasized on the government’s efforts in curbing the challenges faced by businesses especially in relation to investors. In that regard, the President said that some of the efforts undertaken within the past year, such as legislative reform, is already bearing fruit.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/76140',
                    'tags' => '[1,2]',
                    'status' => 1,
                    'html_content' => '<h4>Pres inaugurates new mosque in Kendhikulhudhoo</h4><p>President Ibrahim Mohamed Solih on Tuesday inaugurated ‘Masjid Al-Rahmath’ mosque in Kendhikulhudhoo Island of Noonu Atoll.

The President was warmly welcomed by members of the island council and the people of Kendhikulhudhoo upon his arrival at the island. Minister of Islamic Affairs Dr. Ahmed Zahir and MP for Kendhikulhudhoo constituency, Ahmed Eesa attended the inauguration ceremony alongside the president.

Speaking at the ceremony, President Solih emphasized on the government’s efforts in curbing the challenges faced by businesses especially in relation to investors. In that regard, the President said that some of the efforts undertaken within the past year, such as legislative reform, is already bearing fruit.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:18:44',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:18:44',
                    'deleted_at' => NULL,
                ),
            7 =>
                array(
                    'id' => 8,
                    'category_id' => 6,
                    'title' => 'King Salman Mosque being constructed in capital Male\' will reach completion by the end of the month, the planning ministry has revealed.  The project engineer, Hassan Saamee revealed to the m',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090231837592663309.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=hTv48dLGswA',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090247213871138662.jpeg',
                    'content' => 'King Salman Mosque being constructed in capital Male\' will reach completion by the end of the month, the planning ministry has revealed.

The project engineer, Hassan Saamee revealed to the media during a media tour held Tuesday that according to the agreement with the government, the construction of the mosque must be completed and handed over to the government by 31st January.

While the final interior work are currently ongoing, the project will be completed by the deadline, said Saamee.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[1,4]',
                    'status' => 1,
                    'html_content' => '<h4>King Salman Mosque being constructed in capital Male\' will reach completion by the end of the month, the planning ministry has revealed.  The project engineer, Hassan Saamee revealed to the m</h4><p>King Salman Mosque being constructed in capital Male\' will reach completion by the end of the month, the planning ministry has revealed.

The project engineer, Hassan Saamee revealed to the media during a media tour held Tuesday that according to the agreement with the government, the construction of the mosque must be completed and handed over to the government by 31st January.

While the final interior work are currently ongoing, the project will be completed by the deadline, said Saamee.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:18:56',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:18:56',
                    'deleted_at' => NULL,
                ),
            8 =>
                array(
                    'id' => 9,
                    'category_id' => 5,
                    'title' => 'New PS appointed to Islamic Ministry',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090336598785688357.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=5HI_RknGNVg',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090350013995070743.jpeg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[1,4]',
                    'status' => 1,
                    'html_content' => '<h4>New PS appointed to Islamic Ministry</h4>
<p>As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt
 to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and
  historical heritage from the colonial times, with the perfect balance of modern development built around the historical
   landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of
   colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:19:13',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:19:13',
                    'deleted_at' => NULL,
                ),
            9 =>
                array(
                    'id' => 10,
                    'category_id' => 1,
                    'title' => 'Promoting Quranic knowledge is a top priority – VP',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090417157934009152.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=7hzCeOzhm3M',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605090429315844178786.jpeg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[1,2,3]',
                    'status' => 1,
                    'html_content' => '<h4>Promoting Quranic knowledge is a top priority – VP</h4>
<p>As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various
types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage
from the colonial times, with the perfect balance of modern development built around the historical landmarks to
showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation,
one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:19:24',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:19:24',
                    'deleted_at' => NULL,
                ),
            10 =>
                array(
                    'id' => 11,
                    'category_id' => 2,
                    'title' => 'Pres Solih inaugurates Quran program',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097726679362030910.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=N2FMrb8xfsM',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097739751304548850.jpeg',
                    'content' => 'President Ibrahim Mohamed Solih on Tuesday inaugurated a special program to teach the Holy Quran.

The program is aimed for individuals with special needs, under the government\'s policy to provide assistance to those with special needs, and to provide basic Islamic knowledge.

The program is being conducted by the Islamic Ministry.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[1,2,5]',
                    'status' => 1,
                    'html_content' => '<h4>Pres Solih inaugurates Quran program</h4><p>President Ibrahim Mohamed Solih on Tuesday inaugurated a special program to teach the Holy Quran.

The program is aimed for individuals with special needs, under the government\'s policy to provide assistance to those with special needs, and to provide basic Islamic knowledge.

The program is being conducted by the Islamic Ministry.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:22:50',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:22:50',
                    'deleted_at' => NULL,
                ),
            11 =>
                array(
                    'id' => 12,
                    'category_id' => 4,
                    'title' => 'Gluten-free food made at night in Aberdeen hospital to avoid contamination',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097760309300583396.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=43-pkpwErRc',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097772074931644685.jpeg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Kasun Silva',
                    'link' => 'https://www.bbc.com/news/uk-scotland-north-east-orkney-shetland-51033048',
                    'tags' => '[1]',
                    'status' => 1,
                    'html_content' => '<h4>Gluten-free food made at night in Aberdeen hospital to avoid contamination</h4>
<p>As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to
attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect
 balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally
  rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:47:34',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:47:34',
                    'deleted_at' => NULL,
                ),
            12 =>
                array(
                    'id' => 13,
                    'category_id' => 4,
                    'title' => 'Fitbit for chickpeas, Zipcar for leftovers: the food tech we really need in the 2020s',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097791767559002881.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=eKQWFJmCWZE',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097801176675314927.jpeg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Kasun Silva',
                    'link' => 'https://www.theguardian.com/food/2020/jan/09/fitbit-for-chickpeas-zipcar-for-leftovers-the-food-tech-we-really-need-in-the-2020s',
                    'tags' => '[1]',
                    'status' => 1,
                    'html_content' => '<h4>Fitbit for chickpeas, Zipcar for leftovers: the food tech we really need in the 2020s</h4>
<p>As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to
attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect
balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally
rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and
history into the tourism sector.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:47:52',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:47:52',
                    'deleted_at' => NULL,
                ),
            13 =>
                array(
                    'id' => 14,
                    'category_id' => 4,
                    'title' => 'news_14',
                    'image' => 'https://images-01.avas.mv/post/big_EdkmzdefbnIx0ied6Lnb3iFfx.jpg',
                    'video' => NULL,
                    'video_thumbnail' => 'https://images-01.avas.mv/post/big_EdkmzdefbnIx0ied6Lnb3iFfx.jpg',
                    'content' => 'As one of the biggest industries in the world, the tourism sector has seen numerous countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its culture, heritage, and history into the tourism sector.',
                    'author' => 'Kasun Silva',
                    'link' => 'https://avas.mv/en/66848',
                    'tags' => '[1]',
                    'status' => 1,
                    'html_content' => '<h4>news_14</h4><p>As one of the biggest industries in the world, the tourism sector has seen numerous
countries adapt to various types of tourism to attract visitors. Malaysia, for instance, shows off Penang’s cultural and historical heritage from
 the colonial times, with the perfect balance of modern development built around the historical landmarks to showcase the perfect harmony of the
 then and now. With an equally rich culture, heritage, and history of colonisation, one must wonder where the Maldives went wrong in adapting its
 culture, heritage, and history into the tourism sector.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 6,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 00:29:12',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 11:08:05',
                    'deleted_at' => '2020-01-09 11:08:05',
                ),
            14 =>
                array(
                    'id' => 15,
                    'category_id' => 4,
                    'title' => 'Food \'made from air\' could compete with soya',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097828020924462345.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=hqf_SIQ3JAk',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097847853357697859.jpeg',
                    'content' => 'The protein is produced from soil bacteria fed on hydrogen split from water by electricity.

The researchers say if the electricity comes from solar and wind power, the food can be grown with near-zero greenhouse gas emissions.

If their dreams are realised, it could help the world tackle many of the problems associated with farming.

When I visited Solar Foods\' pilot plant on the outskirts of Helsinki last year the researchers were raising funds for expansion.

Now they say they have attracted 5.5m euros of investment, and they predict – depending on the price of electricity – that their costs will roughly match those for soya production by the end of the decade - perhaps even by 2025.',
                    'author' => 'Kasun Silva',
                    'link' => 'https://www.bbc.com/news/science-environment-51019798',
                    'tags' => '[2,5]',
                    'status' => 1,
                    'html_content' => '<h4>News  Title</h4><p>News content</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 2,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:48:08',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:48:08',
                    'deleted_at' => NULL,
                ),
            15 =>
                array(
                    'id' => 16,
                    'category_id' => 3,
                    'title' => 'Iran attack: Who are the winners and losers in the crisis?',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097878398584894880.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=8oVXqkHOMY8',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097890783241514138.jpeg',
                    'content' => 'Despite the loss of such a powerful military figure, Iran could be a short-term beneficiary of Qasem Soleimani\'s killing.

The general\'s death, and the massive funeral processions that followed, have allowed Tehran to shift public attention away from a violent government crackdown on protests over rising petrol prices in November.

It also allows Iran to demonstrate its ability to rally at a time of crisis, with its notoriously divided political elite pulling together.',
                    'author' => 'Kasun Silva',
                    'link' => 'https://www.bbc.com/news/world-middle-east-51012268',
                    'tags' => '[1,4]',
                    'status' => 1,
                    'html_content' => '<h4>Deepika Padukone: Has Bollywood found a political voice?</h4><p>On a damp winter evening, Deepika Padukone stood in solidarity with a throng of students at Delhi\'s Jawaharlal Nehru University (JNU), protesting against a shocking campus attack allegedly by a mob of rival students linked to Narendra Modi\'s ruling Bharatiya Janata Party (BJP). She made no speeches, and left as quietly as she arrived.

Within minutes, social media erupted with a frenzy that only a Bollywood star can whip up in movie-mad India.

Padukone\'s fans and peers and student leaders lauded her for her "brave support" for the embattled students of the university, which
 has long been in the crosshairs of right-wing groups supporting Mr Modi\'s government. Even those who usually scoff at Bollywood declared
 that they would watch her new film - a feature on an acid attack victim which Padukone has produced and acted in - several times.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 2,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:48:24',
                    'created_at' => '2020-01-09 00:29:12',
                    'updated_at' => '2020-01-09 22:48:24',
                    'deleted_at' => NULL,
                ),
            16 =>
                array(
                    'id' => 17,
                    'category_id' => 1,
                    'title' => 'News  Title',
                    'image' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/1578549993397891380562.png',
                    'video' => NULL,
                    'video_thumbnail' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/1578549993397528296329.png',
                    'content' => 'News content',
                    'author' => 'Author 1',
                    'link' => 'https://avas.mv/en',
                    'tags' => '[1,2]',
                    'status' => 2,
                    'html_content' => '<h4>Prince Harry and Meghan: Where do they get their money?</h4><p>The couple say that about 95% of their income comes from the Prince of Wales. He pays for the public duties of Prince Harry and Meghan, as well as the Duke and Duchess of Cambridge, and some of their private costs.

In total, this funding - in the year Meghan officially joined the Royal Family - stood at just over £5m in 2018-19.

The money comes from Prince Charles\'s income from the Duchy of Cornwall, a vast portfolio of property and financial investments, which brought in £21.6m last year.

In addition, about 5% of the Sussexes\' income comes from the Sovereign Grant. This money is paid from the government to the Royal Family to cover expenses for official duties and looking after royal palaces. The grant is worth £82.4m in this financial year.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 6,
                    'authorized_by' => NULL,
                    'published_at' => NULL,
                    'created_at' => '2020-01-09 11:06:33',
                    'updated_at' => '2020-01-09 11:07:30',
                    'deleted_at' => '2020-01-09 11:07:30',
                ),
            17 =>
                array(
                    'id' => 18,
                    'category_id' => 2,
                    'title' => 'Deepika Padukone: Has Bollywood found a political voice?',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097913733923715746.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=Sy2VMQ7dEnU',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097924425653908234.jpeg',
                    'content' => 'On a damp winter evening, Deepika Padukone stood in solidarity with a throng of students at Delhi\'s Jawaharlal Nehru University (JNU), protesting against a shocking campus attack allegedly by a mob of rival students linked to Narendra Modi\'s ruling Bharatiya Janata Party (BJP). She made no speeches, and left as quietly as she arrived.

Within minutes, social media erupted with a frenzy that only a Bollywood star can whip up in movie-mad India.

Padukone\'s fans and peers and student leaders lauded her for her "brave support" for the embattled students of the university, which has long been in the crosshairs of right-wing groups supporting Mr Modi\'s government. Even those who usually scoff at Bollywood declared that they would watch her new film - a feature on an acid attack victim which Padukone has produced and acted in - several times.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://www.bbc.com/news/world-asia-india-51030991',
                    'tags' => '[1,2,4]',
                    'status' => 1,
                    'html_content' => '<h4>The Harry and Meghan story</h4><p>In the summer of 2016, the two were brought together on a blind date by a mutual friend in London.

"Beautiful" Meghan "just tripped and fell into my life", Harry later told the press, and he knew immediately she was "the one".

After just two dates, the new couple went on holiday together to Botswana, camping out under the stars.

They fell in love "so incredibly quickly", proof the "stars were aligned", said Harry.

To the British press, their romance was catnip. Here was a golden couple who were able to draw vast crowds, speak the language of younger generations and sprinkle royal stardust on any cause.

For months the couple avoided the cameras and it wasn\'t until the 2017 Invictus Games in Toronto that the the two were first photographed holding hands in public, smiling and laughing.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:48:38',
                    'created_at' => '2020-01-09 11:08:21',
                    'updated_at' => '2020-01-09 22:48:38',
                    'deleted_at' => NULL,
                ),
            18 =>
                array(
                    'id' => 19,
                    'category_id' => 3,
                    'title' => 'Prince Harry and Meghan: Where do they get their money?',
                    'image' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/1578591796445836902161.jpg',
                    'video' => 'https://www.youtube.com/watch?v=88NuKAnDGQQ',
                    'video_thumbnail' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/1578591796445402204943.jpg',
                    'content' => 'The couple say that about 95% of their income comes from the Prince of Wales. He pays for the public duties of Prince Harry and Meghan, as well as the Duke and Duchess of Cambridge, and some of their private costs.

In total, this funding - in the year Meghan officially joined the Royal Family - stood at just over £5m in 2018-19.

The money comes from Prince Charles\'s income from the Duchy of Cornwall, a vast portfolio of property and financial investments, which brought in £21.6m last year.

In addition, about 5% of the Sussexes\' income comes from the Sovereign Grant. This money is paid from the government to the Royal Family to cover expenses for official duties and looking after royal palaces. The grant is worth £82.4m in this financial year.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://www.bbc.com/news/explainers-51047186',
                    'tags' => '[1,2]',
                    'status' => 1,
                    'html_content' => '<h4>Prince Harry and Meghan: Where do they get their money?</h4><p>The couple say that about 95% of their income comes from the Prince of Wales. He pays for the public duties of Prince Harry and Meghan, as well as the Duke and Duchess of Cambridge, and some of their private costs.

In total, this funding - in the year Meghan officially joined the Royal Family - stood at just over £5m in 2018-19.

The money comes from Prince Charles\'s income from the Duchy of Cornwall, a vast portfolio of property and financial investments, which brought in £21.6m last year.

In addition, about 5% of the Sussexes\' income comes from the Sovereign Grant. This money is paid from the government to the Royal Family to cover expenses for official duties and looking after royal palaces. The grant is worth £82.4m in this financial year.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:48:51',
                    'created_at' => '2020-01-09 15:39:41',
                    'updated_at' => '2020-01-09 22:48:51',
                    'deleted_at' => NULL,
                ),
            19 =>
                array(
                    'id' => 20,
                    'category_id' => 3,
                    'title' => 'The Harry and Meghan story',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097947255489788745.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=qMfJgoYNXrQ',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097956366994436189.jpeg',
                    'content' => 'In the summer of 2016, the two were brought together on a blind date by a mutual friend in London.

"Beautiful" Meghan "just tripped and fell into my life", Harry later told the press, and he knew immediately she was "the one".

After just two dates, the new couple went on holiday together to Botswana, camping out under the stars.

They fell in love "so incredibly quickly", proof the "stars were aligned", said Harry.

To the British press, their romance was catnip. Here was a golden couple who were able to draw vast crowds, speak the language of younger generations and sprinkle royal stardust on any cause.

For months the couple avoided the cameras and it wasn\'t until the 2017 Invictus Games in Toronto that the the two were first photographed holding hands in public, smiling and laughing.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://www.bbc.com/news/uk-51049276',
                    'tags' => '[1,2]',
                    'status' => 1,
                    'html_content' => '<h4>The Harry and Meghan story</h4><p>In the summer of 2016, the two were brought together on a blind date by a mutual friend in London.

"Beautiful" Meghan "just tripped and fell into my life", Harry later told the press, and he knew immediately she was "the one".

After just two dates, the new couple went on holiday together to Botswana, camping out under the stars.

They fell in love "so incredibly quickly", proof the "stars were aligned", said Harry.

To the British press, their romance was catnip. Here was a golden couple who were able to draw vast crowds, speak the language of younger generations and sprinkle royal stardust on any cause.

For months the couple avoided the cameras and it wasn\'t until the 2017 Invictus Games in Toronto that the the two were first photographed holding hands in public, smiling and laughing.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 22:49:04',
                    'created_at' => '2020-01-09 15:44:39',
                    'updated_at' => '2020-01-09 22:49:04',
                    'deleted_at' => NULL,
                ),
            20 =>
                array(
                    'id' => 21,
                    'category_id' => 7,
                    'title' => 'New Custom-Made Catamarans at Jumeirah Vittaveli Maldives',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097974479928684069.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=JGZGdNTmXxU',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605097985110592847908.jpeg',
                    'content' => 'umeirah Vittaveli has yet again introduced a first in the Maldives with its new fleet of stylish custom-made catamarans that await guests visiting this luxurious island retreat.

Jumeirah’s dedicated airport personnel will welcome you on arrival and guide you to the resort’s lounge where fresh coffee, homemade cookies and a relaxed atmosphere are on offer. Log into the complimentary WiFi to catch up on the day’s news or post your first snap of arriving in this stunning island destination. Next, the resort’s team will accompany you to our top of the line transfer vessels – luxurious catamarans that will make the 20-minute ride past idyllic scenery of lush tropical islands and endless ocean blues pass in no time.',
                    'author' => 'Nimal Silva',
                    'link' => 'https://visitmaldives.com/new-custom-made-catamarans-at-jumeirah-vittaveli-maldives/',
                    'tags' => '[2,6]',
                    'status' => 1,
                    'html_content' => '<h4>New Custom-Made Catamarans at Jumeirah Vittaveli Maldives</h4><p>umeirah Vittaveli has yet again introduced a first in the Maldives with its new fleet of stylish custom-made catamarans that await guests visiting this luxurious island retreat.

Jumeirah’s dedicated airport personnel will welcome you on arrival and guide you to the resort’s lounge where fresh coffee, homemade cookies and a relaxed
 atmosphere are on offer. Log into the complimentary WiFi to catch up on the day’s news or post your first snap of arriving in this stunning island destination. Next
 , the resort’s team will accompany you to our top of the line transfer vessels – luxurious catamarans that will make the 20-minute ride past idyllic scenery of lush
 tropical islands and endless ocean blues pass in no time.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 23:08:20',
                    'created_at' => '2020-01-09 22:59:14',
                    'updated_at' => '2020-01-09 23:08:20',
                    'deleted_at' => NULL,
                ),
            21 =>
                array(
                    'id' => 22,
                    'category_id' => 7,
                    'title' => 'Discover the Wonder, Merriment and Magic of The Festive Season at Dusit Thani Maldives',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605098003883220461595.jpeg',
                    'video' => 'https://www.youtube.com/watch?v=OEaf2eYpKLU',
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605098017241491444993.jpeg',
                    'content' => 'Highlights of the programme include performances by DJ Goli, who is known throughout the region for his eclectic electronic beats; complimentary Zumba and Samba classes with professional dancer Rebecca Aroca, live performances of classic and modern hits by talented Music Duo Vlad & Anastasia; and special themed dinner nights at the resort’s award-winning restaurants.

On Christmas Day, the resort’s Baan Sanook Kids Club will be a hive of activity, with arts and crafts activities, cookie and cupcake-making workshops, fun sports, dancing sessions, Christmas tree decorating, carol singing, gift-giving from Santa, and more special surprises.

On New Year’s Eve, Dusit Thani Maldives will host ‘Once Upon a Time’ – an elegant night of wining, dining and live entertainment, including a headlining performance by Maria Eroyan and Royal Drive Band, whose lead singer, Maria, is best known for singing her way to the semifinals of famous TV shows such as ‘The Voice’ and ‘Live Sound’ in Russia.',
                    'author' => 'Lanka Perera',
                    'link' => 'https://visitmaldives.com/discover-the-wonder-merriment-and-magic-of-the-festive-season-at-dusit-thani-maldives/',
                    'tags' => '[2,4,6]',
                    'status' => 1,
                    'html_content' => '<h4>Discover the Wonder, Merriment and Magic of The Festive Season at Dusit Thani Maldives</h4><p>Highlights of the programme include performances by DJ Goli, who is known throughout the region for his eclectic electronic beats; complimentary Zumba and Samba classes with professional dancer Rebecca Aroca, live performances of classic and modern hits by talented Music Duo Vlad & Anastasia; and special themed dinner nights at the resort’s award-winning restaurants.

On Christmas Day, the resort’s Baan Sanook Kids Club will be a hive of activity, with arts and crafts activities, cookie and cupcake-making workshops, fun sports, dancing sessions, Christmas tree decorating, carol singing, gift-giving from Santa, and more special surprises.

On New Year’s Eve, Dusit Thani Maldives will host ‘Once Upon a Time’ – an elegant night of wining, dining and live entertainment, including a headlining performance by Maria Eroyan and Royal Drive Band, whose lead singer, Maria, is best known for singing her way to the semifinals of famous TV shows such as ‘The Voice’ and ‘Live Sound’ in Russia.</p>',
                    'is_featured' => 0,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 23:08:31',
                    'created_at' => '2020-01-09 23:04:43',
                    'updated_at' => '2020-01-09 23:08:31',
                    'deleted_at' => NULL,
                ),
            22 =>
                array(
                    'id' => 23,
                    'category_id' => 7,
                    'title' => 'ROADSHOW IN 4 CITIES TO PROMOTE MALDIVES IN CHINA',
                    'image' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/1578593270819293985248.jpg',
                    'video' => 'https://www.youtube.com/watch?v=vuibamGoAes',
                    'video_thumbnail' => 'https://dhiraagu-now-mv-qa.arimac.digital/uploads/news/1578593270820455895128.jpg',
                    'content' => 'The four-city roadshow targets the China market to maximize the Maldives brand exposure. This event will serve as a platform
                     for Maldives tourism industry partners to showcase products and network with top travel trade industry partners to increase tourist arrivals
                      from the target markets. Eleven companies from the Maldives Tourism Industry has participated in this roadshow to create potential opportunities
                       and to further strengthen the relationship with the Chinese tour operators. In addition to the destination presentation and one-to-one networking
                        sessions during the event, a raffle draw winner will be chosen from each city to spend holidays in the Maldives, with sponsorship from Paradise
                        Island Resort, Sun Island Resort & Spa, Robinson Club Maldives and Furaveri Maldives. MMPRC has received excellent support from Maldives industry
                        partners for the “Journey to the sunny side” roadshows in 2019. This would mark the completion of the series planned for 2019 where 12 roadshows in
                         37 cities have been conducted this year.',
                    'author' => 'Nimal Silva',
                    'link' => 'https://visitmaldives.com/roadshow-in-4-cities-to-promote-maldives-in-china/',
                    'tags' => '[2,6]',
                    'status' => 1,
                    'html_content' => '<h4>ROADSHOW IN 4 CITIES TO PROMOTE MALDIVES IN CHINA</h4><p>The four-city roadshow targets the China market to maximize the Maldives brand exposure. This event will serve as a platform
                     for Maldives tourism industry partners to showcase products and network with top travel trade industry partners to increase tourist arrivals
                      from the target markets. Eleven companies from the Maldives Tourism Industry has participated in this roadshow to create potential opportunities
                       and to further strengthen the relationship with the Chinese tour operators. In addition to the destination presentation and one-to-one networking
                        sessions during the event, a raffle draw winner will be chosen from each city to spend holidays in the Maldives, with sponsorship from Paradise
                        Island Resort, Sun Island Resort & Spa, Robinson Club Maldives and Furaveri Maldives. MMPRC has received excellent support from Maldives industry
                        partners for the “Journey to the sunny side” roadshows in 2019. This would mark the completion of the series planned for 2019 where 12 roadshows in
                         37 cities have been conducted this year.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 23:08:07',
                    'created_at' => '2020-01-09 23:07:50',
                    'updated_at' => '2020-01-09 23:08:07',
                    'deleted_at' => NULL,
                ),
            23 =>
                array(
                    'id' => 24,
                    'category_id' => 1,
                    'title' => 'Jos Buttler fined for verbal volley at Vernon Philander in Cape Town Test',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605098036244183214820.jpeg',
                    'video' => null,
                    'video_thumbnail' => null,
                    'content' => 'England wicketkeeper Jos Buttler has been fined 15% of his match fee and handed one demerit point after a foul-mouthed verbal volley aimed at South Africa batsman Vernon Philander during the second test at Newlands on Tuesday.

Buttler was heard swearing at Philander through the stump microphone on day five as the South African batted to save the test for the home side, who succumbed to a 189-run defeat to leave the four-match series level at 1-1.

Buttler was found guilty of a breach of the International Cricket Council\'s Code of Conduct for players, specifically the "use of an audible obscenity during an international cricket match".',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://www.indiatoday.in/sports/cricket/story/jos-buttler-fined-15-percent-fee-vernon-philander-swearing-south-africa-vs-england-1635480-2020-01-09',
                    'tags' => '[2,6]',
                    'status' => 1,
                    'html_content' => '<h4>Jos Buttler fined for verbal volley at Vernon Philander in Cape Town Test</h4><p>England wicketkeeper Jos Buttler has been fined 15% of his match fee and handed one demerit point after a foul-mouthed verbal volley aimed at South Africa batsman Vernon Philander during the second test at Newlands on Tuesday.

Buttler was heard swearing at Philander through the stump microphone on day five as the South African batted to save the test for the home side, who succumbed to a 189-run defeat to leave the four-match series level at 1-1.

Buttler was found guilty of a breach of the International Cricket Council\'s Code of Conduct for players, specifically the "use of an audible obscenity during an international cricket match</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 23:17:29',
                    'created_at' => '2020-01-09 23:14:23',
                    'updated_at' => '2020-01-09 23:17:29',
                    'deleted_at' => NULL,
                ),
            24 =>
                array(
                    'id' => 25,
                    'category_id' => 1,
                    'title' => 'Vietnam to compete in 25 international sport events in Q1',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605098066803980709784.jpeg',
                    'video' => null,
                    'video_thumbnail' => null,
                    'content' => 'Vietnamese athletes will compete in the Asian Youth Weightlifting Championship which is held in Qatar from January 1-8, the World Pencak Silat Championship in Thailand from January 6-17, the Asian Carom 3-cushion Championship in the Republic of Korea (RoK) from January 19-23 and the 2014 – 2015 Sepak Takraw Super Series in Malaysia from February 5-8, among others.

In particular, the country’s fencers will join 9 international tournaments early this year. To prepare for the upcoming competitions, the team has been attending a training course in the RoK until the end of February.

Head of the Vietnam Fencing Department Phung Le Quang said the competitions and training course are expected to help fencing athletes improve their capability and have more experience towards the goal of securing the first place at the 28 th SEA Games in Singapore next year and being qualified to participate in the 2016 Olympics.
Vietnam will also host 8 international championships during the year’s first three months.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://english.vietnamnet.vn/fms/sports/120624/vietnam-to-compete-in-25-international-sport-events-in-q1.html',
                    'tags' => '[2,5]',
                    'status' => 1,
                    'html_content' => '<h4>Vietnam to compete in 25 international sport events in Q1</h4><p>Vietnamese athletes will compete in the Asian Youth Weightlifting Championship which is held in Qatar from January 1-8, the World Pencak Silat Championship in Thailand from January 6-17, the Asian Carom 3-cushion Championship in the Republic of Korea (RoK) from January 19-23 and the 2014 – 2015 Sepak Takraw Super Series in Malaysia from February 5-8, among others.

In particular, the country’s fencers will join 9 international tournaments early this year. To prepare for the upcoming competitions, the team has been attending a training course in the RoK until the end of February.

Head of the Vietnam Fencing Department Phung Le Quang said the competitions and training course are expected to help fencing athletes improve their capability and have more experience towards the goal of securing the first place at the 28 th SEA Games in Singapore next year and being qualified to participate in the 2016 Olympics</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 2,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 23:17:42',
                    'created_at' => '2020-01-09 23:17:08',
                    'updated_at' => '2020-01-09 23:17:42',
                    'deleted_at' => NULL,
                ),
            25 =>
                array(
                    'id' => 26,
                    'category_id' => 6,
                    'title' => 'Binance Charity Launches Relief Effort for Australian Bushfires',
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605098131899433440026.jpeg',
                    'video' => null,
                    'video_thumbnail' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/uploads/news/1605098139437462544505.jpeg',
                    'content' => 'The crypto and blockchain community has initiated an array of campaigns aimed at helping people in need around the world, with traditional philanthropic organizations like UNICEF and the World Wildlife Fund (WWF) also explored the benefits of cryptocurrency contributions.

Last November, the Red Cross societies of Norway, Denmark and Kenya launched a two-year plan to replace cash and voucher provision in aid and development efforts with blockchain-backed "local currencies."

In September of last year, ConsenSys, a blockchain startup founded by Ethereum’s co-founder Joseph Lubin, and the WWF launched an Ethereum blockchain-based platform Impactio designed to supervise and fund projects within nongovernmental organizations and standalone companies, as well as to trace how companies’ funds are spent within social impact projects.',
                    'author' => 'Dilusha Jayathilake',
                    'link' => 'https://cointelegraph.com/news/binance-charity-launches-relief-effort-for-australian-bushfires',
                    'tags' => '[2,3]',
                    'status' => 1,
                    'html_content' => '<h4>Binance Charity Launches Relief Effort for Australian Bushfires</h4><p>The crypto and blockchain community has initiated an array of campaigns aimed at helping people in need around the world, with traditional philanthropic organizations like UNICEF and the World Wildlife Fund (WWF) also explored the benefits of cryptocurrency contributions.

Last November, the Red Cross societies of Norway, Denmark and Kenya launched a two-year plan to replace cash and voucher provision in aid and development efforts with blockchain-backed "local currencies."

In September of last year, ConsenSys, a blockchain startup founded by Ethereum’s co-founder Joseph Lubin, and the WWF launched an Ethereum blockchain-based platform Impactio designed to supervise and fund
projects within nongovernmental organizations and standalone companies, as well as to trace how companies’ funds are spent within social impact projects.</p>',
                    'is_featured' => 1,
                    'news_agency_id' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-09 23:23:09',
                    'created_at' => '2020-01-09 23:22:34',
                    'updated_at' => '2020-01-09 23:23:09',
                    'deleted_at' => NULL,
                ),
        ));


    }
}
