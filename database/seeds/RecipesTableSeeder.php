<?php

use Illuminate\Database\Seeder;

class RecipesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('recipes')->delete();

        \DB::table('recipes')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Coconut Curry Chicken',
                'content' => 'Love it. I added a little cayenne pepper for extra kick and served it with yellow rice. Great recipe.',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/recipes/1605096012414132176676.jpeg',
                'video' => 'https://www.youtube.com/watch?v=c3iGVrtnSWw',
                'link' => 'https://www.delish.com/cooking/recipe-ideas/a27423175/easy-coconut-curry-chicken-recipe/',
                'serving' => '5 SERVINGS',
                'prepare_time' => '15 MINS',
                'cook_time' => '20 MINS',
            'ingredients' => '[{"id":"1","item":"1 tbsp. butter"},{"id":"2","item":"1 tbsp. vegetable oil"},{"id":"3","item":"1 medium red onion, chopped"},{"id":"4","item":"2 large shallots, minced"},{"id":"5","item":"Kosher salt"},{"id":"6","item":"2 cloves garlic, minced"},{"id":"7","item":"1 tsp. freshly grated ginger"},{"id":"8","item":"1 1\\/2 tbsp. curry powder"},{"id":"9","item":"2 tbsp. tomato paste"},{"id":"10","item":"1 (13-oz) can coconut milk"},{"id":"11","item":"1\\/2 c. water"},{"id":"12","item":"1 1\\/2 lb. boneless, skinless chicken breast, cut into 1\\" pieces"},{"id":"13","item":"Juice of 1\\/2 lime"},{"id":"14","item":"Lime wedges, for serving"},{"id":"15","item":"Mint leaves, torn, for serving"},{"id":"16","item":"Cilantro leaves, torn, for serving"},{"id":"17","item":"Cooked rice, for serving"}]',
                'directions' => '[{"id":"1","item":"In a large pot or high-sided skillet over medium heat, heat oil and butter. When butter is melted, add onion and shallots and cook until tender and translucent, 6 to 8 minutes."},{"id":"2","item":"Add garlic, ginger, and curry powder and cook until fragrant, 1 minute more. Add tomato paste and cook until darkened slightly, 1 to 2 minutes more."},{"id":"3","item":"Add coconut milk and water and bring to a simmer. Add chicken and cook, stirring occasionally, until chicken is cooked through, 6 to 8 minutes."},{"id":"4","item":"Stir in lime juice and garnish with mint and cilantro. Serve hot with rice."}]',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 11:51:08',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 11:51:08',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Chicken Noodle Curry',
                'content' => 'Skip the Thai takeout and make your own curry at home! This easy chicken noodle curry comes together easily for a simple weeknight dinner that\'s full of flavor and soul comforting. Fall soups have nothing on this dish.',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/recipes/1605096030803434697821.jpeg',
                'video' => 'https://www.youtube.com/watch?v=53KHBDmgQ2w',
                'link' => 'https://www.delish.com/cooking/recipe-ideas/a29359239/chicken-noodle-curry-recipe/',
                'serving' => '4 SERVINGS',
                'prepare_time' => '15 MINS',
                'cook_time' => '1 HOUR',
            'ingredients' => '[{"id":"1","item":"1 (16-oz.) package rice noodles"},{"id":"2","item":"2 tbsp. vegetable oil"},{"id":"3","item":"1 lb. boneless skinless chicken breast, thinly sliced"},{"id":"4","item":"Kosher salt"},{"id":"5","item":"Freshly ground black pepper"},{"id":"6","item":"1 onion, chopped"},{"id":"7","item":"1 green bell pepper, chopped"},{"id":"8","item":"2 cloves garlic, minced"},{"id":"9","item":"1 Thai chili, thinly sliced"},{"id":"10","item":"2 tbsp. curry paste"},{"id":"11","item":"2 (15-oz.) cans coconut milk"},{"id":"12","item":"1 c. low-sodium chicken broth"},{"id":"13","item":"1\\/4 c. low-sodium soy sauce"},{"id":"14","item":"2 tbsp. fish sauce"},{"id":"15","item":"Juice of 1 lime, plus more for garnish"},{"id":"16","item":"Freshly chopped cilantro, for serving"}]',
                'directions' => '[{"id":"1","item":"In a large pot of boiling salted water, cook rice noodles according to package directions. Drain."},{"id":"2","item":"In another large pot over medium-high heat, heat oil. Add chicken and season with salt and pepper. Cook until chicken is golden and cooked through, 5 minutes. Remove chicken from pan and reserve on a plate."},{"id":"3","item":"Return pot to heat and add onion and bell pepper. Cook until soft, 5 minutes. Add garlic and chili and cook until fragrant, 1 minute more. Add curry paste and stir to coat."},{"id":"4","item":"Add coconut milk to pot and bring to a boil. Reduce heat and let simmer 15 minutes, until thickened slightly."},{"id":"5","item":"Add chicken broth, soy sauce, fish sauce, lime juice, and chicken to pot. Continue to simmer for another 10 minutes. Add noodles and toss to coat."},{"id":"6","item":"Garnish with cilantro and serve with lime wedges."}]',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 11:08:42',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 11:08:42',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Chicken Noodle Soup',
            'content' => 'When we\'re sick, there\'s only one thing we crave: this chicken noodle soup. If you wanna pack it with more veggies (hey, they\'re good for you!) throw in some peppers with the onions, or stir in some spinach or kale when it\'s almost done cooking.

You know what else makes us feel better? CHOCOLATE. Treat yourself to some flourless fudge cookies for dessert. You deserve it. 😊',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/recipes/1605096052845511671385.jpeg',
                'video' => 'https://www.youtube.com/watch?v=D4r3t8VMYSI',
                'link' => 'https://www.delish.com/cooking/recipe-ideas/recipes/a51338/homemade-chicken-noodle-soup-recipe/',
                'serving' => '4 PP',
                'prepare_time' => '20 MINS',
                'cook_time' => '20 MINS',
                'ingredients' => '[{"id":"1","item":"2 cloves garlic, minced"},{"id":"2","item":"1 c. diced onion"},{"id":"3","item":"2 carrots, diced"},{"id":"4","item":"1 lb. boneless skinless chicken breast, cut into 1\\/2 inch pieces"},{"id":"5","item":"2 celery stalks, diced"},{"id":"6","item":"kosher salt"},{"id":"7","item":"Freshly ground black pepper"},{"id":"8","item":"32 oz. low-sodium chicken stock"},{"id":"9","item":"1 c. egg noodles"},{"id":"10","item":"1\\/4 c. chopped parsley"}]',
                'directions' => '[{"id":"1","item":"In a large pot over medium heat, heat oil. Add onions, celery, and carrots and cook until softened, 6 minutes. Add garlic and thyme and cook until fragrant, 1 minute more."},{"id":"2","item":"Move vegetables to one side of the pot and add chicken. Season with salt and pepper and cook until no pink remains, 6 to 8 minutes. Add stock and 1\\/2 cup of water and bring to a boil."},{"id":"3","item":"Move vegetables to one side of the pot and add chicken. Season with salt and pepper and cook until no pink remains, 6 to 8 minutes. Add stock and 1\\/2 cup of water and bring to a boil."}]',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 11:53:40',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 11:53:40',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'Creamy Tuscan Chicken',
                'content' => 'Be sure to have some crusty bread on hand, because this sauce is KILLER. When it comes to chicken breast recipes, its hard to beat this one.

Made it? Let us know how it went in the comment section below!',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/recipes/1605096084029304903034.jpeg',
                'video' => 'https://www.youtube.com/watch?v=5XGhppi8_9Q',
                'link' => 'https://www.delish.com/cooking/recipe-ideas/a19636089/creamy-tuscan-chicken-recipe/',
                'serving' => '4 SERVINGS',
                'prepare_time' => '5 MINS',
                'cook_time' => '40 MINS',
                'ingredients' => '[{"id":"1","item":"1 tbsp. extra-virgin olive oil"},{"id":"2","item":"4 boneless skinless chicken breasts"},{"id":"3","item":"Kosher salt"},{"id":"4","item":"Freshly ground black pepper"},{"id":"5","item":"1 tsp. dried oregano"},{"id":"6","item":"3 tbsp. butter"},{"id":"7","item":"3 cloves garlic, minced"},{"id":"8","item":"1 1\\/2 c. cherry tomatoes, halved"},{"id":"9","item":"3 c. baby spinach"},{"id":"10","item":"1\\/2 c. heavy cream"},{"id":"11","item":"1\\/4 c. freshly grated Parmesan"},{"id":"12","item":"Lemon wedges, for serving"}]',
                'directions' => '[{"id":"1","item":"In a skillet over medium heat, heat oil. Add chicken and season with salt, pepper, and oregano. Cook until golden and no longer pink, 8 minutes per side. Remove from skillet and set aside."},{"id":"2","item":"In the same skillet over medium heat, melt butter. Stir in garlic and cook until fragrant, about 1 minute. Add cherry tomatoes and season with salt and pepper. Cook until tomatoes are beginning to burst then add spinach and cook until spinach is beginning to wilt."},{"id":"3","item":"Stir in heavy cream and parmesan and bring mixture to a simmer. Reduce heat to low and simmer until sauce is slightly reduced, about 3 minutes. Return chicken to skillet and cook until heated through, 5 to 7 minutes."},{"id":"4","item":"Serve with lemon wedges."}]',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 11:22:56',
                'created_at' => '2020-01-09 11:22:18',
                'updated_at' => '2020-01-09 11:22:56',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'title' => 'Chicken Pizza',
                'content' => 'Nutritional Information
Serving Size 6 servings, 1 piece (119 g) each
AMOUNT PER SERVING
Calories	240
% Daily Value
Total fat	11g
Saturated fat	5g
Cholesterol	15mg
Sodium	540mg
Carbohydrate	27g
Dietary fibre	2g
Sugars	4g
Protein	10g

Vitamin A	8 %DV
Vitamin C	15 %DV
Calcium	20 %DV
Iron	15 %DV',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/recipes/1605096104332518307255.jpeg',
                'video' => 'https://www.youtube.com/watch?v=HFGKbvp5tX8',
                'link' => 'http://www.kraftcanada.com/recipes/tomato-onion-pizza-172528',
            'serving' => '6 servings, 1 piece (119 g) each',
                'prepare_time' => '10min.',
                'cook_time' => '30min.',
                'ingredients' => '[{"id":"1","item":"1\\/4 recipe Perfect Parmesan Pizza Dough"},{"id":"2","item":"1 cup Catelli Tomato & Basil Pizza Sauce"},{"id":"3","item":"1-1\\/2 cups Cracker Barrel Shredded Mozzarella Cheese , divided"},{"id":"4","item":"1 cup halved cherry tomatoes"},{"id":"5","item":"1\\/2 cup sliced red onion"}]',
                'directions' => '[{"id":"1","item":"Heat oven to 450\\u00baF."},{"id":"2","item":"Roll Perfect Parmesan Pizza Dough into 12-inch circle on lightly floured surface. Transfer to baking sheet sprayed with cooking spray."},{"id":"3","item":"Spread dough with sauce to within 1\\/2 inch of edge. Top with 1 cup cheese, tomatoes and onions."},{"id":"4","item":"Bake 10 min. Top with remaining cheese; bake 8 to 10 min. or until cheese is melted and edge of crust is golden brown."}]',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 16:24:27',
                'created_at' => '2020-01-12 16:24:14',
                'updated_at' => '2020-01-12 16:24:27',
                'deleted_at' => NULL,
            ),
        ));


    }
}
