<?php

use Illuminate\Database\Seeder;

class BadgesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('badges')->delete();

        \DB::table('badges')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'badge_1',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge1.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:00:28',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:00:28',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'badge_2',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge2.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:00:32',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:00:32',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'badge_3',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge3.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:00:37',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:00:37',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'badge_4',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge4.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:00:43',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:00:43',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'title' => 'badge_5',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge5.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:00:49',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:00:49',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'title' => 'badge_6',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge6.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:00:57',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:00:57',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'title' => 'badge_7',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge7.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:01:06',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:01:06',
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'title' => 'badge_8',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge8.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:01:13',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:01:13',
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'title' => 'badge_9',
                'type' => 'donation',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge9.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:01:19',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:01:19',
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'title' => 'badge_10',
                'type' => 'ngo',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge9.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:01:19',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:01:19',
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'title' => 'badge_11',
                'type' => 'ngo',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge9.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:01:19',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:01:19',
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'title' => 'badge_12',
                'type' => 'ngo',
                'status' => 1,
                'description' => 'You have achieved this badge',
                'image' => 'https://now.dhiraagu.com.mv/uploads/badges/badge9.png',
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:01:19',
                'created_at' => '2020-01-10 04:47:21',
                'updated_at' => '2020-01-10 11:01:19',
                'deleted_at' => NULL,
            ),
        ));


    }
}
