<?php

use Illuminate\Database\Seeder;

class MtccRouteStopDaysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mtcc_route_stop_days')->delete();
        
        \DB::table('mtcc_route_stop_days')->insert(array (
            0 => 
            array (
                'id' => 198,
                'route_stop_id' => 55,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 199,
                'route_stop_id' => 55,
                'month_type' => 'normal',
                'day_types' => 'sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 201,
                'route_stop_id' => 57,
                'month_type' => 'normal',
                'day_types' => 'sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 223,
                'route_stop_id' => 55,
                'month_type' => 'normal',
                'day_types' => 'fri',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 224,
                'route_stop_id' => 79,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 225,
                'route_stop_id' => 80,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 226,
                'route_stop_id' => 81,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 227,
                'route_stop_id' => 81,
                'month_type' => 'normal',
                'day_types' => 'fri',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 228,
                'route_stop_id' => 82,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 229,
                'route_stop_id' => 82,
                'month_type' => 'normal',
                'day_types' => 'fri',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 230,
                'route_stop_id' => 83,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 231,
                'route_stop_id' => 84,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 232,
                'route_stop_id' => 85,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 233,
                'route_stop_id' => 86,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 234,
                'route_stop_id' => 87,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 235,
                'route_stop_id' => 88,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 236,
                'route_stop_id' => 89,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 237,
                'route_stop_id' => 90,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 238,
                'route_stop_id' => 91,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 239,
                'route_stop_id' => 92,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 240,
                'route_stop_id' => 93,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 241,
                'route_stop_id' => 94,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 242,
                'route_stop_id' => 95,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 243,
                'route_stop_id' => 96,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 244,
                'route_stop_id' => 97,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 245,
                'route_stop_id' => 98,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 246,
                'route_stop_id' => 99,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 247,
                'route_stop_id' => 100,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 254,
                'route_stop_id' => 107,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 255,
                'route_stop_id' => 108,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 256,
                'route_stop_id' => 109,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 257,
                'route_stop_id' => 110,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 258,
                'route_stop_id' => 111,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 259,
                'route_stop_id' => 112,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 260,
                'route_stop_id' => 113,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 261,
                'route_stop_id' => 114,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 262,
                'route_stop_id' => 115,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 263,
                'route_stop_id' => 116,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 264,
                'route_stop_id' => 117,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 265,
                'route_stop_id' => 118,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 266,
                'route_stop_id' => 119,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 267,
                'route_stop_id' => 120,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 268,
                'route_stop_id' => 121,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 269,
                'route_stop_id' => 122,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 270,
                'route_stop_id' => 123,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 271,
                'route_stop_id' => 124,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 272,
                'route_stop_id' => 125,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 273,
                'route_stop_id' => 126,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 274,
                'route_stop_id' => 127,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 275,
                'route_stop_id' => 128,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 276,
                'route_stop_id' => 129,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 277,
                'route_stop_id' => 130,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 278,
                'route_stop_id' => 131,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 279,
                'route_stop_id' => 132,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 280,
                'route_stop_id' => 133,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 281,
                'route_stop_id' => 134,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 282,
                'route_stop_id' => 135,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 283,
                'route_stop_id' => 136,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 284,
                'route_stop_id' => 137,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 285,
                'route_stop_id' => 138,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 286,
                'route_stop_id' => 139,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 287,
                'route_stop_id' => 140,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 288,
                'route_stop_id' => 141,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 289,
                'route_stop_id' => 142,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 290,
                'route_stop_id' => 143,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 291,
                'route_stop_id' => 144,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 292,
                'route_stop_id' => 145,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 293,
                'route_stop_id' => 146,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 294,
                'route_stop_id' => 147,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 295,
                'route_stop_id' => 148,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 296,
                'route_stop_id' => 149,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 297,
                'route_stop_id' => 150,
                'month_type' => 'normal',
                'day_types' => 'wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 298,
                'route_stop_id' => 151,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 299,
                'route_stop_id' => 152,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 300,
                'route_stop_id' => 153,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 301,
                'route_stop_id' => 154,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 302,
                'route_stop_id' => 155,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 303,
                'route_stop_id' => 156,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 304,
                'route_stop_id' => 157,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 305,
                'route_stop_id' => 158,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 306,
                'route_stop_id' => 159,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 307,
                'route_stop_id' => 160,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 308,
                'route_stop_id' => 161,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 309,
                'route_stop_id' => 162,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 310,
                'route_stop_id' => 163,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 311,
                'route_stop_id' => 164,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 312,
                'route_stop_id' => 165,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 313,
                'route_stop_id' => 166,
                'month_type' => 'normal',
                'day_types' => 'tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 314,
                'route_stop_id' => 167,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 315,
                'route_stop_id' => 168,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 316,
                'route_stop_id' => 169,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 317,
                'route_stop_id' => 170,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 318,
                'route_stop_id' => 171,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 319,
                'route_stop_id' => 172,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 320,
                'route_stop_id' => 173,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 321,
                'route_stop_id' => 174,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 322,
                'route_stop_id' => 175,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 325,
                'route_stop_id' => 178,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 326,
                'route_stop_id' => 179,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 327,
                'route_stop_id' => 180,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 328,
                'route_stop_id' => 181,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 329,
                'route_stop_id' => 182,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 330,
                'route_stop_id' => 183,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 331,
                'route_stop_id' => 184,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 332,
                'route_stop_id' => 185,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 333,
                'route_stop_id' => 186,
                'month_type' => 'normal',
                'day_types' => 'sun,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 334,
                'route_stop_id' => 187,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 335,
                'route_stop_id' => 188,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 336,
                'route_stop_id' => 189,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 337,
                'route_stop_id' => 190,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 338,
                'route_stop_id' => 191,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 339,
                'route_stop_id' => 192,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 340,
                'route_stop_id' => 193,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 341,
                'route_stop_id' => 194,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 342,
                'route_stop_id' => 195,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 343,
                'route_stop_id' => 196,
                'month_type' => 'normal',
                'day_types' => 'mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 344,
                'route_stop_id' => 197,
                'month_type' => 'normal',
                'day_types' => 'mon,tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 345,
                'route_stop_id' => 198,
                'month_type' => 'normal',
                'day_types' => 'mon,tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 346,
                'route_stop_id' => 199,
                'month_type' => 'normal',
                'day_types' => 'mon,tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 347,
                'route_stop_id' => 200,
                'month_type' => 'normal',
                'day_types' => 'mon,tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 348,
                'route_stop_id' => 201,
                'month_type' => 'normal',
                'day_types' => 'mon,tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 349,
                'route_stop_id' => 202,
                'month_type' => 'normal',
                'day_types' => 'mon,tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 350,
                'route_stop_id' => 203,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 351,
                'route_stop_id' => 204,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 352,
                'route_stop_id' => 205,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 353,
                'route_stop_id' => 206,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 354,
                'route_stop_id' => 207,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 355,
                'route_stop_id' => 208,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 356,
                'route_stop_id' => 209,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 357,
                'route_stop_id' => 210,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 358,
                'route_stop_id' => 211,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 359,
                'route_stop_id' => 212,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 360,
                'route_stop_id' => 213,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 361,
                'route_stop_id' => 214,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 362,
                'route_stop_id' => 215,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 363,
                'route_stop_id' => 216,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 364,
                'route_stop_id' => 217,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 365,
                'route_stop_id' => 218,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 366,
                'route_stop_id' => 219,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 367,
                'route_stop_id' => 220,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 368,
                'route_stop_id' => 221,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 369,
                'route_stop_id' => 222,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 370,
                'route_stop_id' => 223,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 371,
                'route_stop_id' => 224,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 372,
                'route_stop_id' => 225,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 373,
                'route_stop_id' => 226,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 374,
                'route_stop_id' => 227,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 375,
                'route_stop_id' => 228,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 376,
                'route_stop_id' => 229,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 377,
                'route_stop_id' => 230,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 378,
                'route_stop_id' => 231,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 379,
                'route_stop_id' => 232,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 380,
                'route_stop_id' => 233,
                'month_type' => 'normal',
                'day_types' => 'sun,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 381,
                'route_stop_id' => 234,
                'month_type' => 'normal',
                'day_types' => 'sun,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 382,
                'route_stop_id' => 235,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 383,
                'route_stop_id' => 236,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 384,
                'route_stop_id' => 237,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 385,
                'route_stop_id' => 238,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 386,
                'route_stop_id' => 239,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 387,
                'route_stop_id' => 240,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 388,
                'route_stop_id' => 241,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 389,
                'route_stop_id' => 242,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 390,
                'route_stop_id' => 243,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 391,
                'route_stop_id' => 244,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 392,
                'route_stop_id' => 245,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 393,
                'route_stop_id' => 246,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 394,
                'route_stop_id' => 247,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 395,
                'route_stop_id' => 248,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 396,
                'route_stop_id' => 249,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 397,
                'route_stop_id' => 250,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 398,
                'route_stop_id' => 251,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 399,
                'route_stop_id' => 252,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 400,
                'route_stop_id' => 253,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 401,
                'route_stop_id' => 254,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 402,
                'route_stop_id' => 255,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 403,
                'route_stop_id' => 256,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 404,
                'route_stop_id' => 257,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 405,
                'route_stop_id' => 258,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 406,
                'route_stop_id' => 259,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 407,
                'route_stop_id' => 260,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 408,
                'route_stop_id' => 261,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 409,
                'route_stop_id' => 262,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 410,
                'route_stop_id' => 263,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 411,
                'route_stop_id' => 264,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 412,
                'route_stop_id' => 265,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 413,
                'route_stop_id' => 266,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 414,
                'route_stop_id' => 267,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 415,
                'route_stop_id' => 268,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 416,
                'route_stop_id' => 269,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 417,
                'route_stop_id' => 270,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 418,
                'route_stop_id' => 271,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 419,
                'route_stop_id' => 272,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 420,
                'route_stop_id' => 273,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 421,
                'route_stop_id' => 274,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 422,
                'route_stop_id' => 275,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 423,
                'route_stop_id' => 276,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 424,
                'route_stop_id' => 277,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 425,
                'route_stop_id' => 278,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 426,
                'route_stop_id' => 279,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 427,
                'route_stop_id' => 280,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 428,
                'route_stop_id' => 281,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 429,
                'route_stop_id' => 282,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 430,
                'route_stop_id' => 283,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 431,
                'route_stop_id' => 284,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 432,
                'route_stop_id' => 285,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 433,
                'route_stop_id' => 286,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 434,
                'route_stop_id' => 287,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 435,
                'route_stop_id' => 288,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 436,
                'route_stop_id' => 289,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 437,
                'route_stop_id' => 290,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 438,
                'route_stop_id' => 291,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 439,
                'route_stop_id' => 292,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 440,
                'route_stop_id' => 293,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 441,
                'route_stop_id' => 294,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 442,
                'route_stop_id' => 295,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 443,
                'route_stop_id' => 296,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 444,
                'route_stop_id' => 297,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 445,
                'route_stop_id' => 298,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 446,
                'route_stop_id' => 299,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 447,
                'route_stop_id' => 300,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 448,
                'route_stop_id' => 301,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 449,
                'route_stop_id' => 302,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 450,
                'route_stop_id' => 303,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 451,
                'route_stop_id' => 304,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 452,
                'route_stop_id' => 305,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 453,
                'route_stop_id' => 306,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 454,
                'route_stop_id' => 307,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 455,
                'route_stop_id' => 308,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 456,
                'route_stop_id' => 309,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 457,
                'route_stop_id' => 310,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 458,
                'route_stop_id' => 311,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 459,
                'route_stop_id' => 312,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 460,
                'route_stop_id' => 313,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 461,
                'route_stop_id' => 314,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 462,
                'route_stop_id' => 315,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 463,
                'route_stop_id' => 316,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 464,
                'route_stop_id' => 317,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 465,
                'route_stop_id' => 318,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 466,
                'route_stop_id' => 319,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 467,
                'route_stop_id' => 320,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 468,
                'route_stop_id' => 321,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 469,
                'route_stop_id' => 322,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 470,
                'route_stop_id' => 323,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 471,
                'route_stop_id' => 324,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 472,
                'route_stop_id' => 325,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 473,
                'route_stop_id' => 326,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 474,
                'route_stop_id' => 327,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 475,
                'route_stop_id' => 328,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 476,
                'route_stop_id' => 329,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 477,
                'route_stop_id' => 330,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 478,
                'route_stop_id' => 331,
                'month_type' => 'normal',
                'day_types' => 'sun,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 479,
                'route_stop_id' => 332,
                'month_type' => 'normal',
                'day_types' => 'sun,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 480,
                'route_stop_id' => 333,
                'month_type' => 'normal',
                'day_types' => 'sun,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 481,
                'route_stop_id' => 334,
                'month_type' => 'normal',
                'day_types' => 'sun,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 489,
                'route_stop_id' => 342,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 496,
                'route_stop_id' => 349,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 497,
                'route_stop_id' => 350,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 498,
                'route_stop_id' => 351,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 499,
                'route_stop_id' => 352,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 500,
                'route_stop_id' => 353,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 501,
                'route_stop_id' => 354,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 514,
                'route_stop_id' => 367,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 515,
                'route_stop_id' => 368,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 516,
                'route_stop_id' => 369,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 517,
                'route_stop_id' => 370,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 518,
                'route_stop_id' => 371,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 519,
                'route_stop_id' => 372,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 520,
                'route_stop_id' => 373,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 521,
                'route_stop_id' => 374,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 522,
                'route_stop_id' => 375,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 523,
                'route_stop_id' => 376,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 524,
                'route_stop_id' => 377,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 525,
                'route_stop_id' => 378,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 526,
                'route_stop_id' => 379,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 527,
                'route_stop_id' => 380,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 528,
                'route_stop_id' => 381,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 529,
                'route_stop_id' => 382,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 530,
                'route_stop_id' => 383,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 531,
                'route_stop_id' => 384,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 532,
                'route_stop_id' => 385,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 533,
                'route_stop_id' => 386,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 535,
                'route_stop_id' => 388,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 536,
                'route_stop_id' => 389,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 537,
                'route_stop_id' => 390,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 538,
                'route_stop_id' => 391,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 539,
                'route_stop_id' => 392,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 540,
                'route_stop_id' => 393,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 541,
                'route_stop_id' => 394,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 542,
                'route_stop_id' => 395,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 543,
                'route_stop_id' => 396,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 544,
                'route_stop_id' => 397,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 545,
                'route_stop_id' => 398,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 546,
                'route_stop_id' => 399,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 548,
                'route_stop_id' => 401,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 549,
                'route_stop_id' => 402,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 550,
                'route_stop_id' => 403,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 551,
                'route_stop_id' => 404,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 556,
                'route_stop_id' => 409,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 557,
                'route_stop_id' => 410,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 558,
                'route_stop_id' => 411,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 559,
                'route_stop_id' => 412,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 560,
                'route_stop_id' => 413,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 561,
                'route_stop_id' => 414,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 562,
                'route_stop_id' => 415,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 563,
                'route_stop_id' => 416,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 564,
                'route_stop_id' => 417,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 565,
                'route_stop_id' => 418,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 566,
                'route_stop_id' => 419,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 567,
                'route_stop_id' => 420,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 568,
                'route_stop_id' => 421,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 569,
                'route_stop_id' => 422,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 570,
                'route_stop_id' => 423,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 571,
                'route_stop_id' => 424,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 572,
                'route_stop_id' => 425,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 573,
                'route_stop_id' => 426,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 574,
                'route_stop_id' => 427,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 575,
                'route_stop_id' => 428,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 576,
                'route_stop_id' => 429,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 577,
                'route_stop_id' => 430,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 578,
                'route_stop_id' => 431,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 579,
                'route_stop_id' => 432,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 580,
                'route_stop_id' => 433,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 581,
                'route_stop_id' => 434,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 582,
                'route_stop_id' => 435,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 583,
                'route_stop_id' => 436,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 584,
                'route_stop_id' => 437,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 585,
                'route_stop_id' => 438,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 586,
                'route_stop_id' => 439,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 587,
                'route_stop_id' => 440,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 588,
                'route_stop_id' => 441,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 589,
                'route_stop_id' => 442,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 590,
                'route_stop_id' => 443,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 591,
                'route_stop_id' => 444,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 592,
                'route_stop_id' => 445,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 593,
                'route_stop_id' => 446,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 594,
                'route_stop_id' => 447,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 596,
                'route_stop_id' => 449,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 599,
                'route_stop_id' => 452,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 601,
                'route_stop_id' => 454,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 602,
                'route_stop_id' => 455,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 603,
                'route_stop_id' => 456,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 604,
                'route_stop_id' => 457,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 605,
                'route_stop_id' => 458,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 606,
                'route_stop_id' => 459,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 607,
                'route_stop_id' => 460,
                'month_type' => 'normal',
                'day_types' => 'tue',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 608,
                'route_stop_id' => 461,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 609,
                'route_stop_id' => 462,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 610,
                'route_stop_id' => 463,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 611,
                'route_stop_id' => 464,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 612,
                'route_stop_id' => 465,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 613,
                'route_stop_id' => 466,
                'month_type' => 'normal',
                'day_types' => 'sun',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 614,
                'route_stop_id' => 467,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 615,
                'route_stop_id' => 468,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 616,
                'route_stop_id' => 469,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 617,
                'route_stop_id' => 470,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 618,
                'route_stop_id' => 471,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 619,
                'route_stop_id' => 472,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 620,
                'route_stop_id' => 473,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 621,
                'route_stop_id' => 474,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 622,
                'route_stop_id' => 475,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 623,
                'route_stop_id' => 476,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 624,
                'route_stop_id' => 477,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 625,
                'route_stop_id' => 478,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 626,
                'route_stop_id' => 479,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 627,
                'route_stop_id' => 480,
                'month_type' => 'normal',
                'day_types' => 'thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 628,
                'route_stop_id' => 481,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 629,
                'route_stop_id' => 482,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 630,
                'route_stop_id' => 483,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 631,
                'route_stop_id' => 484,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 632,
                'route_stop_id' => 485,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 633,
                'route_stop_id' => 486,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 634,
                'route_stop_id' => 487,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 635,
                'route_stop_id' => 488,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 636,
                'route_stop_id' => 489,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 637,
                'route_stop_id' => 490,
                'month_type' => 'normal',
                'day_types' => 'sun,mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 638,
                'route_stop_id' => 491,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 639,
                'route_stop_id' => 492,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 640,
                'route_stop_id' => 493,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 641,
                'route_stop_id' => 494,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 642,
                'route_stop_id' => 495,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 643,
                'route_stop_id' => 496,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 644,
                'route_stop_id' => 497,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 645,
                'route_stop_id' => 498,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 646,
                'route_stop_id' => 499,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 647,
                'route_stop_id' => 500,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 648,
                'route_stop_id' => 501,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 649,
                'route_stop_id' => 502,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 650,
                'route_stop_id' => 503,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 651,
                'route_stop_id' => 504,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 652,
                'route_stop_id' => 505,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 653,
                'route_stop_id' => 506,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 654,
                'route_stop_id' => 507,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 655,
                'route_stop_id' => 508,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 656,
                'route_stop_id' => 509,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 657,
                'route_stop_id' => 510,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 658,
                'route_stop_id' => 511,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 659,
                'route_stop_id' => 512,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 660,
                'route_stop_id' => 513,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 1763,
                'route_stop_id' => 1616,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 1764,
                'route_stop_id' => 1617,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 1765,
                'route_stop_id' => 1618,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 1766,
                'route_stop_id' => 1619,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 1767,
                'route_stop_id' => 1620,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 1768,
                'route_stop_id' => 1621,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 1769,
                'route_stop_id' => 1622,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 1770,
                'route_stop_id' => 1623,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 1771,
                'route_stop_id' => 1624,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 1772,
                'route_stop_id' => 1625,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 1773,
                'route_stop_id' => 1626,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 1774,
                'route_stop_id' => 1627,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 1775,
                'route_stop_id' => 1628,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 1776,
                'route_stop_id' => 1629,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 1777,
                'route_stop_id' => 1630,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 1778,
                'route_stop_id' => 1631,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 1779,
                'route_stop_id' => 1632,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 1780,
                'route_stop_id' => 1633,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 1781,
                'route_stop_id' => 1634,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 1782,
                'route_stop_id' => 1635,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 1783,
                'route_stop_id' => 1636,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 1784,
                'route_stop_id' => 1637,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 1785,
                'route_stop_id' => 1638,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 1786,
                'route_stop_id' => 1639,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 1787,
                'route_stop_id' => 1640,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 1788,
                'route_stop_id' => 1641,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 1789,
                'route_stop_id' => 1642,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 1790,
                'route_stop_id' => 1643,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 1791,
                'route_stop_id' => 1644,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 1792,
                'route_stop_id' => 1645,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 1793,
                'route_stop_id' => 1646,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 1794,
                'route_stop_id' => 1647,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 1795,
                'route_stop_id' => 1648,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 1796,
                'route_stop_id' => 1649,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 1797,
                'route_stop_id' => 1650,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 1798,
                'route_stop_id' => 1651,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 1799,
                'route_stop_id' => 1652,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 1800,
                'route_stop_id' => 1653,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 1801,
                'route_stop_id' => 1654,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 1802,
                'route_stop_id' => 1655,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 1803,
                'route_stop_id' => 1656,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 1804,
                'route_stop_id' => 1657,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 1805,
                'route_stop_id' => 1658,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 1806,
                'route_stop_id' => 1659,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 1807,
                'route_stop_id' => 1660,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 1808,
                'route_stop_id' => 1661,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 1809,
                'route_stop_id' => 1662,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 1810,
                'route_stop_id' => 1663,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 1811,
                'route_stop_id' => 1664,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 1812,
                'route_stop_id' => 1665,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 1813,
                'route_stop_id' => 1666,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 1814,
                'route_stop_id' => 1667,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 1815,
                'route_stop_id' => 1668,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 1816,
                'route_stop_id' => 1669,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 1817,
                'route_stop_id' => 1670,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 1818,
                'route_stop_id' => 1671,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 1819,
                'route_stop_id' => 1672,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 1820,
                'route_stop_id' => 1673,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 1821,
                'route_stop_id' => 1674,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 1822,
                'route_stop_id' => 1675,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 1823,
                'route_stop_id' => 1676,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 1824,
                'route_stop_id' => 1677,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 1825,
                'route_stop_id' => 1678,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 1826,
                'route_stop_id' => 1679,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 1827,
                'route_stop_id' => 1680,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 1828,
                'route_stop_id' => 1681,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 1829,
                'route_stop_id' => 1682,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 1830,
                'route_stop_id' => 1683,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 1831,
                'route_stop_id' => 1684,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 1832,
                'route_stop_id' => 1685,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 1833,
                'route_stop_id' => 1686,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 1834,
                'route_stop_id' => 1687,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 1835,
                'route_stop_id' => 1688,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 1836,
                'route_stop_id' => 1689,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 1837,
                'route_stop_id' => 1690,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 1838,
                'route_stop_id' => 1691,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 1839,
                'route_stop_id' => 1692,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 1840,
                'route_stop_id' => 1693,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 1841,
                'route_stop_id' => 1694,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 1842,
                'route_stop_id' => 1695,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 1843,
                'route_stop_id' => 1696,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 1844,
                'route_stop_id' => 1697,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 1845,
                'route_stop_id' => 1698,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 1846,
                'route_stop_id' => 1699,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 1847,
                'route_stop_id' => 1700,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 1848,
                'route_stop_id' => 1701,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 1849,
                'route_stop_id' => 1702,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 1850,
                'route_stop_id' => 1703,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 1851,
                'route_stop_id' => 1704,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 1852,
                'route_stop_id' => 1705,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 1853,
                'route_stop_id' => 1706,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 1854,
                'route_stop_id' => 1707,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 1855,
                'route_stop_id' => 1708,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 1856,
                'route_stop_id' => 1709,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 1857,
                'route_stop_id' => 1710,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 1858,
                'route_stop_id' => 1711,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 1859,
                'route_stop_id' => 1712,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 1860,
                'route_stop_id' => 1713,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 1861,
                'route_stop_id' => 1714,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 1862,
                'route_stop_id' => 1715,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 1863,
                'route_stop_id' => 1716,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 1864,
                'route_stop_id' => 1717,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_days')->insert(array (
            0 => 
            array (
                'id' => 1865,
                'route_stop_id' => 1718,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1866,
                'route_stop_id' => 1719,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 1867,
                'route_stop_id' => 1720,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 1868,
                'route_stop_id' => 1721,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 1869,
                'route_stop_id' => 1722,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 1870,
                'route_stop_id' => 1723,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 1871,
                'route_stop_id' => 1724,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 1872,
                'route_stop_id' => 1725,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 1873,
                'route_stop_id' => 1726,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 1874,
                'route_stop_id' => 1727,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 1875,
                'route_stop_id' => 1728,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 1876,
                'route_stop_id' => 1729,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 1877,
                'route_stop_id' => 1730,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 1878,
                'route_stop_id' => 1731,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 1879,
                'route_stop_id' => 1732,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 1880,
                'route_stop_id' => 1733,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 1881,
                'route_stop_id' => 1734,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 1882,
                'route_stop_id' => 1735,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 1883,
                'route_stop_id' => 1736,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 1884,
                'route_stop_id' => 1737,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 1885,
                'route_stop_id' => 1738,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 1886,
                'route_stop_id' => 1739,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 1887,
                'route_stop_id' => 1740,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 1888,
                'route_stop_id' => 1741,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 1889,
                'route_stop_id' => 1742,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 1890,
                'route_stop_id' => 1743,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 1891,
                'route_stop_id' => 1744,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 1892,
                'route_stop_id' => 1745,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 1893,
                'route_stop_id' => 1746,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 1894,
                'route_stop_id' => 1747,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 1895,
                'route_stop_id' => 1748,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 1896,
                'route_stop_id' => 1749,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 1897,
                'route_stop_id' => 1750,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 1898,
                'route_stop_id' => 1751,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 1899,
                'route_stop_id' => 1752,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 1900,
                'route_stop_id' => 1753,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 1901,
                'route_stop_id' => 1754,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 1902,
                'route_stop_id' => 1755,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 1903,
                'route_stop_id' => 1756,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 1904,
                'route_stop_id' => 1757,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 1905,
                'route_stop_id' => 1758,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 1906,
                'route_stop_id' => 1759,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 1907,
                'route_stop_id' => 1760,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 1908,
                'route_stop_id' => 1761,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 1909,
                'route_stop_id' => 1762,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 1910,
                'route_stop_id' => 1763,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 1911,
                'route_stop_id' => 1764,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 1912,
                'route_stop_id' => 1765,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 1913,
                'route_stop_id' => 1766,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 1914,
                'route_stop_id' => 1767,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 1915,
                'route_stop_id' => 1768,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 1916,
                'route_stop_id' => 1769,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 1917,
                'route_stop_id' => 1770,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 1918,
                'route_stop_id' => 1771,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 1919,
                'route_stop_id' => 1772,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 1920,
                'route_stop_id' => 1773,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 1921,
                'route_stop_id' => 1774,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 1922,
                'route_stop_id' => 1775,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 1923,
                'route_stop_id' => 1776,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 1924,
                'route_stop_id' => 1777,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 1925,
                'route_stop_id' => 1778,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 1926,
                'route_stop_id' => 1779,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 1927,
                'route_stop_id' => 1780,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 1928,
                'route_stop_id' => 1781,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 1929,
                'route_stop_id' => 1782,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 1930,
                'route_stop_id' => 1783,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 1931,
                'route_stop_id' => 1784,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 1932,
                'route_stop_id' => 1785,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 1933,
                'route_stop_id' => 1786,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 1934,
                'route_stop_id' => 1787,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 1935,
                'route_stop_id' => 1788,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 1936,
                'route_stop_id' => 1789,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 1937,
                'route_stop_id' => 1790,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 1938,
                'route_stop_id' => 1791,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 1939,
                'route_stop_id' => 1792,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 1940,
                'route_stop_id' => 1793,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 1941,
                'route_stop_id' => 1794,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 1942,
                'route_stop_id' => 1795,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 1943,
                'route_stop_id' => 1796,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 1944,
                'route_stop_id' => 1797,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 1945,
                'route_stop_id' => 1798,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 1946,
                'route_stop_id' => 1799,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 1947,
                'route_stop_id' => 1800,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 1948,
                'route_stop_id' => 1801,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 1949,
                'route_stop_id' => 1802,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 1950,
                'route_stop_id' => 1803,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 1951,
                'route_stop_id' => 1804,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 1952,
                'route_stop_id' => 1805,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 1953,
                'route_stop_id' => 1806,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 1954,
                'route_stop_id' => 1807,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 1955,
                'route_stop_id' => 1808,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 1956,
                'route_stop_id' => 1809,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 1957,
                'route_stop_id' => 1810,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 1958,
                'route_stop_id' => 1811,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 1959,
                'route_stop_id' => 1812,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 1960,
                'route_stop_id' => 1813,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 1961,
                'route_stop_id' => 1814,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 1962,
                'route_stop_id' => 1815,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 1963,
                'route_stop_id' => 1816,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 1964,
                'route_stop_id' => 1817,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 1965,
                'route_stop_id' => 1818,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 1966,
                'route_stop_id' => 1819,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 1967,
                'route_stop_id' => 1820,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 1968,
                'route_stop_id' => 1821,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 1969,
                'route_stop_id' => 1822,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 1970,
                'route_stop_id' => 1823,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 1971,
                'route_stop_id' => 1824,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 1972,
                'route_stop_id' => 1825,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 1973,
                'route_stop_id' => 1826,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 1974,
                'route_stop_id' => 1827,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 1975,
                'route_stop_id' => 1828,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 1976,
                'route_stop_id' => 1829,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 1977,
                'route_stop_id' => 1830,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 1978,
                'route_stop_id' => 1831,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 1979,
                'route_stop_id' => 1832,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 1980,
                'route_stop_id' => 1833,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 1981,
                'route_stop_id' => 1834,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 1982,
                'route_stop_id' => 1835,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 1983,
                'route_stop_id' => 1836,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 1984,
                'route_stop_id' => 1837,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 1985,
                'route_stop_id' => 1838,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 1986,
                'route_stop_id' => 1839,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 1987,
                'route_stop_id' => 1840,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 1988,
                'route_stop_id' => 1841,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 1989,
                'route_stop_id' => 1842,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 1990,
                'route_stop_id' => 1843,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 1991,
                'route_stop_id' => 1844,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 1992,
                'route_stop_id' => 1845,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 1993,
                'route_stop_id' => 1846,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 1994,
                'route_stop_id' => 1847,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 1995,
                'route_stop_id' => 1848,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 1996,
                'route_stop_id' => 1849,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 1997,
                'route_stop_id' => 1850,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 1998,
                'route_stop_id' => 1851,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 1999,
                'route_stop_id' => 1852,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 2000,
                'route_stop_id' => 1853,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 2001,
                'route_stop_id' => 1854,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 2002,
                'route_stop_id' => 1855,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 2003,
                'route_stop_id' => 1856,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 2004,
                'route_stop_id' => 1857,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 2005,
                'route_stop_id' => 1858,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 2006,
                'route_stop_id' => 1859,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 2007,
                'route_stop_id' => 1860,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 2008,
                'route_stop_id' => 1861,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 2009,
                'route_stop_id' => 1862,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 2010,
                'route_stop_id' => 1863,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 2011,
                'route_stop_id' => 1864,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 2012,
                'route_stop_id' => 1865,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 2013,
                'route_stop_id' => 1866,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 2014,
                'route_stop_id' => 1867,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 2015,
                'route_stop_id' => 1868,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 2016,
                'route_stop_id' => 1869,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 2017,
                'route_stop_id' => 1870,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 2018,
                'route_stop_id' => 1871,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 2019,
                'route_stop_id' => 1872,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 2020,
                'route_stop_id' => 1873,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 2021,
                'route_stop_id' => 1874,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 2022,
                'route_stop_id' => 1875,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 2023,
                'route_stop_id' => 1876,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 2024,
                'route_stop_id' => 1877,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 2025,
                'route_stop_id' => 1878,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 2026,
                'route_stop_id' => 1879,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 2027,
                'route_stop_id' => 1880,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 2028,
                'route_stop_id' => 1881,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 2029,
                'route_stop_id' => 1882,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 2030,
                'route_stop_id' => 1883,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 2031,
                'route_stop_id' => 1884,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 2032,
                'route_stop_id' => 1885,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 2033,
                'route_stop_id' => 1886,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 2034,
                'route_stop_id' => 1887,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 2035,
                'route_stop_id' => 1888,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 2036,
                'route_stop_id' => 1889,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 2037,
                'route_stop_id' => 1890,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 2038,
                'route_stop_id' => 1891,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 2039,
                'route_stop_id' => 1892,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 2040,
                'route_stop_id' => 1893,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 2041,
                'route_stop_id' => 1894,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 2042,
                'route_stop_id' => 1895,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 2043,
                'route_stop_id' => 1896,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 2044,
                'route_stop_id' => 1897,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 2045,
                'route_stop_id' => 1898,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 2046,
                'route_stop_id' => 1899,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 2047,
                'route_stop_id' => 1900,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 2048,
                'route_stop_id' => 1901,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 2049,
                'route_stop_id' => 1902,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 2050,
                'route_stop_id' => 1903,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 2051,
                'route_stop_id' => 1904,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 2052,
                'route_stop_id' => 1905,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 2053,
                'route_stop_id' => 1906,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 2054,
                'route_stop_id' => 1907,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 2055,
                'route_stop_id' => 1908,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 2056,
                'route_stop_id' => 1909,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 2057,
                'route_stop_id' => 1910,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 2058,
                'route_stop_id' => 1911,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 2059,
                'route_stop_id' => 1912,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 2060,
                'route_stop_id' => 1913,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 2061,
                'route_stop_id' => 1914,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 2062,
                'route_stop_id' => 1915,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 2063,
                'route_stop_id' => 1916,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 2064,
                'route_stop_id' => 1917,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 2065,
                'route_stop_id' => 1918,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 2066,
                'route_stop_id' => 1919,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 2067,
                'route_stop_id' => 1920,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 2068,
                'route_stop_id' => 1921,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 2069,
                'route_stop_id' => 1922,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 2070,
                'route_stop_id' => 1923,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 2071,
                'route_stop_id' => 1924,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 2072,
                'route_stop_id' => 1925,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 2073,
                'route_stop_id' => 1926,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 2074,
                'route_stop_id' => 1927,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 2075,
                'route_stop_id' => 1928,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 2076,
                'route_stop_id' => 1929,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 2077,
                'route_stop_id' => 1930,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 2078,
                'route_stop_id' => 1931,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 2079,
                'route_stop_id' => 1932,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 2080,
                'route_stop_id' => 1933,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 2081,
                'route_stop_id' => 1934,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 2082,
                'route_stop_id' => 1935,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 2083,
                'route_stop_id' => 1936,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 2084,
                'route_stop_id' => 1937,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 2085,
                'route_stop_id' => 1938,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 2086,
                'route_stop_id' => 1939,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 2087,
                'route_stop_id' => 1940,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 2088,
                'route_stop_id' => 1941,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 2089,
                'route_stop_id' => 1942,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 2090,
                'route_stop_id' => 1943,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 2091,
                'route_stop_id' => 1944,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 2092,
                'route_stop_id' => 1945,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 2093,
                'route_stop_id' => 1946,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 2094,
                'route_stop_id' => 1947,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 2095,
                'route_stop_id' => 1948,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 2096,
                'route_stop_id' => 1949,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 2097,
                'route_stop_id' => 1950,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 2098,
                'route_stop_id' => 1951,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 2099,
                'route_stop_id' => 1952,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 2100,
                'route_stop_id' => 1953,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 2101,
                'route_stop_id' => 1954,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 2102,
                'route_stop_id' => 1955,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 2103,
                'route_stop_id' => 1956,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 2104,
                'route_stop_id' => 1957,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 2105,
                'route_stop_id' => 1958,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 2106,
                'route_stop_id' => 1959,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 2107,
                'route_stop_id' => 1960,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 2108,
                'route_stop_id' => 1961,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 2109,
                'route_stop_id' => 1962,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 2110,
                'route_stop_id' => 1963,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 2111,
                'route_stop_id' => 1964,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 2112,
                'route_stop_id' => 1965,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 2113,
                'route_stop_id' => 1966,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 2114,
                'route_stop_id' => 1967,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 2115,
                'route_stop_id' => 1968,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 2116,
                'route_stop_id' => 1969,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 2117,
                'route_stop_id' => 1970,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 2118,
                'route_stop_id' => 1971,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 2119,
                'route_stop_id' => 1972,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 2120,
                'route_stop_id' => 1973,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 2121,
                'route_stop_id' => 1974,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 2122,
                'route_stop_id' => 1975,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 2123,
                'route_stop_id' => 1976,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 2124,
                'route_stop_id' => 1977,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 2125,
                'route_stop_id' => 1978,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 2126,
                'route_stop_id' => 1979,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 2127,
                'route_stop_id' => 1980,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 2128,
                'route_stop_id' => 1981,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 2129,
                'route_stop_id' => 1982,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 2130,
                'route_stop_id' => 1983,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 2131,
                'route_stop_id' => 1984,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 2132,
                'route_stop_id' => 1985,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 2133,
                'route_stop_id' => 1986,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 2134,
                'route_stop_id' => 1987,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 2135,
                'route_stop_id' => 1988,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 2136,
                'route_stop_id' => 1989,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 2137,
                'route_stop_id' => 1990,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 2138,
                'route_stop_id' => 1991,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 2139,
                'route_stop_id' => 1992,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 2140,
                'route_stop_id' => 1993,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 2141,
                'route_stop_id' => 1994,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 2142,
                'route_stop_id' => 1995,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 2143,
                'route_stop_id' => 1996,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 2144,
                'route_stop_id' => 1997,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 2145,
                'route_stop_id' => 1998,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 2146,
                'route_stop_id' => 1999,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 2147,
                'route_stop_id' => 2000,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 2148,
                'route_stop_id' => 2001,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 2149,
                'route_stop_id' => 2002,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 2150,
                'route_stop_id' => 2003,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 2151,
                'route_stop_id' => 2004,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 2152,
                'route_stop_id' => 2005,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 2153,
                'route_stop_id' => 2006,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 2154,
                'route_stop_id' => 2007,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 2155,
                'route_stop_id' => 2008,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 2156,
                'route_stop_id' => 2009,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 2157,
                'route_stop_id' => 2010,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 2158,
                'route_stop_id' => 2011,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 2159,
                'route_stop_id' => 2012,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 2160,
                'route_stop_id' => 2013,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 2161,
                'route_stop_id' => 2014,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 2162,
                'route_stop_id' => 2015,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 2163,
                'route_stop_id' => 2016,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 2164,
                'route_stop_id' => 2017,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 2165,
                'route_stop_id' => 2018,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 2166,
                'route_stop_id' => 2019,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 2167,
                'route_stop_id' => 2020,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 2168,
                'route_stop_id' => 2021,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 2169,
                'route_stop_id' => 2022,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 2170,
                'route_stop_id' => 2023,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 2171,
                'route_stop_id' => 2024,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 2172,
                'route_stop_id' => 2025,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 2173,
                'route_stop_id' => 2026,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 2174,
                'route_stop_id' => 2027,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 2175,
                'route_stop_id' => 2028,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 2176,
                'route_stop_id' => 2029,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 2177,
                'route_stop_id' => 2030,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 2178,
                'route_stop_id' => 2031,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 2179,
                'route_stop_id' => 2032,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 2180,
                'route_stop_id' => 2033,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 2181,
                'route_stop_id' => 2034,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 2182,
                'route_stop_id' => 2035,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 2183,
                'route_stop_id' => 2036,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 2184,
                'route_stop_id' => 2037,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 2185,
                'route_stop_id' => 2038,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 2186,
                'route_stop_id' => 2039,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 2187,
                'route_stop_id' => 2040,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 2188,
                'route_stop_id' => 2041,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 2189,
                'route_stop_id' => 2042,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 2190,
                'route_stop_id' => 2043,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 2191,
                'route_stop_id' => 2044,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 2192,
                'route_stop_id' => 2045,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 2193,
                'route_stop_id' => 2046,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 2194,
                'route_stop_id' => 2047,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 2195,
                'route_stop_id' => 2048,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 2196,
                'route_stop_id' => 2049,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 2197,
                'route_stop_id' => 2050,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 2198,
                'route_stop_id' => 2051,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 2199,
                'route_stop_id' => 2052,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 2200,
                'route_stop_id' => 2053,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 2201,
                'route_stop_id' => 2054,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 2202,
                'route_stop_id' => 2055,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 2203,
                'route_stop_id' => 2056,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 2204,
                'route_stop_id' => 2057,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 2205,
                'route_stop_id' => 2058,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 2206,
                'route_stop_id' => 2059,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 2207,
                'route_stop_id' => 2060,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 2208,
                'route_stop_id' => 2061,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 2209,
                'route_stop_id' => 2062,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 2210,
                'route_stop_id' => 2063,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 2211,
                'route_stop_id' => 2064,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 2212,
                'route_stop_id' => 2065,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 2213,
                'route_stop_id' => 2066,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 2214,
                'route_stop_id' => 2067,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 2215,
                'route_stop_id' => 2068,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 2216,
                'route_stop_id' => 2069,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 2217,
                'route_stop_id' => 2070,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 2218,
                'route_stop_id' => 2071,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 2219,
                'route_stop_id' => 2072,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 2220,
                'route_stop_id' => 2073,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 2221,
                'route_stop_id' => 2074,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 2222,
                'route_stop_id' => 2075,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 2223,
                'route_stop_id' => 2076,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 2224,
                'route_stop_id' => 2077,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 2225,
                'route_stop_id' => 2078,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 2226,
                'route_stop_id' => 2079,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 2227,
                'route_stop_id' => 2080,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 2228,
                'route_stop_id' => 2081,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 2229,
                'route_stop_id' => 2082,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 2230,
                'route_stop_id' => 2083,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 2231,
                'route_stop_id' => 2084,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 2232,
                'route_stop_id' => 2085,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 2233,
                'route_stop_id' => 2086,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 2234,
                'route_stop_id' => 2087,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 2235,
                'route_stop_id' => 2088,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 2236,
                'route_stop_id' => 2089,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 2237,
                'route_stop_id' => 2090,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 2238,
                'route_stop_id' => 2091,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 2239,
                'route_stop_id' => 2092,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 2240,
                'route_stop_id' => 2093,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 2241,
                'route_stop_id' => 2094,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 2242,
                'route_stop_id' => 2095,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 2243,
                'route_stop_id' => 2096,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 2244,
                'route_stop_id' => 2097,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 2245,
                'route_stop_id' => 2098,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 2246,
                'route_stop_id' => 2099,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 2247,
                'route_stop_id' => 2100,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 2248,
                'route_stop_id' => 2101,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 2249,
                'route_stop_id' => 2102,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 2250,
                'route_stop_id' => 2103,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 2251,
                'route_stop_id' => 2104,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 2252,
                'route_stop_id' => 2105,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 2253,
                'route_stop_id' => 2106,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 2254,
                'route_stop_id' => 2107,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 2255,
                'route_stop_id' => 2108,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 2256,
                'route_stop_id' => 2109,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 2257,
                'route_stop_id' => 2110,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 2258,
                'route_stop_id' => 2111,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 2259,
                'route_stop_id' => 2112,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 2260,
                'route_stop_id' => 2113,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 2261,
                'route_stop_id' => 2114,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 2262,
                'route_stop_id' => 2115,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 2263,
                'route_stop_id' => 2116,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 2264,
                'route_stop_id' => 2117,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 2265,
                'route_stop_id' => 2118,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 2266,
                'route_stop_id' => 2119,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 2267,
                'route_stop_id' => 2120,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 2268,
                'route_stop_id' => 2121,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 2269,
                'route_stop_id' => 2122,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 2270,
                'route_stop_id' => 2123,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 2271,
                'route_stop_id' => 2124,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 2272,
                'route_stop_id' => 2125,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 2273,
                'route_stop_id' => 2126,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 2274,
                'route_stop_id' => 2127,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 2275,
                'route_stop_id' => 2128,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 2276,
                'route_stop_id' => 2129,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 2277,
                'route_stop_id' => 2130,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 2278,
                'route_stop_id' => 2131,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 2279,
                'route_stop_id' => 2132,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 2280,
                'route_stop_id' => 2133,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 2281,
                'route_stop_id' => 2134,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 2282,
                'route_stop_id' => 2135,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 2283,
                'route_stop_id' => 2136,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 2284,
                'route_stop_id' => 2137,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 2285,
                'route_stop_id' => 2138,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 2286,
                'route_stop_id' => 2139,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 2287,
                'route_stop_id' => 2140,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 2288,
                'route_stop_id' => 2141,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 2289,
                'route_stop_id' => 2142,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 2290,
                'route_stop_id' => 2143,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 2291,
                'route_stop_id' => 2144,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 2292,
                'route_stop_id' => 2145,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 2293,
                'route_stop_id' => 2146,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 2294,
                'route_stop_id' => 2147,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 2295,
                'route_stop_id' => 2148,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 2296,
                'route_stop_id' => 2149,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 2297,
                'route_stop_id' => 2150,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 2298,
                'route_stop_id' => 2151,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 2299,
                'route_stop_id' => 2152,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 2300,
                'route_stop_id' => 2153,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 2301,
                'route_stop_id' => 2154,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 2302,
                'route_stop_id' => 2155,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 2303,
                'route_stop_id' => 2156,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 2304,
                'route_stop_id' => 2157,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 2305,
                'route_stop_id' => 2158,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 2306,
                'route_stop_id' => 2159,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 2307,
                'route_stop_id' => 2160,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 2308,
                'route_stop_id' => 2161,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 2309,
                'route_stop_id' => 2162,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 2310,
                'route_stop_id' => 2163,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 2311,
                'route_stop_id' => 2164,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 2312,
                'route_stop_id' => 2165,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 2313,
                'route_stop_id' => 2166,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 2314,
                'route_stop_id' => 2167,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 2315,
                'route_stop_id' => 2168,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 2316,
                'route_stop_id' => 2169,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 2317,
                'route_stop_id' => 2170,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 2318,
                'route_stop_id' => 2171,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 2319,
                'route_stop_id' => 2172,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 2320,
                'route_stop_id' => 2173,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 2321,
                'route_stop_id' => 2174,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 2322,
                'route_stop_id' => 2175,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 2323,
                'route_stop_id' => 2176,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 2324,
                'route_stop_id' => 2177,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 2325,
                'route_stop_id' => 2178,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 2326,
                'route_stop_id' => 2179,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 2327,
                'route_stop_id' => 2180,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 2328,
                'route_stop_id' => 2181,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 2329,
                'route_stop_id' => 2182,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 2330,
                'route_stop_id' => 2183,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 2331,
                'route_stop_id' => 2184,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 2332,
                'route_stop_id' => 2185,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 2333,
                'route_stop_id' => 2186,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 2334,
                'route_stop_id' => 2187,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 2335,
                'route_stop_id' => 2188,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 2336,
                'route_stop_id' => 2189,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 2337,
                'route_stop_id' => 2190,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 2338,
                'route_stop_id' => 2191,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 2339,
                'route_stop_id' => 2192,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 2340,
                'route_stop_id' => 2193,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 2341,
                'route_stop_id' => 2194,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 2342,
                'route_stop_id' => 2195,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 2343,
                'route_stop_id' => 2196,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 2344,
                'route_stop_id' => 2197,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 2345,
                'route_stop_id' => 2198,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 2346,
                'route_stop_id' => 2199,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 2347,
                'route_stop_id' => 2200,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 2348,
                'route_stop_id' => 2201,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 2349,
                'route_stop_id' => 2202,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 2350,
                'route_stop_id' => 2203,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 2351,
                'route_stop_id' => 2204,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 2352,
                'route_stop_id' => 2205,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 2353,
                'route_stop_id' => 2206,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 2354,
                'route_stop_id' => 2207,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 2355,
                'route_stop_id' => 2208,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 2356,
                'route_stop_id' => 2209,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 2357,
                'route_stop_id' => 2210,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 2358,
                'route_stop_id' => 2211,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 2359,
                'route_stop_id' => 2212,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 2360,
                'route_stop_id' => 2213,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 2361,
                'route_stop_id' => 2214,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 2362,
                'route_stop_id' => 2215,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 2363,
                'route_stop_id' => 2216,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 2364,
                'route_stop_id' => 2217,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_days')->insert(array (
            0 => 
            array (
                'id' => 2365,
                'route_stop_id' => 2218,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2366,
                'route_stop_id' => 2219,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 2367,
                'route_stop_id' => 2220,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 2368,
                'route_stop_id' => 2221,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 2369,
                'route_stop_id' => 2222,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 2370,
                'route_stop_id' => 2223,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 2371,
                'route_stop_id' => 2224,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 2372,
                'route_stop_id' => 2225,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 2373,
                'route_stop_id' => 2226,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 2374,
                'route_stop_id' => 2227,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 2375,
                'route_stop_id' => 2228,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 2376,
                'route_stop_id' => 2229,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 2377,
                'route_stop_id' => 2230,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 2378,
                'route_stop_id' => 2231,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 2379,
                'route_stop_id' => 2232,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 2380,
                'route_stop_id' => 2233,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 2381,
                'route_stop_id' => 2234,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 2382,
                'route_stop_id' => 2235,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 2383,
                'route_stop_id' => 2236,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 2384,
                'route_stop_id' => 2237,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 2385,
                'route_stop_id' => 2238,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 2386,
                'route_stop_id' => 2239,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 2387,
                'route_stop_id' => 2240,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 2388,
                'route_stop_id' => 2241,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 2389,
                'route_stop_id' => 2242,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 2566,
                'route_stop_id' => 2419,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 2567,
                'route_stop_id' => 482,
                'month_type' => 'normal',
                'day_types' => 'fri',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 2568,
                'route_stop_id' => 481,
                'month_type' => 'normal',
                'day_types' => 'fri',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 2571,
                'route_stop_id' => 2421,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 2572,
                'route_stop_id' => 2422,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 2573,
                'route_stop_id' => 2423,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 2574,
                'route_stop_id' => 2424,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 2575,
                'route_stop_id' => 2425,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 2576,
                'route_stop_id' => 2426,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 2577,
                'route_stop_id' => 2427,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 2578,
                'route_stop_id' => 2428,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 2579,
                'route_stop_id' => 2429,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 2580,
                'route_stop_id' => 2430,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 2581,
                'route_stop_id' => 2431,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 2582,
                'route_stop_id' => 2432,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 2583,
                'route_stop_id' => 2433,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 2584,
                'route_stop_id' => 2434,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 2585,
                'route_stop_id' => 2435,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 2586,
                'route_stop_id' => 2436,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 2587,
                'route_stop_id' => 2437,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 2588,
                'route_stop_id' => 2438,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 2589,
                'route_stop_id' => 2439,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 2590,
                'route_stop_id' => 331,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 2591,
                'route_stop_id' => 2440,
                'month_type' => 'normal',
                'day_types' => 'tue,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 2592,
                'route_stop_id' => 2441,
                'month_type' => 'normal',
                'day_types' => 'mon,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 2593,
                'route_stop_id' => 332,
                'month_type' => 'normal',
                'day_types' => 'mon,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 2594,
                'route_stop_id' => 333,
                'month_type' => 'normal',
                'day_types' => 'mon,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 2595,
                'route_stop_id' => 2442,
                'month_type' => 'normal',
                'day_types' => 'mon,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 2596,
                'route_stop_id' => 57,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 2597,
                'route_stop_id' => 57,
                'month_type' => 'normal',
                'day_types' => 'fri',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 2598,
                'route_stop_id' => 2443,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 2599,
                'route_stop_id' => 2444,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 2600,
                'route_stop_id' => 2445,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 2601,
                'route_stop_id' => 2446,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 2602,
                'route_stop_id' => 2447,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 2603,
                'route_stop_id' => 2448,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 2604,
                'route_stop_id' => 2449,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 2605,
                'route_stop_id' => 2450,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 2606,
                'route_stop_id' => 2451,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 2607,
                'route_stop_id' => 2452,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 2608,
                'route_stop_id' => 2453,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 2609,
                'route_stop_id' => 2454,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 2618,
                'route_stop_id' => 2463,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 2619,
                'route_stop_id' => 2464,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 2620,
                'route_stop_id' => 2465,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 2621,
                'route_stop_id' => 2466,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 2622,
                'route_stop_id' => 2467,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 2623,
                'route_stop_id' => 2468,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 2624,
                'route_stop_id' => 2469,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 2625,
                'route_stop_id' => 2470,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 2626,
                'route_stop_id' => 2471,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 2627,
                'route_stop_id' => 2472,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 2628,
                'route_stop_id' => 2473,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 2629,
                'route_stop_id' => 2474,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 2630,
                'route_stop_id' => 2475,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 2631,
                'route_stop_id' => 2476,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 2632,
                'route_stop_id' => 2477,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 2633,
                'route_stop_id' => 2478,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 2634,
                'route_stop_id' => 2479,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 2635,
                'route_stop_id' => 2480,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 2636,
                'route_stop_id' => 2481,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 2637,
                'route_stop_id' => 2482,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 2638,
                'route_stop_id' => 2483,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 2639,
                'route_stop_id' => 2484,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 2640,
                'route_stop_id' => 2485,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 2641,
                'route_stop_id' => 2486,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 2642,
                'route_stop_id' => 2487,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 2643,
                'route_stop_id' => 2488,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 2644,
                'route_stop_id' => 2489,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 2645,
                'route_stop_id' => 2490,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 2646,
                'route_stop_id' => 2491,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 2647,
                'route_stop_id' => 2492,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 2648,
                'route_stop_id' => 2493,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 2649,
                'route_stop_id' => 2494,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 2650,
                'route_stop_id' => 2495,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 2651,
                'route_stop_id' => 2496,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 2652,
                'route_stop_id' => 2497,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 2653,
                'route_stop_id' => 2498,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 2654,
                'route_stop_id' => 2499,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 2655,
                'route_stop_id' => 2500,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 2656,
                'route_stop_id' => 2501,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 2657,
                'route_stop_id' => 2502,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 2658,
                'route_stop_id' => 2503,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 2659,
                'route_stop_id' => 2504,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 2660,
                'route_stop_id' => 2505,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 2661,
                'route_stop_id' => 2506,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 2662,
                'route_stop_id' => 2507,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 2663,
                'route_stop_id' => 2508,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 2664,
                'route_stop_id' => 2509,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 2665,
                'route_stop_id' => 2510,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 2666,
                'route_stop_id' => 2511,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 2667,
                'route_stop_id' => 2512,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 2668,
                'route_stop_id' => 2513,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 2669,
                'route_stop_id' => 2514,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 2670,
                'route_stop_id' => 2515,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 2671,
                'route_stop_id' => 2516,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 2672,
                'route_stop_id' => 2517,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 2673,
                'route_stop_id' => 2518,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 2674,
                'route_stop_id' => 2519,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 2675,
                'route_stop_id' => 2520,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 2676,
                'route_stop_id' => 2521,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 2677,
                'route_stop_id' => 2522,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 2678,
                'route_stop_id' => 2523,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 2679,
                'route_stop_id' => 2524,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 2680,
                'route_stop_id' => 2525,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 2681,
                'route_stop_id' => 2526,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 2682,
                'route_stop_id' => 2527,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 2683,
                'route_stop_id' => 2528,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 2684,
                'route_stop_id' => 2529,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 2685,
                'route_stop_id' => 2530,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 2686,
                'route_stop_id' => 2531,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 2687,
                'route_stop_id' => 2532,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 2688,
                'route_stop_id' => 2533,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 2689,
                'route_stop_id' => 2534,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 2690,
                'route_stop_id' => 2535,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 2691,
                'route_stop_id' => 2536,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 2692,
                'route_stop_id' => 2537,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 2693,
                'route_stop_id' => 2538,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 2694,
                'route_stop_id' => 2539,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 2695,
                'route_stop_id' => 2540,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 2696,
                'route_stop_id' => 2541,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 2697,
                'route_stop_id' => 2542,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 2698,
                'route_stop_id' => 2543,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 2699,
                'route_stop_id' => 2544,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 2700,
                'route_stop_id' => 2545,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 2701,
                'route_stop_id' => 2546,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 2702,
                'route_stop_id' => 2547,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 2703,
                'route_stop_id' => 2548,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 2704,
                'route_stop_id' => 2549,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 2705,
                'route_stop_id' => 2550,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 2706,
                'route_stop_id' => 2551,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 2707,
                'route_stop_id' => 2552,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 2708,
                'route_stop_id' => 2553,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 2709,
                'route_stop_id' => 2554,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 2710,
                'route_stop_id' => 2555,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 2711,
                'route_stop_id' => 2556,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 2712,
                'route_stop_id' => 2557,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 2713,
                'route_stop_id' => 2558,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 2714,
                'route_stop_id' => 2559,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 2715,
                'route_stop_id' => 2560,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 2716,
                'route_stop_id' => 2561,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 2717,
                'route_stop_id' => 2562,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 2718,
                'route_stop_id' => 2563,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 2719,
                'route_stop_id' => 2564,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 2720,
                'route_stop_id' => 2565,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 2721,
                'route_stop_id' => 2566,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 2722,
                'route_stop_id' => 2567,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 2723,
                'route_stop_id' => 2568,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 2724,
                'route_stop_id' => 2569,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 2725,
                'route_stop_id' => 2570,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 2726,
                'route_stop_id' => 2571,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 2727,
                'route_stop_id' => 2572,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 2728,
                'route_stop_id' => 2573,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 2729,
                'route_stop_id' => 2574,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 2730,
                'route_stop_id' => 2575,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 2731,
                'route_stop_id' => 2576,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 2732,
                'route_stop_id' => 2577,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 2733,
                'route_stop_id' => 2578,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 2734,
                'route_stop_id' => 2579,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 2735,
                'route_stop_id' => 2580,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 2736,
                'route_stop_id' => 2581,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 2737,
                'route_stop_id' => 2582,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 2738,
                'route_stop_id' => 2583,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 2739,
                'route_stop_id' => 2584,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 2740,
                'route_stop_id' => 2585,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 2741,
                'route_stop_id' => 2586,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 2742,
                'route_stop_id' => 2587,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 2743,
                'route_stop_id' => 2588,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 2744,
                'route_stop_id' => 2589,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 2745,
                'route_stop_id' => 2590,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 2746,
                'route_stop_id' => 2591,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 2747,
                'route_stop_id' => 2592,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 2748,
                'route_stop_id' => 2593,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 2749,
                'route_stop_id' => 2594,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 2750,
                'route_stop_id' => 2595,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 2751,
                'route_stop_id' => 2596,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 2752,
                'route_stop_id' => 2597,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 2753,
                'route_stop_id' => 2598,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 2754,
                'route_stop_id' => 2599,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 2755,
                'route_stop_id' => 2600,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 2756,
                'route_stop_id' => 2601,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 2757,
                'route_stop_id' => 2602,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 2758,
                'route_stop_id' => 2603,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 2759,
                'route_stop_id' => 2604,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 2760,
                'route_stop_id' => 2605,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 2761,
                'route_stop_id' => 2606,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 2762,
                'route_stop_id' => 2607,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 2763,
                'route_stop_id' => 2608,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 2764,
                'route_stop_id' => 2609,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 2765,
                'route_stop_id' => 2610,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 2766,
                'route_stop_id' => 2611,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 2767,
                'route_stop_id' => 2612,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 2768,
                'route_stop_id' => 2613,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 2769,
                'route_stop_id' => 2614,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 2770,
                'route_stop_id' => 2615,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 2771,
                'route_stop_id' => 2616,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 2772,
                'route_stop_id' => 2617,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 2773,
                'route_stop_id' => 2618,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 2774,
                'route_stop_id' => 2619,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 2775,
                'route_stop_id' => 2620,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 2776,
                'route_stop_id' => 2621,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 2777,
                'route_stop_id' => 2622,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 2778,
                'route_stop_id' => 2623,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 2779,
                'route_stop_id' => 2624,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 2780,
                'route_stop_id' => 2625,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 2781,
                'route_stop_id' => 2626,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 2782,
                'route_stop_id' => 2627,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 2783,
                'route_stop_id' => 2628,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 2784,
                'route_stop_id' => 2629,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 2785,
                'route_stop_id' => 2630,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 2786,
                'route_stop_id' => 2631,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 2787,
                'route_stop_id' => 2632,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 2788,
                'route_stop_id' => 2633,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 2789,
                'route_stop_id' => 2634,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 2790,
                'route_stop_id' => 2635,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 2791,
                'route_stop_id' => 2636,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 2792,
                'route_stop_id' => 2637,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 2793,
                'route_stop_id' => 2638,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 2794,
                'route_stop_id' => 2639,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 2795,
                'route_stop_id' => 2640,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 2796,
                'route_stop_id' => 2641,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 2797,
                'route_stop_id' => 2642,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 2798,
                'route_stop_id' => 2643,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 2799,
                'route_stop_id' => 2644,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 2800,
                'route_stop_id' => 2645,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 2801,
                'route_stop_id' => 2646,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 2802,
                'route_stop_id' => 2647,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 2803,
                'route_stop_id' => 2648,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 2804,
                'route_stop_id' => 2649,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 2805,
                'route_stop_id' => 2650,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 2806,
                'route_stop_id' => 2651,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 2807,
                'route_stop_id' => 2652,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 2808,
                'route_stop_id' => 2653,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 2809,
                'route_stop_id' => 2654,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 2810,
                'route_stop_id' => 2655,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 2811,
                'route_stop_id' => 2656,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 2812,
                'route_stop_id' => 2657,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 2813,
                'route_stop_id' => 2658,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 2814,
                'route_stop_id' => 2659,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 2815,
                'route_stop_id' => 2660,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 2816,
                'route_stop_id' => 2661,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 2817,
                'route_stop_id' => 2662,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 2818,
                'route_stop_id' => 2663,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 2819,
                'route_stop_id' => 2664,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 2820,
                'route_stop_id' => 2665,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 2821,
                'route_stop_id' => 2666,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 2822,
                'route_stop_id' => 2667,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 2823,
                'route_stop_id' => 2668,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 2824,
                'route_stop_id' => 2669,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 2825,
                'route_stop_id' => 2670,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 2826,
                'route_stop_id' => 2671,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 2827,
                'route_stop_id' => 2672,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 2828,
                'route_stop_id' => 2673,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 2829,
                'route_stop_id' => 2674,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 2830,
                'route_stop_id' => 2675,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 2831,
                'route_stop_id' => 2676,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 2832,
                'route_stop_id' => 2677,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 2833,
                'route_stop_id' => 2678,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 2834,
                'route_stop_id' => 2679,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 2835,
                'route_stop_id' => 2680,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 2836,
                'route_stop_id' => 2681,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 2837,
                'route_stop_id' => 2682,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 2838,
                'route_stop_id' => 2683,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 2839,
                'route_stop_id' => 2684,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 2840,
                'route_stop_id' => 2685,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 2841,
                'route_stop_id' => 2686,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 2842,
                'route_stop_id' => 2687,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 2843,
                'route_stop_id' => 2688,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 2844,
                'route_stop_id' => 2689,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 2845,
                'route_stop_id' => 2690,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 2846,
                'route_stop_id' => 2691,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 2847,
                'route_stop_id' => 2692,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 2848,
                'route_stop_id' => 2693,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 2849,
                'route_stop_id' => 2694,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 2850,
                'route_stop_id' => 2695,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 2851,
                'route_stop_id' => 2696,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 2852,
                'route_stop_id' => 2697,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 2853,
                'route_stop_id' => 2698,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 2854,
                'route_stop_id' => 2699,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 2855,
                'route_stop_id' => 2700,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 2856,
                'route_stop_id' => 2701,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 2857,
                'route_stop_id' => 2702,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 2858,
                'route_stop_id' => 2703,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 2859,
                'route_stop_id' => 2704,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 2860,
                'route_stop_id' => 2705,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 2861,
                'route_stop_id' => 2706,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 2862,
                'route_stop_id' => 2707,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 2863,
                'route_stop_id' => 2708,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 2864,
                'route_stop_id' => 2709,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 2865,
                'route_stop_id' => 2710,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 2866,
                'route_stop_id' => 2711,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 2867,
                'route_stop_id' => 2712,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 2868,
                'route_stop_id' => 2713,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 2869,
                'route_stop_id' => 2714,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 2870,
                'route_stop_id' => 2715,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 2871,
                'route_stop_id' => 2716,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 2872,
                'route_stop_id' => 2717,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 2873,
                'route_stop_id' => 2718,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 2874,
                'route_stop_id' => 2719,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 2875,
                'route_stop_id' => 2720,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 2876,
                'route_stop_id' => 2721,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 2877,
                'route_stop_id' => 2722,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 2878,
                'route_stop_id' => 2723,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 2879,
                'route_stop_id' => 2724,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 2880,
                'route_stop_id' => 2725,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 2881,
                'route_stop_id' => 2726,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 2882,
                'route_stop_id' => 2727,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 2883,
                'route_stop_id' => 2728,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 2884,
                'route_stop_id' => 2729,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 2885,
                'route_stop_id' => 2730,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 2886,
                'route_stop_id' => 2731,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 2887,
                'route_stop_id' => 2732,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 2888,
                'route_stop_id' => 2733,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 2889,
                'route_stop_id' => 2734,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 2890,
                'route_stop_id' => 2735,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 2891,
                'route_stop_id' => 2736,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 2892,
                'route_stop_id' => 2737,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 2893,
                'route_stop_id' => 2738,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 2894,
                'route_stop_id' => 2739,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 2895,
                'route_stop_id' => 2740,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 2896,
                'route_stop_id' => 2741,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 2897,
                'route_stop_id' => 2742,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 2898,
                'route_stop_id' => 2743,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 2899,
                'route_stop_id' => 2744,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 2900,
                'route_stop_id' => 2745,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 2901,
                'route_stop_id' => 2746,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 2902,
                'route_stop_id' => 2747,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 2903,
                'route_stop_id' => 2748,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 2904,
                'route_stop_id' => 2749,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 2905,
                'route_stop_id' => 2750,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 2906,
                'route_stop_id' => 2751,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 2907,
                'route_stop_id' => 2752,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 2908,
                'route_stop_id' => 2753,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 2909,
                'route_stop_id' => 2754,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 2910,
                'route_stop_id' => 2755,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 2911,
                'route_stop_id' => 2756,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 2912,
                'route_stop_id' => 2757,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 2913,
                'route_stop_id' => 2758,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 2914,
                'route_stop_id' => 2759,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 2915,
                'route_stop_id' => 2760,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 2916,
                'route_stop_id' => 2761,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 2917,
                'route_stop_id' => 2762,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 2918,
                'route_stop_id' => 2763,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 2919,
                'route_stop_id' => 2764,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 2920,
                'route_stop_id' => 2765,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 2921,
                'route_stop_id' => 2766,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 2922,
                'route_stop_id' => 2767,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 2923,
                'route_stop_id' => 2768,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 2924,
                'route_stop_id' => 2769,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 2925,
                'route_stop_id' => 2770,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 2926,
                'route_stop_id' => 2771,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 2927,
                'route_stop_id' => 2772,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 2928,
                'route_stop_id' => 2773,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 2929,
                'route_stop_id' => 2774,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 2930,
                'route_stop_id' => 2775,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 2931,
                'route_stop_id' => 2776,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 2932,
                'route_stop_id' => 2777,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 2933,
                'route_stop_id' => 2778,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 2934,
                'route_stop_id' => 2779,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 2935,
                'route_stop_id' => 2780,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 2936,
                'route_stop_id' => 2781,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 2937,
                'route_stop_id' => 2782,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 2938,
                'route_stop_id' => 2783,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 2939,
                'route_stop_id' => 2784,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 2940,
                'route_stop_id' => 2785,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 2941,
                'route_stop_id' => 2786,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 2942,
                'route_stop_id' => 2787,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 2943,
                'route_stop_id' => 2788,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 2944,
                'route_stop_id' => 2789,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 2945,
                'route_stop_id' => 2790,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 2946,
                'route_stop_id' => 2791,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 2947,
                'route_stop_id' => 2792,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 2948,
                'route_stop_id' => 2793,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 2949,
                'route_stop_id' => 2794,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 2950,
                'route_stop_id' => 2795,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 2951,
                'route_stop_id' => 2796,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 2952,
                'route_stop_id' => 2797,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 2953,
                'route_stop_id' => 2798,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 2954,
                'route_stop_id' => 2799,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 2955,
                'route_stop_id' => 2800,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 2956,
                'route_stop_id' => 2801,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 2957,
                'route_stop_id' => 2802,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 2958,
                'route_stop_id' => 2803,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 2959,
                'route_stop_id' => 2804,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 2960,
                'route_stop_id' => 2805,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 2961,
                'route_stop_id' => 2806,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 2962,
                'route_stop_id' => 2807,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 2963,
                'route_stop_id' => 2808,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 2964,
                'route_stop_id' => 2809,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 2965,
                'route_stop_id' => 2810,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 2966,
                'route_stop_id' => 2811,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 2967,
                'route_stop_id' => 2812,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 2968,
                'route_stop_id' => 2813,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 2969,
                'route_stop_id' => 2814,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 2970,
                'route_stop_id' => 2815,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 2971,
                'route_stop_id' => 2816,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 2972,
                'route_stop_id' => 2817,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 2973,
                'route_stop_id' => 2818,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 2974,
                'route_stop_id' => 2819,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 2975,
                'route_stop_id' => 2820,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 2976,
                'route_stop_id' => 2821,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 2977,
                'route_stop_id' => 2822,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 2978,
                'route_stop_id' => 2823,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 2979,
                'route_stop_id' => 2824,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 2980,
                'route_stop_id' => 2825,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 2981,
                'route_stop_id' => 2826,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 2982,
                'route_stop_id' => 2827,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 2983,
                'route_stop_id' => 2828,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 2984,
                'route_stop_id' => 2829,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 2985,
                'route_stop_id' => 2830,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 2986,
                'route_stop_id' => 2831,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 2987,
                'route_stop_id' => 2832,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 2988,
                'route_stop_id' => 2833,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 2989,
                'route_stop_id' => 2834,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 2990,
                'route_stop_id' => 2835,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 2991,
                'route_stop_id' => 2836,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 2992,
                'route_stop_id' => 2837,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 2993,
                'route_stop_id' => 2838,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,tue,wed,thu,fri,sat,hol',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 2994,
                'route_stop_id' => 2839,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 2995,
                'route_stop_id' => 2840,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 2996,
                'route_stop_id' => 2841,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 2997,
                'route_stop_id' => 2842,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 2998,
                'route_stop_id' => 2843,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 2999,
                'route_stop_id' => 2844,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 3000,
                'route_stop_id' => 2845,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 3001,
                'route_stop_id' => 2846,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 3002,
                'route_stop_id' => 2847,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 3003,
                'route_stop_id' => 2848,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 3004,
                'route_stop_id' => 2849,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 3005,
                'route_stop_id' => 2850,
                'month_type' => 'normal',
                'day_types' => 'sun,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 3006,
                'route_stop_id' => 2851,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 3007,
                'route_stop_id' => 2852,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 3008,
                'route_stop_id' => 2853,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 3009,
                'route_stop_id' => 2854,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 3010,
                'route_stop_id' => 2855,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 3011,
                'route_stop_id' => 2856,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 3012,
                'route_stop_id' => 2857,
                'month_type' => 'normal',
                'day_types' => 'mon',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 3013,
                'route_stop_id' => 2858,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 3014,
                'route_stop_id' => 2859,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 3015,
                'route_stop_id' => 2860,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 3016,
                'route_stop_id' => 2861,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 3017,
                'route_stop_id' => 2862,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 3018,
                'route_stop_id' => 2863,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 3019,
                'route_stop_id' => 2864,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 3020,
                'route_stop_id' => 2865,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 3021,
                'route_stop_id' => 2866,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 3022,
                'route_stop_id' => 2867,
                'month_type' => 'normal',
                'day_types' => 'tue,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 3023,
                'route_stop_id' => 2868,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 3024,
                'route_stop_id' => 2869,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 3025,
                'route_stop_id' => 2870,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 3026,
                'route_stop_id' => 2871,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 3027,
                'route_stop_id' => 2872,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 3028,
                'route_stop_id' => 2873,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 3029,
                'route_stop_id' => 2874,
                'month_type' => 'normal',
                'day_types' => 'thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 3030,
                'route_stop_id' => 2875,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 3031,
                'route_stop_id' => 2876,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 3032,
                'route_stop_id' => 2877,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 3033,
                'route_stop_id' => 2878,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 3034,
                'route_stop_id' => 2879,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 3035,
                'route_stop_id' => 2880,
                'month_type' => 'normal',
                'day_types' => 'mon,wed,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 3036,
                'route_stop_id' => 2881,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 3038,
                'route_stop_id' => 2883,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 3039,
                'route_stop_id' => 2884,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 3040,
                'route_stop_id' => 2885,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 3042,
                'route_stop_id' => 2887,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 3043,
                'route_stop_id' => 2888,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 3044,
                'route_stop_id' => 2889,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 3045,
                'route_stop_id' => 2890,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 3047,
                'route_stop_id' => 2892,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 3049,
                'route_stop_id' => 2894,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 3050,
                'route_stop_id' => 2895,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 3051,
                'route_stop_id' => 2896,
                'month_type' => 'normal',
                'day_types' => 'sun,mon,wed',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 3052,
                'route_stop_id' => 2897,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 3053,
                'route_stop_id' => 2898,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 3054,
                'route_stop_id' => 2899,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('mtcc_route_stop_days')->insert(array (
            0 => 
            array (
                'id' => 3055,
                'route_stop_id' => 2900,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 3056,
                'route_stop_id' => 2901,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3057,
                'route_stop_id' => 2902,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 3058,
                'route_stop_id' => 2903,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 3059,
                'route_stop_id' => 2904,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,wed,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 3060,
                'route_stop_id' => 2905,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 3061,
                'route_stop_id' => 2906,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 3062,
                'route_stop_id' => 2907,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 3063,
                'route_stop_id' => 2908,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 3064,
                'route_stop_id' => 2909,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 3065,
                'route_stop_id' => 2910,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 3066,
                'route_stop_id' => 2911,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 3067,
                'route_stop_id' => 2912,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 3068,
                'route_stop_id' => 2913,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 3069,
                'route_stop_id' => 2914,
                'month_type' => 'normal',
                'day_types' => 'sun,tue,thu,sat',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}