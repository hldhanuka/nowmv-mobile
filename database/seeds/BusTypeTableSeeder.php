<?php

use Illuminate\Database\Seeder;

class BusTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bus_types')->truncate();
        DB::table('bus_types')->insert([

            [
                'name' => "MTCC",
                'status' => "1",
                'logo' => "https://now.dhiraagu.com.mv/uploads/travel/MTCC_logo.png",
            ],

            [
                'name' => "GMTL",
                'status' => "1",
                'logo' => "https://now.dhiraagu.com.mv/uploads/travel/MPL_logo.png",
            ]

        ]);
    }
}
