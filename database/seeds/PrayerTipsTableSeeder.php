<?php

use Illuminate\Database\Seeder;

class PrayerTipsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('prayer_tips')->delete();

        \DB::table('prayer_tips')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Tip 01',
                'content' => 'Make your intention to pray',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:21:34',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:21:34',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Tip 02',
                'content' => 'Raise your hands to your ears and say Allahu Akbar',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:21:39',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:21:39',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Tip 03',
                'content' => 'Place your hands over your chest',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:21:44',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 10:21:44',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'Tip 04',
                'content' => 'Keep your eyes focused on the ground',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:21:54',
                'created_at' => '2020-01-09 10:19:41',
                'updated_at' => '2020-01-09 10:21:54',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'title' => 'Tip 05',
                'content' => 'Recite the opening chapter of the Qur’an',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:14',
                'created_at' => '2020-01-09 10:19:52',
                'updated_at' => '2020-01-09 10:22:14',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'title' => 'Tip 06',
            'content' => 'Perform the ruku (bowing down)',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:02',
                'created_at' => '2020-01-09 10:20:04',
                'updated_at' => '2020-01-09 10:22:02',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'title' => 'Tip 07',
                'content' => 'Return to standing up again',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:08',
                'created_at' => '2020-01-09 10:20:14',
                'updated_at' => '2020-01-09 10:22:08',
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'title' => 'Tip 08',
            'content' => 'Perform the sujud (prostration)',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:20',
                'created_at' => '2020-01-09 10:20:25',
                'updated_at' => '2020-01-09 10:22:20',
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'title' => 'Tip 09',
                'content' => 'Say this phrase in sujud',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:27',
                'created_at' => '2020-01-09 10:20:36',
                'updated_at' => '2020-01-09 10:22:27',
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'title' => 'Tip 10',
                'content' => 'Step 11 & 12 – Perform sujud again and then return to a standing position',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:34',
                'created_at' => '2020-01-09 10:20:47',
                'updated_at' => '2020-01-09 10:22:34',
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'title' => 'Tip 11 & 12',
                'content' => 'Perform sujud again and then return to a standing position',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:46',
                'created_at' => '2020-01-09 10:21:15',
                'updated_at' => '2020-01-09 10:22:46',
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'title' => 'Tip 13',
                'content' => 'Perform the tashahud',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 10:22:57',
                'created_at' => '2020-01-09 10:21:28',
                'updated_at' => '2020-01-09 10:22:57',
                'deleted_at' => NULL,
            ),
        ));


    }
}
