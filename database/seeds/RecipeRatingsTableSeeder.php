<?php

use Illuminate\Database\Seeder;

class RecipeRatingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_ratings')->delete();
        
        \DB::table('recipe_ratings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recipe_id' => 1,
                'rate' => 2.0,
                'created_by' => 3,
                'created_at' => '2020-01-09 14:58:47',
                'updated_at' => '2020-01-09 15:17:53',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'recipe_id' => 2,
                'rate' => 4.0,
                'created_by' => 3,
                'created_at' => '2020-01-09 15:11:24',
                'updated_at' => '2020-01-09 15:11:24',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'recipe_id' => 3,
                'rate' => 4.0,
                'created_by' => 3,
                'created_at' => '2020-01-09 20:58:17',
                'updated_at' => '2020-01-09 20:58:17',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'recipe_id' => 1,
                'rate' => 5.0,
                'created_by' => 4,
                'created_at' => '2020-01-11 21:57:45',
                'updated_at' => '2020-01-12 08:33:19',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'recipe_id' => 1,
                'rate' => 4.0,
                'created_by' => 5,
                'created_at' => '2020-01-11 22:05:08',
                'updated_at' => '2020-01-11 22:05:08',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}