<?php

use Illuminate\Database\Seeder;

class MealCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('meal_categories')->delete();

        \DB::table('meal_categories')->insert(array (
            0 =>
            array (
                'id' => 1,
                'restaurant_id' => 1,
                'title' => 'Fish',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091380292803320131.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:24:54',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-12 15:24:54',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'restaurant_id' => 1,
                'title' => 'Ground Beef',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091408245587050757.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 13:24:46',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 13:24:46',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'restaurant_id' => 1,
                'title' => 'Chicken',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091460356488411277.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 13:24:53',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 13:24:53',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'restaurant_id' => 1,
                'title' => 'Breakfast for Dinner/Eggs',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091479138837014153.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 13:25:07',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 13:25:07',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'restaurant_id' => 1,
                'title' => 'Vegetarian',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091506430932848111.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 13:25:16',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 13:25:16',
                'deleted_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'restaurant_id' => 1,
                'title' => 'Pasta',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091534658991529212.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 13:25:27',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 13:25:27',
                'deleted_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'restaurant_id' => 3,
                'title' => 'rice',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091587349429513678.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'restaurant_id' => 3,
                'title' => 'koththu',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091622430732074879.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'restaurant_id' => 3,
                'title' => 'noodles',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091644801853091779.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 2,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 00:29:12',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-09 00:29:12',
                'deleted_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'restaurant_id' => 1,
                'title' => 'test_r',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605091709529909662105.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 12:24:04',
                'created_at' => '2020-01-09 12:12:44',
                'updated_at' => '2020-01-09 12:24:04',
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'restaurant_id' => 1,
                'title' => 'test_r',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099062274155571242.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:26:56',
                'created_at' => '2020-01-09 12:23:35',
                'updated_at' => '2020-01-12 15:26:56',
                'deleted_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'restaurant_id' => 1,
                'title' => 'test_r',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099081073574881895.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:26:44',
                'created_at' => '2020-01-09 12:24:16',
                'updated_at' => '2020-01-12 15:26:44',
                'deleted_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'restaurant_id' => 1,
                'title' => 'test_r',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099094689859468951.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:26:32',
                'created_at' => '2020-01-09 15:54:22',
                'updated_at' => '2020-01-12 15:26:32',
                'deleted_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'restaurant_id' => 2,
                'title' => 'Chicken Bacon',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099116264907521136.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:26:20',
                'created_at' => '2020-01-12 15:23:13',
                'updated_at' => '2020-01-12 15:26:20',
                'deleted_at' => NULL,
            ),
            14 =>
            array (
                'id' => 15,
                'restaurant_id' => 2,
                'title' => 'Cheese Lovers',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099162059165876996.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:26:01',
                'created_at' => '2020-01-12 15:23:37',
                'updated_at' => '2020-01-12 15:26:01',
                'deleted_at' => NULL,
            ),
            15 =>
            array (
                'id' => 16,
                'restaurant_id' => 2,
                'title' => 'Devilled Chicken',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099186178780557889.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:25:48',
                'created_at' => '2020-01-12 15:24:05',
                'updated_at' => '2020-01-12 15:25:48',
                'deleted_at' => NULL,
            ),
            16 =>
            array (
                'id' => 17,
                'restaurant_id' => 2,
                'title' => 'Cheese and Tomato',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099210603256780551.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:25:36',
                'created_at' => '2020-01-12 15:24:21',
                'updated_at' => '2020-01-12 15:25:36',
                'deleted_at' => NULL,
            ),
            17 =>
            array (
                'id' => 18,
                'restaurant_id' => 2,
                'title' => 'Cheesy Onion',
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/meal_categories/1605099230871876280464.jpeg',
                'status' => 1,
                'merchant_id' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-12 15:25:23',
                'created_at' => '2020-01-12 15:24:46',
                'updated_at' => '2020-01-12 15:25:23',
                'deleted_at' => NULL,
            ),
        ));


    }
}
