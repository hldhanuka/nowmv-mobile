<?php

use App\Helpers\APIHelper;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class NgoMerchantRoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create Role
        $ngo_merchant_role = Role::create(['name' => 'ngo_merchant']);

        // ngo merchants permissions
        $ngo_merchants_list_permission = Permission::create(['name' => 'ngo_merchants.list']);
        $ngo_merchants_create_permission = Permission::create(['name' => 'ngo_merchants.create']);
        $ngo_merchants_update_permission = Permission::create(['name' => 'ngo_merchants.update']);
        $ngo_merchants_delete_permission = Permission::create(['name' => 'ngo_merchants.delete']);
        $ngo_merchants_approve_permission = Permission::create(['name' => 'ngo_merchants.approve']);

        // Give permission to admin
        $admin_role = Role::findByName('admin');
        $admin_role->givePermissionTo($ngo_merchants_list_permission);
        $admin_role->givePermissionTo($ngo_merchants_create_permission);
        $admin_role->givePermissionTo($ngo_merchants_update_permission);
        $admin_role->givePermissionTo($ngo_merchants_delete_permission);
        $admin_role->givePermissionTo($ngo_merchants_approve_permission);

        // Give permission to ngo_merchant
        $ngo_merchant_role->givePermissionTo('cms_login');

        $ngo_merchant_role->givePermissionTo('ngos.list');
        $ngo_merchant_role->givePermissionTo('ngos.create');
        $ngo_merchant_role->givePermissionTo('ngos.update');
        $ngo_merchant_role->givePermissionTo('ngos.delete');

        $ngo_merchant_role->givePermissionTo('donations.list');
        $ngo_merchant_role->givePermissionTo('donations.create');
        $ngo_merchant_role->givePermissionTo('donations.update');
        $ngo_merchant_role->givePermissionTo('donations.delete');

        $ngo_merchant_role->givePermissionTo('badges.list');
        $ngo_merchant_role->givePermissionTo('badges.create');
        $ngo_merchant_role->givePermissionTo('badges.update');
        $ngo_merchant_role->givePermissionTo('badges.delete');

        $new_ngo_user = new \App\User();
        $new_ngo_user->email = "ngouser1@nowmv.com";
        $new_ngo_user->mobile = "ngouser1@nowmv.com";
        $new_ngo_user->password = bcrypt('qazwsxxzsawq');
        $new_ngo_user->status = 1;
        $new_ngo_user->save();
        $new_ngo_user->assignRole($ngo_merchant_role);

        $new_ngo_merchant = new \App\NgoMerchant();
        $new_ngo_merchant->name = "Test NGO Merchant";
        $new_ngo_merchant->key = "7400004";
        $new_ngo_merchant->status = 1;
        $new_ngo_merchant->created_by = 1;
        $new_ngo_merchant->authorized_by = 1;
        $new_ngo_merchant->published_at = \Carbon\Carbon::now()->toDateTimeString();
        $new_ngo_merchant->save();

        $new_ngo_merchant_user = new \App\NgoMerchantUser();
        $new_ngo_merchant_user->user_id = $new_ngo_user->id;
        $new_ngo_merchant_user->ngo_merchant_id = $new_ngo_merchant->id;
        $new_ngo_merchant_user->status = 1;
        $new_ngo_merchant_user->created_by = 1;
        $new_ngo_merchant_user->authorized_by = 1;
        $new_ngo_merchant_user->published_at = \Carbon\Carbon::now()->toDateTimeString();
        $new_ngo_merchant_user->save();

    }
}
