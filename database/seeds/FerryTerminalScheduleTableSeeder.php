<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FerryTerminalScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ferry_terminal_schedules')->truncate();
        DB::table('ferry_terminal_schedules')->insert([
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "00:00:00",
                'terminal_id' => 1,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "00:05:00",
                'terminal_id' => 58,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "00:18:00",
                'terminal_id' => 202,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "00:23:00",
                'terminal_id' => 59,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "00:41:00",
                'terminal_id' => 4,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "01:23:00",
                'terminal_id' => 137,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "01:41:00",
                'terminal_id' => 132,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "01:59:00",
                'terminal_id' => 99,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "02:05:00",
                'terminal_id' => 31,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 1,
                'start_location_id' => 1,
                'time' => "02:15:00",
                'terminal_id' => 193,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "00:00:00",
                'terminal_id' => 193,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "00:05:00",
                'terminal_id' => 31,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "00:18:00",
                'terminal_id' => 99,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "00:23:00",
                'terminal_id' => 132,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "00:41:00",
                'terminal_id' => 137,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "01:23:00",
                'terminal_id' => 4,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "01:41:00",
                'terminal_id' => 59,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "01:59:00",
                'terminal_id' => 202,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "02:05:00",
                'terminal_id' => 58,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'ferry_route_id' => 2,
                'start_location_id' => 193,
                'time' => "02:15:00",
                'terminal_id' => 1,
                'status' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
