<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PushNotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('push_notifications')->truncate();
        DB::table('push_notifications')->insert([
            [
                'from' => 0,
                'to' => 3,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 4,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 5,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 10,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 11,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 12,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 13,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 14,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'from' => 0,
                'to' => 15,
                'message' => "Welcome to Now MV",
                'seen' => 0,
                'type'=> 'system',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
