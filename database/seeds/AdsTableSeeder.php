<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('ads')->delete();

        \DB::table('ads')->insert(array (
            0 =>
            array (
                'id' => 1,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084050090888539948.jpeg',
                'title' => 'Sports',
                'type' => 'home',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:45:07',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-10 11:45:07',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084205638326303797.jpeg',
                'title' => 'Entertainment',
                'type' => 'news',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:08:15',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-10 11:08:15',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084624324231971305.jpeg',
                'title' => 'Food',
                'type' => 'food',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:09:54',
                'created_at' => '2020-01-09 00:29:12',
                'updated_at' => '2020-01-10 11:09:54',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084665197361525552.jpeg',
                'title' => 'Sports',
                'type' => 'home',
                'status' => 3,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:10:50',
                'created_at' => '2020-01-09 09:52:48',
                'updated_at' => '2020-01-10 11:13:52',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084686241858226616.jpeg',
                'title' => 'Year End offer',
                'type' => 'home',
                'status' => 3,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-10 11:11:47',
                'created_at' => '2020-01-09 12:15:34',
                'updated_at' => '2020-01-10 13:02:05',
                'deleted_at' => NULL,
            ),
            5 =>
                array (
                    'id' => 6,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084784500735372789.jpeg',
                    'title' => 'Dialog Mega Pomotion',
                    'type' => 'news',
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-12 15:40:55',
                    'created_at' => '2020-01-12 15:40:47',
                    'updated_at' => '2020-01-12 15:40:55',
                    'deleted_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084825154711673554.jpeg',
                    'title' => 'Weather',
                    'type' => 'weather',
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-12 15:40:55',
                    'created_at' => '2020-01-12 15:40:47',
                    'updated_at' => '2020-01-12 15:40:55',
                    'deleted_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084879795710383627.jpeg',
                    'title' => 'Offers',
                    'type' => 'offer',
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-12 15:40:55',
                    'created_at' => '2020-01-12 15:40:47',
                    'updated_at' => '2020-01-12 15:40:55',
                    'deleted_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084929234557145439.jpeg',
                    'title' => 'Travel',
                    'type' => 'travel',
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-12 15:40:55',
                    'created_at' => '2020-01-12 15:40:47',
                    'updated_at' => '2020-01-12 15:40:55',
                    'deleted_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'image' => 'https://dhiraagu-now-mv.s3.ap-south-1.amazonaws.com/upload/ads/1605084976549231039773.jpeg',
                    'title' => 'Donation',
                    'type' => 'donation',
                    'status' => 1,
                    'created_by' => 1,
                    'authorized_by' => 1,
                    'published_at' => '2020-01-12 15:40:55',
                    'created_at' => '2020-01-12 15:40:47',
                    'updated_at' => '2020-01-12 15:40:55',
                    'deleted_at' => NULL,
                ),
        ));


    }
}
