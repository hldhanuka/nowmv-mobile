<?php

use Illuminate\Database\Seeder;

class RestaurantReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('restaurant_reviews')->delete();
        
        \DB::table('restaurant_reviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'restaurant_id' => 1,
                'comment' => 'Test',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'restaurant_id' => 3,
                'comment' => 'Wow',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'restaurant_id' => 1,
                'comment' => 'Happy happy thanksgiving happy happy new hope you have blessed you happy birthday to my family happy thanksgiving happy happy new you all to you all happy',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'restaurant_id' => 1,
                'comment' => 'Happy birthday happy thanksgiving happy happy thanksgiving hope all thanksgiving happy birthday darling hope all thanksgiving happy birthday darling hope all',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'restaurant_id' => 2,
                'comment' => 'Delicious foods',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'restaurant_id' => 5,
                'comment' => 'Great food',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'restaurant_id' => 5,
                'comment' => 'Loved every time I visit',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'restaurant_id' => 5,
                'comment' => 'Great food',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'restaurant_id' => 2,
                'comment' => 'Sfg',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'restaurant_id' => 2,
                'comment' => 'Good',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'restaurant_id' => 5,
                'comment' => 'Test',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'restaurant_id' => 5,
                'comment' => 'Tesr',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}