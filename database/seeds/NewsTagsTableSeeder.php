<?php

use Illuminate\Database\Seeder;

class NewsTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('news_tags')->delete();
        
        \DB::table('news_tags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'National',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 09:43:23',
                'created_at' => '2020-01-09 00:29:17',
                'updated_at' => '2020-01-09 09:43:23',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'International',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 09:45:18',
                'created_at' => '2020-01-09 00:29:17',
                'updated_at' => '2020-01-09 09:45:18',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Local',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 09:44:10',
                'created_at' => '2020-01-09 00:29:17',
                'updated_at' => '2020-01-09 09:44:10',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Business & Fin',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 09:44:32',
                'created_at' => '2020-01-09 09:44:25',
                'updated_at' => '2020-01-09 09:44:32',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Health & Education',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 09:44:51',
                'created_at' => '2020-01-09 09:44:43',
                'updated_at' => '2020-01-09 09:44:51',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Travel',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => '2020-01-09 21:58:43',
                'created_at' => '2020-01-09 12:27:42',
                'updated_at' => '2020-01-09 21:58:43',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}