<?php

use Illuminate\Database\Seeder;

class RecipeReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_reviews')->delete();
        
        \DB::table('recipe_reviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recipe_id' => 1,
                'comment' => 'Very tasty curry',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-09 15:07:50',
                'updated_at' => '2020-01-09 15:07:50',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'recipe_id' => 1,
                'comment' => 'Will try',
                'status' => 1,
                'created_by' => 4,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-09 15:09:01',
                'updated_at' => '2020-01-09 15:09:01',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'recipe_id' => 2,
                'comment' => 'Very good recipe',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-09 15:11:41',
                'updated_at' => '2020-01-09 15:11:41',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'recipe_id' => 1,
                'comment' => 'Bsns',
                'status' => 1,
                'created_by' => 5,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-12 08:33:02',
                'updated_at' => '2020-01-12 08:33:02',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'recipe_id' => 2,
                'comment' => 'Wow',
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-12 12:02:59',
                'updated_at' => '2020-01-12 12:02:59',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'recipe_id' => 1,
                'comment' => 'Test',
                'status' => 1,
                'created_by' => 5,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-12 12:05:07',
                'updated_at' => '2020-01-12 12:05:07',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'recipe_id' => 1,
                'comment' => 'Family love to both of us all the time to get the family happy birthday to you all hope all thanksgiving happy birthday darling hope all thanksgiving    Happy',
                'status' => 1,
                'created_by' => 3,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-12 12:29:17',
                'updated_at' => '2020-01-12 12:29:17',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'recipe_id' => 1,
                'comment' => 'Test',
                'status' => 1,
                'created_by' => 4,
                'authorized_by' => NULL,
                'published_at' => NULL,
                'created_at' => '2020-01-12 19:44:24',
                'updated_at' => '2020-01-12 19:44:24',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}