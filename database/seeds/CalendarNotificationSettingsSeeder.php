<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CalendarNotificationSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notification_type = \App\NotificationType::create([
            'id' => 13,
            'parent_id' => NULL,
            'title' => 'Calendar Notifications',
            'type' => NULL,
            'status' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        $customer_ids = User::role('customer')->pluck('id');
        foreach ($customer_ids as $id) {
            DB::table('user_notification_settings')->insert([
                'user_id' => $id,
                'type_id' => 13,
                'status' => 1,
                'title' => 'Calendar Notifications',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
        }

    }

}
