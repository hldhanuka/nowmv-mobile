<?php

use Illuminate\Database\Seeder;

class QuranRecitationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('quran_recitations')->delete();
        
        \DB::table('quran_recitations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title_ar' => 'الفَاتِحَة ',
                'title_dv' => 'އަލް ފާތިޙާ',
                'title_en' => 'Al-Fatihah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title_ar' => 'البَقَرَة',
                'title_dv' => ' އަލް ބަޤަރާ',
                'title_en' => 'Al-Baqarah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title_ar' => 'آل عِمرَان',
                'title_dv' => ' އާލް އިމްރާން',
                'title_en' => 'Al \'Imran',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title_ar' => ' النِّسَاء',
                'title_dv' => ' އައްނިސާ',
                'title_en' => 'An-Nisa\' ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'title_ar' => ' المَائدة',
                'title_dv' => ' އަލް މާއިދާ',
                'title_en' => 'Al-Ma\'ida',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'title_ar' => ' الاٴنعَام',
                'title_dv' => ' އަލް އަންޢާމް',
                'title_en' => 'Al-An\'am ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'title_ar' => ' الاٴعرَاف',
                'title_dv' => ' އަލް ޢަޢްރާފް',
                'title_en' => 'Al-A\'raf ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'title_ar' => ' الاٴنفَال',
                'title_dv' => ' އަލް އަންފާލް',
                'title_en' => 'Al-Anfal ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'title_ar' => 'التّوبَة',
                'title_dv' => ' އައްތައުބާ',
                'title_en' => 'Al-Taubah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'title_ar' => ' یُونس',
                'title_dv' => 'ޔޫނުސް',
                'title_en' => 'Yunus',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'title_ar' => ' هُود',
                'title_dv' => ' ހޫދު',
                'title_en' => 'Hud',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'title_ar' => ' یُوسُف',
                'title_dv' => ' ޔޫސުފް',
                'title_en' => 'Yusuf',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'title_ar' => 'الرّعد',
                'title_dv' => ' އައް ރަޢްދް',
                'title_en' => 'Ar-Ra\'d ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'title_ar' => ' إبراهیم',
                'title_dv' => 'އިބްރާހީމް',
                'title_en' => 'Ibrahim ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'title_ar' => ' الحِجر',
                'title_dv' => 'އަލް ޙިޖްރު',
                'title_en' => 'Al-Hijr ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'title_ar' => ' النّحل',
                'title_dv' => ' އަންނަޙްލް',
                'title_en' => 'An-Nahl ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'title_ar' => 'الإسرَاء',
                'title_dv' => 'އަލް އިސްރާ',
                'title_en' => 'Al-Isra\'',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'title_ar' => ' الکهف',
                'title_dv' => 'އަލް ކަހްފް',
                'title_en' => 'Al-Kahf ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'title_ar' => ' مَریَم',
                'title_dv' => ' މަރިޔަމް',
                'title_en' => 'Maryam',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'title_ar' => ' طٰه',
                'title_dv' => 'ތާހާ',
                'title_en' => ' Ta-Ha',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'title_ar' => 'الاٴنبیَاء',
                'title_dv' => ' އަލް އަންބިޔާ',
                'title_en' => 'Al-Anbiya',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'title_ar' => ' الحَجّ',
                'title_dv' => 'އަލް ޙައްޖު',
                'title_en' => 'Al-Hajj ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'title_ar' => ' المؤمنون',
                'title_dv' => ' އަލް މޫމިނޫން',
                'title_en' => 'Al-Mu\'minum',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'title_ar' => 'النُّور',
                'title_dv' => ' އަންނޫރު',
                'title_en' => 'An-Nur',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'title_ar' => ' الفُرقان',
                'title_dv' => ' އަލް ފުރުޤާން',
                'title_en' => 'Al-Furqan',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'title_ar' => 'لشُّعَرَاء',
                'title_dv' => 'އައްޝުޢަރާ',
                'title_en' => 'Ashl-Shu\'ara\'',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'title_ar' => 'النَّمل',
                'title_dv' => 'އަންނަމްލް',
                'title_en' => 'Al-Naml ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'title_ar' => ' القَصَص',
                'title_dv' => 'އަލް ޤަޞަޞް',
                'title_en' => 'Al-Qasas',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'title_ar' => ' العَنکبوت',
                'title_dv' => 'އަލް ޢަންކަބޫތު',
                'title_en' => 'Al-\'Ankabut',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'title_ar' => 'الرُّوم',
                'title_dv' => 'އައްރޫމް',
                'title_en' => 'Ar-Rum',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'title_ar' => ' لقمَان',
                'title_dv' => 'ލުޤްމާން',
                'title_en' => 'Luqman',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'title_ar' => ' السَّجدَة',
                'title_dv' => 'އައްސަޖްދަ',
                'title_en' => 'As-Sajdah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'title_ar' => ' الاٴحزَاب',
                'title_dv' => 'އަލް އަޙްޒާބް',
                'title_en' => 'Al-Ahzab',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'title_ar' => ' سَبَإ',
                'title_dv' => 'ސަބާ',
                'title_en' => 'Saba\'',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'title_ar' => ' فَاطِر',
                'title_dv' => 'ފާތިރް',
                'title_en' => 'Fatir',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'title_ar' => ' یسٓ',
                'title_dv' => 'ޔާސީން',
                'title_en' => 'Ya-Sin',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'title_ar' => ' الصَّافات',
                'title_dv' => 'އަލް ޞޯއްފަ',
                'title_en' => 'As-Saffat',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'title_ar' => ' صٓ',
                'title_dv' => 'ޞޯދް',
                'title_en' => 'Sad',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'title_ar' => 'الزُّمَر',
                'title_dv' => 'އައްޒުމަރް',
                'title_en' => 'Az-Zumar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'title_ar' => 'من / غَافر',
                'title_dv' => 'އަލް ޣާފިރް',
                'title_en' => 'Ghafir',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'title_ar' => ' فُصّلَت',
                'title_dv' => 'ފުއްޞިލަތް',
                'title_en' => 'Fussilat',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'title_ar' => 'الشّوریٰ',
                'title_dv' => 'އައްޝޫރާ',
                'title_en' => 'Ash-Shura',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'title_ar' => ' الزّخرُف',
                'title_dv' => 'އައްޒުޚްރޫފް',
                'title_en' => 'Az-Zukhruf',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'title_ar' => ' الدّخان',
                'title_dv' => 'އައްދުކާން',
                'title_en' => 'Ad-Dukhan',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'title_ar' => ' الجَاثیَة',
                'title_dv' => 'އަލް ޖާޡިޔަ',
                'title_en' => 'Al-Jathiyah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'title_ar' => 'الاٴحقاف',
                'title_dv' => 'އަލް އަޙްޤާފް',
                'title_en' => 'Al-Ahqaf',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'title_ar' => ' محَمَّد',
                'title_dv' => 'މުޙަންމަދު',
                'title_en' => 'Muhammad',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'title_ar' => ' الفَتْح',
                'title_dv' => 'އަލް ފަތްޙް',
                'title_en' => 'Al-Fath ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'title_ar' => ' الحُجرَات',
                'title_dv' => 'އަލް ޙުޖުރާތް',
                'title_en' => 'Al-Hujurat',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'title_ar' => ' قٓ',
                'title_dv' => 'ޤާފް',
                'title_en' => 'Qaf',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('quran_recitations')->insert(array (
            0 => 
            array (
                'id' => 51,
                'title_ar' => 'الذّاریَات',
                'title_dv' => 'އައްދާރިޔަ',
                'title_en' => 'Adh-Dhariyat',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 52,
                'title_ar' => ' الطُّور',
                'title_dv' => 'އައްތޫރް',
                'title_en' => 'At-Tur',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 53,
                'title_ar' => 'النّجْم',
                'title_dv' => ' އައްނަޖްމް',
                'title_en' => 'An-Najm ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 54,
                'title_ar' => 'القَمَر',
                'title_dv' => 'އަލް ޤަމަރް',
                'title_en' => 'Al-Qamar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 55,
                'title_ar' => ' الرَّحمٰن',
                'title_dv' => 'އައްރަޙްމާން',
                'title_en' => 'Ar-Rahman',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 56,
                'title_ar' => ' الواقِعَة',
                'title_dv' => ' އަލް ވާގިޢަ',
                'title_en' => 'Al-Waqi\'ah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 57,
                'title_ar' => 'الحَدید',
                'title_dv' => 'އަލް ޙަދީދް',
                'title_en' => 'Al-Hadid',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 58,
                'title_ar' => ' المجَادلة',
                'title_dv' => 'އަލް މުޖާދިލަ',
                'title_en' => 'Al-Mujadilah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 59,
                'title_ar' => 'لحَشر',
                'title_dv' => ' އަލް ޙަޝްރް',
                'title_en' => 'Al-Hashr',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 60,
                'title_ar' => 'لمُمتَحنَة',
                'title_dv' => ' އަލް މުމްތަޙާނަ',
                'title_en' => 'Al-Mumtahanah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 61,
                'title_ar' => ' الصَّف',
                'title_dv' => 'އަލް ޞަފް',
                'title_en' => 'As-Saff ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 62,
                'title_ar' => ' الجُمُعَة',
                'title_dv' => 'އަލް ޖުމްޢަ',
                'title_en' => 'Al-Jumu\'ah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 63,
                'title_ar' => 'لمنَافِقون',
                'title_dv' => 'އަލް މުނާފިޤޫން',
                'title_en' => 'Al-Munafiqun',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 64,
                'title_ar' => 'التّغَابُن',
                'title_dv' => 'އައްތަޣާބުން',
                'title_en' => 'At-Taghabun',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 65,
                'title_ar' => ' الطّلاَق',
                'title_dv' => 'އައްތަލަޤް',
                'title_en' => 'At-Talaq',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 66,
                'title_ar' => ' التّحْریم',
                'title_dv' => 'އައްތަޙްރީމް',
                'title_en' => 'At-Tahrim',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 67,
                'title_ar' => ' المُلک',
                'title_dv' => 'އަލް މުލްކު',
                'title_en' => 'Al-Mulk ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 68,
                'title_ar' => ' القَلَم',
                'title_dv' => 'އަލް ޤަލަމް',
                'title_en' => 'Al-Qalam',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 69,
                'title_ar' => ' الحَاقَّة',
                'title_dv' => 'އަލް ޙާއްޤަ',
                'title_en' => 'Al-Haqqah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 70,
                'title_ar' => 'المعَارج',
                'title_dv' => 'އަލް މަޢާރިޖް',
                'title_en' => 'Al-Ma\'arij',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 71,
                'title_ar' => ' نُوح',
                'title_dv' => 'ނޫޙް',
                'title_en' => 'Nuh',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 72,
                'title_ar' => ' الجنّ',
                'title_dv' => 'އަލް ޖިންނި',
                'title_en' => 'Al-Jinn ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 73,
                'title_ar' => ' المُزمّل',
                'title_dv' => 'އަލް މުޒަންމިލް',
                'title_en' => 'Al-Muzzammil',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 74,
                'title_ar' => 'المدَّثِّر',
                'title_dv' => 'އަލް މުއްދަޡިރް',
                'title_en' => 'Al-Muddaththir',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 75,
                'title_ar' => 'القِیَامَة',
                'title_dv' => 'އަލް ޤިޔާމަ',
                'title_en' => 'Al-Qiyamah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 76,
                'title_ar' => ' الإنسَان',
                'title_dv' => 'އަލް އިންސާން',
                'title_en' => 'Al-Insan',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 77,
                'title_ar' => 'المُرسَلات',
                'title_dv' => 'އަލް މުރްސަލާތް',
                'title_en' => 'Al-Mursalat',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 78,
                'title_ar' => ' النّبَإِ',
                'title_dv' => 'އަންނަބާ',
                'title_en' => 'An-Naba ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 79,
                'title_ar' => 'لنَّازعَات',
                'title_dv' => 'އަންނާޒިޢާ',
                'title_en' => 'An-Nazi\'at',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 80,
                'title_ar' => 'عَبَسَ',
                'title_dv' => 'ޢަބަސަ',
                'title_en' => 'Abasa',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 81,
                'title_ar' => 'التّکویر',
                'title_dv' => ' އައްތަކްވީރް',
                'title_en' => 'At-Takwir',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 82,
                'title_ar' => ' الانفِطار',
                'title_dv' => 'އަލް އިންފިތާރް',
                'title_en' => 'Al-Infitar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 83,
                'title_ar' => 'المطفّفِین',
                'title_dv' => 'އަލް މުތައްފިފީން',
                'title_en' => 'Al-Mutaffifin',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 84,
                'title_ar' => 'الانشقاق',
                'title_dv' => 'އަލް އަންޝިޤާޤް',
                'title_en' => 'Al-Inshiqaq',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 85,
                'title_ar' => ' البُرُوج',
                'title_dv' => 'އަލް ބުރޫޖް',
                'title_en' => 'Al-Buruj',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 86,
                'title_ar' => 'الطّارق',
                'title_dv' => 'އައްތޯރިޤް',
                'title_en' => 'At-Tariq',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 87,
                'title_ar' => '  الاٴعلی',
                'title_dv' => 'އަލް ޢަޢްލާ',
                'title_en' => 'Al-A\'la ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 88,
                'title_ar' => 'الغَاشِیَة',
                'title_dv' => 'އަލް ޣާޝިޔަ',
                'title_en' => 'Al-Ghashiyah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 89,
                'title_ar' => 'الفَجر',
                'title_dv' => 'އަލް ފަޖްރް',
                'title_en' => 'Al-Fajr ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 90,
                'title_ar' => ' البَلَد',
                'title_dv' => 'އަލް ބަލަދް',
                'title_en' => 'Al-Balad',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 91,
                'title_ar' => ' الشّمس',
                'title_dv' => 'އައްޝަމްސް',
                'title_en' => 'Ash-Shams',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 92,
                'title_ar' => ' اللیْل',
                'title_dv' => 'އައްލައިލް',
                'title_en' => 'Al-Lail ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 93,
                'title_ar' => ' الضّحیٰ',
                'title_dv' => 'އައްޟުޙާ',
                'title_en' => 'Ad-Duha ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 94,
                'title_ar' => ' الشَّرح',
                'title_dv' => 'އަލް އިންޝިރާޙް',
                'title_en' => 'Ash-Sharh',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 95,
                'title_ar' => ' التِّین',
                'title_dv' => 'އައްތީން',
                'title_en' => 'At-Tin',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 96,
                'title_ar' => ' العَلق',
                'title_dv' => 'އަލްޢަލަޤް',
                'title_en' => 'Al-\'Alaq',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 97,
                'title_ar' => 'القَدر',
                'title_dv' => 'އަލްޤަދްރް',
                'title_en' => 'Al-Qadr ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 98,
                'title_ar' => ' البَیّنَة',
                'title_dv' => 'އަލްބައްޔިނަ',
                'title_en' => 'Al-Baiyinah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 99,
                'title_ar' => ' الزّلزَلة',
                'title_dv' => 'އައްޒަލްޒަލަ',
                'title_en' => 'Az-Zalzala',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 100,
                'title_ar' => 'العَادیَات',
                'title_dv' => 'އަލްޢާދިޔަ',
                'title_en' => 'Al-\'Adiyat',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('quran_recitations')->insert(array (
            0 => 
            array (
                'id' => 101,
                'title_ar' => ' القَارعَة',
                'title_dv' => 'އަލްޤާރިޢަ',
                'title_en' => 'Al-Qari\'ah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 102,
                'title_ar' => ' التّکاثُر',
                'title_dv' => 'އައްތަކާޡުރް',
                'title_en' => 'At-Takathur',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 103,
                'title_ar' => ' العَصر',
                'title_dv' => 'އަލްޢަޞްރް',
                'title_en' => 'Al-\'Asr',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 104,
                'title_ar' => ' الهُمَزة',
                'title_dv' => 'އަލް ހުމަޒަ',
                'title_en' => 'Al-Humazah',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 105,
                'title_ar' => 'الفِیل',
                'title_dv' => 'އަލް ފީލް',
                'title_en' => 'Al-Fil ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 106,
                'title_ar' => ' القُرَیش',
                'title_dv' => 'ޤުރައިޝް',
                'title_en' => 'Quraish',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 107,
                'title_ar' => ' المَاعون',
                'title_dv' => 'އަލް މާޢޫން',
                'title_en' => 'Al-Ma\'un',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 108,
                'title_ar' => ' الکَوثَر',
                'title_dv' => 'އަލް ކައުޡަރް',
                'title_en' => 'Al-Kauthar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 109,
                'title_ar' => ' الکافِرون',
                'title_dv' => 'އަލް ކާފިރޫން',
                'title_en' => 'Al-Kafirun',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 110,
                'title_ar' => 'النّصر',
                'title_dv' => 'އައްނަޞްރް',
                'title_en' => 'An-Nasr',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 111,
                'title_ar' => ' المَسَد',
                'title_dv' => 'އަލް މަސަދް',
                'title_en' => 'Al-Masad',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 112,
                'title_ar' => ' الإخلاص',
                'title_dv' => 'އަލް އިޚްލާޞް',
                'title_en' => 'Al-Ikhlas',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 113,
                'title_ar' => ' الفَلَق',
                'title_dv' => 'އަލް ފަލަޤް',
                'title_en' => 'Al-Falaq',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 114,
                'title_ar' => ' النَّاس',
                'title_dv' => 'އައްނާސް',
                'title_en' => 'An-Nas ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}