<?php

use Illuminate\Database\Seeder;

class ZakatIslandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('zakat_islands')->delete();
        
        \DB::table('zakat_islands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'atoll_id' => 1,
                'name' => 'Male\'',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'atoll_id' => 2,
                'name' => 'Thuraakunu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'atoll_id' => 2,
                'name' => 'Uligamu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'atoll_id' => 2,
                'name' => 'Hathifushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'atoll_id' => 2,
                'name' => 'Mulhadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'atoll_id' => 2,
                'name' => 'Hoarafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'atoll_id' => 2,
                'name' => 'Ihavandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'atoll_id' => 2,
                'name' => 'Kelaa',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'atoll_id' => 2,
                'name' => 'Vashafaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'atoll_id' => 2,
                'name' => 'Dhiddhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'atoll_id' => 2,
                'name' => 'Filladhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'atoll_id' => 2,
                'name' => 'Maarandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'atoll_id' => 2,
                'name' => 'Thakandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'atoll_id' => 2,
                'name' => 'Utheemu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'atoll_id' => 2,
                'name' => 'Muraidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'atoll_id' => 2,
                'name' => 'Baarah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'atoll_id' => 3,
                'name' => 'Faridhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'atoll_id' => 3,
                'name' => 'Hanimaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'atoll_id' => 3,
                'name' => 'Finey',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'atoll_id' => 3,
                'name' => 'Naivaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'atoll_id' => 3,
                'name' => 'Nellaidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'atoll_id' => 3,
                'name' => 'Hirimaradhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'atoll_id' => 3,
                'name' => 'Nolhivaranfaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'atoll_id' => 3,
                'name' => 'Nolhivaram',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'atoll_id' => 3,
                'name' => 'Kurinbi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'atoll_id' => 3,
                'name' => 'Kunburudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'atoll_id' => 3,
                'name' => 'Kulhudhuffushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'atoll_id' => 3,
                'name' => 'Kumundhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'atoll_id' => 3,
                'name' => 'Neykurendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'atoll_id' => 3,
                'name' => 'Vaikaradhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'atoll_id' => 3,
                'name' => 'Maavaidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'atoll_id' => 3,
                'name' => 'Makunudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'atoll_id' => 4,
                'name' => 'Noomaraa',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'atoll_id' => 4,
                'name' => 'Kanditheemu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'atoll_id' => 4,
                'name' => 'Goidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'atoll_id' => 4,
                'name' => 'Feydhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'atoll_id' => 4,
                'name' => 'Feevah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'atoll_id' => 4,
                'name' => 'Bileffahi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'atoll_id' => 4,
                'name' => 'Foakaidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'atoll_id' => 4,
                'name' => 'Narudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'atoll_id' => 4,
                'name' => 'Maroshi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'atoll_id' => 4,
                'name' => 'Lhaimagu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'atoll_id' => 4,
                'name' => 'Funadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'atoll_id' => 4,
                'name' => 'Komandoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'atoll_id' => 4,
                'name' => 'Maaungoodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'atoll_id' => 5,
                'name' => 'Henbadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'atoll_id' => 5,
                'name' => 'Kendhikolhudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'atoll_id' => 5,
                'name' => 'Maalhendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'atoll_id' => 5,
                'name' => 'Kudafari',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'atoll_id' => 5,
                'name' => 'Landhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'atoll_id' => 5,
                'name' => 'Maafaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'atoll_id' => 5,
                'name' => 'Lhohi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'atoll_id' => 5,
                'name' => 'Miladhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'atoll_id' => 5,
                'name' => 'Magoodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'atoll_id' => 5,
                'name' => 'Manadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'atoll_id' => 5,
                'name' => 'Holhudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'atoll_id' => 5,
                'name' => 'Foddhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'atoll_id' => 5,
                'name' => 'Velidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'atoll_id' => 6,
                'name' => 'Alifushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'atoll_id' => 6,
                'name' => 'Vaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'atoll_id' => 6,
                'name' => 'Rasgetheemu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'atoll_id' => 6,
                'name' => 'Angolhitheemu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'atoll_id' => 6,
                'name' => 'Ungoofaaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'atoll_id' => 6,
                'name' => 'Kandholhudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'atoll_id' => 6,
                'name' => 'Maakurathu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'atoll_id' => 6,
                'name' => 'Rasmaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'atoll_id' => 6,
                'name' => 'Innamaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'atoll_id' => 6,
                'name' => 'Maduvvari',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'atoll_id' => 6,
                'name' => 'Inguraidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'atoll_id' => 6,
                'name' => 'Meedhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'atoll_id' => 6,
                'name' => 'Fainu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'atoll_id' => 6,
                'name' => 'Kinolhas',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'atoll_id' => 7,
                'name' => 'Kudarikilu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'atoll_id' => 7,
                'name' => 'Kamadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'atoll_id' => 7,
                'name' => 'Kendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'atoll_id' => 7,
                'name' => 'Kihaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'atoll_id' => 7,
                'name' => 'Dhonfanu',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'atoll_id' => 7,
                'name' => 'Dharavandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'atoll_id' => 7,
                'name' => 'Maalhos',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'atoll_id' => 7,
                'name' => 'Eydhafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'atoll_id' => 7,
                'name' => 'Thulhaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'atoll_id' => 7,
                'name' => 'Hithaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'atoll_id' => 7,
                'name' => 'Fulhadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'atoll_id' => 7,
                'name' => 'Fehendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'atoll_id' => 7,
                'name' => 'Goidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'atoll_id' => 8,
                'name' => 'Hinnavaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'atoll_id' => 8,
                'name' => 'Naifaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'atoll_id' => 8,
                'name' => 'Kurendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'atoll_id' => 8,
                'name' => 'Olhuvelifushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'atoll_id' => 9,
                'name' => 'Kaashidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'atoll_id' => 9,
                'name' => 'Gaafaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'atoll_id' => 9,
                'name' => 'Dhiffushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'atoll_id' => 9,
                'name' => 'Thulusdhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'atoll_id' => 9,
                'name' => 'Huraa',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'atoll_id' => 9,
                'name' => 'Himmafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'atoll_id' => 9,
                'name' => 'Gulhi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'atoll_id' => 9,
                'name' => 'Maafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'atoll_id' => 9,
                'name' => 'Guraidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'atoll_id' => 10,
                'name' => 'Thoddoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'atoll_id' => 10,
                'name' => 'Rasdhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'atoll_id' => 10,
                'name' => 'Ukulhas',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'atoll_id' => 10,
                'name' => 'Mathiveri',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'atoll_id' => 10,
                'name' => 'Bodufolhudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'atoll_id' => 10,
                'name' => 'Feridhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'atoll_id' => 10,
                'name' => 'Maalhos',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'atoll_id' => 10,
                'name' => 'Himandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'atoll_id' => 11,
                'name' => 'Haggnaameedhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'atoll_id' => 11,
                'name' => 'Omadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'atoll_id' => 11,
                'name' => 'Kunburudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'atoll_id' => 11,
                'name' => 'Mahibadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'atoll_id' => 11,
                'name' => 'Mandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'atoll_id' => 11,
                'name' => 'Dhangethi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'atoll_id' => 11,
                'name' => 'Dhigurah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'atoll_id' => 11,
                'name' => 'Dhiddhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'atoll_id' => 11,
                'name' => 'Fenfushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'atoll_id' => 11,
                'name' => 'Maamigili',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'atoll_id' => 12,
                'name' => 'Fulidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'atoll_id' => 12,
                'name' => 'Thinadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'atoll_id' => 12,
                'name' => 'Felidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'atoll_id' => 12,
                'name' => 'Keyodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'atoll_id' => 12,
                'name' => 'Rakeedhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'atoll_id' => 13,
                'name' => 'Dhiggaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'atoll_id' => 13,
                'name' => 'Maduvvari',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'atoll_id' => 13,
                'name' => 'Raimmandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'atoll_id' => 13,
                'name' => 'Madifushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'atoll_id' => 13,
                'name' => 'Veyvah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'atoll_id' => 13,
                'name' => 'Mulah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'atoll_id' => 13,
                'name' => 'Muli',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'atoll_id' => 13,
                'name' => 'Naalaafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'atoll_id' => 13,
                'name' => 'Kolhufushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'atoll_id' => 14,
                'name' => 'Feeali',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'atoll_id' => 14,
                'name' => 'Bileddhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'atoll_id' => 14,
                'name' => 'Magoodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'atoll_id' => 14,
                'name' => 'Dharanboodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'atoll_id' => 14,
                'name' => 'Nilandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'atoll_id' => 15,
                'name' => 'Meedhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'atoll_id' => 15,
                'name' => 'Bandidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'atoll_id' => 15,
                'name' => 'Rinbudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'atoll_id' => 15,
                'name' => 'Hulhudheli',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'atoll_id' => 15,
                'name' => 'Vaanee',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'atoll_id' => 15,
                'name' => 'Maaenboodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'atoll_id' => 15,
                'name' => 'Kudahuvadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'atoll_id' => 16,
                'name' => 'Buruni',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'atoll_id' => 16,
                'name' => 'Vilufushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'atoll_id' => 16,
                'name' => 'Madifushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'atoll_id' => 16,
                'name' => 'Dhiyamigili',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'atoll_id' => 16,
                'name' => 'Guraidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'atoll_id' => 16,
                'name' => 'Kandoodhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'atoll_id' => 16,
                'name' => 'Vandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'atoll_id' => 16,
                'name' => 'Hirilandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'atoll_id' => 16,
                'name' => 'Gaadhiffushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'atoll_id' => 16,
                'name' => 'Thimarafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'atoll_id' => 16,
                'name' => 'Veymandoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'atoll_id' => 16,
                'name' => 'Kinbidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'atoll_id' => 16,
                'name' => 'Omadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'atoll_id' => 17,
                'name' => 'Isdhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'atoll_id' => 17,
                'name' => 'Dhanbidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'atoll_id' => 17,
                'name' => 'Maabaidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'atoll_id' => 17,
                'name' => 'Mundoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'atoll_id' => 17,
                'name' => 'Kalhaidhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'atoll_id' => 17,
                'name' => 'Gan',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'atoll_id' => 17,
                'name' => 'Maavah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'atoll_id' => 17,
                'name' => 'Fonadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'atoll_id' => 17,
                'name' => 'Gaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'atoll_id' => 17,
                'name' => 'Maamendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'atoll_id' => 17,
                'name' => 'Hithadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'atoll_id' => 17,
                'name' => 'Kunahandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'atoll_id' => 18,
                'name' => 'Kolamaafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'atoll_id' => 18,
                'name' => 'Villingili',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'atoll_id' => 18,
                'name' => 'Maamendhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'atoll_id' => 18,
                'name' => 'Nilandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'atoll_id' => 18,
                'name' => 'Dhaandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'atoll_id' => 18,
                'name' => 'Dhevvadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'atoll_id' => 18,
                'name' => 'Kondey',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'atoll_id' => 18,
                'name' => 'Dhiyadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'atoll_id' => 18,
                'name' => 'Gemanafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'atoll_id' => 18,
                'name' => 'Kanduhulhudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'atoll_id' => 19,
                'name' => 'Thinadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'atoll_id' => 19,
                'name' => 'Madaveli',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'atoll_id' => 19,
                'name' => 'Hoandeddhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'atoll_id' => 19,
                'name' => 'Nadallaa',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'atoll_id' => 19,
                'name' => 'Gaddhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'atoll_id' => 19,
                'name' => 'Rathafandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'atoll_id' => 19,
                'name' => 'Vaadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'atoll_id' => 19,
                'name' => 'Fiyoaree',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'atoll_id' => 19,
                'name' => 'Faresmaathodaa',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'atoll_id' => 20,
                'name' => 'Fuvahmulah',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'atoll_id' => 21,
                'name' => 'Hulhudhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'atoll_id' => 21,
                'name' => 'Hithadhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'atoll_id' => 21,
                'name' => 'Maradhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'atoll_id' => 21,
                'name' => 'Feydhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'atoll_id' => 21,
                'name' => 'Gan',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'atoll_id' => 4,
                'name' => 'Milandhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'atoll_id' => 6,
                'name' => 'Dhuvaafaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'atoll_id' => 6,
                'name' => 'Hulhudhuffaaru',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'atoll_id' => 8,
                'name' => 'Maafilaafushi',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'atoll_id' => 21,
                'name' => 'Meedhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'atoll_id' => 21,
                'name' => 'Maradhoofeydhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'atoll_id' => 21,
                'name' => 'Hulhumeedhoo',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'atoll_id' => 1,
                'name' => 'Hulhule\'',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'atoll_id' => 1,
                'name' => 'Hulhumale\'',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'atoll_id' => 1,
                'name' => 'Villingili',
                'created_at' => '2020-04-20 00:00:00',
                'updated_at' => '2020-04-20 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}