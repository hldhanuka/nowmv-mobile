<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ZakatCommoditySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zakat_commodities')->truncate();
        DB::table('zakat_commodities')->insert([
            [
                'id' => 1,
                'type' => "Normal Rice",
                'price' => 12,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 2,
                'type' => "Thailand White Rice",
                'price' => 52,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 3,
                'type' => "Red Rice",
                'price' => 78,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 4,
                'type' => "Basmati Rice (Aadhaige)",
                'price' => 65,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 5,
                'type' => "Basmati Rice (Molhu)",
                'price' => 89,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 6,
                'type' => "Basmati Rice (Emme Molhu)",
                'price' => 125,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 7,
                'type' => "Normal Flour",
                'price' => 10,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'id' => 8,
                'type' => "Atta Flour",
                'price' => 59,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ]);
    }
}
