<?php

use Illuminate\Database\Seeder;

class MtccCalendarAnnualHolidaysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mtcc_calendar_annual_holidays')->delete();
        
        \DB::table('mtcc_calendar_annual_holidays')->insert(array (
            0 => 
            array (
                'id' => 1,
                'day' => 1,
                'month' => 1,
                'description' => 'New Year',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'day' => 1,
                'month' => 5,
                'description' => 'Labour Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'day' => 26,
                'month' => 7,
                'description' => 'Independance Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'day' => 3,
                'month' => 11,
                'description' => 'Victory Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'day' => 9,
                'month' => 12,
                'description' => 'The day Maldives embraced Islam',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'day' => 11,
                'month' => 11,
                'description' => 'Republic Day',
                'created_at' => '2020-04-08 00:00:00',
                'updated_at' => '2020-04-08 00:00:00',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}