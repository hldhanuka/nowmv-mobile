<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NewsAgencyUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_agency_users')->truncate();
        DB::table('news_agency_users')->insert([
            [
                'user_id' => 6,
                'news_agency_id' => 1,
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
            [
                'user_id' => 7,
                'news_agency_id' => 2,
                'status' => 1,
                'created_by' => 1,
                'authorized_by' => 1,
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ]
        ]);
    }
}
