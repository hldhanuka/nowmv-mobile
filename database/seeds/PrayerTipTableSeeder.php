<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class PrayerTipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prayer_tips')->truncate();
        DB::table('prayer_tips')->insert([
            [
                'title' => "tip_1",
                'content' => "As one of the biggest industries in the world, the tourism",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => "tip_2",
                'content' => "As one of the biggest industries in the world",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'title' => "tip_3",
                'content' => "As one of the biggest",
                'status' => 1,
                'created_by' => "2",
                'authorized_by' => "1",
                'published_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
