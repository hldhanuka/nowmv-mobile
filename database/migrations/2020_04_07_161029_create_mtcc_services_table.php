<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',45)->default('Premium ferry')->nullable();
            $table->string('description', 2048)->nullable();
            $table->string('summary', 300)->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->tinyInteger('enable_route_link')->default(1)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_services');
    }
}
