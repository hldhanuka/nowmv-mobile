<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccCalendarAnnualHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_calendar_annual_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('day')->nullable();
            $table->integer('month')->nullable();
            $table->string('description',512)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_calendar_annual_holidays');
    }
}
