<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_meals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id');
            $table->bigInteger('meal_id');
            $table->string('created_by')->nullable();
            $table->string('authorised_by')->nullable();
            $table->string('published_at')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_meals');
    }
}
