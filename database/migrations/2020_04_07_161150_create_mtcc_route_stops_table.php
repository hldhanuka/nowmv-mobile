<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccRouteStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_route_stops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('route_id')->nullable();
            $table->char('direction')->default('F')->nullable();
            $table->integer('sequence')->default(	1)->nullable();
            $table->bigInteger('ticketing_service_point')->default(	1)->nullable();
            $table->bigInteger('originating_service_point')->default(	NULL)->nullable();
            $table->bigInteger('destination_service_point')->default(	NULL)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_route_stops');
    }
}
