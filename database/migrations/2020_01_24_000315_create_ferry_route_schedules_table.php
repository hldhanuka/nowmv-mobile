<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFerryRouteSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ferry_route_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('schedule_type_id');
            $table->bigInteger('ferry_route_id');
            $table->time('starting_time')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['schedule_type_id','status']);
            $table->index(['ferry_route_id','status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ferry_route_schedules');
    }
}
