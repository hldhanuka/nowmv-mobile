<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ngo_id')->default(0)->nullable();
            $table->string('title')->nullable();
            $table->decimal('max_limit',22,2)->nullable();
            $table->string('image',500)->nullable();
            $table->string('tag')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('type')->nullable();
            $table->text('content')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('authorized_by')->nullable();
            $table->dateTime('published_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
