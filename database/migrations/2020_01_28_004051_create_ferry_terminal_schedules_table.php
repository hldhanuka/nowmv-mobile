<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFerryTerminalSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ferry_terminal_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ferry_route_id')->nullable();
            $table->bigInteger('start_location_id')->nullable();
            $table->bigInteger('terminal_id')->nullable();
            $table->time('time')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['ferry_route_id','start_location_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ferry_terminal_schedules');
    }
}
