<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZakatPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zakat_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('full_name')->nullable();
            $table->string('id_number')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('atoll_id')->nullable();
            $table->bigInteger('island_id')->nullable();
            $table->integer('no_of_person')->nullable();
            $table->bigInteger('commodity_id')->nullable();
            $table->decimal('zakat_amount',22,2)->nullable();
            $table->decimal('sadagath_amount',22,2)->nullable();
            $table->decimal('total_amount',22,2)->nullable();
            $table->string('payment_method');
            $table->tinyInteger('status')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('payment_invoice_number')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('result_data_reference_id')->nullable();
            $table->text('result_data_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zakat_payments');
    }
}
