<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('from')->nullable();
            $table->bigInteger('to')->nullable();
            $table->tinyInteger('seen')->nullable();
            $table->text('message')->nullable();
            $table->string('type')->nullable();
            $table->bigInteger('relation_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['to','seen']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notifications');
    }
}
