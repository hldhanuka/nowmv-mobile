<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodAndIsBestColumnsToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->tinyInteger('is_best')->after('image')->default(0)->nullable();
            $table->tinyInteger('for_male')->after('is_best')->default(1)->nullable();
            $table->tinyInteger('for_female')->after('for_male')->default(1)->nullable();
            $table->integer('min_age')->after('for_female')->default(null)->nullable();
            $table->integer('max_age')->after('min_age')->default(null)->nullable();
            $table->dateTime('start_datetime')->after('max_age')->default(null)->nullable();
            $table->dateTime('end_datetime')->after('start_datetime')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('is_best');
            $table->dropColumn('for_male');
            $table->dropColumn('for_female');
            $table->dropColumn('min_age');
            $table->dropColumn('max_age');
            $table->dropColumn('start_datetime');
            $table->dropColumn('end_datetime');
        });
    }
}
