<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsTargetOfferAndIsSendPushNotificationToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->tinyInteger('is_target_offer')->after('has_redeem')->default(0)->nullable();
            $table->tinyInteger('is_send_push_notification')->after('is_target_offer')->default(0)->nullable();
            $table->text('push_message')->after('is_send_push_notification')->nullable();
            $table->tinyInteger('push_notification_status')->after('push_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('is_target_offer');
            $table->dropColumn('is_send_push_notification');
            $table->dropColumn('push_message');
            $table->dropColumn('push_notification_status');
        });
    }
}
