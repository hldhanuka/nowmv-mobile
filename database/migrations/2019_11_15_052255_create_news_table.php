<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id');
            $table->string('title')->nullable();
            $table->string('image',500)->nullable();
            $table->string('video',500)->nullable();
            $table->string('video_thumbnail',500)->nullable();
            $table->text('content')->nullable();
            $table->text('html_content')->nullable();
            $table->string('author')->nullable();
            $table->string('link',500)->nullable();
            $table->string('tags')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('is_featured')->nullable();
            $table->bigInteger('news_agency_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('authorized_by')->nullable();
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['category_id', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
