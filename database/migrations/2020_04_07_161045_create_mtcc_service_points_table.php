<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccServicePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_service_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type',45)->default('ticketing_terminal')->nullable();
            $table->string('name', 75)->default('Service Point')->nullable();
            $table->string('description', 150)->default('Service Point')->nullable();
            $table->bigInteger('map_location_id')->default(1)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_service_points');
    }
}
