<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type',25)->default('Ferry Link')->nullable();
            $table->string('name',100)->default('Default route name')->nullable();
            $table->string('description',2500)->default('Default route description')->nullable();
            $table->float('fare_adult')->default(0)->nullable();
            $table->float('fare_underage')->default(0)->nullable();
            $table->float('fare_infant')->default(0)->nullable();
            $table->text('accessories_allowed')->default(NULL)->nullable();
            $table->integer('capacity')->default(NULL)->nullable();
            $table->string('fare_note')->default(NULL)->nullable();
            $table->integer('duration')->default(NULL)->nullable();
            $table->bigInteger('origin_service_point_id')->default(NULL)->nullable();
            $table->bigInteger('destination_service_point_id')->default(NULL)->nullable();
            $table->string('contact_detail', 1000)->default(NULL)->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->bigInteger('service_id')->default(NULL)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_routes');
    }
}
