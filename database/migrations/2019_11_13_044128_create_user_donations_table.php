<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_donations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('donation_id');
            $table->string('payment_method');
            $table->tinyInteger('status')->nullable();
            $table->decimal('amount',22,2)->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('payment_invoice_number')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('result_data_reference_id')->nullable();
            $table->text('result_data_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_donations');
    }
}
