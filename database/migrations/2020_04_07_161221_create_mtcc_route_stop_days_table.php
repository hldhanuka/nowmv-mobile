<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccRouteStopDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_route_stop_days', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('route_stop_id')->nullable();
            $table->string('month_type',45)->default('normal')->nullable();
            $table->string('day_types',45)->default('sat,sun,mon,tue,wed,thu')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_route_stop_days');
    }
}
