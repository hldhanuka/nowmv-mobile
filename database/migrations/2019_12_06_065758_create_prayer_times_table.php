<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrayerTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prayer_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('atoll_id');
            $table->integer('prayer_id');
            $table->integer('day')->nullable();
            $table->integer('month_id')->nullable();
            $table->time('time')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['atoll_id','prayer_id','day','month_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prayer_times');
    }
}
