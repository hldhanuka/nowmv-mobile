<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusHaltSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_halt_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bus_route_id')->nullable();
            $table->bigInteger('start_location_id')->nullable();
            $table->bigInteger('halt_id')->nullable();
            $table->time('time')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['bus_route_id','status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_halt_schedules');
    }
}
