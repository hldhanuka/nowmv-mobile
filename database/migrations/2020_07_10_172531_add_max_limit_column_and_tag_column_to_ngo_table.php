<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxLimitColumnAndTagColumnToNgoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngos', function (Blueprint $table) {
            $table->decimal('max_limit',22,2)->after('content')->nullable();
            $table->string('tag')->after('max_limit')->nullable();
            $table->string('type')->default("ngo")->after('tag')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngos', function (Blueprint $table) {
            $table->dropColumn('max_limit');
            $table->dropColumn('tag');
            $table->dropColumn('type');
        });
    }
}
