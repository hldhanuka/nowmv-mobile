<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccCalendarFixedHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_calendar_fixed_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('date')->nullable();;
            $table->string('description',128)->nullable();;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_calendar_fixed_holidays');
    }
}
