<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccRouteStopMultipointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_route_stop_multipoints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('route_id')->nullable();
            $table->integer('sequence')->default(	1)->nullable();
            $table->bigInteger('originating_service_point')->default(	NULL)->nullable();
            $table->bigInteger('destination_service_point')->default(	NULL)->nullable();
            $table->integer('duration')->default(NULL)->nullable();
            $table->integer('wait')->default(NULL)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_route_stop_multipoints');
    }
}
