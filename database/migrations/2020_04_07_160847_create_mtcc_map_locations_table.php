<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtccMapLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtcc_map_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('island_id')->default(1)->nullable();
            $table->string('label')->default('Location Label')->nullable();
            $table->decimal('latitude', 18, 8)->default(	4.21261000)->nullable();
            $table->decimal('longitude', 18, 8)->default(	73.54220000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtcc_map_locations');
    }
}
