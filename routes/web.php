<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/about', 'PageController@about')->name('about');

// Public Share Links
Route::get('share/{code}', 'ShortLinkController@shortenLink')->name('shorten-link');
Route::get('share/event/{event_id}/{code}', 'ShareController@shareEvent')->name("share-event");
Route::get('share/badge/{badge_id}/{code}', 'ShareController@shareBadge')->name("share-badge"); // Not Use Now
Route::get('share/ngo/{ngo_id}', 'ShareController@shareNgo')->name("share-ngo");
Route::get('share/ngo/{ngo_id}/{badge_code}', 'ShareController@shareNgoByBadgeCode')->name("share-ngo-by-badge_code");
Route::get('share/donation/{donation_id}', 'ShareController@shareDonation')->name("share-donation");
Route::get('share/donation/{donation_id}/{badge_code}', 'ShareController@shareDonationByBadgeCode')->name("share-donation-by-badge_code");


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes... (Unwanted)
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('test-pdf', 'API\QuranRecitationController@testPDF');
Route::get('download-quran-recitation/{pdf_id}', 'API\QuranRecitationController@downloadQuranRecitationPDF');

Route::group(['middleware' => 'auth'], function () {

    //imi games
    Route::get('promo-games', 'CMS\IMIGamesStatsController@games')->name('imi_games.promo-games');
    Route::get('game-by-game-id', 'CMS\IMIGamesStatsController@getGameByGameUUID')->name('imi_games.game-details');

    Route::get('imi-games-reward-list', 'CMS\IMIGamesStatsController@getRewardList')->name('imi_games.reward-list');

    Route::get('game-subscription', 'CMS\IMIGamesStatsController@gameSubscriptionIndex')->name('imi_games.game-subscription-index');
    Route::get('game-unsubscription', 'CMS\IMIGamesStatsController@gameUnsubscriptionIndex')->name('imi_games.game-unsubscription-index');
    Route::get('game-revenue', 'CMS\IMIGamesStatsController@gameRevenueIndex')->name('imi_games.revenue-index');
    Route::get('game-leaderboard', 'CMS\IMIGamesStatsController@gameLeaderBoardIndex')->name('imi_games.leaderboard-index');
    Route::get('game-winners', 'CMS\IMIGamesStatsController@getWinnersData')->name('imi_games.get-winners-data');
    Route::get('game-winner-send-package', 'CMS\IMIGamesStatsController@gameWinnerSendPackage')->name('imi_games.game-winner-send-package');
    Route::post('game-winner-send-package', 'CMS\IMIGamesStatsController@gameWinnerSendPackageSubmit')->name('imi_games.game-winner-send-package');

    // Home Route
    Route::get('home', 'HomeController@home')->name('home');
    // Ads Routes
    Route::resource('ads', 'CMS\AdController');
    Route::post('ads/change-status', 'CMS\AdController@changeStatus')->name('ads.change-status');
    Route::get('ads-load-data', 'CMS\AdController@loadData')->name('ads.load-data');
    // NGO Merchants Routes
    Route::resource('ngo_merchants', 'CMS\NgoMerchantController');
    Route::post('ngo_merchants/change-status', 'CMS\NgoMerchantController@changeStatus')->name('ngo_merchants.change-status');
    Route::get('ngo_merchants-load-data', 'CMS\NgoMerchantController@loadData')->name('ngo_merchants.load-data');
    // Donations Routes
    Route::resource('donations', 'CMS\DonationController');
    Route::post('donations/change-status', 'CMS\DonationController@changeStatus')->name('donations.change-status');
    Route::get('donations-load-data', 'CMS\DonationController@loadData')->name('donations.load-data');
    // Badges Routes
    Route::resource('badges', 'CMS\BadgeController');
    Route::post('badges/change-status', 'CMS\BadgeController@changeStatus')->name('badges.change-status');
    Route::get('badges-load-data', 'CMS\BadgeController@loadData')->name('badges.load-data');
    // Event Tags Routes
    Route::resource('event_tags', 'CMS\EventTagController');
    Route::get('event_tags-load-data', 'CMS\EventTagController@loadData')->name('event_tags.load-data');
    // Events Routes
    Route::resource('events', 'CMS\EventController');
    Route::post('events/change-status', 'CMS\EventController@changeStatus')->name('events.change-status');
    Route::get('events-load-data', 'CMS\EventController@loadData')->name('events.load-data');
    // Interests Routes
    Route::resource('interests', 'CMS\InterestController');
    Route::post('interests/change-status', 'CMS\InterestController@changeStatus')->name('interests.change-status');
    Route::get('interests-load-data', 'CMS\InterestController@loadData')->name('interests.load-data');
    // Restaurants Routes
    Route::resource('restaurants', 'CMS\RestaurantController');
    Route::post('restaurants/change-status', 'CMS\RestaurantController@changeStatus')->name('restaurants.change-status');
    Route::get('restaurants-load-data', 'CMS\RestaurantController@loadData')->name('restaurants.load-data');
    // Restaurants Reviews Routes
    Route::resource('restaurant_reviews', 'CMS\RestaurantReviewController');
    Route::get('restaurant_reviews-load-data', 'CMS\RestaurantReviewController@loadData')->name('restaurant_reviews.load-data');
    // Meals Categories Routes
    Route::resource('meal_categories', 'CMS\MealCategoryController');
    Route::post('meal_categories/change-status', 'CMS\MealCategoryController@changeStatus')->name('meal_categories.change-status');
    Route::get('meal_categories-load-data', 'CMS\MealCategoryController@loadData')->name('meal_categories.load-data');
    Route::get('load-meal-categories-by-restaurant-id/{restaurant_id}', 'CMS\MealCategoryController@loadMealCategoriesByRestaurantId')->name('load-meal-categories-by-restaurant-id');
    // Meals Routes
    Route::resource('meals', 'CMS\MealController');
    Route::post('meals/change-status', 'CMS\MealController@changeStatus')->name('meals.change-status');
    Route::get('meals-load-data', 'CMS\MealController@loadData')->name('meals.load-data');
    // Meals Reviews Routes
    Route::resource('meal_reviews', 'CMS\MealReviewController');
    Route::get('meal_reviews-load-data', 'CMS\MealReviewController@loadData')->name('meal_reviews.load-data');
    // Recipes Routes
    Route::resource('recipes', 'CMS\RecipeController');
    Route::post('recipes/change-status', 'CMS\RecipeController@changeStatus')->name('recipes.change-status');
    Route::get('recipes-load-data', 'CMS\RecipeController@loadData')->name('recipes.load-data');
    // Recipes Reviews Routes
    Route::resource('recipe_reviews', 'CMS\RecipeReviewController');
    Route::get('recipe_reviews-load-data', 'CMS\RecipeReviewController@loadData')->name('recipe_reviews.load-data');
    // News Agencies Routes
    Route::resource('news_agencies', 'CMS\NewsAgencyController');
    Route::post('news_agencies/change-status', 'CMS\NewsAgencyController@changeStatus')->name('news_agencies.change-status');
    Route::get('news_agencies-load-data', 'CMS\NewsAgencyController@loadData')->name('news_agencies.load-data');
    // News Categories Routes
    Route::resource('news_categories', 'CMS\NewsCategoryController');
    Route::post('news_categories/change-status', 'CMS\NewsCategoryController@changeStatus')->name('news_categories.change-status');
    Route::get('news_categories-load-data', 'CMS\NewsCategoryController@loadData')->name('news_categories.load-data');
    // News Tags Routes
    Route::resource('news_tags', 'CMS\NewsTagController');
    Route::post('news_tags/change-status', 'CMS\NewsTagController@changeStatus')->name('news_tags.change-status');
    Route::get('news_tags-load-data', 'CMS\NewsTagController@loadData')->name('news_tags.load-data');
    // News Routes
    Route::resource('news', 'CMS\NewsController');
    Route::post('news/change-status', 'CMS\NewsController@changeStatus')->name('news.change-status');
    Route::get('news-load-data', 'CMS\NewsController@loadData')->name('news.load-data');
    // Merchants Routes
    Route::resource('merchants', 'CMS\MerchantController');
    Route::post('merchants/change-status', 'CMS\MerchantController@changeStatus')->name('merchants.change-status');
    Route::get('merchants-load-data', 'CMS\MerchantController@loadData')->name('merchants.load-data');
    // Offer Categories Routes
    Route::resource('offer_categories', 'CMS\OfferCategoryController');
    Route::post('offer_categories/change-status', 'CMS\OfferCategoryController@changeStatus')->name('offer_categories.change-status');
    Route::get('offer_categories-load-data', 'CMS\OfferCategoryController@loadData')->name('offer_categories.load-data');
    // Offers Routes
    Route::resource('offers', 'CMS\OfferController');
    Route::post('offers/change-status', 'CMS\OfferController@changeStatus')->name('offers.change-status');
    Route::get('offers-load-data', 'CMS\OfferController@loadData')->name('offers.load-data');
    // Calendar Schedule Types Routes
    Route::resource('calendar_schedule_types', 'CMS\CalendarScheduleTypeController');
    Route::get('calendar_schedule_types-load-data', 'CMS\CalendarScheduleTypeController@loadData')->name('calendar_schedule_types.load-data');
    // Bus Schedule Types Routes
    Route::resource('bus_schedule_types', 'CMS\BusScheduleTypeController');
    Route::get('bus_schedule_types-load-data', 'CMS\BusScheduleTypeController@loadData')->name('bus_schedule_types.load-data');
    // Bus Locations Routes
    Route::resource('bus_locations', 'CMS\BusLocationController');
    Route::get('bus_locations-load-data', 'CMS\BusLocationController@loadData')->name('bus_locations.load-data');
    // Bus Route Schedule Routes
    Route::resource('bus_route_schedules', 'CMS\BusRouteScheduleController');
    Route::get('bus_route_schedules-load-data', 'CMS\BusRouteScheduleController@loadData')->name('bus_route_schedules.load-data');
    // Bus Halt Schedule
    Route::resource('bus_halt_schedules', 'CMS\BusHaltScheduleController');
    Route::get('bus_halt_schedules-load-data', 'CMS\BusHaltScheduleController@loadData')->name('bus_halt_schedules.load-data');
    // Bus Routes Routes
    Route::resource('bus_routes', 'CMS\BusRouteController');
    Route::get('bus_routes-load-data', 'CMS\BusRouteController@loadData')->name('bus_routes.load-data');
    //Ngo Routes
    Route::resource('ngos', 'CMS\NgoController');
    Route::get('ngos-load-data', 'CMS\NgoController@loadData')->name('ngos.load-data');
    // Ferry Locations Routes
    Route::resource('ferry_locations', 'CMS\FerryLocationController');
    Route::get('ferry_locations-load-data', 'CMS\FerryLocationController@loadData')->name('ferry_locations.load-data');

    // Ferry Terminal Schedule
    Route::resource('ferry_terminal_schedules', 'CMS\FerryTerminalScheduleController');
    Route::get('ferry_terminal_schedules-load-data', 'CMS\FerryTerminalScheduleController@loadData')->name('ferry_terminal_schedules.load-data');

    // Ferry Routes Routes
    Route::resource('ferry_routes', 'CMS\FerryRouteController');
    Route::get('ferry_routes-load-data', 'CMS\FerryRouteController@loadData')->name('ferry_routes.load-data');

    // Ferry Route Schedule Routes
    Route::resource('ferry_route_schedules', 'CMS\FerryRouteScheduleController');
    Route::get('ferry_route_schedules-load-data', 'CMS\FerryRouteScheduleController@loadData')->name('ferry_route_schedules.load-data');

    // Prayer Tips Routes
    Route::resource('prayer_tips', 'CMS\PrayerTipController');
    Route::post('prayer_tips/change-status', 'CMS\PrayerTipController@changeStatus')->name('prayer_tips.change-status');
    Route::get('prayer_tips-load-data', 'CMS\PrayerTipController@loadData')->name('prayer_tips.load-data');
    // Support Teams Routes
    Route::resource('support_teams', 'CMS\SupportTeamController');
    Route::post('support_teams/change-status', 'CMS\SupportTeamController@changeStatus')->name('support_teams.change-status');
    Route::get('support_teams-load-data', 'CMS\SupportTeamController@loadData')->name('support_teams.load-data');
    // Videos Routes
    Route::resource('videos', 'CMS\VideoController');
    Route::post('videos/change-status', 'CMS\VideoController@changeStatus')->name('videos.change-status');
    Route::get('videos-load-data', 'CMS\VideoController@loadData')->name('videos.load-data');
    // Users Routes
    Route::resource('users', 'CMS\UserController');
    Route::get('users-load-data', 'CMS\UserController@loadData')->name('users.load-data');
    Route::post('users-reset-password', 'CMS\UserController@resetPassword')->name('users.reset-password');
    Route::post('users-status-change', 'CMS\UserController@userStatusChange')->name('users.users-status-change');

    // Ferry Schedule
    Route::resource('ferry_schedule_types', 'CMS\FerryScheduleTypeController');
    Route::get('ferry_schedule_types-load-data', 'CMS\FerryScheduleTypeController@loadData')->name('ferry_schedule_types.load-data');

    //zakat_payments
    Route::get('zakat-payments', 'CMS\ZakatPaymentController@index')->name('zakat_payments.index');
    Route::get('zakat-payments-load-data', 'CMS\ZakatPaymentController@loadData')->name('zakat_payments.load-data');
    Route::get('zakat-payments-csv', 'CMS\ZakatPaymentController@generateCsv')->name('zakat_payments.csv');
    Route::post('zakat-payments-download-report', 'CMS\ZakatPaymentController@export')->name('zakat_payments.download-report');

    // Settings Routes
    Route::get('settings/index', 'CMS\SettingController@index')->name('settings.index');
    Route::post('settings/update', 'CMS\SettingController@update')->name('settings.update');
    Route::get('settings-load-data', 'CMS\SettingController@loadData')->name('settings.load-data');

    // Push Notifications Routes
    Route::get('push_notifications/index', 'CMS\PushNotificationController@index')->name('push_notifications.index');
    Route::post('push_notifications/update', 'CMS\PushNotificationController@update')->name('push_notifications.update');
    Route::get('push_notifications-load-data', 'CMS\PushNotificationController@loadData')->name('push_notifications.load-data');

    // Profiles
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'CMS\ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'CMS\ProfileController@update']);
    Route::post('profile-image-update', ['as' => 'profile.image.update', 'uses' => 'CMS\ProfileController@profileImageUpdate']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'CMS\ProfileController@password']);

    // Crop
    Route::post('image-cropper/upload', 'ImageCropperController@upload')->name('image_crop');


    Route::get('public_images/index', 'CMS\PublicImageController@index')->name('public_images.index');
    Route::post('public_images/index', 'CMS\PublicImageController@store')->name('public_images.store');
    Route::delete('public_images/index', 'CMS\PublicImageController@destroy')->name('public_images.destroy');
    Route::get('public-images-load-data', 'CMS\PublicImageController@loadData')->name('public_images.load-data');


    // Custom page
    Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);








});

/*Route::get('/imi-games/page', function () {
    return view('pages/imi-games/page');
})->name('page');*/
