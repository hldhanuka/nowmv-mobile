<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/quran-recitations', 'API\QuranRecitationController@quranRecitationList');

$throttle = config("app.env") !== "production" ? 'throttle:600,1' : 'throttle:100,1';

Route::get('/terms-and-services', 'PageController@termsAndServices');
Route::get('/imi-terms-and-services', 'PageController@imiTermsAndServices');
Route::get('/privacy-policy', 'PageController@privacyPolicy')->name('privacy-policy');
Route::get('/frequently-asked-questions', 'PageController@frequentlyAskedQuestions');

Route::get('/ferry/get-seating-layout-detail-by-id/{id}', 'API\FerryController@getSeatingLayoutDetailById');

Route::get('/get-news-view-by-id/{id}', 'API\NewsController@getNewsViewById');

// Social Login (Development support endpoints) // TODO remove later
//Route::get('/social-login/login/{provider}', 'API\SocialAccountController@login')->middleware("web");
//Route::get('/social-login/callback/{provider}', 'API\SocialAccountController@callback')->middleware("web");

//Quran Recitation
Route::get('/test-pdf-iframe', 'API\QuranRecitationController@testPDFIframe');

Route::middleware(['commonHeaders', $throttle])->group(function () {

    // General (check-version)
    Route::post('/check-version', 'API\SettingsController@checkVersion');

    // Auth
    Route::post('/login', 'API\AuthController@login');

    // Social Login
    Route::post('/social-login/mobile-login', 'API\SocialAccountController@mobileLogin');

    Route::middleware('jwt')->group(function () {
        // Auth Others
        Route::post('/verify-login', 'API\AuthController@verifyLogin');
        Route::post('/register', 'API\AuthController@register');
        Route::post('/submit-user-current-location', 'API\AuthController@submitUserCurrentLocation');
        Route::post('/submit-user-live-location', 'API\AuthController@submitUserLiveLocation');
        Route::get('/get-user-profile', 'API\UserController@getUserProfile');
        Route::post('/update-user-profile', 'API\UserController@updateUserProfile');
        Route::post('/update-user-image', 'API\UserController@updateUserImage');
        Route::post('/logout', 'API\AuthController@logout');

        // Social Login
        Route::post('/social-login/mobile-register', 'API\SocialAccountController@mobileRegister');
        Route::post('/social-login/otp-resend', 'API\SocialAccountController@otpResend');
        Route::post('/social-login/mobile-verify', 'API\SocialAccountController@mobileVerify');

        // General
        Route::get('/get-home-page-data', 'API\HomeController@getHomePageData');
        Route::get('/get-all-contacts', 'API\UserController@getAllContacts');
        Route::get('/get-user-notification-settings', 'API\UserNotificationSettingController@getUserNotificationSettings');
        Route::post('submit-notification-settings', 'API\UserNotificationSettingController@submitNotificationSettings');
        Route::post('/submit-home-screen-order', 'API\HomeController@submitHomeScreenOrder');
        Route::get('/get-prayer-tracker-settings', 'API\HomeController@getPrayerTrackerSettings');
        Route::post('/submit-prayer-tracker-settings', 'API\HomeController@submitPrayerTrackerSettings');
        Route::get('/get-home-screen-order', 'API\HomeController@getHomeScreenOrder');
        Route::get('/support-teams', 'API\SupportTeamController@supportTeams');

        // Ads
        Route::get('/ad-by-type/{type}', 'API\AdController@adByType');

        // Badges
        Route::get('/badges', 'API\BadgeController@badges');
        Route::get('/badges/{id}', 'API\BadgeController@getBadgeById');
        Route::post('/save-badges', 'API\BadgeController@saveBadges');
        Route::get('/get-user-badges', 'API\UserBadgeController@userBadges');

        // Donations
        Route::get('/get-donations-list', 'API\DonationController@getDonationsList');
        Route::get('/donations/{id}', 'API\DonationController@getDonationById');
        Route::post('/submit-donation-payment', 'API\UserDonationController@submitDonationPayment');
        Route::post('/verify-donation-payment', 'API\UserDonationController@verifyDonationPayment');
        Route::get('/get-donation-history', 'API\UserDonationController@getDonationHistory');

        //Ngos
        Route::get('/get-ngos-list', 'API\NgoController@getNgosList');
        Route::get('/ngos/{id}', 'API\NgoController@getNgoById');
        Route::post('/submit-ngo-payment', 'API\UserNgoController@submitNgoPayment');
        Route::post('/verify-ngo-payment', 'API\UserNgoController@verifyNgoPayment');

        // News
        Route::get('/get-all-news-categories', 'API\NewsCategoryController@getAllNewsCategories');
        Route::get('/get-news-by-category/{category}', 'API\NewsController@getNewsByCategoryId');
        Route::get('/get-news-by-agency/{agency_id}', 'API\NewsController@getNewsByAgencyId');
        Route::get('/get-news-by-tag/{tag_id}', 'API\NewsController@getNewsByTagId');
        Route::get('/get-all-news', 'API\NewsController@getAllNews');
        Route::get('/get-news/{id}', 'API\NewsController@getNewsById');

        // Interests
        Route::get('/interests', 'API\InterestController@interests');
        Route::get('/interests/{id}', 'API\InterestController@getInterestById');
        Route::post('/save-user-interests', 'API\UserInterestController@saveUserInterests');
        Route::get('/get-user-interests', 'API\UserInterestController@getUserInterests');

        // Food
        Route::get('/get-food-home-page-data', 'API\RestaurantController@getFoodHomePageData');
        Route::get('/get-food-news', 'API\NewsController@getFoodNews');
        Route::get('/get-recipe-by-id/{id}', 'API\RecipeController@getRecipeById');
        Route::get('/get-recipe-list', 'API\RecipeController@getRecipeList');
        Route::get('/get-suggested-restaurants', 'API\RestaurantController@getSuggestedRestaurants');
        Route::post('/update-recipe-favourite', 'API\RecipeController@updateRecipeFavourite');
        Route::post('/submit-recipe-review', 'API\RecipeReviewController@submitRecipeReview');
        Route::post('/update-recipe-review', 'API\RecipeReviewController@updateRecipeReview');
        Route::delete('/remove-recipe-review', 'API\RecipeReviewController@removeRecipeReview');
        Route::post('/submit-recipe-rating', 'API\RecipeRatingController@submitRecipeRating');
        Route::get('/get-user-rating-by-meal-id/{id}', 'API\MealRatingController@getUserRatingByMealId');
        Route::get('/get-restaurants-home-page-data', 'API\RestaurantController@getRestaurantHomePageData');
        Route::get('/meals/{id}', 'API\MealController@getMealById');
        Route::get('/restaurants', 'API\RestaurantController@getRestaurants');
        Route::get('/restaurants/{id}', 'API\RestaurantController@getRestaurantById');
        Route::post('/submit-restaurant-review', 'API\RestaurantReviewController@submitRestaurantReview');
        Route::post('/update-restaurant-review', 'API\RestaurantReviewController@updateRestaurantReview');
        Route::delete('/remove-restaurant-review', 'API\RestaurantReviewController@removeRestaurantReview');
        Route::post('/submit-restaurant-rating', 'API\RestaurantRatingController@submitRestaurantRating');
        Route::get('/get-user-rating-by-restaurant-id/{id}', 'API\RestaurantRatingController@getUserRatingByRestaurantId');
        Route::get('/get-meal-categories-by-restaurant-id/{id}', 'API\MealCategoryController@getMealCategoryByRestaurantId');
        Route::post('/submit-meal-review', 'API\MealReviewController@submitMealReview');
        Route::post('/update-meal-review', 'API\MealReviewController@updateMealReview');
        Route::delete('/remove-meal-review', 'API\MealReviewController@removeMealReview');
        Route::post('/submit-meal-rating', 'API\MealRatingController@submitMealRating');
        Route::get('/get-user-rating-by-meal-id/{id}', 'API\MealRatingController@getUserRatingByMealId');
        Route::post('/find-nearby-restaurants', 'API\RestaurantController@findNearbyRestaurants');
        Route::get('/get-meal-of-the-day', 'API\MealController@getMealOfTheDay');
        Route::post('/restaurants-search', 'API\RestaurantController@restaurantsSearch');

        // Events
        Route::get('/get-all-events', 'API\EventController@getAllEvents');
        Route::get('/get-event-calender-types', 'API\EventController@getEventCalenderTypes');
        Route::post('/get-event-calender', 'API\EventController@getEventCalender');
        Route::post('/find-nearby-events', 'API\EventController@findNearbyEvents');
        Route::post('/search-events', 'API\EventController@searchEvents');
        Route::get('/events/{id}', 'API\EventController@getEventById');
        Route::get('/get-all-event-tags', 'API\EventController@getAllEventTags');
        Route::post('/create-private-event', 'API\EventController@createPrivateEvent');
        Route::post('/update-private-event/{id}', 'API\EventController@updatePrivateEvent');
        Route::delete('/remove-private-event', 'API\EventController@removePrivateEvent');
        Route::get('/get-contacts-by-event-id/{id}', 'API\EventInviteeController@getContactsByEventId');
        Route::get('/get-chat-members-by-event-id/{id}', 'API\EventInviteeController@getChatMembersByEventId');
        Route::post('/update-event-favourite', 'API\EventController@updateFavouriteEvent');
        Route::post('/get-friends', 'API\UserController@getFriends');
        Route::post('/invite-friends-to-event', 'API\EventInviteeController@inviteFriendsToEvent');
        Route::post('/invite-all-to-event', 'API\EventInviteeController@inviteAllToEvent');
        Route::get('/my-events', 'API\EventController@myEvents');
        Route::post('/search-my-events', 'API\EventController@searchMyEvents');
        Route::get('/get-event-reminder/{id}', 'API\EventController@getEventReminder');

        // Event Messages
        Route::get('/event-messages/get-messages-by-event-id/{id}', 'API\EventMessageController@getMessagesByEventId');
        Route::post('/event-messages/send-message', 'API\EventMessageController@sendMessage');
        Route::get('/event-messages/seen-all-messages-by-event-id/{id}', 'API\EventMessageController@seenAllMessagesByEventId');
        Route::delete('/event-messages/remove-messages', 'API\EventMessageController@removeMessages');

        // Offer
        Route::get('/offers', 'API\OfferController@getAllOffers');
        Route::get('/offer-categories', 'API\OfferCategoryController@getOfferCategories');
        Route::get('/get-offer-by-category-id/{category}', 'API\OfferController@getOfferByCategoryId');
        Route::post('/submit-offer-payment', 'API\UserOfferController@submitOfferPayment');
        Route::post('/verify-offer-payment', 'API\UserOfferController@verifyOfferPayment');
        Route::get('/get-offer-history', 'API\UserOfferController@getOfferHistory');

        // Search
        Route::post('/global-search', 'API\SearchController@globalSearch');

        // Weather
        Route::get('/get-all-weather-locations', 'API\WeatherController@getAllWeatherLocations');
        Route::get('/get-weather-location-detail-by-id/{id}', 'API\WeatherController@getWeatherLocationDetailById');
        Route::post('/add-weather-location', 'API\UserWeatherLocationController@addWeatherLocation');
        Route::delete('/remove-weather-location', 'API\UserWeatherLocationController@removeWeatherLocation');
        Route::get('/get-user-weather-locations', 'API\UserWeatherLocationController@getUserWeatherLocations');

        // Travel
        Route::get('/get-flight-arrivals', 'API\FlightController@getFlightArrivals');
        Route::get('/get-flight-departures', 'API\FlightController@getFlightDepartures');
        Route::get('/ferry/get-island-list', 'API\FerryController@getIslandList');
        Route::post('/ferry/get-available-ferries', 'API\FerryController@getAvailablePrivateFerries');
        Route::get('/ferry/get-ticket-plan-detail-by-id/{id}', 'API\FerryController@getTicketPlanDetailById');
        // TODO please keep old services
        //Route::post('/ferry/get-available-public-ferries', 'API\FerryController@getAvailablePublicFerries');
        //Route::get('/bus/get-all-locations', 'API\BusController@getAllLocations');
        //Route::post('/bus/get-available-buses', 'API\BusController@getAvailableBuses');

        // MTCC && MPL
        Route::get('/ferry/get-public-island-list', 'API\MtccFerryController@getPublicIslandList');
        Route::post('/ferry/get-available-public-ferries', 'API\MtccFerryController@getAvailablePublicFerries');
        Route::get('/bus/get-all-locations', 'API\MtccBusController@getAllLocations');
        Route::post('/bus/get-available-buses', 'API\MtccBusController@getAvailableBuses');

        // Prayer
        Route::get('/get-all-prayer-locations', 'API\PrayerLocationController@getAllPrayerLocations');
        Route::post('/submit-prayer-location', 'API\PrayerLocationController@submitPrayerLocation');
        Route::get('/get-user-prayer-location', 'API\PrayerLocationController@getUserPrayerLocation');
        Route::post('/get-prayer-mate-times-by-date', 'API\PrayerController@getPrayerMateTimesByDate');
        Route::post('/get-prayer-mate-times-by-range', 'API\PrayerController@getPrayerMateTimesByRange');
        Route::get('/get-prayer-mate-tips', 'API\PrayerController@getPrayerMateTips');
        Route::post('/get-prayer-fasting-calendar', 'API\PrayerController@getPrayerFastingCalendar');
        Route::post('/get-prayer-status-calendar', 'API\PrayerController@getPrayerStatusCalendar');
        Route::post('/submit-prayer-range', 'API\PrayerController@submitPrayerRange');
        Route::post('/submit-prayer-status', 'API\PrayerController@submitPrayerStatus');
        Route::post('/submit-prayer-fasting', 'API\PrayerController@submitPrayerFasting');

        // Video
        Route::get('/get-videos-list', 'API\VideoController@getVideosList');

        // E Directory
        Route::get('/get-e-directory', 'API\EDirectoryController@getEDirectory'); // TODO

        // Notification
        Route::get('/notifications/get-notifications', 'API\PushNotificationController@getNotifications');
        Route::get('/notifications/get-new-notification-count', 'API\PushNotificationController@getNewNotificationCount');
        Route::delete('/notifications/remove-notifications', 'API\PushNotificationController@removeNotification');
        Route::post('/notifications/invitation-response', 'API\PushNotificationController@invitationResponse');

        // Get Sync mobile DB
        Route::get('/sync/notifications/get-event-reminder-notifications', 'API\SyncController@getEventReminderNotifications');
        Route::get('/sync/notifications/get-bus-reminder-notifications', 'API\SyncController@getBusReminderNotifications');
        Route::get('/sync/notifications/get-flight-reminder-notifications', 'API\SyncController@getFlightReminderNotifications');
        Route::post('/sync/notifications/get-prayer-reminder-notifications-by-range', 'API\SyncController@getPrayerReminderNotificationsByRange');

        // Reminders
        Route::get('/reminders/get-prayer-reminder', 'API\ReminderNotificationController@getPrayerReminder');
        Route::post('/reminders/set-prayer-reminder', 'API\ReminderNotificationController@setPrayerReminder');
        Route::post('/reminders/set-flight-reminder', 'API\ReminderNotificationController@setFlightReminder');
        Route::post('/reminders/set-bus-reminder', 'API\ReminderNotificationController@setBusReminder');
        Route::post('/reminders/set-event-reminder', 'API\ReminderNotificationController@setEventReminder');

        // Check Share Response
        Route::get('/check-share-response/{type}/{id}', 'ShareController@checkShareResponse');

        // Zakat
        Route::get('/zakat/get-atoll-list', 'API\ZakatController@getAtollList');
        Route::get('/zakat/get-island-list-by-atoll-id/{id}', 'API\ZakatController@getIslandListByAtollID');
        Route::get('/zakat/get-commodity-list', 'API\ZakatController@getCommodityList');
        Route::post('/zakat/submit-zakat-payment', 'API\ZakatController@submitZakatPayment');
        Route::post('/zakat/verify-zakat-payment', 'API\ZakatController@verifyZakatPayment');

        // Send Custom Push Notification
        Route::post('/testing/send-custom-push-notification', 'API\PushNotificationController@sendCustomPushNotification');

    });
});

// External Services
Route::middleware('webServiceHeaders')->group(function () {

    Route::post('/external/login', 'API\External\AuthController@login');

    Route::middleware('external_jwt')->group(function () {

        // News
        Route::get('/external/get-agent-news', 'API\External\NewsController@getAgentNews');
        Route::get('/external/get-news-tags', 'API\External\NewsController@getNewsTags');
        Route::get('/external/get-news-categories', 'API\External\NewsController@getNewsCategories');
        Route::get('/external/get-agent-news-by-id/{id}', 'API\External\NewsController@getAgentNewsById');
        Route::post('/external/create-agent-news', 'API\External\NewsController@createAgentNews');
        Route::post('/external/update-agent-news/{id}', 'API\External\NewsController@updateAgentNews');
        Route::delete('/external/remove-agent-news', 'API\External\NewsController@removeAgentNews');

    });
});
