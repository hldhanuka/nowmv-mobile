<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationType extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function children(){
        return $this->hasMany( 'App\NotificationType', 'parent_id', 'id' );
    }

    public function activeChildren(){
        return $this->hasMany( 'App\NotificationType', 'parent_id', 'id' )->active();
    }

    public function parent(){
        return $this->hasOne( 'App\NotificationType', 'id', 'parent_id' );
    }

}
