<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsAgencyUser extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function agency()
    {
        return $this->belongsTo('App\NewsAgency', 'news_agency_id', 'id');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }
}
