<?php

namespace App;

use App\Helpers\APIHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushNotification extends Model
{
    use SoftDeletes;

    protected $fillable = ['from', 'to', 'seen', 'message', 'type', 'created_at', 'updated_at'];

    protected $appends = ['sender_details', 'content_type', 'data_object'];

    public function scopeOffset($query, $offset)
    {
        if ($offset != null) {
            return $query->skip($offset);
        } else {
            return $query;
        }
    }

    public function from()
    {
        return $this->belongsTo('App\User', 'from', 'id');
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to', 'id');
    }

    public function getSenderDetailsAttribute()
    {
        $sender_details = $this->from()->take(1)->select('image', 'first_name', 'last_name', 'status')->get()->toArray();
        if (isset($sender_details[0])) {
            return $sender_details[0];
        } else {
            return APIHelper::getSenderDetailArray($this->attributes['type']);
        }
    }

    public function getContentTypeAttribute()
    {
        if ($this->attributes['type'] == "event_invitation") {
            return "event_invitation";
        } else {
            return "push_notification";
        }
    }

    public function getDataObjectAttribute()
    {
        if ($this->attributes['type'] == "event_invitation") {
            $event_invitee = EventInvitee::withTrashed()->find($this->attributes['relation_id']);
            return $event_invitee;
        } else {
            return null;
        }
    }

}
