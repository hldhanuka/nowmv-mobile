<?php

namespace App\Console;

use App\Console\Commands\SendCalendarNotifications;
use App\Console\Commands\SendReminderNotifications;
use App\Console\Commands\SetPrayerReminder;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SetPrayerReminder::class,
        SendReminderNotifications::class,
        SendCalendarNotifications::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('set:prayer-reminder')
            ->daily();

        $schedule->command('send:reminder-notifications')
            ->everyMinute();

        $schedule->command('send:calender-notifications')
            ->dailyAt('08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
