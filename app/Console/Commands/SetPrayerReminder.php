<?php

namespace App\Console\Commands;

use App\Helpers\APIHelper;
use App\ReminderNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SetPrayerReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:prayer-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set prayer reminders for the day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $today = Carbon::now();
            $english_day = strtolower($today->englishDayOfWeek);

            $all_users = User::with('userSetting')->active()->has('userSetting')->get();
            $prayer_types = ['fajr', 'dhuhr', 'asr', 'maghrib', 'isha'];
            $all_reminder_notifications = [];
            $selected_user_ids = [];

            foreach ($all_users as $one_user) {
                $user_id = $one_user->id;
                foreach ($prayer_types as $prayer_type) {
                    if (in_array($english_day, json_decode($one_user->userSetting->prayer_reminder, true)[$prayer_type])) {

                        // check location set
                        if ($one_user->prayer_location_id != null) {
                            // check reminder is active
                            if (APIHelper::getUserOrUsersByActiveSetting("prayer_reminder", $user_id)) {
                                // Get prayer mate time
                                $prayer_mate_time = APIHelper::getPrayerMateTimeByDateAndUser($today, $one_user, true);
                                if ($prayer_mate_time != null) {
                                    $temp_data = $this->getPrayerTempDataArray($prayer_mate_time, $user_id, $prayer_type, $today);
                                    array_push($all_reminder_notifications, $temp_data);
                                    $selected_user_ids[] = $user_id;
                                }
                            }
                        }
                    }
                }
            }

            // Delete previous all prayer data
            ReminderNotification::whereIn("user_id", array_unique($selected_user_ids))->where("type", "prayer")->delete();

            // Insert today prayer reminders
            ReminderNotification::insert($all_reminder_notifications);
        } catch (\Exception $e) {
            // remove error
            Log::critical("CRON Issue - SetPrayerReminder");
            report($e);
        }
    }

    public function getPrayerTempDataArray($prayer_mate_time, $user_id, $type, $date)
    {
        $now = Carbon::now();
        $date_time = Carbon::parse($date->toDateString() . " " . $prayer_mate_time[$type]["time"]);
        $prayer_reminder_before = config('dhiraagu.prayer_reminder_before');
        $schedule_time = $date_time->copy()->subMinutes($prayer_reminder_before)->toDateTimeString();
        $content = Str::ucfirst($type) . " prayer at " . APIHelper::setDhiraaguDateFormat($date_time, "time");
        return [
            "type" => "prayer",
            "user_id" => $user_id,
            "content" => $content,
            "date_time" => $schedule_time,
            "relation_id" => $prayer_mate_time[$type]["id"],
            "status" => 1,
            "created_at" => $now,
            "updated_at" => $now,
        ];
    }
}
