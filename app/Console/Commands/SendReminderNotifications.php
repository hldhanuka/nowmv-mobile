<?php

namespace App\Console\Commands;

use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\ReminderNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendReminderNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:reminder-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder Notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $now = Carbon::now()->format('Y-m-d H:i');
            $temp_all_active_reminders = ReminderNotification::active()->where(DB::raw("DATE_FORMAT(date_time, '%Y-%m-%d %H:%i')"), $now);
            $all_active_reminders = $temp_all_active_reminders->get();
            $temp_all_active_reminders->update(["status" => 0]);
            foreach ($all_active_reminders as $active_reminder) {
                $id = $active_reminder->relation_id;
                $data = ["type" => APIHelper::getAppRedirectTypeByReminderNotificationType($active_reminder->type), "id" => $id != null ? (string)$id : $id, "reminder_notification" => $active_reminder];
                OneSignalPush::sendReminderNotification($active_reminder->content, $active_reminder->type, $data, 0, $active_reminder->user_id, null);
            }
        } catch (\Exception $e) {
            // remove error
        }

    }
}
