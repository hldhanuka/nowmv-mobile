<?php

namespace App\Console\Commands;

use App\Helpers\OneSignalPush;
use App\ScheduleType;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendCalendarNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:calender-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Calender Notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $today = Carbon::now()->toDateString();
            $all_calender_schedules = ScheduleType::where('type', 'calendar')->active()->get()->sortBy('priority');
            $all_calender_reminders = [];

            foreach ($all_calender_schedules as $calender_schedule) {
                $schedule_dates = json_decode($calender_schedule->date, true);
                $schedule_dates = collect($schedule_dates)->where('date', $today)->where('notification', true);
                if (count($schedule_dates) != 0) {
                    $all_calender_reminders = array_merge($all_calender_reminders, $schedule_dates->toArray());
                }
            }

            if (count($all_calender_reminders) > 0) {
                $message = count($all_calender_reminders) == 1 ? 'You have 1 upcoming event; ' : 'You have ' . count($all_calender_reminders) . ' upcoming events; ';

                foreach ($all_calender_reminders as $calender_reminder) {
                    $message .= (next($all_calender_reminders)) ? $calender_reminder['title'] . ', ' : $calender_reminder['title'] . '.';
                }

                $data = ["type" => "event_home","id" => null];
                OneSignalPush::sendCalenderNotification($message, 'calender', $data, 0);
            }
        } catch (\Exception $e) {
            //remove error
        }
    }
}

