<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavouriteRecipe extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','recipe_id'];
}
