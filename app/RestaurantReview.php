<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantReview extends Model
{
    use SoftDeletes;

    protected $fillable = ['restaurant_id', 'comment', 'status'];

    protected $appends = ['created_user_rating'];

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->select(['id', 'first_name', 'last_name', 'image', 'status' , 'email']);
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'id');
    }

    public function getCreatedUserRatingAttribute()
    {
        $created_user_rating = RestaurantRating::where("created_by", $this->attributes["created_by"])->where("restaurant_id", $this->attributes['restaurant_id'])->first();

        if ($created_user_rating != null) {
            $rate = $created_user_rating->rate;
        } else {
            $rate = 0;
        }
        return $rate;
    }
}
