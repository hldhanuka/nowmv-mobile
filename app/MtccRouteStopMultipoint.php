<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccRouteStopMultipoint extends Model
{
    use SoftDeletes;

    public function route(){
        return $this->belongsTo('App\MtccRoute', 'route_id', 'id');
    }

    public function scopeBus($query){
        return $query->whereHas('route', function ($query){
            $query->bus();
        });
    }
}
