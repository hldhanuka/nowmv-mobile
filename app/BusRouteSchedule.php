<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusRouteSchedule extends Model
{
    use SoftDeletes;

    protected $fillable = ['schedule_type_id','bus_route_id','starting_time','status','created_at','updated_at'];

    protected $appends = ['status_text'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function busRoute()
    {
        return $this->belongsTo('App\BusRoute', 'bus_route_id', 'id');
    }

    public function scheduleType()
    {
        return $this->belongsTo('App\ScheduleType', 'schedule_type_id', 'id');
    }
}
