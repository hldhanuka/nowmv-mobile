<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccCalendarAnnualHoliday extends Model
{
    use SoftDeletes;
}
