<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSetting extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','screen_order','enable_prayer_tracker','prayer_reminder'];
}
