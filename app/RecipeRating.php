<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecipeRating extends Model
{
    use SoftDeletes;

    protected $fillable = ['restaurant_id', 'recipe_id', 'rate', 'created_by'];
}
