<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccServicePoint extends Model
{
    use SoftDeletes;

    public function getIdAttribute($value)
    {
        return (string)$value;
    }

    public function getNameAttribute($value)
    {
        return $value . ' (MTCC)';
    }

    public function mapLocation()
    {
        return $this->belongsTo('App\MtccMapLocation', 'map_location_id', 'id');
    }
}
