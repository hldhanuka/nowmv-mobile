<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventInvitee extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text'];

    protected $fillable = ['user_id', 'event_id', 'invitee_id', 'status'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function invitee()
    {
        return $this->belongsTo('App\User', 'invitee_id', 'id');
    }


}
