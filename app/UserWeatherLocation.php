<?php

namespace App;

use App\Helpers\APIHelper;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWeatherLocation extends Model
{
    use SoftDeletes;

    protected $fillable =['user_id', 'location_id'];
}
