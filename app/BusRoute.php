<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusRoute extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function fromLocation()
    {
        return $this->belongsTo('App\BusLocation', 'from', 'id');
    }

    public function toLocation()
    {
        return $this->belongsTo('App\BusLocation', 'to', 'id');
    }

    public function busType()
    {
        return $this->belongsTo('App\BusType', 'bus_type_id', 'id');
    }

    public function busHaltSchedule()
    {
        return $this->hasMany('App\BusHaltSchedule', 'bus_route_id', 'id');
    }
}
