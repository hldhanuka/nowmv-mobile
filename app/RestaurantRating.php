<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantRating extends Model
{
    use SoftDeletes;

    protected $fillable = ['restaurant_id','created_by','rate'];
}
