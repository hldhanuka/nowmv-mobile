<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class ValidMobileArray implements Rule
{
    private $mobile_numbers;
    private $err_numbers = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($mobile_numbers)
    {
        $this->mobile_numbers = $mobile_numbers;
    }

    public function __toString()
    {
        return 'ValidMobileArray';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = true;
        foreach ($this->mobile_numbers as $mobile_number) {
            if (! User::role("customer")->where('mobile', $mobile_number)->active()->exists()) {
                array_push( $this->err_numbers, $mobile_number);
                $valid = false;
            }
        }

        if($valid){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return implode (", ", $this->err_numbers) . ' are not a registered mobile numbers.';
    }
}
