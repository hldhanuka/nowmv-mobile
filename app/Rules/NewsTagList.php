<?php

namespace App\Rules;

use App\NewsTag;
use Illuminate\Contracts\Validation\Rule;

class NewsTagList implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tag_ids = NewsTag::pluck('id')->toArray();
        $new_array = array_map('trim', explode(',', $value));


        if(count($new_array)){
            foreach ($new_array as $id){
                if(!in_array($id, $tag_ids)){
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid news tags.';
    }
}
