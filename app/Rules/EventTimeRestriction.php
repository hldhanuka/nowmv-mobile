<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class EventTimeRestriction implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $request_date = request()->date;
        $request_time_stamp = Carbon::parse($request_date." ".$value);
        $current_date_time = Carbon::now();
        if(($request_time_stamp)->greaterThanOrEqualTo($current_date_time)){
            return true;
        } else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid time input. Time must be the current time or afterwards ';
    }
}
