<?php

namespace App\Rules;

use App\Donation;
use App\Event;
use App\News;
use App\Ngo;
use App\Restaurant;
use Illuminate\Contracts\Validation\Rule;

class AnotherValueInArray implements Rule
{
    private $redirect_type;
    private $err_message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($redirect_type)
    {
        $this->redirect_type = $redirect_type;
    }

    public function __toString()
    {
        return 'AnotherValueInArray';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $req_array = ['news', 'event', 'restaurant', 'donation', 'ngo'];

        if (in_array($this->redirect_type, $req_array)) {
            if ($value!=null){
                if ( $this->idExists($this->redirect_type, $value)){
                    return true;
                } else {
                    $this->err_message = 'Given :attribute is not available. ';
                    return false;
                }
            } else {
                $this->err_message = 'The :attribute field is required.';
                return false;
            }
        } else {
            return true;
        }
    }

    public function idExists($type, $id)
    {
        if($type=='news'){
            return (News::where('id', '=', $id)->exists());
        } elseif ($type=='event') {
            return (Event::where('id', '=', $id)->exists());
        } elseif ($type=='restaurant') {
            return (Restaurant::where('id', '=', $id)->exists());
        } elseif ($type=='donation') {
            return (Donation::where('id', '=', $id)->exists());
        } elseif ($type=='ngo') {
            return (Ngo::where('id', '=', $id)->exists());
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->err_message;
    }
}
