<?php

namespace App\Rules;

use App\Interest;
use App\ScheduleType;
use Illuminate\Contracts\Validation\Rule;

class ArrayInput implements Rule
{
    private $type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->type == "interests") {
            $ids = Interest::pluck('id')->toArray();
        } else if ($this->type == "calendar_type") {
            $ids = ScheduleType::calendarSchedule()->active()->pluck('id')->toArray();
        } else {
            return false;
        }

        if (is_array($value)) {
            foreach ($value as $id) {
                if (!in_array($id, $ids)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid ids.';
    }
}
