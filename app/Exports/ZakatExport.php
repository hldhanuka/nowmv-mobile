<?php

namespace App\Exports;

use App\User;
use App\ZakatPayment;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ZakatExport implements FromQuery, WithMapping, WithHeadings
{
    private $from;
    private $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function query()
    {
        return ZakatPayment::query()->with(['island', 'atoll'])->whereBetween('updated_at', [$this->from, $this->to]);
    }

    public function headings(): array
    {
        return [
            'ID',
            'Mobile No.',
            'Name',
            'ID Card No',
            'Address',
            'Atoll',
            'Island',
            'Number of Persons',
            'Commodity',
            'Sadagath Amount (MVR)',
            'Total Price (MVR)',
            'Trxn Date',
            'Trxn Time',
            'Order No.',
            'Response',
            'TXN ID',
        ];
    }

    public function map($payment): array
    {
        return [
            $payment->id,
            $payment->mobile_number,
            $payment->full_name,
            $payment->id_number,
            $payment->address,
            $payment->atoll->name,
            $payment->island->name,
            $payment->no_of_person,
            $payment->commodity->type,
            $payment->sadagath_amount,
            $payment->total_amount,
            Carbon::parse($payment->updated_at)->toDateString(),
            Carbon::parse($payment->updated_at)->toTimeString(),
            $payment->payment_invoice_number,
            $payment->response_text,
            $payment->transaction_id,
        ];
    }
}
