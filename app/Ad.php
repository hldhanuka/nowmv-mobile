<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $appends = ['status_text'];

    //Retrieve status
    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function scopeDonation($query)
    {
        return $query->where('type', 'donation');
    }

    public function scopeFood($query)
    {
        return $query->where('type', 'food');
    }

    public function scopeHome($query)
    {
        return $query->where('type', 'home');
    }

    public function scopeNews($query)
    {
        return $query->where('type', 'news');
    }

    public function scopeOffer($query)
    {
        return $query->where('type', 'offer');
    }

    public function scopeTravel($query)
    {
        return $query->where('type', 'travel');
    }

    public function scopeWeather($query)
    {
        return $query->where('type', 'weather');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }
}
