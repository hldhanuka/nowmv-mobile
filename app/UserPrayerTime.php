<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPrayerTime extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'prayer_time_id', 'prayer_location_id', 'status', 'date_time'];
}
