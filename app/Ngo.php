<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ngo extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'supporters', 'content_type'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function ngoMerchant()
    {
        return $this->belongsTo('App\NgoMerchant', 'ngo_merchant_id', 'id');
    }

    public function ngoImages()
    {
        return $this->hasMany('App\NgoImage', 'ngo_id', 'id')->orderBy('order');
    }

    public function donations()
    {
        return $this->hasMany('App\Donation', 'ngo_id', 'id');
    }

    public function getSupportersAttribute()
    {
        $donation_ids = $this->donations()->pluck('id')->toArray();
        return UserNgoDonation::active()->whereIn('ngo_id', $donation_ids)->groupBy('user_id')->get()->count();
    }

    public function ngoBadges()
    {
        return $this->hasMany('App\NgoBadge', 'ngo_id', 'id');
    }

    public function getContentTypeAttribute()
    {
        return "ngo";
    }

}
