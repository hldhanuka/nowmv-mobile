<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'supporters', 'content_type', 'logo'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function ngo()
    {
        return $this->belongsTo('App\Ngo', 'ngo_id', 'id');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function getSupportersAttribute()
    {
        return UserDonation::active()->where('donation_id',$this->attributes['id'])->groupBy('user_id')->get()->count();
    }

    public function getLogoAttribute()
    {
        if($this->ngo!=null){
            return $this->ngo->logo;
        } else {
            return asset("assets/img/100x100.png");
        }
    }

    public function donationBadges()
    {
        return $this->hasMany('App\DonationBadge', 'donation_id', 'id');
    }

    public function getContentTypeAttribute()
    {
        return "donation";
    }
}
