<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccRouteStopDay extends Model
{
    use SoftDeletes;
}
