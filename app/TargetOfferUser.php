<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TargetOfferUser extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Offer', 'offer_id', 'id');
    }
}
