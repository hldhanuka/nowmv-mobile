<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'ingredients_array', 'directions_array', 'average_rating', 'is_user_favourite', 'video_content_type'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function getIngredientsArrayAttribute()
    {
        return json_decode($this->attributes['ingredients'], true);
    }

    public function getDirectionsArrayAttribute()
    {
        return json_decode($this->attributes['directions'], true);
    }

    public function reviews()
    {
        return $this->hasMany('App\RecipeReview', 'recipe_id', 'id');
    }

    public function ratings()
    {
        return $this->hasMany('App\RecipeRating');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'id');
    }

    public function userFavouriteRecipe()
    {
        return $this->hasMany('App\UserFavouriteRecipe', 'recipe_id', 'id');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function getAverageRatingAttribute()
    {
        return round($this->ratings()->avg('rate'), 1);
    }

    public function getIsUserFavouriteAttribute()
    {
        if(APIHelper::loggedUser()!==null){
            return $this->userFavouriteRecipe()->where("user_id", APIHelper::loggedUser()->id)->count();
        } else if(auth()->user()!=null){
            return $this->userFavouriteRecipe()->where("user_id", auth()->user()->id)->count();
        } else {
            return 0;
        }
    }

    public function getVideoContentTypeAttribute()
    {
        if($this->attributes['video']!=null){
            if(strpos($this->attributes['video'], 'www.youtube.com') != false){
                $type = "youtube";
            } else {
                $type = "other";
            }
        } else {
            $type = null;
        }
        return $type;
    }

}
