<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZakatPayment extends Model
{
    use SoftDeletes;

    protected $appends = ['response_text'];

    public function island()
    {
        return $this->belongsTo('App\ZakatIsland', 'island_id', 'id');
    }

    public function atoll()
    {
        return $this->belongsTo('App\ZakatAtoll', 'atoll_id', 'id');
    }

    public function commodity()
    {
        return $this->belongsTo('App\ZakatCommodity', 'commodity_id', 'id');
    }

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getResponseTextAttribute()
    {
        return CMSHelper::getResponseText($this->attributes['status']);
    }

}
