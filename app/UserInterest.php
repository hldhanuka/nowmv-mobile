<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInterest extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function interest()
    {
        return $this->belongsTo('App\Interest', 'interest_id', 'id');
    }
}
