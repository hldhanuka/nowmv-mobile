<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NgoImage extends Model
{
    use SoftDeletes;

    public function ngo()
    {
        return $this->belongsTo('App\Ngo', 'ngo_id', 'id');
    }
}
