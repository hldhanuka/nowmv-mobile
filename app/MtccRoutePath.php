<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccRoutePath extends Model
{
    use SoftDeletes;
}
