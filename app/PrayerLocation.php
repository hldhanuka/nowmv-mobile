<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrayerLocation extends Model
{
    use SoftDeletes;

    public function getIslandAttribute($value)
    {
        return $this->attributes['atoll'] . " " .$value;
    }
}
