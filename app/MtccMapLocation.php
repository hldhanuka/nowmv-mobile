<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccMapLocation extends Model
{
    use SoftDeletes;

    public function island()
    {
        return $this->belongsTo('App\MtccIsland', 'island_id', 'id');
    }

}
