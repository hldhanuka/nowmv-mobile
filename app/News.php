<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'tag_data', 'news_agent_logo', 'content_type', 'video_content_type'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function scopeOffset($query, $offset)
    {
        if ($offset != null) {
            return $query->skip($offset);
        } else {
            return $query;
        }
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function newsAgency()
    {
        return $this->belongsTo('App\NewsAgency', 'news_agency_id', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function getTagDataAttribute()
    {
        $tags_id_array = json_decode($this->attributes['tags']);
        $tags = NewsTag::select(['id','title','status'])->active()->get();
        $tag_data = [];
        foreach ($tags_id_array as $tag_id){
            $tag_data[] = $tags->where('id',$tag_id)->first();
        }
        return $tag_data;
    }

    public function getNewsAgentLogoAttribute()
    {
        $news_agency = $this->newsAgency()->first();
        if($news_agency!=null){
            return $news_agency->logo;
        }
        return null;
    }

    public function getContentTypeAttribute()
    {
        return "news";
    }

    public function getVideoContentTypeAttribute()
    {
        if($this->attributes['video']!=null){
            if(strpos($this->attributes['video'], 'www.youtube.com') != false){
                $type = "youtube";
            } else {
                $type = "other";
            }
        } else {
            $type = null;
        }
        return $type;
    }
}
