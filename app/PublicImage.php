<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PublicImage extends Model
{
    use SoftDeletes;

    protected $fillable = ['url'];

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }


}
