<?php

namespace App\Libraries\MPL;

use GuzzleHttp\Client;

class MPL
{
    /**
     * @param $url
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public static function apiCall($url)
    {
        $request = [];
        $client = new Client(['http_errors' => false]);
        $response = $client->request("GET", $url, $request);
        return json_decode($response->getBody(), true);
    }

    public static function getAllStops()
    {
        try {
            $response = self::apiCall(config('dhiraagu.mpl_api.get_all_stop'));
            if (isset($response["success"]) && $response["success"] && isset($response["data"])) {
                $result = self::filterStopArray($response["data"]);
            } else {
                $result = [];
            }
            return $result;
        } catch (\Exception $e) {
            return [];
        }
    }


    public static function getSchedule($from, $to)
    {
        try {
            $query_params = "?departure_stop=" . $from . "&arrival_stop=" . $to;
            $response = self::apiCall(config('dhiraagu.mpl_api.get_schedule') . $query_params);
            if (isset($response["success"]) && $response["success"] && isset($response["data"][0])) {
                $data = $response["data"][0];
                $result = [
                    "name" => $data["route"]["name"] ?? "",
                    "code" => $data["route"]["code"] ?? "",
                    "stops" => self::filterStopArray($data["route"]["stops"] ?? []),
                    "schedules" => $data["schedules"] ?? [],
                ];
            } else {
                $result = [];
            }
            return $result;
        } catch (\Exception $e) {
            return [];
        }

    }

    private static function filterStopArray($result)
    {
        try {
            array_walk($result, function (&$item) {
                $item['id'] = $item['_id'];
                $item['name'] = $item['name'] . "  (MPL)";
                unset($item['_id']);
                unset($item['__v']);
                unset($item['code']);
                unset($item['latitude']);
                unset($item['longitude']);
                unset($item['zone']);
            });
            return $result;
        } catch (\Exception $e) {
            return [];
        }
    }

}
