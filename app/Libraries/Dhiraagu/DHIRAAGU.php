<?php

namespace App\Libraries\Dhiraagu;

use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

/**
 * Class IDEABIZ.
 */
class DHIRAAGU extends Controller
{
    /**
     * @param $method
     * @param $url
     * @param $request_body
     * @param $headers
     * @param string $guzzle_body_type
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws Exceptions\InvalidFileContentException
     * @throws Exceptions\InvalidResponseException
     * @throws Exceptions\TokenGenerateException
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public static function apiCall($method, $url, $request_body, $headers = [], $guzzle_body_type = 'json')
    {
        $helper = new ConfigHelper();
        if ($headers == "json") {
            $headers = ['Content-Type' => 'application/json', 'Authorization' => "Bearer " . self::getAccessToken()];
        }
        $request = [
            'headers' => $headers,
            $guzzle_body_type => $request_body,
        ];
        $client = new Client(['http_errors' => false]);
        $response = $client->request($method, $url, $request);
        $helper->logService($url, $method, $request['headers'], $request['json'], $response);
        $status_code = $response->getStatusCode();
        if ($status_code == 401) {
            self::generateAccessToken();
            $request['headers']['Authorization'] = "Bearer " . self::getAccessToken();
            $response = $client->request($method, $url, $request);
            $helper->logService($url, $method, $request['headers'], $request['json'], $response);
        }

        return self::makeResponse($response->getBody(), $response->getStatusCode());
    }

    /**
     * @return mixed
     * @throws Exceptions\InvalidFileContentException
     *
     */
    private static function getAccessToken()
    {
        $auth = new Authentication();

        return $auth->getAccessToken();
    }

    /**
     * @throws Exceptions\InvalidFileContentException
     * @throws Exceptions\InvalidResponseException
     * @throws Exceptions\TokenGenerateException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private static function generateAccessToken()
    {
        $auth = new Authentication();
        return $auth->generateAccessToken();
    }

    /**
     * @param $body
     * @param $status_code
     *
     * @return mixed
     */
    private static function makeResponse($body, $status_code)
    {
        if ($status_code == 200 || $status_code == 201 || $status_code == 202) {
            $response = [
                "success" => true,
                "status_code" => $status_code,
                "body" => json_decode($body, true)
            ];

        } else {
            $response = [
                "success" => false,
                "status_code" => $status_code,
                "body" => json_decode($body, true)
            ];

        }
        return $response;

    }

    /**
     * @param string $message
     * @param array $numbers_array
     *
     * @return mixed
     */
    public static function sendSMS($message, $numbers_array)
    {
        $api_body = [
            "login" => [
                "username" => config('dhiraagu.sms_username'),
                "password" => config('dhiraagu.sms_password')
            ],
            "messages" => [
                [
                    "sender" => config('dhiraagu.static_api_data.sms_sender'),
                    "message" => $message,
                    "number" => $numbers_array,
                ]
            ]
        ];

        return self::apiCall('POST', APIHelper::getDhiraaguURL('sms_notification'), $api_body, "json");

    }
}
