<?php
/**
 * Created by PhpStorm.
 * User: Dasun Dissanayake
 * Date: 2019-11-02
 * Time: 11:49 AM.
 */

namespace App\Libraries\Dhiraagu;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Class ConfigHelper.
 */
class ConfigHelper
{
    /**
     * @var string
     */
    public $environment;

    /**
     * @var string
     */
    public $base_url;

    /**
     * @var string
     */
    public $grant_type;

    /**
     * @var string
     */
    public $scope;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var boolean
     */
    public $log_enable;

    /**
     * @var string
     */
    public $log_path;

    /**
     * @var boolean
     */
    public $log_request_headers;

    /**
     * ConfigHelper constructor.
     */
    public function __construct()
    {
        $this->environment = config('dhiraagu.environment');
        $this->base_url = config('dhiraagu.'.$this->environment.'.base_url');
        $this->grant_type = config('dhiraagu.'.$this->environment.'.grant_type');
        $this->username = config('dhiraagu.username');
        $this->password = config('dhiraagu.password');
        $this->log_enable = config('dhiraagu.log_enable');
        $this->log_path = config('dhiraagu.log_path');
        $this->log_request_headers = config('dhiraagu.log_request_headers');
    }

    /**
     * @param string $part
     *
     * @return string $base_url
     */
    public function getUrl($part)
    {
        $base_url = $this->base_url.$part;

        return $base_url;
    }

    /**
     * @return array $headers
     */
    public function getAccessTokenGenerateRequestHeaders()
    {
        $headers = [
            'Content-Type'  => 'application/x-www-form-urlencoded'
        ];

        return $headers;
    }

    /**
     * @throws Exceptions\InvalidFileContentException
     *
     * @return array
     */
    public function getAccessTokenGenerateRequestBody()
    {
        if ($this->grant_type == 'password') {
            $request_body = [
                'grant_type' => $this->grant_type,
                'username'   => $this->username,
                'password'   => $this->password,
            ];
        } else {
            $auth = new Authentication();
            $request_body = [
                'grant_type'    => $this->grant_type,
                'refresh_token' => $auth->getRefreshToken(),
            ];
        }

        return $request_body;
    }

    /**
     * @param null $url
     * @param null $method
     * @param null $request_headers
     * @param null $request_body
     * @param null $response
     * @param null $file_name
     *
     * @throws \Exception
     */
    public function logService($url = null, $method = null, $request_headers = null, $request_body = null, $response = null, $file_name = null)
    {
        if ($this->log_enable) {
            $content = [
                'Request URL'      => $url,
                'Request Method'   => $method,
                'Request Headers'  => $request_headers,
                'Request Body'     => $request_body,
                'Status Code'      => $response->getStatusCode(),
                'Response Headers' => $response->getHeaders(),
                'Response Body'    => json_decode($response->getBody()) != null ? json_decode($response->getBody()) : $response->getBody(),
            ];
            if (!$this->log_request_headers) {
                $content = array_except($content, 'Request Headers');
            }

            $log = new Logger('DHIRAAGU');
            $file_name = $file_name != null ? $file_name : date('Y-m-d').'.log';
            $log->pushHandler(new StreamHandler($this->log_path.'/'.$file_name), Logger::INFO);
            $log->info('SERVICE LOG', $content);
        }
    }
}
