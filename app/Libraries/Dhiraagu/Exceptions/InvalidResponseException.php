<?php
/**
 * Created by PhpStorm.
 * User: Dasun Dissanayake
 * Date: 2019-11-02
 * Time: 11:49 AM.
 */

namespace App\Libraries\Dhiraagu\Exceptions;

use Exception;
use Throwable;

class InvalidResponseException extends Exception implements DhiraaguException
{
    /**
     * InvalidResponseException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'Invalid response format. Please check the token_generate.log file.',
        $code = 6,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
