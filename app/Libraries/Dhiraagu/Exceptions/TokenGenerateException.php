<?php
/**
 * Created by PhpStorm.
 * User: Dasun Dissanayake
 * Date: 2019-11-02
 * Time: 11:49 AM.
 */

namespace App\Libraries\Dhiraagu\Exceptions;

use Exception;
use Throwable;

/**
 * Class TokenGenerateException.
 */
class TokenGenerateException extends Exception implements DhiraaguException
{
    /**
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'Token generation fail. Please check the token_generate.log file.',
        $code = 4,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
