<?php
/**
 * Created by PhpStorm.
 * User: Dasun Dissanayake
 * Date: 2019-11-02
 * Time: 11:49 AM.
 */

namespace App\Libraries\Dhiraagu\Exceptions;

use Throwable;

/**
 * Interface DhiraaguException.
 */
interface DhiraaguException extends Throwable
{
}
