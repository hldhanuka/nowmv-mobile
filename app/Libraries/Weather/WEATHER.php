<?php


namespace App\Libraries\Weather;

use GuzzleHttp\Client;

class WEATHER
{
    /**
     * @param $url
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public static function apiCall($url)
    {
        $request = [
            'headers' => ['Authorization' => "Token ".self::getAccessToken()]
        ];
        $client = new Client(['http_errors' => false]);
        $response = $client->request("GET", $url, $request);
        return json_decode($response->getBody(), true);
    }

    /**
     * @return mixed
     */
    private static function getAccessToken()
    {
        return config('dhiraagu.weather_data.token');
    }
}
