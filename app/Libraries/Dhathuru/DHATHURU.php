<?php


namespace App\Libraries\Dhathuru;

use GuzzleHttp\Client;

class DHATHURU
{
    /**
     * @param $url
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public static function apiCall($url)
    {
        $request = [
            'form_params' => self::getCredentials()
        ];
        $client = new Client(['http_errors' => false]);
        $response = $client->request("POST", $url, $request);
        return json_decode($response->getBody(), true);
    }

    /**
     * @return array
     */
    private static function getCredentials()
    {
        return config('dhiraagu.dhathuru_credentials');
    }
}
