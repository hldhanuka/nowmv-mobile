<?php

namespace App;

use App\Helpers\APIHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventMessage extends Model
{
    use SoftDeletes;

    protected $fillable = ['from', 'seen', 'message', 'created_at', 'updated_at'];

    protected $appends = ['sender_details', 'is_mine'];

    public function from()
    {
        return $this->belongsTo('App\User', 'from', 'id');
    }

    public function getSenderDetailsAttribute()
    {
        $sender_details = $this->from()->take(1)->select('image', 'first_name', 'last_name', 'status')->get()->toArray();
        if (isset($sender_details[0])) {
            return $sender_details[0];
        } else {
            return [
                "image" => asset("assets/img/120x120.png"),
                "first_name" => "Dhiraagu Now MV",
                "last_name" => "APP"
            ];
        }
    }

    public function getIsMineAttribute()
    {
        if(APIHelper::loggedUser()!=null){
            return $this->attributes['from'] == APIHelper::loggedUser()->id;
        } else {
            return false;
        }

    }
}
