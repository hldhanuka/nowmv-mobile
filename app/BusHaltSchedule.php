<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusHaltSchedule extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function startLocation()
    {
        return $this->belongsTo('App\BusLocation', 'start_location_id', 'id');
    }
    public function halt()
    {
        return $this->belongsTo('App\BusLocation', 'halt_id', 'id');
    }

    public function busRoute()
    {
        return $this->belongsTo('App\BusRoute', 'bus_route_id', 'id');
    }
}
