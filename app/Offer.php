<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'content_type'];

    protected $hidden = ['is_send_push_notification', 'push_notification_status', 'push_message'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\OfferCategory', 'category_id', 'id');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Merchant', 'merchant_id', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function userOffers()
    {
        return $this->hasMany('App\UserOffer');
    }

    public function getContentTypeAttribute()
    {
        return "offer";
    }

}
