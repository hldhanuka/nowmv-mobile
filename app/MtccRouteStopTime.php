<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccRouteStopTime extends Model
{
    use SoftDeletes;
}
