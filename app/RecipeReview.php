<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecipeReview extends Model
{
    use SoftDeletes;

    protected $fillable = ['recipe_id', 'comment', 'status', 'created_by'];

    protected $appends = ['created_user_rating'];

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->select(['id', 'first_name', 'last_name', 'image', 'status', 'email']);
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function recipe()
    {
        return $this->belongsTo('App\Recipe', 'recipe_id', 'id');
    }

    public function getCreatedUserRatingAttribute()
    {
        $created_user_rating = RecipeRating::where("created_by", $this->attributes["created_by"])->where("recipe_id", $this->attributes['recipe_id'])->first();

        if ($created_user_rating != null) {
            $rate = $created_user_rating->rate;
        } else {
            $rate = 0;
        }
        return $rate;
    }
}
