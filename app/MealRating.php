<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MealRating extends Model
{
    use SoftDeletes;

    protected $fillable = ['restaurant_id', 'meal_id', 'rate', 'created_by'];
}
