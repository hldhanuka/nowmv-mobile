<?php
/**
 * Created by PhpStorm.
 * User: Dasun
 * Date: 2020-01-24
 * Time: 11:21 AM
 */

namespace App\Helpers;

use App\PushNotification;
use Carbon\Carbon;
use OneSignal;

class OneSignalPush
{
    public static function sendPushNotification($message, $type, $data = null, $sender_id = 0, $receiver_id = null, $relation_id = null)
    {
        try {
            $environment = config("onesignal.DHIRAAGU_PUSH_ENVIRONMENT");
            if ($receiver_id == null) {
                // Sending a Notification To All Users
                $all_customers = APIHelper::getUserOrUsersByActiveSetting($type);
                $now = Carbon::now()->toDateTimeString();

                // Chunk message send because of the ONESIGNAL tag limit(200)
                foreach ($all_customers->chunk(50) as $customers) {
                    $notification_data = [];
                    $tagged_user_array = [];
                    foreach ($customers as $customer) {
                        $notification_data[] = [
                            "from" => $sender_id,
                            "to" => $customer->id,
                            "seen" => 0,
                            "message" => $message,
                            "type" => $type,
                            "redirect_type" => $data ? $data['type'] : null,
                            "redirect_id" => $data ? $data['id'] : null,
                            "created_at" => $now,
                            "updated_at" => $now
                        ];
                        $tagged_user_array[] = ["field" => "tag", "key" => "_id", "relation" => "=", "value" => $customer->id];
                        $tagged_user_array[] = ["operator" => "AND"];
                        $tagged_user_array[] = ["field" => "tag", "key" => "env", "relation" => "=", "value" => $environment];
                        if ($customer != $customers->last()) {
                            $tagged_user_array[] = ["operator" => "OR"];
                        }
                    }
                    PushNotification::insert($notification_data);

                    $params = [
                        'app_id' => config('onesignal.app_id'),
                        'content_available' => false, // Not a background push
                        'contents' => ["en" => $message],
                        'tags' => $tagged_user_array,
                        'ios_badgeType' => 'Increase',
                        'ios_badgeCount' => 1,
                        'data' => $data,
                    ];

                    OneSignal::sendNotificationCustom($params);
                }

            } else {
                // Sending a Notification To A Specific User
                $user = APIHelper::getUserOrUsersByActiveSetting($type, $receiver_id);
                if ($user != null) {
                    $new_push = new PushNotification();
                    $new_push->from = $sender_id;
                    $new_push->to = $receiver_id;
                    $new_push->seen = 0;
                    $new_push->message = $message;
                    $new_push->type = $type;
                    $new_push->redirect_type = $data ? $data['type'] : null;
                    $new_push->redirect_id = $data ? $data['id'] : null;
                    $new_push->relation_id = $relation_id;
                    $new_push->save();
                    $new_push_id = $new_push->id;
                    $tagged_user_array[] = ["field" => "tag", "key" => "_id", "relation" => "=", "value" => $receiver_id];
                    $tagged_user_array[] = ["operator" => "AND"];
                    $tagged_user_array[] = ["field" => "tag", "key" => "env", "relation" => "=", "value" => $environment];

                    $params = [
                        'app_id' => config('onesignal.app_id'),
                        'content_available' => false, // Not a background push
                        'contents' => ["en" => $message],
                        'tags' => $tagged_user_array,
                        'ios_badgeType' => 'Increase',
                        'ios_badgeCount' => 1,
                        'data' => $data,
                    ];

                    OneSignal::sendNotificationCustom($params);
                }
            }

        } catch (\Exception $e) {
            report($e);
        }
    }

    public static function sendReminderNotification($message, $type, $data = null, $sender_id = 0, $receiver_id = null, $relation_id = null)
    {
        try {
            $environment = config("onesignal.DHIRAAGU_PUSH_ENVIRONMENT");

            // Sending a Notification To A Specific User
            $user = APIHelper::getUserOrUsersByActiveSetting($type, $receiver_id);
            if ($user != null) {
                $new_push = new PushNotification();
                $new_push->from = $sender_id;
                $new_push->to = $receiver_id;
                $new_push->seen = 0;
                $new_push->message = $message;
                $new_push->type = $type;
                $new_push->redirect_type = $data ? $data['type'] : null;
                $new_push->redirect_id = $data ? $data['id'] : null;
                $new_push->relation_id = $relation_id;
                $new_push->save();
                $new_push_id = $new_push->id;
                $tagged_user_array[] = ["field" => "tag", "key" => "_id", "relation" => "=", "value" => $receiver_id];
                $tagged_user_array[] = ["operator" => "AND"];
                $tagged_user_array[] = ["field" => "tag", "key" => "env", "relation" => "=", "value" => $environment];

                $params = [
                    'app_id' => config('onesignal.app_id'),
                    'content_available' => false, // A background push(silent push)
                    'contents' => ["en" => $message],
                    'tags' => $tagged_user_array,
                    'ios_badgeType' => 'SetTo',
                    'ios_badgeCount' => 0,
                    'data' => $data,
                    'isAndroid' => true, // https://documentation.onesignal.com/reference/create-notification#platform-to-deliver-to
                    'isIos' => false, // iOS all Reminder notifications handle by APP side
                ];

                OneSignal::sendNotificationCustom($params);
            }

        } catch (\Exception $e) {
            report($e);
        }
    }

    public static function sendCalenderNotification($message, $type, $data = null, $sender_id = 0, $receiver_id = null)
    {
        try {
            $environment = config("onesignal.DHIRAAGU_PUSH_ENVIRONMENT");
            if ($receiver_id == null) {
                // Sending a Notification To All Users
                $all_customers = APIHelper::getUserOrUsersByActiveSetting($type);
                $now = Carbon::now()->toDateTimeString();

                // Chunk message send because of the ONESIGNAL tag limit(200)
                foreach ($all_customers->chunk(50) as $customers) {
                    $notification_data = [];
                    $tagged_user_array = [];
                    foreach ($customers as $customer) {
                        $notification_data[] = [
                            "from" => $sender_id,
                            "to" => $customer->id,
                            "seen" => 0,
                            "message" => $message,
                            "type" => $type,
                            "redirect_type" => $data ? $data['type'] : null,
                            "redirect_id" => $data ? $data['id'] : null,
                            "created_at" => $now,
                            "updated_at" => $now
                        ];
                        $tagged_user_array[] = ["field" => "tag", "key" => "_id", "relation" => "=", "value" => $customer->id];
                        $tagged_user_array[] = ["operator" => "AND"];
                        $tagged_user_array[] = ["field" => "tag", "key" => "env", "relation" => "=", "value" => $environment];
                        if ($customer != $customers->last()) {
                            $tagged_user_array[] = ["operator" => "OR"];
                        }
                    }
                    PushNotification::insert($notification_data);

                    $params = [
                        'app_id' => config('onesignal.app_id'),
                        'content_available' => false, // Not a background push
                        'contents' => ["en" => $message],
                        'tags' => $tagged_user_array,
                        'ios_badgeType' => 'Increase',
                        'ios_badgeCount' => 1,
                        'data' => $data,
                    ];

                    OneSignal::sendNotificationCustom($params);
                }
            }
        } catch (\Exception $e) {
            report($e);
        }
    }

    public static function customPushNotification($message, $type, $data = null, $sender_id = 0, $receiver_id = null, $relation_id = null)
    {
        try {
            $environment = config("onesignal.DHIRAAGU_PUSH_ENVIRONMENT");
            $tagged_user_array[] = ["field" => "tag", "key" => "_id", "relation" => "=", "value" => $receiver_id];
            $tagged_user_array[] = ["operator" => "AND"];
            $tagged_user_array[] = ["field" => "tag", "key" => "env", "relation" => "=", "value" => $environment];

            $params = [
                'app_id' => config('onesignal.app_id'),
                'content_available' => false, // Not a background push
                'contents' => ["en" => $message],
                'tags' => $tagged_user_array,
                'ios_badgeType' => 'Increase',
                'ios_badgeCount' => 1,
                'data' => $data,
            ];

            OneSignal::sendNotificationCustom($params);

        } catch (\Exception $e) {
            report($e);
        }

    }
}
