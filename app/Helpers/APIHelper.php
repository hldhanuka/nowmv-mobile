<?php


namespace App\Helpers;

use App\BusHaltSchedule;
use App\EventInvitee;
use App\FerryTerminalSchedule;
use App\Libraries\Weather\WEATHER;
use App\MtccCalendarAnnualHoliday;
use App\MtccCalendarFixedHoliday;
use App\NotificationType;
use App\PrayerLocation;
use App\PrayerTime;
use App\PushNotification;
use App\ShortLink;
use App\User;
use App\UserNotificationSetting;
use App\UserPrayerTime;
use App\UserSetting;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class APIHelper
 * @package App\Helpers
 */
class APIHelper
{
    /**
     * @param bool $status
     * @param string $message
     * @param null $data
     * @param int $status_code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function makeAPIResponse($status = true, $message = "Done", $data = null, $status_code = 200)
    {
        $response = [
            "success" => $status,
            "message" => $message,
            "data" => $data,

        ];
        if ($data != null || is_array($data)) {
            $response["data"] = $data;
        }
        return response()->json($response, $status_code);
    }


    /**
     * @param bool $status
     * @param string $message
     * @param null $data
     * @param int $status_code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function makePaginateAPIResponse($status = true, $message = "Done", $data = null, $status_code = 200)
    {
        $response = [
            "success" => $status,
            "message" => $message,
            "data" => $data->items(),
            "paginator" => [
                "per_page" => $data->perPage(),
                "current_page" => $data->currentPage(),
                "last_page" => $data->lastPage(),
                "from" => $data->firstItem(),
                "to" => $data->lastItem(),
                "total" => $data->total()
            ]
        ];

        return response()->json($response, $status_code);
    }

    /**
     * @param $action
     * @param $request
     * @throws \Exception
     */
    public static function specialRequestLog($action, $request)
    {
        $log_text = "ACTION => " . $action . " | CLIENT IP => " . APIHelper::getIp() . " | REQUEST URL => " . $request->getUri() . " | REQUEST METHOD => " . $request->getMethod() . " | REQUEST BODY => " . (string)$request->getContent();
        $log = [$log_text];
        $orderLog = new \Monolog\Logger("API");
        $orderLog->pushHandler(new \Monolog\Handler\StreamHandler(storage_path('logs/service/special.log')), \Monolog\Logger::CRITICAL);
        $orderLog->info('Log', $log);
    }

    /**
     * @param $request
     * @throws \Exception
     */
    public static function tooManyAttemptLog($request)
    {
        $log_text = "CLIENT IP => " . APIHelper::getIp() . " | REQUEST URL => " . $request->getUri() . " | REQUEST METHOD => " . $request->getMethod() . " | REQUEST BODY => " . (string)$request->getContent();
        $log = [$log_text];
        $orderLog = new \Monolog\Logger("API");
        $orderLog->pushHandler(new \Monolog\Handler\StreamHandler(storage_path('logs/service/too_many_attempt.log')), \Monolog\Logger::CRITICAL);
        $orderLog->info('Log', $log);
    }

    /**
     * @return string
     */
    public static function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

    /**
     * @param $errors
     * @return array
     */
    public static function errorsResponse($errors)
    {
        $error_data = [];
        foreach ($errors as $x => $x_value) {
            $data['source'] = $x;
            foreach ($x_value as $value) {
                if (is_array($value)) {
                    $data['detail'] = $value[1];
                } else {
                    $data['detail'] = $value;
                }
            }
            $error_data[] = $data;
        }
        $response = [
            "success" => false,
            "message" => "Validation Errors",
            "errors" => $error_data
        ];
        return $response;
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function setDefaultNotificationSettings($user_id)
    {
        $user = User::find($user_id);
        if ($user != null) {
            $all_notifications = NotificationType::all();
            $data = [];
            $user_data = [];
            foreach ($all_notifications as $notification) {
                $data['title'] = $notification->title;
                $data['type_id'] = $notification->id;
                $data['user_id'] = $user_id;
                $data['status'] = 1;
                $data['created_at'] = Carbon::now()->toDateTimeString();
                $data['updated_at'] = Carbon::now()->toDateTimeString();
                array_push($user_data, $data);
            }
            $result = UserNotificationSetting::insert($user_data);
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function setDefaultHomeScreenOrder($user_id)
    {
        $user = User::find($user_id);
        if ($user != null) {
            $data = [
                ["id" => 1, "title" => "NEWS"],
                ["id" => 2, "title" => "ADS"],
                ["id" => 3, "title" => "EVENTS"],
                ["id" => 4, "title" => "VIDEO GALLERY"],
                ["id" => 5, "title" => "PRAYER TRACKER"]
            ];
            $result = UserSetting::updateOrCreate(['user_id' => $user->id], ['screen_order' => json_encode($data)]);
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function setDefaultPrayerReminder($user_id)
    {
        $user = User::find($user_id);
        if ($user != null) {
            $data = [
                "fajr" => [],
                "dhuhr" => [],
                "asr" => [],
                "maghrib" => [],
                "isha" => []
            ];
            $result = UserSetting::updateOrCreate(['user_id' => $user->id], ['prayer_reminder' => json_encode($data)]);
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $uri_name
     * @param null $replace_key
     * @param null $replace_value
     * @return mixed|string
     */
    public static function getDhiraaguURL($uri_name, $replace_key = null, $replace_value = null)
    {
        $environment = config('dhiraagu.environment');
        $host = config('dhiraagu.' . $environment . '.base_url');
        $uri = config('dhiraagu.api.' . $uri_name);
        $full_url = $host . $uri;
        if ($replace_key != null) {
            $final_url = str_replace($replace_key, $replace_value, $full_url);
        } else {
            $final_url = $full_url;
        }

        return $final_url;
    }

    /**
     * @param $uri_name
     * @param array $replace_array
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function getDhathuruURL($uri_name, $replace_array = [])
    {
        $full_url = config('dhiraagu.dhathuru_api.' . $uri_name);
        if (count($replace_array)) {
            foreach ($replace_array as $segment => $value) {
                $full_url = str_replace($segment, $value, $full_url);
            }
        }
        return $full_url;
    }

    /**
     * @param $mobile
     * @return false|string
     */
    public static function formatMobile($mobile)
    {
        if ($mobile[0] == '+') {
            $mobile = substr($mobile, 3, strlen($mobile));
        }

        if ($mobile[0] == '0') {
            $mobile = ltrim($mobile, "0");
        }

        if ($mobile[0] == '9' && $mobile[1] == '6' && $mobile[2] == '0') {
            $mobile = substr($mobile, 3, strlen($mobile));
        }

        return $mobile;
    }

    public static function isBypassMobile($mobile)
    {
        $bypassed_mobiles = explode(",", config('dhiraagu.bypass_mobile'));
        return in_array($mobile, $bypassed_mobiles);
    }

    public static function loggedUser($select = ['*'])
    {
        $mobile = request()->mobile;
        return User::select($select)->where('mobile', $mobile)->first();
    }

    /**
     * @param $date
     * @param $user
     * @param bool $ignore_sunrise
     * @return array|null
     */
    public static function getPrayerMateTimeByDateAndUser($date, $user, $ignore_sunrise = false)
    {
        $user_prayer_location_id = $user->prayer_location_id;
        if ($user_prayer_location_id != null) {
            $prayer_location = PrayerLocation::find($user_prayer_location_id);
            $time_adjustment = $prayer_location->time_adjustment;
            $prayer_times = PrayerTime::where([
                'atoll_id' => $prayer_location->atoll_id,
                'day' => $date->day,
                'month_id' => $date->month,
            ])->get();
            $prayer_times_ids = $prayer_times->pluck('id')->toArray();
            $user_mentioned_prayer_times = UserPrayerTime::where(['user_id' => $user->id, 'status' => 1])->whereIn('prayer_time_id', $prayer_times_ids)->get();
            if (count($prayer_times)) {

                $fajr = $prayer_times->where("prayer_id", config('dhiraagu.prayer_types.fajr'))->first();
                $public_fajr_datetime = Carbon::parse($date->toDateString() . " " . $fajr->time);
                $user_fajr_datetime = $public_fajr_datetime->copy()->addMinute($time_adjustment);
                $is_user_prayed_fajr = $user_mentioned_prayer_times->where('prayer_time_id', $fajr->id)->where('date_time', $public_fajr_datetime)->isNotEmpty();
                $fajr_pray_text = self::getPrayText($is_user_prayed_fajr, $user_fajr_datetime);

                $sunrise = $prayer_times->where("prayer_id", config('dhiraagu.prayer_types.sunrise'))->first();
                $public_sunrise_datetime = Carbon::parse($date->toDateString() . " " . $sunrise->time);
                $user_sunrise_datetime = $public_sunrise_datetime->copy()->addMinute($time_adjustment);
                $is_user_prayed_sunrise = $user_mentioned_prayer_times->where('prayer_time_id', $sunrise->id)->where('date_time', $public_sunrise_datetime)->isNotEmpty();
                $sunrise_pray_text = self::getPrayText($is_user_prayed_sunrise, $user_sunrise_datetime);

                $dhuhr = $prayer_times->where("prayer_id", config('dhiraagu.prayer_types.dhuhr'))->first();
                $public_dhuhr_datetime = Carbon::parse($date->toDateString() . " " . $dhuhr->time);
                $user_dhuhr_datetime = $public_dhuhr_datetime->copy()->addMinute($time_adjustment);
                $is_user_prayed_dhuhr = $user_mentioned_prayer_times->where('prayer_time_id', $dhuhr->id)->where('date_time', $public_dhuhr_datetime)->isNotEmpty();
                $dhuhr_pray_text = self::getPrayText($is_user_prayed_dhuhr, $user_dhuhr_datetime);

                $asr = $prayer_times->where("prayer_id", config('dhiraagu.prayer_types.asr'))->first();
                $public_asr_datetime = Carbon::parse($date->toDateString() . " " . $asr->time);
                $user_asr_datetime = $public_asr_datetime->copy()->addMinute($time_adjustment);
                $is_user_prayed_asr = $user_mentioned_prayer_times->where('prayer_time_id', $asr->id)->where('date_time', $public_asr_datetime)->isNotEmpty();
                $asr_pray_text = self::getPrayText($is_user_prayed_asr, $user_asr_datetime);

                $maghrib = $prayer_times->where("prayer_id", config('dhiraagu.prayer_types.maghrib'))->first();
                $public_maghrib_datetime = Carbon::parse($date->toDateString() . " " . $maghrib->time);
                $user_maghrib_datetime = $public_maghrib_datetime->copy()->addMinute($time_adjustment);
                $is_user_prayed_maghrib = $user_mentioned_prayer_times->where('prayer_time_id', $maghrib->id)->where('date_time', $public_maghrib_datetime)->isNotEmpty();
                $maghrib_pray_text = self::getPrayText($is_user_prayed_maghrib, $user_maghrib_datetime);

                $isha = $prayer_times->where("prayer_id", config('dhiraagu.prayer_types.isha'))->first();
                $public_isha_datetime = Carbon::parse($date->toDateString() . " " . $isha->time);
                $user_isha_datetime = $public_isha_datetime->copy()->addMinute($time_adjustment);
                $is_user_prayed_isha = $user_mentioned_prayer_times->where('prayer_time_id', $isha->id)->where('date_time', $public_isha_datetime)->isNotEmpty();
                $isha_pray_text = self::getPrayText($is_user_prayed_isha, $user_isha_datetime);

                $user_prayer_times =
                    [
                        'fajr' => [
                            "id" => $fajr->id,
                            "time" => $user_fajr_datetime->toTimeString(),
                            "pray" => $fajr_pray_text
                        ],
                        'sunrise' => [
                            "id" => $sunrise->id,
                            "time" => $user_sunrise_datetime->toTimeString(),
                            "pray" => $sunrise_pray_text
                        ],
                        'dhuhr' => [
                            "id" => $dhuhr->id,
                            "time" => $user_dhuhr_datetime->toTimeString(),
                            "pray" => $dhuhr_pray_text
                        ],
                        'asr' => [
                            "id" => $asr->id,
                            "time" => $user_asr_datetime->toTimeString(),
                            "pray" => $asr_pray_text
                        ],
                        'maghrib' => [
                            "id" => $maghrib->id,
                            "time" => $user_maghrib_datetime->toTimeString(),
                            "pray" => $maghrib_pray_text
                        ],
                        'isha' => [
                            "id" => $isha->id,
                            "time" => $user_isha_datetime->toTimeString(),
                            "pray" => $isha_pray_text
                        ],
                    ];

                if ($ignore_sunrise) {
                    $user_prayer_times = Arr::except($user_prayer_times, ['sunrise']);
                }

                return $user_prayer_times;
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    /**
     * @param $carbon_date
     * @param $user
     * @param $prayer_tracker
     * @return string
     */
    public static function getNextPrayerTime($carbon_date, $user, $prayer_tracker = null)
    {
        $next_prayer_time = "";
        $is_found_next_prayer_time = false;
        $only_date = $carbon_date->toDateString();
        if ($prayer_tracker != null) {
            foreach ($prayer_tracker as $prayer_time) {
                if (Carbon::parse($only_date . " " . $prayer_time["time"])->isFuture()) {
                    $is_found_next_prayer_time = true;
                    $next_prayer_time = $prayer_time["time"];
                    break;
                }
            }
            if ($is_found_next_prayer_time) {
                return $next_prayer_time;
            } else {
                $next_date = $carbon_date->addDay();
                $next_prayer_tracker = APIHelper::getPrayerMateTimeByDateAndUser($next_date, $user, true);
                return self::getNextPrayerTime($next_date, $user, $next_prayer_tracker);
            }
        } else {
            return $next_prayer_time;
        }
    }

    /**
     * @param $is_prayed
     * @param $date_time
     * @return string
     */
    public static function getPrayText($is_prayed, $date_time)
    {
        if ($is_prayed) {
            return "PRAYED";
        } else {
            if ($date_time->isFuture()) {
                return "YET TO PRAY";
            } else {
                return "NOT PRAYED";
            }
        }
    }

    /**
     * @param $array
     * @param $object
     * @param int $position
     * @return array
     */
    public static function pushObjectToMiddleOfArray($array, $object, $position = 0)
    {
        $index = 0;
        $data = [];
        foreach ($array as $item) {
            if ($index == $position) {
                $data[] = $object;
            }
            $data[] = $item;
            $index++;
        }
        return $data;
    }

    /**
     * @return string
     */
    public static function generateRandomCode()
    {
        $timestamp = round(microtime(true) * 1000);
        $code = base64_encode($timestamp . rand(111111111, 999999999));
        return $code;
    }

    /**
     * @param $data
     * @return array
     */
    public static function addWeatherImage($data)
    {

        if (isset($data["hourly"]) && isset($data["hourly"]["sky_condition"])) {
            $sky_condition = $data["hourly"]["sky_condition"];
        } else {
            $sky_condition = "Fine";
        }

        switch ($sky_condition) {
            case "Partly Cloudy":
                $image = url("/uploads/weather/Partly_Cloudy.jpg");
                break;
            case "Cloudy":
            case "Overcast":
                $image = url("/uploads/weather/Overcast.jpg");
                break;
            case "Windy":
                $image = url("/uploads/weather/Windy.jpg");
                break;
            case "Hazy":
            case "Haze":
                $image = url("/uploads/weather/Haze.jpg");
                break;
            case "Showers":
            case "Slight Showers":
                $image = url("/uploads/weather/Slight_Showers.jpg");
                break;
            case "Moderate Showers":
                $image = url("/uploads/weather/Moderate_Showers.jpg");
                break;
            case "Heavy Showers":
                $image = url("/uploads/weather/Heavy_Showers.jpg");
                break;
            case "Slight Rain":
                $image = url("/uploads/weather/Slight_Rain.jpg");
                break;
            case "Moderate Rain":
                $image = url("/uploads/weather/Moderate_Rain.jpg");
                break;
            case "Torrential Rain":
            case "Heavy Rain":
                $image = url("/uploads/weather/Heavy_Rain.jpg");
                break;
            case "Slight Thundershowers":
                $image = url("/uploads/weather/Slight_Thundershowers.jpg");
                break;
            case "Moderate Thundershowers":
                $image = url("/uploads/weather/Moderate_Thundershowers.jpg");
                break;
            case "Heavy Thundershowers":
                $image = url("/uploads/weather/Heavy_Thundershowers.jpg");
                break;
            case "Squally Showers":
                $image = url("/uploads/weather/Squally_Showers.jpg");
                break;
            case "Squally Thundershowers":
                $image = url("/uploads/weather/Squally_Thundershowers.jpg");
                break;
            case "Thunder":
                $image = url("/uploads/weather/Thunder.jpg");
                break;
            case "Fine":
            case "Clear":
            default:
                $image = url("/uploads/weather/Fine.jpg");
                break;
        }

        $data["image"] = $image;
        $data["date"] = Carbon::now()->toDateString();
        return $data;
    }

    /**
     * @param $bus_schedule_data
     * @param $date
     * @param $from
     * @param $to
     * @return array
     */
    public static function getBusRouteAllSchedule($bus_schedule_data, $date, $from, $to)
    {
        $bus_starting_time = Carbon::parse($date . " " . $bus_schedule_data->starting_time);
        $final_array["schedule_time"] = $bus_starting_time;
        $final_array["schedule_list"] = [];
        $final_array["valid_direction"] = true;

        $to_halt = BusHaltSchedule::where("bus_route_id", $bus_schedule_data->bus_route_id)->where('halt_id', $to)->first();
        if ($to_halt != null) {
            $bus_halt_schedules = BusHaltSchedule::with('halt')->where("bus_route_id", $bus_schedule_data->bus_route_id)->orderBy("time")->get();
            $final_array = [];

            foreach ($bus_halt_schedules as $bus_halt_schedule) {
                $hours = Carbon::parse($bus_halt_schedule->time)->hour;
                $minutes = Carbon::parse($bus_halt_schedule->time)->minute;
                $seconds = Carbon::parse($bus_halt_schedule->time)->second;
                $actual_time = $bus_starting_time->copy()->addHours($hours)->addMinutes($minutes)->addSeconds($seconds)->toDateTimeString();
                if ($bus_halt_schedule->halt_id == $from) {
                    $is_current = true;
                    $final_array["schedule_time"] = $actual_time;
                    $final_array["valid_direction"] = $to_halt->time > $bus_halt_schedule->time;
                } else {
                    $is_current = false;
                }
                $final_array["schedule_list"][] = [
                    "name" => $bus_halt_schedule->halt->name,
                    "schedule_time" => $actual_time,
                    "is_current" => $is_current
                ];

            }
        }
        return $final_array;
    }

    /**
     * @param $ferry_schedule_data
     * @param $date
     * @param $from
     * @param $to
     * @return array
     */
    public static function getFerryRouteAllSchedule($ferry_schedule_data, $date, $from, $to)
    {
        $ferry_starting_time = Carbon::parse($date . " " . $ferry_schedule_data->starting_time);
        $final_array["schedule_time"] = $ferry_starting_time;
        $final_array["schedule_last_time"] = null;
        $final_array["schedule_list"] = [];
        $final_array["valid_direction"] = true;

        $to_terminal = FerryTerminalSchedule::where("ferry_route_id", $ferry_schedule_data->ferry_route_id)->where('terminal_id', $to)->first();
        if ($to_terminal != null) {
            $ferry_terminal_schedules = FerryTerminalSchedule::where("ferry_route_id", $ferry_schedule_data->ferry_route_id)->orderBy("time")->get();
            $final_array = [];

            foreach ($ferry_terminal_schedules as $ferry_terminal_schedule) {
                $hours = Carbon::parse($ferry_terminal_schedule->time)->hour;
                $minutes = Carbon::parse($ferry_terminal_schedule->time)->minute;
                $seconds = Carbon::parse($ferry_terminal_schedule->time)->second;
                $actual_time = $ferry_starting_time->copy()->addHours($hours)->addMinutes($minutes)->addSeconds($seconds)->toDateTimeString();
                if ($ferry_terminal_schedule->terminal_id == $from) {
                    $is_current = true;
                    $final_array["terminal_id"] = $ferry_terminal_schedule->terminal_id;
                    $final_array["schedule_time"] = $actual_time;
                    $final_array["valid_direction"] = $to_terminal->time > $ferry_terminal_schedule->time;
                } else {
                    $is_current = false;
                }
                $final_array["schedule_list"][] = [
                    "terminal_id" => $ferry_terminal_schedule->terminal_id,
                    "schedule_time" => $actual_time,
                    "is_current" => $is_current
                ];
                $final_array["schedule_last_time"] = $actual_time;

            }
        }
        return $final_array;
    }

    /**
     * @param $type
     * @param null $id
     * @return mixed
     */
    public static function getUserOrUsersByActiveSetting($type, $id = null)
    {
        switch ($type) {
            case 'system' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 1)->active();
                });
                break;
            case 'transaction' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 2)->active();
                });
                break;
            case 'weather' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 3)->active();
                });
                break;
            case 'travel' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 4)->active();
                });
                break;
            case 'offer' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 5)->active();
                });
                break;
            case 'tip' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 6)->active();
                });
                break;
            case 'event' :
            case 'event_invitation' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 7)->active();
                });
                break;
            case 'prayer_reminder' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 8)->active();
                });
                break;
            case 'travel_reminder' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 9)->active();
                });
                break;
            case 'bus' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 10)->active();
                });
                break;
            case 'ferry' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 11)->active();
                });
                break;
            case 'flight' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 12)->active();
                });
                break;
            case 'calender' :
                $users = User::role("customer")->whereHas('userNotificationSettings', function ($sub_query) {
                    $sub_query->where('type_id', 13)->active();
                });
                break;
            default:
                $users = User::role("customer");
        }

        if ($id == null) {
            // all users
            $users = $users->get();
        } else {
            // for one user
            $users = $users->where("id", $id)->first();
        }
        return $users;

    }

    /**
     * @param $type
     * @return array
     */
    public static function getSenderDetailArray($type)
    {
        switch ($type) {
            case 'system' :
                $array = [
                    "image" => asset("assets/img/120x120.png"),
                    "first_name" => "Dhiraagu Now MV",
                    "last_name" => ""
                ];
                break;
            case 'weather' :
                $array = [
                    "image" => asset("assets/img/weather.png"),
                    "first_name" => "Weather",
                    "last_name" => "",
                ];
                break;
            case 'offer' :
                $array = [
                    "image" => asset("assets/img/promotion.png"),
                    "first_name" => "Promotions",
                    "last_name" => "",
                ];
                break;
            case 'bus' :
            case 'flight' :
            case 'ferry' :
            case 'travel' :
                $array = [
                    "image" => asset("assets/img/travel.png"),
                    "first_name" => "Travel",
                    "last_name" => "",
                ];
                break;
            case 'tip' :
                $array = [
                    "image" => asset("assets/img/tips.png"),
                    "first_name" => "Tips",
                    "last_name" => "",
                ];
                break;
            case 'event' :
                $array = [
                    "image" => asset("assets/img/events.png"),
                    "first_name" => "Event",
                    "last_name" => "",
                ];
                break;
            case 'prayer' :
                $array = [
                    "image" => asset("assets/img/prayer.png"),
                    "first_name" => "Prayer",
                    "last_name" => "",
                ];
                break;
            default:
                $array = [
                    "image" => asset("assets/img/120x120.png"),
                    "first_name" => "Dhiraagu Now MV",
                    "last_name" => ""
                ];
        }
        return $array;
    }

    /**
     * @param $date
     * @param string $format
     * @return string
     */
    public static function setDhiraaguDateFormat($date, $format = "datetime")
    {
        try {
            if ($date != null) {
                $carbon_date = Carbon::parse($date);
                if ($format == "datetime") {
                    $format_date = $carbon_date->format("M d, Y h:i A");
                } else if ($format == "date") {
                    $format_date = $carbon_date->format("M d, Y");
                } else if ($format == "time") {
                    $format_date = $carbon_date->format("h:i A");
                } else {
                    $format_date = $carbon_date->format("M d, Y");
                }
            } else {
                $format_date = "";
            }
        } catch (\Exception $e) {
            $format_date = "";
        }
        return $format_date;
    }

    /**
     * @param $amount
     * @return string
     */
    public static function setAmountFormat($amount)
    {
        if (fmod($amount, 1) !== 0.00) {
            return number_format($amount, 2, ".", ",");
        } else {
            return number_format($amount, 0, ".", ",");
        }
    }


    /**
     * @param array $mobile_array
     * @param integer $event
     * @param integer $inviter_user
     */
    public static function createInactiveUsersAndInviteToEvent($mobile_array, $event, $inviter_user)
    {
        try {
            foreach ($mobile_array as $mobile) {
                if (User::where("mobile", $mobile)->first() == null) {
                    $code = rand(111111111, 999999999);
                    $invitee_user = new User();
                    $invitee_user->first_name = "user_" . $code;
                    $invitee_user->last_name = "(guest)";
                    $invitee_user->email = $code . "@nowmv.com";
                    $invitee_user->mobile = $mobile;
                    $invitee_user->birthday = Carbon::now()->toDateString();
                    $invitee_user->address = $code;
                    $invitee_user->gender = 'male';
                    $invitee_user->status = 4;
                    $invitee_user->save();

                    // user in the DB then create DB record
                    $event_invitation = EventInvitee::updateOrCreate(['user_id' => $inviter_user->id, 'event_id' => $event->id, 'invitee_id' => $invitee_user->id], ['status' => 2]);
                    if ($event_invitation) {
                        // Create push notification
                        $creator_full_name = $inviter_user->first_name . " " . $inviter_user->last_name;
                        $format_date = Carbon::parse($event->date . " " . $event->time)->format('l jS \\of F Y h:i A');
                        $message = $creator_full_name . "  invited you to \"" . $event->name . "\" event held on " . $format_date . " at " . $event->location_name;

                        $new_push = new PushNotification();
                        $new_push->from = $inviter_user->id;
                        $new_push->to = $invitee_user->id;
                        $new_push->seen = 0;
                        $new_push->message = $message;
                        $new_push->type = "event_invitation";
                        $new_push->relation_id = $event_invitation->id;
                        $new_push->save();
                    }
                }
            }
        } catch (\Exception $e) {
            report($e);
        }
    }

    /**
     * @param string $web_link
     * @return string
     */
    public static function createFirebaseShareLink($web_link)
    {
        $encoded_web_link = urlencode($web_link);
        $firebase_url = config('app.firebase.dynamic_link.base_link') .
            $encoded_web_link .
            "&apn=" . config('app.firebase.dynamic_link.android_apn') .
            "&afl=" . urlencode(config('app.firebase.dynamic_link.android_afl')) .
            "&amv=" . config('app.firebase.dynamic_link.android_amv') .
            "&ibi=" . config('app.firebase.dynamic_link.ios_ibi') .
            "&isi=" . config('app.firebase.dynamic_link.ios_isi') .
            "&imv=" . config('app.firebase.dynamic_link.ios_imv') .
            "&efr=1";
        return APIHelper::generateShortenLink($firebase_url);
    }

    /**
     * @param $url
     * @return string
     */
    public static function generateShortenLink($url)
    {
        $short_url = ShortLink::where("link", $url)->first();
        if ($short_url != null) {
            $code = $short_url->code;
        } else {
            $code = Str::random(10);
            if (ShortLink::where("code", $code)->exists()) {
                return self::generateShortenLink($url);
            } else {
                $short_url = new ShortLink();
                $short_url->link = $url;
                $short_url->code = $code;
                $short_url->save();
            }
        }
        return route('shorten-link', ['code' => $code]);
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return integer
     */
    public static function getNearestWeatherLocationID($latitude, $longitude)
    {
        $weather_url = config('dhiraagu.weather_data.station_list');
        $weather_data = WEATHER::apiCall($weather_url);

        if ($latitude == null || $longitude == null) {
            $prayer_location = PrayerLocation::first();
            $latitude = $prayer_location->latitude;
            $longitude = $prayer_location->longitude;
        }

        $distances = [];
        foreach ($weather_data as $key => $location) {
            $a = $latitude - $location['lat'];
            $b = $longitude - $location['lon'];
            $distance = sqrt(($a ** 2) + ($b ** 2));
            $distances[$key] = $distance;
        }
        asort($distances);
        $closest = $weather_data[key($distances)];
        return $closest['id'];
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return object
     */
    public static function getMyNearestPrayerLocation($latitude, $longitude)
    {
        if ($latitude == null || $longitude == null) {
            $prayer_location = PrayerLocation::first();
            $latitude = $prayer_location->latitude;
            $longitude = $prayer_location->longitude;
        }

        return PrayerLocation::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->orderBy('distance')->first();
    }

    /**
     * @param $date
     * @return boolean
     */
    public static function checkDateIsHoliday($date)
    {
        $date = Carbon::parse($date);
        $date->day;
        $date->month;

        $annual_holiday = MtccCalendarAnnualHoliday::where('day', $date->day)->where('month', $date->month)->first();
        if ($annual_holiday != null) {
            return true;
        } else {
            $fixed_holiday = MtccCalendarFixedHoliday::whereDate('date', $date->toDateString())->first();
            if ($fixed_holiday != null) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Get APP Redirect Type by ReminderNotification table Type
     * @param string $type
     * @return string $redirect_type
     */
    public static function getAppRedirectTypeByReminderNotificationType($type)
    {
        switch ($type) {
            case "event":
                $redirect_type = "event";
                break;
            case "prayer":
                $redirect_type = "prayer_mate";
                break;
            case "bus":
                $redirect_type = "travel_bus";
                break;
            case "ferry":
                $redirect_type = "travel_ferry";
                break;
            case "flight":
                $redirect_type = "travel_flight";
                break;
            default:
                $redirect_type = "home";
                break;
        }
        return $redirect_type;
    }

}
