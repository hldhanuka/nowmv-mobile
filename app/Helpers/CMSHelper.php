<?php


namespace App\Helpers;

use App\Libraries\Dhathuru\DHATHURU;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class CMSHelper
{
    public static function fileUpload($file_name, $upload_path)
    {
        $upload_success = false;
        $fileName = '';

        // CHECK IS S3
        if (config('filesystems.default') == "s3") {
            $destinationPath = rtrim($upload_path, "/");
            // checking file is valid.
            if (Input::hasFile($file_name)) {
                if (Input::file($file_name) != null && Input::file($file_name)->isValid()) {
                    $s3 = Storage::disk('s3');
                    $path = $s3->put($destinationPath, Input::file($file_name), 'public');
                    $upload_success = true;
                }
            }

            if ($upload_success) {
                return $s3->url($path);
            } else {
                return null;
            }

        } else {
            if (substr($upload_path, -1) != "/") {
                $destinationPath = $upload_path . "/";
            } else {
                $destinationPath = $upload_path;
            }

            // checking file is valid.
            if (Input::hasFile($file_name)) {
                if (Input::file($file_name) != null && Input::file($file_name)->isValid()) {
                    $extension = Input::file($file_name)->getClientOriginalExtension(); // getting image extension
                    $fileName = CMSHelper::randomFileNameCreate($upload_path, $extension); // rename image file

                    // NORMAL UPLOAD
                    if (Input::file($file_name)->move($destinationPath, $fileName)) {
                        $upload_success = true;
                    }
                }
            }

            if ($upload_success) {
                return asset('/') . $destinationPath . $fileName;
            } else {
                return null;
            }
        }


    }

    public static function base64FileUpload($base64data, $upload_path)
    {
        $upload_success = false;

        // CHECK IS S3
        if (config('filesystems.default') == "s3") {
            $destinationPath = rtrim($upload_path, "/");
            if ($base64data != null) {
                list($baseType, $base64code) = explode(';base64,', $base64data);
                list(, $extension) = explode('/', $baseType);

                $image = base64_decode($base64code);
                if ($image != null && $extension != null) {
                    $fileName = self::randomFileNameCreate($destinationPath, $extension);
                    $fullPath = $destinationPath . '/' . $fileName;
                    $s3 = Storage::disk('s3');
                    $s3->put($fullPath, $image, 'public');
                    $upload_success = true;
                }
            }

            if ($upload_success) {
                return $s3->url($fullPath);
            } else {
                return false;
            }
        } else {
            if (substr($upload_path, -1) != "/") {
                $destinationPath = $upload_path . "/";
            } else {
                $destinationPath = $upload_path;
            }

            if ($base64data != null) {
                list($baseType, $base64code) = explode(';base64,', $base64data);
                list(, $extension) = explode('/', $baseType);

                $image = base64_decode($base64code);
                $fileName = CMSHelper::randomFileNameCreate($upload_path, $extension); // rename image file
                $fullPath = $destinationPath . $fileName;
                file_put_contents($fullPath, $image);
                $upload_success = true;
            }

            if ($upload_success) {
                return asset('/') . $fullPath;
            } else {
                return null;
            }
        }
    }

    public static function uploadMultipleFileToStorage($file_name, $index, $upload_path)
    {
        $upload_success = false;
        $fileName = '';

        if (config('filesystems.default') == "s3") {
            $destinationPath = rtrim($upload_path, "/");
            if (Input::hasFile($file_name)) {
                if (Input::file($file_name)[$index] != null && Input::file($file_name)[$index]->isValid()) {
                    $s3 = Storage::disk('s3');
                    $path = $s3->put($destinationPath, Input::file($file_name)[$index], 'public');
                    $upload_success = true;
                }
            }

            if ($upload_success) {
                return $s3->url($path);
            } else {
                return null;
            }

        } else {
            if (substr($upload_path, -1) != "/") {
                $destinationPath = $upload_path . "/";
            } else {
                $destinationPath = $upload_path;
            }

            // checking file is valid.
            if (Input::hasFile($file_name)[$index]) {
                if (Input::file($file_name)[$index] != null && Input::file($file_name)[$index]->isValid()) {
                    $extension = Input::file($file_name)[$index]->getClientOriginalExtension(); // getting image extension
                    $fileName = CMSHelper::randomFileNameCreate($upload_path, $extension); // rename image file

                    // NORMAL UPLOAD
                    if (Input::file($file_name)[$index]->move($destinationPath, $fileName)) {
                        $upload_success = true;
                    }
                }
            }

            if ($upload_success) {
                return asset('/') . $destinationPath . $fileName;
            } else {
                return null;
            }
        }
    }

    public static function randomFileNameCreate($path, $extension)
    {
        $timestamp = round(microtime(true) * 1000);
        $fileName = $timestamp . rand(111111111, 999999999) . '.' . $extension;
        if (file_exists(storage_path($path . $fileName))) {
            return self::randomFileNameCreate($path, $extension);
        } else {
            return $fileName;
        }
    }

    public static function createItemArray($array)
    {

        $result_array = [];

        if (is_array($array)) {
            $new_array = array_filter($array, 'strlen');
            $index = 1;
            foreach ($new_array as $item) {
                $data["id"] = strval($index);
                $data["item"] = $item;
                $result_array[] = $data;
                $index++;
            }
        }

        return $result_array;
    }

    public static function createDateTitleArray($date_array, $title_array)
    {
        $result_array = [];
        if (is_array($date_array) && is_array($title_array) && (count($date_array) == count($title_array))) {
            for ($i = 0; $i < count($date_array); $i++) {
                $data["date"] = $date_array[$i];
                $data["title"] = $title_array[$i];
                $result_array[] = $data;
            }
        }
        return $result_array;
    }

    //for enable notification option
    public static function createDateTitleNotificationArray($date_array, $title_array, $notification_array)
    {
        $result_array = [];
        if (is_array($date_array) && is_array($title_array) && is_array($notification_array) && (count($date_array) == count($title_array) && count($date_array) == count($notification_array))) {
            for ($i = 0; $i < count($date_array); $i++) {
                $data["date"] = $date_array[$i];
                $data["title"] = $title_array[$i];
                if ($notification_array[$i] == "1") {
                    $data["notification"] = true;
                } else {
                    $data["notification"] = false;
                }
                $result_array[] = $data;
            }
        }
        return $result_array;
    }

    public static function getStatusText($status)
    {
        $text = "";
        switch ($status) {
            case 0 :
                $text = "Inactive";
                break;
            case 1 :
                $text = "Active";
                break;
            case 2 :
                $text = "Pending";
                break;
            case 3 :
                $text = "Reject";
                break;
            case 4 :
                $text = "Guest";
                break;
            default :
                break;
        }
        return $text;
    }

    public static function getResponseText($status)
    {
        $text = "";
        switch ($status) {
            case 0 :
                $text = "Error";
                break;
            case 1 :
                $text = "Successful";
                break;
            case 2 :
                $text = "Pending";
                break;
            case 3 :
                $text = "Reject";
                break;
            default :
                break;
        }
        return $text;
    }

    public static function getFerryLocations()
    {
        $island_list_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_island_list"));
        $locations = isset($island_list_response["status"]) && $island_list_response["status"] ? collect($island_list_response["data"])->sortBy("island_name") : collect([]);
        return $locations;
    }

    public static function createDateScheduleArray($dates)
    {
        $final_array = [];
        $date_array = explode(',', $dates);
        foreach ($date_array as $date) {
            $final_array [] = ["date" => Carbon::parse($date)->toDateString(), "title" => ""];
        }
        return json_encode($final_array);
    }
}
