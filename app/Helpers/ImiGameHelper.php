<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

use App\Libraries\Dhiraagu\ConfigHelper;

class ImiGameHelper
{
    public static function sendAPICall($method = "GET", $url = "", $body = null, $headers = ['Content-Type' => 'application/json', 'Accept' => 'application/json; charset=utf-8'], $guzzle_body_type = 'json')
    {
        $curl_exception = false;

        $client = new Client(['http_errors' => false, 'verify' => false]);

        $request_body = [
            'headers' => $headers,
            $guzzle_body_type => $body,
            'timeout' => 30,
        ];

        try {
            $response = $client->request($method, $url, $request_body);

            // api log
            $helper = new ConfigHelper();
            $helper->logService($url, $method, $request_body['headers'], $request_body[$guzzle_body_type], $response);
        } catch (\Exception $e) {
            Log::critical($e);

            $curl_exception = true;
        }

        if (!$curl_exception && $response->getBody()) {
            $status_code = $response->getStatusCode();

            $response_body["body"] = json_decode((string)$response->getBody(), true);
            $response_body["status_code"] = $status_code;
            $response_body["headers"] = $response->getHeaders();

            return $response_body;
        } else {
            $log_data = ["ACTION" => "API ERROR", "URL" => $url, "BODY" => $body, "HEADERS" => $headers, "GUZZLE_BODY_TYPE" => $guzzle_body_type];

            Log::critical(json_encode($log_data));

            return null;
        }
    }
}
