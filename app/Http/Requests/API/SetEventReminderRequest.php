<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SetEventReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_id' => 'required|exists:events,id',
            'date_time' => 'required|date_format:Y-m-d H:i:s',
            'before_1_month' => 'required_without_all:before_1_week,before_1_day|boolean|valid_scheduled_time:date_time,1month',
            'before_1_week' => 'required_without_all:before_1_month,before_1_day|boolean|valid_scheduled_time:date_time,1week',
            'before_1_day' => 'required_without_all:before_1_month,before_1_week|boolean|valid_scheduled_time:date_time,1day',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message=APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message,JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
