<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SetBusReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_time' => 'required|date_format:Y-m-d H:i:s',
            'from' => 'required',
            'to' => 'required',
            'before_30_min' => 'required_without_all:before_15_min,before_5_min|boolean|valid_scheduled_time:date_time,30minutes',
            'before_15_min' => 'required_without_all:before_30_min,before_5_min|boolean|valid_scheduled_time:date_time,15minutes',
            'before_5_min' => 'required_without_all:before_30_min,before_15_min|boolean|valid_scheduled_time:date_time,5minutes',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message=APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message,JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

}
