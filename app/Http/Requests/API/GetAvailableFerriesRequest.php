<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use App\Libraries\Dhathuru\DHATHURU;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class GetAvailableFerriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $island_segment_array = [
            "{island_id}" => request()->input('island_id'),
        ];
        $island_detail_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_island_details", $island_segment_array));
        if (isset($island_detail_response["status"]) && $island_detail_response["status"] && is_array($island_detail_response["data"])) {
            $exist = request()->input('island_id');
        } else {
            $exist = false;
        }
        return [
            'island_id' => 'required|numeric|min:1|max:205|in:'.$exist,
            'date' => 'required|date|date_format:Y-m-d|after:'.Carbon::yesterday()->toDateString(),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
