<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use App\ZakatCommodity;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SubmitZakatPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'id_number' => 'required',
            'address' => 'required',
            'atoll_id' => 'required|exists:zakat_atolls,id',
            'island_id' => 'required|exists:zakat_islands,id',
            'no_of_person' => 'required|numeric',
            'commodity_id' => 'required|exists:zakat_commodities,id',
            'zakat_amount' => 'required|numeric',
            'sadagath_amount' => 'nullable|numeric',
            'total_amount' => 'required',
            'mobile' => 'required',
            'payment_method' => 'required|in:dhiraagu_pay',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
