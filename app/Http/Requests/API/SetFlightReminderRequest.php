<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SetFlightReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:arrival,departure',
            'flight' => 'required',
            'origin' => 'required',
            'destination' => 'required',
            'date_time' => 'required|date_format:Y-m-d H:i:s',
            'before_3_hour' => 'required_without_all:before_1_hour,before_30_min,before_15_min|boolean|valid_scheduled_time:date_time,3hour',
            'before_1_hour' => 'required_without_all:before_3_hour,before_30_min,before_15_min|boolean|valid_scheduled_time:date_time,1hour',
            'before_30_min' => 'required_without_all:before_3_hour,before_1_hour,before_15_min|boolean|valid_scheduled_time:date_time,30minutes',
            'before_15_min' => 'required_without_all:before_3_hour,before_1_hour,before_30_min|boolean|valid_scheduled_time:date_time,15minutes',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message=APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message,JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
