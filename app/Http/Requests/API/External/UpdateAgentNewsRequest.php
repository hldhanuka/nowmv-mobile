<?php

namespace App\Http\Requests\API\External;

use App\Helpers\APIHelper;
use App\Rules\NewsTagList;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UpdateAgentNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        return [
            'category_id' => 'required|exists:news_categories,id',
            'title' => 'required',
            'image' => ['nullable','image', 'dimensions:max_width=2000','max:4096', Rule::requiredIf(function () use ($request) {
                return !$request->hasFile('video');
            })],
            'video' => 'nullable|url|regex:/https:\/\/www\.youtube\.com\/watch\?v=[^&]+/i',
            'video_thumbnail' => ['nullable','image', 'dimensions:max_width=2000', 'max:4096', Rule::requiredIf(function () use ($request) {
                return $request->input('video')!=null;
            })],
            'content' => 'required',
            'author' => 'nullable',
            'link' => 'nullable|url',
            'tags' => ['required', new NewsTagList()],
            'is_featured' => 'required',
            'news_agency_id' => ['required', Rule::in($request->my_news_agency_ids)]
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message=APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message,JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

}
