<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => "required",
            "last_name" => "required",
            "email" => "required|email",
            "mobile" => ["required","numeric", Rule::unique('users', 'mobile')->where(function ($query) {
                return $query->where('status', '!=',4);
            })],
            "birthday" => "nullable|date|date_format:Y-m-d|before:" . Carbon::tomorrow()->toDateString(),
            "address" => "nullable",
            "gender" => "nullable|in:male,female",
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

    protected function validationData()
    {
        if ($this->request->has('mobile')) {
            $this->request->add(['mobile' => APIHelper::formatMobile(trim($this->request->get('mobile')))]);
        }
        if ($this->request->has('email')) {
            $this->request->add(['email' => trim($this->request->get('email'))]);
        }
        return $this->all();
    }

}
