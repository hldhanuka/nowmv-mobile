<?php

namespace App\Http\Requests\API;

use App\Helpers\APIHelper;
use App\Rules\ArrayInput;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SaveInterestsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'interests' => ['nullable', 'array', new ArrayInput("interests")]
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = APIHelper::errorsResponse($errors);
        throw new HttpResponseException(response()->json($message, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
