<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VideoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('videos.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        return [
            'title' => 'required',
            'url' => 'nullable|video_url|required_without:video_file',
            'video_file' => 'nullable|mimes:mp4|max:512000|required_without:url',
            'video_thumbnail' => 'image|dimensions:max_width=2000,ratio=2,min_width=1000|max:4096',
        ];
    }

    public function messages()
    {
        return [
            'video_thumbnail.required' => 'Video Thumbnail is required.',
            'video_thumbnail.image' => 'File must be image file',
            'video_thumbnail.dimensions' => 'Video Thumbnail dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=2:1',
            'video_thumbnail.max' => 'Video Thumbnail maximum size must be 4MB',
        ];
    }
}
