<?php

namespace App\Http\Requests\CMS;

use App\Rules\AnotherValueInArray;
use Illuminate\Foundation\Http\FormRequest;

class PushNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('push_notifications.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $redirect_type = request()->input("redirect_type");

        return [
            "message" => "required|max:200",
            "type" => "required|in:system,weather,offer,travel,tip",
            "id" => [new AnotherValueInArray($redirect_type)]
        ];
    }
}
