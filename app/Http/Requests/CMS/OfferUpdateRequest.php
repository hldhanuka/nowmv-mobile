<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class OfferUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('offers.update') || auth()->user()->hasPermissionTo('merchant_offer.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required|exists:offer_categories,id',
            //'amount' => 'required',
            'has_redeem' => 'required|in:1,0',
            'image' => 'image|dimensions:max_width=2000,ratio=1,min_width=1000|max:4096',
            'is_best' => 'required|in:1,0',
            'for_male' => 'required|in:1,0',
            'for_female' => 'required|in:1,0',
            'min_age' => 'nullable|numeric|integer|min:0|max:120',
            'max_age' => 'nullable|numeric|integer|min:0|max:120|gte:min_age',
            'start_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'end_datetime' => 'nullable|date_format:Y-m-d H:i:s',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'type.required' => 'Type is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=1:1',
            'image.max' => 'Image maximum size must be 4MB',
 
        ];
    }
}
