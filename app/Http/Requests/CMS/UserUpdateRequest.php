<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('users.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();

        return [
            'merchant_ids' => [Rule::requiredIf(function () use ($request) {
                return $request->input('restaurants') || $request->input('restaurant_reviews') || $request->input('meal_categories') || $request->input('meals') || $request->input('meal_reviews') || $request->input('recipes') || $request->input('recipe_reviews') || $request->input('offer_categories') || $request->input('offers');
            })],
            'ngo_merchant_ids' => [Rule::requiredIf(function () use ($request) {
                return $request->input('ngos') || $request->input('donations') || $request->input('badges');
            })],
            'news_agency_ids' => [Rule::requiredIf(function () use ($request) {
                return $request->input('news_categories') || $request->input('news_tags') || $request->input('news');
            })],
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
