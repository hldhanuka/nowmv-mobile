<?php

namespace App\Http\Requests\CMS;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('events.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'date' => 'required|after_or_equal:'.Carbon::now()->toDateString(),
            'time' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'image' => 'image|dimensions:max_width=2000,ratio=2,min_width=1000|max:4096'

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Title is required',
            'type.required'  => 'Type is required',
            'date.required'  => 'Date is required',
            'date.after_or_equal'  => 'Invalid Date',
            'time.required'  => 'Time is required',
            'longitude.required'  => 'Longitude is required',
            'latitude.required'  => 'Latitude is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=2:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];

    }
}
