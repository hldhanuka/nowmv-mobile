<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class DonationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('donations.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'max_limit' => 'required',
            'ngo_id' => 'required|exists:ngos,id',
            'tag' => 'required|max:15',
            'image' => 'image|dimensions:max_width=2000,ratio=1.5,min_width=1000|max:4096',

        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'max_limit.required' => 'Donation maximum limit is required',
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=3:2',
            'image.max' => 'Image maximum size must be 4MB',
        ];

    }
}
