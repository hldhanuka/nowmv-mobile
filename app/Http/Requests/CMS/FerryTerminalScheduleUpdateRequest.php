<?php

namespace App\Http\Requests\CMS;

use App\Helpers\CMSHelper;
use Illuminate\Foundation\Http\FormRequest;

class FerryTerminalScheduleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('ferry_terminal_schedules.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $island_list = CMSHelper::getFerryLocations()->implode('island_id', ',');
        return [
            'ferry_route_id' => 'required|exists:ferry_routes,id',
            'start_location_id' => 'required|in:'.$island_list,
            'terminal_id' => 'required|in:'.$island_list.'|different:from',
            'status' => 'required|boolean',
            'time' => 'required|date_format:H:i',
        ];
    }

    public function messages()
    {
        return [
            'time.date_format' => 'Invalid time format'
        ];

    }
}
