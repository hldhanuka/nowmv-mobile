<?php

namespace App\Http\Requests\CMS;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('users.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();

        return [
            'type' => 'required|in:admin,news_agent,merchant,ngo_merchant,custom',
            'merchant_ids' => [Rule::requiredIf(request()->input('type') == 'merchant'), Rule::requiredIf(function () use ($request) {
                return $request->input('restaurants') || $request->input('restaurant_reviews') || $request->input('meal_categories') || $request->input('meals') || $request->input('meal_reviews') || $request->input('recipes') || $request->input('recipe_reviews') || $request->input('offer_categories') || $request->input('offers');
            })],
            'ngo_merchant_ids' => [Rule::requiredIf(request()->input('type') == 'ngo_merchant'), Rule::requiredIf(function () use ($request) {
                return $request->input('ngos') || $request->input('donations') || $request->input('badges');
            })],
            'news_agency_ids' => [Rule::requiredIf(request()->input('type') == 'news_agent'), Rule::requiredIf(function () use ($request) {
                return $request->input('news_categories') || $request->input('news_tags') || $request->input('news');
            })],
            'image' => 'required|image|dimensions:ratio=1,min_width=100|max:4096',
            'first_name' => 'nullable',
            'last_name' => 'nullable',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'mobile' => 'required|numeric|unique:users,mobile',
            'address' => 'required|string',
            'birthday' => 'required|date|before_or_equal:' . Carbon::now()->toDateString(),
            'gender' => 'required|in:male,female',
        ];
    }
    public function messages()
    {
        return [
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 100px, Ratio=1:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }
}
