<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MealCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('meals.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required|exists:meal_categories,id',
            'restaurant_id' => 'required|exists:restaurants,id',
            'price' => 'required',
            'is_featured' => 'required|in:1,0',
            'image' => ['nullable','image','dimensions:max_width=2000,ratio=2,min_width=1000','max:4096',Rule::requiredIf(request()->input('is_featured')==1)]
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'category_id.required' => 'Category ID is required',
            'restaurant_id.required' => 'Restaurant ID is required',
            'price.required' => 'Price is required',
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=2:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }
}
