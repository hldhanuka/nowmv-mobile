<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class FerryLocationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('ferry_locations.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'status'=> 'required|boolean',
            'type'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'type.required'  => 'Type is required',
            'status.required'  => 'Status is required',
        ];
    }
}
