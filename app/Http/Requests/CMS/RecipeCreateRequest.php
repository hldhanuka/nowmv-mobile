<?php

namespace App\Http\Requests\CMS;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Foundation\Http\FormRequest;

class RecipeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('recipes.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'image' => 'required|image|dimensions:max_width=2000,ratio=2,min_width=1000|max:4096',
            'video' => 'nullable|video_url',
            'video_file' => 'nullable|mimes:mp4|max:512000',
            'link' => 'nullable|url',
            'ingredients' => 'required|array',
            'directions' => 'required|array',
        ];
    }

    protected function validationData()
    {
        $this->request->add(['ingredients' => CMSHelper::createItemArray($this->request->get('ingredients'))]);
        $this->request->add(['directions' => CMSHelper::createItemArray($this->request->get('directions'))]);
        return $this->all();
    }
    public function messages()
    {
        return [
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=2:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }
}
