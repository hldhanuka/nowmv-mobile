<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class NgoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('ngos.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required",
            "ngo_merchant_id" => "required|exists:ngo_merchants,id",
            'max_limit' => 'required',
            "logo" => "required|image|dimensions:max_width=2000,ratio=1,min_width=100|max:4096",
            "images" => "required|array",
            "images.*" => "required|image|dimensions:max_width=2000,ratio=1.5,min_width=1000|max:4096",
            "content" => "required",
            'tag' => 'required|max:15',
            "status" => "required|boolean"
        ];
    }

    public function messages()
    {
        return [
            'images.required' => 'At least one image is required',
            'images.*.image' => 'Image must be a image file',
            'images.*.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=3:2',
            'images.*.max' => 'Image file must not larger than 4MB',
            'logo.required' => 'Logo is required',
            'logo.image' => 'File must be image file',
            'logo.dimensions' => 'Logo dimensions not matched. Maximum width =  2000px, Minimum width = 100px, Ratio=1:1',
            'logo.max' => 'Logo maximum size must be 4MB',
        ];
    }
}
