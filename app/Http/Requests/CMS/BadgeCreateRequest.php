<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class BadgeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('badges.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'nullable',
            'image' => 'required|image|dimensions:max_width=2000,ratio=1,min_width=100|max:4096',
            'type' => 'required|in:ngo,donation',
            'ngo_id' => 'required_if:type,ngo',
            'donation_id' => 'required_if:type,donation',
            'winning_limit' => 'required|numeric'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 100px, Ratio=1:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }

}
