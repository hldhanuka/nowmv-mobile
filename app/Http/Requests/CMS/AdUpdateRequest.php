<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class AdUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('ads.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'type' => 'required',
            'image' => 'image|dimensions:max_width=2000,ratio=2,min_width=1000|max:4096'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'type.required' => 'Type is required',
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=2:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }
}
