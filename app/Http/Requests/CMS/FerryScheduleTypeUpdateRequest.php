<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class FerryScheduleTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('schedule_types.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'priority' => 'required|integer|min:1',
            'date_array' => 'required',
        ];
    }

    public function messages()
    {
        return [
//            'date.*.required' => 'Date field is required',
//            'date.*.date' => 'Invalid date format',
//            'title.*.required' => 'Title is required',
//            'title.*.max' => 'Max 30 characters',
        ];
    }
}
