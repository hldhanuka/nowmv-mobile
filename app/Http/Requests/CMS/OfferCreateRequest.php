<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OfferCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('offers.create') || auth()->user()->hasPermissionTo('merchant_offer.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        $val = [ 'title' => 'required',
            'category_id' => 'required|exists:offer_categories,id',
            //'amount' => 'required',
            'has_redeem' => 'required|in:1,0',
            'image' => 'required|image|dimensions:max_width=2000,ratio=1,min_width=1000|max:4096',
            'is_best' => 'required|in:1,0',
            'for_male' => 'required|in:1,0',
            'for_female' => 'required|in:1,0',
            'min_age' => 'nullable|numeric|integer|min:0|max:120',
            'max_age' => 'nullable|numeric|integer|min:0|max:120|gte:min_age',
            'start_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'end_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'push_message' =>  ['nullable', Rule::requiredIf(function () use ($request) {
                return ($request->input('is_send_push_notification') == "1");
            })],
            'mobile_numbers' => 'nullable',
            'mobile_numbers_csv' => 'nullable','mimes:csv,xls,xlsx'

        ];

        if($request->input('is_target_offer') == "1"){
            if($request->input('mobile_numbers') == null && !$request->hasFile('mobile_numbers_csv')){
                $val['mobile_numbers'] = 'required';
            }
            elseif($request->input('mobile_numbers') == null){
                $val['mobile_numbers_csv'] = 'required';
            }
            elseif (!$request->hasFile('mobile_numbers_csv')){
                $val['mobile_numbers'] = 'required';
            }
        }
        return $val;
    }

    public function messages()
    {
        $request = request();

        $msg = [
            'title.required' => 'Title is required',
            'type.required' => 'Type is required',
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=1:1',
            'image.max' => 'Image maximum size must be 4MB',
            'mobile_numbers.required' => 'At least one mobile number is required',
            'mobile_numbers_csv.required' => 'Mobile Numbers csv file is required',
        ];
        if($request->input('mobile_numbers') == null && !$request->hasFile('mobile_numbers_csv')){
            $msg['mobile_numbers.required'] = 'Either mobile numbers or csv file is required';
        }
        return $msg;
    }
}
