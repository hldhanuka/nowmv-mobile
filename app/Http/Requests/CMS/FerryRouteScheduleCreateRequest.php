<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class FerryRouteScheduleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('ferry_route_schedules.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'schedule_types' => 'required|array',
            'ferry_route_id' => 'required|exists:ferry_routes,id',
            'starting_time' => 'required|date_format:H:i',
            'status' => 'required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'starting_time.date_format' => 'Invalid time format'
        ];

    }
}
