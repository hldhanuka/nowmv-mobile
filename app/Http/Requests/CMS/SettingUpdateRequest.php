<?php

namespace App\Http\Requests\CMS;

use App\Rules\ValidVersionInput;
use Illuminate\Foundation\Http\FormRequest;

class SettingUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('settings.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ios_new_version" => "required",
            "ios_update_required" => "required|in:1,0",
            "android_new_version" => "required",
            "android_update_required" => "required|in:1,0",
            "radius" => "required|numeric",
            "greeting" => "required|string",
            "is_fasting_enable" => "required|in:1,0",
            "fasting_start_date" => "required|date|before_or_equal:fasting_end_date|date_format:Y-m-d",
            "fasting_end_date" => "required|date|date_format:Y-m-d",
            "donation_position" => "required|numeric|min:1",
            "food_position" => "required|numeric|min:1",
            "news_position" => "required|numeric|min:1",
            "offer_position" => "required|numeric|min:1",
            "android_dhiraagu_pay_url"=>"required|url",
            "ios_dhiraagu_pay_url"=>"required|url"
        ];
    }
}
