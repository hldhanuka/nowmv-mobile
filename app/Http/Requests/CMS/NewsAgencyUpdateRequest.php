<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class NewsAgencyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('news_agencies.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'logo' => 'image|dimensions:max_width=2000,ratio=1,min_width=100|max:4096',

        ];
    }

    public function messages()
    {
        return [
            'logo.required' => 'Logo is required',
            'logo.image' => 'File must be image file',
            'logo.dimensions' => 'Logo dimensions not matched. Maximum width =  2000px, Minimum width = 100px, Ratio=1:1',
            'logo.max' => 'Logo maximum size must be 4MB',
        ];
    }
}
