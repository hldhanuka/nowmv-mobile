<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NewsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('news.update') || auth()->user()->hasPermissionTo('agent_news.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        return [
            'title' => 'required',
            'author' => 'nullable',
            'category_id' => 'required|exists:news_categories,id',
            'news_agency_id' => 'required',
            'image' => 'nullable|dimensions:max_width=2000,ratio=3|image|max:4096',
            'video' => 'nullable|video_url',
            'video_file' => 'nullable|mimes:mp4|max:512000',
            'video_thumbnail' => ['nullable', 'image', 'max:4096', 'dimensions:max_width=2000,ratio=1.5', Rule::requiredIf(function () use ($request) {
                return ($request->input('video') != null && $request->input('old_video_thumbnail') == null);
            })],
            'link' => 'nullable|url',
            'content' => 'required',
            'is_english' => 'required|boolean',
            'is_featured' => 'required',
            'tags' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'category_id.required' => 'Category ID is required',
            'author.required' => 'Author is required',
            'is_featured.required' => 'Featured news is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=3:2',
            'image.max' => 'Image maximum size must be 4MB',
            'video_thumbnail.image' => 'File must be image file',
            'video_thumbnail.dimensions' => 'Video Thumbnail dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=3:2',
            'video_thumbnail.max' => 'Video Thumbnail maximum size must be 4MB',
        ];
    }
}
