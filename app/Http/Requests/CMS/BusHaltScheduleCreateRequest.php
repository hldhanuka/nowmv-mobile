<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class BusHaltScheduleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('bus_halt_schedules.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bus_route' => 'required',
            'start_location' => 'required',
            'halt' => 'required',
            'status' => 'required|boolean',
            'time' => 'required|date_format:H:i',
        ];
    }

    public function messages()
    {
        return [
            'time.date_format' => 'Invalid time format'
        ];

    }
}
