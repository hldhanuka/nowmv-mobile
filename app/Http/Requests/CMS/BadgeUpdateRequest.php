<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class BadgeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('badges.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'nullable',
            'type' => 'required|in:ngo,donation',
            'ngo_id' => 'required_if:type,ngo',
            'donation_id' => 'required_if:type,donation',
            'winning_limit' => 'required|numeric',
            'image' => 'image|dimensions:max_width=2000,ratio=1,min_width=100 |max:4096',

        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 100px, Ratio=1:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }
}
