<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class MealCategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('meal_categories.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'restaurant_id' => 'required|exists:restaurants,id',
            'image' => 'required|image|dimensions:max_width=2000,ratio=4,min_width=1000|max:4096',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'restaurant_id.required' => 'Restaurant ID is required',
            'image.required' => 'Image is required',
            'image.image' => 'File must be image file',
            'image.dimensions' => 'Image dimensions not matched. Maximum width =  2000px, Minimum width = 1000px, Ratio=4:1',
            'image.max' => 'Image maximum size must be 4MB',
        ];
    }
}
