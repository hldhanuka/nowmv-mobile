<?php

namespace App\Http\Requests\CMS;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Libraries\Dhathuru\DHATHURU;
use Illuminate\Foundation\Http\FormRequest;

class FerryRouteCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('ferry_routes.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $island_list = CMSHelper::getFerryLocations()->implode('island_id', ',');
        return [
            'name' => 'required',
//            'from' => 'required|exists:ferry_locations,id',
//            'to' => 'required|exists:ferry_locations,id|different:from',
            'from' => 'required|in:'.$island_list,
            'to' => 'required|in:'.$island_list.'|different:from',
            'status' => 'required|boolean'
        ];
    }
}
