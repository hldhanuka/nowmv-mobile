<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\SubmitNotificationSettingsRequest;
use App\NotificationType;
use App\UserNotificationSetting;
use App\UserSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserNotificationSettingController extends Controller
{
    public function getUserNotificationSettings(Request $request)
    {
        try {
            $notification_types = NotificationType::with(["activeChildren"])->whereNull('parent_id')->active()->get();
            $this_user_notification_settings = UserNotificationSetting::where('user_id', APIHelper::loggedUser()->id)->get();
            $user_parent_notification_settings = [];
            foreach ($notification_types as $notification_type) {
                $user_child_notification_settings = [];
                if (count($notification_type->activeChildren)) {
                    foreach ($notification_type->activeChildren as $child_notification_type) {
                        $this_user_child_notification_settings = $this_user_notification_settings->where('user_id', APIHelper::loggedUser()->id)->where("type_id", $child_notification_type->id)->first();
                        if ($this_user_child_notification_settings != null) {
                            array_push($user_child_notification_settings, $this_user_child_notification_settings);
                        }
                    }
                }
                $this_user_notification_settings = $this_user_notification_settings->where('user_id', APIHelper::loggedUser()->id)->where("type_id", $notification_type->id)->first();

                if ($this_user_notification_settings != null) {
                    $this_user_notification_settings->children = $user_child_notification_settings;
                    array_push($user_parent_notification_settings, $this_user_notification_settings);
                }
            }
            return APIHelper::makeAPIResponse(true, "Done", $user_parent_notification_settings, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitNotificationSettings(SubmitNotificationSettingsRequest $request)
    {
        try {
            $data = $request->input('data');
            $notification_types = NotificationType::with('children')->active()->get();

            foreach ($data as $result) {
                $notification_type = $notification_types->where('id', $result['type_id'])->first();
                UserNotificationSetting::updateOrCreate(['user_id' => APIHelper::loggedUser()->id, 'type_id' => $result['type_id']], ['status' => $result['status'], 'title' => $notification_type['title']]);
            }

            return APIHelper::makeAPIResponse(true, "Successfully submitted", null, 200);

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

}
