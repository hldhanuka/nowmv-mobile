<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\Helpers\APIHelper;
use App\Http\Requests\API\SetBusReminderRequest;
use App\Http\Requests\API\SetEventReminderRequest;
use App\Http\Requests\API\SetFlightReminderRequest;
use App\Http\Requests\API\SetPrayerReminderRequest;
use App\ReminderNotification;
use App\UserSetting;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ReminderNotificationController extends Controller
{
    public function getPrayerReminder(Request $request)
    {
        try {
            $user_setting = UserSetting::where('user_id', APIHelper::loggedUser()->id)->first();
            if ($user_setting != null && $user_setting->prayer_reminder != null) {
                $prayer_reminder_settings = json_decode($user_setting->prayer_reminder, true);
                return APIHelper::makeAPIResponse(true, "Done", $prayer_reminder_settings, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not Found or settings not found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function setPrayerReminder(SetPrayerReminderRequest $request)
    {
        try {

            $user = APIHelper::loggedUser();
            $user_id = $user->id;
            $user_prayer_location_id = $user->prayer_location_id;

            // user settings update
            UserSetting::where("user_id", $user_id)->update(["prayer_reminder" => json_encode($request->input())]);

            if ($user_prayer_location_id != null) {
                $is_active = APIHelper::getUserOrUsersByActiveSetting("prayer_reminder", $user_id);
                if ($is_active) {
                    $fajr = $request->input("fajr");
                    $dhuhr = $request->input("dhuhr");
                    $asr = $request->input("asr");
                    $maghrib = $request->input("maghrib");
                    $isha = $request->input("isha");

                    $today = Carbon::now();

                    // Delete previous all prayer data
                    ReminderNotification::where(["user_id" => $user_id, "type" => "prayer"])->delete();

                    $all_reminder_notifications = [];

                    $prayer_mate_data = APIHelper::getPrayerMateTimeByDateAndUser($today, $user, true);
                    if ($prayer_mate_data != null) {
                        if (in_array(strtolower($today->englishDayOfWeek), $fajr)) {
                            $prayer_mate_time = Carbon::parse($today->toDateString() . " " . $prayer_mate_data["fajr"]["time"]);
                            if ($prayer_mate_time->isFuture()) {
                                $temp_data = $this->getPrayerTempDataArray($prayer_mate_time, $user_id, "fajr", $today, $prayer_mate_data["fajr"]["id"]);
                                array_push($all_reminder_notifications, $temp_data);
                            }
                        }
                        if (in_array(strtolower($today->englishDayOfWeek), $dhuhr)) {
                            $prayer_mate_time = Carbon::parse($today->toDateString() . " " . $prayer_mate_data["dhuhr"]["time"]);
                            if ($prayer_mate_time->isFuture()) {
                                $temp_data = $this->getPrayerTempDataArray($prayer_mate_time, $user_id, "dhuhr", $today, $prayer_mate_data["dhuhr"]["id"]);
                                array_push($all_reminder_notifications, $temp_data);
                            }
                        }
                        if (in_array(strtolower($today->englishDayOfWeek), $asr)) {
                            $prayer_mate_time = Carbon::parse($today->toDateString() . " " . $prayer_mate_data["asr"]["time"]);
                            if ($prayer_mate_time->isFuture()) {
                                $temp_data = $this->getPrayerTempDataArray($prayer_mate_time, $user_id, "asr", $today, $prayer_mate_data["asr"]["id"]);
                                array_push($all_reminder_notifications, $temp_data);
                            }
                        }
                        if (in_array(strtolower($today->englishDayOfWeek), $maghrib)) {
                            $prayer_mate_time = Carbon::parse($today->toDateString() . " " . $prayer_mate_data["maghrib"]["time"]);
                            if ($prayer_mate_time->isFuture()) {
                                $temp_data = $this->getPrayerTempDataArray($prayer_mate_time, $user_id, "maghrib", $today, $prayer_mate_data["maghrib"]["id"]);
                                array_push($all_reminder_notifications, $temp_data);
                            }
                        }
                        if (in_array(strtolower($today->englishDayOfWeek), $isha)) {
                            $prayer_mate_time = Carbon::parse($today->toDateString() . " " . $prayer_mate_data["isha"]["time"]);
                            if ($prayer_mate_time->isFuture()) {
                                $temp_data = $this->getPrayerTempDataArray($prayer_mate_time, $user_id, "isha", $today, $prayer_mate_data["isha"]["id"]);
                                array_push($all_reminder_notifications, $temp_data);
                            }
                        }
                    }

                    $result = ReminderNotification::insert($all_reminder_notifications);
                    if ($result) {
                        return APIHelper::makeAPIResponse(true, "Prayer reminder set! You will be receiving an alert 5 mins prior to each prayer everyday (or on the set days)", 200);
                    } else {
                        return APIHelper::makeAPIResponse(false, "User prayer reminder error", null, 500);
                    }
                } else {
                    return APIHelper::makeAPIResponse(false, "You have disabled the prayer notification. Please active it first", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Your prayer location doesn't exist. Please update it first", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getPrayerTempDataArray($prayer_mate_time, $user_id, $type, $today, $relation_id = null)
    {
        $prayer_reminder_before = config('dhiraagu.prayer_reminder_before');
        $schedule_time = $prayer_mate_time->copy()->subMinutes($prayer_reminder_before)->toDateTimeString();
        $content = Str::ucfirst($type) . " prayer at " . APIHelper::setDhiraaguDateFormat($prayer_mate_time, "time");
        return [
            "type" => "prayer",
            "user_id" => $user_id,
            "content" => $content,
            "date_time" => $schedule_time,
            "relation_id" => $relation_id,
            "status" => 1,
            "created_at" => $today,
            "updated_at" => $today,
        ];
    }

    public function setFlightReminder(SetFlightReminderRequest $request)
    {
        $user = APIHelper::loggedUser();
        $user_id = $user->id;
        $is_active = APIHelper::getUserOrUsersByActiveSetting("flight", $user_id);
        if ($is_active) {
            $type = $request->input("type");
            $flight = $request->input("flight");
            $origin = $request->input("origin");
            $destination = $request->input("destination");
            $date_time = Carbon::parse($request->input("date_time"));
            $before_3_hour = $request->input("before_3_hour");
            $before_1_hour = $request->input("before_1_hour");
            $before_30_min = $request->input("before_30_min");
            $before_15_min = $request->input("before_15_min");

            if ($before_3_hour) {
                $temp_data = $this->getFlightTempDataArray($date_time->subHours(3), $user_id, $type, $flight, $origin, $destination, "3 hours");
            } else if ($before_1_hour) {
                $temp_data = $this->getFlightTempDataArray($date_time->subHours(1), $user_id, $type, $flight, $origin, $destination, "1 hour");
            } else if ($before_30_min) {
                $temp_data = $this->getFlightTempDataArray($date_time->subMinutes(30), $user_id, $type, $flight, $origin, $destination, "30 mins");
            } else if ($before_15_min) {
                $temp_data = $this->getFlightTempDataArray($date_time->subMinutes(15), $user_id, $type, $flight, $origin, $destination, "15 mins");
            } else {
                $temp_data = [];
            }

            $result = ReminderNotification::insert($temp_data);
            if ($result) {
                return APIHelper::makeAPIResponse(true, "Done", 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User flight reminder error", null, 500);
            }

        } else {
            return APIHelper::makeAPIResponse(false, "You have disabled the flight notifications. Please active it first", null, 400);
        }
    }

    public function getFlightTempDataArray($schedule_time, $user_id, $type, $flight, $origin, $destination, $time_gap)
    {
        $now = Carbon::now();
        if ($type == "departure") {
            $content = "Flight departure; " . $flight . " to " . $destination . " departing in " . $time_gap;
        } else {
            $content = "Flight arrival; " . $flight . " from " . $origin . " arriving in " . $time_gap;
        }

        return [
            "type" => "flight",
            "user_id" => $user_id,
            "content" => $content,
            "date_time" => $schedule_time,
            "status" => 1,
            "created_at" => $now,
            "updated_at" => $now,
        ];
    }

    public function setBusReminder(SetBusReminderRequest $request)
    {
        $user = APIHelper::loggedUser();
        $user_id = $user->id;
        $is_active = APIHelper::getUserOrUsersByActiveSetting("bus", $user_id);
        if ($is_active) {
            $date_time = Carbon::parse($request->input("date_time"));
            $from = $request->input("from");
            $to = $request->input("to");
            $before_30_min = $request->input("before_30_min");
            $before_15_min = $request->input("before_15_min");
            $before_5_min = $request->input("before_5_min");

            if ($before_30_min) {
                $temp_data = $this->getBusTempDataArray($date_time->subMinutes(30), $user_id, $from, $to, "30 mins");
            } else if ($before_15_min) {
                $temp_data = $this->getBusTempDataArray($date_time->subMinutes(15), $user_id, $from, $to, "15 mins");
            } else if ($before_5_min) {
                $temp_data = $this->getBusTempDataArray($date_time->subMinutes(5), $user_id, $from, $to, "5 mins");
            } else {
                $temp_data = [];
            }

            $result = ReminderNotification::insert($temp_data);
            if ($result) {
                return APIHelper::makeAPIResponse(true, "Done", 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User bus reminder error", null, 500);
            }

        } else {
            return APIHelper::makeAPIResponse(false, "You have disabled the bus notifications. Please active it first", null, 400);
        }
    }

    public function getBusTempDataArray($schedule_time, $user_id, $from, $to, $time_gap)
    {
        $now = Carbon::now();
        $content = "Your bus from " . $from . " to " . $to . " arriving in " . $time_gap;

        return [
            "type" => "bus",
            "user_id" => $user_id,
            "content" => $content,
            "date_time" => $schedule_time,
            "status" => 1,
            "created_at" => $now,
            "updated_at" => $now,
        ];
    }

    public function setEventReminder(SetEventReminderRequest $request)
    {
        $user = APIHelper::loggedUser();
        $user_id = $user->id;
        $is_active = APIHelper::getUserOrUsersByActiveSetting("event", $user_id);
        if ($is_active) {
            $event_id = $request->input("event_id");
            $date_time = Carbon::parse($request->input("date_time"));
            $before_1_month = $request->input("before_1_month");
            $before_1_week = $request->input("before_1_week");
            $before_1_day = $request->input("before_1_day");

            if ($before_1_month) {
                $temp_data = $this->getEventTempDataArray($date_time->copy()->subMonths(1), $user_id, $event_id, APIHelper::setDhiraaguDateFormat($date_time, "datetime"));
            } else if ($before_1_week) {
                $temp_data = $this->getEventTempDataArray($date_time->copy()->subWeeks(1), $user_id, $event_id, APIHelper::setDhiraaguDateFormat($date_time, "datetime"));
            } else if ($before_1_day) {
                $temp_data = $this->getEventTempDataArray($date_time->copy()->subDays(1), $user_id, $event_id, APIHelper::setDhiraaguDateFormat($date_time, "datetime"));
            } else {
                $temp_data = [];
            }
            ReminderNotification::where('user_id', APIHelper::loggedUser()->id)->where('type', 'event')->where('relation_id', $event_id)->delete();
            $result = ReminderNotification::insert($temp_data);
            if ($result) {
                return APIHelper::makeAPIResponse(true, "Done", 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User event reminder error", null, 500);
            }

        } else {
            return APIHelper::makeAPIResponse(false, "You have disabled the event notifications. Please active it first", null, 400);
        }
    }

    public function getEventTempDataArray($schedule_time, $user_id, $event_id, $date_time)
    {
        $now = Carbon::now();

        $event = Event::find($event_id);
        if($event != null){
            $content = "Your have an upcoming event, \"".$event->name."\" on " . $date_time;
        } else {
            $content = "Your have an upcoming event, on " . $date_time;
        }

        return [
            "type" => "event",
            "user_id" => $user_id,
            "content" => $content,
            "date_time" => $schedule_time,
            "relation_id" => $event_id,
            "status" => 1,
            "created_at" => $now,
            "updated_at" => $now,
        ];
    }
}
