<?php

namespace App\Http\Controllers\API;

use App\Ad;
use App\Event;
use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\SubmitHomeScreenOrderRequest;
use App\Http\Requests\API\SubmitPrayerTrackerSettingsRequest;
use App\Libraries\Weather\WEATHER;
use App\News;
use App\Setting;
use App\UserSetting;
use App\UserWeatherLocation;
use App\Video;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function getHomePageData(Request $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $user_id = $user->id;

            if ($user != null) {
                $greeting = Setting::where('title', 'greeting')->first()->text;

                $first_name = $user->first_name != null ? $user->first_name : $user->mobile;

                $featured_news = News::where('is_featured', 1)->active()->orderBy('published_at', 'DESC')->limit(4)->get();

                $latest_news = News::orderBy('published_at', 'DESC')->active()->limit(3)->get();

                $ads = Ad::home()->active()->inRandomOrder()->limit(1)->get();

                $user_weather_data = null;
                $user_first_weather_location = UserWeatherLocation::where("user_id", $user_id)->first();

                if ($user_first_weather_location != null) {
                    try{
                        $closest_weather_location_id = APIHelper::getNearestWeatherLocationID($user->latitude, $user->longitude);

                        $url = str_replace("{id}", $closest_weather_location_id, config('dhiraagu.weather_data.station_details'));

                        $user_weather_data = WEATHER::apiCall($url);
                        $user_weather_data = APIHelper::addWeatherImage($user_weather_data);
                        $user_live_location = APIHelper::getMyNearestPrayerLocation($user->latitude, $user->longitude);

                        $user_weather_data["name"] = $user_live_location->island;
                    } catch (\Exception $e) {
                        report($e);

                        $user_weather_data = null;
                    }
                }

                $events = Event::with('tag')->withCount('favourites')
                    ->active()->public()
//                    ->orWhere('created_by', $user_id)
//                    ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
//                        $query->where('invitee_id', $user_id);
//                    })
                    ->latest()->limit(4)->get();

                $videos = Video::active()->latest()->limit(4)->get();

                $prayer_tracker = APIHelper::getPrayerMateTimeByDateAndUser(Carbon::now(), $user, true);

                $next_prayer_time = APIHelper::getNextPrayerTime(Carbon::now(), $user, $prayer_tracker);

                $is_enable_prayer_tracker = $user->userSetting->enable_prayer_tracker;

                //connect imi games to home page
                try{
                    $url = "https://api-saas.staging.imigames.io/api/v1/ag/game?page=0&size=4";

                    $request = [
                        'headers' => [
                            'Authorization' => $request->header()["authorization"][0],
                            'x-platform' => $request->header()["x-oauth-client-name"][0],
                            'version' => $request->header()["x-oauth-client-version"][0]
                        ]
                    ];

                    $client = new Client(['http_errors' => false]);

                    $response = $client->request("GET", $url, $request);

                    $response = json_decode($response->getBody(), true);

                    $games =    $response["data"];
                } catch (\Exception $e) {
                    report($e);

                    $games = [];
                }

                $data = [
                    'greeting' => $greeting,
                    'first_name' => $first_name,
                    'featured_news' => $featured_news,
                    'latest_news' => $latest_news,
                    'ads' => $ads,
                    'weather' => $user_weather_data,
                    'events' => $events,
                    'videos' => $videos,
                    'next_prayer_time' => $next_prayer_time,
                    'prayer_tracker' => $prayer_tracker,
                    'is_enable_prayer_tracker' => $is_enable_prayer_tracker,
                    'games' =>$games,
                ];

                return APIHelper::makeAPIResponse(true, "Done", $data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not found. Please register first", null, 400);
            }
        } catch (\Exception $e) {
            dd($e);

            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitHomeScreenOrder(SubmitHomeScreenOrderRequest $request)
    {

        try {
            $data = $request->input('data');
            $result = UserSetting::updateOrCreate(['user_id' => APIHelper::loggedUser()->id], ['screen_order' => json_encode($data)]);
            if ($result) {
                return APIHelper::makeAPIResponse(true, "Successfully submitted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service error", null, 500);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getHomeScreenOrder(Request $request)
    {
        try {
            $user_setting = UserSetting::where('user_id', APIHelper::loggedUser()->id)->first();
            if ($user_setting != null) {
                $data = json_decode($user_setting->screen_order);
                return APIHelper::makeAPIResponse(true, "Done", $data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not Found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getPrayerTrackerSettings(Request $request)
    {
        try {
            $user_setting = UserSetting::where('user_id', APIHelper::loggedUser()->id)->first();
            if ($user_setting != null) {
                $data = ["is_enable" => $user_setting->enable_prayer_tracker];
                return APIHelper::makeAPIResponse(true, "Done", $data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not Found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitPrayerTrackerSettings(SubmitPrayerTrackerSettingsRequest $request)
    {
        try {
            $user_setting = UserSetting::where('user_id', APIHelper::loggedUser()->id)->first();
            if ($user_setting != null) {
                $user_setting->enable_prayer_tracker = $request->input("is_enable");
                $user_setting->save();
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not Found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}

