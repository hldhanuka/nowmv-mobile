<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\AddWeatherLocationRequest;
use App\Http\Requests\API\RemoveWeatherLocationRequest;
use App\Libraries\Weather\WEATHER;
use App\UserBadge;
use App\UserWeatherLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserWeatherLocationController extends Controller
{
    public function getUserWeatherLocations(Request $request)
    {
        try {
            $weather_locations = UserWeatherLocation::where('user_id', APIHelper::loggedUser()->id)->get();
            $location_data = [];
            foreach ($weather_locations as $weather_location){
                $url = str_replace("{id}", $weather_location->location_id, config('dhiraagu.weather_data.station_details'));
                $data = WEATHER::apiCall($url);
                $data = APIHelper::addWeatherImage($data);
                $location_data[] = $data;
            }
            return APIHelper::makeAPIResponse(true, "Done", $location_data, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function addWeatherLocation(AddWeatherLocationRequest $request)
    {
        try {
            $location_id = $request->input("location_id");
            DB::beginTransaction();
            $user_weather_location = UserWeatherLocation::updateOrCreate(['user_id' => APIHelper::loggedUser()->id, "location_id" => $location_id]);
            DB::commit();
            if ($user_weather_location) {
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service error", null, 500);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function removeWeatherLocation(RemoveWeatherLocationRequest $request)
    {
        try {
            $location_id = $request->input("location_id");
            DB::beginTransaction();
            UserWeatherLocation::where(['user_id' => APIHelper::loggedUser()->id, "location_id" => $location_id])->delete();
            DB::commit();
            return APIHelper::makeAPIResponse(true, "Done", null, 200);

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
