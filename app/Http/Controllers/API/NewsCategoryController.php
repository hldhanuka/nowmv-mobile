<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\NewsCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsCategoryController extends Controller
{
    public function getAllNewsCategories(Request $request)
    {
        try {
            $news_categories = NewsCategory::whereHas('news',function($query){
                $query->active();
            })->active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $news_categories, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getNewsCategoryById(Request $request, $id)
    {
        try {
            $news_category = NewsCategory::where('id', $id)->active()->first();
            if ($news_category != null) {
                return APIHelper::makeAPIResponse(true, "Done", $news_category, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "News Category not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
