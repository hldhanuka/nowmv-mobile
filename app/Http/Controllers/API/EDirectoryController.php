<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EDirectoryController extends Controller
{
    public function getEDirectory(Request $request){
        try {
            $mobile = APIHelper::loggedUser()->mobile;

            if ($mobile !=null) {
                $response = DHIRAAGU::apiCall('GET', APIHelper::getDhiraaguURL('e_directory', '{serviceNo}', $mobile), null, "json");

                if (config('dhiraagu.bypass_service') || $response["success"] && isset($response["body"])) {

                    return APIHelper::makeAPIResponse(true, "Done", $response["body"], 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Dhiraagu service error", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "User mobile number not found. Please update profile", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }
}
