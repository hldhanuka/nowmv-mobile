<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Helpers\APIHelper;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class SettingsController extends Controller
{
    public function checkVersion(Request $request)
    {
        try {
            $inputs = $request->input();
            $setting = Setting::where('title', 'app_settings')->pluck('text')->first();
            $data = json_decode($setting, true);
            $result = Arr::only($data[$inputs["client"]], ['new_version', 'update_required']);

            if (version_compare($inputs["current_version"], $result["new_version"]) < 0) {
                $result["update_available"] = 1;
            } else {
                $result["update_available"] = 0;
            }

            $is_fasting_enable = Setting::where('title', 'is_fasting_enable')->pluck('text')->first();
            $result["is_fasting_enable"] = intval($is_fasting_enable);
            $fasting_start_date = Setting::where('title', 'fasting_start_date')->pluck('text')->first();
            $result["fasting_start_date"] = $fasting_start_date;
            $fasting_end_date = Setting::where('title', 'fasting_end_date')->pluck('text')->first();
            $result["fasting_end_date"] = $fasting_end_date;

            return APIHelper::makeAPIResponse(true, "Done", $result, 200);

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null,500);
        }

    }

}
