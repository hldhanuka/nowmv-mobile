<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    public function getVideosList(Request $request)
    {
        try {
            $videos = Video::active()->latest()->get();
            return APIHelper::makeAPIResponse(true, "Done", $videos, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
