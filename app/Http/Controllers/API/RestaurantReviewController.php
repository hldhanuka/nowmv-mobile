<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\RemoveRestaurantReviewRequest;
use App\Http\Requests\API\SubmitRestaurantReviewRequest;
use App\Http\Requests\API\UpdateRestaurantReviewRequest;
use App\Restaurant;
use App\RestaurantReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantReviewController extends Controller
{

    public function submitRestaurantReview(SubmitRestaurantReviewRequest $request)
    {
        try{
            $restaurant_id = $request->input("restaurant_id");
            $comment = $request->input("comment");
            $restaurant = Restaurant::active()->find($restaurant_id);
            if($restaurant!=null){
                RestaurantReview::insert(['restaurant_id'=>$restaurant_id,'comment'=>$comment, 'status'=>1, 'created_by'=>APIHelper::loggedUser()->id]);
                return APIHelper::makeAPIResponse(true, "Successfully submitted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false,"Restaurant not found", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updateRestaurantReview(UpdateRestaurantReviewRequest $request)
    {
        try {
            $id = $request->input("id");
            $comment = $request->input("comment");
            $restaurant_review = RestaurantReview::where('created_by', APIHelper::loggedUser()->id)->find($id);
            if ($restaurant_review != null) {
                $restaurant_review->update(['comment' => $comment]);
                return APIHelper::makeAPIResponse(true, "Successfully updated", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Restaurant review not found or not yours", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function removeRestaurantReview(RemoveRestaurantReviewRequest $request)
    {
        try {
            $id = $request->input("id");
            $restaurant_review = RestaurantReview::where('created_by', APIHelper::loggedUser()->id)->find($id);
            if ($restaurant_review != null) {
                $restaurant_review->delete();
                return APIHelper::makeAPIResponse(true, "Successfully deleted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Restaurant review not found or not yours", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
