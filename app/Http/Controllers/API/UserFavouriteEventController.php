<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\User;
use App\UserFavouriteEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserFavouriteEventController extends Controller
{
    public function userFavourites(Request $request)
    {
        try {
            $user_favourite_events = UserFavouriteEvent::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $user_favourite_events, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getUserFavouriteEventById(Request $request, $id)
    {
        try {
            $user_favourite_event= UserFavouriteEvent::where('id', $id)->active()->first();
            if ($user_favourite_event != null) {
                return APIHelper::makeAPIResponse(true, "Done", $user_favourite_event, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User Favourite Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
