<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\MtccGetAvailableBusesRequest;
use App\Libraries\MPL\MPL;
use App\MtccRoute;
use App\MtccRouteStop;
use App\MtccRouteStopDay;
use App\MtccRouteStopTime;
use App\MtccServicePoint;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MtccBusController extends Controller
{
    public function getAllLocations(Request $request)
    {
        try {
            $all_mtcc_bus_locations = MtccServicePoint::select(['id', 'name'])->where("type", "bus_stop")->get()->toArray();
            $all_mpl_bus_locations = MPL::getAllStops();
            $all_bus_locations = array_merge($all_mtcc_bus_locations, $all_mpl_bus_locations);
            $all_bus_locations = collect($all_bus_locations)->sortBy("name")->values()->all();
            return APIHelper::makeAPIResponse(true, "Done", $all_bus_locations, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getAvailableBuses(MtccGetAvailableBusesRequest $request)
    {
        try {
            $now = Carbon::now();
            $current_date = $now->copy()->toDateString();
            $today_is_holiday = APIHelper::checkDateIsHoliday($current_date);
            $from = $request->input("from");
            $to = $request->input("to");

            $final_schedule_data = [];

            if (preg_match("/[a-z]/i", $from) && preg_match("/[a-z]/i", $to)) {
                // MPL
                $mpl_all_stops = MPL::getAllStops();
                $mpl_data = MPL::getSchedule($from, $to);
                if (count($mpl_data)) {
                    // Schedule exists
                    $route_name = $mpl_data["name"] ?? "";
                    $all_stops = $mpl_data["stops"] ?? [];
                    $all_schedules = $mpl_data["schedules"] ?? [];
                    $from_object = collect($mpl_all_stops)->where("id", $from)->first();
                    $to_object = collect($mpl_all_stops)->where("id", $to)->first();

                    $final_stops = [];
                    foreach ($all_stops as $stop) {
                        $temp_stop = [
                            "name" => $stop["name"],
                            "schedule_time" => null,
                            "is_current" => $stop["id"] == $from
                        ];
                        array_push($final_stops, $temp_stop);
                    }

                    foreach ($all_schedules as $schedule) {
                        $schedule_time = Carbon::parse($current_date . " " . $schedule["time"]);
                        // TODO check holiday
                        if ($schedule_time->isFuture()) {
                            $temp_schedule = [
                                "route_name" => $route_name,
                                "from_location" => $from_object["name"] ?? "",
                                "to_location" => $to_object["name"] ?? "",
                                "bus_type" => "MPL",
                                "logo" => config('dhiraagu.mpl_logo'),
                                "schedule_time" => $schedule_time->toDateTimeString(),
                                "schedule_data" => $final_stops
                            ];
                            array_push($final_schedule_data, $temp_schedule);
                        }
                    }
                } else {
                    return APIHelper::makeAPIResponse(false, "No Available Buses", null, 404);
                }
            } else {
                // MTCC
                $from_object = MtccServicePoint::where("id", $from)->where("type", "bus_stop")->first();
                $to_object = MtccServicePoint::where("id", $to)->where("type", "bus_stop")->first();

                if ($today_is_holiday) {
                    $short_english_day = "hol";
                } else {
                    $short_english_day = strtolower($now->shortEnglishDayOfWeek);
                }

                $route = MtccRoute::with(['routeStops', 'originServicePoint', 'destinationServicePoint'])->whereHas('routeStops', function ($query) use ($from) {
                    $query->where('originating_service_point', $from);
                })->whereHas('routeStops', function ($query) use ($to) {
                    $query->where('destination_service_point', $to);
                })->first();


                // check route exist
                if ($route != null) {

                    $route_stops_ids = $route->routeStops()->where('originating_service_point', $from)->pluck('id')->unique();

                    $all_route_stops_id = MtccRouteStop::select(["originating_service_point"])->distinct()->where("route_id", $route->id)->groupBy('originating_service_point')->orderBy('sequence')->pluck("originating_service_point")->toArray();
                    $all_halts = MtccServicePoint::whereIn("id", $all_route_stops_id)->where("type", "bus_stop")->get();

                    $final_stops = [];
                    foreach ($all_halts as $stop) {
                        $temp_stop = [
                            "name" => $stop["name"],
                            "schedule_time" => null,
                            "is_current" => $stop["id"] == $from
                        ];
                        $final_stops[] = $temp_stop;
                    }

                    $route_stop_days = MtccRouteStopDay::whereIn("route_stop_id", $route_stops_ids)->where("day_types", "like", "%" . $short_english_day . "%")->get()->pluck('id')->toArray();
                    $all_schedules = MtccRouteStopTime::whereIn("route_day_id", $route_stop_days)->get();

                    foreach ($all_schedules as $schedule) {
                        $schedule_time = Carbon::parse($current_date . " " . $schedule["departure_time"]);
                        if ($schedule_time->isFuture()) {
                            $temp_schedule = [
                                "route_name" => $route->name,
                                "from_location" => $from_object["name"] ?? "",
                                "to_location" => $to_object["name"] ?? "",
                                "bus_type" => "MPL",
                                "logo" => config('dhiraagu.mtcc_logo'),
                                "schedule_time" => $schedule_time->toDateTimeString(),
                                "schedule_data" => $final_stops
                            ];
                            $final_schedule_data[] = $temp_schedule;
                        }
                    }
                }

            }

            if (count($final_schedule_data)) {
                return APIHelper::makeAPIResponse(true, "Done", $final_schedule_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "No Available Buses", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
