<?php

namespace App\Http\Controllers\API;


use App\Helpers\APIHelper;
use App\OfferCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfferCategoryController extends Controller
{
    public function getOfferCategories(Request $request)
    {

        try {
            $offer_categories = OfferCategory::whereHas('offers',function($query){
                $query->active();
            })->active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $offer_categories, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", 500);
        }
    }
}
