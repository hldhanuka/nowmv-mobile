<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\UserBadge;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserBadgeController extends Controller
{
    public function userBadges(Request $request)
    {
        try {
            $user_badges = UserBadge::with(['badge','donation','ngo'])->where('user_id', APIHelper::loggedUser()->id)->active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $user_badges, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getUserBadgeById(Request $request, $id)
    {
        try {
            $user_badge = UserBadge::where('id', $id)->active()->first();
            if ($user_badge != null) {
                return APIHelper::makeAPIResponse(true, "Done", $user_badge, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User Badge not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
