<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Requests\API\SubmitOfferPaymentRequest;
use App\Http\Requests\API\VerifyOfferPaymentRequest;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\Offer;
use App\Setting;
use App\UserOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserOfferController extends Controller
{
    public function submitOfferPayment(SubmitOfferPaymentRequest $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $offer_id = $request->input('offer_id');
            $payment_method = $request->input('payment_method');
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $amount = floatval($request->input('amount'));
            $payment_invoice_number = round(microtime(true) * 1000) . "_" . $user_id . "_" . $offer_id;

            $offer = Offer::active()->find($offer_id);
            if ($offer != null) {

                if ($offer->merchant != null && $offer->merchant->key != null) {
                    DB::beginTransaction();
                    $user_offer = new UserOffer();
                    $user_offer->user_id = $user_id;
                    $user_offer->offer_id = $offer_id;
                    $user_offer->merchant_id = $offer->merchant->id;
                    $user_offer->payment_method = $payment_method;
                    $user_offer->status = 2;
                    $user_offer->amount = $amount;
                    $user_offer->mobile_number = $mobile;
                    $user_offer->payment_invoice_number = $payment_invoice_number;
                    $user_offer->save();
                    DB::commit();


                    $api_body = [
                        "username" => config('dhiraagu.static_api_data.mfs_username'),
                        "merchantKey" => config('dhiraagu.static_api_data.mfs_merchantKey'),
                        "originationNumber" => $offer->merchant->key,
                        "destinationNumber" => $mobile,
                        "amount" => $amount,
                        "paymentInvoiceNumber" => $payment_invoice_number,
                        "transactionDescription" => "Offer Payment"
                    ];

                    $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('payment_otp_request'), $api_body, "json");

                    if (config('dhiraagu.bypass_service') || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {

                        $user_offer->transaction_id = isset($response["body"]["transactionId"]) ? $response["body"]["transactionId"] : null;
                        $user_offer->result_data_reference_id = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["referenceId"])) ? $response["body"]["resultData"]["referenceId"] : null;
                        $user_offer->result_data_message = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["message"])) ? $response["body"]["resultData"]["message"] : null;
                        $user_offer->save();

                        $offer_response = [
                            "offer_payment_id" => $user_offer->id,
                            "payment_invoice_number" => $payment_invoice_number
                        ];

                        return APIHelper::makeAPIResponse(true, "Done", $offer_response, 200);
                    } else if (isset($response["body"]["resultData"]) &&
                        isset($response["body"]["resultData"]["message"]) &&
                        $response["body"]["resultData"]["message"] == "QUERYPROFILE:Destination number in wrong format(1137)") {
                        $android_dhiraagu_pay_url = Setting::where('title', 'android_dhiraagu_pay_url')->pluck('text')->first();
                        $ios_dhiraagu_pay_url = Setting::where('title', 'ios_dhiraagu_pay_url')->pluck('text')->first();
                        $data = [
                            "android_dhiraagu_pay_url" => $android_dhiraagu_pay_url != null ? $android_dhiraagu_pay_url : null,
                            "ios_dhiraagu_pay_url" => $ios_dhiraagu_pay_url != null ? $ios_dhiraagu_pay_url : null,
                        ];
                        return APIHelper::makeAPIResponse(false, "Please register on Dhiraagu pay", $data, 428);
                    } else {
                        return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
                    }
                } else if (isset($response["body"]["resultData"]) &&
                    isset($response["body"]["resultData"]["message"]) &&
                    $response["body"]["resultData"]["message"] == "QUERYPROFILE:Limit Check Failed(1140)") {
                    return APIHelper::makeAPIResponse(false, "Insufficient balance in your wallet to process this transaction", null, 500);
                } else if (isset($response["body"]["resultData"]) &&
                    isset($response["body"]["resultData"]["message"]) &&
                    $response["body"]["resultData"]["message"] == "QUERYPROFILE:Requested Service is not subscribed by the User(1061)") {
                    return APIHelper::makeAPIResponse(false, "Service not available", null, 500);
                } else {
                    return APIHelper::makeAPIResponse(false, "Offer has not assign to merchant or merchant origination number", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Offer is inactive", null, 400);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }

    public function verifyOfferPayment(VerifyOfferPaymentRequest $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $offer_payment_id = $request->input('offer_payment_id');
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $otp = trim($request->input('otp'));

            $user_offer = UserOffer::find($offer_payment_id);
            $offer = Offer::active()->find($user_offer->offer_id);

            if ($offer != null && $user_offer != null) {

                if ($user_offer->mobile_number != $mobile) {
                    return APIHelper::makeAPIResponse(false, "Invalid mobile number", null, 400);
                }

                if ($user_offer->status != 1) {
                    $api_body = [
                        "username" => config('dhiraagu.static_api_data.mfs_username'),
                        "merchantKey" => config('dhiraagu.static_api_data.mfs_merchantKey'),
                        "destinationNumber" => $mobile,
                        "transactionDescription" => "Offer Payment",
                        "referenceId" => $user_offer->result_data_reference_id,
                        "otpString" => $otp
                    ];

                    $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('payment_otp_verify'), $api_body, "json");

                    if ((config('dhiraagu.bypass_service') && ($otp == "1234" || $otp == "123456")) || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                        // Success payment
                        $user_offer->status = 1;
                        $user_offer->transaction_id = isset($response["body"]["transactionId"]) ? $response["body"]["transactionId"] : null;
                        $user_offer->result_data_reference_id = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["referenceId"])) ? $response["body"]["resultData"]["referenceId"] : null;
                        $user_offer->result_data_message = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["message"])) ? $response["body"]["resultData"]["message"] : null;
                        $user_offer->save();

                        $offer_response = [
                            "offer_payment_id" => $user_offer->id,
                            "message" => "Thank you for your payment",
                        ];

                        $amount = APIHelper::setAmountFormat($user_offer->amount);

                        // Push notification
                        $push_message = "You have payed MVR " . $amount . " to \"" . $offer->title . "\" on " . APIHelper::setDhiraaguDateFormat($user_offer->updated_at, "datetime");
                        $data = ["type" => "offer_home","id" => null];
                        OneSignalPush::sendPushNotification($push_message, "transaction", $data, 0, $user_id, null);

                        return APIHelper::makeAPIResponse(true, "Payment Success", $offer_response, 200);
                    } else {
                        return APIHelper::makeAPIResponse(false, "Invalid OTP code", null, 400);
                    }

                } else {
                    return APIHelper::makeAPIResponse(false, "Offer already paid", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Offer is inactive", null, 400);
            }


        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }

    public function getOfferHistory(Request $request)
    {
        try {
            $offer_history = UserOffer::with('offer')->select(['id', 'user_id', 'offer_id', 'payment_method', 'status', 'amount', 'mobile_number', 'payment_invoice_number', 'created_at', 'updated_at'])->where('user_id', APIHelper::loggedUser()->id)->active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $offer_history, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }
}
