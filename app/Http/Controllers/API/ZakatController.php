<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\SubmitZakatPaymentRequest;
use App\Http\Requests\API\VerifyZakatPaymentRequest;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\Setting;
use App\ZakatAtoll;
use App\ZakatCommodity;
use App\ZakatIsland;
use App\ZakatPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ZakatController extends Controller
{
    public function getAtollList(Request $request)
    {
        try {
            $data = ZakatAtoll::orderBy("name")->get();
            return APIHelper::makeAPIResponse(true, "Done", $data, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getIslandListByAtollID(Request $request, $id)
    {
        try {
            $atoll = ZakatAtoll::find($id);
            if ($atoll != null) {
                $data = ZakatIsland::where("atoll_id", $id)->orderBy("name")->get();
                return APIHelper::makeAPIResponse(true, "Done", $data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Atoll not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getCommodityList(Request $request)
    {
        try {
            $data = ZakatCommodity::orderBy("price")->get();
            return APIHelper::makeAPIResponse(true, "Done", $data, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitZakatPayment(SubmitZakatPaymentRequest $request)
    {
        try {
            // STOP ZAKAT PAYMENT
            return APIHelper::makeAPIResponse(false, "As per Islamic Ministry’s instructions, this year’s Fitr Zakat payment collection has been closed as of 6 pm on 12 May 2021", null, 500);

            $user_id = APIHelper::loggedUser()->id;
            $full_name = $request->input('full_name');
            $id_number = $request->input('id_number');
            $address = $request->input('address');
            $atoll_id = $request->input('atoll_id');
            $island_id = $request->input('island_id');
            $no_of_person = $request->input('no_of_person');
            $commodity_id = $request->input('commodity_id');
            $zakat_amount = $request->input('zakat_amount');
            $sadagath_amount = $request->input('sadagath_amount');
            $total_amount = $request->input('total_amount');
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $payment_method = $request->input('payment_method');
            $payment_invoice_number = "ZK" . round(microtime(true) * 1000) . "_" . $user_id;

            // Check calculation
            $zakat_commodity = ZakatCommodity::find($commodity_id);
            $calculated_zakat_amount = floatval($no_of_person * $zakat_commodity->price);

            if ($calculated_zakat_amount != floatval($zakat_amount)) {
                return APIHelper::makeAPIResponse(false, "Invalid Zakat amount", null, 400);
            }

            if (floatval($calculated_zakat_amount + $sadagath_amount) != floatval($total_amount)) {
                return APIHelper::makeAPIResponse(false, "Invalid total amount", null, 400);
            }

            DB::beginTransaction();
            $zakat_payment = new ZakatPayment();
            $zakat_payment->user_id = $user_id;
            $zakat_payment->full_name = $full_name;
            $zakat_payment->id_number = $id_number;
            $zakat_payment->address = $address;
            $zakat_payment->atoll_id = $atoll_id;
            $zakat_payment->island_id = $island_id;
            $zakat_payment->no_of_person = $no_of_person;
            $zakat_payment->commodity_id = $commodity_id;
            $zakat_payment->zakat_amount = $zakat_amount;
            $zakat_payment->sadagath_amount = $sadagath_amount;
            $zakat_payment->total_amount = $total_amount;
            $zakat_payment->payment_method = $payment_method;
            $zakat_payment->status = 2;
            $zakat_payment->mobile_number = $mobile;
            $zakat_payment->payment_invoice_number = $payment_invoice_number;
            $zakat_payment->save();
            DB::commit();

            $api_body = [
                "username" => config('dhiraagu.static_api_data.mfs_username'),
                "merchantKey" => config('dhiraagu.static_api_data.mfs_merchantKey'),
                "originationNumber" => config('dhiraagu.static_api_data.mfs_zakat_originationNumber'),
                "destinationNumber" => $mobile,
                "amount" => $total_amount,
                "paymentInvoiceNumber" => $payment_invoice_number,
                "transactionDescription" => "Zakat Payment"
            ];

            $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('payment_otp_request'), $api_body, "json");

            if (config('dhiraagu.bypass_service') || ($response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"])) {

                $zakat_payment->transaction_id = isset($response["body"]["transactionId"]) ? $response["body"]["transactionId"] : null;
                $zakat_payment->result_data_reference_id = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["referenceId"])) ? $response["body"]["resultData"]["referenceId"] : null;
                $zakat_payment->result_data_message = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["message"])) ? $response["body"]["resultData"]["message"] : null;
                $zakat_payment->save();

                $donation_response = [
                    "zakat_payment_id" => $zakat_payment->id,
                    "payment_invoice_number" => $payment_invoice_number
                ];

                return APIHelper::makeAPIResponse(true, "Done", $donation_response, 200);
            } else if (isset($response["body"]["resultData"]) &&
                isset($response["body"]["resultData"]["message"]) &&
                $response["body"]["resultData"]["message"] == "QUERYPROFILE:Destination number in wrong format(1137)") {
                $android_dhiraagu_pay_url = Setting::where('title', 'android_dhiraagu_pay_url')->pluck('text')->first();
                $ios_dhiraagu_pay_url = Setting::where('title', 'ios_dhiraagu_pay_url')->pluck('text')->first();
                $data = [
                    "android_dhiraagu_pay_url" => $android_dhiraagu_pay_url != null ? $android_dhiraagu_pay_url : null,
                    "ios_dhiraagu_pay_url" => $ios_dhiraagu_pay_url != null ? $ios_dhiraagu_pay_url : null,
                ];
                return APIHelper::makeAPIResponse(false, "Please register on Dhiraagu pay", $data, 428);
            } else if (isset($response["body"]["resultData"]) &&
                isset($response["body"]["resultData"]["message"]) &&
                $response["body"]["resultData"]["message"] == "QUERYPROFILE:Limit Check Failed(1140)") {
                return APIHelper::makeAPIResponse(false, "Insufficient balance in your wallet to process this transaction", null, 500);
            } else if (isset($response["body"]["resultData"]) &&
                isset($response["body"]["resultData"]["message"]) &&
                $response["body"]["resultData"]["message"] == "QUERYPROFILE:Requested Service is not subscribed by the User(1061)") {
                return APIHelper::makeAPIResponse(false, "Service not available", null, 500);
            } else {
                return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }

    public function verifyZakatPayment(VerifyZakatPaymentRequest $request)
    {
        try {
            // STOP ZAKAT PAYMENT
            return APIHelper::makeAPIResponse(false, "As per Islamic Ministry’s instructions, this year’s Fitr Zakat payment collection has been closed as of 6 pm on 12 May 2021", null, 500);

            $user_id = APIHelper::loggedUser()->id;
            $zakat_payment_id = $request->input('zakat_payment_id');
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $otp = trim($request->input('otp'));

            $zakat_payment = ZakatPayment::find($zakat_payment_id);

            if ($zakat_payment->mobile_number != $mobile) {
                return APIHelper::makeAPIResponse(false, "Invalid mobile number", null, 400);
            }

            if ($zakat_payment->status != 1) {
                $api_body = [
                    "username" => config('dhiraagu.static_api_data.mfs_username'),
                    "merchantKey" => config('dhiraagu.static_api_data.mfs_merchantKey'),
                    "destinationNumber" => $mobile,
                    "transactionDescription" => "Zakat Payment",
                    "referenceId" => $zakat_payment->result_data_reference_id,
                    "otpString" => $otp
                ];

                $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('payment_otp_verify'), $api_body, "json");

                if ((config('dhiraagu.bypass_service') && ($otp == "1234")) || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                    // Success payment
                    $zakat_payment->status = 1;
                    $zakat_payment->transaction_id = isset($response["body"]["transactionId"]) ? $response["body"]["transactionId"] : null;
                    $zakat_payment->result_data_reference_id = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["referenceId"])) ? $response["body"]["resultData"]["referenceId"] : null;
                    $zakat_payment->result_data_message = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["message"])) ? $response["body"]["resultData"]["message"] : null;
                    $zakat_payment->save();

                    $zakat_response = [
                        "zakat_payment_id" => $zakat_payment->transaction_id,
                        "message" => "Thank you for your payment",
                    ];

                    $zakat_amount = APIHelper::setAmountFormat($zakat_payment->zakat_amount);
                    $sadagath_amount = APIHelper::setAmountFormat($zakat_payment->sadagath_amount);

                    // Push notification
                    $push_message = "Your Fitr Zakat of MVR " . $zakat_amount . " and Sadagath of MVR " . $sadagath_amount . " has been successfully received on " . APIHelper::setDhiraaguDateFormat($zakat_payment->updated_at, "datetime") . ". This amount will be distributed to those in need by Ministry of Islamic Affairs. Thank you for your contribution";
                    OneSignalPush::sendPushNotification($push_message, "transaction", null, 0, $user_id, null);

                    return APIHelper::makeAPIResponse(true, "Zakat payment Success", $zakat_response, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Invalid OTP code", null, 400);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Zakath payment already paid", null, 400);
            }


        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }

}
