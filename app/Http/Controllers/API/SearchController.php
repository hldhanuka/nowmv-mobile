<?php

namespace App\Http\Controllers\API;

use App\Donation;
use App\Event;
use App\Helpers\APIHelper;
use App\Meal;
use App\News;
use App\Offer;
use App\Recipe;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SearchController extends Controller
{
    public function globalSearch(Request $request)
    {
        $user_id = APIHelper::loggedUser()->id;
        $search = $request->input("search");
        $result_data = [];
        $content_limit = 100;

        //Check whether $search is empty or not
        if ($search != null && $search != "") {

            // Restaurant search results
            $restaurant_search_results = Restaurant::where(function($query) use ($search) {
                $query->whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER (content) like "%' . strtolower($search) .'%"')->get();
            })->active()->latest()->get();

            // Restaurant Keyword search
            $restaurant_search_keywords = ["restaurant","restaurants","food","foods"];
            if(in_array(strtolower($search), $restaurant_search_keywords)){
                $restaurant_search_results = Restaurant::active()->latest()->get();
            }

            foreach ($restaurant_search_results as $restaurant_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $restaurant_search_result->id;
                    $temp_data["title"] = $restaurant_search_result->title;
                    $temp_data["image"] = $restaurant_search_result->image;
                    $temp_data["content"] = Str::limit($restaurant_search_result->content, $content_limit);
                    $temp_data["type"] = "restaurant";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // News search results
            $news_search_results = News::where(function($query) use ($search) {
                $query->whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER(content) like "%' . strtolower($search) . '%"')->get();
            })->active()->latest()->get();

            // News Keyword search
            $news_search_keywords = ["news"];
            if(in_array(strtolower($search), $news_search_keywords)){
                $news_search_results = News::active()->latest()->get();
            }

            foreach ($news_search_results as $news_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $news_search_result->id;
                    $temp_data["title"] = $news_search_result->title;
                    if($news_search_result->video!=null){
                        $temp_data["image"] = $news_search_result->video_thumbnail;
                    } else {
                        $temp_data["image"] = $news_search_result->image;
                    }
                    $temp_data["content"] = Str::limit($news_search_result->content, $content_limit);
                    $temp_data["type"] = "news";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // Recipe search results
            $recipe_search_results = Recipe::where(function($query) use ($search) {
                $query->whereRaw('LOWER(title) like "%' . strtolower($search) . '%"')->get();
            })->active()->latest()->get();

            // Recipe Keyword search
            $recipe_search_keywords = ["recipe","recipes","food","foods"];
            if(in_array(strtolower($search), $recipe_search_keywords)){
                $recipe_search_results = Recipe::active()->latest()->get();
            }

            foreach ($recipe_search_results as $recipe_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $recipe_search_result->id;
                    $temp_data["title"] = $recipe_search_result->title;
                    $temp_data["image"] = $recipe_search_result->image;
                    $temp_data["content"] = Str::limit($recipe_search_result->content, $content_limit);
                    $temp_data["type"] = "recipe";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // Meals search
            $meals_search_results = Meal::where(function($query) use ($search) {
                $query->whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER(content) like "%' . strtolower($search) . '%"')->get();
            })->active()->latest()->get();

            // Meals Keyword search
            $meal_search_keywords = ["meal","meals","food","foods"];
            if(in_array(strtolower($search), $meal_search_keywords)){
                $meals_search_results = Meal::active()->latest()->get();
            }

            foreach ($meals_search_results as $meals_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $meals_search_result->id;
                    $temp_data["title"] = $meals_search_result->title;
                    $temp_data["image"] = $meals_search_result->image;
                    $temp_data["content"] = Str::limit($meals_search_result->content, $content_limit);
                    $temp_data["type"] = "meal";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // Events search results
            $all_event = Event::active()->public()
                ->orWhere('created_by', $user_id)
                ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                    $query->where('invitee_id', $user_id);
                })->get();

            $search_event = Event::whereRaw('LOWER(name) like "%' . strtolower($search) . '%"')
                ->orWhereRaw('LOWER(description) like "%' . strtolower($search) . '%"')
                ->orWhereRaw('LOWER(location_name) like "%' . strtolower($search) . '%"')->get();

            $events_search_results = collect($all_event)->intersect($search_event)->sortByDesc("created_at")->flatten();

            // Event Keyword search
            $recipe_search_keywords = ["event","events"];
            if(in_array(strtolower($search), $recipe_search_keywords)){
                $events_search_results = Event::active()->public()
                ->orWhere('created_by', $user_id)
                ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                    $query->where('invitee_id', $user_id);
                })
                ->latest()->get();
            }

            foreach ($events_search_results as $events_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $events_search_result->id;
                    $temp_data["title"] = $events_search_result->name;
                    $temp_data["image"] = $events_search_result->image;
                    $temp_data["content"] = Str::limit($events_search_result->description, $content_limit);
                    $temp_data["type"] = "event";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // Donations search results
            $donations_search_results =  Donation::whereHas('ngo', function($q){
                $q->active();
            })->where(function($query) use ($search) {
                $query->whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER(content) like "%' . strtolower($search) . '%"')->get();
            })->active()->latest()->get();

            // Donation Keyword search
            $donation_search_keywords = ["donation","donations"];
            if(in_array(strtolower($search), $donation_search_keywords)){
                $donations_search_results = Donation::active()->latest()->get();
            }

            foreach ($donations_search_results as $donations_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $donations_search_result->id;
                    $temp_data["title"] = $donations_search_result->title;
                    $temp_data["image"] = $donations_search_result->image;
                    $temp_data["content"] = Str::limit($donations_search_result->content, 50);
                    $temp_data["type"] = "donation";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // Offer search results
            $offers_search_results =   Offer::where(function($query) use ($search) {
                $query->whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER(content) like "%' . strtolower($search) .  '%"')->get();
            })->active()->latest()->get();

            // Offer Keyword search
            $offer_search_keywords = ["offer","offers"];
            if(in_array(strtolower($search), $offer_search_keywords)){
                $offers_search_results = Offer::active()->latest()->get();
            }

            foreach ($offers_search_results as $offers_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $offers_search_result->id;
                    $temp_data["title"] = $offers_search_result->title;
                    $temp_data["image"] = $offers_search_result->image;
                    $temp_data["content"] = Str::limit($offers_search_result->content, 50);
                    $temp_data["type"] = "offer";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            $result_data = array_map(function($array) {
                $array['content'] = $array['content']!=null?$array['content']:"";
                return $array;
            }, $result_data);

            return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);
        } else {
            return APIHelper::makeAPIResponse(false, "Search Field Cannot be Empty", null, 400);
        }
    }
}
