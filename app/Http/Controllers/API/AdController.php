<?php

namespace App\Http\Controllers\API;

use App\Ad;
use App\Helpers\APIHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdController extends Controller
{
    public function adByType(Request $request, $type)
    {
        try {
            if ($type == "all") {
                $ads = Ad::active()->inRandomOrder()->get();
            } else if ($type != null) {
                $ads = Ad::where("type", $type)->active()->inRandomOrder()->get();
                if (count($ads) == 0) {
                    return APIHelper::makeAPIResponse(false, "Ads not found", null, 404);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Type not defined", null, 400);
            }

            return APIHelper::makeAPIResponse(true, "Done", $ads, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
