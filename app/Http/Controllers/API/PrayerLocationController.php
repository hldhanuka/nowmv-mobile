<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\SubmitPrayerLocationRequest;
use App\PrayerLocation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrayerLocationController extends Controller
{
    public function getAllPrayerLocations(Request $request)
    {
        try {
            $prayer_locations = PrayerLocation::orderBy('island')->get();
            return APIHelper::makeAPIResponse(true, "Done", $prayer_locations, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }

    public function submitPrayerLocation(SubmitPrayerLocationRequest $request)
    {
        try {
            $user = User::find(APIHelper::loggedUser()->id);
            if ($user != null) {
                $user->prayer_location_id = $request->input('location_id');
                $user->save();
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not found", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getUserPrayerLocation(Request $request)
    {
        try {
            $user = User::find(APIHelper::loggedUser()->id);
            if ($user != null) {
                $prayer_location = PrayerLocation::find($user->prayer_location_id);
                if($user->prayer_location_id!=null && $prayer_location!=null){
                    return APIHelper::makeAPIResponse(true, "Done", $prayer_location, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "User location doesn't exist", null, 400);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "User not found", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

}
