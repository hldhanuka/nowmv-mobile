<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\SocialMobileLoginRequest;
use App\Http\Requests\API\SocialMobileRegisterRequest;
use App\Http\Requests\API\SocialOTPResendRequest;
use App\Http\Requests\API\SocialOTPVerifyRequest;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\SocialAccount;
use App\User;
use AppleSignIn\ASDecoder;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class SocialAccountController extends Controller
{
    protected $jwt;

    /**
     * SocialAccountController constructor.
     * @param \Tymon\JWTAuth\JWTAuth $jwt
     */
    public function __construct(\Tymon\JWTAuth\JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Mobile Login API
     * @param SocialMobileLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mobileLogin(SocialMobileLoginRequest $request)
    {
        try {
            // get request data
            $provider = trim($request->input("provider"));
            $provider_user_id = trim($request->input("provider_user_id"));
            $provider_token = trim($request->input("provider_token"));
            $provider_token_secret = trim($request->input("provider_token_secret"));
            $device_id = trim($request->input("device_id"));

            try {
                if ($provider == "google") {
                    // OAuth 2.0
                    $provider_user = $this->getUserByToken($provider_token);
                    if (!$this->verifyGoogleUser($provider_user, $provider_user_id)) {
                        // Google authentication error
                        return APIHelper::makeAPIResponse(false, "Google authentication error", null, 400);
                    }
                } else if ($provider == "facebook") {
                    // OAuth 2.0
                    $provider_user = Socialite::driver($provider)->userFromToken($provider_token);
                } else if ($provider == "twitter") {
                    // OAuth 1.0
                    $provider_user = Socialite::driver($provider)->userFromTokenAndSecret($provider_token, $provider_token_secret);
                } else if ($provider == "apple") {
                    // Check using griffinledingham/php-apple-signin library
                    $provider_user = ASDecoder::getAppleSignInPayload($provider_token);
                } else {
                    // provider not found
                    return APIHelper::makeAPIResponse(false, "Provider not found", null, 404);
                }
            } catch (\GuzzleHttp\Exception\ClientException $clientException) {
                report($clientException);
                $response = $clientException->getResponse();
                $provider_response = json_decode($response->getBody());
                if ($provider == "google") {
                    $error_message = isset($provider_response->error_description) ? $provider_response->error_description : "Invalid request";
                } else if ($provider == "facebook") {
                    $error_message = isset($provider_response->error->message) ? $provider_response->error->message : "Invalid request";
                } else if ($provider == "twitter") {
                    $error_message = isset($provider_response->error->message) ? $provider_response->error->message : "Invalid request";
                } else {
                    $error_message = "Invalid request";
                }
                return APIHelper::makeAPIResponse(false, $error_message, null, $response->getStatusCode());
            } catch (\Exception $e) {
                report($e);
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }

            if ($provider_user != null) {
                // Backend Authenticated
                if ($provider == "google") {
                    $authenticated_provider_user_id = $provider_user["body"]["sub"];
                } else if ($provider == "apple") {
                    $authenticated_provider_user_id = $provider_user->getUser();
                } else {
                    $authenticated_provider_user_id = $provider_user->getId();
                }

                if ($authenticated_provider_user_id == $provider_user_id) {
                    // Matched both user ids

                    $social_account = SocialAccount::where("provider", $provider)
                        ->where("provider_user_id", $provider_user_id)
                        ->first();

                    // Check social account exist and Check user approved the OTP
                    $is_existing_registered_user = ($social_account != null) && ($social_account->user != null) && ($social_account->user->status == 1);

                    // Check social account exist and Check user approved the OTP
                    if ($is_existing_registered_user) {
                        // Existing Social Account with approved OTP
                        $mobile = $social_account->user->mobile;
                        // Normal access token generate
                        $payload = $this->jwt->factory()
                            ->setTTL(config('jwt.default_token_time'))
                            ->customClaims([
                                'sub' => $mobile,
                                'data' => [
                                    'mobile' => $mobile,
                                    'device_id' => $device_id,
                                    'token_type' => 'long',
                                    'login_type' => 'social',
                                    'social' => [
                                        'provider' => $provider,
                                        'provider_user_id' => $provider_user_id,
                                        'provider_token' => $provider_token,
                                        'provider_token_secret' => $provider_token_secret
                                    ]
                                ]
                            ])
                            ->make();
                    } else {
                        // New Social Account or Social Account without approved OTP
                        $mobile = 0;
                        // Short access token generate
                        $payload = $this->jwt->factory()
                            ->setTTL(config('jwt.login_token_time'))
                            ->customClaims([
                                'sub' => $mobile,
                                'data' => [
                                    'mobile' => $mobile,
                                    'device_id' => $device_id,
                                    'token_type' => 'short',
                                    'login_type' => 'social',
                                    'social' => [
                                        'provider' => $provider,
                                        'provider_user_id' => $provider_user_id,
                                        'provider_token' => $provider_token,
                                        'provider_token_secret' => $provider_token_secret
                                    ]
                                ]
                            ])
                            ->make();
                    }

                    $token = $this->jwt->manager()->encode($payload)->get();

                    $user_response_body = [
                        'token' => (string)$token,
                        'registered_user' => $is_existing_registered_user
                    ];

                    // Success token response
                    return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
                } else {
                    // Not matched user ids
                    return APIHelper::makeAPIResponse(false, "User ids not matched", null, 400);
                }
            } else {
                // Unauthorize
                return APIHelper::makeAPIResponse(false, "User not authorize", null, 401);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    /**
     * Mobile Register API
     * @param SocialMobileRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mobileRegister(SocialMobileRegisterRequest $request)
    {
        try {
            $provider = trim($request->input('provider'));
            $provider_user_id = trim($request->input('provider_user_id'));
            $provider_token = trim($request->input("provider_token"));
            $provider_token_secret = trim($request->input("provider_token_secret"));
            $device_id = trim($request->input("device_id"));
            $first_name = trim($request->input('first_name'));
            $last_name = trim($request->input('last_name'));
            $email = trim($request->input('email'));
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $birthday = trim($request->input('birthday'));
            $address = trim($request->input('address'));
            $gender = trim($request->input('gender'));

            try {
                if ($provider == "google") {
                    // OAuth 2.0
                    $provider_user = $this->getUserByToken($provider_token);
                    if (!$this->verifyGoogleUser($provider_user, $provider_user_id)) {
                        // Google authentication error
                        return APIHelper::makeAPIResponse(false, "Google authentication error", null, 400);
                    }
                } else if ($provider == "facebook") {
                    // OAuth 2.0
                    $provider_user = Socialite::driver($provider)->userFromToken($provider_token);
                } else if ($provider == "twitter") {
                    // OAuth 1.0
                    $provider_user = Socialite::driver($provider)->userFromTokenAndSecret($provider_token, $provider_token_secret);
                } else if ($provider == "apple") {
                    // Check using griffinledingham/php-apple-signin library
                    $provider_user = ASDecoder::getAppleSignInPayload($provider_token);
                } else {
                    // provider not found
                    return APIHelper::makeAPIResponse(false, "Provider not found", null, 404);
                }
            } catch (\GuzzleHttp\Exception\ClientException $clientException) {
                report($clientException);
                $response = $clientException->getResponse();
                $provider_response = json_decode($response->getBody());
                return APIHelper::makeAPIResponse(false, $provider_response->error->message, null, $response->getStatusCode());
            } catch (\Exception $e) {
                report($e);
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }

            if ($provider_user != null) {
                // Backend Authenticated
                if ($provider == "google") {
                    $authenticated_provider_user_id = $provider_user["body"]["sub"];
                    $profile_image = $provider_user["body"]["picture"];
                } else if ($provider == "apple") {
                    $authenticated_provider_user_id = $provider_user->getUser();
                    $profile_image = null;
                } else {
                    $authenticated_provider_user_id = $provider_user->getId();
                    $profile_image = $provider_user->getAvatar();
                }

                if ($authenticated_provider_user_id == $provider_user_id) {
                    // Matched both user ids

                    $social_account = SocialAccount::where("provider", $provider)
                        ->where("provider_user_id", $provider_user_id)
                        ->first();

                    // Check social account exist and Check user approved the OTP
                    $is_existing_registered_user = ($social_account != null) && ($social_account->user != null) && ($social_account->user->status == 1);
                    $is_existing_unapproved_user = ($social_account != null) && ($social_account->user != null) && ($social_account->user->status == 3);
                    $guest = User::where("mobile", $mobile)->guest()->first();
                    $is_guest_user = ($guest != null) && ($social_account == null);

                    if ($is_existing_registered_user) {
                        // Existing Social Account with approved OTP
                        return APIHelper::makeAPIResponse(false, "User already registered", null, 400);
                    } else if ($is_existing_unapproved_user) {
                        // Existing Social Account Again Register
                        $user = $social_account->user;
                        $user->first_name = $first_name;
                        $user->last_name = $last_name;
                        $user->email = $email;
                        $user->image = $profile_image;
                        $user->mobile = $mobile;
                        $user->birthday = $birthday;
                        $user->address = $address;
                        $user->gender = $gender;
                        $user->status = 3;
                        $result = $user->save();

                        if ($result) {
                            $payload = $this->jwt->factory()
                                ->setTTL(config('jwt.default_token_time'))
                                ->customClaims([
                                    'sub' => $mobile,
                                    'data' => [
                                        'mobile' => $mobile,
                                        'device_id' => $device_id,
                                        'token_type' => 'short',
                                        'login_type' => 'social',
                                        'social' => [
                                            'provider' => $provider,
                                            'provider_user_id' => $provider_user_id,
                                            'provider_token' => $provider_token,
                                            'provider_token_secret' => $provider_token_secret
                                        ]
                                    ]
                                ])
                                ->make();

                            $token = $this->jwt->manager()->encode($payload)->get();

                            $user_response_body = [
                                'token' => (string)$token
                            ];

                            // Send OTP
                            $api_body = [
                                "clientReference" => config('dhiraagu.static_api_data.clientReference'),
                                "mobileNumber" => $mobile,
                                "senderId" => config('dhiraagu.static_api_data.senderId'),
                                "messageTemplate" => config('dhiraagu.static_api_data.messageTemplate'),
                                "otpServiceDescription" => config('dhiraagu.static_api_data.otpServiceDescription'),
                            ];

                            $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('generate_otp'), $api_body, "json");

                            if (config('dhiraagu.bypass_service') || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                                // Success OTP send response
                                return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
                            } else {
                                return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
                            }

                        } else {
                            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
                        }

                    } else if ($is_guest_user) {
                        // Guest user
                        $user = $guest;
                    } else {
                        // New Social Account Registration
                        $user = new User();
                    }

                    $user->first_name = $first_name;
                    $user->last_name = $last_name;
                    $user->email = $email;
                    $user->image = $profile_image;
                    $user->mobile = $mobile;
                    $user->birthday = $birthday;
                    $user->address = $address;
                    $user->gender = $gender;
                    $user->status = 3;
                    $result = $user->save();

                    $social_account = new SocialAccount();
                    $social_account->user_id = $user->id;
                    $social_account->provider_user_id = $provider_user_id;
                    $social_account->provider = $provider;
                    $social_account->save();

                    // Add customer role to new user
                    $user->assignRole("customer");

                    if ($result) {
                        $set_default_notification_settings = APIHelper::setDefaultNotificationSettings($user->id);
                        $set_default_settings = APIHelper::setDefaultHomeScreenOrder($user->id);
                        $set_default_prayer_reminder = APIHelper::setDefaultPrayerReminder($user->id);

                        $payload = $this->jwt->factory()
                            ->setTTL(config('jwt.default_token_time'))
                            ->customClaims([
                                'sub' => $mobile,
                                'data' => [
                                    'mobile' => $mobile,
                                    'device_id' => $device_id,
                                    'token_type' => 'short',
                                    'login_type' => 'social',
                                    'social' => [
                                        'provider' => $provider,
                                        'provider_user_id' => $provider_user_id,
                                        'provider_token' => $provider_token,
                                        'provider_token_secret' => $provider_token_secret
                                    ]
                                ]
                            ])
                            ->make();

                        $token = $this->jwt->manager()->encode($payload)->get();

                        $user_response_body = [
                            'token' => (string)$token
                        ];

                        // Send OTP
                        $api_body = [
                            "clientReference" => config('dhiraagu.static_api_data.clientReference'),
                            "mobileNumber" => $mobile,
                            "senderId" => config('dhiraagu.static_api_data.senderId'),
                            "messageTemplate" => config('dhiraagu.static_api_data.messageTemplate'),
                            "otpServiceDescription" => config('dhiraagu.static_api_data.otpServiceDescription'),
                        ];

                        $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('generate_otp'), $api_body, "json");

                        if (config('dhiraagu.bypass_service') || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                            // Success OTP send response
                            return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
                        } else {
                            return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
                        }

                    } else {
                        return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
                    }

                } else {
                    // Not matched user ids
                    return APIHelper::makeAPIResponse(false, "User ids not matched", null, 400);
                }
            } else {
                // Unauthorize
                return APIHelper::makeAPIResponse(false, "User not authorize", null, 401);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function otpResend(SocialOTPResendRequest $request)
    {
        try {
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $device_id = trim($request->input('device_id'));
            $provider = trim($request->input('provider'));
            $provider_user_id = trim($request->input('provider_user_id'));
            $provider_token = trim($request->input("provider_token"));
            $provider_token_secret = trim($request->input("provider_token_secret"));

            $api_body = [
                "clientReference" => config('dhiraagu.static_api_data.clientReference'),
                "mobileNumber" => $mobile,
                "senderId" => config('dhiraagu.static_api_data.senderId'),
                "messageTemplate" => config('dhiraagu.static_api_data.messageTemplate'),
                "otpServiceDescription" => config('dhiraagu.static_api_data.otpServiceDescription'),
            ];

            $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('generate_otp'), $api_body, "json");

            if (config('dhiraagu.bypass_service') || APIHelper::isBypassMobile($mobile) || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                $payload = $this->jwt->factory()
                    ->setTTL(config('jwt.login_token_time'))
                    ->customClaims([
                        'sub' => $mobile,
                        'data' => [
                            'mobile' => $mobile,
                            'device_id' => $device_id,
                            'token_type' => 'short',
                            'login_type' => 'social',
                            'social' => [
                                'provider' => $provider,
                                'provider_user_id' => $provider_user_id,
                                'provider_token' => $provider_token,
                                'provider_token_secret' => $provider_token_secret
                            ]
                        ]
                    ])
                    ->make();

                $token = $this->jwt->manager()->encode($payload)->get();

                $user_response_body = [
                    'pre_token' => (string)$token
                ];

                return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
            }
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Token Error", null, 500);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }


    }

    public function mobileVerify(SocialOTPVerifyRequest $request)
    {
        try {
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $device_id = trim($request->input('device_id'));
            $otp = trim($request->input('otp'));
            $provider = trim($request->input('provider'));
            $provider_user_id = trim($request->input('provider_user_id'));
            $provider_token = trim($request->input("provider_token"));
            $provider_token_secret = trim($request->input("provider_token_secret"));

            $payload = $this->jwt->getPayload();
            $token_mobile = $payload->get('data.mobile');
            $token_device_id = $payload->get('data.device_id');

            if ($mobile == $token_mobile && $device_id == $token_device_id) {
                // Token data and request data validated
                $api_body = [
                    "mobileNumber" => $mobile,
                    "code" => $otp
                ];

                $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('verify_otp', '{ClientRefrerence}', config('dhiraagu.static_api_data.clientReference')), $api_body, "json");

                if ((config('dhiraagu.bypass_service') && ($otp == "123456")) || (APIHelper::isBypassMobile($mobile) && ($otp == "123456")) || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                    $payload = $this->jwt->factory()
                        ->setTTL(config('jwt.default_token_time'))
                        ->customClaims([
                            'sub' => $mobile,
                            'data' => [
                                'mobile' => $mobile,
                                'device_id' => $device_id,
                                'token_type' => 'long',
                                'login_type' => 'otp',
                                'social' => [
                                    'provider' => $provider,
                                    'provider_user_id' => $provider_user_id,
                                    'provider_token' => $provider_token,
                                    'provider_token_secret' => $provider_token_secret
                                ]
                            ]
                        ])
                        ->make();

                    $token = $this->jwt->manager()->encode($payload)->get();
                    $user = User::where("mobile", $mobile)->first();
                    $user->status = 1;
                    $user->save();

                    $user_response_body = [
                        'token' => (string)$token,
                        'registered_user' => $user != null
                    ];

                    // Push notification
                    $push_message = "Welcome to Now MV";
                    $data = ["type" => "home", "id" => null];
                    OneSignalPush::sendPushNotification($push_message, "system", $data, 0, $user->id, null);

                    return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Invalid OTP code", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Invalid Request", null, 500);
            }
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Token Error", null, 500);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    /***
     *  FOR TESTING PURPOSE // TODO remove later
     *
     * @param Request $request
     * @param $provider
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|void
     */
    public function login(Request $request, $provider)
    {
        try {
            if ($provider == "google" || $provider == "facebook" || $provider == "facebook_new" || $provider == "twitter" || $provider == "twitter_new") {
                return Socialite::with($provider)->redirect();
            } else {
                return abort(404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }


    /***
     *  FOR TESTING PURPOSE // TODO remove later
     *
     * @param Request $request
     * @param $provider
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|void
     */
    public function callback(Request $request, $provider)
    {
        try {
            if ($provider == "google" || $provider == "facebook" || $provider == "facebook_new" || $provider == "twitter" || $provider == "twitter_new") {

                //For Twitter
                /*$provider_user = Socialite::driver($provider)->user();    //for old user
                //$provider_user = Socialite::driver("twitter_new")->user();    //for new user
                $token = $provider_user->token;
                $token_secret = $provider_user->tokenSecret;

                $user_from_token = Socialite::driver($provider)->userFromTokenAndSecret($token, $token_secret);
                dd($user_from_token);
                $user = $this->findOrCreateUser($user_from_token , $provider);
                dd($user);*/

                //For Facebook
                $provider_user = Socialite::driver($provider)->user();           //get old user
                //$provider_user = Socialite::driver("facebook_new")->user();    //get new user
                $token = $provider_user->token;

                try {
                    $user_from_token = Socialite::driver("facebook_new")->userFromToken($token);
                    //dd( $user_from_token);
                } catch (\GuzzleHttp\Exception\ClientException $clientException) {
                    $response = $clientException->getResponse();
                    $provider_response = json_decode($response->getBody());

                    $user_from_token = Socialite::driver($provider)->userFromToken($token);
                    //dd($provider_response->error->message, $user_from_token);
                }
                $user = $this->findOrCreateUser($user_from_token, $provider);
                //dd($user);

                //For Google
                /*$provider_user = Socialite::driver($provider)->user();
                $token = $provider_user->token; //access token
                $response = $this->getUserByToken($token);
                dd($response);
                dd($this->verifyGoogleUser($response, $provider_user->getId()));*/

                //$user = $this->findOrCreateUser($provider_user , $provider);

                Auth::login($user, true);
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return abort(404);
            }
        } catch (\Exception $e) {
            //dd($e->getMessage());
            report($e);
            return abort(500);
        }
    }

    /**
     * FOR TESTING PURPOSE // TODO remove later
     * @param $provider_user
     * @param $provider
     * @return mixed
     */
    public function findOrCreateUser($provider_user, $provider)
    {
        $account = SocialAccount::where("provider", $provider)
            ->where("provider_user_id", $provider_user->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $user = User::whereEmail($provider_user->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $provider_user->getEmail(),
                    'name' => $provider_user->getName(),
                ]);
            }

            $user->socialAccounts()->create([
                'provider_user_id' => $provider_user->getId(),
                'provider' => $provider,
            ]);

            return $user;
        }
    }

    protected function getUserByToken($token)
    {
        $request = [
            'headers' => [
                'Accept' => 'application/json'
            ]
        ];

        $client = new Client(['http_errors' => false]);
        $response = $client->request("GET", 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $token, $request);
        $final_response = [
            "body" => json_decode($response->getBody(), true),
            "status" => $response->getStatusCode(),
        ];
        return $final_response;
    }

    protected function verifyGoogleUser($response, $provider_user_id)
    {
        $google_client_id = config("services.google.client_id");
        $google_ios_client_id = config("services.google_ios.client_id");
        $iss = "https://accounts.google.com";

        if ($response["status"] == 200) {
            if (($google_client_id == $response["body"]["aud"] || $google_ios_client_id == $response["body"]["aud"])) {
                if ($provider_user_id == $response["body"]["sub"]) {
                    if ($iss == $response["body"]["iss"]) {
                        if (Carbon::createFromTimestamp($response["body"]["exp"])->isFuture()) {
                            $is_verified = true;
                        } else {
                            Log::warning("Google authentication error - token expired");
                            $is_verified = false;
                        }
                    } else {
                        Log::warning("Google authentication error - iss not matched");
                        $is_verified = false;
                    }
                } else {
                    Log::warning("Google authentication error - user id not matched");
                    $is_verified = false;
                }
            } else {
                Log::warning("Google authentication error - client id not matched");
                $is_verified = false;
            }

        } else {
            Log::warning("Google authentication error - tokeninfo service error");
            $is_verified = false;
        }

        return $is_verified;
    }

}
