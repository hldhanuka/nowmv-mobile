<?php

namespace App\Http\Controllers\API;

use App\FerryRoute;
use App\FerryRouteSchedule;
use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Http\Requests\API\GetAvailableFerriesRequest;
use App\Libraries\Dhathuru\DHATHURU;
use App\ScheduleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FerryController extends Controller
{
    public function getIslandList(Request $request)
    {
        try {
            $island_list = CMSHelper::getFerryLocations();
            $atoll_list_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_atoll_list"));

            $atoll_list = collect([]);

            if (isset($atoll_list_response["status"]) && $atoll_list_response["status"] && is_array($atoll_list_response["data"])) {
                $atoll_list = collect($atoll_list_response["data"]);
            }

            $new_island_list = [];
            $index = 1;
            foreach ($island_list as $island) {
                $new_island = [];
                $atoll_id = $island["atoll_id"];
                $new_island["id"] = $index;
                $atoll_data = $atoll_list->where("atoll_id", $atoll_id)->first();
                $new_island["atoll_id"] = intval($atoll_id);
                $new_island["atoll_name"] = $atoll_data != null ? $atoll_data["atoll_name"] : "-";
                $new_island["island_id"] = intval($island["island_id"]);
                $new_island["island_name"] = $island["island_name"];
                $new_island_list[] = $new_island;
                $index++;
            }
            return APIHelper::makeAPIResponse(true, "Done", $new_island_list, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getAvailablePublicFerries(GetAvailableFerriesRequest $request)
    {
        try {
            $date = Carbon::parse($request->input("date"))->toDateString();
            $from = $request->input("island_id");
            $island_list = CMSHelper::getFerryLocations();

            $all_ferry_schedule_types = ScheduleType::ferrySchedule()->active()->priority()->get();
            $selected_schedule_type = null;
            $final_schedule_data = [];

            foreach ($all_ferry_schedule_types as $schedule_type) {
                foreach ($schedule_type->date_array as $item) {
                    if ($date == $item["date"]) {
                        $selected_schedule_type = $schedule_type->id;
                        break 2;
                    }
                }
            }

            if ($selected_schedule_type != null) {
                $main_route_ids = FerryRoute::where(["from" => $from])->pluck("id")->toArray();
                if (count($main_route_ids)) {
                    // Is Main Route
                    $schedule_data_array = FerryRouteSchedule::with(["ferryRoute"])->active()->where("schedule_type_id", $selected_schedule_type)->whereIn("ferry_route_id", $main_route_ids)->orderBy('starting_time')->get();
                    $schedule_data = $this->createFerryScheduleDataArray($schedule_data_array, $date, $from);
                } else {
                    // Check Terminals
                    $ferry_route_ids = FerryRoute::active()->whereHas('ferryTerminalSchedule', function ($query) use ($from) {
                        $query->where('terminal_id', $from);
                    })->pluck('id')->toArray();

                    $schedule_data_array = FerryRouteSchedule::with(["ferryRoute"])->active()->where("schedule_type_id", $selected_schedule_type)->whereIn("ferry_route_id", $ferry_route_ids)->orderBy('starting_time')->get();
                    $schedule_data = $this->createFerryScheduleDataArray($schedule_data_array, $date, $from);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "No Available Ferries", null, 404);
            }

            foreach ($schedule_data as $schedule) {
                $schedule["from_location"] = $island_list->where("island_id", $schedule["from_location"])->first()["island_name"];
                $schedule["to_location"] = $island_list->where("island_id", $schedule["to_location"])->first()["island_name"];
                $final_schedule_data[] = $schedule;
            }

            if (count($final_schedule_data)) {
                return APIHelper::makeAPIResponse(true, "Done", $final_schedule_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "No Available Ferries", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function createFerryScheduleDataArray($schedule_data_array, $date, $from)
    {
        $final_schedule_data = [];
        foreach ($schedule_data_array as $schedule_data) {
            $schedule_time = $date . " " . $schedule_data->starting_time;
            if (Carbon::parse($schedule_time)->isFuture()) {
                $temp_data = [];
                $full_schedule_time = APIHelper::getFerryRouteAllSchedule($schedule_data, $date, $from, $schedule_data->ferryRoute->to);
                if ($full_schedule_time["valid_direction"]) {
                    $temp_data["route_name"] = $schedule_data->ferryRoute->name;
                    $temp_data["from_location"] = $schedule_data->ferryRoute->from;
                    $temp_data["to_location"] = $schedule_data->ferryRoute->to;
                    $temp_data["schedule_start_time"] = $full_schedule_time["schedule_time"];
                    $temp_data["schedule_end_time"] = $full_schedule_time["schedule_last_time"];
                    $temp_data["schedule_data"] = $full_schedule_time["schedule_list"];
                    $final_schedule_data[] = $temp_data;
                }
            }
        }
        return $final_schedule_data;
    }

    public function getAvailablePrivateFerries(GetAvailableFerriesRequest $request)
    {
        try {
            $date = $request->input("date");
            $island_schedule = [];
            $segment_array = [
                "{island_id}" => $request->input("island_id"),
                "{date}" => Carbon::parse($date)->format("d-m-Y"),
            ];

            $island_schedule_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_island_schedules", $segment_array));

            if (isset($island_schedule_response["status"]) && $island_schedule_response["status"] && is_array($island_schedule_response["data"])) {
                $island_schedule = $island_schedule_response["data"];
            }

            $new_island_schedule = [];
            foreach ($island_schedule as $schedule) {
                $new_schedule = [];
                $new_schedule["schedule_id"] = intval($schedule["schedule_id"]);
                $new_schedule["plan_id"] = intval($schedule["plan_id"]);
                $new_schedule["vessel_id"] = intval($schedule["vessel_id"]);
                $new_schedule["vessel_name"] = $schedule["vessel_name"];
                $new_schedule["vessel_type"] = $schedule["vessel_type"];
                $new_schedule["schedule_time"] = Carbon::parse($date . " " . $schedule["sch_time_str"])->toDateTimeString();
                $new_schedule["available_seats"] = intval($schedule["seats_avail_psngr"]);
                $route = explode(" - ", $schedule["route_full"]);
                $new_schedule["start_location"] = $route[0];
                $new_schedule["end_location"] = end($route);

                $new_schedule["route"] = $route;
                $new_island_schedule[] = $new_schedule;
            }


            return APIHelper::makeAPIResponse(true, "Done", $new_island_schedule, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getTicketPlanDetailById(Request $request, $id)
    {
        try {
            $result_array = [];
            $segment_array = [
                "{plan_id}" => $id,
            ];

            $ticket_plan_detail_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_ticket_plan_details", $segment_array));

            $ticket_plan_detail = [];
            if (isset($ticket_plan_detail_response["status"]) && $ticket_plan_detail_response["status"] && is_array($ticket_plan_detail_response["data"]) && isset($ticket_plan_detail_response["data"][0])) {
                $ticket_plan_detail = $ticket_plan_detail_response["data"][0];
                $vessel_segment_array = [
                    "{vessel_id}" => $ticket_plan_detail["vessel_id"],
                ];

                $get_boat_details_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_boat_details", $vessel_segment_array));

                if (isset($get_boat_details_response["status"]) && $get_boat_details_response["status"] && is_array($get_boat_details_response["data"])) {
                    $home_port_id = $get_boat_details_response["data"][0]["island_id_hp"];
                    $vessel_name = $get_boat_details_response["data"][0]["vessel_name"];
                } else {
                    return APIHelper::makeAPIResponse(false, "Dhathuru error", null, 400);
                }

                $island_segment_array = [
                    "{island_id}" => $home_port_id,
                ];

                $island_detail_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_island_details", $island_segment_array));

                if (isset($island_detail_response["status"]) && $island_detail_response["status"] && is_array($island_detail_response["data"])) {
                    $island_name = $island_detail_response["data"][0]["island_name"];
                } else {
                    return APIHelper::makeAPIResponse(false, "Dhathuru error", null, 400);
                }

                $result_array["display_price"] = "Price per person MVR " . $ticket_plan_detail_response["data"][0]["price_alo"] . " upwards";
                $result_array["home_port_name"] = $island_name . " | #" . $home_port_id;
                $result_array["vessel_name"] = $vessel_name;

                return APIHelper::makeAPIResponse(true, "Done", $result_array, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Invalid plan id", null, 404);
            }


        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getSeatingLayoutDetailById(Request $request, $id)
    {
        try {
            $segment_array = [
                "{vessel_id}" => $id,
            ];

            $seating_layout_detail_response = DHATHURU::apiCall(APIHelper::getDhathuruURL("get_seating_layout_details", $segment_array));
            if (isset($seating_layout_detail_response["status"]) && $seating_layout_detail_response["status"] && isset($seating_layout_detail_response["data"])) {
                $seating_layout_detail = $seating_layout_detail_response["data"];
                if (!($seating_layout_detail != strip_tags($seating_layout_detail))) {
                    $seating_layout_detail = "<h1 align='center'>Ferry service error</h1>";
                }
            }
            return view('app_pages.ferry_seating_layout', ["seating_layout_detail" => $seating_layout_detail]);
        } catch (\Exception $e) {
            report($e);
            $seating_layout_detail = "<h1 align='center'>Ferry service error</h1>";
            return view('app_pages.ferry_seating_layout', ["seating_layout_detail" => $seating_layout_detail]);
        }
    }
}
