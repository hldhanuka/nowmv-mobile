<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\EventInvitee;
use App\EventMessage;
use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\InviteAllToEventRequest;
use App\Http\Requests\API\InviteFriendsToEventRequest;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventInviteeController extends Controller
{
    public function getContactsByEventId(Request $request, $event_id)
    {
        try {
            $event = Event::private()->find($event_id);
            if ($event != null) {
                $invitees = EventInvitee::with(["invitee" => function ($query) {
                    $query->select('id', 'first_name', 'last_name', 'email', 'mobile', 'image', 'status');
                }])->where('event_id', $event_id)->get();
                if ($invitees != null) {
                    return APIHelper::makeAPIResponse(true, "Done", $invitees, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Contact on this event was not found", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function inviteFriendsToEvent(InviteFriendsToEventRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $user_id = $user->id;
            $friends = $request->input('friends');
            $event_id = $request->input('event_id');
            $event = Event::myEvent()->find($event_id);
            if ($event != null) {
                EventInvitee::where(['user_id' => $user_id, 'event_id' => $event_id])->delete();
                foreach ($friends as $friend) {
                    // Create DB record
                    $event_invitation = EventInvitee::updateOrCreate(['user_id' => $user_id, 'event_id' => $event_id, 'invitee_id' => $friend], ['status' => 2]);
                    if ($event_invitation) {
                        // Create push notification
                        $creator_full_name = $user->first_name . " " . $user->last_name;
                        $format_date = Carbon::parse($event->date . " " . $event->time)->format('l jS \\of F Y h:i A');
                        $message = $creator_full_name . "  invited you to \"" . $event->name . "\" event held on " . $format_date . " at " . $event->location_name;
                        $data = ["type" => "notifications","id" => null];
                        OneSignalPush::sendPushNotification($message, "event_invitation", $data, $user_id, $friend, $event_invitation->id);
                    }
                }
                return APIHelper::makeAPIResponse(true, "Invite Successful", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function inviteAllToEvent(InviteAllToEventRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $user_id = $user->id;
            $friends = $request->input('friends');
            $event_id = $request->input('event_id');
            $event = Event::myEvent()->find($event_id);
            if ($event != null) {
                EventInvitee::where(['user_id' => $user_id, 'event_id' => $event_id])->delete();
                $has_unregistered_people = false;
                $unregistered_people_mobiles = [];
                foreach ($friends as $friend) {
                    $mobile = APIHelper::formatMobile($friend);
                    $db_user = User::where("mobile", $mobile)->first();

                    // Check and ignore current user mobile
                    if ($user->mobile != $mobile) {
                        if ($db_user != null) {
                            // user in the DB then create DB record
                            $event_invitation = EventInvitee::updateOrCreate(['user_id' => $user_id, 'event_id' => $event_id, 'invitee_id' => $db_user->id], ['status' => 2]);
                            if ($event_invitation) {
                                // Create push notification
                                $creator_full_name = $user->first_name . " " . $user->last_name;
                                $format_date = Carbon::parse($event->date . " " . $event->time)->format('l jS \\of F Y h:i A');
                                $message = $creator_full_name . "  invited you to \"" . $event->name . "\" event held on " . $format_date . " at " . $event->location_name;
                                $data = ["type" => "notifications","id" => null];
                                OneSignalPush::sendPushNotification($message, "event_invitation", $data, $user_id, $db_user->id, $event_invitation->id);
                            }
                        } else {
                            // user not in the DB
                            $has_unregistered_people = true;
                            $unregistered_people_mobiles[] = $mobile;
                        }
                    }
                }

                if ($has_unregistered_people) {
                    $creator_full_name = $user->first_name . " " . $user->last_name;
                    $format_date = Carbon::parse($event->date . " " . $event->time)->format('l jS \\of F Y h:i A');
                    $message = $creator_full_name . "  invited you to \"" . $event->name . "\" event held on " . $format_date . " at " . $event->location_name . " Please go to this link " . $event->share_link;
                    // send bulk SMS
                    DHIRAAGU::sendSMS($message, $unregistered_people_mobiles);
                    APIHelper::createInactiveUsersAndInviteToEvent($unregistered_people_mobiles, $event, $user);
                }

                return APIHelper::makeAPIResponse(true, "Invite Successful", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getChatMembersByEventId(Request $request, $event_id)
    {
        try {
            $event = Event::private()->find($event_id);
            if ($event != null) {
                $invitees = EventInvitee::with(["invitee" => function ($query) {
                    $query->select('id', 'first_name', 'last_name', 'email', 'mobile', 'image', 'status');
                }])->where('event_id', $event_id)->get();
                $chat_members = $invitees->pluck('invitee');

                $created_user = User::select(['id', 'first_name', 'last_name', 'email', 'mobile', 'image', 'status'])->find($event->created_by);
                $chat_members->push($created_user);
                $chat_members = $chat_members->sortBy("first_name")->flatten();

                $final_result = [];
                foreach ($chat_members as $chat_member) {
                    $chat_member["is_current_user"] = $chat_member["id"] == $created_user->id;
                    array_push($final_result, $chat_member);
                }

                if (count($final_result)) {
                    return APIHelper::makeAPIResponse(true, "Done", $final_result, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Contact on this event was not found", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
