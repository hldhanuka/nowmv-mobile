<?php

namespace App\Http\Controllers\API;

use App\Ad;
use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Offer;
use App\OfferCategory;
use App\TargetOfferUser;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    public function getAllOffers(Request $request)
    {
        try {
            $offers = new Offer();
            $offers = $this->filterOfferByGenderAndAge($offers);
            $offers = $this->filterOfferByTargetOfferUsers($offers);
            return APIHelper::makeAPIResponse(true, "Done", $offers, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getOfferByCategoryId(Request $request, $category)
    {
        try {
            if ($category == "all") {
                $offers = new Offer();
                $offers = $this->filterOfferByGenderAndAge($offers);
                $offers = $this->filterOfferByTargetOfferUsers($offers);
                $selected_ad = Ad::offer()->active()->inRandomOrder()->first();
                if ($selected_ad != null) {
                    $selected_ad->content_type = "ad";
                    $offers = APIHelper::pushObjectToMiddleOfArray($offers, $selected_ad, 2);
                }
            } else {
                $offer_category = OfferCategory::find($category)->active()->get();
                if ($offer_category != null) {
                    $offers = new Offer();
                    $offers = $offers->where('category_id', $category);
                    $offers = $this->filterOfferByGenderAndAge($offers);
                    $offers = $this->filterOfferByTargetOfferUsers($offers);
                } else {
                    return APIHelper::makeAPIResponse(false, "Offer category not found", null, 404);
                }
            }

            return APIHelper::makeAPIResponse(true, "Done", $offers, 200);

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function filterOfferByGenderAndAge($offers)
    {
        $user = APIHelper::loggedUser();
        if ($user->age) {
            $offers = $offers->where(function ($query) use ($user) {
                $query->where("min_age", "<=", $user->age)->orWhereNull("min_age");
            });
            $offers = $offers->where(function ($query) use ($user) {
                $query->where("max_age", ">=", $user->age)->orWhereNull("max_age");
            });
        }
        if ($user->gender == "male") {
            $offers = $offers->where("for_male", 1);
        }
        if ($user->gender == "female") {
            $offers = $offers->where("for_female", 1);
        }
        $offers = $offers->active()->latest()->get();
        return $offers;
    }

    public function filterOfferByTargetOfferUsers($offers)
    {
        $user = APIHelper::loggedUser();
        $current_user_targeted_offer_ids =  TargetOfferUser::where('user_id', $user->id)->pluck('offer_id')->toArray();
        $filtered_offers = $offers->filter(function ($value) use ($current_user_targeted_offer_ids) {
            return (!$value->is_target_offer) ||  ($value->is_target_offer && in_array($value->id, $current_user_targeted_offer_ids));
        });
        return $filtered_offers->values();
    }
}
