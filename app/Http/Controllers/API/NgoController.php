<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Ngo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NgoController extends Controller
{
    public function getNgosList(Request $request)
    {
        try {
            $ngos = Ngo::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $ngos, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNgoById(Request $request, $id)
    {
        try {
            $ngo = Ngo::with(['ngoBadges', 'ngoImages'])->where('id', $id)->active()->first();
            if ($ngo != null) {
                return APIHelper::makeAPIResponse(true, "Done", $ngo, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "NGO not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
