<?php

namespace App\Http\Controllers\API;

use App\EventInvitee;
use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\PushNotificationInvitationResponseRequest;
use App\PushNotification;
use Illuminate\Http\Request;

class PushNotificationController extends Controller
{
    public function getNotifications(Request $request)
    {
        $user = APIHelper::loggedUser();

        // Pagination
        $limit = $request->input("limit");
        $offset = $request->input("offset");

        $all_notifications = PushNotification::select("id", "from", "seen", "message", "type", "redirect_type", "redirect_id", "relation_id", "created_at")
            ->where('to', $user->id)
            ->orderBy("created_at", "DESC")
            ->offset($offset)
            ->take($limit)
            ->get();
        $seen_notifications = PushNotification::where('to', $user->id)->update(["seen" => 1]);
        return APIHelper::makeAPIResponse(true, "Done", $all_notifications, 200);
    }

    public function seenAllNotifications(Request $request)
    {
        $user = APIHelper::loggedUser();
        PushNotification::where("to", $user->id)->update(['seen' => 1]);
        return APIHelper::makeAPIResponse(true, "Notification updated", null, 200);

    }

    public function getNewNotificationCount(Request $request)
    {
        $user = APIHelper::loggedUser();
        $data["unread_count"] = PushNotification::where("to", $user->id)->where("seen", 0)->count();
        return APIHelper::makeAPIResponse(true, "Done", $data, 200);
    }

    public function removeNotification(Request $request)
    {
        $user = APIHelper::loggedUser();
        $notification = PushNotification::find($request->input('notification_id'));
        if ($notification != null) {
            if ($notification->to == $user->id) {
                $notification->delete();
                return APIHelper::makeAPIResponse(true, "Notification delete Successful", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Unauthorized action", null, 403);
            }

        } else {
            return APIHelper::makeAPIResponse(false, "Notification not found", null, 404);
        }
    }

    public function invitationResponse(PushNotificationInvitationResponseRequest $request)
    {
        $user = APIHelper::loggedUser();
        $invitation_id = $request->input('invitation_id');
        $notification_id = $request->input('notification_id');
        $type = $request->input('type');
        $status = $request->input('status');
        $invitation = null;

        // TODO Add else if condition when add new type
        if ($type == "event_invitation") {
            $invitation = EventInvitee::with("event.createdUser")->where("id", $invitation_id)->where("invitee_id", $user->id)->first();
        }

        if ($invitation != null) {
            $invitation->status = $status;
            $result = $invitation->save();
            if ($result) {
                $push_type = null;

                if ($type == "event_invitation") {
                    $push_type = "event";
                    if ($status == 1) {
                        $message = "Hey " . $invitation->event->createdUser->first_name . ", " . $user->first_name . " accepted your invitation to \"" . $invitation->event->name . "\" on " . APIHelper::setDhiraaguDateFormat($invitation->event->date . " " . $invitation->event->time, "datetime");
                    } else {
                        $message = "Hey " . $invitation->event->createdUser->first_name . ", " . $user->first_name . " rejected your invitation to \"" . $invitation->event->name . "\" on " . APIHelper::setDhiraaguDateFormat($invitation->event->date . " " . $invitation->event->time, "datetime");
                    }
                }

                // Remove notification after respond
                $notification = PushNotification::find($notification_id);
                if ($notification != null) {
                    $notification->delete();
                }

                $id = $invitation->event->id;
                $data = ["type" => "event", "id" => $id != null ? (string)$id : $id];
                OneSignalPush::sendPushNotification($message, $push_type, $data, $user->id, $invitation->user_id, null);
                return APIHelper::makeAPIResponse(true, "Invitation Update Successful", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }

        } else {
            return APIHelper::makeAPIResponse(false, "Invitation not found", null, 404);
        }
    }

    public function sendCustomPushNotification(Request $request)
    {
        $user = APIHelper::loggedUser();
        $message = "Testing Push Notification";
        $type = "test";
        $data = $request->input();
        $sender_id = 0; // System
        $receiver_id = $user->id;
        $relation_id = null;
        OneSignalPush::customPushNotification($message, $type, $data, $sender_id, $receiver_id, $relation_id);
        return APIHelper::makeAPIResponse(true, "Done", null, 200);
    }
}
