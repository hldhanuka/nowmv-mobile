<?php

namespace App\Http\Controllers\API;

use App\Ad;
use App\Donation;
use App\UserBadge;
use App\UserDonation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\APIHelper;

class DonationController extends Controller
{
    public function getDonationsList(Request $request)
    {

        try {
            $donations = Donation::whereHas('ngo', function($q){
                $q->active();
            })->active()->latest()->get();
            if (count($donations)) {
                $selected_ad = Ad::donation()->active()->inRandomOrder()->first();
                if ($selected_ad != null) {
                    $selected_ad->content_type = "ad";
                    $donations = APIHelper::pushObjectToMiddleOfArray($donations, $selected_ad, 2);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Donations not found", null, 404);
            }
            return APIHelper::makeAPIResponse(true, "Done", $donations, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getDonationById(Request $request, $id)
    {
        try {
            $user = APIHelper::loggedUser();
            $donation = Donation::with('donationBadges')->whereHas('ngo', function($q){
                $q->active();
            })->where('id', $id)->active()->first();
            if ($donation != null) {
                $user_total_amount = UserDonation::myDonations()->where('donation_id', $donation->id)->active()->sum('amount');
                $user_badge_count = UserBadge::where(['user_id' => $user->id, 'donation_id' => $donation->id])->active()->count();
                $donation->user_total_amount = floatval($user_total_amount);
                $donation->user_badge_count = $user_badge_count;
                return APIHelper::makeAPIResponse(true, "Done", $donation, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Donation not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

}
