<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\SubmitRecipeRatingRequest;
use App\Recipe;
use App\RecipeRating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeRatingController extends Controller
{
    public function getUserRatingByRecipeId(Request $request, $recipe_id)
    {
        try {
            $recipe = Recipe::active()->find($recipe_id);
            if ($recipe != null) {
                $recipe_rate = RecipeRating::where(['recipe_id' => $recipe_id, 'created_by' => APIHelper::loggedUser()->id])->first();
                if ($recipe_rate != null) {
                    return APIHelper::makeAPIResponse(true, "Done", $recipe_rate, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Recipe rating not found", null, 404);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Recipe not found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function submitRecipeRating(SubmitRecipeRatingRequest $request)
    {
        try {
            $recipe_id = $request->input("recipe_id");
            $rate = $request->input("rate");
            $recipe = Recipe::active()->find($recipe_id);
            if ($recipe != null) {
                $result = RecipeRating::updateOrCreate(['recipe_id' => $recipe_id, 'created_by' => APIHelper::loggedUser()->id], ['rate' => $rate]);
                if($result){
                    $response = ["average_rating"=>$recipe->average_rating];
                    return APIHelper::makeAPIResponse(true, "Successfully submitted", $response, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, 'Service error', null, 500);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Recipe not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, 'Service error', null, 500);
        }
    }
}
