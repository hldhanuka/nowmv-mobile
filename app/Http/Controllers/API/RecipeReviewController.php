<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\RemoveRecipeReviewRequest;
use App\Http\Requests\API\SubmitRecipeReviewRequest;
use App\Http\Requests\API\UpdateRecipeReviewRequest;
use App\Recipe;
use App\RecipeReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeReviewController extends Controller
{
    public function submitRecipeReview(SubmitRecipeReviewRequest $request)
    {
        try {
            $recipe_id = $request->input("recipe_id");
            $comment = $request->input("comment");
            $recipe = Recipe::find($recipe_id);
            if ($recipe != null) {
                RecipeReview::insert(['recipe_id' => $recipe_id, 'comment' => $comment, 'status' => 1, 'created_by' => APIHelper::loggedUser()->id, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
                return APIHelper::makeAPIResponse(true, "Successfully submitted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Recipe not found", null, 400);
            }


        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updateRecipeReview(UpdateRecipeReviewRequest $request)
    {
        try {
            $id = $request->input("id");
            $comment = $request->input("comment");
            $recipe_review = RecipeReview::where('created_by', APIHelper::loggedUser()->id)->find($id);
            if ($recipe_review != null) {
                $recipe_review->update(['comment' => $comment]);
                return APIHelper::makeAPIResponse(true, "Successfully updated", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Recipe review not found or not yours", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function removeRecipeReview(RemoveRecipeReviewRequest $request)
    {
        try {
            $id = $request->input("id");
            $recipe_review = RecipeReview::where('created_by', APIHelper::loggedUser()->id)->find($id);
            if ($recipe_review != null) {
                $recipe_review->delete();
                return APIHelper::makeAPIResponse(true, "Successfully deleted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Recipe review not found or not yours", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
