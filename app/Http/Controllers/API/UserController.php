<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Http\Requests\API\GetFriendsRequest;
use App\Http\Requests\API\UpdateUserImageRequest;
use App\Http\Requests\API\UpdateUserProfileRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getAllContacts(Request $request)
    {
        try {
            $contacts = User::select(['id','first_name','last_name','email','mobile','image','status'])->role('customer')->active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $contacts, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getUserById(Request $request, $id)
    {
        try {
            $user = User::where('id', $id)->active()->first();
            if ($user != null) {
                return APIHelper::makeAPIResponse(true, "Done", $user, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getUserProfile(Request $request)
    {
        try {
            $user = APIHelper::loggedUser(['id','first_name','last_name','email','mobile','image','birthday','address','gender','status', 'prayer_location_id']);

            if ($user!=null) {
                return APIHelper::makeAPIResponse(true, "Done", $user, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function updateUserProfile(UpdateUserProfileRequest $request)
    {
        try {
            $first_name = trim($request->input('first_name'));
            $last_name = trim($request->input('last_name'));
            $email = trim($request->input('email'));
            $birthday = trim($request->input('birthday'));
            $address = trim($request->input('address'));
            $gender = trim($request->input('gender'));

            $user = APIHelper::loggedUser();
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user->birthday = $birthday;
            $user->address = $address;
            $user->gender = $gender;
            $result = $user->save();

            if ($result!=null) {
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function updateUserImage(UpdateUserImageRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $upload_result = CMSHelper::fileUpload("image", "uploads/profile/");
            $user->image = $upload_result;
            $result = $user->save();

            if ($result!=null) {
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function getFriends(GetFriendsRequest $request)
    {
        try {
            $numbers = $request->input('numbers');
            foreach ($numbers as $key => $value) {
                if ($value[0] == "#" || $value[0] == "*") {
                    unset($numbers[$key]);
                }

                if ($value[0] == '+') {
                    $numbers[$key] = substr($value, 3, strlen($value));
                    $value = $numbers[$key];
                }

                if ($value[0] == '0') {
                    $numbers[$key] = ltrim($value, "0");
                }

            }

            $numbers = array_values($numbers);

            $del_val = APIHelper::loggedUser()->mobile;
            $numbers = array_filter($numbers, function ($e) use ($del_val) {
                return ($e !== $del_val);
            });

            $data = User::whereIn('mobile', $numbers)->select('id', 'first_name', 'last_name', 'mobile', 'image', 'status')->get();
            return APIHelper::makeAPIResponse(true, "Done", $data, 200);

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }
}
