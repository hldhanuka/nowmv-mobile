<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\PrayerLocation;
use App\PrayerTime;
use App\ReminderNotification;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SyncController extends Controller
{
    public function getEventReminderNotifications()
    {
        $user = APIHelper::loggedUser();
        $data = ReminderNotification::where('user_id', $user->id)
            ->where('date_time', '>', now()->toDateTimeString())
            ->where('type', 'event')->get();
        return APIHelper::makeAPIResponse(true, "Done", $data, 200);
    }

    public function getBusReminderNotifications()
    {
        $user = APIHelper::loggedUser();
        $data = ReminderNotification::where('user_id', $user->id)
            ->where('date_time', '>', now()->toDateTimeString())
            ->where('type', 'bus')->get();
        return APIHelper::makeAPIResponse(true, "Done", $data, 200);
    }

    public function getFlightReminderNotifications()
    {
        $user = APIHelper::loggedUser();
        $data = ReminderNotification::where('user_id', $user->id)
            ->where('date_time', '>', now()->toDateTimeString())
            ->where('type', 'flight')->get();
        return APIHelper::makeAPIResponse(true, "Done", $data, 200);
    }

    public function getPrayerReminderNotificationsByRange(Request $request)
    {
        $start_date = Carbon::parse($request->input('start_date'))->toDateString();
        if ($start_date == null) {
            $start_date = now()->toDateString();
        }

        $end_date = Carbon::parse($request->input('end_date'))->toDateString();
        if ($end_date == null) {
            $end_date = now()->toDateString();
        }

        $period = CarbonPeriod::create($start_date, $end_date);
        $user = APIHelper::loggedUser();
        $user_id = $user->id;
        $prayer_reminder_before = config('dhiraagu.prayer_reminder_before');
        $prayer_types = ['fajr', 'dhuhr', 'asr', 'maghrib', 'isha'];
        $result_data = [];
        $user_prayer_location_id = $user->prayer_location_id;
        // check user location is set
        if ($user_prayer_location_id != null) {
            $prayer_location = PrayerLocation::find($user_prayer_location_id);
            // check reminder is active
            if (APIHelper::getUserOrUsersByActiveSetting("prayer_reminder", $user_id)) {
                // loop all the dates between the date range
                foreach ($period as $date) {
                    $english_day = strtolower($date->englishDayOfWeek);

                    $prayer_times = PrayerTime::where([
                        'atoll_id' => $prayer_location->atoll_id,
                        'day' => $date->day,
                        'month_id' => $date->month,
                    ])->get();

                    if (count($prayer_times)) {
                        foreach ($prayer_types as $prayer_type) {
                            if (in_array($english_day, json_decode($user->userSetting->prayer_reminder, true)[$prayer_type])) {
                                $time_adjustment = $prayer_location->time_adjustment;
                                $all_selected_prayer_times = clone $prayer_times;
                                $selected_prayer_type = $all_selected_prayer_times->where("prayer_id", config('dhiraagu.prayer_types.' . $prayer_type))->first();
                                $public_fajr_datetime = Carbon::parse($date->toDateString() . " " . $selected_prayer_type->time);
                                $user_fajr_datetime = $public_fajr_datetime->copy()->addMinute($time_adjustment);
                                $date_time = $user_fajr_datetime->copy()->subMinutes($prayer_reminder_before)->toDateTimeString();
                                $temp_data = [
                                    "type" => "prayer",
                                    "prayer_type" => $prayer_type,
                                    "user_id" => $user_id,
                                    "content" => Str::ucfirst($prayer_type) . " prayer at " . APIHelper::setDhiraaguDateFormat($user_fajr_datetime, "time"),
                                    "relation_id" => $selected_prayer_type->id,
                                    "date_time" => $date_time,
                                ];
                                array_push($result_data, $temp_data);
                            }
                        }
                    } else {
                        $result_data = [];
                    }
                }
                return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User prayer notification settings off", null, 400);
            }
        } else {
            return APIHelper::makeAPIResponse(false, "User location doesn't exist", null, 400);
        }
    }

    public function getPrayerTempDataArray($prayer_mate_time, $user_id, $type, $date)
    {
        $now = Carbon::now();
        $date_time = Carbon::parse($date->toDateString() . " " . $prayer_mate_time[$type]["time"]);
        $prayer_reminder_before = config('dhiraagu.prayer_reminder_before');
        $schedule_time = $date_time->copy()->subMinutes($prayer_reminder_before)->toDateTimeString();
        $content = Str::ucfirst($type) . " prayer at " . APIHelper::setDhiraaguDateFormat($date_time, "time");
        return [
            "type" => "prayer",
            "user_id" => $user_id,
            "content" => $content,
            "date_time" => $schedule_time,
            "status" => 1,
            "created_at" => $now,
            "updated_at" => $now,
        ];
    }

}
