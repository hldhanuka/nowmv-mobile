<?php

namespace App\Http\Controllers\API;

use App\Ad;
use App\Helpers\APIHelper;
use App\News;
use App\NewsAgency;
use App\NewsCategory;
use App\NewsTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function getAllNews(Request $request)
    {
        try {
            // Pagination
            $limit = $request->input("limit");
            $offset = $request->input("offset");
            $news = News::active()->latest()->offset($offset)->take($limit)->get();

            return APIHelper::makeAPIResponse(true, "Done", $news, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getNewsById(Request $request, $id)
    {
        try {
            $news = News::where('id', $id)->active()->first();
            if ($news != null) {
                return APIHelper::makeAPIResponse(true, "Done", $news, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "News not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsByCategoryId(Request $request, $category)
    {
        try {
            // Pagination
            $limit = $request->input("limit");
            $offset = $request->input("offset");

            if ($category == "all") {
                $news_by_category = News::active()->latest()->offset($offset)->take($limit)->get();
                $selected_ad = Ad::news()->active()->inRandomOrder()->first();
                if ($selected_ad != null) {
                    $selected_ad->content_type = "ad";
                    $news_by_category = APIHelper::pushObjectToMiddleOfArray($news_by_category, $selected_ad, 2);
                }
            } else if ($category == "latest") {
                $news_by_category = News::active()->orderBy('published_at', 'DESC')->offset($offset)->take($limit)->get();
            } else {
                $news_category = NewsCategory::find($category);
                if ($news_category != null) {
                    $news_by_category = News::where('category_id', $category)->active()->latest()->offset($offset)->take($limit)->get();
                } else {
                    return APIHelper::makeAPIResponse(false, "News category not found", null, 404);
                }
            }
            return APIHelper::makeAPIResponse(true, "Done", $news_by_category, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsByAgencyId(Request $request, $agency_id)
    {
        try {
            // Pagination
            $limit = $request->input("limit");
            $offset = $request->input("offset");

            $news_agency = NewsAgency::find($agency_id);
            if ($news_agency != null) {
                $news_by_agency = News::where('news_agency_id', $agency_id)->active()->latest()->offset($offset)->take($limit)->get();
            } else {
                return APIHelper::makeAPIResponse(false, "News agency not found", null, 404);
            }
            return APIHelper::makeAPIResponse(true, "Done", $news_by_agency, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsByTagId(Request $request, $tag_id)
    {
        try {
            // Pagination
            $limit = $request->input("limit");
            $offset = $request->input("offset");

            $news_tag = NewsTag::find($tag_id);
            if ($news_tag != null) {
                $all_news = News::active()->latest()->get();
                $news_by_tag = $all_news->filter(function ($value) use ($tag_id) {
                    return in_array($tag_id, collect($value->tag_data)->pluck('id')->toArray());
                })->splice($offset)->take($limit)->flatten();
            } else {
                return APIHelper::makeAPIResponse(false, "News agency not found", null, 404);
            }
            return APIHelper::makeAPIResponse(true, "Done", $news_by_tag, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getFoodNews(Request $request)
    {
        try {
            // Pagination
            $limit = $request->input("limit");
            $offset = $request->input("offset");

            $food_news_type = NewsCategory::where("title", "Food")->first();
            $food_news = News::where('category_id', $food_news_type->id)->active()->latest()->offset($offset)->take($limit)->get();;

            $selected_ad = Ad::food()->active()->inRandomOrder()->first();
            if ($selected_ad != null) {
                $selected_ad->content_type = "ad";
                $food_news = APIHelper::pushObjectToMiddleOfArray($food_news, $selected_ad, 2);
            }

            return APIHelper::makeAPIResponse(true, "Done", $food_news, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsViewById(Request $request, $id)
    {
        try {
            $news = News::where('id', $id)->active()->first();
            if ($news != null) {
                return view('app_pages.news_html_view', ["news" => $news]);
            } else {
                return abort(404);
            }
        } catch (\Exception $e) {
            report($e);
            return abort(500);
        }
    }

}
