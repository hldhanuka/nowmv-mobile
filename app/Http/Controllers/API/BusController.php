<?php

namespace App\Http\Controllers\API;

use App\BusHaltSchedule;
use App\BusLocation;
use App\BusRoute;
use App\BusRouteSchedule;
use App\Helpers\APIHelper;
use App\Http\Requests\API\GetAvailableBusesRequest;
use App\ScheduleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusController extends Controller
{
    public function getAllLocations(Request $request)
    {
        try {
            $locations = BusLocation::active()->orderBy('name')->get();
            return APIHelper::makeAPIResponse(true, "Done", $locations, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getAvailableBuses(GetAvailableBusesRequest $request)
    {
        try {
            $today = Carbon::now()->toDateString();
            $from = $request->input("from");
            $to = $request->input("to");
            $all_bus_schedule_types = ScheduleType::busSchedule()->active()->priority()->get();
            $selected_schedule_type = null;
            $final_schedule_data = [];

            foreach ($all_bus_schedule_types as $schedule_type) {
                foreach ($schedule_type->date_array as $item) {
                    if ($today == $item["date"]) {
                        $selected_schedule_type = $schedule_type->id;
                        break 2;
                    }
                }
            }

            if ($selected_schedule_type != null) {
                $main_route = BusRoute::where(["from" => $from, "to" => $to])->first();
                if ($main_route != null) {
                    // Is Main Route
                    $schedule_data_array = BusRouteSchedule::with(["busRoute", "busRoute.fromLocation", "busRoute.toLocation", "busRoute.busType"])->active()->where(["schedule_type_id" => $selected_schedule_type, "bus_route_id" => $main_route->id])->orderBy('starting_time')->get();
                    $final_schedule_data = $this->createScheduleDataArray($schedule_data_array, $today, $from, $to);
                } else {
                    // Check Halts
                    $bus_route_ids = BusRoute::active()->whereHas('busHaltSchedule', function ($query) use ($from) {
                        $query->where('halt_id', $from);
                    })->whereHas('busHaltSchedule', function ($query) use ($to) {
                        $query->where('halt_id', $to);
                    })->pluck('id')->toArray();

                    $schedule_data_array = BusRouteSchedule::with(["busRoute", "busRoute.fromLocation", "busRoute.toLocation", "busRoute.busType"])->active()->where("schedule_type_id", $selected_schedule_type)->whereIn("bus_route_id", $bus_route_ids)->orderBy('starting_time')->get();
                    $final_schedule_data = $this->createScheduleDataArray($schedule_data_array, $today, $from, $to);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "No Available Buses", null, 404);
            }

            if (count($final_schedule_data)) {
                return APIHelper::makeAPIResponse(true, "Done", $final_schedule_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "No Available Buses", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function createScheduleDataArray($schedule_data_array, $today, $from, $to)
    {
        $final_schedule_data = [];
        foreach ($schedule_data_array as $schedule_data) {
            $schedule_time = $today . " " . $schedule_data->starting_time;
            if (Carbon::parse($schedule_time)->isFuture()) {
                $temp_data = [];
                $full_schedule_time = APIHelper::getBusRouteAllSchedule($schedule_data, $today, $from, $to);
                if ($full_schedule_time["valid_direction"]) {
                    $temp_data["route_name"] = $schedule_data->busRoute->name;
                    $temp_data["from_location"] = $schedule_data->busRoute->fromLocation->name;
                    $temp_data["to_location"] = $schedule_data->busRoute->toLocation->name;
                    $temp_data["bus_type"] = $schedule_data->busRoute->busType->name;
                    $temp_data["logo"] = $schedule_data->busRoute->busType->logo;
                    $temp_data["schedule_time"] = $full_schedule_time["schedule_time"];
                    $temp_data["schedule_data"] = $full_schedule_time["schedule_list"];
                    $final_schedule_data[] = $temp_data;
                }
            }
        }
        return $final_schedule_data;
    }
}
