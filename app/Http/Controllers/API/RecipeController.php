<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\UpdateRecipeFavouriteRequest;
use App\Recipe;
use App\RecipeRating;
use App\UserFavouriteRecipe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeController extends Controller
{
    public function getRecipeById(Request $request, $id)
    {
        try {
            $recipe = Recipe::with(['restaurant','reviews', 'reviews.createdUser'])
                ->withCount('reviews')
                ->withCount('userFavouriteRecipe')
                ->where('id', $id)
                ->active()->first();

            if ($recipe != null) {
                return APIHelper::makeAPIResponse(true, "Done", $recipe, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Recipe not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getRecipeList(Request $request)
    {
        try {
            $recipes = Recipe::withCount('reviews')
                ->withCount('userFavouriteRecipe')
                ->active()->latest()->get();
            return APIHelper::makeAPIResponse(true, "Done", $recipes, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updateRecipeFavourite(UpdateRecipeFavouriteRequest $request)
    {
        try {
            $is_favourite = $request->input('is_favourite');
            $recipe_id = $request->input('recipe_id');

            if ($is_favourite) {
                $recipe_favourite = UserFavouriteRecipe::updateOrCreate(['user_id' => APIHelper::loggedUser()->id, 'recipe_id' => $recipe_id]);
            } else {
                $recipe_favourite = UserFavouriteRecipe::where(['user_id' => APIHelper::loggedUser()->id, 'recipe_id' => $recipe_id])->delete();
            }
            return APIHelper::makeAPIResponse(true, "Update Successful", null, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }


}
