<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FlightController extends Controller
{

    public function getFlightArrivals(Request $request)
    {

        $flight_data = $this->getFlightData(config('dhiraagu.flight_data.arrivals'), "ARRIVALS");

        // Pagination
        $limit = $request->input("limit");
        $offset = $request->input("offset");
        $filter = $request->input("filter");
        if ($limit != null && $offset != null) {
            if ($filter != null) {
                $flight_data = collect($flight_data)->where("status", $filter)->splice($offset)->take($limit);
            } else {
                $flight_data = collect($flight_data)->splice($offset)->take($limit);
            }
        }

        return APIHelper::makeAPIResponse(true, "Done", $flight_data, 200);
    }

    public function getFlightDepartures(Request $request)
    {
        $flight_data = $this->getFlightData(config('dhiraagu.flight_data.departures'), "DEPARTURE");

        // Pagination
        $limit = $request->input("limit");
        $offset = $request->input("offset");
        $filter = $request->input("filter");
        if ($limit != null && $offset != null) {
            if ($filter != null) {
                $flight_data = collect($flight_data)->where("status", $filter)->splice($offset)->take($limit);
            } else {
                $flight_data = collect($flight_data)->splice($offset)->take($limit);
            }
        }

        return APIHelper::makeAPIResponse(true, "Done", $flight_data, 200);
    }

    public function getFlightData($url, $type)
    {
        try {
            $air_port = config('dhiraagu.flight_data.air_port');
            $page = str_replace(["\n", "\t", "\r"], "", file_get_contents($url));
            $image_base_url = config('dhiraagu.flight_data.image_base_url');
            $doc = new \DOMDocument();
            libxml_use_internal_errors(true);
            $doc->loadHTML($page);
            $tables = $doc->getElementsByTagName('table');
            $flight_data = [];
            $table_index = 1;
            $current_date = "";
            foreach ($tables as $table) {
                if ($table_index == 4) {
                    $tr_index = 0;
                    $last_data = [];
                    foreach ($table->childNodes as $childNode) {
                        if ($tr_index == 1) {
                            try {
                                $current_date = Carbon::parse(trim($childNode->nodeValue))->toDateString();
                            } catch (\Exception $e) {
                                $current_date = Carbon::now()->toDateString();
                            }
                        }
                        if ($tr_index >= 4) {

                            $td_index = 0;
                            if (isset($childNode->childNodes->length) && $childNode->childNodes->length > 5) {
                                $data = [];
                                foreach ($childNode->childNodes as $tdNode) {
                                    if ($td_index == 0) {
                                        $data["image"] = $this->getFlightImageUrl($tdNode->childNodes->item(0)->getAttribute('src'));
                                    } else if ($td_index == 1) {
                                        $data["flight"] = $tdNode->nodeValue;
                                    } else if ($td_index == 2) {
                                        if ($type == "DEPARTURE") {
                                            if ($tdNode->nodeValue == "" && isset($last_data["origin"])) {
                                                $destination = $last_data["destination"];
                                            } else {
                                                $destination = $tdNode->nodeValue;
                                            }
                                            $data["origin"] = $air_port;
                                            $data["destination"] = str_replace("\\", "/", $destination);
                                        } else {
                                            if ($tdNode->nodeValue == "" && isset($last_data["origin"])) {
                                                $origin = $last_data["origin"];
                                            } else {
                                                $origin = $tdNode->nodeValue;
                                            }
                                            $data["origin"] = str_replace("\\", "/", $origin);
                                            $data["destination"] = $air_port;
                                        }

                                    } else if ($td_index == 3) {
                                        if ($tdNode->nodeValue == "" && isset($last_data["time"])) {
                                            $time = $last_data["time"];
                                        } else {
                                            $time = $tdNode->nodeValue;
                                        }
                                        $data["time"] = $time;
                                        $data["date_time"] = Carbon::parse($current_date . " " . $time)->toDateTimeString();
                                    } else if ($td_index == 4) {
                                        $data["estimated"] = $tdNode->nodeValue != " " ? Carbon::parse($current_date . " " . $tdNode->nodeValue)->toDateTimeString() : null;
                                    } else if ($td_index == 5) {
                                        $data["status"] = $tdNode->nodeValue != " " ? strtoupper($tdNode->nodeValue) : "PENDING";
                                    }
                                    $td_index++;
                                }
                                $last_data = $data;
                                array_push($flight_data, $data);
                            } else if (isset($childNode->childNodes->length) && $childNode->childNodes->length > 0) {
                                foreach ($childNode->childNodes as $tdNode) {
                                    if (strpos($tdNode->nodeValue, 'PASSENGER ARRIVALS') !== false) {
                                        // Ignore row
                                    } else {
                                        try {
                                            foreach ($tdNode->childNodes as $last_child) {
                                                $current_date = Carbon::parse(trim($tdNode->nodeValue))->toDateString();
                                            }
                                        } catch (\Exception $e) {
                                        }
                                    }
                                }
                            }
                        }
                        $tr_index++;
                    }
                }

                $table_index++;
            }
            $filtered_flight_data = array_map(function ($array) {
                unset($array['time']);
                return $array;
            }, $flight_data);
            return $filtered_flight_data;
        } catch (\Exception $e) {
            report($e);
            return [];
        }
    }

    public function getFlightImageUrl($url_segments)
    {
        $image_base_url = config('dhiraagu.flight_data.image_base_url');
        $destination_path = "flight_logo";
        $old_flight_logo_url = $image_base_url . $url_segments;
        $new_path_segment = $destination_path . $url_segments;

        // CHECK IS S3
        if (config('filesystems.default') == "s3") {
            $s3 = Storage::disk('s3');
            $exists = $s3->exists($destination_path . $url_segments);
            // checking file is exists.
            if ($exists) {
                $new_file_logo = $destination_path . $url_segments;
                return $s3->url($new_file_logo);
            } else {
                $image_content = @file_get_contents($old_flight_logo_url);
                // checking file is exists.
                if ($image_content === false) {
                    return asset('assets/img/flight.gif');
                } else {
                    $path = $s3->put($new_path_segment, $image_content, 'public');
                    return $s3->url($path);
                }
            }
        } else {
            $local_image_url = asset($new_path_segment);
            $image_content = @file_get_contents($local_image_url);
            // checking file is exists.
            if ($image_content === false) {
                return asset('assets/img/flight.gif');
            } else {
                file_put_contents(public_path($new_path_segment), $image_content);
                return public_path($new_path_segment);
            }
        }
    }
}
