<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\MealCategory;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MealCategoryController extends Controller
{
    public function mealCategories(Request $request)
    {
        try {
            $meal_categories = MealCategory::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $meal_categories, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getMealCategoryByRestaurantId(Request $request, $restaurant_id)
    {
        try {
            $restaurant = Restaurant::find($restaurant_id);
            if ($restaurant != null) {
                $meal_category = MealCategory::with('meals')->where('restaurant_id', $restaurant_id)->active()->get();
                if ($meal_category != null) {
                    return APIHelper::makeAPIResponse(true, "Done", $meal_category, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Meal category by restaurant not found", null, 404);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Restaurant not found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
