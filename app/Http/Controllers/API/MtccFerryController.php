<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\GetAvailablePublicFerriesRequest;
use App\MtccRoute;
use App\MtccRouteStop;
use App\MtccRouteStopDay;
use App\MtccRouteStopTime;
use App\MtccServicePoint;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MtccFerryController extends Controller
{
    public function getPublicIslandList(Request $request)
    {
        try {
            try {
                $all_mtcc_bus_locations = MtccServicePoint::with('mapLocation.island')->where("type", "island_port")->get();
                $all_terminal_list = [];

                $index = 1;
                foreach ($all_mtcc_bus_locations as $mtcc_bus_location) {
                    $temp = [];
                    $temp["id"] = $index;
                    $temp["atoll_name"] = $mtcc_bus_location->mapLocation->island->atoll;
                    $temp["point_id"] = $mtcc_bus_location->id;
                    $temp["point_name"] = $mtcc_bus_location->getOriginal('name');
                    $all_terminal_list[] = $temp;
                    $index++;
                }
                return APIHelper::makeAPIResponse(true, "Done", $all_terminal_list, 200);
            } catch (\Exception $e) {
                report($e);
                return APIHelper::makeAPIResponse(false, "Service error", null, 500);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getAvailablePublicFerries(GetAvailablePublicFerriesRequest $request)
    {
        try {
            $date = Carbon::parse($request->input("date"));
            $selected_date = $date->toDateString();
            $date_is_holiday = APIHelper::checkDateIsHoliday($selected_date);
            $from = $request->input("point_id");

            $final_schedule_data = [];

            if ($date_is_holiday) {
                $short_english_day = "hol";
            } else {
                $short_english_day = strtolower($date->shortEnglishDayOfWeek);
            }

            $route = MtccRoute::with(['routeStops', 'originServicePoint', 'destinationServicePoint'])->whereHas('routeStops', function ($query) use ($from) {
                $query->where('originating_service_point', $from);
            })->first();

            // check route exist
            if ($route != null) {

                $route_stops_ids = $route->routeStops()->where('originating_service_point', $from)->pluck('id')->unique();

                $all_route_stops_id = MtccRouteStop::select(["originating_service_point"])->distinct()->where("route_id", $route->id)->groupBy('originating_service_point')->orderBy('sequence')->pluck("originating_service_point")->toArray();
                $all_terminals = MtccServicePoint::whereIn("id", $all_route_stops_id)->where("type", "island_port")->get();

                $final_stops = [];
                foreach ($all_terminals as $terminal) {
                    $temp_stop = [
                        "terminal_id" => $terminal->id,
                        "terminal_name" => $terminal->getOriginal('name'),
                        "schedule_time" => null,
                        "is_current" => $terminal["id"] == $from
                    ];
                    $final_stops[] = $temp_stop;
                }

                $route_stop_days = MtccRouteStopDay::whereIn("route_stop_id", $route_stops_ids)->where("day_types", "like", "%" . $short_english_day . "%")->get()->pluck('id')->toArray();
                $all_schedules = MtccRouteStopTime::whereIn("route_day_id", $route_stop_days)->get();

                foreach ($all_schedules as $schedule) {
                    $schedule_time = Carbon::parse($selected_date . " " . $schedule["departure_time"]);
                    if ($schedule_time->isFuture()) {
                        $temp_schedule = [
                            "route_name" => $route->name,
                            "from_location" => $route->originServicePoint->getOriginal('name'),
                            "to_location" => $route->destinationServicePoint->getOriginal('name'),
                            "duration" => self::getDurationText($route->duration),
                            "schedule_time" => $schedule_time->toDateTimeString(),
                            "end_time" => $schedule_time->copy()->addMinutes($route->duration)->toDateTimeString(),
                            "schedule_data" => $final_stops
                        ];
                        $final_schedule_data[] = $temp_schedule;
                    }
                }
            }

            if (count($final_schedule_data)) {
                return APIHelper::makeAPIResponse(true, "Done", $final_schedule_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "No Available Ferries", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public static function getDurationText($duration)
    {
        $text = "";
        $duration = intval($duration);
        $hours = intval($duration / 60);
        $minutes = $duration % 60;
        if ($hours > 1) {
            $text .= $hours . "hrs";
        } else if ($hours == 1) {
            $text .= $hours . "hr";
        }

        if ($minutes > 1) {
            $text .= " " . $minutes . "mins";
        } else if ($minutes == 1) {
            $text .= " " . $minutes . "min";
        }
        return trim($text);
    }
}
