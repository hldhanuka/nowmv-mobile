<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Libraries\Weather\WEATHER;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WeatherController extends Controller
{
    public function getAllWeatherLocations(Request $request)
    {
        try{
            $url = config('dhiraagu.weather_data.station_list');
            $data = WEATHER::apiCall($url);
            return APIHelper::makeAPIResponse(true,"Done", $data, 200);
        } catch (\Exception $e){
            report($e);
            return APIHelper::makeAPIResponse(false,"Service error", null, 500);
        }
    }

    public function getWeatherLocationDetailById(Request $request, $id)
    {
        try{
            $url = str_replace("{id}", $id, config('dhiraagu.weather_data.station_details'));
            $data = WEATHER::apiCall($url);
            $data = APIHelper::addWeatherImage($data);
            return APIHelper::makeAPIResponse(true,"Done", $data, 200);
        } catch (\Exception $e){
            report($e);
            return APIHelper::makeAPIResponse(false,"Service error", null, 500);
        }
    }
}
