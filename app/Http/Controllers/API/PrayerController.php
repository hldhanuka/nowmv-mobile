<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\GetPrayerCalendarRequest;
use App\Http\Requests\API\GetPrayerMateTimesByRangeRequest;
use App\Http\Requests\API\GetPrayerMateTimesRequest;
use App\Http\Requests\API\SubmitPrayerFastingRequest;
use App\Http\Requests\API\SubmitPrayerLocationRequest;
use App\Http\Requests\API\SubmitPrayerRangeRequest;
use App\Http\Requests\API\SubmitPrayerStatusRequest;
use App\PrayerLocation;
use App\PrayerTime;
use App\PrayerTip;
use App\User;
use App\UserFasting;
use App\UserPrayerTime;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class PrayerController extends Controller
{
    public function getPrayerMateTimesByDate(GetPrayerMateTimesRequest $request)
    {
        try {
            $date = $request->input("date");
            $user = APIHelper::loggedUser();
            $user_prayer_location_id = $user->prayer_location_id;
            if ($user_prayer_location_id != null) {
                $user_prayer_times = APIHelper::getPrayerMateTimeByDateAndUser(Carbon::parse($date), $user, false);
                return APIHelper::makeAPIResponse(true, "Done", $user_prayer_times, 200);

            } else {
                return APIHelper::makeAPIResponse(false, "User location doesn't exist", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getPrayerMateTimesByRange(GetPrayerMateTimesByRangeRequest $request)
    {
        try {
            $period = CarbonPeriod::create($request->input('start_date'), $request->input('end_date'));
            $user = APIHelper::loggedUser();
            $result_data = [];
            $user_prayer_location_id = $user->prayer_location_id;
            if ($user_prayer_location_id != null) {
                foreach ($period as $date) {
                    $formatted_date = $date->toDateString();
                    $data["date"] = $formatted_date;
                    $data["data"] = APIHelper::getPrayerMateTimeByDateAndUser($date, $user, false);
                    $result_data[] = $data;
                }
                return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User location doesn't exist", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitPrayerStatus(SubmitPrayerStatusRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $prayer_time_id = $request->input('prayer_time_id');
            $date = Carbon::parse($request->input('date'));
            $sunrise_id = config('dhiraagu.prayer_types.sunrise');
            $prayer_time = PrayerTime::where('prayer_id', '!=', $sunrise_id)->find($prayer_time_id);
            $status = $request->input('status');

            if ($prayer_time != null) {
                $date_time = Carbon::parse($date->toDateString() . " " . $prayer_time->time)->toDateTimeString();
                $db_prayer_time_date = Carbon::parse($date->year . "-" . $prayer_time->month_id . "-" . $prayer_time->day . " " . $prayer_time->time)->toDateTimeString();
                if ($date_time == $db_prayer_time_date) {
                    $result = UserPrayerTime::updateOrCreate(['user_id' => $user->id, 'prayer_time_id' => $prayer_time_id], ['prayer_location_id' => $user->prayer_location_id, 'status' => $status, 'date_time' => $date_time]);
                    if ($result) {
                        return APIHelper::makeAPIResponse(true, "Done", 200);
                    } else {
                        return APIHelper::makeAPIResponse(false, "User prayer time not updated", null, 500);
                    }
                } else {
                    return APIHelper::makeAPIResponse(false, "Prayer time id and date not matched. Please check again", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Invalid Prayer time id. Please check again", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitPrayerRange(SubmitPrayerRangeRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $prayer_current_location = PrayerLocation::find($user->prayer_location_id);

            if ($prayer_current_location != null) {
                $atoll_id = $prayer_current_location->atoll_id;
                $prayer_times = PrayerTime::where('atoll_id', $atoll_id)->get();
                $user_prayer_times = UserPrayerTime::where('user_id', $user->id)->where('status', 1)->get();
                $period = CarbonPeriod::create($request->input('start_date'), $request->input('end_date'));
                $prayer_types = config('dhiraagu.prayer_types');

                $total_fajr = 0;
                $total_dhuhr = 0;
                $total_asr = 0;
                $total_maghrib = 0;
                $total_isha = 0;

                $user_prayed_fajr = 0;
                $user_prayed_dhuhr = 0;
                $user_prayed_asr = 0;
                $user_prayed_maghrib = 0;
                $user_prayed_isha = 0;

                foreach ($period as $date) {
                    foreach ($prayer_types as $type => $id) {
                        if ($id != 1) {
                            $current_prayer_time = $prayer_times->where('prayer_id', $id)->where('day', $date->day)->where('month_id', $date->month)->first();
                            if ($current_prayer_time != null) {
                                ${"total_" . $type}++;
                                $current_user_prayer_time = $user_prayer_times->where('prayer_time_id', $current_prayer_time->id)->where('date_time', Carbon::parse($date->toDateString() . " " . $current_prayer_time->time))->where('status', 1)->first();
                                if ($current_user_prayer_time != null) {
                                    ${"user_prayed_" . $type}++;
                                }
                            }
                        }
                    }
                }

                $data = [
                    "fajr" => round(($user_prayed_fajr / $total_fajr) * 100, 2),
                    "dhuhr" => round(($user_prayed_dhuhr / $total_dhuhr) * 100, 2),
                    "asr" => round(($user_prayed_asr / $total_asr) * 100, 2),
                    "maghrib" => round(($user_prayed_maghrib / $total_maghrib) * 100, 2),
                    "isha" => round(($user_prayed_isha / $total_isha) * 100, 2),
                ];

                return APIHelper::makeAPIResponse(true, "Done", $data, 200);

            } else {
                return APIHelper::makeAPIResponse(false, "User location doesn't exist", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getPrayerFastingCalendar(GetPrayerCalendarRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $user_fasted_date_array = UserFasting::where('user_id', $user->id)->where('status', 1)->pluck("date")->toArray();
            $period = CarbonPeriod::create($request->input('start_date'), $request->input('end_date'));
            $result_data = [];

            foreach ($period as $date) {
                $formatted_date = $date->toDateString();
                $data["date"] = $formatted_date;
                $data["status"] = in_array($formatted_date, $user_fasted_date_array);
                $result_data[] = $data;
            }
            return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getPrayerStatusCalendar(GetPrayerCalendarRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $prayer_current_location = PrayerLocation::find($user->prayer_location_id);

            if ($prayer_current_location != null) {
                $user_prayer_times = UserPrayerTime::select(DB::raw('date(date_time) as date, count(*) as count'))->where('user_id', $user->id)->where('status', 1)->groupBy('date')->get();
                $period = CarbonPeriod::create($request->input('start_date'), $request->input('end_date'));
                $result_data = [];

                foreach ($period as $date) {
                    $formatted_date = $date->toDateString();
                    $user_prayed_times = $user_prayer_times->where("date", $formatted_date)->first();
                    $data["date"] = $formatted_date;
                    $data["status"] = $user_prayed_times!=null?$user_prayed_times->count==5:false;
                    $result_data[] = $data;
                }
                return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User location doesn't exist", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getPrayerMateTips(Request $request)
    {
        try {
            $prayer_tips = PrayerTip::active()->get();

            if ($prayer_tips != null) {
                return APIHelper::makeAPIResponse(true, "Done", $prayer_tips, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Prayer tip not found", null, 400);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function submitPrayerFasting(SubmitPrayerFastingRequest $request)
    {
        try {
            $prayer_date = Carbon::parse($request->input('date'))->toDateString();
            $status = $request->input('status');

            $result = UserFasting::updateOrCreate(['user_id' => APIHelper::loggedUser()->id, 'date' => $prayer_date], ['status' => $status]);
            if ($result) {
                return APIHelper::makeAPIResponse(true, "Done", 200);
            } else {
                return APIHelper::makeAPIResponse(false, "User prayer fasting not updated", null, 500);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
