<?php

namespace App\Http\Controllers\API;

use App\FeaturedMeal;
use App\Helpers\APIHelper;
use App\Meal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MealController extends Controller
{
    public function getMeals(Request $request)
    {
        try {
            $meals = Meal::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $meals, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getMealById(Request $request, $id)
    {
        try {
            $meal = Meal::with(['restaurant', 'reviews', 'reviews.createdUser'])->where('id', $id)->active()->first();
            if ($meal != null) {
                return APIHelper::makeAPIResponse(true, "Done", $meal, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Meal not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getMealOfTheDay(Request $request)
    {
        try {
            $meal_of_the_day = FeaturedMeal::with(['meal','restaurant'])->active()->get();

            //added this to fix data mismatch on delete meals.
            //Remove this after the data mismatch is fixed.
            $meals = Meal::all()->pluck('id')->toArray();
            $meal_of_the_day_new = [];
            foreach ($meal_of_the_day as $meal){
                if(in_array($meal->meal_id, $meals)){
                    array_push($meal_of_the_day_new,$meal);
                }
            }


            return APIHelper::makeAPIResponse(true, "Done", $meal_of_the_day_new, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

}
