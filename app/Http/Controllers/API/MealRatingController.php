<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\SubmitMealRatingRequest;
use App\Meal;
use App\MealRating;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MealRatingController extends Controller
{
    public function getUserRatingByMealId(Request $request, $meal_id)
    {
        try {
            $meal = Meal::find($meal_id);
            if ($meal != null) {
                $meal_rate = MealRating::where(['meal_id' => $meal_id, 'created_by' => APIHelper::loggedUser()->id])->first();
                if ($meal_rate != null) {
                    return APIHelper::makeAPIResponse(true, "Done", $meal_rate, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Meal rating not found", null, 404);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Meal not found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function submitMealRating(SubmitMealRatingRequest $request)
    {
        try {
            $meal_id = $request->input("meal_id");
            $rate = $request->input("rate");
            $meal = Meal::active()->find($meal_id);
            if ($meal != null) {
                $result = MealRating::updateOrCreate(['restaurant_id' => $meal->restaurant_id, 'meal_id' => $meal_id, 'created_by' => APIHelper::loggedUser()->id], ['rate' => $rate]);
                if($result){
                    $response = ["average_rating"=>$meal->average_rating];
                    return APIHelper::makeAPIResponse(true, "Successfully submitted", $response, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, 'Service error', null, 500);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Meal not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, 'Service error', null, 500);
        }
    }
}
