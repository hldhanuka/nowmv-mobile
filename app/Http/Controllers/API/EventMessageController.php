<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\EventMessage;
use App\Helpers\APIHelper;
use App\Http\Requests\API\EventMessageRemoveMessagesRequest;
use App\Http\Requests\API\EventMessageSendMessageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventMessageController extends Controller
{
    public function getMessagesByEventId(Request $request, $id)
    {
        try {
            $user = APIHelper::loggedUser();
            $event = Event::with(['eventMessages', 'eventInvitees'])
                ->select('id', 'name', 'image', 'status', 'code', 'created_by', 'date', 'time')
                ->find($id);
            if ($event != null) {
                if($event->is_my_event){
                    // Auto set seen when get messages by event id
                    EventMessage::where("event_id", $id)->where("from", "!=", $user->id)->update(["seen"=>1]);
                    return APIHelper::makeAPIResponse(true, "Done", $event, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Unauthorize Action", null, 403);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event Not Found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function sendMessage(EventMessageSendMessageRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $event_id = $request->input("event_id");
            $message = $request->input("message");
            $event = Event::find($event_id);
            if($event != null && $event->is_my_event){
                $event_message = new EventMessage();
                $event_message->event_id = $event_id;
                $event_message->from = $user->id;
                $event_message->seen = 0;
                $event_message->message = $message;
                $event_message->save();
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Unauthorize Action", null, 403);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function seenAllMessagesByEventId(Request $request, $id)
    {
        try {
            $user = APIHelper::loggedUser();
            $event = Event::find($id);
            if ($event != null) {
                if($event->is_my_event){
                    EventMessage::where("event_id", $id)->where("from", "!=", $user->id)->update(["seen"=>1]);
                    return APIHelper::makeAPIResponse(true, "Done", null, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Unauthorize Action", null, 403);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event Not Found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function removeMessages(EventMessageRemoveMessagesRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $message_id = $request->input("message_id");
            $event_message = EventMessage::find($message_id);
            if ($event_message != null) {
                $event_message = EventMessage::where('id', $message_id)->where('from', $user->id)->first();
                if($event_message != null){
                    $event_message->delete();
                    return APIHelper::makeAPIResponse(true, "Done", null, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Unauthorize Action", null, 403);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event Message Not Found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }
}
