<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\SaveInterestsRequest;
use App\UserInterest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserInterestController extends Controller
{

    public function getUserInterests(Request $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $user_interest = UserInterest::with('interest')->where("user_id", $user_id)->get();
            return APIHelper::makeAPIResponse(true, "Done", $user_interest, 200);

        } catch (\Exception $e){
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function saveUserInterests(SaveInterestsRequest $request)
    {
        try {
            $user_interests = [];
            $user_id = APIHelper::loggedUser()->id;
            $interests = $request->input("interests");

            foreach ($interests as $interest){
                $data = [];
                $data["user_id"] = $user_id;
                $data["interest_id"] = $interest;
                $data["created_at"] = Carbon::now()->toDateTimeString();
                $data["updated_at"] = Carbon::now()->toDateTimeString();
                array_push($user_interests, $data);
            }

            UserInterest::where("user_id", $user_id)->delete();

            $result = UserInterest::insert($user_interests);
            if ($result) {
                return APIHelper::makeAPIResponse(true, "User Interest Saved", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service error", null, 500);
            }
        } catch (\Exception $e){
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
