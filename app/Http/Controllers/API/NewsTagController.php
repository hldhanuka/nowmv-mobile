<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\NewsTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsTagController extends Controller
{
    public function newsTags(Request $request)
    {
        try {
            $news_tag = NewsTag::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $news_tag, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsTagById(Request $request, $id)
    {
        try {
            $news_tag = NewsTag::where('id', $id)->active()->first();
            if ($news_tag != null) {
                return APIHelper::makeAPIResponse(true, "Done", $news_tag, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "News tag not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
