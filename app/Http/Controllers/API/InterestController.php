<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Interest;
use App\UserInterest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

class InterestController extends Controller
{
    public function interests(Request $request)
    {
        try {
            $interests = Interest::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $interests, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getInterestById(Request $request, $id)
    {
        try {
            $interest = Interest::where('id', $id)->active()->first();
            if ($interest != null) {
                return APIHelper::makeAPIResponse(true, "Done", $interest, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Interest not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

}
