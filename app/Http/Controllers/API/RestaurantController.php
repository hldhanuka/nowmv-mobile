<?php

namespace App\Http\Controllers\API;

use App\Ad;
use App\Donation;
use App\Event;
use App\FeaturedMeal;
use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Meal;
use App\News;
use App\NewsCategory;
use App\Offer;
use App\Recipe;
use App\Restaurant;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RestaurantController extends Controller
{
    public function getRestaurants(Request $request)
    {
        try {
            $restaurants = Restaurant::active()->get();
            $restaurants = $restaurants->sortByDesc('average_rating')->flatten();
            return APIHelper::makeAPIResponse(true, "Done", $restaurants, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getSuggestedRestaurants(Request $request)
    {
        try {
            $suggested_restaurants = Restaurant::withCount('myReviews')->orderBy('my_reviews_count', 'desc')->active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $suggested_restaurants, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getRestaurantById(Request $request, $id)
    {
        try {
            $restaurant = Restaurant::with(['reviews', 'reviews.createdUser'])->where('id', $id)->active()->first();
            if ($restaurant != null) {
                return APIHelper::makeAPIResponse(true, "Done", $restaurant, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Restaurant not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function findNearbyRestaurants(Request $request)
    {
        try {
            $latitude = $request->input("latitude");
            $longitude = $request->input("longitude");
            $radius = Setting::where('title', 'radius')->first()->text;
            $restaurants = Restaurant::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->active()->having('distance', '<', $radius)->orderBy('distance')->get();
            return APIHelper::makeAPIResponse(true, "Done", $restaurants, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }


    }

    public function getRestaurantHomePageData(Request $request){
        try{

            $restaurant = Restaurant::latest()->active()->get();
            $top_restaurants = Restaurant::active()->get();
            $sorted_top_restaurants = $top_restaurants->where("average_rating", ">", 0)->sortByDesc('average_rating')->flatten();

            /*
             * Old best offer logic (mostly redeemed offer)
            $user_offers =  Offer::select(['id','category_id','title','status','image'])->get();
            foreach ($user_offers as $user_offer){
                $user_offer['user_offer_count'] = $user_offer->userOffers()->count('id');
            }
            $best_user_offers = $user_offers->sortByDesc('user_offer_count')->flatten();
            */

            $offer_controller = new OfferController();
            $best_offers = new Offer();
            $best_offers = $best_offers->select(['id','category_id','title','status','image']);
            $best_offers = $best_offers->where("is_best", 1);
            $best_offers = $offer_controller->filterOfferByGenderAndAge($best_offers);
            $data = [
                    'restaurants' => $restaurant,
                    'top_restaurants' => $sorted_top_restaurants,
                    'best_offers' => $best_offers
            ];
            return APIHelper::makeAPIResponse(true, "Done", $data, 200);
        } catch (\Exception $e){
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getFoodHomePageData(Request $request){
        try{

            $restaurants = Restaurant::active()->latest()->limit(4)->get();
            $ads = Ad::food()->active()->inRandomOrder()->get();
            $recipes = Recipe::withCount('reviews')->withCount('userFavouriteRecipe')->active()->latest()->limit(4)->get();
            $meal_of_the_day = FeaturedMeal::with(['meal','restaurant'])->active()->latest()->limit(4)->get();

            //added this to fix data mismatch on delete meals.
            //Remove this after the data mismatch is fixed.
            $meals = Meal::all()->pluck('id')->toArray();
            $meal_of_the_day_new = [];
            foreach ($meal_of_the_day as $meal){
                if(in_array($meal->meal_id, $meals)){
                    array_push($meal_of_the_day_new,$meal);
                }
            }


            $food_news_type = NewsCategory::where("title","Food")->first();
            $news = News::where('category_id', $food_news_type->id)->active()->latest()->limit(4)->get();

            $data = [
                'restaurants' => $restaurants,
                'ad' => $ads,
                'recipes' => $recipes,
                'meal_of_the_day' => $meal_of_the_day_new,
                'news' => $news
            ];
            return APIHelper::makeAPIResponse(true, "Done", $data, 200);
        } catch (\Exception $e){
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function restaurantsSearch(Request $request)
    {
        $search = $request->input("search");
        $result_data = [];
        $content_limit = 100;

        //Check whether $search is empty or not
        if ($search != null && $search != "") {

            // Restaurant search results
            $restaurant_search_results = Restaurant::whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER (content) like "%' . strtolower($search) .'%"')->active()->get();

            foreach ($restaurant_search_results as $restaurant_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $restaurant_search_result->id;
                    $temp_data["title"] = $restaurant_search_result->title;
                    $temp_data["image"] = $restaurant_search_result->image;
                    $temp_data["content"] = Str::limit($restaurant_search_result->content, $content_limit);
                    $temp_data["type"] = "restaurant";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            // Meals search
            $meals_search_results = Meal::whereRaw('LOWER(title) like "%' . strtolower($search) . '%" or LOWER(content) like "%' . strtolower($search) . '%"')->active()->get();
            foreach ($meals_search_results as $meals_search_result) {
                try {
                    $temp_data = [];
                    $temp_data["id"] = $meals_search_result->id;
                    $temp_data["title"] = $meals_search_result->title;
                    $temp_data["image"] = $meals_search_result->image;
                    $temp_data["content"] = Str::limit($meals_search_result->content, $content_limit);
                    $temp_data["type"] = "meal";
                    array_push($result_data, $temp_data);
                } catch (\Exception $e) {
                    report($e);
                }
            }

            $result_data = array_map(function($array) {
                $array['content'] = $array['content']!=null?$array['content']:"";
                return $array;
            }, $result_data);

            return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);
        } else {
            return APIHelper::makeAPIResponse(false, "Search Field Cannot be Empty", null, 400);
        }
    }

}
