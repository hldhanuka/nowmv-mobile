<?php

namespace App\Http\Controllers\API\External;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\External\CreateAgentNewsRequest;
use App\Http\Requests\API\External\RemoveAgentNewsRequest;
use App\Http\Requests\API\External\UpdateAgentNewsRequest;
use App\News;
use App\NewsCategory;
use App\NewsTag;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function getAgentNews(Request $request)
    {
        try {
            $news = News::whereIn('news_agency_id', $request->my_news_agency_ids)->get();
            return APIHelper::makeAPIResponse(true, "Done", $news, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsTags(Request $request)
    {
        try {
            $news_tags = NewsTag::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $news_tags, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNewsCategories(Request $request)
    {
        try {
            $news_categories = NewsCategory::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $news_categories, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getAgentNewsById(Request $request, $id)
    {
        try {
            $news = News::where('id', $id)->whereIn('news_agency_id', $request->my_news_agency_ids)->first();
            if ($news != null) {
                return APIHelper::makeAPIResponse(true, "Done", $news, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "News not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function createAgentNews(CreateAgentNewsRequest $request)
    {
        try {
            $upload_image_result = CMSHelper::fileUpload("image", "uploads/news/");
            $upload_video_thumbnail_result = CMSHelper::fileUpload("video_thumbnail", "uploads/news/");

            $news = new News();
            $news->category_id = $request->input("category_id");
            $news->title = $request->input("title");
            $news->image = $upload_image_result;
            $news->video = $request->input("video");
            $news->video_thumbnail = $upload_video_thumbnail_result;
            $news->content = $request->input("content");
            $news->author = $request->input("author");
            $news->link = $request->input("link");
            $tags_input = $request->input("tags")!=null?array_map('trim', explode(',', $request->input("tags"))):[];
            $tags_array = array_map(function($value) { return (int)$value; },$tags_input);
            $news->tags = json_encode($tags_array);
            $news->is_featured = $request->input("is_featured");
            $news->status = 2;
            $news->created_by = auth()->user()->id;
            $news->news_agency_id = $request->input("news_agency_id");
            $news->html_content = $request->input("html_content");
            $result = $news->save();
            if ($result) {
                return APIHelper::makeAPIResponse(true, "Done", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service error", null, 500);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updateAgentNews(UpdateAgentNewsRequest $request, $id)
    {
        try {
            $upload_image_result = CMSHelper::fileUpload("image", "uploads/news/");
            $upload_video_thumbnail_result = CMSHelper::fileUpload("video_thumbnail", "uploads/news/");

            $agent_ids = auth()->user()->agents()->pluck("news_agency_id");
            $news = News::whereIn("news_agency_id",$agent_ids)->find($id);
            if($news !=null){
                $news->category_id = $request->input("category_id");
                $news->title = $request->input("title");
                $news->image = $upload_image_result;
                $news->video = $request->input("video");
                $news->video_thumbnail = $upload_video_thumbnail_result;
                $news->content = $request->input("content");
                $news->author = $request->input("author");
                $news->link = $request->input("link");
                $tags_input = $request->input("tags")!=null?array_map('trim', explode(',', $request->input("tags"))):[];
                $tags_array = array_map(function($value) { return (int)$value; },$tags_input);
                $news->tags = json_encode($tags_array);
                $news->is_featured = $request->input("is_featured");
                $news->status = 2;
                $news->created_by = auth()->user()->id;
                $news->news_agency_id = $request->input("news_agency_id");
                $news->html_content = $request->input("html_content");
                $result = $news->save();
                if ($result) {
                    return APIHelper::makeAPIResponse(true, "Done", null, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Service error", null, 500);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "News not found", null, 404);
            }


        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function removeAgentNews(RemoveAgentNewsRequest $request)
    {
        try {
            $news_id = $request->input("news_id");

            $agent_ids = auth()->user()->agents()->pluck("news_agency_id");
            $news = News::whereIn("news_agency_id",$agent_ids)->find($news_id);

            if($news !=null){
                if ($news->delete()) {
                    return APIHelper::makeAPIResponse(true, "Done", null, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Service error", null, 500);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "News not found in agency", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
