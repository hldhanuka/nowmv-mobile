<?php

namespace App\Http\Controllers\API\External;

use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\External\LoginRequest;
use App\NewsAgency;
use App\NewsAgencyUser;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $jwt;

    public function __construct(\Tymon\JWTAuth\JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function login(LoginRequest $request)
    {
        try {
            $credentials = ['email' => trim($request->input('username')), 'password' => trim($request->input('password')), 'status' => 1];
            if (Auth::attempt($credentials)) {
                $user = auth()->user();
                if ($user->status == 1) {

                    $my_news_agency_ids = NewsAgencyUser::where('user_id', $user->id)->active()->pluck("news_agency_id")->toArray();
                    $my_news_agencies = NewsAgency::select(['id', 'name', 'logo', 'status'])->whereIn('id', $my_news_agency_ids)->active()->get();
                    $payload = $this->jwt->factory()
                        ->setTTL(config('jwt.default_token_time'))
                        ->customClaims(['sub' => $user->id, "data" => ['token_type' => 'external', 'email' => $user->email, 'news_agencies' => $my_news_agencies]])
                        ->make();

                    $token = $this->jwt->manager()->encode($payload)->get();

                    $user_response_body = [
                        'token' => (string)$token,
                        'news_agencies' => $my_news_agencies
                    ];

                    return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);

                } else {
                    return APIHelper::makeAPIResponse(false, "User not active", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Invalid credentials", null, 400);
            }
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Token Error", null, 500);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

}
