<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\EventInvitee;
use App\EventMessage;
use App\EventTag;
use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CreatePrivateEventRequest;
use App\Http\Requests\API\GetEventCalendarRequest;
use App\Http\Requests\API\RemovePrivateEventRequest;
use App\Http\Requests\API\SearchEventRequest;
use App\Http\Requests\API\UpdateFavouriteEventRequest;
use App\Http\Requests\API\UpdatePrivateEventRequest;
use App\PushNotification;
use App\ScheduleType;
use App\Setting;
use App\UserFavouriteEvent;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public function getAllEvents(Request $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $events = Event::with('tag')->withCount(['favourites','unreadMessage'])
                ->active()->public()
                /*
                ->orWhere('created_by', $user_id)
                ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                    $query->where('invitee_id', $user_id);
                })*/
                ->latest()->get();
            return APIHelper::makeAPIResponse(true, "Done", $events, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getEventCalenderTypes(Request $request)
    {
        try {
            $calendar_types = ScheduleType::select(["id", "name", "status", "date"])->calendarSchedule()->active()->priority()->get();
            return APIHelper::makeAPIResponse(true, "Done", $calendar_types, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getEventCalender(GetEventCalendarRequest $request)
    {
        try {
            $user = APIHelper::loggedUser();
            $user_id = $user->id;
            $period = CarbonPeriod::create($request->input('start_date'), $request->input('end_date'));
            $events = Event::active()->public()->orWhere('created_by', $user->id)->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                $query->where('invitee_id', $user_id)->where('status', 1);
            })->latest()->get();
            $schedule_types = ScheduleType::calendarSchedule()->active()->whereIn("id", $request->input('type_id'))->get();

            $result_data = [];
            foreach ($period as $date) {
                $formatted_date = $date->toDateString();
                $data["date"] = $formatted_date;
                $data["data"] = [];
                foreach ($schedule_types as $schedule_type) {
                    $related_values = collect(json_decode($schedule_type->date))->where("date", $formatted_date)->flatten();
                    if (count($related_values)) {
                        $related_values = data_fill($related_values, '*.type', $schedule_type->name);
                        $data["data"] = array_merge($data["data"], Arr::flatten($related_values));
                    }

                }
                $data["events"] = $events->where("date", $formatted_date)->flatten();
                $result_data[] = $data;
            }
            return APIHelper::makeAPIResponse(true, "Done", $result_data, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getEventById(Request $request, $id)
    {
        try {
            $event = Event::with('tag')->withCount(['favourites','unreadMessage'])->where('id', $id)->active()->first();
            if ($event != null) {
                if ($event->type == "private" && ($event->is_my_event == false)) {
                    return APIHelper::makeAPIResponse(false, "This event not related to you", null, 403);
                }
                return APIHelper::makeAPIResponse(true, "Done", $event, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getAllEventTags(Request $request)
    {
        try {
            $event_tags = EventTag::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $event_tags, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function createPrivateEvent(CreatePrivateEventRequest $request)
    {
        try {
            $upload_result = CMSHelper::fileUpload("image", "uploads/private_events/");
            $private_event = new Event();
            $private_event->name = $request->input('name');
            $private_event->date = $request->input('date');
            $private_event->time = $request->input('time');
            $private_event->type = "private";
            $private_event->tag_id = $request->input('tag_id');
            $private_event->description = $request->input('description');
            $private_event->image = $upload_result;
            $private_event->latitude = $request->input('latitude');
            $private_event->longitude = $request->input('longitude');
            $private_event->location_name = $request->input('location_name');
            $private_event->status = 1;
            $private_event->code = APIHelper::generateRandomCode();
            $private_event->created_by = APIHelper::loggedUser()->id;
            $private_event->save();
            return APIHelper::makeAPIResponse(true, "Done", $private_event, 200);
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updatePrivateEvent(UpdatePrivateEventRequest $request, $id)
    {
        try {
            $private_event = Event::myEvent()->private()->find($id);
            if ($private_event != null) {
                $private_event->name = $request->input('name');
                $private_event->date = $request->input('date');
                $private_event->time = $request->input('time');
                $private_event->type = "private";
                $private_event->tag_id = $request->input('tag_id');
                $private_event->description = $request->input('description');
                if ($request->hasFile("image")) {
                    $upload_result = CMSHelper::fileUpload("image", "uploads/private_events/");
                    $private_event->image = $upload_result;
                }
                $private_event->latitude = $request->input('latitude');
                $private_event->longitude = $request->input('longitude');
                $private_event->location_name = $request->input('location_name');
                $private_event->status = 1;
                $private_event->save();
                return APIHelper::makeAPIResponse(true, "Done", $private_event, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function removePrivateEvent(RemovePrivateEventRequest $request)
    {
        try {
            $event_id = $request->input("event_id");
            $event = Event::myEvent()->private()->find($event_id);

            if ($event != null) {
                EventInvitee::where("event_id", $event_id)->delete();
                EventMessage::where("event_id", $event_id)->delete();
                PushNotification::where(["type" => "event_invitation", "relation_id" => $event_id])->delete();
                if ($event->delete()) {
                    return APIHelper::makeAPIResponse(true, "Done", null, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Service error", null, 500);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updateFavouriteEvent(UpdateFavouriteEventRequest $request)
    {
        try {
            $is_favourite = $request->input('is_favourite');
            $event_id = $request->input('event_id');

            if ($is_favourite) {
                $event_favourite = UserFavouriteEvent::updateOrCreate(['user_id' => APIHelper::loggedUser()->id, 'event_id' => $event_id], ['status' => 1]);
            } else {
                $event_favourite = UserFavouriteEvent::where(['user_id' => APIHelper::loggedUser()->id, 'event_id' => $event_id])->delete();
            }
            return APIHelper::makeAPIResponse(true, "Update Successful", null, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function findNearbyEvents(Request $request)
    {
        try {
            $latitude = $request->input("latitude");
            $longitude = $request->input("longitude");
            $radius = Setting::where('title', 'radius')->first()->text;
            $user_id = APIHelper::loggedUser()->id;
            $events = Event::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->with('tag')->withCount('favourites')->public()->orWhere('created_by', $user_id)->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                $query->where('invitee_id', $user_id);
            })->active()->having('distance', '<', $radius)->orderBy('distance')->get();
            return APIHelper::makeAPIResponse(true, "Done", $events, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function searchEvents(SearchEventRequest $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $search = $request->input('search');
            $tag_id = $request->input('tag_id');
            $date = $request->input('date');
            $all_event = Event::with('tag')->withCount(['favourites', 'unreadMessage'])
                ->active()->public()
                /*
                ->orWhere('created_by', $user_id)
                ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                    $query->where('invitee_id', $user_id);
                })
                */
                ->get();

            if ($tag_id != null) {
                $all_event = $all_event->where('tag_id', $tag_id);
            }
            if ($date != null) {
                $all_event = $all_event->where('date', $date);
            }

            $search_event = Event::with('tag')->withCount(['favourites', 'unreadMessage'])
                ->whereRaw('LOWER(name) like "%' . strtolower($search) . '%"')
                ->orWhereRaw('LOWER(description) like "%' . strtolower($search) . '%"')
                ->orWhereRaw('LOWER(location_name) like "%' . strtolower($search) . '%"')->get();

            $result = collect($all_event)->intersect($search_event)->sortByDesc("created_at")->flatten();

            return APIHelper::makeAPIResponse(true, "Done", $result, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function myEvents(Request $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $my_events = Event::with('tag')->withCount(['favourites', 'unreadMessage'])
                ->active()->private()
                ->where('created_by', $user_id)
                ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                    $query->where('invitee_id', $user_id);
                })->latest()->get();
            return APIHelper::makeAPIResponse(true, "Done", $my_events, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function searchMyEvents(Request $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $search = $request->input('search');
            $my_events = Event::with('tag')->withCount(['favourites', 'unreadMessage'])
                ->active()->private()
                ->where('created_by', $user_id)
                ->orWhereHas('eventInvitees', function ($query) use ($user_id) {
                    $query->where('invitee_id', $user_id);
                })->get();

            $search_events = Event::with('tag')->withCount(['favourites', 'unreadMessage'])
                ->whereRaw('LOWER(name) like "%' . strtolower($search) . '%"')
                ->orWhereRaw('LOWER(description) like "%' . strtolower($search) . '%"')
                ->orWhereRaw('LOWER(location_name) like "%' . strtolower($search) . '%"')->get();

            $result = collect($my_events)->intersect($search_events)->sortByDesc("created_at")->flatten();

            return APIHelper::makeAPIResponse(true, "Done", $result, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function getEventReminder(Request $request, $id)
    {
        try {
            $event = Event::with('myReminderNotification')->where('id', $id)->active()->first();

            if ($event != null) {
                if ($event->hasReminder) {
                    $reminder = $event->myReminderNotification[0];
                    $reminder_date = Carbon::parse($reminder->date_time);
                    $event_date = Carbon::parse($event->date);
                    $diff_in_days = $reminder_date->diffInDays($event_date);
                    $data = [
                        "date_time" => $reminder->date_time,
                        "message" => ""
                    ];

                    if ($diff_in_days == 0) {
                        $data["message"] = 'You have already set a reminder to notify before one day for this event.';
                    } else if ($diff_in_days == 6) {
                        $data["message"] = 'You have already set a reminder to notify before one week for this event.';
                    } else if ($diff_in_days == 30) {
                        $data["message"] = 'You have already set a reminder to notify before one month for this event.';
                    } else {
                        $data["message"] = 'You have already set a reminder to notify this event.';
                    }
                    return APIHelper::makeAPIResponse(true, "Done", $data, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Event reminder not found", null, 404);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Event not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }


}
