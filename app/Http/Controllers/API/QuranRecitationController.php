<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\QuranRecitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class QuranRecitationController extends Controller
{
    public function testPDF(Request $request){
        try {
            return response()->file(public_path('uploads\pdfs\sample.pdf'),[
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="sample.pdf"'
            ]);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function quranRecitationList(Request $request){

        $quran_recitaions = QuranRecitation::all();

        return APIHelper::makeAPIResponse(true, "QuranRecitation List", $quran_recitaions, 200);

    }

    public function downloadQuranRecitationPDF(Request $request){

        try {
            return response()->file(public_path( "uploads/pdfs/".$request->pdf_id.".pdf"),[
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="sample.pdf"'
            ]);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function testPDFIframe(){
        try {
            return view('app_pages.quran_recitation');
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
