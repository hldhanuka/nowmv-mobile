<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\RegisterRequest;
use App\Http\Requests\API\SubmitUserCurrentLocationRequest;
use App\Http\Requests\API\VerifyLoginRequest;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\PrayerLocation;
use App\User;
use App\UserWeatherLocation;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    use ThrottlesLogins;

    protected $jwt;

    protected $maxAttempts = 3;
    protected $decayMinutes = 1;

    public function __construct(\Tymon\JWTAuth\JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function login(LoginRequest $request)
    {
        try {
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $device_id = trim($request->input('device_id'));

            $api_body = [
                "clientReference" => config('dhiraagu.static_api_data.clientReference'),
                "mobileNumber" => $mobile,
                "senderId" => config('dhiraagu.static_api_data.senderId'),
                "messageTemplate" => config('dhiraagu.static_api_data.messageTemplate'),
                "otpServiceDescription" => config('dhiraagu.static_api_data.otpServiceDescription'),
            ];

            $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('generate_otp'), $api_body, "json");

            if (config('dhiraagu.bypass_service') || APIHelper::isBypassMobile($mobile) || ($response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"])) {
                $payload = $this->jwt->factory()
                    ->setTTL(config('jwt.login_token_time'))
                    ->customClaims([
                        'sub' => $mobile,
                        'data' => [
                            'mobile' => $mobile,
                            'device_id' => $device_id,
                            'token_type' => 'short',
                            'login_type' => 'otp',
                            'social' => null
                        ]
                    ])
                    ->make();

                $token = $this->jwt->manager()->encode($payload)->get();

                $user_response_body = [
                    'pre_token' => (string)$token
                ];

                return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
            }
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Token Error", null, 500);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function verifyLogin(VerifyLoginRequest $request)
    {
        try {
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return $this->sendLockoutResponse($request);
            }

            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $device_id = trim($request->input('device_id'));
            $otp = trim($request->input('otp'));

            $payload = $this->jwt->getPayload();
            $token_mobile = $payload->get('data.mobile');
            $token_device_id = $payload->get('data.device_id');

            if ($mobile == $token_mobile && $device_id == $token_device_id) {
                // Token data and request data validated
                $api_body = [
                    "mobileNumber" => $mobile,
                    "code" => $otp
                ];

                $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('verify_otp', '{ClientRefrerence}', config('dhiraagu.static_api_data.clientReference')), $api_body, "json");

                if ((config('dhiraagu.bypass_service') && ($otp == "123456")) || (APIHelper::isBypassMobile($mobile) && ($otp == "123456")) || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {


                    $user = User::where("mobile", $mobile)->first();


                    if ($user!=null){
                        $user_name = ($user->first_name!=null)?$user->first_name." ".$user->last_name:null;
                    }else{
                        $user_name = null;
                    }

                    $payload = $this->jwt->factory()
                        ->setTTL(config('jwt.default_token_time'))
                        ->customClaims([
                            'sub' => $mobile,
                            'data' => [
                                'user_name' => $user_name,
                                'mobile' => $mobile,
                                'device_id' => $device_id,
                                'token_type' => 'long',
                                'login_type' => 'otp',
                                'social' => null
                            ]
                        ])
                        ->make();

                    $token = $this->jwt->manager()->encode($payload)->get();
                    $user_exists = User::where("mobile", $mobile)->notGuest()->exists();

                    $user_response_body = [
                        'token' => (string)$token,
                        'registered_user' => $user_exists
                    ];

                    $this->clearLoginAttempts($request);

                    return APIHelper::makeAPIResponse(true, "Done", $user_response_body, 200);
                } else {

                    $this->incrementLoginAttempts($request);

                    return APIHelper::makeAPIResponse(false, "Invalid OTP code", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Invalid Request", null, 500);
            }
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Token Error", null, 500);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function register(RegisterRequest $request)
    {
        try {
            $first_name = trim($request->input('first_name'));
            $last_name = trim($request->input('last_name'));
            $email = trim($request->input('email'));
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $birthday = trim($request->input('birthday'));
            $address = trim($request->input('address'));
            $gender = trim($request->input('gender'));

            // check guest
            $guest = User::where("mobile", $mobile)->guest()->first();
            if ($guest != null) {
                // Guest user
                $user = $guest;
            } else {
                // New Social Account Registration
                $user = new User();
            }

            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user->mobile = $mobile;
            $user->birthday = $birthday;
            $user->address = $address;
            $user->gender = $gender;
            $user->status = 1;
            $result = $user->save();

            // Add customer role to new user
            $user->assignRole("customer");

            if ($result) {
                $set_default_notification_settings = APIHelper::setDefaultNotificationSettings($user->id);
                $set_default_settings = APIHelper::setDefaultHomeScreenOrder($user->id);
                $set_default_prayer_reminder = APIHelper::setDefaultPrayerReminder($user->id);

                // Push notification
                $push_message = "Welcome to Now MV";
                $data = ["type" => "home","id" => null];
                OneSignalPush::sendPushNotification($push_message, "system", $data, 0, $user->id, null);

                return APIHelper::makeAPIResponse(true, "User Registered", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            $payload = $this->jwt->getPayload();
            $token_mobile = $payload->get('data.mobile');
            $mobile = APIHelper::formatMobile($request->input("mobile"));
            if ($token_mobile == $mobile) {
                $token = $this->jwt->getToken();
                $this->jwt->invalidate(true);
                return APIHelper::makeAPIResponse(true, "User Logout", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Token mobile and request mobile not matched", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    /**
     * Set User First time prayer location and First time weather location (After the register call)
     * @param SubmitUserCurrentLocationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitUserCurrentLocation(SubmitUserCurrentLocationRequest $request)
    {
        try {
            $location_permission_granted = $request->input('location_permission_granted');
            $latitude = $request->input('latitude');
            $longitude = $request->input('longitude');

            if ($location_permission_granted && $latitude != null && $longitude != null) {
                // location permission granted
                $prayer_location = APIHelper::getMyNearestPrayerLocation($latitude, $longitude);
                if ($prayer_location == null) {
                    return APIHelper::makeAPIResponse(false, "Nearest location not found", null, 400);
                }

            } else {
                // location permission not granted
                // Set default location as Male
                $prayer_location = PrayerLocation::find(1);
                $latitude = $prayer_location->latitude;
                $longitude = $prayer_location->longitude;
            }

            $user = APIHelper::loggedUser();
            $user->location_permission_granted = $location_permission_granted;
            $user->latitude = $latitude;
            $user->longitude = $longitude;
            $user->prayer_location_id = $prayer_location->id;
            $user->save();

            $closest_weather_location_id = APIHelper::getNearestWeatherLocationID($latitude, $longitude);

            UserWeatherLocation::updateOrCreate(['user_id' => APIHelper::loggedUser()->id, "location_id" => $closest_weather_location_id]);

            return APIHelper::makeAPIResponse(true, "Done", null, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    /**
     * Set User location when before goes to home page
     * @param SubmitUserCurrentLocationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitUserLiveLocation(SubmitUserCurrentLocationRequest $request)
    {
        try {
            $location_permission_granted = $request->input('location_permission_granted');
            $latitude = $request->input('latitude');
            $longitude = $request->input('longitude');
            $user = APIHelper::loggedUser();

            if ($location_permission_granted && $latitude != null && $longitude != null) {
                // location permission granted
            } else {
                // location permission not granted
                // Set default location as prayer location
                if ($user->prayer_location_id != null) {
                    $user_prayer_location = PrayerLocation::find($user->prayer_location_id);
                } else {
                    $user_prayer_location = PrayerLocation::find(1);
                }
                $latitude = $user_prayer_location->latitude;
                $longitude = $user_prayer_location->longitude;
            }

            $user->location_permission_granted = $location_permission_granted;
            $user->latitude = $latitude;
            $user->longitude = $longitude;
            $user->save();

            return APIHelper::makeAPIResponse(true, "Done", null, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service Error", null, 500);
        }
    }

    protected function throttleKey(Request $request)
    {
        return $request->input('mobile').'|'.$request->ip();
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );
        return APIHelper::makeAPIResponse(false, "Too many invalid login attempts. Please try again in ".$seconds." seconds", null, 429);
    }

}
