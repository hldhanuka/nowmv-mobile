<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\NotificationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationTypeController extends Controller
{
    public function notificationTypes(Request $request)
    {
        try {
            $notification_types = NotificationType::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $notification_types, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getNotificationTypeById(Request $request, $id)
    {
        try {
            $notification_type = NotificationType::where('id', $id)->active()->first();
            if ($notification_type != null) {
                return APIHelper::makeAPIResponse(true, "Done", $notification_type, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Notification type not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
