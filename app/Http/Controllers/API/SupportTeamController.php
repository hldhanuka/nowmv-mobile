<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\SupportTeam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupportTeamController extends Controller
{
    public function supportTeams(Request $request)
    {
        try {
            $support_teams = SupportTeam::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $support_teams, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getSupportTeamById(Request $request, $id)
    {
        try {
            $support_team = SupportTeam::where('id', $id)->active()->first();
            if ($support_team != null) {
                return APIHelper::makeAPIResponse(true, "Done", $support_team, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Support team not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
