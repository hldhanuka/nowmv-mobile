<?php

namespace App\Http\Controllers\API;

use App\Badge;
use App\Helpers\APIHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BadgeController extends Controller
{
    public function badges(Request $request)
    {
        try {
            $badges = Badge::active()->get();
            return APIHelper::makeAPIResponse(true, "Done", $badges, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function getBadgeById(Request $request, $id)
    {
        try {
            $badge = Badge::where('id', $id)->active()->first();
            if ($badge != null) {
                return APIHelper::makeAPIResponse(true, "Done", $badge, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Badge not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

}
