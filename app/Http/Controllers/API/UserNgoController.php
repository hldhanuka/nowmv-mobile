<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\SubmitNgoPaymentRequest;
use App\Http\Requests\API\VerifyNgoPaymentRequest;
use App\Libraries\Dhiraagu\DHIRAAGU;
use App\Ngo;
use App\NgoBadge;
use App\Setting;
use App\UserBadge;
use App\UserNgoDonation;
use Illuminate\Support\Facades\DB;

class UserNgoController extends Controller
{
    public function submitNgoPayment(SubmitNgoPaymentRequest $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $ngo_id = $request->input('ngo_id');
            $payment_method = $request->input('payment_method');
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $amount = floatval($request->input('amount'));
            $payment_invoice_number = round(microtime(true) * 1000) . "_" . $ngo_id;

            $ngo = Ngo::active()->find($ngo_id);

            if ($ngo != null) {

                if ($ngo->ngoMerchant != null && $ngo->ngoMerchant->key != null) {

                    DB::beginTransaction();
                    $user_ngo_donation = new UserNgoDonation();
                    $user_ngo_donation->user_id = $user_id;
                    $user_ngo_donation->ngo_id = $ngo_id;
                    $user_ngo_donation->payment_method = $payment_method;
                    $user_ngo_donation->status = 2;
                    $user_ngo_donation->amount = $amount;
                    $user_ngo_donation->mobile_number = $mobile;
                    $user_ngo_donation->payment_invoice_number = $payment_invoice_number;
                    $user_ngo_donation->save();
                    DB::commit();


                    $api_body = [
                        "username" => config('dhiraagu.static_api_data.mfs_username'),
                        "merchantKey" => config('dhiraagu.static_api_data.mfs_merchantKey'),
                        "originationNumber" => $ngo->ngoMerchant->key,
                        "destinationNumber" => $mobile,
                        "amount" => $amount,
                        "paymentInvoiceNumber" => $payment_invoice_number,
                        "transactionDescription" => "Donation Payment"
                    ];

                    $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('payment_otp_request'), $api_body, "json");

                    if (config('dhiraagu.bypass_service') || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {

                        $user_ngo_donation->transaction_id = isset($response["body"]["transactionId"]) ? $response["body"]["transactionId"] : null;
                        $user_ngo_donation->result_data_reference_id = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["referenceId"])) ? $response["body"]["resultData"]["referenceId"] : null;
                        $user_ngo_donation->result_data_message = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["message"])) ? $response["body"]["resultData"]["message"] : null;
                        $user_ngo_donation->save();

                        $donation_response = [
                            "ngo_payment_id" => $user_ngo_donation->id,
                            "payment_invoice_number" => $payment_invoice_number
                        ];

                        return APIHelper::makeAPIResponse(true, "Done", $donation_response, 200);
                    } else if (isset($response["body"]["resultData"]) &&
                        isset($response["body"]["resultData"]["message"]) &&
                        $response["body"]["resultData"]["message"] == "QUERYPROFILE:Destination number in wrong format(1137)") {
                        $android_dhiraagu_pay_url = Setting::where('title', 'android_dhiraagu_pay_url')->pluck('text')->first();
                        $ios_dhiraagu_pay_url = Setting::where('title', 'ios_dhiraagu_pay_url')->pluck('text')->first();
                        $data = [
                            "android_dhiraagu_pay_url" => $android_dhiraagu_pay_url != null ? $android_dhiraagu_pay_url : null,
                            "ios_dhiraagu_pay_url" => $ios_dhiraagu_pay_url != null ? $ios_dhiraagu_pay_url : null,
                        ];
                        return APIHelper::makeAPIResponse(false, "Please register on Dhiraagu pay", $data, 428);
                    } else if (isset($response["body"]["resultData"]) &&
                        isset($response["body"]["resultData"]["message"]) &&
                        $response["body"]["resultData"]["message"] == "QUERYPROFILE:Limit Check Failed(1140)") {
                        return APIHelper::makeAPIResponse(false, "Insufficient balance in your wallet to process this transaction", null, 500);
                    } else if (isset($response["body"]["resultData"]) &&
                        isset($response["body"]["resultData"]["message"]) &&
                        $response["body"]["resultData"]["message"] == "QUERYPROFILE:Requested Service is not subscribed by the User(1061)") {
                        return APIHelper::makeAPIResponse(false, "Service not available", null, 500);
                    } else {
                        return APIHelper::makeAPIResponse(false, "Dhiraagu Service Error", null, 500);
                    }
                } else {
                    return APIHelper::makeAPIResponse(false, "Donation has not assign to merchant or merchant origination number", null, 400);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Donation is inactive or NGO is inactive", null, 400);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }

    public function verifyNgoPayment(VerifyNgoPaymentRequest $request)
    {
        try {
            $user_id = APIHelper::loggedUser()->id;
            $ngo_payment_id = $request->input('ngo_payment_id');
            $mobile = APIHelper::formatMobile(trim($request->input('mobile')));
            $otp = trim($request->input('otp'));

            $user_ngo_donation = UserNgoDonation::find($ngo_payment_id);
            $ngo = Ngo::active()->find($user_ngo_donation->ngo_id);

            if ($ngo != null) {

                if ($user_ngo_donation->mobile_number != $mobile) {
                    return APIHelper::makeAPIResponse(false, "Invalid mobile number", null, 400);
                }

                if ($user_ngo_donation->status != 1) {
                    $api_body = [
                        "username" => config('dhiraagu.static_api_data.mfs_username'),
                        "merchantKey" => config('dhiraagu.static_api_data.mfs_merchantKey'),
                        "destinationNumber" => $mobile,
                        "transactionDescription" => "Donation Payment",
                        "referenceId" => $user_ngo_donation->result_data_reference_id,
                        "otpString" => $otp
                    ];

                    $response = DHIRAAGU::apiCall('POST', APIHelper::getDhiraaguURL('payment_otp_verify'), $api_body, "json");

                    if ((config('dhiraagu.bypass_service') && ($otp == "1234" || $otp == "123456")) || $response["success"] && isset($response["body"]["transactionStatus"]) && $response["body"]["transactionStatus"]) {
                        // Success payment
                        $user_ngo_donation->status = 1;
                        $user_ngo_donation->transaction_id = isset($response["body"]["transactionId"]) ? $response["body"]["transactionId"] : null;
                        $user_ngo_donation->result_data_reference_id = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["referenceId"])) ? $response["body"]["resultData"]["referenceId"] : null;
                        $user_ngo_donation->result_data_message = (isset($response["body"]["resultData"]) && isset($response["body"]["resultData"]["message"])) ? $response["body"]["resultData"]["message"] : null;
                        $user_ngo_donation->save();

                        $total_donation_amount = UserNgoDonation::where(["user_id" => $user_id, "ngo_id" => $user_ngo_donation->ngo_id])->active()->sum("amount");
                        $user_won_badges_array = UserBadge::active()->where("user_id", $user_id)->pluck("badge_id")->toArray();

                        $newly_winning_donation_badges = NgoBadge::with(['badge'])->whereHas('ngo', function ($q1) {
                            $q1->active();
                        })->whereHas('badge', function ($q2) {
                            $q2->active();
                        })->active()->where("ngo_id", $user_ngo_donation->ngo_id)->where("winning_limit", "<=", $total_donation_amount)->whereNotIn("badge_id", $user_won_badges_array)->get();

                        $is_won_badge = count($newly_winning_donation_badges) > 0;
                        $new_badges = [];
                        foreach ($newly_winning_donation_badges as $newly_winning_donation_badge) {
                            $data = [];
                            $random_code = APIHelper::generateRandomCode();
                            $badge = $newly_winning_donation_badge->badge;

                            $new_user_badge = new UserBadge();
                            $new_user_badge->user_id = $user_id;
                            $new_user_badge->badge_id = $badge->id;
                            $new_user_badge->ngo_id = $user_ngo_donation->ngo_id;
                            $new_user_badge->type = "ngo";
                            $new_user_badge->code = $random_code;
                            $new_user_badge->status = 1;
                            $new_user_badge->save();

                            $data["id"] = $badge->id;
                            $data["title"] = $badge->title;
                            $data["image"] = $badge->image;
                            $data["share_link"] = $new_user_badge->share_link;
                            $data["description"] = $badge->description;

                            array_push($new_badges, $data);
                        }

                        $donation_response = [
                            "ngo_payment_id" => $user_ngo_donation->id,
                            "message" => "Thank you for your donation",
                            "is_won_badge" => $is_won_badge,
                            "badges" => $new_badges
                        ];

                        $amount = APIHelper::setAmountFormat($user_ngo_donation->amount);

                        // Push notification
                        $push_message = "You have donated MVR " . $amount . " to \"" . $ngo->title . "\" on " . APIHelper::setDhiraaguDateFormat($user_ngo_donation->updated_at, "datetime");
                        $data = ["type" => "donation_history","id" => null];
                        OneSignalPush::sendPushNotification($push_message, "transaction", $data, 0, $user_id, null);

                        return APIHelper::makeAPIResponse(true, "Payment Success", $donation_response, 200);
                    } else {
                        return APIHelper::makeAPIResponse(false, "Invalid OTP code", null, 400);
                    }

                } else {
                    return APIHelper::makeAPIResponse(false, "Donation already paid", null, 400);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Donation is inactive or NGO is inactive", null, 400);
            }


        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(true, "Service error", null, 500);
        }
    }
}
