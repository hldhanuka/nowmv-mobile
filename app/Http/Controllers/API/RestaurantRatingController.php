<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\SubmitRestaurantRatingRequest;
use App\Restaurant;
use App\RestaurantRating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantRatingController extends Controller
{
    public function getUserRatingByRestaurantId(Request $request, $restaurant_id)
    {
        try {
            $restaurant = Restaurant::active()->find($restaurant_id);
            if ($restaurant != null) {
                $restaurant_rate = RestaurantRating::where(['restaurant_id' => $restaurant_id, 'created_by' => APIHelper::loggedUser()->id])->first();
                if ($restaurant_rate != null) {
                    return APIHelper::makeAPIResponse(true, "Done", $restaurant_rate, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, "Restaurant rating not found", null, 404);
                }

            } else {
                return APIHelper::makeAPIResponse(false, "Restaurant not found", null, 404);
            }

        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }

    }

    public function submitRestaurantRating(SubmitRestaurantRatingRequest $request)
    {
        try {
            $restaurant_id = $request->input("restaurant_id");
            $rate = $request->input("rate");
            $restaurant = Restaurant::active()->find($restaurant_id);

            if ($restaurant != null) {
                $result = RestaurantRating::updateOrCreate(['restaurant_id' => $restaurant_id, 'created_by' => APIHelper::loggedUser()->id], ['rate' => $rate]);
                if($result){
                    $response = ["average_rating"=>$restaurant->average_rating];
                    return APIHelper::makeAPIResponse(true, "Successfully submitted", $response, 200);
                } else {
                    return APIHelper::makeAPIResponse(false, 'Service error', null, 500);
                }
            } else {
                return APIHelper::makeAPIResponse(false, "Restaurant not found", null, 404);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
