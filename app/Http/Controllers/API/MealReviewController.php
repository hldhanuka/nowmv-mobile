<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIHelper;
use App\Http\Requests\API\RemoveMealReviewRequest;
use App\Http\Requests\API\SubmitMealReviewRequest;
use App\Http\Requests\API\UpdateMealReviewRequest;
use App\Meal;
use App\MealReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MealReviewController extends Controller
{
    public function submitMealReview(SubmitMealReviewRequest $request)
    {
        try {

            $meal_id = $request->input("meal_id");
            $comment = $request->input("comment");
            $meal = Meal::active()->find($meal_id);
            if ($meal != null) {
                MealReview::insert(['restaurant_id' => $meal->restaurant_id, 'meal_id' => $meal_id, 'comment' => $comment, 'status' => 1, 'created_by' => APIHelper::loggedUser()->id, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
                return APIHelper::makeAPIResponse(true, "Successfully submitted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Meal not found", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function updateMealReview(UpdateMealReviewRequest $request)
    {
        try {
            $id = $request->input("id");
            $comment = $request->input("comment");
            $meal_review = MealReview::where('created_by', APIHelper::loggedUser()->id)->find($id);
            if ($meal_review != null) {
                $meal_review->update(['comment' => $comment]);
                return APIHelper::makeAPIResponse(true, "Successfully updated", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Meal review not found or not yours", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }

    public function removeMealReview(RemoveMealReviewRequest $request)
    {
        try {
            $id = $request->input("id");
            $meal_review = MealReview::where('created_by', APIHelper::loggedUser()->id)->find($id);
            if ($meal_review != null) {
                $meal_review->delete();
                return APIHelper::makeAPIResponse(true, "Successfully deleted", null, 200);
            } else {
                return APIHelper::makeAPIResponse(false, "Meal review not found or not yours", null, 400);
            }
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
