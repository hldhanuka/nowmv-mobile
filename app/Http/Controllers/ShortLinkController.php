<?php

namespace App\Http\Controllers;

use App\ShortLink;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ShortLinkController extends Controller
{

    /**
     * Display a listing of the resource.
     * @param $code
     * @return string
     * @return \Illuminate\Http\Response
     */
    public function shortenLink($code)
    {
        $find = ShortLink::where('code', $code)->first();
        if ($find != null) {
            return redirect($find->link);
        } else {
            return abort(404);
        }

    }
}
