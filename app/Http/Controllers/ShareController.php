<?php

namespace App\Http\Controllers;

use App\Badge;
use App\Donation;
use App\Event;
use App\Helpers\APIHelper;
use App\Ngo;
use App\UserBadge;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    public function shareEvent(Request $request, $id, $code)
    {
        $url = $request->fullUrl();
        $share_event = Event::where('id', $id)->where('code', $code)->first();
        if ($share_event != null) {
            return view('pages.events.share_event', ["share_event" => $share_event, "url" => $url]);
        } else {
            return abort(404);
        }
    }

    public function shareBadge(Request $request, $id, $code)
    {
        $url = $request->fullUrl();
        $user_badge = UserBadge::where('badge_id', $id)->where('code', $code)->first();
        if ($user_badge != null) {
            $share_badge = Badge::find($id);
            return view('pages.badges.share_badge', ["share_badge" => $share_badge, "url" => $url]);
        } else {
            return abort(404);
        }
    }

    public function shareNgo(Request $request, $ngo_id)
    {
        $url = $request->fullUrl();
        $share_ngo = Ngo::find($ngo_id);
        if ($share_ngo != null) {
            return view('pages.ngos.share_ngo', ["share_ngo" => $share_ngo, "url" => $url]);
        } else {
            return abort(404);
        }
    }

    public function shareNgoByBadgeCode(Request $request, $ngo_id, $badge_code)
    {
        $url = $request->fullUrl();
        $user_badge = UserBadge::where('code', $badge_code)->first();
        if ($user_badge != null) {
            $share_badge = Badge::find($user_badge->badge_id);
            if ($share_badge != null) {
                return view('pages.badges.share_badge', ["share_badge" => $share_badge, "url" => $url]);
            } else {
                return abort(404);
            }
        } else {
            return abort(404);
        }
    }

    public function shareDonation(Request $request, $donation_id)
    {
        $url = $request->fullUrl();
        $share_donation = Donation::find($donation_id);
        if ($share_donation != null) {
            return view('pages.donations.share_donation', ["share_donation" => $share_donation, "url" => $url]);
        } else {
            return abort(404);
        }
    }

    public function shareDonationByBadgeCode(Request $request, $donation_id, $badge_code)
    {
        $url = $request->fullUrl();
        $user_badge = UserBadge::where('code', $badge_code)->first();
        if ($user_badge != null) {
            $share_badge = Badge::find($user_badge->badge_id);
            if ($share_badge != null) {
                return view('pages.badges.share_badge', ["share_badge" => $share_badge, "url" => $url]);
            } else {
                return abort(404);
            }
        } else {
            return abort(404);
        }
    }

    public function checkShareResponse(Request $request, $type, $id)
    {
        try {
            $user = APIHelper::loggedUser();
            switch ($type) {
                case "event":
                    $event = Event::with(['eventInvitees' => function ($query) use ($user) {
                        $query->where('user_id', $user->id);
                    }])->find($id);
                    if ($event != null) {
                        $go_to_notifications = count($event->eventInvitees) > 0 && $event->eventInvitees()->pending()->first() != null;
                        $result = [
                            "id" => $id,
                            "type" => $type,
                            "data_object" => [
                                "go_to_notifications" => $go_to_notifications
                            ]
                        ];
                    } else {
                        return APIHelper::makeAPIResponse(true, "Event not found", null, 400);
                    }

                    break;
                case "badge":
                    $badge = Badge::with(['userBadges' => function ($query) use ($user) {
                        $query->where('user_id', $user->id);
                    }, 'donationBadges'])->active()->find($id);
                    if ($badge != null) {
                        if (count($badge->userBadges) > 0 && $badge->userBadges()->first() != null) {
                            $donation_id = $badge->userBadges()->first()->donation_id;
                        } else if (count($badge->donationBadges) > 0 && $badge->donationBadges()->first() != null) {
                            $donation_id = $badge->donationBadges()->first()->donation_id;
                        } else {
                            $latest = Donation::active()->latest()->first();
                            $donation_id = $latest != null ? $latest->id : null;
                        }

                        $result = [
                            "id" => $id,
                            "type" => $type,
                            "data_object" => [
                                "donation_id" => $donation_id
                            ]
                        ];
                    } else {
                        return APIHelper::makeAPIResponse(true, "Badge not found", null, 400);
                    }
                    break;
                default:
                    return APIHelper::makeAPIResponse(true, "Invalid Type", null, 400);
                    break;
            }

            return APIHelper::makeAPIResponse(true, "Done", $result, 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Service error", null, 500);
        }
    }
}
