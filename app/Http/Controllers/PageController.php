<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    /**
     * Display all the static pages when authenticated
     *
     * @param string $page
     * @return \Illuminate\View\View
     */
    public function index(string $page)
    {
        if (view()->exists("pages.{$page}")) {
            return view("pages.{$page}");
        }
        return abort(404);
    }

    public function termsAndServices() {
        return view('app_pages.terms_services');
    }

    public function imiTermsAndServices() {
        return view('app_pages.imi_terms_services');
    }

    public function privacyPolicy(){
        return view('app_pages.privacy_policy');
    }

    public function frequentlyAskedQuestions(){
        return view('app_pages.frequently_asked_questions');
    }

    public function about(){
        return view('about');
    }
}
