<?php

namespace App\Http\Controllers;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Http\Request;

class ImageCropperController extends Controller
{
    public function upload(Request $request)
    {
        try {
            $base64data = $request->input('image');
            $upload_path = $request->input('upload_path');
            $url = CMSHelper::base64FileUpload($base64data, $upload_path);
            return APIHelper::makeAPIResponse(true, "Image Uploaded Successfully", $url, $status_code = 200);
        } catch (\Exception $e) {
            report($e);
            return APIHelper::makeAPIResponse(false, "Image Uploaded Error", null, $status_code = 500);
        }
    }


}
