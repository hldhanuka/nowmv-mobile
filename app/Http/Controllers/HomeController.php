<?php

namespace App\Http\Controllers;

use App\Meal;
use App\Restaurant;
use App\User;
use App\UserDonation;
use App\UserOffer;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function welcome()
    {
        if (!auth()->user()) {
            return view('welcome');
        } else {
            activity()->log('SUCCESS|REDIRECT|Home Page'); // ACTIVITY LOG
            return redirect()->route('home');
        }

    }

    public function home()
    {
        $start_date = Carbon::now()->subDays(30);
        $end_date = Carbon::now();
        $last_7_day_start = Carbon::now()->subDays(7);
        $period = CarbonPeriod::create($start_date, $end_date);
        $users = User::role("customer")->whereBetween('created_at', [$start_date->toDateTimeString(), $end_date->toDateTimeString()])->get();
        $user_donations = UserDonation::whereBetween('updated_at', [$last_7_day_start->toDateTimeString(), $end_date->toDateTimeString()])->get();
        $user_offers = UserOffer::whereBetween('updated_at', [$last_7_day_start->toDateTimeString(), $end_date->toDateTimeString()])->get();
        $date_array = [];
        $count_array = [];
        $last_7_day_array = [];
        $last_7_day_user_donations_array = [];
        $last_7_day_user_offers_array = [];

        foreach ($period as $date) {
            $formatted_date = $date->shortEnglishMonth . " " . $date->day;
            $iso_date = $date->toDateString();
            $date_array[] = $formatted_date;
            $count_array[] = $users->filter(function ($item) use ($iso_date) {
                return false !== stristr($item->created_at, $iso_date);
            })->count();
            if ($date->isAfter($last_7_day_start)) {
                $last_7_day_array[] = $formatted_date;
                $last_7_day_user_offers_array[] = $user_offers->filter(function ($item) use ($iso_date) {
                    return false !== stristr($item->updated_at, $iso_date);
                })->count();
                $last_7_day_user_donations_array[] = $user_donations->filter(function ($item) use ($iso_date) {
                    return false !== stristr($item->updated_at, $iso_date);
                })->count();
            }

        }

        $restaurants = Restaurant::all();
        $meals = Meal::all();

        activity()->log('SUCCESS|VIEW|Home Page'); // ACTIVITY LOG
        return view('pages.home', [
            "date_array" => $date_array,
            "count_array" => $count_array,
            "last_7_day_array" => $last_7_day_array,
            "last_7_day_user_offers_array" => $last_7_day_user_offers_array,
            "last_7_day_user_donations_array" => $last_7_day_user_donations_array,
            "restaurants" => $restaurants->sortByDesc("average_rating")->take(10)->flatten(),
            "meals" => $meals->sortByDesc("average_rating")->take(10)->flatten(),
        ]);
    }
}
