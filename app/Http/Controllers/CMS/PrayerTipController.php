<?php

namespace App\Http\Controllers\CMS;

use App\PrayerTip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\PrayerTipCreateRequest;
use App\Http\Requests\CMS\PrayerTipUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class PrayerTipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('prayer_tips.list')) {
            $prayer_tips = PrayerTip::Latest()->paginate(10);
            return view('pages.prayer_tips.index', ["prayer_tips" => $prayer_tips]);
        } else {
            activity()->log('ERROR|VIEW|Prayer Tip List|No prayer_tips.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('prayer_tips.create')) {
            activity()->log('SUCCESS|VIEW|Prayer Tip Create'); // ACTIVITY LOG
            return view('pages.prayer_tips.create');
        } else {
            activity()->log('ERROR|VIEW|Prayer Tip Create|No prayer_tips.create permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrayerTipCreateRequest $request)
    {
        $prayer_tip = new PrayerTip();
        $prayer_tip->title = $request->input("title");
        $prayer_tip->content = $request->input("content");
        $prayer_tip->status = 2;
        $prayer_tip->created_by = auth()->user()->id;

        $result = $prayer_tip->save();

        if ($result) {
            activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('SUCCESS|CREATE|Prayer Tip Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('prayer_tips.index');
        } else {
            activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('ERROR|CREATE|Prayer Tip Create|Fail'); // ACTIVITY LOG
            return redirect()->route('prayer_tips.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('prayer_tips.update')) {
            $prayer_tip = PrayerTip::find($id);
            activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('SUCCESS|VIEW|Prayer Tip Edit'); // ACTIVITY LOG
            return view('pages.prayer_tips.edit', ["prayer_tip" => $prayer_tip]);
        } else {
            activity()->log('ERROR|VIEW|Prayer Tip Edit|No prayer_tips.update permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrayerTipUpdateRequest $request, $id)
    {
        $prayer_tip = PrayerTip::find($id);
        $prayer_tip->title = $request->input("title");
        $prayer_tip->content = $request->input("content");
        $prayer_tip->status = 2;
        $prayer_tip->created_by = auth()->user()->id;

        $result = $prayer_tip->save();

        if ($result) {
            activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('SUCCESS|UPDATE|Prayer Tip Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('prayer_tips.index');
        } else {
            activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('ERROR|UPDATE|Prayer Tip Update|Fail'); // ACTIVITY LOG
            return redirect()->route('prayer_tips.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('prayer_tips.delete')) {
            $prayer_tip = PrayerTip::find($id);
            $result = $prayer_tip->delete();
            if ($result) {
                activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('SUCCESS|DELETE|Prayer Tip Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('prayer_tips.index');
            } else {
                activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('ERROR|DELETE|Prayer Tip Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('prayer_tips.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Prayer Tip Delete|No prayer_tips.delete permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('prayer_tips.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $prayer_tip = PrayerTip::find($id);
            $prayer_tip->status = $status;
            if ($status == 1) {
                $prayer_tip->published_at = Carbon::now()->toDateTimeString();
            }

            $prayer_tip->authorized_by = auth()->user()->id;

            $result = $prayer_tip->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('SUCCESS|APPROVE|Prayer Tip Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($prayer_tip)->withProperties($prayer_tip)->log('ERROR|APPROVE|Prayer Tip Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            return abort(403);
        }

    }

    public function loadData()
    {
        $prayer_tips = PrayerTip::Latest()->get();

        return Datatables::of($prayer_tips)
            ->addColumn('created_user_email', function ($prayer_tip) {
                return $prayer_tip->createdUser != null ? $prayer_tip->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($prayer_tip) {
                return $prayer_tip->authorizedUser != null ? $prayer_tip->authorizedUser->email : "";
            })
            ->addColumn('action', function ($prayer_tip) {
                $html = '';

                if (auth()->user()->hasPermissionTo('prayer_tips.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $prayer_tip->id . ')"><i class="far fa-check-square"></i></button>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('prayer_tips.update')) {
                    $html .= '<a href="' . route('prayer_tips.edit', $prayer_tip->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('prayer_tips.delete')) {
                    $html .= '<form method="post" action="' . route('prayer_tips.destroy', $prayer_tip->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
