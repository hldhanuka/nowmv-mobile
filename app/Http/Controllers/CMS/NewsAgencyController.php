<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\NewsAgencyCreateRequest;
use App\Http\Requests\CMS\NewsAgencyUpdateRequest;
use App\News;
use App\NewsAgency;
use App\NewsAgencyUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class NewsAgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('news_agencies.list')) {
            return view('pages.news_agencies.index');
        } else {
            activity()->log('ERROR|VIEW|News Agency List|No news_agencies.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('news_agencies.create')) {
            activity()->log('SUCCESS|VIEW|News Agency Create'); // ACTIVITY LOG
            return view('pages.news_agencies.create');
        } else {
            activity()->log('ERROR|VIEW|News Agency Create|No news_agencies.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsAgencyCreateRequest $request)
    {
        $upload_result_logo = CMSHelper::fileUpload("logo", "uploads/news_agencies/");
        $news_agency = new NewsAgency();
        $news_agency->name = $request->input("name");
        $news_agency->logo = $upload_result_logo;
//        $news_agency->logo = $request->input("logo_url");
        $news_agency->status = 2;
        $news_agency->created_by = auth()->user()->id;

        $result = $news_agency->save();

        if ($result) {
            activity()->performedOn($news_agency)->withProperties($news_agency)->log('SUCCESS|CREATE|News Agency Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('news_agencies.index');
        } else {
            activity()->performedOn($news_agency)->withProperties($news_agency)->log('ERROR|CREATE|News Agency Create|Fail'); // ACTIVITY LOG
            return redirect()->route('news_agencies.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('news_agencies.update')) {
            $news_agency = NewsAgency::find($id);
            activity()->performedOn($news_agency)->withProperties($news_agency)->log('SUCCESS|VIEW|News Agency Edit'); // ACTIVITY LOG
            return view('pages.news_agencies.edit', ["news_agency" => $news_agency]);
        } else {
            activity()->log('ERROR|VIEW|News Agency Edit|No news_agencies.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsAgencyUpdateRequest $request, $id)
    {
        $upload_result_logo = CMSHelper::fileUpload("logo", "uploads/news_agencies/");
        $news_agency = NewsAgency::find($id);
        $news_agency->name = $request->input("name");
        $news_agency->logo = $upload_result_logo;
        if($upload_result_logo != null){
            $news_agency->logo = $upload_result_logo;
        }
//        if($request->input('logo_url') != null){
//            $news_agency->logo = $request->input('logo_url');
//        }
        $news_agency->status = 2;
        $news_agency->created_by = auth()->user()->id;

        $result = $news_agency->save();

        if ($result) {
            activity()->performedOn($news_agency)->withProperties($news_agency)->log('SUCCESS|UPDATE|News Agency Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('news_agencies.index');
        } else {
            activity()->performedOn($news_agency)->withProperties($news_agency)->log('ERROR|UPDATE|News Agency Update|Fail'); // ACTIVITY LOG
            return redirect()->route('news_agencies.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('news_agencies.delete')) {
            $news_agency = NewsAgency::find($id);
            $has_news = News::where("news_agency_id", $id)->exists();
            $has_agency_users = NewsAgencyUser::where("news_agency_id", $id)->exists();
            if ($has_news) {
                activity()->performedOn($news_agency)->withProperties($news_agency)->log('ERROR|DELETE|News Agency Delete|This news agency already has news.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This news agency already has news.');
                return redirect()->route('news_agencies.index');
            } else if ($has_agency_users) {
                activity()->performedOn($news_agency)->withProperties($news_agency)->log('ERROR|DELETE|News Agency Delete|This news agency already has news agency users.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This news agency already has news agency users.');
                return redirect()->route('news_agencies.index');
            } else {
                $result = $news_agency->delete();

                if ($result) {
                    activity()->performedOn($news_agency)->withProperties($news_agency)->log('SUCCESS|DELETE|News Agency Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('news_agencies.index');
                } else {
                    activity()->performedOn($news_agency)->withProperties($news_agency)->log('ERROR|DELETE|News Agency Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('news_agencies.index');
                }
            }
        } else {
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('news_agencies.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $news_agency = NewsAgency::find($id);
            $news_agency->status = $status;

            if ($status == 1) {
                $news_agency->published_at = Carbon::now()->toDateTimeString();
            }
            $news_agency->authorized_by = auth()->user()->id;

            $result = $news_agency->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_agency)->withProperties($news_agency)->log('SUCCESS|APPROVE|News Agency Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_agency)->withProperties($news_agency)->log('ERROR|APPROVE|News Agency Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|News Agency Approve|No news_agencies.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('news_agencies.list')) {
            if (auth()->user()->hasRole('admin')) {
                $news_agencies = NewsAgency::get();
            } else if (auth()->user()->hasRole('custom')) {
                $news_agencies = NewsAgency::where('created_by', auth()->user()->id)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($news_agencies)
            ->editColumn('logo', function ($news_agency) {
                return '<img width="50px" src="' . $news_agency->logo . '" alt="">';
            })
            ->addColumn('created_user_email', function ($news_agency) {
                return $news_agency->createdUser != null ? $news_agency->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($news_agency) {
                return $news_agency->authorizedUser != null ? $news_agency->authorizedUser->email : "";
            })
            ->addColumn('action', function ($news_agency) {
                $html = '';

                if (auth()->user()->hasPermissionTo('news_agencies.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $news_agency->id . ')">
                        <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news_agencies.update')) {
                    $html .= '<a href="' . route('news_agencies.edit', $news_agency->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news_agencies.delete')) {
                    $html .= '<form method="post" action="' . route('news_agencies.destroy', $news_agency->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()">
                        <i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['logo', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
