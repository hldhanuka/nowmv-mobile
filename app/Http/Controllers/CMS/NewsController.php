<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\NewsCreateRequest;
use App\Http\Requests\CMS\NewsUpdateRequest;
use App\News;
use App\NewsAgency;
use App\NewsAgencyUser;
use App\NewsCategory;
use App\NewsTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('news.list') || auth()->user()->hasPermissionTo('agent_news.list')) {
            $news = News::Latest()->paginate(10);
            return view('pages.news.index', ["news" => $news]);
        } else {
            activity()->log('ERROR|VIEW|News List|No news.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('news.create') || auth()->user()->hasPermissionTo('agent_news.create')) {
            $tags_list = NewsTag::active()->select("id", "title", "status")->get();
            $categories = NewsCategory::active()->select("id", "title", "status")->get();
            $news_agencies = NewsAgency::active()->select("id", "name", "status")->get();
            activity()->log('SUCCESS|VIEW|News Create'); // ACTIVITY LOG
            return view('pages.news.create', ["tags_list" => $tags_list, "categories" => $categories, "news_agencies" => $news_agencies]);
        } else {
            activity()->log('ERROR|VIEW|News Create|No news.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCreateRequest $request)
    {
//        $upload_image = $request->input("image_url");
        $upload_image = CMSHelper::fileUpload("image", "uploads/news/");
        $upload_video_file = CMSHelper::fileUpload("video_file", "uploads/news/");
//        $upload_video_thumbnail = $request->input("video_thumbnail_url");
        $upload_video_thumbnail = CMSHelper::fileUpload("video_thumbnail", "uploads/news/");
        $news_1 = new News();
        $news_1->title = $request->input("title");
        $news_1->category_id = $request->input("category_id");
        $news_1->author = $request->input("author");
        $news_1->image = $upload_image;
        $news_1->link = $request->input("link");
        $news_1->video = $request->input("video");  // video URL
        if ($upload_video_file != null) {
            $news_1->video = $upload_video_file;    // video file upload URL
        }
        $news_1->video_thumbnail = $upload_video_thumbnail;
        $news_1->is_english = $request->input("is_english");
        $news_1->content = $request->input("content");
        $news_1->html_content = $request->input("html_content");
        $tags_input = $request->input("tags") != null ? $request->input("tags") : [];
        $tags_array = array_map(function ($value) {
            return (int)$value;
        }, $tags_input);
        $news_1->tags = json_encode($tags_array);
        $news_1->is_featured = $request->input("is_featured");
        $news_1->status = 2;
        $news_1->created_by = auth()->user()->id;
        $news_1->news_agency_id = $request->input("news_agency_id");

        $result = $news_1->save();

        if ($result) {
            activity()->performedOn($news_1)->withProperties($news_1)->log('SUCCESS|CREATE|News Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('news.index');
        } else {
            activity()->performedOn($news_1)->withProperties($news_1)->log('ERROR|CREATE|News Create|Fail'); // ACTIVITY LOG
            return redirect()->route('news.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('news.update') || auth()->user()->hasPermissionTo('agent_news.update')) {

            if (auth()->user()->hasRole('admin')) {
                $news_1 = News::find($id);
            } else if (auth()->user()->hasRole('news_agent') || auth()->user()->hasRole('custom')) {
                $agency_ids = auth()->user()->agents->pluck("news_agency_id")->toArray();
                $news_1 = News::whereIn("news_agency_id", $agency_ids)->find($id);
                if ($news_1 == null) {
                    return abort(403);
                }
            } else {
                return abort(403);
            }
            $tags_list = NewsTag::active()->select("id", "title", "status")->get();
            $categories = NewsCategory::active()->select("id", "title", "status")->get();
            $news_agencies = NewsAgency::active()->select("id", "name", "status")->get();
            activity()->performedOn($news_1)->withProperties($news_1)->log('SUCCESS|VIEW|News Edit'); // ACTIVITY LOG
            return view('pages.news.edit', ["news_1" => $news_1, "tags_list" => $tags_list, "categories" => $categories, "news_agencies" => $news_agencies]);
        } else {
            activity()->log('ERROR|VIEW|News Edit|No news.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsUpdateRequest $request, $id)
    {

//        $upload_image = $request->input("image_url");
        $upload_image = CMSHelper::fileUpload("image", "uploads/news/");
        $upload_video_file = CMSHelper::fileUpload("video_file", "uploads/news/");
//        $upload_video_thumbnail = $request->input("video_thumbnail_url");
        $upload_video_thumbnail = CMSHelper::fileUpload("video_thumbnail", "uploads/news/");
        $news_1 = News::find($id);
        $news_1->title = $request->input("title");
        $news_1->category_id = $request->input("category_id");
        $news_1->author = $request->input("author");
        if ($upload_image != null) {
            $news_1->image = $upload_image;
        }
        $news_1->link = $request->input("link");
        $news_1->video = $request->input("video");
        $news_1->html_content = $request->input("html_content");
        if ($upload_video_file != null) {
            $news_1->video = $upload_video_file;
        }
        if ($upload_video_thumbnail != null) {
            $news_1->video_thumbnail = $upload_video_thumbnail;
        }
        // If remove video url and video file then remove video_thumbnail
        if ($upload_video_file == null && $request->input("video") == null) {
            $news_1->video_thumbnail = null;
        }
        $news_1->is_english = $request->input("is_english");
        $news_1->content = $request->input("content");
        $tags_input = $request->input("tags") != null ? $request->input("tags") : [];
        $tags_array = array_map(function ($value) {
            return (int)$value;
        }, $tags_input);
        $news_1->tags = json_encode($tags_array);
        $news_1->status = 2;
        $news_1->is_featured = $request->input("is_featured");
        $news_1->created_by = auth()->user()->id;
        $news_1->news_agency_id = $request->input("news_agency_id");

        $result = $news_1->save();

        if ($result) {
            activity()->performedOn($news_1)->withProperties($news_1)->log('SUCCESS|UPDATE|News Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('news.index');
        } else {
            activity()->performedOn($news_1)->withProperties($news_1)->log('ERROR|UPDATE|News Update|Fail'); // ACTIVITY LOG
            return redirect()->route('news.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('news.delete')) {
            $news_1 = News::find($id);
            $result = $news_1->delete();
            if ($result) {
                activity()->performedOn($news_1)->withProperties($news_1)->log('SUCCESS|DELETE|News Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('news.index');
            } else {
                activity()->performedOn($news_1)->withProperties($news_1)->log('ERROR|DELETE|News Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('news.index');
            }
        } else {
            activity()->log('ERROR|DELETE|News Delete|No news.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('news.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $news_1 = News::find($id);
            $news_1->status = $status;
            if ($status == 1) {
                $news_1->published_at = Carbon::now()->toDateTimeString();
            }

            $news_1->authorized_by = auth()->user()->id;

            $result = $news_1->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_1)->withProperties($news_1)->log('SUCCESS|APPROVE|News Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_1)->withProperties($news_1)->log('ERROR|APPROVE|News Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|News Approve|No news.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasRole('admin')) {
            $news = News::get();
        } else if (auth()->user()->hasPermissionTo('agent_news.list')) {
            $agency_ids = NewsAgencyUser::where("user_id", auth()->user()->id)->pluck("news_agency_id")->toArray();
            $news = News::whereIn('news_agency_id', $agency_ids)->get();
        } else if (auth()->user()->hasRole('custom') && auth()->user()->hasPermissionTo('news.list')) {
            $news = News::where('created_by',auth()->user()->id)->get();
        } else {
            return abort(403);
        }

        return Datatables::of($news)
            ->editColumn('image', function ($news_1) {
                return '<img width="50px" src="' . $news_1->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($news_1) {
                return $news_1->createdUser != null ? $news_1->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($news_1) {
                return $news_1->authorizedUser != null ? $news_1->authorizedUser->email : "";
            })
            ->addColumn('action', function ($news_1) {

                $html = '';

                if (auth()->user()->hasPermissionTo('news.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $news_1->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news.update') || auth()->user()->hasPermissionTo('agent_news.update')) {
                    $html .= '<a href="' . route('news.edit', $news_1->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news.delete') || auth()->user()->hasPermissionTo('agent_news.delete')) {
                    $html .= '<form method="post" action="' . route('news.destroy', $news_1->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
