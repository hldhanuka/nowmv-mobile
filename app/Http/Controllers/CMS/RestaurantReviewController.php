<?php

namespace App\Http\Controllers\CMS;

use App\MerchantUser;
use App\Restaurant;
use App\RestaurantReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class RestaurantReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('restaurant_reviews.list')) {
            $restaurant_reviews = RestaurantReview::with(['restaurant'])->latest()->paginate(10);
            return view('pages.restaurant_reviews.index', ["restaurant_reviews" => $restaurant_reviews]);
        } else {
            activity()->log('ERROR|VIEW|Restaurant Review List|No restaurant_reviews.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('restaurant_reviews.delete')) {
            $restaurant_review = RestaurantReview::find($id);
            $result = $restaurant_review->delete();

            if ($result) {
                activity()->performedOn($restaurant_review)->withProperties($restaurant_review)->log('SUCCESS|DELETE|Restaurant Review Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('restaurant_reviews.index');
            } else {
                activity()->performedOn($restaurant_review)->withProperties($restaurant_review)->log('ERROR|DELETE|Restaurant Review Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('restaurant_reviews.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Restaurant Review Delete|No restaurant_reviews.delete permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    public function loadData()
    {
        if(auth()->user()->hasPermissionTo('restaurant_reviews.list')) {
            if (auth()->user()->hasRole('admin')) {
                $restaurant_reviews = RestaurantReview::with('restaurant')->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
                $restaurant_ids = Restaurant::whereIn("merchant_id", $merchant_ids)->pluck("id")->toArray();
                $restaurant_reviews = RestaurantReview::with('restaurant')->whereIn('restaurant_id', $restaurant_ids)->Latest()->get();
            } else {
                return abort(403);
            }
        }else {
            return abort(403);
        }

        return Datatables::of($restaurant_reviews)
            ->editColumn('restaurant_id', function ($restaurant_review) {
                return ($restaurant_review->restaurant != null) ? $restaurant_review->restaurant->title : "-";
            })
            ->addColumn('created_user_email', function ($restaurant_review) {
                return ($restaurant_review->createdUser != null) ? $restaurant_review->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($restaurant_review) {
                return ($restaurant_review->authorizedUser != null) ? $restaurant_review->authorizedUser->email : "";
            })
            ->addColumn('action', function ($restaurant_review) {

                $html = '';
                if (auth()->user()->hasRole("admin")||auth()->user()->hasPermissionTo('restaurant_reviews.delete') ) {
                    $html .= '<form method="post" action="' . route('restaurant_reviews.destroy', $restaurant_review->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['restaurant_id', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
