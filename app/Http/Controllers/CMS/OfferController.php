<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\OfferCreateRequest;
use App\Http\Requests\CMS\OfferUpdateRequest;
use App\Merchant;
use App\MerchantUser;
use App\Offer;
use App\OfferCategory;
use App\Rules\ValidMobileArray;
use App\TargetOfferUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('offers.list')) {
            return view('pages.offers.index');
        } else if (auth()->user()->hasPermissionTo('merchant_offer.list')) {
            return view('pages.offers.index');
        } else {
            activity()->log('ERROR|VIEW|Offer List|No offers.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('offers.create') || auth()->user()->can('merchant_offer.create')) {
            $categories = OfferCategory::active()->select("id", "title", "status")->orderBy('title')->get();
            $merchants = Merchant::active()->select("id", "name", "status")->orderBy('name')->get();
            activity()->log('SUCCESS|VIEW|Offer Create'); // ACTIVITY LOG
            return view('pages.offers.create', ["categories" => $categories, "merchants" => $merchants]);
        } else {
            activity()->log('ERROR|VIEW|Offer Create|No offers.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferCreateRequest $request)
    {
        if ($request->input("is_target_offer")) { //validate mobile numbers

            if ($request->input('mobile_numbers')) {
                $mobile_numbers = array_map('trim', explode(',', $request->input("mobile_numbers")));
                $request->validate([
                    'mobile_numbers' => [new ValidMobileArray($mobile_numbers)]
                ]);
            }

            if ($request->file('mobile_numbers_csv')) {
                $file = $request->file('mobile_numbers_csv');
                $csv_mobile_numbers = array_filter(Arr::flatten(Excel::toArray([], $file)));
                $request->validate([
                    'mobile_numbers_csv' => [new ValidMobileArray($csv_mobile_numbers)]
                ]);
            }

        }

        try {
            DB::beginTransaction();
            $upload_result = CMSHelper::fileUpload("image", "uploads/offers/");
            $offer = new Offer();
            $offer->title = $request->input("title");
            $offer->category_id = $request->input("category_id");
            $offer->merchant_id = $request->input("merchant_id");
            $offer->content = $request->input("content");
            $offer->image = $upload_result;
//            $offer->image = $request->input("image_url");
            $offer->is_best = $request->input("is_best");
            $offer->for_male = $request->input("for_male");
            $offer->for_female = $request->input("for_female");
            $offer->min_age = $request->input("min_age");
            $offer->max_age = $request->input("max_age");
            if ($request->input("start_datetime")) {
                $offer->start_datetime = Carbon::parse($request->input("start_datetime"));
            } else {
                $offer->start_datetime = null;
            }
            if ($request->input("end_datetime")) {
                $offer->end_datetime = Carbon::parse($request->input("end_datetime"));
            } else {
                $offer->end_datetime = null;
            }
            if ($request->input("is_target_offer")) {
                $offer->is_target_offer = $request->input("is_target_offer");
            }
            if ($request->input("is_send_push_notification")) {
                $offer->is_send_push_notification = $request->input("is_send_push_notification");
                $offer->push_message = $request->input("push_message");
                $offer->push_notification_status = "0";
            }
            //$offer->amount = $request->input("amount");
            $offer->has_redeem = $request->input("has_redeem");
            $offer->status = 2;
            $offer->created_by = auth()->user()->id;

            $offer->save();

            if ($offer->is_target_offer) {  //create target offer users
                $mobile_numbers = array_map('trim', explode(',', $request->input("mobile_numbers")));
                $all_target_users = User::role("customer")->whereIn('mobile', $mobile_numbers)->active()->get();

                if ($request->file('mobile_numbers_csv')) {
                    $file = $request->file('mobile_numbers_csv');
                    $csv_mobile_numbers = Arr::flatten(Excel::toArray([], $file));
                    $all_target_users = $all_target_users->merge(User::role("customer")->whereIn('mobile', $csv_mobile_numbers)->active()->get());
                }
                $all_target_users = $all_target_users->unique();

                foreach ($all_target_users as $target_user) {
                    $target_offer_user = new TargetOfferUser();
                    $target_offer_user->offer_id = $offer->id;
                    $target_offer_user->user_id = $target_user->id;
                    $target_offer_user->save();
                    activity()->performedOn($target_offer_user)->withProperties($target_offer_user)->log('SUCCESS|CREATE|TargetOfferUser Create'); // ACTIVITY LOG
                }
            }

            DB::commit();
            activity()->performedOn($offer)->withProperties($offer)->log('SUCCESS|CREATE|Offer Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('offers.index');

        } catch (QueryException $ex) {
            DB::rollBack();
            activity()->performedOn($offer)->withProperties($offer)->log('ERROR|CREATE|Offer Create|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('offers.index');
        } catch (\Exception $ex) {
            DB::rollBack();
            activity()->performedOn($offer)->withProperties($offer)->log('ERROR|CREATE|Offer Create|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('offers.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobile_numbers_string = '';
        if (auth()->user()->hasPermissionTo('offers.update') || auth()->user()->hasPermissionTo('merchant_offer.update')) {
            if (auth()->user()->hasRole('admin')) {
                $offer = Offer::find($id);
                $offer_users = TargetOfferUser::on()->where('offer_id', $id)->pluck('user_id');
                $mobile_numbers = User::on()->whereIn('id', $offer_users)->pluck('mobile');
                if (count($mobile_numbers) > 0) {
                    $mobile_numbers_string = join(',', $mobile_numbers->toArray());
                }
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $merchant_ids = auth()->user()->merchants->pluck("merchant_id")->toArray();
                $offer = Offer::whereIn("merchant_id", $merchant_ids)->find($id);
                $offer_users = TargetOfferUser::on()->where('offer_id', $id)->pluck('user_id');
                $mobile_numbers = User::on()->whereIn('id', $offer_users)->pluck('mobile');
                if (count($mobile_numbers) > 0) {
                    $mobile_numbers_string = join(',', $mobile_numbers->toArray());
                }
                if ($offer == null) {
                    return abort(403);
                }
            }
            $categories = OfferCategory::active()->select("id", "title", "status")->orderBy('title')->get();
            $merchants = Merchant::active()->select("id", "name", "status")->orderBy('name')->get();
            activity()->performedOn($offer)->withProperties($offer)->log('SUCCESS|VIEW|Offer Edit'); // ACTIVITY LOG
            return view('pages.offers.edit', ["offer" => $offer, "categories" => $categories, "merchants" => $merchants, "mobile_numbers" => $mobile_numbers_string]);
        } else {
            activity()->log('ERROR|VIEW|Offer Edit|No offers.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OfferUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/offers/");
        $offer = Offer::find($id);
        $offer->title = $request->input("title");
        $offer->category_id = $request->input("category_id");
        $offer->merchant_id = $request->input("merchant_id");
        $offer->content = $request->input("content");
        if ($upload_result != null) {
            $offer->image = $upload_result;
        }
//        if ($request->input("image_url") != null) {
//            $offer->image = $request->input("image_url");
//        }
        $offer->is_best = $request->input("is_best");
        $offer->for_male = $request->input("for_male");
        $offer->for_female = $request->input("for_female");
        $offer->min_age = $request->input("min_age");
        $offer->max_age = $request->input("max_age");
        if ($request->input("start_datetime")) {
            $offer->start_datetime = Carbon::parse($request->input("start_datetime"));
        } else {
            $offer->start_datetime = null;
        }
        if ($request->input("end_datetime")) {
            $offer->end_datetime = Carbon::parse($request->input("end_datetime"));
        } else {
            $offer->end_datetime = null;
        }
        //$offer->amount = $request->input("amount");
        $offer->has_redeem = $request->input("has_redeem");
        $offer->status = 2;
        $offer->created_by = auth()->user()->id;

        $result = $offer->save();

        if ($result) {
            activity()->performedOn($offer)->withProperties($offer)->log('SUCCESS|UPDATE|Offer Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('offers.index');
        } else {
            activity()->performedOn($offer)->withProperties($offer)->log('ERROR|UPDATE|Offer Update|Fail'); // ACTIVITY LOG
            return redirect()->route('offers.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('offers.delete')) {
            $offer = Offer::find($id);
            DB::beginTransaction();
            if ($offer->is_target_offer) {
                $target_users = TargetOfferUser::where('offer_id', $offer->id)->get();
                foreach ($target_users as $target_user) {
                    $target_user->delete();
                }
            }
            $result = $offer->delete();

            if ($result) {
                DB::commit();
                activity()->performedOn($offer)->withProperties($offer)->log('SUCCESS|DELETE|Offer Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('offers.index');
            } else {
                DB::rollBack();
                activity()->performedOn($offer)->withProperties($offer)->log('ERROR|DELETE|Offer Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('offers.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Offer Delete|No offers.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('offers.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $offer = Offer::find($id);
            $offer->status = $status;

            if ($status == 1) {
                $offer->published_at = Carbon::now()->toDateTimeString();
                if ($offer->push_notification_status == "0") { //send push notifications
                    $message = $offer->push_message;
                    if ($offer->is_target_offer) {
                        $target_users = TargetOfferUser::where('offer_id', $offer->id)->get();
                        foreach ($target_users as $target_user) {
                            $data = ["type" => "offer_home", "id" => null];
                            OneSignalPush::sendPushNotification($message, "offer", $data, 0, $target_user->user_id, null);
                        }
                        $offer->push_notification_status = "1";
                    } else {
                        $data = ["type" => "offer_home", "id" => null];
                        OneSignalPush::sendPushNotification($message, "offer", $data, 0, null, null);
                        $offer->push_notification_status = "1";
                    }
                }
            }

            $offer->authorized_by = auth()->user()->id;

            $result = $offer->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($offer)->withProperties($offer)->log('SUCCESS|APPROVE|Offer Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($offer)->withProperties($offer)->log('ERROR|APPROVE|Offer Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Offer Approve|No offers.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('offers.list')) {
            $offers = Offer::with(['category'])->get();
        } else if (auth()->user()->hasPermissionTo('merchant_offer.list')) {
            $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
            $offers = Offer::with(['category'])->whereIn('merchant_id', $merchant_ids)->get();
        } else {
            return abort(403);
        }

        return Datatables::of($offers)
            ->editColumn('image', function ($offer) {
                return '<img width="50px" src="' . $offer->image . '" alt="">';
            })
            ->editColumn('category_id', function ($offer) {
                return $offer->category->title;
            })
            ->addColumn('created_user_email', function ($offer) {
                return $offer->createdUser != null ? $offer->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($offer) {
                return $offer->authorizedUser != null ? $offer->authorizedUser->email : "";
            })
            ->addColumn('action', function ($offer) {

                $html = '';

                if (auth()->user()->hasPermissionTo('offers.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $offer->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('offers.update') || auth()->user()->hasPermissionTo('merchant_offer.update')) {
                    $html .= '<a href="' . route('offers.edit', $offer->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('offers.delete') || auth()->user()->hasPermissionTo('merchant_offer.delete')) {
                    $html .= '<form method="post" action="' . route('offers.destroy', $offer->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['image', 'category_id', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
