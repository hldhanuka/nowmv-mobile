<?php

namespace App\Http\Controllers\CMS;

use App\BusHaltSchedule;
use App\BusLocation;
use App\BusRoute;
use App\BusRouteSchedule;
use App\BusType;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\BusRouteCreateRequest;
use App\Http\Requests\CMS\BusRouteUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class BusRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('bus_routes.list')) {
            $bus_routes = BusRoute::with(['fromLocation', 'toLocation'])->Latest()->paginate(10);
            return view('pages.bus_routes.index', ["bus_routes" => $bus_routes]);
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('bus_routes.create')) {
            $locations = BusLocation::active()->select("id", "name", "status")->orderBy('name')->get();
            $bus_types = BusType::select("id", "name", "status")->active()->get();
            return view('pages.bus_routes.create', ["locations" => $locations, "bus_types"=>$bus_types]);
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusRouteCreateRequest $request)
    {
        $bus_route = new BusRoute();
        $bus_route->name = $request->input("name");
        $bus_route->from = $request->input("from");
        $bus_route->to = $request->input("to");
        $bus_route->bus_type_id = $request->input("type");
        $bus_route->status = $request->input("status");

        $result = $bus_route->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('bus_routes.index');
        } else {
            return redirect()->route('bus_routes.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('bus_routes.update')) {
            $bus_route = BusRoute::find($id);
            $locations = BusLocation::active()->select("id", "name", "status")->orderBy('name')->get();
            $bus_types = BusType::active()->select("id", "name", "status")->get();
            return view('pages.bus_routes.edit', ["bus_route" => $bus_route, "locations" => $locations, "bus_types"=>$bus_types]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BusRouteUpdateRequest $request, $id)
    {
        $bus_route = BusRoute::find($id);
        $bus_route->name = $request->input("name");
        $bus_route->from = $request->input("from");
        $bus_route->to = $request->input("to");
        $bus_route->bus_type_id = $request->input("type");
        $bus_route->status = $request->input("status");

        $result = $bus_route->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Update');
            return redirect()->route('bus_routes.index');
        } else {
            return redirect()->route('bus_routes.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('bus_routes.delete')) {
            $bus_route = BusRoute::find($id);
            $result = $bus_route->delete();

            $has_bus_route_schedule = BusRouteSchedule::where("bus_route_id", $id)->exists();
            $has_bus_halt_schedule = BusHaltSchedule::where("bus_route_id", $id)->exists();
            if($has_bus_route_schedule){
                connectify('error', 'Error', 'Some bus route schedules already use this bus route.');
                return redirect()->route('bus_routes.index');
            }else if($has_bus_halt_schedule){
                connectify('error', 'Error', 'Some bus halt schedules already use this bus route.');
                return redirect()->route('bus_routes.index');
            } else {
                if ($result) {
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('bus_routes.index');
                } else {
                    return redirect()->route('bus_routes.index');
                }
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $bus_routes = BusRoute::with(['fromLocation','toLocation'])->Latest()->get();

        return Datatables::of($bus_routes)
            ->editColumn('from', function ($bus_route) {
                return $bus_route->fromLocation->name;
            })
            ->editColumn('to', function ($bus_route) {
                return $bus_route->toLocation->name;
            })
            ->editColumn('type', function ($bus_route) {
                return $bus_route->busType->name;
            })
            ->addColumn('action', function ($bus_route) {
                return '
                         <a href="'. route('bus_routes.edit',$bus_route->id) .'">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a>
                         <br>
                         <br>
                         <form method="post" action="'. route('bus_routes.destroy',$bus_route->id) .'">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['from','to','type','action'])
            ->make(true);

    }
}
