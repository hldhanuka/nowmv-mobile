<?php

namespace App\Http\Controllers\CMS;

use App\FerryRoute;
use App\FerryTerminalSchedule;
use App\Helpers\CMSHelper;
use App\Http\Requests\CMS\FerryTerminalScheduleCreateRequest;
use App\Http\Requests\CMS\FerryTerminalScheduleUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class FerryTerminalScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ferry_terminal_schedules.list')) {
            return view('pages.ferry_terminal_schedules.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ferry_terminal_schedules.create')) {
            $ferry_routes = FerryRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $locations = CMSHelper::getFerryLocations();
            return view('pages.ferry_terminal_schedules.create', ["ferry_routes" =>$ferry_routes, "locations" => $locations]);
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FerryTerminalScheduleCreateRequest $request)
    {
        $ferry_terminal_schedule = new FerryTerminalSchedule();
        $ferry_terminal_schedule->ferry_route_id = $request->input("ferry_route_id");
        $ferry_terminal_schedule->start_location_id = $request->input("start_location_id");
        $ferry_terminal_schedule->terminal_id = $request->input("terminal_id");
        $ferry_terminal_schedule->status = $request->input("status");
        $ferry_terminal_schedule->time = $request->input("time");

        $result = $ferry_terminal_schedule->save();
        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ferry_terminal_schedules.index');
        } else {
            return redirect()->route('ferry_terminal_schedules.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_terminal_schedules.update')) {
            $ferry_terminal_schedule = FerryTerminalSchedule::find($id);
            $ferry_routes = FerryRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $locations = CMSHelper::getFerryLocations();
            return view('pages.ferry_terminal_schedules.edit', ["ferry_routes" =>$ferry_routes, "locations" => $locations, "ferry_terminal_schedule"=>$ferry_terminal_schedule]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FerryTerminalScheduleUpdateRequest $request, $id)
    {
        $ferry_terminal_schedule = FerryTerminalSchedule::find($id);
        $ferry_terminal_schedule->ferry_route_id = $request->input("ferry_route_id");
        $ferry_terminal_schedule->start_location_id = $request->input("start_location_id");
        $ferry_terminal_schedule->terminal_id = $request->input("terminal_id");
        $ferry_terminal_schedule->status = $request->input("status");
        $ferry_terminal_schedule->time = $request->input("time");

        $result = $ferry_terminal_schedule->save();
        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ferry_terminal_schedules.index');
        } else {
            return redirect()->route('ferry_terminal_schedules.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_terminal_schedules.delete')) {
            $ferry_terminal_schedule = FerryTerminalSchedule::find($id);
            $result = $ferry_terminal_schedule->delete();

            if ($result) {
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('ferry_terminal_schedules.index');
            } else {
                return redirect()->route('ferry_terminal_schedules.index');
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $ferry_terminal_schedules = FerryTerminalSchedule::with('ferryRoute')->Latest()->get();
        $island_list = CMSHelper::getFerryLocations();

        return Datatables::of($ferry_terminal_schedules)
            ->editColumn('ferry_route', function ($ferry_terminal_schedule) {
                return $ferry_terminal_schedule->ferryRoute->name;
            })
            ->editColumn('start_location', function ($ferry_terminal_schedule) use ($island_list){
                return $island_list->where("island_id",$ferry_terminal_schedule->start_location_id)->first()["island_name"];
            })
            ->editColumn('terminal', function ($ferry_terminal_schedule) use ($island_list){
                return $island_list->where("island_id",$ferry_terminal_schedule->terminal_id)->first()["island_name"];
            })
            ->addColumn('action', function ($ferry_terminal_schedule) {
                return '
                         <a href="'. route('ferry_terminal_schedules.edit',$ferry_terminal_schedule->id) .'">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a>
                         <br>
                         <br>
                         <form method="post" action="'. route('ferry_terminal_schedules.destroy',$ferry_terminal_schedule->id) .'">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['ferry_route','start_location','terminal', 'action'])
            ->make(true);

    }
}
