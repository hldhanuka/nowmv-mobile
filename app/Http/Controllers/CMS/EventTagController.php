<?php

namespace App\Http\Controllers\CMS;

use App\Event;
use App\EventTag;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\EventTagCreateRequest;
use App\Http\Requests\CMS\EventTagUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class EventTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('event_tags.list')) {
            return view('pages.event_tags.index');
        } else {
            activity()->log('ERROR|VIEW|Event Tags List|No event_tags.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('event_tags.create')) {
            activity()->log('SUCCESS|VIEW|Event Tag Create'); // ACTIVITY LOG
            return view('pages.event_tags.create');
        } else {
            activity()->log('ERROR|VIEW|Event Tag Create|No event_tags.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EventTagCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EventTagCreateRequest $request)
    {
        $event_tag = new EventTag();
        $event_tag->title = $request->input("title");
        $event_tag->status = 1;
        //$event_tag->created_by = auth()->user()->id;

        $result = $event_tag->save();

        if ($result) {
            activity()->performedOn($event_tag)->withProperties($event_tag)->log('SUCCESS|CREATE|Event Tag Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('event_tags.index');
        } else {
            activity()->performedOn($event_tag)->withProperties($event_tag)->log('ERROR|CREATE|Event Tag Create|Fail'); // ACTIVITY LOG
            return redirect()->route('event_tags.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('event_tags.update')) {
            $event_tag = EventTag::find($id);
            activity()->performedOn($event_tag)->withProperties($event_tag)->log('SUCCESS|VIEW|Event Tag Edit'); // ACTIVITY LOG
            return view('pages.event_tags.edit', ["event_tag" => $event_tag]);
        } else {
            activity()->log('ERROR|VIEW|Event Tag Edit|No event_tags.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventTagUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EventTagUpdateRequest $request, $id)
    {
        $event_tag = EventTag::find($id);
        $event_tag->title = $request->input("title");
        $event_tag->status = 1;
        //$event_tag->created_by = auth()->user()->id;

        $result = $event_tag->save();

        if ($result) {
            activity()->performedOn($event_tag)->withProperties($event_tag)->log('SUCCESS|UPDATE|Event Tag Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('event_tags.index');
        } else {
            activity()->performedOn($event_tag)->withProperties($event_tag)->log('ERROR|UPDATE|Event Tag Update|Fail'); // ACTIVITY LOG
            return redirect()->route('event_tags.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('event_tags.delete')) {
            $event_tag = EventTag::find($id);

            $event_has_event_tag = Event::where("tag_id", $id)->exists();
            if ($event_has_event_tag) {
                activity()->log('ERROR|DELETE|Event Tag Delete|Event tag already used in events'); // ACTIVITY LOG
                connectify('error', 'Error', 'Event tag already used in events');
                return redirect()->route('event_tags.index');
            } else {
                $result = $event_tag->delete();

                if ($result) {
                    activity()->performedOn($event_tag)->withProperties($event_tag)->log('SUCCESS|DELETE|Event Tag Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('event_tags.index');
                } else {
                    activity()->performedOn($event_tag)->withProperties($event_tag)->log('ERROR|DELETE|Event Tag Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('event_tags.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|Event Tag Delete|No event_tags.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('event_tags.list')) {
            if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('custom')) {
                $event_tags = EventTag::get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($event_tags)
            ->addColumn('action', function ($event_tag) {
                $html = '';

                if (auth()->user()->hasPermissionTo('event_tags.update') || auth()->user()->hasPermissionTo('event_tags.update')) {
                    $html .= '<a href="' . route('event_tags.edit', $event_tag->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('event_tags.delete') || auth()->user()->hasPermissionTo('event_tags.delete')) {
                    $html .= '<form method="post" action="' . route('event_tags.destroy', $event_tag->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['action'])
            ->make(true);

    }
}
