<?php

namespace App\Http\Controllers\CMS;

use App\MerchantUser;
use App\Offer;
use App\Merchant;
use App\Restaurant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\MerchantCreateRequest;
use App\Http\Requests\CMS\MerchantUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('merchants.list')) {
            $merchants = Merchant::latest()->paginate(10);
            return view('pages.merchants.index', ["merchants" => $merchants]);
        } else {
            activity()->log('ERROR|VIEW|Merchant List|No merchants.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('merchants.create')) {
            activity()->log('SUCCESS|VIEW|Merchant Create'); // ACTIVITY LOG
            return view('pages.merchants.create');
        } else {
            activity()->log('ERROR|VIEW|Merchant Create|No merchants.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MerchantCreateRequest $request)
    {
        $merchant = new Merchant();
        $merchant->name = $request->input('name');
        $merchant->key = $request->input('key');
        $merchant->status = 2;
        $merchant->created_by = auth()->user()->id;
        $result = $merchant->save();

        if ($result) {
            activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|CREATE|Merchant Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('merchants.index');
        } else {
            activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|CREATE|Merchant Create|Fail'); // ACTIVITY LOG
            return redirect()->route('merchants.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('merchants.update')) {
            $merchant = Merchant::find($id);
            activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|VIEW|Merchant Edit'); // ACTIVITY LOG
            return view('pages.merchants.edit', ["merchant" => $merchant]);
        } else {
            activity()->log('ERROR|VIEW|Merchant Edit|No merchants.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MerchantUpdateRequest $request, $id)
    {
        $merchant = Merchant::find($id);
        $merchant->name = $request->input('name');
        $merchant->key = $request->input('key');
        $merchant->status = 2;
        $merchant->created_by = auth()->user()->id;
        $result = $merchant->save();

        if ($result) {
            activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|UPDATE|Merchant Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('merchants.index');
        } else {
            activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|UPDATE|Merchant Update|Fail'); // ACTIVITY LOG
            return redirect()->route('merchants.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('merchants.delete')) {
            $merchant = Merchant::find($id);
            $has_offer = Offer::where("merchant_id", $id)->exists();
            $has_restaurant = Restaurant::where("merchant_id", $id)->exists();
            if ($has_offer) {
                activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|DELETE|Merchant Delete|This merchant already has offers.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This merchant already has offers.');
                return redirect()->route('merchants.index');
            } else if ($has_restaurant) {
                activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|DELETE|Merchant Delete|This merchant already has restaurants.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This merchant already has restaurants.');
                return redirect()->route('merchants.index');
            } else {
                MerchantUser::where("merchant_id", $id)->delete();
                $result = $merchant->delete();
                if ($result) {
                    activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|DELETE|Merchant Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('merchants.index');
                } else {
                    activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|DELETE|Merchant Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('merchants.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|Merchant Delete|No merchants.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('merchants.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $merchant = Merchant::find($id);
            $merchant->status = $status;

            if ($status == 1) {
                $merchant->published_at = Carbon::now()->toDateTimeString();
            }
            $merchant->authorized_by = auth()->user()->id;

            $result = $merchant->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|APPROVE|Merchant Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|APPROVE|Merchant Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Merchant Approve|No merchants.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('merchants.list')) {
            if (auth()->user()->hasRole('admin')) {
                $merchants = Merchant::get();
            } else if (auth()->user()->hasRole('custom')) {
                $merchants = Merchant::where('created_by', auth()->user()->id)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($merchants)
            ->addColumn('created_user_email', function ($merchant) {
                return $merchant->createdUser != null ? $merchant->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($merchant) {
                return $merchant->authorizedUser != null ? $merchant->authorizedUser->email : "";
            })
            ->addColumn('action', function ($merchant) {
                $html = '';

                if (auth()->user()->hasPermissionTo('merchants.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $merchant->id . ')">
                        <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('merchants.update') || auth()->user()->hasPermissionTo('merchants.update')) {
                    $html .= '<a href="' . route('merchants.edit', $merchant->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('merchants.delete') || auth()->user()->hasPermissionTo('merchants.delete')) {
                    $html .= '<form method="post" action="' . route('merchants.destroy', $merchant->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
