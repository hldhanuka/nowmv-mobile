<?php

namespace App\Http\Controllers\CMS;


use App\FerryLocation;
use App\FerryRoute;
use App\FerryRouteSchedule;
use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Http\Requests\CMS\BusRouteUpdateRequest;
use App\Http\Requests\CMS\FerryRouteCreateRequest;
use App\Http\Requests\CMS\FerryRouteUpdateRequest;
use App\Http\Requests\CMS\FerryScheduleTypeCreateRequest;
use App\Libraries\Dhathuru\DHATHURU;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class FerryRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ferry_routes.list')) {
            $ferry_routes = FerryRoute::with(['fromLocation', 'toLocation'])->Latest()->paginate(10);
            return view('pages.ferry_routes.index', ["ferry_routes" => $ferry_routes]);
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ferry_routes.create')) {
            //$locations = FerryLocation::active()->select("id", "name", "status")->orderBy('name')->get();
            $locations = CMSHelper::getFerryLocations();
            return view('pages.ferry_routes.create', ["locations" => $locations]);
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FerryRouteCreateRequest $request)
    {
        $ferry_route = new FerryRoute();
        $ferry_route->name = $request->input("name");
        $ferry_route->from = $request->input("from");
        $ferry_route->to = $request->input("to");
        $ferry_route->type = "public";
        $ferry_route->status = $request->input("status");

        $result = $ferry_route->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ferry_routes.index');
        } else {
            return redirect()->route('ferry_routes.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_routes.update')) {
            $ferry_route = FerryRoute::find($id);
            //$locations = FerryRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $locations = CMSHelper::getFerryLocations();
            return view('pages.ferry_routes.edit', ["ferry_route" => $ferry_route, "locations" => $locations]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FerryRouteUpdateRequest $request, $id)
    {
        $ferry_route = FerryRoute::find($id);
        $ferry_route->name = $request->input("name");
        $ferry_route->from = $request->input("from");
        $ferry_route->to = $request->input("to");
        $ferry_route->type = "public";
        $ferry_route->status = $request->input("status");

        $result = $ferry_route->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Update');
            return redirect()->route('ferry_routes.index');
        } else {
            return redirect()->route('ferry_routes.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_routes.delete')) {
            $ferry_route = FerryRoute::find($id);
            $has_ferry_route_schedules = FerryRouteSchedule::where("ferry_route_id", $id)->exists();
            if ($has_ferry_route_schedules) {
                connectify('error', 'Error', 'This ferry route already use in some route schedules');
                return redirect()->route('ferry_locations.index');
            } else {
                $result = $ferry_route->delete();

                if ($result) {
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('ferry_routes.index');
                } else {
                    return redirect()->route('ferry_routes.index');
                }
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        //$ferry_routes = FerryRoute::with(['fromLocation', 'toLocation'])->Latest()->get();
        $island_list = CMSHelper::getFerryLocations();
        $ferry_routes = FerryRoute::Latest()->get();

        return Datatables::of($ferry_routes)
            ->editColumn('from', function ($ferry_route) use ($island_list) {
                return $island_list->where("island_id",$ferry_route->from)->first()["island_name"];
            })
            ->editColumn('to', function ($ferry_route) use ($island_list) {
                return $island_list->where("island_id",$ferry_route->to)->first()["island_name"];
            })
            ->addColumn('action', function ($ferry_route) {
                return '
                         <a href="' . route('ferry_routes.edit', $ferry_route->id) . '">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a>
                         <br>
                         <br>
                         <form method="post" action="' . route('ferry_routes.destroy', $ferry_route->id) . '">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['from', 'to', 'action'])
            ->make(true);

    }
}


