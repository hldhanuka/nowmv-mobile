<?php

namespace App\Http\Controllers\CMS;

use App\Ad;
use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\AdCreateRequest;
use App\Http\Requests\CMS\AdUpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ads.list')) {
            return view('pages.ads.index');
        } else {
            activity()->log('ERROR|VIEW|Ads List|No ads.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ads.create')) {
            $ad_counts = Ad::select('type', DB::raw('count(*) as total'))
                ->groupBy('type')
                ->active()
                ->get();
            activity()->log('SUCCESS|VIEW|Ad Create'); // ACTIVITY LOG
            return view('pages.ads.create', ['ad_counts' => $ad_counts]);
        } else {
            activity()->log('ERROR|VIEW|Ad Create|No ads.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/ads/");
        $ad = new Ad();
        $ad->title = $request->input("title");
        $ad->type = $request->input("type");
        $ad->image = $upload_result;
        //$ad->image = $request->input('image_url');
        $ad->status = 2;
        $ad->created_by = auth()->user()->id;

        if ($ad->status == 1) {
            $ad->published_at = Carbon::now()->toDateTimeString();
        }
        if ($ad->status == 1) {
            $ad->authorized_by = auth()->user()->id;
        }

        $result = $ad->save();

        if ($result) {
            activity()->performedOn($ad)->withProperties($ad)->log('SUCCESS|CREATE|Ad Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ads.index');
        } else {
            activity()->performedOn($ad)->withProperties($ad)->log('ERROR|CREATE|Ad Create|Fail'); // ACTIVITY LOG
            return redirect()->route('ads.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('ads.update')) {
            $ad = Ad::find($id);
            $ad_counts = Ad::select('type', DB::raw('count(*) as total'))
                ->groupBy('type')
                ->active()
                ->get();
            activity()->performedOn($ad)->withProperties($ad)->log('SUCCESS|VIEW|Ads Edit'); // ACTIVITY LOG
            return view('pages.ads.edit', ['ad' => $ad, 'ad_counts' => $ad_counts]);
        } else {
            activity()->log('ERROR|VIEW|Ad Edit|No ads.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/ads/");
        $ad = Ad::find($id);
        $ad->title = $request->input("title");
        $ad->type = $request->input("type");
        if ($upload_result != null) {
            $ad->image = $upload_result;
        }
        /*if($request->input('image_url') != null){
            $ad->image = $request->input('image_url');
        }*/
        $ad->status = 2;
        $ad->created_by = auth()->user()->id;

        $result = $ad->save();

        if ($result) {
            activity()->performedOn($ad)->withProperties($ad)->log('SUCCESS|UPDATE|Ads Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('ads.index');
        } else {
            activity()->performedOn($ad)->withProperties($ad)->log('ERROR|UPDATE|Ads Update|Fail'); // ACTIVITY LOG
            return redirect()->route('ads.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ads.delete')) {
            $ad = Ad::find($id);
            $result = $ad->delete();

            if ($result) {
                activity()->performedOn($ad)->withProperties($ad)->log('SUCCESS|DELETE|Ad Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('ads.index');
            } else {
                activity()->performedOn($ad)->withProperties($ad)->log('ERROR|DELETE|Ad Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('ads.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Ad Delete|No ads.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('ads.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $ad = Ad::find($id);
            $ad->status = $status;

            if ($status == 1) {
                $ad->published_at = Carbon::now()->toDateTimeString();
            }
            $ad->authorized_by = auth()->user()->id;

            $result = $ad->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($ad)->withProperties($ad)->log('SUCCESS|APPROVE|Ad Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed fail",
                    "data" => null,

                ];
                activity()->performedOn($ad)->withProperties($ad)->log('ERROR|APPROVE|Ad Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Ad Approve|No ads.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('ads.list')) {
            if (auth()->user()->hasRole('admin')) {
                $ads = Ad::get();
            } else {
                $ads = Ad::where('created_by', auth()->user()->id)->get();
            }
        } else {
            return abort(403);
        }
        return Datatables::of($ads)
            ->editColumn('image', function ($ad) {
                return '<img width="50px" src="' . $ad->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($ad) {
                return $ad->createdUser != null ? $ad->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($ad) {
                return $ad->authorizedUser != null ? $ad->authorizedUser->email : "";
            })
            ->addColumn('action', function ($ad) {
                $html = '';

                if (auth()->user()->hasPermissionTo('ads.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $ad->id . ')">
                                <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('ads.update') || auth()->user()->hasPermissionTo('ads.update')) {
                    $html .= '<a href="' . route('ads.edit', $ad->id) . '">
                                <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                            </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('ads.delete') || auth()->user()->hasPermissionTo('ads.delete')) {
                    $html .= '<form method="post" action="' . route('ads.destroy', $ad->id) . '">
                                 <input type="hidden" name="_token" value="' . csrf_token() . '">
                                 <input type="hidden" name="_method" value="DELETE">
                                 <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()">
                                 <i class="fas fa-trash-alt"></i></button>
                            </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);
    }
}
