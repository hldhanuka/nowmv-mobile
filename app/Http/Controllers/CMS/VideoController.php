<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Video;
use Carbon\Carbon;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\VideoCreateRequest;
use App\Http\Requests\CMS\VideoUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('videos.list')) {
            $videos = Video::Latest()->paginate(10);
            return view('pages.videos.index', ["videos" => $videos]);
        } else {
            activity()->log('ERROR|VIEW|Video List|No videos.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('videos.create')) {
            activity()->log('SUCCESS|VIEW|Video Create'); // ACTIVITY LOG
            return view('pages.videos.create');
        } else {
            activity()->log('ERROR|VIEW|Video Create|No videos.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoCreateRequest $request)
    {
        $upload_video_file = CMSHelper::fileUpload("video_file", "uploads/video/");
        $upload_result = CMSHelper::fileUpload("video_thumbnail", "uploads/video/");
        $video = new Video();
        $video->title = $request->input("title");
        $video->url = $request->input("url");  // video URL
        if ($upload_video_file != null) {
            $video->url = $upload_video_file;    // video file upload URL
        }
        $video->video_thumbnail = $upload_result;
//        $video->video_thumbnail = $request->input('video_thumbnail_url');
        $video->status = 2;
        $video->created_by = auth()->user()->id;

        $result = $video->save();

        if ($result) {
            activity()->performedOn($video)->withProperties($video)->log('SUCCESS|CREATE|Video Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('videos.index');
        } else {
            activity()->performedOn($video)->withProperties($video)->log('ERROR|CREATE|Video Create|Fail'); // ACTIVITY LOG
            return redirect()->route('videos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('videos.update')) {
            $video = Video::find($id);
            activity()->performedOn($video)->withProperties($video)->log('SUCCESS|VIEW|Video Edit'); // ACTIVITY LOG
            return view('pages.videos.edit', ["video" => $video]);
        } else {
            activity()->log('ERROR|VIEW|Video Edit|No videos.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(VideoUpdateRequest $request, $id)
    {
        $upload_video_file = CMSHelper::fileUpload("video_file", "uploads/video/");
        $upload_result = CMSHelper::fileUpload("video_thumbnail", "uploads/video/");
        $video = Video::find($id);
        $video->title = $request->input("title");
        $video->url = $request->input("url");  // video URL
        if ($upload_video_file != null) {
            $video->url = $upload_video_file;    // video file upload URL
        }
        if ($upload_result != null) {
            $video->video_thumbnail = $upload_result;
        }
//        if ($request->input('video_thumbnail_url') != null) {
//            $video->video_thumbnail = $request->input('video_thumbnail_url');
//        }
        $video->status = 2;
        $video->created_by = auth()->user()->id;

        $result = $video->save();

        if ($result) {
            activity()->performedOn($video)->withProperties($video)->log('SUCCESS|UPDATE|Video Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('videos.index');
        } else {
            activity()->performedOn($video)->withProperties($video)->log('ERROR|UPDATE|Video Update|Fail'); // ACTIVITY LOG
            return redirect()->route('videos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('videos.delete')) {
            $video = Video::find($id);
            $result = $video->delete();
            if ($result) {
                activity()->performedOn($video)->withProperties($video)->log('SUCCESS|DELETE|Video Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('videos.index');
            } else {
                activity()->performedOn($video)->withProperties($video)->log('ERROR|DELETE|Video Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('videos.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Video Delete|No videos.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('videos.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $video = Video::find($id);
            $video->status = $status;
            if ($status == 1) {
                $video->published_at = Carbon::now()->toDateTimeString();
            }

            $video->authorized_by = auth()->user()->id;

            $result = $video->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($video)->withProperties($video)->log('SUCCESS|APPROVE|Video Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($video)->withProperties($video)->log('ERROR|APPROVE|Video Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('videos.list')) {
            if (auth()->user()->hasRole('admin')) {
                $videos = Video::get();
            } else {
                $videos = Video::where('created_by', auth()->user()->id)->get();
            }
        } else {
            return abort(403);
        }
        return Datatables::of($videos)
            ->editColumn('video_thumbnail', function ($video) {
                return '<img width="50px" src="' . $video->video_thumbnail . '" alt="">';
            })
            ->addColumn('created_user_email', function ($video) {
                return $video->createdUser != null ? $video->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($video) {
                return $video->authorizedUser != null ? $video->authorizedUser->email : "";
            })
            ->addColumn('action', function ($video) {
                $html = '';

                if (auth()->user()->hasPermissionTo('videos.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $video->id . ')">
                                <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('videos.update') || auth()->user()->hasPermissionTo('videos.update')) {
                    $html .= '<a href="' . route('videos.edit', $video->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('videos.delete') || auth()->user()->hasPermissionTo('videos.delete')) {
                    $html .= '<form method="post" action="' . route('videos.destroy', $video->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()">
                        <i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['video_thumbnail', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }

}
