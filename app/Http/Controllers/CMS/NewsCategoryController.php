<?php

namespace App\Http\Controllers\CMS;

use App\News;
use App\NewsCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\NewsCategoryCreateRequest;
use App\Http\Requests\CMS\NewsCategoryUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('news_categories.list')) {
            return view('pages.news_categories.index');
        } else {
            activity()->log('ERROR|VIEW|News Category List|No news_categories.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('news_categories.create')) {
            activity()->log('SUCCESS|VIEW|News Category Create'); // ACTIVITY LOG
            return view('pages.news_categories.create');
        } else {
            activity()->log('ERROR|VIEW|News Category Create|No news_categories.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCategoryCreateRequest $request)
    {
        $news_category = new NewsCategory();
        $news_category->title = $request->input("title");
        $news_category->status = 2;
        $news_category->created_by = auth()->user()->id;

        $result = $news_category->save();

        if ($result) {
            activity()->performedOn($news_category)->withProperties($news_category)->log('SUCCESS|CREATE|News Category Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('news_categories.index');
        } else {
            activity()->performedOn($news_category)->withProperties($news_category)->log('ERROR|CREATE|News Category Create|Fail'); // ACTIVITY LOG
            return redirect()->route('news_categories.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('news_categories.update')) {
            $news_category = NewsCategory::find($id);
            activity()->performedOn($news_category)->withProperties($news_category)->log('SUCCESS|VIEW|News Category Edit'); // ACTIVITY LOG
            return view('pages.news_categories.edit', ["news_category" => $news_category]);
        } else {
            activity()->log('ERROR|VIEW|News Category Edit|No news_categories.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsCategoryUpdateRequest $request, $id)
    {
        $news_category = NewsCategory::find($id);
        $news_category->title = $request->input("title");
        $news_category->status = 2;
        $news_category->created_by = auth()->user()->id;

        $result = $news_category->save();

        if ($result) {
            activity()->performedOn($news_category)->withProperties($news_category)->log('SUCCESS|UPDATE|News Category Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('news_categories.index');
        } else {
            activity()->performedOn($news_category)->withProperties($news_category)->log('ERROR|UPDATE|News Category Update|Fail'); // ACTIVITY LOG
            return redirect()->route('news_categories.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('news_categories.delete')) {
            $news_category = NewsCategory::find($id);
            $has_news = News::where("category_id", $id)->exists();
            if ($has_news) {
                activity()->performedOn($news_category)->withProperties($news_category)->log('ERROR|DELETE|News Category Delete|This news category already has news.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This news category already has news.');
                return redirect()->route('news_agencies.index');
            } else {
                $result = $news_category->delete();
                if ($result) {
                    activity()->performedOn($news_category)->withProperties($news_category)->log('SUCCESS|DELETE|News Category Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('news_categories.index');
                } else {
                    activity()->performedOn($news_category)->withProperties($news_category)->log('ERROR|DELETE|News Category Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('news_categories.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|News Category Delete|No news_categories.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('news_categories.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $news_category = NewsCategory::find($id);
            $news_category->status = $status;

            if ($status == 1) {
                $news_category->published_at = Carbon::now()->toDateTimeString();
            }
            $news_category->authorized_by = auth()->user()->id;

            $result = $news_category->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_category)->withProperties($news_category)->log('SUCCESS|APPROVE|News Category Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_category)->withProperties($news_category)->log('ERROR|APPROVE|News Category Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|News Category Approve|No news_categories.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $news_categories = NewsCategory::Latest()->get();

        return Datatables::of($news_categories)
            ->addColumn('created_user_email', function ($news_category) {
                return $news_category->createdUser != null ? $news_category->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($news_category) {
                return $news_category->authorizedUser != null ? $news_category->authorizedUser->email : "";
            })
            ->addColumn('action', function ($news_category) {

                $html = '';
                if (auth()->user()->hasPermissionTo('news_categories.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $news_category->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news_categories.update')) {
                    $html .= '<a href="' . route('news_categories.edit', $news_category->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news_categories.delete')) {
                    $html .= '<form method="post" action="' . route('news_categories.destroy', $news_category->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
