<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\SupportTeamCreateRequest;
use App\Http\Requests\CMS\SupportTeamUpdateRequest;
use App\SupportTeam;
use Yajra\DataTables\DataTables;

class SupportTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('support_teams.list')) {
            return view('pages.support_teams.index');
        } else {
            activity()->log('ERROR|VIEW|Support Team List|No support_teams.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionto('support_teams.create')) {
            activity()->log('SUCCESS|VIEW|Support Team Create'); // ACTIVITY LOG
            return view('pages.support_teams.create');
        } else {
            activity()->log('ERROR|VIEW|Support Team Create|No support_teams.create permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupportTeamCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/support_teams/");
        $support_team = new SupportTeam();
        $support_team->name = $request->input("name");
        $support_team->email = $request->input("email");
        $support_team->phone = $request->input("phone");
        $support_team->location = $request->input("location");
        $support_team->image = $upload_result;
//        $support_team->image = $request->input("image_url");
        $support_team->status = 1;
        $result = $support_team->save();

        if ($result) {
            activity()->performedOn($support_team)->withProperties($support_team)->log('SUCCESS|CREATE|Support Team Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('support_teams.index');
        } else {
            activity()->performedOn($support_team)->withProperties($support_team)->log('ERROR|CREATE|Support Team Create|Fail'); // ACTIVITY LOG
            return redirect()->route('support_teams.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('support_teams.update')) {
            $support_team = SupportTeam::find($id);
            activity()->performedOn($support_team)->withProperties($support_team)->log('SUCCESS|VIEW|Support Team Edit'); // ACTIVITY LOG
            return view('pages.support_teams.edit', ["support_team" => $support_team]);
        } else {
            activity()->log('ERROR|VIEW|Badge Edit|No support_teams.update permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SupportTeamUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/support_teams/");
        $support_team = SupportTeam::find($id);
        $support_team->name = $request->input("name");
        $support_team->email = $request->input("email");
        $support_team->phone = $request->input("phone");
        $support_team->location = $request->input("location");
        if ($upload_result != null) {
            $support_team->image = $upload_result;
        }
//        if ($request->input("image_url") != null) {
//            $support_team->image = $request->input("image_url");
//        }
        $support_team->status = 1;
        $result = $support_team->save();

        if ($result) {
            activity()->performedOn($support_team)->withProperties($support_team)->log('SUCCESS|UPDATE|Support Team Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('support_teams.index');
        } else {
            activity()->performedOn($support_team)->withProperties($support_team)->log('ERROR|UPDATE|Support Team Update|Fail'); // ACTIVITY LOG
            return redirect()->route('support_teams.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('support_teams.delete')) {
            $support_team = SupportTeam::find($id);
            $result = $support_team->delete();

            if ($result) {
                activity()->performedOn($support_team)->withProperties($support_team)->log('SUCCESS|DELETE|Support Team Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('support_teams.index');
            } else {
                activity()->performedOn($support_team)->withProperties($support_team)->log('ERROR|DELETE|Support Team Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('support_teams.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Support Team Delete|No support_teams.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $support_teams = SupportTeam::Latest()->get();

        return Datatables::of($support_teams)
            ->editColumn('image', function ($support_team) {
                return '<img width="50px" src="' . $support_team->image . '" alt="">';
            })
            ->addColumn('action', function ($support_team) {
                $html = '';

                if (auth()->user()->hasPermissionTo('support_teams.update')) {
                    $html .= '<a href="' . route('support_teams.edit', $support_team->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('support_teams.delete')) {
                    $html .= '<form method="post" action="' . route('support_teams.destroy', $support_team->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['image', 'action'])
            ->make(true);

    }
}
