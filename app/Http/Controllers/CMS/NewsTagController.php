<?php

namespace App\Http\Controllers\CMS;

use App\Http\Requests\CMS\NewsTagCreateRequest;
use App\Http\Requests\CMS\NewsTagUpdateRequest;
use App\News;
use App\NewsTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class NewsTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('news_tags.list')) {
            $news_tags = NewsTag::Latest()->paginate(10);
            return view('pages.news_tags.index', ["news_tags" => $news_tags]);
        } else {
            activity()->log('ERROR|VIEW|News Tag List|No news_tags.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('news_tags.create')) {
            activity()->log('SUCCESS|VIEW|News Tag Create'); // ACTIVITY LOG
            return view('pages.news_tags.create');
        } else {
            activity()->log('ERROR|VIEW|News Tag Create|No news_tags.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsTagCreateRequest $request)
    {
        $news_tag = new NewsTag();
        $news_tag->title = $request->input("title");
        $news_tag->status = 2;
        $news_tag->created_by = auth()->user()->id;

        $result = $news_tag->save();

        if ($result) {
            activity()->performedOn($news_tag)->withProperties($news_tag)->log('SUCCESS|CREATE|News Tag Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('news_tags.index');
        } else {
            activity()->performedOn($news_tag)->withProperties($news_tag)->log('ERROR|CREATE|News Tag Create|Fail'); // ACTIVITY LOG
            return redirect()->route('news_tags.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('news_tags.update')) {
            $news_tag = NewsTag::find($id);
            activity()->performedOn($news_tag)->withProperties($news_tag)->log('SUCCESS|VIEW|News Tag Edit'); // ACTIVITY LOG
            return view('pages.news_tags.edit', ["news_tag" => $news_tag]);
        } else {
            activity()->log('ERROR|VIEW|News Tag Edit|No news_tags.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsTagUpdateRequest $request, $id)
    {
        $news_tag = NewsTag::find($id);
        $news_tag->title = $request->input("title");
        $news_tag->status = 2;
        $news_tag->created_by = auth()->user()->id;

        $result = $news_tag->save();

        if ($result) {
            activity()->performedOn($news_tag)->withProperties($news_tag)->log('SUCCESS|UPDATE|News Tag Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Update');
            return redirect()->route('news_tags.index');
        } else {
            activity()->performedOn($news_tag)->withProperties($news_tag)->log('ERROR|UPDATE|News Tag Update|Fail'); // ACTIVITY LOG
            return redirect()->route('news_tags.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('news_tags.delete')) {
            $news_tag = NewsTag::find($id);
            $has_news = false;
            $all_news_tags = News::pluck("tags");
            foreach ($all_news_tags as $one_news_tag){
                if(in_array($id, json_decode($one_news_tag))){
                    $has_news = true; break;
                }
            }
            if ($has_news) {
                activity()->performedOn($news_tag)->withProperties($news_tag)->log('ERROR|DELETE|News Tag Delete|This news tag already has news.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This news tag already has news.');
                return redirect()->route('news_tags.index');
            } else {
                $result = $news_tag->delete();
                if ($result) {
                    activity()->performedOn($news_tag)->withProperties($news_tag)->log('SUCCESS|DELETE|News Tag Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('news_tags.index');
                } else {
                    activity()->performedOn($news_tag)->withProperties($news_tag)->log('ERROR|DELETE|News Tag Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('news_tags.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|News Tag Delete|No news_tags.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('news_tags.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $news_tag = NewsTag::find($id);
            $news_tag->status = $status;

            if ($status == 1) {
                $news_tag->published_at = Carbon::now()->toDateTimeString();
            }
            $news_tag->authorized_by = auth()->user()->id;

            $result = $news_tag->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_tag)->withProperties($news_tag)->log('SUCCESS|APPROVE|News Tag Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($news_tag)->withProperties($news_tag)->log('ERROR|APPROVE|News Tag Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|News Tag Approve|No news_tags.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $news_tags = NewsTag::Latest()->get();

        return Datatables::of($news_tags)
            ->addColumn('created_user_email', function ($news_tag) {
                return $news_tag->createdUser != null ? $news_tag->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($news_tag) {
                return $news_tag->authorizedUser != null ? $news_tag->authorizedUser->email : "";
            })
            ->addColumn('action', function ($news_tag) {

                $html = '';

                if (auth()->user()->hasPermissionTo('news_tags.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $news_tag->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news_tags.update')) {
                    $html .= '<a href="' . route('news_tags.edit', $news_tag->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('news_tags.delete')) {
                    $html .= '<form method="post" action="' . route('news_tags.destroy', $news_tag->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
