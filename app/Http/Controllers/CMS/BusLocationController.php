<?php

namespace App\Http\Controllers\CMS;

use App\BusHaltSchedule;
use App\BusLocation;
use App\BusRoute;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\BusLocationCreateRequest;
use App\Http\Requests\CMS\BusLocationUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class BusLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('bus_locations.list')) {
            return view('pages.bus_locations.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('bus_locations.create')) {
            return view('pages.bus_locations.create');
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusLocationCreateRequest $request)
    {
        $bus_location = new BusLocation();
        $bus_location->name = $request->input("name");
        $bus_location->status = $request->input("status");
        $bus_location->type = $request->input("type");
        $result = $bus_location->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('bus_locations.index');
        } else {
            return redirect()->route('bus_locations.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('bus_locations.update')) {
            $bus_location = BusLocation::find($id);
            return view('pages.bus_locations.edit', ["bus_location" => $bus_location]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BusLocationUpdateRequest $request, $id)
    {
        $bus_location = BusLocation::find($id);
        $bus_location->name = $request->input("name");
        $bus_location->status = $request->input("status");
        $bus_location->type = $request->input("type");
        $result = $bus_location->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Update');
            return redirect()->route('bus_locations.index');
        } else {
            return redirect()->route('bus_locations.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('bus_locations.delete')) {
            $bus_location = BusLocation::find($id);
            $result = $bus_location->delete();

            $has_bus_halt_schedule = BusHaltSchedule::where("start_location_id", $id)->orWhere("halt_id", $id)->exists();
            $has_bus_route = BusRoute::where("from", $id)->orWhere("to", $id)->exists();
            if($has_bus_halt_schedule){
                connectify('error', 'Error', 'This location already use in some halt schedules');
                return redirect()->route('bus_locations.index');
            } else if ($has_bus_route) {
                connectify('error', 'Error', 'This location already use in some routes');
                return redirect()->route('bus_locations.index');
            } else {
                if ($result) {
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('bus_locations.index');
                } else {
                    return redirect()->route('bus_locations.index');
                }
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $bus_locations = BusLocation::Latest()->get();

        return Datatables::of($bus_locations)
            ->addColumn('action', function ($bus_location) {
                return '
                         <a href="' . route('bus_locations.edit', $bus_location->id) . '">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a>
                         <br>
                         <br>
                         <form method="post" action="' . route('bus_locations.destroy', $bus_location->id) . '">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['action'])
            ->make(true);

    }
}
