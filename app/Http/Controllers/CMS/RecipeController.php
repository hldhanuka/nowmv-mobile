<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Recipe;
use App\RecipeImage;
use App\RecipeRating;
use App\RecipeReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\RecipeCreateRequest;
use App\Http\Requests\CMS\RecipeUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('recipes.list')) {
            $recipes = Recipe::Latest()->paginate(10);
            return view('pages.recipes.index', ["recipes" => $recipes]);
        } else {
            activity()->log('ERROR|VIEW|Recipe List|No recipes.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('recipes.create')) {
            activity()->log('SUCCESS|VIEW|Recipe Create'); // ACTIVITY LOG
            return view('pages.recipes.create');
        } else {
            activity()->log('ERROR|VIEW|Recipe Create|No recipes.create permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeCreateRequest $request)
    {
        $upload_image = CMSHelper::fileUpload("image", "uploads/recipes/");
        $upload_video_file = CMSHelper::fileUpload("video_file", "uploads/recipes/");
        $recipe = new Recipe();
        $recipe->title = $request->input("title");
        $recipe->image = $upload_image;
//        $recipe->image = $request->input("image_url");
        $recipe->link = $request->input("link");
        $recipe->video = $request->input("video");  // video URL
        if ($upload_video_file != null) {
            $recipe->video = $upload_video_file;    // video file upload URL
        }
        $recipe->serving = $request->input("serving");
        $recipe->prepare_time = $request->input("prepare_time");
        $recipe->cook_time = $request->input("cook_time");
        $recipe->ingredients = json_encode($request->input("ingredients"));
        $recipe->directions = json_encode($request->input("directions"));
        $recipe->content = $request->input("content");
        $recipe->status = 2;
        $recipe->created_by = auth()->user()->id;

        $result = $recipe->save();

        if ($result) {
            activity()->performedOn($recipe)->withProperties($recipe)->log('SUCCESS|CREATE|Recipe Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('recipes.index');
        } else {
            activity()->performedOn($recipe)->withProperties($recipe)->log('ERROR|CREATE|Recipe Create|Fail'); // ACTIVITY LOG
            return redirect()->route('recipes.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('recipes.update')) {
            $recipe = Recipe::find($id);
            if (auth()->user()->hasRole("admin")) {
                activity()->performedOn($recipe)->withProperties($recipe)->log('SUCCESS|VIEW|Recipe Edit'); // ACTIVITY LOG
                return view('pages.recipes.edit', ["recipe" => $recipe]);
            } else if (auth()->user()->hasRole("merchant") || auth()->user()->hasRole('custom') && $recipe->created_by == auth()->user()->id) {
                activity()->performedOn($recipe)->withProperties($recipe)->log('SUCCESS|VIEW|Recipe Edit'); // ACTIVITY LOG
                return view('pages.recipes.edit', ["recipe" => $recipe]);
            } else {
                activity()->log('ERROR|VIEW|Recipe Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
        } else {
            activity()->log('ERROR|VIEW|Recipe Edit|No recipes.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeUpdateRequest $request, $id)
    {
        $upload_image = CMSHelper::fileUpload("image", "uploads/recipes/");
        $upload_video_file = CMSHelper::fileUpload("video_file", "uploads/recipes/");
        $recipe = Recipe::find($id);
        $recipe->title = $request->input("title");
        if ($upload_image != null) {
            $recipe->image = $upload_image;
        }
//        if ($request->input("image_url") != null) {
//            $recipe->image = $request->input("image_url");
//        }
        $recipe->link = $request->input("link");
        $recipe->video = $request->input("video");
        if ($upload_video_file != null) {
            $recipe->video = $upload_video_file;
        }
        $recipe->serving = $request->input("serving");
        $recipe->prepare_time = $request->input("prepare_time");
        $recipe->cook_time = $request->input("cook_time");
        $recipe->ingredients = json_encode($request->input("ingredients"));
        $recipe->directions = json_encode($request->input("directions"));
        $recipe->content = $request->input("content");
        $recipe->status = 2;

        $result = $recipe->save();

        if ($result) {
            activity()->performedOn($recipe)->withProperties($recipe)->log('SUCCESS|UPDATE|Recipe Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('recipes.index');
        } else {
            activity()->performedOn($recipe)->withProperties($recipe)->log('ERROR|UPDATE|Recipe Update|Fail'); // ACTIVITY LOG
            return redirect()->route('recipes.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('recipes.delete')) {
            $recipe = Recipe::find($id);
            RecipeRating::where("recipe_id", $id)->delete();
            RecipeReview::where("recipe_id", $id)->delete();
            RecipeImage::where("recipe_id", $id)->delete();
            $result = $recipe->delete();
            if ($result) {
                activity()->performedOn($recipe)->withProperties($recipe)->log('SUCCESS|DELETE|Recipe Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('recipes.index');
            } else {
                activity()->performedOn($recipe)->withProperties($recipe)->log('ERROR|DELETE|Recipe Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('recipes.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Recipe Delete|No recipes.delete permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('recipes.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $recipe = Recipe::find($id);
            $recipe->status = $status;

            if ($status == 1) {
                $recipe->published_at = Carbon::now()->toDateTimeString();
            }
            $recipe->authorized_by = auth()->user()->id;

            $result = $recipe->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($recipe)->withProperties($recipe)->log('SUCCESS|APPROVE|Recipe Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($recipe)->withProperties($recipe)->log('ERROR|APPROVE|Recipe Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Recipe Approve|No recipes.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('recipes.list')) {
            if (auth()->user()->hasRole('admin')) {
                $recipes = Recipe::Latest()->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $recipes = Recipe::where("created_by", auth()->user()->id)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($recipes)
            ->editColumn('image', function ($recipe) {
                return '<img width="50px" src="' . $recipe->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($recipe) {
                return $recipe->createdUser != null ? $recipe->createdUser->email : "";
            })
            ->addColumn('action', function ($recipe) {
                $html = '';
                if (auth()->user()->hasRole("admin") || auth()->user()->hasPermissionTo('recipes.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $recipe->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }
                $html .= '<a href="' . route('recipes.edit', $recipe->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>
                        <form method="post" action="' . route('recipes.destroy', $recipe->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'action'])
            ->make(true);

    }
}
