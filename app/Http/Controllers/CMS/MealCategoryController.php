<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Meal;
use App\MealCategory;
use App\MerchantUser;
use App\Restaurant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\MealCategoryCreateRequest;
use App\Http\Requests\CMS\MealCategoryUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class MealCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('meal_categories.list')) {
            return view('pages.meal_categories.index');
        } else {
            activity()->log('ERROR|VIEW|Meal Category List|No meal_categories.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('meal_categories.create')) {
            $restaurants = Restaurant::active()->select("id", "title", "status")->orderBy('title')->get();
            activity()->log('SUCCESS|VIEW|Meal Category Create'); // ACTIVITY LOG
            return view('pages.meal_categories.create', ["restaurants" => $restaurants]);
        } else {
            activity()->log('ERROR|VIEW|Meal Category Create|No meal_categories.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MealCategoryCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/meal_categories/");

        $meal_category = new MealCategory();
        $meal_category->title = $request->input("title");
        $meal_category->restaurant_id = $request->input("restaurant_id");
        $meal_category->image = $upload_result;
//        $meal_category->image = $request->input("image_url");
        $meal_category->status = 2;
        $meal_category->created_by = auth()->user()->id;

        if ($meal_category->status == 1) {
            $meal_category->published_at = Carbon::now()->toDateTimeString();
        }
        if ($meal_category->status == 1) {
            $meal_category->authorized_by = auth()->user()->id;
        }
        $restaurant = Restaurant::find($request->input("restaurant_id"));
        $meal_category->merchant_id = $restaurant != null ? $restaurant->merchant_id : null;
        $result = $meal_category->save();

        if ($result) {
            activity()->performedOn($meal_category)->withProperties($meal_category)->log('SUCCESS|CREATE|Meal Category Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('meal_categories.index');
        } else {
            activity()->performedOn($meal_category)->withProperties($meal_category)->log('ERROR|CREATE|Meal Category Create|Fail'); // ACTIVITY LOG
            return redirect()->route('meal_categories.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('meal_categories.update')) {
            $meal_category = MealCategory::find($id);
            $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
            if (auth()->user()->hasRole("admin") || in_array($meal_category->merchant_id, $merchant_ids)) {
                $restaurants = Restaurant::active()->select("id", "title", "status")->orderBy('title')->get();
                activity()->performedOn($meal_category)->withProperties($meal_category)->log('SUCCESS|VIEW|Meal Category Edit'); // ACTIVITY LOG
                return view('pages.meal_categories.edit', ["meal_category" => $meal_category, "restaurants" => $restaurants]);
            } else {
                activity()->log('ERROR|VIEW|Meal Category Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
        } else {
            activity()->log('ERROR|VIEW|Meal Category Edit|No meal_categories.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MealCategoryUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/meal_categories/");

        $meal_category = MealCategory::find($id);
        $meal_category->title = $request->input("title");
        $meal_category->restaurant_id = $request->input("restaurant_id");
        if ($upload_result != null) {
            $meal_category->image = $upload_result;
        }
//        if ($request->input("image_url") != null) {
//            $meal_category->image = $request->input("image_url");
//        }
        $meal_category->status = 2;
        $meal_category->created_by = auth()->user()->id;

        if ($meal_category->status == 1) {
            $meal_category->published_at = Carbon::now()->toDateTimeString();
        }
        if ($meal_category->status == 1) {
            $meal_category->authorized_by = auth()->user()->id;
        }

        $restaurant = Restaurant::find($request->input("restaurant_id"));
        $meal_category->merchant_id = $restaurant != null ? $restaurant->merchant_id : null;

        $result = $meal_category->save();

        if ($result) {
            activity()->performedOn($meal_category)->withProperties($meal_category)->log('SUCCESS|UPDATE|Meal Category Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('meal_categories.index');
        } else {
            activity()->performedOn($meal_category)->withProperties($meal_category)->log('ERROR|UPDATE|Meal Category Update|Fail'); // ACTIVITY LOG
            return redirect()->route('meal_categories.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('meal_categories.delete')) {
            $meal_category = MealCategory::find($id);
            $has_meal = Meal::where("category_id", $id)->exists();
            if ($has_meal) {
                activity()->performedOn($meal_category)->withProperties($meal_category)->log('ERROR|DELETE|Meal Category Delete|This meal category already has meals.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This meal category already has meals.');
                return redirect()->route('meal_categories.index');
            } else {
                $result = $meal_category->delete();
                if ($result) {
                    activity()->performedOn($meal_category)->withProperties($meal_category)->log('SUCCESS|DELETE|Meal Category Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully deleted the category');
                    return redirect()->route('meal_categories.index');
                } else {
                    activity()->performedOn($meal_category)->withProperties($meal_category)->log('ERROR|DELETE|Meal Category Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('meal_categories.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|Meal Category Delete|No meal_categories.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('meal_categories.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $meal_category = MealCategory::find($id);
            $meal_category->status = $status;

            if ($status == 1) {
                $meal_category->published_at = Carbon::now()->toDateTimeString();
            }
            $meal_category->authorized_by = auth()->user()->id;

            $result = $meal_category->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($meal_category)->withProperties($meal_category)->log('SUCCESS|APPROVE|Meal Category Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($meal_category)->withProperties($meal_category)->log('ERROR|APPROVE|Meal Category Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Meal Category Approve|No meal_categories.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('meal_categories.list')) {
            if (auth()->user()->hasRole('admin')) {
                $meal_categories = MealCategory::with(['restaurant', 'merchant'])->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
                $meal_categories = MealCategory::with(['restaurant', 'merchant'])->whereIn('merchant_id', $merchant_ids)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }

        return Datatables::of($meal_categories)
            ->editColumn('image', function ($meal_category) {
                return '<img width="50px" src="' . $meal_category->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($meal_category) {
                return ($meal_category->createdUser != null) ? $meal_category->createdUser->email : "-";
            })
            ->addColumn('merchant_id', function ($meal_category) {
                return ($meal_category->merchant != null) ? $meal_category->merchant->name : "-";
            })
            ->addColumn('restaurant_id', function ($meal_category) {
                return ($meal_category->restaurant != null) ? $meal_category->restaurant->title : "-";
            })
            ->addColumn('action', function ($meal_category) {

                $html = '';

                if (auth()->user()->hasPermissionTo('meal_categories.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $meal_category->id . ')">
                            <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('meal_categories.update')) {
                    $html .= '<a href="' . route('meal_categories.edit', $meal_category->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('meal_categories.delete')) {
                    $html .= '<form method="post" action="' . route('meal_categories.destroy', $meal_category->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'merchant_id', 'restaurant_id', 'action'])
            ->make(true);
    }

    public function loadMealCategoriesByRestaurantId($restaurant_id)
    {
        $meal_categories = MealCategory::where("restaurant_id", $restaurant_id)->get();
        return response()->json($meal_categories);
    }
}
