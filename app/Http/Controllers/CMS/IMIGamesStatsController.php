<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\APIHelper;
use App\Helpers\ImiGameHelper;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpOffice\PhpSpreadsheet\Settings;

class IMIGamesStatsController extends Controller
{
    public function generateAccessToken(){

        $headers = ['Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';
        $url =  config('app.imi_games_stats_token_generate_host')."/auth/realms/NowMv/protocol/openid-connect/token";

        $data= array();
        $data["client_id"]="nowmv-dashboard";
        $data["client_secret"]="5bf8102e-47d1-4f9e-b6a8-cbb7efe7dc3a";
        $data["grant_type"]="client_credentials";

        $response = ImiGameHelper::sendAPICall('POST', $url, $data,$headers,$guzzle_body_type);

        $access_token = $response["body"]["access_token"];

        Setting::where('title','imi_games_stats_token')
            ->update(["text"=>$access_token]);

        return $access_token;
    }

    public function getAccessToken(){

        $settings = Setting::firstOrCreate(['title'=>'imi_games_stats_token']);

        if ($settings->text!=null){

            $token_valid_datetime = Carbon::parse($settings->updated_at)->addMinutes(4)->toDateTimeString();

            if(!($token_valid_datetime >= Carbon::now()->toDateTimeString())){
                $token=$this->generateAccessToken();
            }else{
                $token =  $settings->text;
            }
        }else{
            $token =  $this->generateAccessToken();
        }

       return $token;

    }

    public function games(Request $request){

        $token = $this->getAccessToken();
        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';
        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/games/promo";
        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);
        $games = $response["body"]["data"];

        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/subscription/summary?package_type=DHIRAAGU_NORMAL&page=1&size=10";
        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);
        $subscription_data = $response["body"]["data"];

        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/subscription/summary?package_type=DHIRAAGU_NORMAL&page=1&size=10";
        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);
        $subscription_data = $response["body"]["data"];

        return view('pages/imi-games/promo_games')->with(["games"=>$games,"subscription_data"=>$subscription_data]);
    }

    public function getGameByGameUUID(Request $request){

        /*$uuid = $request->uuid;

        if ($uuid==null){
            return redirect()->back();
        }

        $token = $this->getAccessToken();
        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';
        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/subscription?package_type=DHIRAAGU_NORMAL&data_type=Daily&start_date=2020-01-01 00:00:00&end_date=".Carbon::now()->toDateTimeString()."&game_uuid=".$uuid."";
        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);

        $subscription_data_daily = $response["body"]["data"];

        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/unsubscription?package_type=DHIRAAGU_NORMAL&data_type=Daily&start_date=2020-01-01 00:00:00&end_date=".Carbon::now()->toDateTimeString()."&game_uuid=".$uuid."";
        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);


        $unsubscription_data_daily = $response["body"]["data"];


        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/revenue?package_type=DHIRAAGU_NORMAL&data_type=Daily&start_date=2020-01-01 00:00:00&end_date=".Carbon::now()->toDateTimeString()."&game_uuid=".$uuid."";
        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);


        $revenue_data_daily = $response["body"]["data"];*/


        $data = $request->input();


        return view('pages/imi-games/game_details')->with($data);
    }

    public function gameSubscriptionIndex(Request $request){
        $subscription_data_daily = array();

        if (($request->input("start_date") != null) && $request->input("end_date")){
            $start_date = Carbon::parse($request->input("start_date"))->toDateString();
            $end_date = Carbon::parse($request->input("end_date"))->toDateString();

            $uuid= $request->input('uuid');
            $token = $this->getAccessToken();
            $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
            $guzzle_body_type = 'form_params';

            $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/subscription?package_type=DHIRAAGU_NORMAL&data_type=Daily&start_date=".$start_date." 00:00:00&end_date=".$end_date." 23:59:59&game_uuid=".$uuid."";

            $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);

            $subscription_data_daily = $response["body"]["data"];
        }

        $data = $request->input();

        $data["subscription_data_daily"] = $subscription_data_daily;

        return view('pages/imi-games/subscription_data')->with($data);
    }

    public function gameUnsubscriptionIndex(Request $request){
        $unsubscription_data_daily = array();

        if (($request->input("start_date") != null) && $request->input("end_date")){
            $start_date = Carbon::parse($request->input("start_date"))->toDateString();
            $end_date = Carbon::parse($request->input("end_date"))->toDateString();

            $uuid= $request->input('uuid');
            $token = $this->getAccessToken();
            $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
            $guzzle_body_type = 'form_params';

            $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/unsubscription?package_type=DHIRAAGU_NORMAL&data_type=Daily&start_date=".$start_date." 00:00:00&end_date=".$end_date." 23:59:59&game_uuid=".$uuid."";

            $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);

            $unsubscription_data_daily = $response["body"]["data"];
        }

        $data = $request->input();

        $data["unsubscription_data_daily"] = $unsubscription_data_daily;

        return view('pages/imi-games/unsubscription_data')->with($data);
    }

    public function gameRevenueIndex(Request $request){
        $revenue_data_daily = array();

        if ($request->input("start_date") != null && $request->input("end_date")){
            $start_date = Carbon::parse($request->input("start_date"))->toDateString();
            $end_date = Carbon::parse($request->input("end_date"))->toDateString();

            $uuid= $request->input('uuid');
            $token = $this->getAccessToken();
            $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
            $guzzle_body_type = 'form_params';

            $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/revenue?package_type=DHIRAAGU_NORMAL&data_type=Daily&start_date=".$start_date." 00:00:00&end_date=".$end_date." 23:59:59&game_uuid=".$uuid."";

            $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);

            $revenue_data_daily = $response["body"]["data"];
        }

        $data = $request->input();

        $data["revenue_data_daily"] = $revenue_data_daily;

        return view('pages/imi-games/revenue_data')->with($data);
    }

    public function gameLeaderBoardIndex(Request $request){
        $token = $this->getAccessToken();
        $leader_board_created = false;
        $uuid= $request->input('uuid');
        $page= ($request->input('page')!=null)?$request->input('page'):0;
        $request_record_count= 5;
        $reward_leaderboard_api_error = '';
        $is_reward_leaderboard_api_called = false;

        if (($request->input("start_date") != null) && $request->input("end_date")){
            $is_reward_leaderboard_api_called = true;

            $start_date = Carbon::parse($request->input("start_date"))->toDateString()." 00:00:00";
            $end_date = Carbon::parse($request->input("end_date"))->toDateString()." 23:59:59";

            //convert date to the relavent time zone
            //$start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date)->toAtomString();
            //$end_date = Carbon::createFromFormat('Y-m-d H:i:s', $end_date)->toAtomString();

            // ------------- TEST -------------
            //$start_date = '2021-10-20T19:00:00.000+0000';
            //$end_date = '2021-11-30T18:59:59.000+0000';
            // ------------- TEST -------------

            $data= array();
            $data["game_uuid"] = $uuid;
            $data["start_date"] = $start_date;
            $data["end_date"] = $end_date;

            $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/json'];

            $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/reward/leaderboard";

            $response = ImiGameHelper::sendAPICall('POST', $url, $data, $headers);

            if (($response["status_code"] != 200) && (isset($response['body']['errorMessage']))){
                $reward_leaderboard_api_error = $response['body']['errorMessage'];
            }

            if ($response["status_code"] == 201){
                $leader_board_created = true;
            }
        }

        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';

        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/leaderboard/reward/search?game_uuid=".$uuid."&page=".$page."&size=".$request_record_count."";

        $response = ImiGameHelper::sendAPICall('GET', $url, null, $headers, $guzzle_body_type);

        $leader_boards = $response["body"]["data"];

        $is_last_page = (count($leader_boards) < $request_record_count);

        $data = array();
        $data = $request->input();

        $data["leader_board_created"] = $leader_board_created;
        $data["leader_boards"] = $leader_boards;
        $data["current_page"] = $page;
        $data["is_last_page"] = $is_last_page;
        $data["total_records"] = count($leader_boards);
        $data["reward_leaderboard_api_error"] = $reward_leaderboard_api_error;
        $data["is_reward_leaderboard_api_called"] = $is_reward_leaderboard_api_called;

        return view('pages/imi-games/leader_board')->with($data);
    }

    public function getWinnersData(Request $request){
        $uuid= $request->input('uuid');

        $token = $this->getAccessToken();

        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';

        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/leaderboard/reward/".$uuid."";

        $response = ImiGameHelper::sendAPICall('GET', $url, null,$headers,$guzzle_body_type);

        $winners_data = $response["body"]["data"]["leaderboard_data"];

        return json_encode($winners_data);
    }

    public function gameWinnerSendPackage(Request $request){
        $uuid= $request->input('uuid');
        $msisdn= $request->input('msisdn');

        // NOTE :- can send only eligible packs
        // NOTE :- can send only whetever the packs come from the API (gameWinnerSendPackageSubmit)
        // --- TEST ------
        //$msisdn = '7327559';
        // --- TEST ------

        $token = $this->getAccessToken();

        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';

        //$url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/eligible/bundles?package_type=DHIRAAGU_NORMAL&game_uuid=".$uuid."&msisdn=".$msisdn."";
        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/eligible/bundles/v2?package_type=DHIRAAGU_NORMAL&game_uuid=".$uuid."&msisdn=".$msisdn."";

        $response = ImiGameHelper::sendAPICall('GET', $url, null, $headers, $guzzle_body_type);

        $eligible_bundle_list = $response["body"]["data"]["eligible_bundle_list"];
        $offer_name = $response["body"]["data"]["offer_name"];

        $data = $request->input();
        $data["eligible_bundle_list"] = $eligible_bundle_list;
        $data["offer_name"] = $offer_name;

        return view('pages/imi-games/winner_package_select')->with($data);
    }

    public function gameWinnerSendPackageSubmit(Request $request){
        $game_uuid = $request->input('game_uuid');
        $offer_name = $request->input('offer_name');
        $package_name = $request->input('package_name');
        $msisdn = $request->input('msisdn');

        // --- TEST ------
        //$msisdn = '7327559';
        // --- TEST ------

        $token = $this->getAccessToken();

        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];
        $guzzle_body_type = 'form_params';

        $url = config('app.imi_games_stats_host')."/api/v1/admin/dashboard/provision/addon?package_type=DHIRAAGU_NORMAL&game_uuid=".$game_uuid."&msisdn=".$msisdn."&offer_name=".$offer_name."&bundle_system_name=".$package_name."";

        $response = ImiGameHelper::sendAPICall('POST', $url, null, $headers, $guzzle_body_type);

        if ($response["status_code"] == 200){
            return redirect()->route("imi_games.reward-list");
         } else {
            return redirect()->back();
        }
    }

    public function getRewardList(Request $request){
        $token = $this->getAccessToken();

        $page = ($request->input('page') != null) ? $request->input('page') : 0;
        $request_record_count = 5;

        $headers = ['Authorization'=>'Bearer '.$token,'Content-Type' => 'application/x-www-form-urlencoded'];

        $guzzle_body_type = 'form_params';
        $url =  config('app.imi_games_stats_host')."/api/v1/admin/dashboard/reward/history?package_type=DHIRAAGU_NORMAL&page=".$page."&size=".$request_record_count;

        $response = ImiGameHelper::sendAPICall('GET', $url, null, $headers, $guzzle_body_type);

        $rewards_data = $response["body"]["data"]["dataRows"];

        $is_last_page = (count($rewards_data) < $request_record_count);

        $data = $request->input();

        $data["rewards_data"] = $rewards_data;
        $data["current_page"] = $page;
        $data["is_last_page"] = $is_last_page;
        $data["total_records"] = count($rewards_data);

        return view('pages/imi-games/reward_list')->with($data);
    }
}
