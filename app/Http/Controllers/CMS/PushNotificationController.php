<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\OneSignalPush;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\PushNotificationRequest;

class PushNotificationController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasPermissionTo('push_notifications.create')) {

            return view('pages.push_notifications.index');
        } else {
            return abort(403);
        }
    }

    public function update(PushNotificationRequest $request)
    {
        try {
            $message = $request->input("message");
            $type = $request->input("type");
            $redirect_type = $request->input("redirect_type");
            $id = $request->input("id");
            $data = ["type" => $redirect_type, "id" => $id != null ? (string)$id : $id];

            if ($redirect_type == null) {
                $data = null;
            }

            OneSignalPush::sendPushNotification($message, $type, $data, 0, null, null);

            activity()->log('SUCCESS|UPDATE|Push Notification Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Send');
            return redirect()->route('push_notifications.index');
        } catch (\Exception $e) {
            report($e);
            activity()->log('ERROR|UPDATE|Push Notification Update|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Error on Send');
            return redirect()->route('push_notifications.index');
        }
    }
}
