<?php

namespace App\Http\Controllers\CMS;

use App\Http\Requests\CMS\ZakatDownloadCSVRequest;
use App\ZakatPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Exports\ZakatExport;
use Maatwebsite\Excel\Facades\Excel;

class ZakatPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('zakat_payments.list')) {
            return view('pages.zakat_payments.index');
        } else {
            activity()->log('ERROR|VIEW|Zakat Payment List|No zakat_payments.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $zakat_payments = ZakatPayment::with('island')->get();

        return Datatables::of($zakat_payments)
            ->addColumn('island_id', function ($zakat_payment) {
                return $zakat_payment->island->name;
            })
            ->rawColumns(['island_id'])
            ->make(true);

    }

    public function generateCsv()
    {
        return view('pages.zakat_payments.report');
    }

    public function export(ZakatDownloadCSVRequest $request)
    {
        try {
            $from_date = $request->input("from_date");
            $to_date = $request->input("to_date");
            $from = Carbon::parse($from_date)->startOfDay()->toDateTimeString();
            $to = Carbon::parse($to_date)->endOfDay()->toDateTimeString();
            activity()->log('SUCCESS|EXPORT|Zakat Report|from=' . $from . '|to=' . $to); // ACTIVITY LOG
            return Excel::download(new ZakatExport($from, $to), 'Zakat Payment Report ' . $from_date . ' - ' . $to_date . '.csv');
        } catch (\Exception $e){
            activity()->log('CRITICAL|EXPORT|Zakat Report|from=' . $request->input("from_date") . '|to=' . $request->input("to_date")); // ACTIVITY LOG
            report($e);
            return abort(500);
        }
    }
}
