<?php

namespace App\Http\Controllers\CMS;

use App\FerryLocation;
use App\FerryRoute;
use App\FerryRouteSchedule;
use App\Http\Requests\CMS\FerryLocationCreateRequest;
use App\Http\Requests\CMS\FerryLocationUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class FerryLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ferry_locations.list')) {
            return view('pages.ferry_locations.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ferry_locations.create')) {
            return view('pages.ferry_locations.create');
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FerryLocationCreateRequest $request)
    {
        $ferry_location = new FerryLocation();
        $ferry_location->name = $request->input("name");
        $ferry_location->status = $request->input("status");
        $ferry_location->type = $request->input("type");
        $result = $ferry_location->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ferry_locations.index');
        } else {
            return redirect()->route('ferry_locations.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_locations.update')) {
            $ferry_location = FerryLocation::find($id);
            return view('pages.ferry_locations.edit', ["ferry_location" => $ferry_location]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FerryLocationUpdateRequest $request, $id)
    {
        $ferry_location = FerryLocation::find($id);
        $ferry_location->name = $request->input("name");
        $ferry_location->status = $request->input("status");
        $ferry_location->type = $request->input("type");
        $result = $ferry_location->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Update');
            return redirect()->route('ferry_locations.index');
        } else {
            return redirect()->route('ferry_locations.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_locations.delete')) {
            $ferry_location = FerryLocation::find($id);

            $has_ferry_terminal_schedule = FerryRouteSchedule::where("start_location_id", $id)->orWhere("terminal_id", $id)->exists();
            $has_ferry_route = FerryRoute::where("from", $id)->orWhere("to", $id)->exists();
            if ($has_ferry_terminal_schedule) {
                connectify('error', 'Error', 'This location already use in some schedules');
                return redirect()->route('ferry_locations.index');
            } else if ($has_ferry_route) {
                connectify('error', 'Error', 'This location already use in some routes');
                return redirect()->route('ferry_locations.index');
            } else {
                $result = $ferry_location->delete();
                if ($result) {
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('ferry_locations.index');
                } else {
                    return redirect()->route('ferry_locations.index');
                }
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $ferry_locations = FerryLocation::Latest()->get();

        return Datatables::of($ferry_locations)
            ->addColumn('action', function ($ferry_location) {
                return '
                         <a href="' . route('ferry_locations.edit', $ferry_location->id) . '">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a>
                         <br>
                         <br>
                         <form method="post" action="' . route('ferry_locations.destroy', $ferry_location->id) . '">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['action'])
            ->make(true);

    }
}
