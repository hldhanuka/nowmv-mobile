<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\UserCreateRequest;
use App\Http\Requests\CMS\UserUpdateRequest;
use App\Merchant;
use App\MerchantUser;
use App\NewsAgency;
use App\NewsAgencyUser;
use App\NgoMerchant;
use App\NgoMerchantUser;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('users.list')) {
            return view('pages.users.index');
        } else {
            activity()->log('ERROR|VIEW|User List|No users.list permission'); // ACTIVITY LOG
            return abort(403);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('users.create')) {
            $news_agencies = NewsAgency::active()->select("id", "name", "status")->get();
            $merchants = Merchant::active()->select("id", "name", "status")->get();
            $ngo_merchants = NgoMerchant::active()->select("id", "name", "status")->get();
            activity()->log('SUCCESS|VIEW|User Create'); // ACTIVITY LOG
            return view('pages.users.create', ["news_agencies" => $news_agencies, "merchants" => $merchants, "ngo_merchants" => $ngo_merchants]);
        } else {
            activity()->log('ERROR|VIEW|User Create|No users.create permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/profile/");

        $user = new User();
        $user->image = $upload_result;
//        $user->image = $request->input('image_url');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->mobile = $request->input('mobile');
        $user->address = $request->input('address');
        $user->birthday = Carbon::parse($request->input('birthday'))->toDateString();
        $user->gender = $request->input('gender');
        $user->status = 1;
        $result = $user->save();
        activity()->performedOn($user)->withProperties($user)->log('SUCCESS|CREATE|User Create'); // ACTIVITY LOG

        if ($request->input('type') == "merchant") {
            foreach ($request->input('merchant_ids') as $merchant_id) {
                $merchant_user = new MerchantUser();
                $merchant_user->user_id = $user->id;
                $merchant_user->merchant_id = $merchant_id;
                $merchant_user->status = 1;
                $merchant_user->created_by = auth()->user()->id;
                $merchant_user->authorized_by = auth()->user()->id;
                $merchant_user->published_at = Carbon::now()->toDateTimeString();
                $merchant_user->save();
                activity()->performedOn($merchant_user)->withProperties($merchant_user)->log('SUCCESS|CREATE|merchant Create'); // ACTIVITY LOG
            }
        }

        if ($request->input('type') == "ngo_merchant") {
            foreach ($request->input('ngo_merchant_ids') as $ngo_merchant_id) {
                $ngo_merchant_user = new NgoMerchantUser();
                $ngo_merchant_user->user_id = $user->id;
                $ngo_merchant_user->ngo_merchant_id = $ngo_merchant_id;
                $ngo_merchant_user->status = 1;
                $ngo_merchant_user->created_by = auth()->user()->id;
                $ngo_merchant_user->authorized_by = auth()->user()->id;
                $ngo_merchant_user->published_at = Carbon::now()->toDateTimeString();
                $ngo_merchant_user->save();
                activity()->performedOn($ngo_merchant_user)->withProperties($ngo_merchant_user)->log('SUCCESS|CREATE|ngo_merchant Create'); // ACTIVITY LOG
            }
        }

        if ($request->input('type') == "news_agent") {
            foreach ($request->input('news_agency_ids') as $news_agency_id) {
                $news_agency_user = new NewsAgencyUser();
                $news_agency_user->user_id = $user->id;
                $news_agency_user->news_agency_id = $news_agency_id;
                $news_agency_user->status = 1;
                $news_agency_user->created_by = auth()->user()->id;
                $news_agency_user->authorized_by = auth()->user()->id;
                $news_agency_user->published_at = Carbon::now()->toDateTimeString();
                $news_agency_user->save();
                activity()->performedOn($news_agency_user)->withProperties($news_agency_user)->log('SUCCESS|CREATE|news_agent Create'); // ACTIVITY LOG
            }
        }
        try {
            //custom roles
            if ($request['type'] == 'custom') {
                $name_index = 0;
                $permissions = [];
                $request_names = array_keys($request->toArray());
                foreach ($request_names as $name) {
                    if ($name != '_token') {
                        if ($name != 'type') {
                            if ($name == 'first_name') {
                                break;
                            }
                            $array = [$name => $request[$name]];
                            $permissions = $permissions + $array;
                        }
                    }
                }
                if (count($permissions) > 0) {
                    $permissions_name = array_keys($permissions);
                    foreach ($permissions as $permission) {

                        if (array_key_exists('list', $permission) && $permission['list'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'list';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('create', $permission) && $permission['create'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'create';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('update', $permission) && $permission['update'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'update';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('delete', $permission) && $permission['delete'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'delete';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('approve', $permission) && $permission['approve'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'approve';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('csv', $permission) && $permission['csv'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'csv';
                            $user->givePermissionTo($permission_name);
                        }

                        $name_index++;
                    }
                }
                $user->givePermissionTo('cms_login');
                $user->assignRole('custom');

                if ($request->has('merchant_ids')) {
                    foreach ($request->input('merchant_ids') as $merchant_id) {
                        $merchant_user = new MerchantUser();
                        $merchant_user->user_id = $user->id;
                        $merchant_user->merchant_id = $merchant_id;
                        $merchant_user->status = 1;
                        $merchant_user->created_by = auth()->user()->id;
                        $merchant_user->authorized_by = auth()->user()->id;
                        $merchant_user->published_at = Carbon::now()->toDateTimeString();
                        $merchant_user->save();
                        activity()->performedOn($merchant_user)->withProperties($merchant_user)->log('SUCCESS|CREATE|merchant Create'); // ACTIVITY LOG
                    }
                }
                if ($request->has('ngo_merchant_ids')) {
                    foreach ($request->input('ngo_merchant_ids') as $ngo_merchant_id) {
                        $ngo_merchant_user = new NgoMerchantUser();
                        $ngo_merchant_user->user_id = $user->id;
                        $ngo_merchant_user->ngo_merchant_id = $ngo_merchant_id;
                        $ngo_merchant_user->status = 1;
                        $ngo_merchant_user->created_by = auth()->user()->id;
                        $ngo_merchant_user->authorized_by = auth()->user()->id;
                        $ngo_merchant_user->published_at = Carbon::now()->toDateTimeString();
                        $ngo_merchant_user->save();
                        activity()->performedOn($ngo_merchant_user)->withProperties($ngo_merchant_user)->log('SUCCESS|CREATE|ngo_merchant Create'); // ACTIVITY LOG
                    }
                }
                if ($request->has('news_agency_ids')) {
                    foreach ($request->input('news_agency_ids') as $news_agency_id) {
                        $news_agency_user = new NewsAgencyUser();
                        $news_agency_user->user_id = $user->id;
                        $news_agency_user->news_agency_id = $news_agency_id;
                        $news_agency_user->status = 1;
                        $news_agency_user->created_by = auth()->user()->id;
                        $news_agency_user->authorized_by = auth()->user()->id;
                        $news_agency_user->published_at = Carbon::now()->toDateTimeString();
                        $news_agency_user->save();
                        activity()->performedOn($news_agency_user)->withProperties($news_agency_user)->log('SUCCESS|CREATE|news_agent Create'); // ACTIVITY LOG
                    }
                }

            } else {
                $user->assignRole($request->input('type'));
            }

        } catch (QueryException $ex) {
            activity()->performedOn($user)->withProperties($user)->log('ERROR|CREATE|User Create|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('users.index');
        } catch (Exception $e) {
            activity()->performedOn($user)->withProperties($user)->log('ERROR|CREATE|User Create|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('users.index');
        }
        if ($result) {
            activity()->performedOn($user)->withProperties($user)->log('SUCCESS|CREATE|User Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('users.index');
        } else {
            activity()->performedOn($user)->withProperties($user)->log('ERROR|CREATE|User Create|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('users.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('users.update')) {
            $user = User::find($id);
            $permissions = collect($user->permissions);
            $news_agencies = NewsAgency::active()->select("id", "name", "status")->get();
            $merchants = Merchant::active()->select("id", "name", "status")->get();
            $ngo_merchants = NgoMerchant::active()->select("id", "name", "status")->get();
            $merchant_ids = collect(MerchantUser::where('user_id', $id)->active()->get());
            $ngo_merchant_ids = collect(NgoMerchantUser::where('user_id', $id)->active()->get());
            $news_agency_ids = collect(NewsAgencyUser::where('user_id', $id)->active()->get());
            if ($user->hasRole("custom")) {
                activity()->performedOn($user)->withProperties($user)->log('SUCCESS|VIEW|Update User'); // ACTIVITY LOG
                return view('pages.users.edit', ["user" => $user, "permissions" => $permissions, "news_agencies" => $news_agencies, "merchants" => $merchants, "ngo_merchants" => $ngo_merchants, 'merchant_ids' => $merchant_ids, 'ngo_merchant_ids' => $ngo_merchant_ids, 'news_agency_ids' => $news_agency_ids]);
            } else {
                return abort(403);
            }
        } else {
            activity()->log('ERROR|VIEW|Calendar Schedule Type Edit|No users.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $user = User::find($id);
            if (!$request->has('merchant_ids') ||
                (!$request->has('restaurants') &&
                    !$request->has('restaurant_reviews') &&
                    !$request->has('meal_categories') &&
                    !$request->has('meals') &&
                    !$request->has('meal_reviews') &&
                    !$request->has('recipes') &&
                    !$request->has('recipe_reviews') &&
                    !$request->has('offer_categories') &&
                    !$request->has('offers'))
            ) {
                if (MerchantUser::where('user_id', $user->id)->exists()) {
                    $ids = MerchantUser::on()->select('id')->where('user_id', $user->id)->pluck('id');
                    foreach ($ids as $user_id) {
                        MerchantUser::destroy($user_id);
                    }
                }
            } else {
                $merchant_users_old = MerchantUser::on()->where(['user_id' => $user->id])->get();
                foreach ($merchant_users_old as $merchant_user_old) {
                    $attributes = $merchant_user_old->getAttributes();
                    if (in_array($attributes['merchant_id'], $request->input('merchant_ids'))) {
                        continue;
                    } else {
                        MerchantUser::destroy($attributes['id']);
                    }
                }
                foreach ($request->input('merchant_ids') as $merchant_id) {


                    if (!MerchantUser::on()->where(['user_id' => $user->id, 'merchant_id' => $merchant_id])->exists()) {

                        $merchant_user = new MerchantUser();
                        $merchant_user->user_id = $user->id;
                        $merchant_user->merchant_id = $merchant_id;
                        $merchant_user->status = 1;
                        $merchant_user->created_by = auth()->user()->id;
                        $merchant_user->authorized_by = auth()->user()->id;
                        $merchant_user->published_at = Carbon::now()->toDateTimeString();
                        $merchant_user->save();
                    }

                }
            }
            if (!$request->has('ngo_merchant_ids') ||
                (!$request->has('ngos') &&
                    !$request->has('donations') &&
                    !$request->has('badges'))
            ) {
                if (NgoMerchantUser::where('user_id', $user->id)->exists()) {
                    $ids = NgoMerchantUser::on()->select('id')->where('user_id', $user->id)->pluck('id');
                    foreach ($ids as $user_id) {
                        NgoMerchantUser::destroy($user_id);
                    }
                }
            } else {
                $ngo_merchant_users_old = NgoMerchantUser::on()->where(['user_id' => $user->id])->get();
                foreach ($ngo_merchant_users_old as $ngo_merchant_user_old) {
                    $attributes = $ngo_merchant_user_old->getAttributes();
                    if (in_array($attributes['ngo_merchant_id'], $request->input('ngo_merchant_ids'))) {
                        continue;
                    } else {
                        NgoMerchantUser::destroy($attributes['id']);
                    }
                }
                foreach ($request->input('ngo_merchant_ids') as $ngo_merchant_id) {
                    if (!NgoMerchantUser::on()->where(['user_id' => $user->id, 'ngo_merchant_id' => $ngo_merchant_id])->exists()) {
                        $ngo_merchant_user = new NgoMerchantUser();
                        $ngo_merchant_user->user_id = $user->id;
                        $ngo_merchant_user->ngo_merchant_id = $ngo_merchant_id;
                        $ngo_merchant_user->status = 1;
                        $ngo_merchant_user->created_by = auth()->user()->id;
                        $ngo_merchant_user->authorized_by = auth()->user()->id;
                        $ngo_merchant_user->published_at = Carbon::now()->toDateTimeString();
                        $ngo_merchant_user->save();
                    }
                }
            }
            if (!$request->has('news_agency_ids') ||
                (!$request->has('news_categories') &&
                    !$request->has('news_tags') &&
                    !$request->has('news'))
            ) {
                if (NewsAgencyUser::where('user_id', $user->id)->exists()) {
                    $ids = NewsAgencyUser::on()->select('id')->where('user_id', $user->id)->pluck('id');
                    foreach ($ids as $user_id) {
                        NewsAgencyUser::destroy($user_id);
                    }
                }
            } else {
                $news_agency_users_old = NewsAgencyUser::on()->where(['user_id' => $user->id])->get();
                foreach ($news_agency_users_old as $news_agency_user_old) {
                    $attributes = $news_agency_user_old->getAttributes();
                    if (in_array($attributes['news_agency_id'], $request->input('news_agency_ids'))) {
                        continue;
                    } else {
                        NewsAgencyUser::destroy($attributes['id']);
                    }
                }
                foreach ($request->input('news_agency_ids') as $news_agency_id) {

                    if (!NewsAgencyUser::on()->where(['user_id' => $user->id, 'news_agency_id' => $news_agency_id])->exists()) {

                        $news_agency_user = new NewsAgencyUser();
                        $news_agency_user->user_id = $user->id;
                        $news_agency_user->news_agency_id = $news_agency_id;
                        $news_agency_user->status = 1;
                        $news_agency_user->created_by = auth()->user()->id;
                        $news_agency_user->authorized_by = auth()->user()->id;
                        $news_agency_user->published_at = Carbon::now()->toDateTimeString();
                        $news_agency_user->save();
                    }
                }
            }

            if ($user->hasRole('custom')) {
                $user->syncPermissions();
                $name_index = 0;
                $permissions = [];
                $request_names = array_keys($request->toArray());
                foreach ($request_names as $name) {
                    if ($name != '_token') {
                        if ($name != '_method') {
                            if ($name == 'merchant_ids') {
                                break;
                            }
                            if ($name == 'ngo_merchants_ids') {
                                break;
                            }
                            if ($name == 'news_agency_ids') {
                                break;
                            }
                            $array = [$name => $request[$name]];
                            $permissions = $permissions + $array;

                        }
                    }
                }
                if (count($permissions) > 0) {
                    $permissions_name = array_keys($permissions);
                    foreach ($permissions as $permission) {

                        if (array_key_exists('list', $permission) && $permission['list'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'list';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('create', $permission) && $permission['create'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'create';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('update', $permission) && $permission['update'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'update';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('delete', $permission) && $permission['delete'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'delete';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('approve', $permission) && $permission['approve'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'approve';
                            $user->givePermissionTo($permission_name);
                        }
                        if (array_key_exists('csv', $permission) && $permission['csv'] == '1') {
                            $permission_name = $permissions_name[$name_index] . '.' . 'csv';
                            $user->givePermissionTo($permission_name);
                        }

                        $name_index++;
                    }
                }
                $user->givePermissionTo('cms_login');
                $user->assignRole('custom');

                activity()->performedOn($user)->withProperties($user)->log('SUCCESS|UPDATE|User Permissions Update'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Updated');
                return redirect()->route('users.index');

            } else {
                return abort(404);
            }

        } catch
        (QueryException $ex) {
            activity()->performedOn($user)->withProperties($user)->log('ERROR|CREATE|User Permissions Update|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('users.index');
        } catch (\Exception $e) {
            activity()->performedOn($user)->withProperties($user)->log('ERROR|CREATE|User Permissions Update|Fail'); // ACTIVITY LOG
            connectify('error', 'Error', 'Service Error');
            return redirect()->route('users.index');
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('users.delete')) {
            $user = User::find($id);
            $result = $user->delete();

            if ($result) {
                activity()->performedOn($user)->withProperties($user)->log('SUCCESS|DELETE|User Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('users.index');
            } else {
                activity()->performedOn($user)->withProperties($user)->log('ERROR|DELETE|User Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('users.index');
            }
        } else {
            activity()->log('ERROR|DELETE|User Delete|No users.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $users = User::Latest()->get();

        return Datatables::of($users)
            ->editColumn('image', function ($user) {
                return '<img width="50px" src="' . $user->image . '" alt="">';
            })
            ->editColumn('roles', function ($user) {
                return implode(", ", $user->roles()->pluck('name')->toArray());
            })
            ->addColumn('action', function ($user) {
                $html = '';
                if (auth()->user()->id != $user->id) {
                    if (!$user->hasRole("customer")) {
                        // Other users
                        $html = '<button class="btn btn-warning" onclick="changeStatus(' . $user->id . ')" title="reset password"><i class="fas fa-lock"></i></button><br><br>';
                    }

                    if (auth()->user()->hasPermissionTo('users.update') && $user->hasRole("custom")) {
                        $html .= '<a href="' . route('users.edit', $user->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                    }

                    if ($user->status == 1) {
                        $html = $html . '<form method="post" action="' . route('users.users-status-change') . '">
                            <input type="hidden" name="_token" value="' . csrf_token() . '">
                            <input type="hidden" name="id" value="' . $user->id . '">
                            <input type="hidden" name="status" value="3">
                            <button class="btn btn-danger" type="submit" title="reject"><i class="fas fa-ban"></i></button>
                            </form>';
                    } else if ($user->status == 3) {
                        $html = $html . '<form method="post" action="' . route('users.users-status-change') . '">
                            <input type="hidden" name="_token" value="' . csrf_token() . '">
                            <input type="hidden" name="id" value="' . $user->id . '">
                            <input type="hidden" name="status" value="1">
                            <button class="btn btn-success" type="submit" title="activate"><i class="fas fa-check"></i></button>
                            </form>';
                    }
                } else {
                    $html = '';
                }
                return $html;
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }

    public function resetPassword(Request $request)
    {
        if (auth()->user()->hasPermissionTo('users.update')) {
            $reset_password = Setting::where('title', "reset_password")->first()->text;
            $user = User::find($request->input("id"));
            $user->password = bcrypt($reset_password);
            $result = $user->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Password has been reset",
                    "data" => null,

                ];
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Password has not been reset",
                    "data" => null,

                ];
                return response()->json($response);
            }
        } else {
            return abort(403);
        }
    }

    public function userStatusChange(Request $request)
    {
        if (auth()->user()->hasPermissionTo('users.update')) {
            $user = User::find($request->input("id"));
            $user->status = $request->input("status");
            $result = $user->save();

            if ($result) {
                connectify('success', 'Success', 'Successfully change the status');
                return redirect()->route('users.index');
            } else {
                connectify('error', 'Error', 'Error on status change');
                return redirect()->route('users.index');
            }
        } else {
            return abort(403);
        }
    }
}
