<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\InterestCreateRequest;
use App\Http\Requests\CMS\InterestUpdateRequest;
use App\Interest;
use App\UserInterest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class InterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('interests.list')) {
            return view('pages.interests.index');
        } else {
            activity()->log('ERROR|VIEW|Interests List|No interests.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('interests.create')) {
            activity()->log('SUCCESS|VIEW|Interest Create'); // ACTIVITY LOG
            return view('pages.interests.create');
        } else {
            activity()->log('ERROR|VIEW|Interest Create|No interests.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InterestCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InterestCreateRequest $request)
    {
        $interest = new Interest();
        $interest->title = $request->input("title");
        $interest->status = 2;
        $interest->created_by = auth()->user()->id;

        $result = $interest->save();

        if ($result) {
            activity()->performedOn($interest)->withProperties($interest)->log('SUCCESS|CREATE|Interest Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('interests.index');
        } else {
            activity()->performedOn($interest)->withProperties($interest)->log('ERROR|CREATE|Interest Create|Fail'); // ACTIVITY LOG
            return redirect()->route('interests.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('interests.update')) {
            $interest = Interest::find($id);
            activity()->performedOn($interest)->withProperties($interest)->log('SUCCESS|VIEW|Interest Edit'); // ACTIVITY LOG
            return view('pages.interests.edit', ["interest" => $interest]);
        } else {
            activity()->log('ERROR|VIEW|Interest Edit|No interests.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InterestUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(InterestUpdateRequest $request, $id)
    {
        $interest = Interest::find($id);
        $interest->title = $request->input("title");
        $interest->status = 2;
        $interest->created_by = auth()->user()->id;

        $result = $interest->save();

        if ($result) {
            activity()->performedOn($interest)->withProperties($interest)->log('SUCCESS|UPDATE|Interest Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('interests.index');
        } else {
            activity()->performedOn($interest)->withProperties($interest)->log('ERROR|UPDATE|Interest Update|Fail'); // ACTIVITY LOG
            return redirect()->route('interests.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('interests.delete')) {
            $interest = Interest::find($id);
            UserInterest::where("interest_id", $id)->delete();
            $result = $interest->delete();
            if ($result) {
                activity()->performedOn($interest)->withProperties($interest)->log('SUCCESS|DELETE|Interest Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('interests.index');
            } else {
                activity()->performedOn($interest)->withProperties($interest)->log('ERROR|DELETE|Interest Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('interests.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Interest Delete|No interests.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('interests.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $interest = Interest::find($id);
            $interest->status = $status;

            if ($status == 1) {
                $interest->published_at = Carbon::now()->toDateTimeString();
            }
            $interest->authorized_by = auth()->user()->id;

            $result = $interest->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($interest)->withProperties($interest)->log('SUCCESS|APPROVE|Interest Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed error",
                    "data" => null,

                ];
                activity()->performedOn($interest)->withProperties($interest)->log('ERROR|APPROVE|Interest Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Interest Approve|No interests.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $interests = Interest::Latest()->get();

        return Datatables::of($interests)
            ->addColumn('created_user_email', function ($interest) {
                return $interest->createdUser != null ? $interest->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($interest) {
                return $interest->authorizedUser != null ? $interest->authorizedUser->email : "";
            })
            ->addColumn('action', function ($interest) {
                $html = '';

                if (auth()->user()->hasPermissionTo('interests.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $interest->id . ')"><i class="far fa-check-square"></i></button>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('interests.update')) {
                    $html .= '<a href="' . route('interests.edit', $interest->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('interests.delete')) {
                    $html .= '<form method="post" action="' . route('interests.destroy', $interest->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
