<?php

namespace App\Http\Controllers\CMS;

use App\BusHaltSchedule;
use App\BusLocation;
use App\BusRoute;
use App\Http\Requests\CMS\BusHaltScheduleCreateRequest;
use App\Http\Requests\CMS\BusHaltScheduleUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class BusHaltScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('bus_halt_schedules.list')) {
            return view('pages.bus_halt_schedules.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('bus_halt_schedules.create')) {
            $bus_routes = BusRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $start_locations = BusLocation::active()->where('type', "main")->select("id", "name", "status")->get();
            $bus_halts = BusLocation::active()->select("id", "name", "status")->orderBy('name')->get();
            return view('pages.bus_halt_schedules.create', ["bus_routes" =>$bus_routes, "start_locations" => $start_locations, "bus_halts"=>$bus_halts]);
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusHaltScheduleCreateRequest $request)
    {
        $bus_halt_schedule = new BusHaltSchedule();
        $bus_halt_schedule->bus_route_id = $request->input("bus_route");
        $bus_halt_schedule->start_location_id = $request->input("start_location");
        $bus_halt_schedule->halt_id = $request->input("halt");
        $bus_halt_schedule->status = $request->input("status");
        $bus_halt_schedule->time = $request->input("time");

        $result = $bus_halt_schedule->save();
        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('bus_halt_schedules.index');
        } else {
            return redirect()->route('bus_halt_schedules.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('bus_halt_schedules.update')) {
            $bus_halt_schedule = BusHaltSchedule::find($id);
            $bus_routes = BusRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $start_locations = BusLocation::active()->where('type', "main")->select("id", "name", "status")->orderBy('name')->get();
            $bus_halts = BusLocation::active()->select("id", "name", "status")->orderBy('name')->get();
            return view('pages.bus_halt_schedules.edit', ["bus_halt_schedule"=> $bus_halt_schedule, "bus_routes" =>$bus_routes, "start_locations" => $start_locations, "bus_halts"=>$bus_halts]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BusHaltScheduleUpdateRequest $request, $id)
    {
        $bus_halt_schedule = BusHaltSchedule::find($id);
        $bus_halt_schedule->bus_route_id = $request->input("bus_route");
        $bus_halt_schedule->start_location_id = $request->input("start_location");
        $bus_halt_schedule->halt_id = $request->input("halt");
        $bus_halt_schedule->status = $request->input("status");
        $bus_halt_schedule->time = $request->input("time");

        $result = $bus_halt_schedule->save();
        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('bus_halt_schedules.index');
        } else {
            return redirect()->route('bus_halt_schedules.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('bus_halt_schedules.delete')) {
            $bus_halt_schedule = BusHaltSchedule::find($id);
            $result = $bus_halt_schedule->delete();

            if ($result) {
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('bus_halt_schedules.index');
            } else {
                return redirect()->route('bus_halt_schedules.index');
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $bus_halt_schedules = BusHaltSchedule::with(['startLocation','halt','busRoute'])->Latest()->get();

        return Datatables::of($bus_halt_schedules)
            ->editColumn('bus_route', function ($bus_halt_schedule) {
                return $bus_halt_schedule->busRoute->name;
            })
            ->editColumn('start_location', function ($bus_halt_schedule) {
                return $bus_halt_schedule->startLocation->name;
            })
            ->editColumn('halt', function ($bus_halt_schedule) {
                return $bus_halt_schedule->halt->name;
            })
            ->addColumn('action', function ($bus_halt_schedule) {
                return '
                         <a href="'. route('bus_halt_schedules.edit',$bus_halt_schedule->id) .'">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a>
                         <br>
                         <br>
                         <form method="post" action="'. route('bus_halt_schedules.destroy',$bus_halt_schedule->id) .'">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['bus_route','start_location','halt', 'action'])
            ->make(true);

    }
}
