<?php

namespace App\Http\Controllers\CMS;

use App\Badge;
use App\Donation;
use App\DonationBadge;
use App\Event;
use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\BadgeCreateRequest;
use App\Http\Requests\CMS\BadgeUpdateRequest;
use App\Ngo;
use App\NgoBadge;
use App\NgoMerchantUser;
use App\UserBadge;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('badges.list')) {
            return view('pages.badges.index');
        } else {
            activity()->log('ERROR|VIEW|Badges List|No badges.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('badges.create')) {
            if (auth()->user()->hasRole('admin')) {
                $donations = Donation::active()->select("id", "title", "status")->orderBy('title')->get();
                $ngos = Ngo::get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngos = Ngo::whereIn("ngo_merchant_id", auth()->user()->ngoMerchants()->pluck("ngo_merchant_id"))->get();
                $ngo_ids = $ngos->pluck("id")->toArray();
                $donations = Donation::with('ngo')->whereIn('ngo_id', $ngo_ids)->select("id", "title", "status")->orderBy('title')->get();
            } else {
                activity()->log('ERROR|VIEW|Badge Create|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
            activity()->log('SUCCESS|VIEW|Badge Create'); // ACTIVITY LOG
            return view('pages.badges.create', ["donations" => $donations, "ngos" => $ngos]);
        } else {
            activity()->log('ERROR|VIEW|Badges Create|No badges.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BadgeCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BadgeCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/badges/");
        $badge = new Badge();
        $badge->title = $request->input("title");
        $badge->type = $request->input("type");
        $badge->description = $request->input("description");
        $badge->image = $upload_result;
//        $badge->image = $request->input("image_url");
        $badge->status = 2;
        $badge->created_by = auth()->user()->id;

        if ($badge->status == 1) {
            $badge->published_at = Carbon::now()->toDateTimeString();
        }
        if ($badge->status == 1) {
            $badge->authorized_by = auth()->user()->id;
        }
        $result = $badge->save();

        if ($request->input("type") == "donation") {
            $donation_badge = new DonationBadge();
            $donation_badge->donation_id = $request->input("donation_id");
            $donation_badge->badge_id = $badge->id;
            $donation_badge->winning_limit = $request->input("winning_limit");
            $donation_badge->status = 1;
            $donation_badge->save();
            activity()->performedOn($donation_badge)->withProperties($donation_badge)->log('SUCCESS|CREATE|Donation Badge Create'); // ACTIVITY LOG
        } else {
            $ngo_badge = new NgoBadge();
            $ngo_badge->ngo_id = $request->input("ngo_id");
            $ngo_badge->badge_id = $badge->id;
            $ngo_badge->winning_limit = $request->input("winning_limit");
            $ngo_badge->status = 1;
            $ngo_badge->save();
            activity()->performedOn($ngo_badge)->withProperties($ngo_badge)->log('SUCCESS|CREATE|Ngo Badge Create'); // ACTIVITY LOG
        }

        if ($result) {
            activity()->performedOn($badge)->withProperties($badge)->log('SUCCESS|CREATE|Badge Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('badges.index');
        } else {
            activity()->performedOn($badge)->withProperties($badge)->log('ERROR|CREATE|Badge Create|Fail'); // ACTIVITY LOG
            return redirect()->route('badges.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('badges.update')) {
            $badge = Badge::find($id);
            $donation_badge = DonationBadge::where('badge_id', $badge->id)->first();
            $ngo_badge = NgoBadge::where('badge_id', $badge->id)->first();

            if (auth()->user()->hasRole('admin')) {
                $donations = Donation::active()->select("id", "title", "status")->orderBy('title')->get();
                $ngos = Ngo::get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngos = Ngo::whereIn("ngo_merchant_id", auth()->user()->ngoMerchants()->pluck("ngo_merchant_id"))->get();
                $ngo_ids = $ngos->pluck("id")->toArray();
                $donations = Donation::with('ngo')->whereIn('ngo_id', $ngo_ids)->select("id", "title", "status")->orderBy('title')->get();
            } else {
                activity()->log('ERROR|VIEW|Badges Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }

            activity()->performedOn($badge)->withProperties($badge)->log('SUCCESS|VIEW|Badge Edit'); // ACTIVITY LOG
            return view('pages.badges.edit', ["badge" => $badge, "ngos" => $ngos, "donations" => $donations, "donation_badge" => $donation_badge, "ngo_badge" => $ngo_badge]);
        } else {
            activity()->log('ERROR|VIEW|Badge Edit|No badges.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BadgeUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BadgeUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/badges/");
        $badge = Badge::find($id);
        $badge->title = $request->input("title");
        $badge->type = $request->input("type");
        $badge->description = $request->input("description");
        if ($upload_result != null) {
            $badge->image = $upload_result;
        }
//        if ($request->input("image_url") != null) {
//            $badge->image = $request->input("image_url");
//        }
        $badge->status = 2;
        $badge->created_by = auth()->user()->id;
        $result = $badge->save();

        if ($request->input("type") == "donation") {
            $donation_badge_id = $request->input("donation_badge_id");
            $donation_badge = DonationBadge::find($donation_badge_id);
            $donation_badge->donation_id = $request->input("donation_id");
            $donation_badge->badge_id = $badge->id;
            $donation_badge->winning_limit = $request->input("winning_limit");
            $donation_badge->status = 1;
            $donation_badge->save();
        } else {
            $ngo_badge_id = $request->input("ngo_badge_id");
            $ngo_badge = NgoBadge::find($ngo_badge_id);
            $ngo_badge->ngo_id = $request->input("ngo_id");
            $ngo_badge->badge_id = $badge->id;
            $ngo_badge->winning_limit = $request->input("winning_limit");
            $ngo_badge->status = 1;
            $ngo_badge->save();
        }

        if ($result) {
            activity()->performedOn($badge)->withProperties($badge)->log('SUCCESS|UPDATE|Badge Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('badges.index');
        } else {
            activity()->performedOn($badge)->withProperties($badge)->log('ERROR|UPDATE|Badge Update|Fail'); // ACTIVITY LOG
            return redirect()->route('badges.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('badges.delete')) {
            $badge = Badge::find($id);

            $user_has_badge = UserBadge::where("badge_id", $id)->exists();
            if ($user_has_badge) {
                activity()->performedOn($badge)->withProperties($badge)->log('ERROR|DELETE|Badge Delete|Some users already has this badge.'); // ACTIVITY LOG
                connectify('error', 'Error', 'Some users already has this badge.');
                return redirect()->route('badges.index');
            } else {
                DonationBadge::where("badge_id", $id)->delete();
                $result = $badge->delete();
                if ($result) {
                    activity()->performedOn($badge)->withProperties($badge)->log('SUCCESS|DELETE|Badge Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('badges.index');
                } else {
                    activity()->performedOn($badge)->withProperties($badge)->log('ERROR|DELETE|Badge Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('badges.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|Badge Delete|No badges.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('badges.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $badge = Badge::find($id);
            $badge->status = $status;
            if ($status == 1) {
                $badge->published_at = Carbon::now()->toDateTimeString();
            }
            $badge->authorized_by = auth()->user()->id;

            $result = $badge->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($badge)->withProperties($badge)->log('SUCCESS|APPROVE|Badge Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($badge)->withProperties($badge)->log('ERROR|APPROVE|Badge Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Badge Approve|No badges.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * @throws \Exception
     */
    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('badges.list')) {
            if (auth()->user()->hasRole('admin')) {
                $badges = Badge::Latest()->get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngo_ids = Ngo::whereIn("ngo_merchant_id", auth()->user()->ngoMerchants()->pluck("ngo_merchant_id"))->pluck("id")->toArray();
                $donation_ids = Donation::with('ngo')->whereIn('ngo_id', $ngo_ids)->pluck("id")->toArray();
                $badges = Badge::whereHas("ngoBadges", function ($q) use ($ngo_ids) {
                    $q->whereIn('ngo_id', $ngo_ids);
                })->orWhereHas("donationBadges", function ($q) use ($donation_ids) {
                    $q->whereIn('donation_id', $donation_ids);
                })->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($badges)
            ->editColumn('image', function ($badge) {
                return '<img width="50px" src="' . $badge->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($badge) {
                return $badge->createdUser != null ? $badge->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($badge) {
                return $badge->authorizedUser != null ? $badge->authorizedUser->email : "";
            })
            ->addColumn('action', function ($badge) {

                $html = '';

                if (auth()->user()->hasPermissionTo('badges.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $badge->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('badges.update')) {
                    $html .= '<a href="' . route('badges.edit', $badge->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('badges.delete')) {
                    $html .= '<form method="post" action="' . route('badges.destroy', $badge->id) . '">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                    </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;

            })
            ->rawColumns(['image', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
