<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Meal;
use App\MealCategory;
use App\Merchant;
use App\MerchantUser;
use App\PrayerTip;
use App\Restaurant;
use App\RestaurantRating;
use App\RestaurantReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\RestaurantCreateRequest;
use App\Http\Requests\CMS\RestaurantUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('restaurants.list')) {
            $restaurants = Restaurant::Latest()->paginate(10);
            return view('pages.restaurants.index', ["restaurants" => $restaurants]);
        } else {
            activity()->log('ERROR|VIEW|Restaurant List|No restaurants.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('restaurants.create')) {
            $merchants = Merchant::active()->select("id", "name", "status")->orderBy('name')->get();
            activity()->log('SUCCESS|VIEW|Restaurant Create'); // ACTIVITY LOG
            return view('pages.restaurants.create', ["merchants" => $merchants]);
        } else {
            activity()->log('ERROR|VIEW|Restaurant Create|No restaurants.create permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RestaurantCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/restaurants/");
        $restaurant = new Restaurant();
        $restaurant->title = $request->input("title");
        $restaurant->merchant_id = $request->input("merchant_id");
        $restaurant->longitude = $request->input("longitude");
        $restaurant->latitude = $request->input("latitude");
        $restaurant->location_name = $request->input("location_name");
        $restaurant->content = $request->input("content");
        $restaurant->image = $upload_result;
//        $restaurant->image = $request->input("image_url");
        $restaurant->status = 2;
        $restaurant->created_by = auth()->user()->id;

        $result = $restaurant->save();

        if ($result) {
            activity()->performedOn($restaurant)->withProperties($restaurant)->log('SUCCESS|CREATE|Restaurant Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('restaurants.index');
        } else {
            activity()->performedOn($restaurant)->withProperties($restaurant)->log('ERROR|CREATE|Restaurant Create|Fail'); // ACTIVITY LOG
            return redirect()->route('restaurants.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('restaurants.update')) {
            $restaurant = Restaurant::find($id);
            $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
            if (auth()->user()->hasRole("admin") || in_array($restaurant->merchant_id, $merchant_ids)) {
                $merchants = Merchant::active()->select("id", "name", "status")->orderBy('name')->get();
                activity()->performedOn($restaurant)->withProperties($restaurant)->log('SUCCESS|VIEW|Restaurant Edit'); // ACTIVITY LOG
                return view('pages.restaurants.edit', ["restaurant" => $restaurant, "merchants" => $merchants]);
            } else {
                activity()->log('ERROR|VIEW|Restaurant Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
        } else {
            activity()->log('ERROR|VIEW|Restaurant Edit|No restaurants.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RestaurantUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/restaurants/");
        $restaurant = Restaurant::find($id);
        $restaurant->title = $request->input("title");
        $restaurant->merchant_id = $request->input("merchant_id");
        $restaurant->longitude = $request->input("longitude");
        $restaurant->latitude = $request->input("latitude");
        $restaurant->location_name = $request->input("location_name");
        $restaurant->content = $request->input("content");
        if ($upload_result != null) {
            $restaurant->image = $upload_result;
        }
//        if ($request->input("image_url") != null) {
//            $restaurant->image = $request->input("image_url");
//        }
        $restaurant->status = 2;
        $restaurant->created_by = auth()->user()->id;
        $result = $restaurant->save();

        if ($result) {
            activity()->performedOn($restaurant)->withProperties($restaurant)->log('SUCCESS|UPDATE|Restaurant Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('restaurants.index');
        } else {
            activity()->performedOn($restaurant)->withProperties($restaurant)->log('ERROR|UPDATE|Restaurant Update|Fail'); // ACTIVITY LOG
            return redirect()->route('restaurants.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('restaurants.delete')) {
            $restaurant = Restaurant::find($id);
            $has_meal_category = MealCategory::where("restaurant_id", $id)->exists();
            if ($has_meal_category) {
                activity()->performedOn($restaurant)->withProperties($restaurant)->log('ERROR|DELETE|Restaurant Delete|This restaurant already has meal categories'); // ACTIVITY LOG
                connectify('error', 'Error', 'This restaurant already has meal categories');
                return redirect()->route('restaurants.index');
            } else {
                RestaurantReview::where("restaurant_id", $id)->delete();
                RestaurantRating::where("restaurant_id", $id)->delete();
                $result = $restaurant->delete();
                if ($result) {
                    activity()->performedOn($restaurant)->withProperties($restaurant)->log('SUCCESS|DELETE|Restaurant Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('restaurants.index');
                } else {
                    activity()->performedOn($restaurant)->withProperties($restaurant)->log('ERROR|DELETE|Restaurant Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('restaurants.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|Restaurant Delete|No restaurants.delete permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('restaurants.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $restaurant = Restaurant::find($id);
            $restaurant->status = $status;

            if ($status == 1) {
                $restaurant->published_at = Carbon::now()->toDateTimeString();
            }
            $restaurant->authorized_by = auth()->user()->id;

            $result = $restaurant->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($restaurant)->withProperties($restaurant)->log('SUCCESS|APPROVE|Restaurant Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($restaurant)->withProperties($restaurant)->log('ERROR|APPROVE|Restaurant Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Restaurant Approve|No restaurants.approve permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('restaurants.list')) {
            if (auth()->user()->hasRole('admin')) {
                $restaurant = Restaurant::with('merchant')->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
                $restaurant = Restaurant::with('merchant')->whereIn('merchant_id', $merchant_ids)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($restaurant)
            ->editColumn('image', function ($restaurant) {
                return '<img width="50px" src="' . $restaurant->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($restaurant) {
                return ($restaurant->createdUser != null) ? $restaurant->createdUser->email : "-";
            })
            ->addColumn('merchant_id', function ($restaurant) {
                return ($restaurant->merchant != null) ? $restaurant->merchant->name : "-";
            })
            ->addColumn('action', function ($restaurant) {
                $html = '';

                if (auth()->user()->hasPermissionTo('restaurants.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $restaurant->id . ')">
                            <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('restaurants.update') || auth()->user()->hasPermissionTo('merchants.update')) {
                    $html .= '<a href="' . route('restaurants.edit', $restaurant->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('restaurants.delete') || auth()->user()->hasPermissionTo('merchants.delete')) {
                    $html .= '<form method="post" action="' . route('restaurants.destroy', $restaurant->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'merchant_id', 'action'])
            ->make(true);

    }
}
