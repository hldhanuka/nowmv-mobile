<?php

namespace App\Http\Controllers\CMS;

use App\Donation;
use App\DonationBadge;
use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\DonationCreateRequest;
use App\Http\Requests\CMS\DonationUpdateRequest;
use App\Ngo;
use App\UserDonation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('donations.list')) {
            return view('pages.donations.index');
        } else {
            activity()->log('ERROR|VIEW|Donation List|No donations.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('donations.create')) {
            if (auth()->user()->hasRole('admin')) {
                $ngos = Ngo::active()->select("id", "title", "status")->orderBy('title')->get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngos = Ngo::whereIn("ngo_merchant_id", auth()->user()->ngoMerchants()->pluck("ngo_merchant_id"))->select("id", "title", "status")->orderBy('title')->get();
            } else {
                activity()->log('ERROR|VIEW|Donation Create|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
            activity()->log('SUCCESS|VIEW|Donation Create'); // ACTIVITY LOG
            return view('pages.donations.create', ["ngos" => $ngos]);
        } else {
            activity()->log('ERROR|VIEW|Donation Create|No donations.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DonationCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/donations/");
        $donation = new Donation();
        $donation->title = $request->input("title");
        $donation->type = "campaign";
        $donation->content = $request->input("content");
        $donation->tag = $request->input("tag");
        $donation->max_limit = $request->input("max_limit");
        $donation->ngo_id = $request->input("ngo_id");
        $donation->image = $upload_result;
//        $donation->image = $request->input("image_url");
        $donation->status = 2;
        $donation->created_by = auth()->user()->id;

        $result = $donation->save();

        if ($result) {
            activity()->performedOn($donation)->withProperties($donation)->log('SUCCESS|CREATE|Donation Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('donations.index');
        } else {
            activity()->performedOn($donation)->withProperties($donation)->log('ERROR|CREATE|Donation Create|Fail'); // ACTIVITY LOG
            return redirect()->route('donations.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('donations.update')) {
            $donation = Donation::find($id);
            if (auth()->user()->hasRole('admin')) {
                $ngos = Ngo::active()->select("id", "title", "status")->orderBy('title')->get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngos = Ngo::whereIn("ngo_merchant_id", auth()->user()->ngoMerchants()->pluck("ngo_merchant_id"))->select("id", "title", "status")->orderBy('title')->get();
            } else {
                activity()->log('ERROR|VIEW|Donation Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
            activity()->performedOn($donation)->withProperties($donation)->log('SUCCESS|VIEW|Donation Edit'); // ACTIVITY LOG
            return view('pages.donations.edit', ["donation" => $donation, "ngos" => $ngos]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DonationUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/donations/");
        $donation = Donation::find($id);
        $donation->title = $request->input("title");
        $donation->content = $request->input("content");
        $donation->tag = $request->input("tag");
        $donation->max_limit = $request->input("max_limit");
        if ($upload_result != null) {
            $donation->image = $upload_result;
        }
//        if($request->input("image_url") != null){
//            $donation->image = $request->input("image_url");
//        }
        $donation->ngo_id = $request->input("ngo_id");
        $donation->status = 2;
        $donation->created_by = auth()->user()->id;

        $result = $donation->save();

        if ($result) {
            activity()->performedOn($donation)->withProperties($donation)->log('SUCCESS|UPDATE|Donation Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('donations.index');
        } else {
            activity()->performedOn($donation)->withProperties($donation)->log('ERROR|UPDATE|Donation Update|Fail'); // ACTIVITY LOG
            return redirect()->route('donations.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('donations.delete')) {
            $donation = Donation::find($id);
            DonationBadge::where("donation_id", $id)->delete();
            UserDonation::where("donation_id", $id)->delete();
            $result = $donation->delete();
            if ($result) {
                activity()->performedOn($donation)->withProperties($donation)->log('SUCCESS|DELETE|Donation Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('donations.index');
            } else {
                activity()->performedOn($donation)->withProperties($donation)->log('ERROR|DELETE|Donation Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('donations.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Donation Delete|No donations.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('donations.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $donation = Donation::find($id);
            $donation->status = $status;
            if ($status == 1) {
                $donation->published_at = Carbon::now()->toDateTimeString();
            }
            $donation->authorized_by = auth()->user()->id;

            $result = $donation->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($donation)->withProperties($donation)->log('SUCCESS|APPROVE|Donation Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($donation)->withProperties($donation)->log('ERROR|APPROVE|Donation Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Donation Approve|No Donation.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('donations.list')) {
            if (auth()->user()->hasRole('admin')) {
                $donations = Donation::with('ngo')->Latest()->get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngo_ids = Ngo::whereIn("ngo_merchant_id", auth()->user()->ngoMerchants()->pluck("ngo_merchant_id"))->pluck("id")->toArray();
                $donations = Donation::with('ngo')->whereIn('ngo_id', $ngo_ids)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($donations)
            ->editColumn('image', function ($donation) {
                return '<img width="50px" src="' . $donation->image . '" alt="">';
            })
            ->addColumn('created_user_email', function ($donation) {
                return $donation->createdUser != null ? $donation->createdUser->email : "";
            })
            ->addColumn('ngo_name', function ($donation) {
                return $donation->ngo != null ? $donation->ngo->title : "Not Assigned";
            })
            ->addColumn('action', function ($donation) {

                $html = '';

                if (auth()->user()->hasPermissionTo('donations.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $donation->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('donations.update')) {
                    $html .= '<a href="' . route('donations.edit', $donation->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('donations.delete')) {
                    $html .= '<form method="post" action="' . route('donations.destroy', $donation->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;

            })
            ->rawColumns(['image', 'created_user_email', 'ngo_name', 'action'])
            ->make(true);

    }
}
