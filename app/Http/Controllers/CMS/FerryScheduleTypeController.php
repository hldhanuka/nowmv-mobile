<?php

namespace App\Http\Controllers\CMS;

use App\BusLocation;
use App\FerryScheduleType;
use App\Helpers\CMSHelper;
use App\Http\Requests\CMS\FerryScheduleTypeCreateRequest;
use App\Http\Requests\CMS\FerryScheduleTypeUpdateRequest;
use App\ScheduleType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class FerryScheduleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('schedule_types.list')) {
            return view('pages.ferry_schedule_types.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('schedule_types.create')) {
            return view('pages.ferry_schedule_types.create');
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(FerryScheduleTypeCreateRequest $request)
    {
        $date_title_array = CMSHelper::createDateTitleArray ($request->input('date'), $request->input('title'));

        $schedule_type = new ScheduleType();
        $schedule_type->name = $request->input('name');
        $schedule_type->type = 'ferry';
        $schedule_type->priority = $request->input('priority');
        $schedule_type->date = CMSHelper::createDateScheduleArray($request->input('date_array'));
        $schedule_type->status = 1;
        $result = $schedule_type->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ferry_schedule_types.index');
        } else {
            return redirect()->route('ferry_schedule_types.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('schedule_types.update')) {
            $schedule_type = ScheduleType::find($id);
            return view('pages.ferry_schedule_types.edit', ["schedule_type" => $schedule_type]);
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FerryScheduleTypeUpdateRequest $request, $id)
    {
        $date_title_array = CMSHelper::createDateTitleArray ($request->input('date'), $request->input('title'));

        $schedule_type = ScheduleType::find($id);
        $schedule_type->name = $request->input('name');
        $schedule_type->priority = $request->input('priority');
        $schedule_type->date = CMSHelper::createDateScheduleArray($request->input('date_array'));
        $schedule_type->status = 1;
        $result = $schedule_type->save();

        if ($result) {
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('ferry_schedule_types.index');
        } else {
            return redirect()->route('ferry_schedule_types.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('schedule_types.delete')) {
            $schedule_type = ScheduleType::find($id);
            $result = $schedule_type->delete();
            if ($result) {
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('ferry_schedule_types.index');
            } else {
                return redirect()->route('ferry_schedule_types.index');
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $schedule_types = ScheduleType::ferrySchedule()->Latest()->get();

        return Datatables::of($schedule_types)
            ->addColumn('action', function ($schedule_type) {
                return '
                        <a href="'. route('ferry_schedule_types.edit',$schedule_type->id) .'">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>
                        <form method="post" action="'. route('ferry_schedule_types.destroy',$schedule_type->id) .'">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
            })
            ->rawColumns(['action'])
            ->make(true);

    }
}
