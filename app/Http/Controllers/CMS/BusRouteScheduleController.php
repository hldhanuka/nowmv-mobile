<?php

namespace App\Http\Controllers\CMS;

use App\BusRoute;
use App\BusRouteSchedule;
use App\Http\Requests\CMS\BusRouteScheduleCreateRequest;
use App\ScheduleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class BusRouteScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('bus_route_schedules.list')) {
            return view('pages.bus_route_schedules.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('bus_route_schedules.create')) {
            $bus_routes = BusRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $schedule_types = ScheduleType::active()->where("type", 'bus')->select("id", "name", "status")->get();
            return view('pages.bus_route_schedules.create', ["bus_routes" =>$bus_routes, "schedule_types" => $schedule_types]);
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusRouteScheduleCreateRequest $request)
    {
        $schedule_types_input = $request->input("schedule_types") != null ? $request->input("schedule_types") : [];
        $bus_route_id = $request->input("bus_route_id");
        $starting_time = $request->input("starting_time");
        $status = $request->input("status");
        $final_data = [];
        foreach ($schedule_types_input as $schedule_type){
            $final_data[] = [
                "schedule_type_id" => intval($schedule_type),
                "bus_route_id" => intval($bus_route_id),
                "starting_time" => $starting_time,
                "status" => intval($status),
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString()
            ];
        }
        $result = BusRouteSchedule::insert($final_data);
        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('bus_route_schedules.index');
        } else {
            return redirect()->route('bus_route_schedules.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('bus_route_schedules.delete')) {
            $bus_route_schedule = BusRouteSchedule::find($id);
            $result = $bus_route_schedule->delete();

            if ($result) {
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('bus_route_schedules.index');
            } else {
                return redirect()->route('bus_route_schedules.index');
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $bus_route_schedules = BusRouteSchedule::with(['scheduleType','busRoute'])->Latest()->get();

        return Datatables::of($bus_route_schedules)
            ->editColumn('bus_route', function ($bus_route_schedule) {
                return $bus_route_schedule->busRoute->name;
            })
            ->editColumn('schedule_type', function ($bus_route_schedule) {
                return $bus_route_schedule->scheduleType->name;
            })
            ->addColumn('action', function ($bus_route_schedule) {
                return '
                         <!--<a href="'. route('bus_route_schedules.edit',$bus_route_schedule->id) .'">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a> 
                         <br>
                         <br> -->
                         <form method="post" action="'. route('bus_route_schedules.destroy',$bus_route_schedule->id) .'">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['bus_route','schedule_type', 'action'])
            ->make(true);

    }
}
