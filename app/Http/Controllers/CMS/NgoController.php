<?php

namespace App\Http\Controllers\CMS;

use App\Donation;
use App\Helpers\CMSHelper;
use App\Http\Requests\CMS\NgoCreateRequest;
use App\Http\Requests\CMS\NgoUpdateRequest;
use App\MerchantUser;
use App\Ngo;
use App\NgoImage;
use App\NgoMerchant;
use App\NgoMerchantUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class NgoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ngos.list')) {
            return view('pages.ngos.index');
        } else {
            activity()->log('ERROR|VIEW|NGO List|No ngos.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ngos.create')) {
            $ngo_merchants = NgoMerchant::active()->select("id", "name", "status")->orderBy('name')->get();
            activity()->log('SUCCESS|VIEW|NGO Create'); // ACTIVITY LOG
            return view('pages.ngos.create', ["ngo_merchants" => $ngo_merchants]);
        } else {
            activity()->log('ERROR|VIEW|NGO Create|No ngos.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function store(NgoCreateRequest $request)
    {
        // custom validate
        $validator = Validator::make($request->all(),
            ['images' => 'max:3'],
            ['images.max' => 'Maximum 3 images']
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $upload_result_logo = CMSHelper::fileUpload("logo", "uploads/ngos/");
        $ngo = new Ngo();
        $ngo->title = $request->input("title");
        $ngo->ngo_merchant_id = $request->input("ngo_merchant_id");
        $ngo->type = "ngo";
        $ngo->content = $request->input("content");
        $ngo->tag = $request->input("tag");
        $ngo->max_limit = $request->input("max_limit");
        $ngo->logo = $upload_result_logo;
//        $ngo->logo = $request->input('logo_url');
        $ngo->status = $request->input("status");
        $result = $ngo->save();

        for ($index = 0; $index < count(Input::file("images")); $index++) {
            $ngo_image_path = CMSHelper::uploadMultipleFileToStorage("images", $index, "uploads/ngo_images/");
            $ngo_image = new NgoImage();
            $ngo_image->ngo_id = $ngo->id;
            $ngo_image->image = $ngo_image_path;
            $ngo_image->order = ($index + 1);
            $ngo_image->save();
        }

        if ($result) {
            activity()->performedOn($ngo)->withProperties($ngo)->log('SUCCESS|CREATE|NGO Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ngos.index');
        } else {
            activity()->performedOn($ngo)->withProperties($ngo)->log('ERROR|CREATE|NGO Create|Fail'); // ACTIVITY LOG
            return redirect()->route('ngos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View|void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('ngos.update')) {
            $ngo = Ngo::with('ngoImages')->find($id);
            $ngo_merchant_ids = NgoMerchantUser::where("user_id", auth()->user()->id)->pluck("ngo_merchant_id")->toArray();
            if (auth()->user()->hasRole("admin") || in_array($ngo->ngo_merchant_id, $ngo_merchant_ids) || auth()->user()->hasRole('custom')) {
                $ngo_merchants = NgoMerchant::active()->select("id", "name", "status")->orderBy('name')->get();
                activity()->performedOn($ngo)->withProperties($ngo)->log('SUCCESS|VIEW|NGO Edit'); // ACTIVITY LOG
                return view('pages.ngos.edit', ["ngo" => $ngo, "ngo_merchants" => $ngo_merchants]);
            } else {
                activity()->log('ERROR|VIEW|NGO Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
        } else {
            activity()->log('ERROR|VIEW|NGO Edit|No ngos.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NgoUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function update(NgoUpdateRequest $request, $id)
    {
        // Get old image count
        $old_count = $request->input("old") != null ? count($request->input("old")) : 0;
        $max_image_count = (int)(3 - $old_count);
        // custom validate
        $validator = Validator::make($request->all(),
            ['images' => 'required_without:old|max:' . $max_image_count],
            ['images.required_without' => 'Minimum 1 slider image required', 'images.max' => 'Maximum 3 slider images']
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $upload_result_logo = CMSHelper::fileUpload("logo", "uploads/ngos/");
        $ngo = Ngo::find($id);
        $ngo->title = $request->input("title");
        $ngo->ngo_merchant_id = $request->input("ngo_merchant_id");
        $ngo->content = $request->input("content");
        $ngo->tag = $request->input("tag");
        $ngo->max_limit = $request->input("max_limit");
        if ($upload_result_logo != null) {
            $ngo->logo = $upload_result_logo;
        }
//        if ($request->input('logo_url') != null) {
//        $ngo->logo = $request->input('logo_url');
//        }
        $ngo->status = $request->input("status");
        $result = $ngo->save();

        // Delete removed images
        if ($old_count > 0) {
            NgoImage::where('ngo_id', $id)->whereNotIn('id', $request->input("old"))->delete();
        }

        if (Input::hasFile("images")) {
            for ($index = 0; $index < count(Input::file("images")); $index++) {
                $ngo_image_path = CMSHelper::uploadMultipleFileToStorage("images", $index, "uploads/ngo_images/");
                $ngo_image = new NgoImage();
                $ngo_image->ngo_id = $ngo->id;
                $ngo_image->image = $ngo_image_path;
                $ngo_image->order = ($old_count + 1);
                $ngo_image->save();
                $old_count++;
            }
        }

        if ($result) {
            activity()->performedOn($ngo)->withProperties($ngo)->log('SUCCESS|UPDATE|NGO Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ngos.index');
        } else {
            activity()->performedOn($ngo)->withProperties($ngo)->log('ERROR|UPDATE|NGO Update|Fail'); // ACTIVITY LOG
            return redirect()->route('ngos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ngos.delete')) {
            $ngo = Ngo::find($id);
            $has_donation = Donation::where("ngo_id", $id)->exists();
            if ($has_donation) {
                activity()->performedOn($ngo)->withProperties($ngo)->log('ERROR|DELETE|NGO Delete|This ngo already has donations.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This ngo already has donations.');
                return redirect()->route('ngos.index');
            } else {
                $result = $ngo->delete();
                if ($result) {
                    activity()->performedOn($ngo)->withProperties($ngo)->log('SUCCESS|DELETE|NGO Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('ngos.index');
                } else {
                    activity()->performedOn($ngo)->withProperties($ngo)->log('ERROR|DELETE|NGO Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('ngos.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|NGO Delete|No ngos.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * @return DataTables|void
     * @throws \Exception
     */
    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('ngos.list')) {
            if (auth()->user()->hasRole('admin')) {
                $ngos = Ngo::with('ngoMerchant')->Latest()->get();
            } else if (auth()->user()->hasRole('ngo_merchant') || auth()->user()->hasRole('custom')) {
                $ngo_merchant_ids = NgoMerchantUser::where("user_id", auth()->user()->id)->pluck("ngo_merchant_id")->toArray();
                $ngos = Ngo::with('ngoMerchant')->whereIn('ngo_merchant_id', $ngo_merchant_ids)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($ngos)
            ->editColumn('logo', function ($ngos) {
                return '<img width="50px" src="' . $ngos->logo . '" alt="">';
            })
            ->addColumn('ngo_merchant', function ($ngos) {
                return $ngos->ngoMerchant != null ? $ngos->ngoMerchant->name : "-";
            })
            ->addColumn('action', function ($ngos) {
                $html = '';

                if (auth()->user()->hasPermissionTo('ngos.update')) {
                    $html .= '<a href="' . route('ngos.edit', $ngos->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('ngos.delete')) {
                    $html .= '<form method="post" action="' . route('ngos.destroy', $ngos->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['logo', 'ngo_merchant', 'action'])
            ->make(true);

    }
}
