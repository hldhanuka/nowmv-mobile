<?php

namespace App\Http\Controllers\CMS;

use App\Offer;
use App\OfferCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\OfferCategoryCreateRequest;
use App\Http\Requests\CMS\OfferCategoryUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class OfferCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('offer_categories.list') || auth()->user()->can('merchant_offer_category.list')) {
            $offer_categories = OfferCategory::Latest()->paginate(10);
            return view('pages.offer_categories.index', ["offer_categories" => $offer_categories]);
        } else {
            activity()->log('ERROR|VIEW|Offer Category List|No offer_categories.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('offer_categories.create')) {
            activity()->log('SUCCESS|VIEW|Offer Category Create'); // ACTIVITY LOG
            return view('pages.offer_categories.create');
        } else {
            activity()->log('ERROR|VIEW|Offer Category Create|No offer_categories.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferCategoryCreateRequest $request)
    {
        $offer_category = new OfferCategory();
        $offer_category->title = $request->input("title");
        $offer_category->status = 2;
        $offer_category->created_by = auth()->user()->id;

        $result = $offer_category->save();

        if ($result) {
            activity()->performedOn($offer_category)->withProperties($offer_category)->log('SUCCESS|CREATE|Offer Category Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('offer_categories.index');
        } else {
            activity()->performedOn($offer_category)->withProperties($offer_category)->log('ERROR|CREATE|Offer Category Create|Fail'); // ACTIVITY LOG
            return redirect()->route('offer_categories.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('offer_categories.update')) {
            $offer_category = OfferCategory::find($id);
            activity()->performedOn($offer_category)->withProperties($offer_category)->log('SUCCESS|VIEW|Offer Category Edit'); // ACTIVITY LOG
            return view('pages.offer_categories.edit', ["offer_category" => $offer_category]);
        } else {
            activity()->log('ERROR|VIEW|Offer Category Edit|No offer_categories.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OfferCategoryUpdateRequest $request, $id)
    {
        $offer_category = OfferCategory::find($id);
        $offer_category->title = $request->input("title");
        $offer_category->status = 2;
        $offer_category->created_by = auth()->user()->id;

        $result = $offer_category->save();

        if ($result) {
            activity()->performedOn($offer_category)->withProperties($offer_category)->log('SUCCESS|UPDATE|Offer Category Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('offer_categories.index');
        } else {
            activity()->performedOn($offer_category)->withProperties($offer_category)->log('ERROR|UPDATE|Offer Category Update|Fail'); // ACTIVITY LOG
            return redirect()->route('offer_categories.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('offer_categories.delete')) {
            $offer_category = OfferCategory::find($id);
            $has_offer = Offer::where("category_id", $id)->exists();
            if($has_offer){
                activity()->performedOn($offer_category)->withProperties($offer_category)->log('ERROR|DELETE|Offer Category Delete|This category already has offers.'); // ACTIVITY LOG
                connectify('error', 'Error', 'This category already has offers.');
                return redirect()->route('offer_categories.index');
            } else {
                $result = $offer_category->delete();
                if ($result) {
                    activity()->performedOn($offer_category)->withProperties($offer_category)->log('SUCCESS|DELETE|Offer Category Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('offer_categories.index');
                } else {
                    activity()->performedOn($offer_category)->withProperties($offer_category)->log('ERROR|DELETE|Offer Category Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('offer_categories.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|Offer Category Delete|No offer_categories.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('offer_categories.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $offer_category = OfferCategory::find($id);
            $offer_category->status = $status;

            if ($status == 1) {
                $offer_category->published_at = Carbon::now()->toDateTimeString();
            }
            $offer_category->authorized_by = auth()->user()->id;

            $result = $offer_category->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($offer_category)->withProperties($offer_category)->log('SUCCESS|APPROVE|Offer Category Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($offer_category)->withProperties($offer_category)->log('ERROR|APPROVE|Offer Category Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Offer Category Approve|No offer_categories.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $offer_categories = OfferCategory::Latest()->get();

        return Datatables::of($offer_categories)
            ->addColumn('created_user_email', function ($offer_category) {
                return $offer_category->createdUser != null ? $offer_category->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($offer_category) {
                return $offer_category->authorizedUser != null ? $offer_category->authorizedUser->email : "";
            })
            ->addColumn('action', function ($offer_category) {

                $html = '';

                if (auth()->user()->hasPermissionTo('offer_categories.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $offer_category->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('offer_categories.update')) {
                    $html .= '<a href="' . route('offer_categories.edit', $offer_category->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('offer_categories.delete')) {
                    $html .= '<form method="post" action="' . route('offer_categories.destroy', $offer_category->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;

            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
