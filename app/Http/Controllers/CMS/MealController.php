<?php

namespace App\Http\Controllers\CMS;

use App\FeaturedMeal;
use App\Helpers\CMSHelper;
use App\Meal;
use App\MealCategory;
use App\MealRating;
use App\MealReview;
use App\Merchant;
use App\MerchantUser;
use App\Restaurant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\MealCreateRequest;
use App\Http\Requests\CMS\MealUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('meals.list')) {
            return view('pages.meals.index');
        } else {
            activity()->log('ERROR|VIEW|Meal List|No meals.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('meals.create')) {
            $categories = MealCategory::active()->select("id", "title", "status")->orderBy('title')->get();
            $restaurants = Restaurant::active()->select("id", "title", "status")->orderBy('title')->get();
            $merchants = Merchant::active()->select("id", "name", "status")->orderBy('name')->get();
            activity()->log('SUCCESS|VIEW|Meals Create'); // ACTIVITY LOG
            return view('pages.meals.create', ["categories" => $categories, "restaurants" => $restaurants, "merchants" => $merchants]);
        } else {
            activity()->log('ERROR|VIEW|Meals Create|No meals.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MealCreateRequest $request)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/meals/");
        $meal = new Meal();
        $meal->title = $request->input("title");
        $meal->merchant_id = $request->input("merchant_id");
        $meal->price = $request->input("price");
        $meal->content = $request->input("content");
        $meal->category_id = $request->input("category_id");
        $meal->restaurant_id = $request->input("restaurant_id");
        $meal->image = $upload_result;
//        $meal->image = $request->input("image_url");
        $meal->status = 2;
        $meal->created_by = auth()->user()->id;

        $restaurant = Restaurant::find($request->input("restaurant_id"));
        $meal->merchant_id = $restaurant != null ? $restaurant->merchant_id : null;

        $result = $meal->save();

        $is_featured = $request->input("is_featured");
        if ($is_featured == 1) {
            $featured_created = FeaturedMeal::insert(["restaurant_id" => $meal->restaurant_id, "meal_id" => $meal->id, "created_by" => auth()->user()->id, "status" => 1]);
        }

        if ($result) {
            activity()->performedOn($meal)->withProperties($meal)->log('SUCCESS|CREATE|Meal Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('meals.index');
        } else {
            activity()->performedOn($meal)->withProperties($meal)->log('ERROR|CREATE|Meal Create|Fail'); // ACTIVITY LOG
            return redirect()->route('meals.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('meals.update')) {
            $meal = Meal::with('featuredMeal')->find($id);
            $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
            if (auth()->user()->hasRole("admin") || in_array($meal->merchant_id, $merchant_ids)) {
                $categories = MealCategory::where("restaurant_id", $meal->restaurant_id)->active()->select("id", "title", "status")->orderBy('title')->get();
                $restaurants = Restaurant::active()->select("id", "title", "status")->orderBy('title')->get();
                $merchants = Merchant::active()->select("id", "name", "status")->orderBy('name')->get();
                activity()->performedOn($meal)->withProperties($meal)->log('SUCCESS|VIEW|Meal Edit'); // ACTIVITY LOG
                return view('pages.meals.edit', ["meal" => $meal, "categories" => $categories, "restaurants" => $restaurants, "merchants" => $merchants]);
            } else {
                activity()->log('ERROR|VIEW|Meal Edit|Invalid role'); // ACTIVITY LOG
                return abort(403);
            }
        } else {
            activity()->log('ERROR|VIEW|Meal Edit|No meals.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MealUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/meals/");
        $meal = Meal::find($id);
        $meal->title = $request->input("title");
        $meal->merchant_id = $request->input("merchant_id");
        $meal->price = $request->input("price");
        $meal->content = $request->input("content");
        $meal->category_id = $request->input("category_id");
        $meal->restaurant_id = $request->input("restaurant_id");
        if ($upload_result != null) {
            $meal->image = $upload_result;
        }
//        if ($request->input("image_url") != null) {
//            $meal->image = $request->input("image_url");
//        }
        $meal->status = 2;
        $meal->created_by = auth()->user()->id;

        $restaurant = Restaurant::find($request->input("restaurant_id"));
        $meal->merchant_id = $restaurant != null ? $restaurant->merchant_id : null;

        $result = $meal->save();

        $is_featured = $request->input("is_featured");
        if ($is_featured == 1) {
            $featured_created = FeaturedMeal::updateOrCreate(["meal_id" => $meal->id], ["restaurant_id" => $meal->restaurant_id, "created_by" => auth()->user()->id, "status" => 1]);
        } else {
            $has_featured = FeaturedMeal::where("meal_id", $meal->id)->first();
            if ($has_featured != null) {
                $has_featured->status = 0;
                $has_featured->save();
            }
        }

        if ($result) {
            activity()->performedOn($meal)->withProperties($meal)->log('SUCCESS|UPDATE|Meal Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('meals.index');
        } else {
            activity()->performedOn($meal)->withProperties($meal)->log('ERROR|UPDATE|Meal Update|Fail'); // ACTIVITY LOG
            return redirect()->route('meals.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('meals.delete')) {
            $meal = Meal::find($id);
            MealRating::where("meal_id")->delete();
            MealReview::where("meal_id")->delete();
            FeaturedMeal::where("meal_id")->delete();
            $result = $meal->delete();
            if ($result) {
                activity()->performedOn($meal)->withProperties($meal)->log('SUCCESS|DELETE|Meal Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('meals.index');
            } else {
                activity()->performedOn($meal)->withProperties($meal)->log('ERROR|DELETE|Meal Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('meals.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Meal Delete|No meals.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('meals.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $meal = Meal::find($id);
            $meal->status = $status;

            if ($status == 1) {
                $meal->published_at = Carbon::now()->toDateTimeString();
            }
            $meal->authorized_by = auth()->user()->id;

            $result = $meal->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($meal)->withProperties($meal)->log('SUCCESS|APPROVE|Meal Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($meal)->withProperties($meal)->log('ERROR|APPROVE|Meal Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Meal Approve|No meals.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('meals.list')) {
            if (auth()->user()->hasRole('admin')) {
                $meals = Meal::with('merchant')->Latest()->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $merchant_ids = MerchantUser::where("user_id", auth()->user()->id)->pluck("merchant_id")->toArray();
                //$restaurant_ids = Restaurant::whereIn("merchant_id", $merchant_ids)->pluck("id")->toArray();
                $meals = Meal::with('merchant')->whereIn('merchant_id', $merchant_ids)->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($meals)
            ->editColumn('image', function ($meal) {
                return '<img width="50px" src="' . $meal->image . '" alt="">';
            })
            ->editColumn('restaurant_id', function ($meal) {
                return isset($meal->restaurant) ? $meal->restaurant->title : "-";
            })
            ->addColumn('merchant_id', function ($meal) {
                return isset($meal->merchant) ? $meal->merchant->name : "-";
            })
            ->addColumn('created_user_email', function ($meal) {
                return $meal->createdUser != null ? $meal->createdUser->email : "-";
            })
            ->addColumn('action', function ($meal) {
                $html = '';

                if (auth()->user()->hasPermissionTo('meals.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $meal->id . ')"><i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('meals.update')) {
                    $html .= '<a href="' . route('meals.edit', $meal->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('meals.delete')) {
                    $html .= '<form method="post" action="' . route('meals.destroy', $meal->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'merchant_id', 'action'])
            ->make(true);

    }
}
