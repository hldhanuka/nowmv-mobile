<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Meal;
use App\MealReview;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MealReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('meal_reviews.list')) {
            $meal_reviews = MealReview::with(['meal', 'restaurant'])->Latest()->paginate(10);
            return view('pages.meal_reviews.index', ["meal_reviews" => $meal_reviews]);
        } else {
            activity()->log('ERROR|VIEW|Meal Review List|No meal_reviews.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('meal_reviews.delete')) {
            $meal_review = MealReview::find($id);
            $result = $meal_review->delete();

            if ($result) {
                activity()->performedOn($meal_review)->withProperties($meal_review)->log('SUCCESS|DELETE|Meal Review Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('meal_reviews.index');
            } else {
                activity()->performedOn($meal_review)->withProperties($meal_review)->log('ERROR|DELETE|Meal Review Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('meal_reviews.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Meal Review Delete|No meal_reviews.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('meal_reviews.list')) {

            if (auth()->user()->hasRole('admin')) {
                $meal_reviews = MealReview::with(['meal', 'restaurant'])->Latest()->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $meal_ids = Meal::where("created_by", auth()->user()->id)->pluck("id")->toArray();
                $meal_reviews = MealReview::with(['meal', 'restaurant'])->whereIn('meal_id', $meal_ids)->Latest()->get();
            }
        } else {
            return abort(403);
        }

        return Datatables::of($meal_reviews)
            ->editColumn('restaurant_id', function ($meal_review) {
                return ($meal_review->restaurant != null) ? $meal_review->restaurant->title : "-";
            })
            ->editColumn('meal_id', function ($meal_review) {
                return ($meal_review->meal != null) ? $meal_review->meal->title : "-";
            })
            ->addColumn('created_user_email', function ($meal_review) {
                return ($meal_review->createdUser != null) ? $meal_review->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($meal_review) {
                return ($meal_review->authorizedUser != null) ? $meal_review->authorizedUser->email : "";
            })
            ->addColumn('action', function ($meal_review) {
                $html = '';
                if (auth()->user()->hasRole("admin") || (auth()->user()->hasPermissionTo('meal_reviews.delete'))) {
                    $html .= '<form method="post" action="' . route('meal_reviews.destroy', $meal_review->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['restaurant_id', 'meal_id', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
