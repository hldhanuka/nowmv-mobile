<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Recipe;
use App\RecipeReview;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RecipeReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('recipe_reviews.list')) {
            $recipe_reviews = RecipeReview::with('recipe')->latest()->paginate(10);
            return view('.pages.recipe_reviews.index', ["recipe_reviews" => $recipe_reviews]);
        } else {
            activity()->log('ERROR|VIEW|Recipe Review List|No recipe_reviews.list permission'); // ACTIVITY LOG
            return abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('recipe_reviews.delete')) {
            $recipe_review = RecipeReview::find($id);
            $result = $recipe_review->delete();

            if ($result) {
                activity()->performedOn($recipe_review)->withProperties($recipe_review)->log('SUCCESS|DELETE|Recipe Review Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('recipe_reviews.index');
            } else {
                activity()->performedOn($recipe_review)->withProperties($recipe_review)->log('ERROR|DELETE|Recipe Review Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('recipe_reviews.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Recipe Review Delete|No recipe_reviews.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('recipe_reviews.list')) {
            if (auth()->user()->hasRole('admin')) {
                $recipe_reviews = RecipeReview::with('recipe')->Latest()->get();
            } else if (auth()->user()->hasRole('merchant') || auth()->user()->hasRole('custom')) {
                $recipe_ids = Recipe::where("created_by", auth()->user()->id)->pluck("id")->toArray();
                $recipe_reviews = RecipeReview::with('recipe')->whereIn('recipe_id', $recipe_ids)->Latest()->get();
            } else {
                return abort(403);
            }
        } else {
            return abort(403);
        }
        return Datatables::of($recipe_reviews)
            ->editColumn('recipe_id', function ($recipe_review) {
                return ($recipe_review->recipe != null) ? $recipe_review->recipe->title : "-";
            })
            ->addColumn('created_user_email', function ($recipe_review) {
                return ($recipe_review->createdUser != null) ? $recipe_review->createdUser->email : "-";
            })
            ->addColumn('authorized_user_email', function ($recipe_review) {
                return ($recipe_review->authorizedUser != null) ? $recipe_review->authorizedUser->email : "-";
            })
            ->addColumn('action', function ($recipe_review) {

                $html = '';
                if (auth()->user()->hasRole("admin") || auth()->user()->hasPermissionTo('recipe_previews.delete')) {
                    $html .= '<form method="post" action="' . route('recipe_reviews.destroy', $recipe_review->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }
                if ($html == '') {
                    $html .= 'No Action';
                }
                return $html;

            })
            ->rawColumns(['recipe_id', 'created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
