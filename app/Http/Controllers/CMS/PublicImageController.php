<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\PublicImageCreateRequest;
use App\PublicImage;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PublicImageController extends Controller
{
    public function index()
    {
        return view('pages.public_images.index');


    }

    public function create()
    {
        return view('pages.public_images.index');

    }

    public function store(PublicImageCreateRequest $request)
    {
        if (auth()->user()->hasPermissionTo('public_images.create')) {
            $upload_result = CMSHelper::fileUpload("image", "uploads/other_images/");

//            $upload_result = $request->input("image_url");
            $image_1 = new PublicImage();
            $image_1->url = $upload_result;
            $image_1->created_by = auth()->user()->id;
            $result = $image_1->save();

            if ($result) {
                activity()->performedOn($image_1)->withProperties($image_1)->log('SUCCESS|CREATE|News Create'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Created');
                return redirect()->route('public_images.index');
            } else {
                activity()->performedOn($image_1)->withProperties($image_1)->log('ERROR|CREATE|News Create|Fail'); // ACTIVITY LOG
                return redirect()->route('public_images.index');
            }
        } else {
            return abort(403);
        }
    }

    public function destroy(Request $request)
    {
        if (auth()->user()->hasPermissionTo('public_images.delete')) {
            $id = $request['id'];
            $image_1 = PublicImage::find($id);
            $result = $image_1->delete();
            if ($result) {
                activity()->performedOn($image_1)->withProperties($image_1)->log('SUCCESS|DELETE|News Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('public_images.index');
            } else {
                activity()->performedOn($image_1)->withProperties($image_1)->log('ERROR|DELETE|News Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('public_images.index');
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        if (auth()->user()->hasPermissionTo('public_images.list')) {
            if (auth()->user()->hasRole('admin')) {
                $images = PublicImage::get();
            } else {
                $images = PublicImage::where('created_by', auth()->user()->id)->get();
            }
        } else {
            return abort(403);
        }
        return Datatables::of($images)
            ->editColumn('image', function ($image_1) {
                return '<img width="50px" src="' . $image_1->url . '" alt="">';
            })
            ->addColumn('created_user_email', function ($image_1) {
                return $image_1->createdUser != null ? $image_1->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($image_1) {
                return $image_1->authorizedUser != null ? $image_1->authorizedUser->email : "";
            })
            ->addColumn('action', function ($image_1) {

                $html = '';
                $html .= '<button class="btn btn-warning" title="Copy" onclick="copyToClipboard(\'' . $image_1->url . '\')"><i class="far fa-copy"></i></button><br><br>';

                if (auth()->user()->id === $image_1->created_by) {
                    $html .= '<form method="post" action="' . route('public_images.destroy', ['id' => $image_1->id]) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }

                return $html;
            })
            ->rawColumns(['image', 'created_user_email', 'action'])
            ->make(true);


    }
}
