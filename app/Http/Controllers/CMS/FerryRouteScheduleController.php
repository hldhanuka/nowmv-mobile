<?php

namespace App\Http\Controllers\CMS;

use App\FerryRoute;
use App\FerryRouteSchedule;
use App\Http\Requests\CMS\FerryRouteScheduleCreateRequest;
use App\ScheduleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class FerryRouteScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ferry_route_schedules.list')) {
            return view('pages.ferry_route_schedules.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ferry_route_schedules.create')) {
            $ferry_routes = FerryRoute::active()->select("id", "name", "status")->orderBy('name')->get();
            $schedule_types = ScheduleType::active()->where("type", 'ferry')->select("id", "name", "status")->get();
            return view('pages.ferry_route_schedules.create', ["ferry_routes" =>$ferry_routes, "schedule_types" => $schedule_types]);
        } else {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FerryRouteScheduleCreateRequest $request)
    {
        $schedule_types_input = $request->input("schedule_types") != null ? $request->input("schedule_types") : [];
        $ferry_route_id = $request->input("ferry_route_id");
        $starting_time = $request->input("starting_time");
        $status = $request->input("status");
        $final_data = [];
        foreach ($schedule_types_input as $schedule_type){
            $final_data[] = [
                "schedule_type_id" => intval($schedule_type),
                "ferry_route_id" => intval($ferry_route_id),
                "starting_time" => $starting_time,
                "status" => intval($status),
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString()
            ];
        }
        $result = FerryRouteSchedule::insert($final_data);
        if ($result) {
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ferry_route_schedules.index');
        } else {
            return redirect()->route('ferry_route_schedules.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ferry_route_schedules.delete')) {
            $ferry_route_schedule = FerryRouteSchedule::find($id);


            $result = $ferry_route_schedule->delete();

            if ($result) {
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('ferry_route_schedules.index');
            } else {
                return redirect()->route('ferry_route_schedules.index');
            }
        } else {
            return abort(403);
        }
    }

    public function loadData()
    {
        $ferry_route_schedules = FerryRouteSchedule::with(['scheduleType','ferryRoute'])->Latest()->get();

        return Datatables::of($ferry_route_schedules)
            ->editColumn('ferry_route', function ($ferry_route_schedule) {
                return $ferry_route_schedule->ferryRoute->name;
            })
            ->editColumn('schedule_type', function ($ferry_route_schedule) {
                return $ferry_route_schedule->scheduleType->name;
            })
            ->addColumn('action', function ($ferry_route_schedule) {
                return '
                         <!--<a href="'. route('ferry_route_schedules.edit',$ferry_route_schedule->id) .'">
                         <button class="btn btn-default"title="edit"><i class="fas fa-edit"></i></button>
                         </a> 
                         <br>
                         <br> -->
                         <form method="post" action="'. route('ferry_route_schedules.destroy',$ferry_route_schedule->id) .'">
                         <input type="hidden" name="_token" value="' . csrf_token() . '">
                         <input type="hidden" name="_method" value="DELETE">
                         <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                         </form>';
            })
            ->rawColumns(['ferry_route','schedule_type', 'action'])
            ->make(true);

    }
}
