<?php

namespace App\Http\Controllers\CMS;

use App\Http\Requests\CMS\SettingUpdateRequest;
use App\Rules\ValidVersionInput;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SettingController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasPermissionTo('settings.update')) {
            $app_settings = Setting::where("title", "app_settings")->first()->text;
            $app_settings = json_decode($app_settings, true);
            $ad_positions = Setting::where("title", "ad_position")->first()->text;
            $ad_positions = json_decode($ad_positions, true);
            $radius = Setting::where("title", "radius")->first()->text;
            $greeting = Setting::where("title", "greeting")->first()->text;
            $is_fasting_enable = Setting::where("title", "is_fasting_enable")->first()->text;
            $fasting_start_date = Setting::where("title", "fasting_start_date")->first()->text;
            $fasting_end_date = Setting::where("title", "fasting_end_date")->first()->text;
            $reset_password = Setting::where("title", "reset_password")->first()->text;
            $android_dhiraagu_pay_url = Setting::where("title", "android_dhiraagu_pay_url")->first()->text;
            $ios_dhiraagu_pay_url = Setting::where("title", "ios_dhiraagu_pay_url")->first()->text;

            return view('pages.settings.index', ["app_settings" => $app_settings, "radius" => $radius,
                "greeting" => $greeting, "is_fasting_enable" => $is_fasting_enable, "fasting_start_date" => $fasting_start_date, "fasting_end_date" => $fasting_end_date,
                "reset_password" => $reset_password, "ad_position" => $ad_positions, "android_dhiraagu_pay_url" => $android_dhiraagu_pay_url, "ios_dhiraagu_pay_url" => $ios_dhiraagu_pay_url]);
        } else {
            return abort(403);
        }

    }

    public function update(SettingUpdateRequest $request)
    {
        if (auth()->user()->hasPermissionTo('settings.update')) {

            $validator = Validator::make($request->all(), [
                "ios_new_version" => [new ValidVersionInput()],
                "android_new_version" => [new ValidVersionInput()],

            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $request_app_settings = [
                "ios" => [
                    "new_version" => $request->input("ios_new_version"),
                    "update_required" => intval($request->input("ios_update_required"))
                ],
                "android" => [
                    "new_version" => $request->input("android_new_version"),
                    "update_required" => intval($request->input("android_update_required"))
                ],
            ];

            $app_settings = Setting::where("title", "app_settings")->first();
            $app_settings->text = json_encode($request_app_settings);
            $app_settings->save();

            $radius = Setting::where("title", "radius")->first();
            $radius->text = $request->input("radius");
            $radius->save();

            $android_dhiraagu_pay_url = Setting::where("title", "android_dhiraagu_pay_url")->first();
            $android_dhiraagu_pay_url->text = $request->input("android_dhiraagu_pay_url");
            $android_dhiraagu_pay_url->save();

            $ios_dhiraagu_pay_url = Setting::where("title", "ios_dhiraagu_pay_url")->first();
            $ios_dhiraagu_pay_url->text = $request->input("ios_dhiraagu_pay_url");
            $ios_dhiraagu_pay_url->save();

            $greeting = Setting::where("title", "greeting")->first();
            $greeting->text = $request->input("greeting");
            $greeting->save();

            $is_fasting_enable = Setting::where("title", "is_fasting_enable")->first();
            $is_fasting_enable->text = intval($request->input("is_fasting_enable"));
            $is_fasting_enable->save();

            $fasting_start_date = Setting::where("title", "fasting_start_date")->first();
            $fasting_start_date->text = Carbon::parse($request->input("fasting_start_date"))->toDateString();
            $fasting_start_date->save();

            $fasting_end_date = Setting::where("title", "fasting_end_date")->first();
            $fasting_end_date->text = Carbon::parse($request->input("fasting_end_date"))->toDateString();
            $fasting_end_date->save();

            $request_ad_position = [
                "donation_position" => intval($request->input("donation_position")),
                "food_position" => intval($request->input("food_position")),
                "news_position" => intval($request->input("news_position")),
                "offer_position" => intval($request->input("offer_position"))
            ];

            $ad_position = Setting::where("title", "ad_position")->first();
            $ad_position->text = json_encode($request_ad_position);
            $ad_position->save();

            $reset_password = Setting::where("title", "reset_password")->first();
            $reset_password->text = $request->input("reset_password");
            $reset_password->save();

            activity()->withProperties($request->input())->log('SUCCESS|UPDATE|Settings'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('settings.index');
        } else {
            return abort(403);
        }

    }

}
