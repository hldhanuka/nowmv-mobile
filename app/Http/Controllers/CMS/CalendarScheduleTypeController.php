<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\CalendarScheduleTypeCreateRequest;
use App\Http\Requests\CMS\CalendarScheduleTypeUpdateRequest;
use App\ScheduleType;
use Yajra\DataTables\DataTables;

class CalendarScheduleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('schedule_types.list')) {
            return view('pages.calendar_schedule_types.index');
        } else {
            activity()->log('ERROR|VIEW|Calendar Schedule Type List|No schedule_types.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('schedule_types.create')) {
            activity()->log('SUCCESS|VIEW|Calendar Schedule Type Create'); // ACTIVITY LOG
            return view('pages.calendar_schedule_types.create');
        } else {
            activity()->log('ERROR|VIEW|Calendar Schedule Type Create|No schedule_types.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CalendarScheduleTypeCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CalendarScheduleTypeCreateRequest $request)
    {
        //for enable notification option
        $date_title_notification_array = CMSHelper::createDateTitleNotificationArray($request->input('date'), $request->input('title'), $request->input('notification'));

        $schedule_type = new ScheduleType();
        $schedule_type->name = $request->input('name');
        $schedule_type->type = 'calendar';
        $schedule_type->priority = $request->input('priority');
        $schedule_type->date = json_encode($date_title_notification_array);
        $schedule_type->status = 1;
        $result = $schedule_type->save();

        if ($result) {
            activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('SUCCESS|CREATE|Calendar Schedule Type Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('calendar_schedule_types.index');
        } else {
            activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('ERROR|CREATE|Calendar Schedule Type Create|Fail'); // ACTIVITY LOG
            return redirect()->route('calendar_schedule_types.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('schedule_types.update')) {
            $schedule_type = ScheduleType::find($id);
            activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('SUCCESS|VIEW|Calendar Schedule Type Edit'); // ACTIVITY LOG
            return view('pages.calendar_schedule_types.edit', ["schedule_type" => $schedule_type]);
        } else {
            activity()->log('ERROR|VIEW|Calendar Schedule Type Edit|No schedule_types.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CalendarScheduleTypeUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CalendarScheduleTypeUpdateRequest $request, $id)
    {
        //for enable notification option
        $date_title_notification_array = CMSHelper::createDateTitleNotificationArray($request->input('date'), $request->input('title'), $request->input('notification'));
        $schedule_type = ScheduleType::find($id);
        $schedule_type->name = $request->input('name');
        $schedule_type->priority = $request->input('priority');
        $schedule_type->date = json_encode($date_title_notification_array);
        $schedule_type->status = 1;
        $result = $schedule_type->save();

        if ($result) {
            activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('SUCCESS|UPDATE|Calendar Schedule Type Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('calendar_schedule_types.index');
        } else {
            activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('ERROR|UPDATE|Calendar Schedule Type Update|Fail'); // ACTIVITY LOG
            return redirect()->route('calendar_schedule_types.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('schedule_types.delete')) {
            $schedule_type = ScheduleType::find($id);
            $result = $schedule_type->delete();
            if ($result) {
                activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('SUCCESS|DELETE|Calendar Schedule Type Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('calendar_schedule_types.index');
            } else {
                activity()->performedOn($schedule_type)->withProperties($schedule_type)->log('ERROR|DELETE|Calendar Schedule Type Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('calendar_schedule_types.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Calendar Schedule Type Delete|No schedule_types.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function loadData()
    {
        $schedule_types = ScheduleType::calendarSchedule()->get();

        return Datatables::of($schedule_types)
            ->addColumn('action', function ($schedule_type) {
                $html = '';

                if (auth()->user()->hasPermissionTo('schedule_types.update')) {
                    $html .= '<a href="' . route('calendar_schedule_types.edit', $schedule_type->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('schedule_types.delete')) {
                    $html .= '<form method="post" action="' . route('calendar_schedule_types.destroy', $schedule_type->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['action'])
            ->make(true);

    }
}
