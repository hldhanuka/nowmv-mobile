<?php

namespace App\Http\Controllers\CMS;

use App\Helpers\CMSHelper;
use Gate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CMS\ProfileRequest;
use App\Http\Requests\CMS\PasswordRequest;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $user = auth()->user();
        return view('profile.edit',["user"=>$user]);
    }

    /**
     * Update the profile
     *
     * @param \App\Http\Requests\ProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());
        connectify('success', 'Success', 'Profile successfully updated.');
        return back();
    }

    /**
     * Change the password
     *
     * @param \App\Http\Requests\PasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => bcrypt($request->get('password'))]);
        connectify('success', 'Success', 'Password successfully updated.');
        return back();
    }

    public function profileImageUpdate(Request $request){

        $user = User::find(auth()->user()->id);
        $upload_result = CMSHelper::fileUpload("image", "uploads/profile/");
        $user->image = $upload_result;
        $user->save();

        connectify('success', 'Success', 'Image successfully updated.');
        return back();
    }
}
