<?php

namespace App\Http\Controllers\CMS;

use App\Ad;
use App\Event;
use App\EventInvitee;
use App\EventMessage;
use App\EventTag;
use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\EventCreateRequest;
use App\Http\Requests\CMS\EventUpdateRequest;
use App\PushNotification;
use App\Rules\EventTimeRestriction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Validator;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('events.list')) {
            return view('pages.events.index');
        } else {
            activity()->log('ERROR|VIEW|Events List|No events.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('events.create')) {
            $tags = EventTag::active()->select("id", "title", "status")->orderBy('title')->get();
            activity()->log('SUCCESS|VIEW|Event Create'); // ACTIVITY LOG
            return view('pages.events.create', ["tags" => $tags]);
        } else {
            activity()->log('ERROR|VIEW|Event Create|No events.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EventCreateRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EventCreateRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'time' => new EventTimeRestriction()
        ]);

        if ($validator->fails()) {
            return redirect()->route('events.create')
                ->withErrors($validator)
                ->withInput();
        }

        $upload_result = CMSHelper::fileUpload("image", "uploads/events/");
        $event = new Event();
        $event->name = $request->input("name");
        $event->date = Carbon::parse($request->input("date"))->toDateString();
        $event->time = Carbon::parse($request->input("time"))->toTimeString();
        $event->location_name = $request->input("location_name");
        $event->longitude = $request->input("longitude");
        $event->latitude = $request->input("latitude");
        $event->description = $request->input("description");
        $event->type = "public";
        $event->tag_id = $request->input("tag_id");
        $event->image = $upload_result;
//        $event->image = $request->input("image_url");
        $event->code = APIHelper::generateRandomCode();
        $event->status = 2;
        $event->created_by = auth()->user()->id;

        if ($event->status==1){
            $event->published_at = Carbon::now()->toDateTimeString();
        }
        if ($event->status==1){
            $event->authorized_by = auth()->user()->id;
        }
        $result = $event->save();

        if ($result) {
            activity()->performedOn($event)->withProperties($event)->log('SUCCESS|CREATE|Event Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('events.index');
        } else {
            activity()->performedOn($event)->withProperties($event)->log('ERROR|CREATE|Event Create|Fail'); // ACTIVITY LOG
            return redirect()->route('events.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return void
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|void
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('events.update')) {
            $event = Event::find($id);
            $tags = EventTag::active()->select("id", "title", "status")->orderBy('title')->get();
            activity()->performedOn($event)->withProperties($event)->log('SUCCESS|VIEW|Event Edit'); // ACTIVITY LOG
            return view('pages.events.edit', ["event" => $event, "tags" => $tags]);
        } else {
            activity()->log('ERROR|VIEW|Event Edit|No events.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EventUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EventUpdateRequest $request, $id)
    {
        $upload_result = CMSHelper::fileUpload("image", "uploads/events/");
        $event = Event::find($id);
        $event->name = $request->input("name");
        $event->date = Carbon::parse($request->input("date"))->toDateString();
        $event->time = Carbon::parse($request->input("time"))->toTimeString();
        $event->location_name = $request->input("location_name");
        $event->longitude = $request->input("longitude");
        $event->latitude = $request->input("latitude");
        $event->description = $request->input("description");
        $event->tag_id = $request->input("tag_id");
        if($upload_result!=null){
            $event->image = $upload_result;
        }
//        if($request->input("image_url")!=null){
//            $event->image = $request->input("image_url");
//        }
        $event->status = 2;
        $event->created_by = auth()->user()->id;

        if ($event->status==1){
            $event->published_at = Carbon::now()->toDateTimeString();
        }
        if ($event->status==1){
            $event->authorized_by = auth()->user()->id;
        }
        $result = $event->save();

        if ($result) {
            activity()->performedOn($event)->withProperties($event)->log('SUCCESS|UPDATE|Event Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('events.index');
        } else {
            activity()->performedOn($event)->withProperties($event)->log('ERROR|UPDATE|Event Update|Fail'); // ACTIVITY LOG
            return redirect()->route('events.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('events.delete')) {
            $event = Event::find($id);
            EventInvitee::where("event_id", $id)->delete();
            EventMessage::where("event_id", $id)->delete();
            PushNotification::where(["type"=>"event_invitation", "relation_id"=>$id])->delete();
            $result = $event->delete();
            if ($result) {
                activity()->performedOn($event)->withProperties($event)->log('SUCCESS|DELETE|Event Delete'); // ACTIVITY LOG
                connectify('success', 'Success', 'Successfully Deleted');
                return redirect()->route('events.index');
            } else {
                activity()->performedOn($event)->withProperties($event)->log('ERROR|DELETE|Event Delete|Fail'); // ACTIVITY LOG
                return redirect()->route('events.index');
            }
        } else {
            activity()->log('ERROR|DELETE|Event Delete|No events.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('events.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $event = Event::find($id);
            $event->status = $status;

            if ($status == 1) {
                $event->published_at = Carbon::now()->toDateTimeString();
            }
            $event->authorized_by = auth()->user()->id;

            $result = $event->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($event)->withProperties($event)->log('SUCCESS|APPROVE|Event Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status change Error",
                    "data" => null,

                ];
                activity()->performedOn($event)->withProperties($event)->log('ERROR|APPROVE|Event Approve|'.$status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|Event Approve|No events.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * @throws \Exception
     */
    public function loadData()
    {
        if(auth()->user()->hasPermissionTo('events.list')) {
            if (auth()->user()->hasRole('admin')) {
                $events = Event::where('type', "public")->get();
            } else {
                $events = Event::where('type', "public")->where('created_by', auth()->user()->id)->get();
            }
        }else{
            return abort(403);
        }
        return Datatables::of($events)
            ->editColumn('image', function ($event) {
                return '<img width="50px" src="'. $event->image .'" alt="">';
            })
            ->addColumn('created_user_email', function ($event) {
                return $event->createdUser!=null?$event->createdUser->email:"";
            })
            ->addColumn('authorized_user_email', function ($event) {
                return $event->authorizedUser!=null?$event->authorizedUser->email:"";
            })
            ->addColumn('action', function ($event) {
                $html = '';

                if (auth()->user()->hasPermissionTo('events.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus('. $event->id .')">
                                <i class="far fa-check-square"></i></button><br><br>';
                }

                if (auth()->user()->hasPermissionTo('events.update') || auth()->user()->hasPermissionTo('events.update')) {
                    $html .= '<a href="'. route('events.edit',$event->id) .'">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a><br><br>';
                }

                if (auth()->user()->hasPermissionTo('events.delete') || auth()->user()->hasPermissionTo('events.delete')) {
                    $html .= '<form method="post" action="'. route('events.destroy',$event->id) .'">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()">
                        <i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['image','created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
