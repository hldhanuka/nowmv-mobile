<?php

namespace App\Http\Controllers\CMS;

use App\Donation;
use App\Merchant;
use App\Ngo;
use App\NgoMerchant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\NgoMerchantCreateRequest;
use App\Http\Requests\CMS\NgoMerchantUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class NgoMerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('ngo_merchants.list')) {
            $ngo_merchants = NgoMerchant::latest()->paginate(10);
            return view('pages.ngo_merchants.index', ["ngo_merchants" => $ngo_merchants]);
        } else {
            activity()->log('ERROR|VIEW|Interests List|No ngo_merchants.list permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('ngo_merchants.create')) {
            activity()->log('SUCCESS|VIEW|NGO Merchant Create'); // ACTIVITY LOG
            return view('pages.ngo_merchants.create');
        } else {
            activity()->log('ERROR|VIEW|NGO Merchant Create|No ngo_merchants.create permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NgoMerchantCreateRequest $request)
    {
        $merchant = new NgoMerchant();
        $merchant->name = $request->input('name');
        $merchant->key = $request->input('key');
        $merchant->status = 2;
        $merchant->created_by = auth()->user()->id;
        $result = $merchant->save();

        if ($result) {
            activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|CREATE|NGO Merchant Create'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Created');
            return redirect()->route('ngo_merchants.index');
        } else {
            activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|CREATE|NGO Merchant Create|Fail'); // ACTIVITY LOG
            return redirect()->route('ngo_merchants.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasPermissionTo('ngo_merchants.update')) {
            $ngo_merchant = NgoMerchant::find($id);
            activity()->performedOn($ngo_merchant)->withProperties($ngo_merchant)->log('SUCCESS|VIEW|NGO Merchant Edit'); // ACTIVITY LOG
            return view('pages.ngo_merchants.edit', ["ngo_merchant" => $ngo_merchant]);
        } else {
            activity()->log('ERROR|VIEW|NGO Merchant Edit|No ngo_merchants.update permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NgoMerchantUpdateRequest $request, $id)
    {
        $ngo_merchant = NgoMerchant::find($id);
        $ngo_merchant->name = $request->input('name');
        $ngo_merchant->key = $request->input('key');
        $ngo_merchant->status = 2;
        $ngo_merchant->created_by = auth()->user()->id;
        $result = $ngo_merchant->save();

        if ($result) {
            activity()->performedOn($ngo_merchant)->withProperties($ngo_merchant)->log('SUCCESS|UPDATE|NGO Merchant Update'); // ACTIVITY LOG
            connectify('success', 'Success', 'Successfully Updated');
            return redirect()->route('ngo_merchants.index');
        } else {
            activity()->performedOn($ngo_merchant)->withProperties($ngo_merchant)->log('ERROR|UPDATE|NGO Merchant Update|Fail'); // ACTIVITY LOG
            return redirect()->route('ngo_merchants.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->hasPermissionTo('ngo_merchants.delete')) {
            $merchant = NgoMerchant::find($id);
            $has_ngo = Ngo::where("ngo_merchant_id", $id)->exists();
            if ($has_ngo) {
                connectify('error', 'Error', 'This merchant already has assigned NGO.');
                return redirect()->route('ngo_merchants.index');
            } else {
                $result = $merchant->delete();
                if ($result) {
                    activity()->performedOn($merchant)->withProperties($merchant)->log('SUCCESS|DELETE|NGO Merchant Delete'); // ACTIVITY LOG
                    connectify('success', 'Success', 'Successfully Deleted');
                    return redirect()->route('ngo_merchants.index');
                } else {
                    activity()->performedOn($merchant)->withProperties($merchant)->log('ERROR|DELETE|NGO Merchant Delete|Fail'); // ACTIVITY LOG
                    return redirect()->route('ngo_merchants.index');
                }
            }
        } else {
            activity()->log('ERROR|DELETE|NGO Merchant Delete|No ngo_merchants.delete permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function changeStatus(Request $request)
    {
        if (auth()->user()->hasPermissionTo('ngo_merchants.approve')) {
            $id = $request->input("id");
            $status = $request->input("status");
            $ngo_merchant = NgoMerchant::find($id);
            $ngo_merchant->status = $status;

            if ($status == 1) {
                $ngo_merchant->published_at = Carbon::now()->toDateTimeString();
            }
            $ngo_merchant->authorized_by = auth()->user()->id;

            $result = $ngo_merchant->save();

            if ($result) {
                $response = [
                    "success" => true,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($ngo_merchant)->withProperties($ngo_merchant)->log('SUCCESS|APPROVE|NGO Merchant Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => "Status changed",
                    "data" => null,

                ];
                activity()->performedOn($ngo_merchant)->withProperties($ngo_merchant)->log('ERROR|APPROVE|NGO Merchant Approve|' . $status); // ACTIVITY LOG
                return response()->json($response);
            }
        } else {
            activity()->log('ERROR|APPROVE|NGO Merchant Approve|No ngo_merchants.approve permission'); // ACTIVITY LOG
            return abort(403);
        }
    }

    public function loadData()
    {
        $ngo_merchants = NgoMerchant::Latest()->get();

        return Datatables::of($ngo_merchants)
            ->addColumn('created_user_email', function ($ngo_merchant) {
                return $ngo_merchant->createdUser != null ? $ngo_merchant->createdUser->email : "";
            })
            ->addColumn('authorized_user_email', function ($ngo_merchant) {
                return $ngo_merchant->authorizedUser != null ? $ngo_merchant->authorizedUser->email : "";
            })
            ->addColumn('action', function ($ngo_merchant) {
                $html = '';

                if (auth()->user()->hasPermissionTo('ngo_merchants.approve')) {
                    $html .= '<button class="btn btn-warning" title="approve / reject" onclick="changeStatus(' . $ngo_merchant->id . ')"><i class="far fa-check-square"></i></button>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('ngo_merchants.update')) {
                    $html .= '<a href="' . route('ngo_merchants.edit', $ngo_merchant->id) . '">
                        <button class="btn btn-default" title="edit"><i class="fas fa-edit"></i></button>
                        </a>
                        <br>
                        <br>';
                }

                if (auth()->user()->hasPermissionTo('ngo_merchants.delete')) {
                    $html .= '<form method="post" action="' . route('ngo_merchants.destroy', $ngo_merchant->id) . '">
                        <input type="hidden" name="_token" value="' . csrf_token() . '">
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="button" title="delete" onclick="deleteListViewItem()"><i class="fas fa-trash-alt"></i></button>
                        </form>';
                }

                if ($html == '') {
                    $html .= 'No action';
                }
                return $html;
            })
            ->rawColumns(['created_user_email', 'authorized_user_email', 'action'])
            ->make(true);

    }
}
