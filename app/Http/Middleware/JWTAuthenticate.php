<?php

namespace App\Http\Middleware;

use App\Helpers\APIHelper;
use App\User;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class JWTAuthenticate
{
    private $jwt;
    private $user;

    public function __construct(\Tymon\JWTAuth\JWTAuth $jwt, User $user)
    {
        $this->jwt = $jwt;
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            $this->jwt->setToken($request->bearerToken());
            $payload = $this->jwt->getPayload();
            $mobile = $payload->get('data.mobile');
            $device_id = $payload->get('data.device_id');
            $token_type = $payload->get('data.token_type');

            if ($this->jwt->check() && ($mobile != null || $mobile == 0) && $device_id != null && $token_type != null) {

                if ($token_type == "long") {
                    $user = User::where('mobile', $mobile)->notGuest()->first();
                    if ($user!=null) {
                        if (strpos($request->getUri(), 'api/register') != false) {
                            // Register call with existing mobile number
                            return APIHelper::makeAPIResponse(false, "User mobile already registered", null, 400);
                        }
                        $request->mobile = $mobile;
                        // SUCCESS PATH
                    } else {
                        if (strpos($request->getUri(), 'api/register') != false) {
                            // register call
                            if (APIHelper::formatMobile(trim($request->input('mobile'))) != $mobile) {
                                // Different Mobile Number with Token
                                return APIHelper::makeAPIResponse(false, "Invalid Mobile", null, 400);
                            }
                        } else {
                            APIHelper::specialRequestLog("User does not exist with requesting token", $request);
                            return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
                        }
                    }
                } elseif ($token_type == "short") {

                    if (!(
                        strpos($request->getUri(), 'api/verify-login') != false ||
                        strpos($request->getUri(), 'api/social-login/mobile-register') != false ||
                        strpos($request->getUri(), 'api/social-login/mobile-verify') != false ||
                        strpos($request->getUri(), 'api/social-login/otp-resend') != false
                    )) {
                        // verify-login call
                        APIHelper::specialRequestLog("Request Using Short Token to other services", $request);
                        return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
                    }

                    if (strpos($request->getUri(), 'api/social-login/mobile-register') != false) {
                        $request_mobile = APIHelper::formatMobile(trim($request->input('mobile')));
                        $user = User::where('mobile', $request_mobile)->notGuest()->first();
                        // Check mobile number exists
                        if($user!=null){
                            // Register call with existing mobile number
                            return APIHelper::makeAPIResponse(false, "User mobile already registered", null, 400);
                        }
                        // SUCCESS PATH - new mobile number
                    }
                } else {
                    return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
                }
            } else {
                APIHelper::specialRequestLog("Request Token Invalid", $request);
                return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
            }
        } catch (TokenExpiredException $e) {
            return APIHelper::makeAPIResponse(false, "Token Expired", null, 401);
        } catch (TokenInvalidException $e) {
            APIHelper::specialRequestLog("Request Token Invalid | ERROR => " . $e->getMessage(), $request);
            return APIHelper::makeAPIResponse(false, "Invalid Token", null, 401);
        } catch (JWTException $e) {
            APIHelper::specialRequestLog("Request JWTException | ERROR => " . $e->getMessage(), $request);
            return APIHelper::makeAPIResponse(false, "Token Exception", null, 400);
        } catch (\Exception $e) {
            report($e);
            APIHelper::specialRequestLog("Request JWTException | ERROR => " . $e->getMessage(), $request);
            return APIHelper::makeAPIResponse(false, "Token Exception", null, 400);
        }

        return $next($request);
    }
}
