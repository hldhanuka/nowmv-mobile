<?php

namespace App\Http\Middleware;

use App\Helpers\APIHelper;
use App\NewsAgencyUser;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class WebServiceJWTAuthenticate
{
    private $jwt;
    private $user;

    public function __construct(JWTAuth $jwt, User $user)
    {
        $this->jwt = $jwt;
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            $this->jwt->setToken($request->bearerToken());
            $payload = $this->jwt->getPayload();
            $user_id = $payload->get('sub');
            $email = $payload->get('data.email');
            $token_type = $payload->get('data.token_type');

            if ($this->jwt->check() && $email != null && $token_type != null) {

                if ($token_type == "external") {
                    $user = $this->jwt->authenticate();
                    if ($user != null) {
                        // Successfully Authenticated
                        $request->my_news_agency_ids = NewsAgencyUser::where('user_id', Auth::user()->id)->pluck('news_agency_id')->toArray();
                    } else {
                        return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
                    }
                } else {
                    return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
                }
            } else {
                APIHelper::specialRequestLog("Request Token Invalid", $request);
                return APIHelper::makeAPIResponse(false, "Token invalid", null, 401);
            }
        } catch (TokenExpiredException $e) {
            return APIHelper::makeAPIResponse(false, "Token Expired", null, 401);
        } catch (TokenInvalidException $e) {
            APIHelper::specialRequestLog("Request Token Invalid | ERROR => " . $e->getMessage(), $request);
            return APIHelper::makeAPIResponse(false, "Invalid Token", null, 401);
        } catch (JWTException $e) {
            APIHelper::specialRequestLog("Request JWTException | ERROR => " . $e->getMessage(), $request);
            return APIHelper::makeAPIResponse(false, "Token Exception", null, 400);
        } catch (\Exception $e) {
            report($e);
            APIHelper::specialRequestLog("Request JWTException | ERROR => " . $e->getMessage(), $request);
            return APIHelper::makeAPIResponse(false, "Token Exception", null, 400);
        }

        return $next($request);
    }
}
