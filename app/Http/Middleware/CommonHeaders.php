<?php

namespace App\Http\Middleware;

use App\Helpers\APIHelper;
use App\Setting;
use Closure;
use Illuminate\Support\Str;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Tymon\JWTAuth\Exceptions\JWTException;

class CommonHeaders
{
    protected $jwt;

    public function __construct(\Tymon\JWTAuth\JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client_name = $request->header('X-OAuth-Client-Name');
        $secret = $request->header('X-OAuth-Client-Secret');
        $version = $request->header('X-OAuth-Client-Version');

        $client_validate = $this->checkClientSecret($client_name, $secret, $version);

        if ($client_validate) {
            $response = $next($request);

            try {
                $payload = $this->jwt->getPayload();
                $mobile = $payload->get('data.mobile');
                if (!$mobile) {
                    $username = "XXXXXXX";
                } else {
                    $username = $mobile;
                }

            } catch (JWTException $e) {
                $username = "XXXXXXX";
            }
            $this->setAPILog("MOBILE : | " . $username . " | URL : " . (string)$request->getUri() . " | Request body : " . (string)$request->getContent() . " | Response code : " . $response->getStatusCode() . " | Response body : " . (string)$response->getContent());

            return $response;
        } else {
            return APIHelper::makeAPIResponse(false, "Invalid Headers", null, 400);
        }
    }

    public function checkClientSecret($client_name, $secret, $version)
    {
        $auth_clients = Setting::where('title', 'oauth_clients')->pluck('text')->first();
        $all_clients = collect(json_decode($auth_clients, true));
        $auth_client = $all_clients->where("name", $client_name)->where("secret", $secret)->where("version", $version)->first();
        if ($auth_client != null) {
            return true;
        }
        return false;
    }

    public static function setAPILog($log_text)
    {
        $logFile = date("Y-m-d") . '.log';
        $log = [Str::limit($log_text, 2000, "...")];
        $orderLog = new Logger("API");
        $orderLog->pushHandler(new StreamHandler(storage_path('logs/service/' . $logFile)), Logger::INFO);
        $orderLog->info('Log', $log);
    }
}
