<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\One\TwitterProvider;
use Laravel\Socialite\Two\FacebookProvider;
use League\OAuth1\Client\Server\Twitter as TwitterServer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('video_url', function ($attribute, $value, $parameters, $validator) {
            if (preg_match('/https:\/\/www\.youtube\.com\/watch\?v=[^&]+|[^&]\.mp4$/i', $value)) {
                return true;
            } else {
                return false;
            }
        });

        Validator::extend('valid_scheduled_time', function ($attribute, $value, $parameters, $validator) {
            if ((bool)$value) {
                $date_time = Carbon::parse(request()->input($parameters[0]));
                if ($date_time->copy()->sub($parameters[1])->isFuture()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        });

        Schema::defaultStringLength(191);

        //new socialite providers
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'facebook_new',
            function ($app) use ($socialite) {
                $config = $app['config']['services.facebook_new'];
                return $socialite->buildProvider(FacebookProvider::class, $config);
            }
        );

        $socialite->extend(
            'twitter_new',
            function ($app) use ($socialite) {
                $config = $app['config']['services.twitter_new'];
                return new TwitterProvider(
                    $app['request'], new TwitterServer($socialite->formatConfig($config))
                );
            }
        );

    }
}
