<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'video_content_type'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }


    public function getVideoContentTypeAttribute()
    {
        if($this->attributes['url']!=null){
            if(strpos($this->attributes['url'], 'www.youtube.com') != false){
                $type = "youtube";
            } else {
                $type = "other";
            }
        } else {
            $type = null;
        }
        return $type;
    }
}
