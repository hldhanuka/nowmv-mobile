<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNgoDonation extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'content_type'];

    public function scopeMyDonations($query)
    {
        if(APIHelper::loggedUser()!==null){
            return $query->where('user_id', APIHelper::loggedUser()->id);
        } else if(auth()->user()!=null){
            return $query->where('user_id', auth()->user()->id);
        } else {
            return [];
        }
    }

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function ngo()
    {
        return $this->belongsTo('App\Ngo', 'ngo_id', 'id');
    }

    public function getContentTypeAttribute()
    {
        return "ngo";
    }
}
