<?php

namespace App;

use App\Helpers\CMSHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use HasRoles;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'mobile', 'birthday', 'address', 'gender', 'verification_code', 'is_verified', 'status', 'prayer_location_id'];

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['status_text'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function scopeGuest($query)
    {
        return $query->where('status', 4);
    }

    public function scopeNotGuest($query)
    {
        return $query->where('status', '!=', 4);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function getGenderAttribute($value)
    {
        if ($value == null) {
            return "";
        } else {
            return $value;
        }
    }

    public function getBirthdayAttribute($value)
    {
        if ($value == null || $value == "0000-00-00") {
            return "";
        } else {
            return $value;
        }
    }

    public function getAgeAttribute()
    {
        if ($this->attributes['birthday'] == null || $this->attributes['birthday'] == "0000-00-00") {
            return null;
        } else {
            return Carbon::parse($this->attributes['birthday'])->age;
        }
    }


    public function getImageAttribute($value)
    {
        if ($value == null) {
            return asset("uploads/profile/default.jpg");
        } else {
            return $value;
        }
    }

    public function agents()
    {
        return $this->hasMany('App\NewsAgencyUser', 'user_id', 'id');
    }

    public function merchants()
    {
        return $this->hasMany('App\MerchantUser', 'user_id', 'id');
    }

    public function ngoMerchants()
    {
        return $this->hasMany('App\NgoMerchantUser', 'user_id', 'id');
    }

    public function userSetting()
    {
        return $this->hasOne('App\UserSetting', 'user_id', 'id');
    }

    public function userNotificationSettings()
    {
        return $this->hasMany('App\UserNotificationSetting', 'user_id', 'id');
    }

    public function socialAccounts()
    {
        return $this->hasMany('App\SocialAccount', 'user_id', 'id');
    }

    //added for relation of public images
    public function public_images()
    {
        return $this->hasMany('App\PublicImage', 'created_by', 'id');
    }
}
