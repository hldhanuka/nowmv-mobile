<?php

namespace App;

use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleType extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text','date_array'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function getDateArrayAttribute()
    {
        return json_decode($this->attributes['date'],true);
    }

    public function scopeBusSchedule($query)
    {
        return $query->where('type', 'bus');
    }

    public function scopeCalendarSchedule($query)
    {
        return $query->where('type', 'calendar');
    }

    public function scopeFerrySchedule($query)
    {
        return $query->where('type', 'ferry');
    }

    public function scopePriority($query)
    {
        return $query->orderBy('priority');
    }


}
