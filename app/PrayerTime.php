<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrayerTime extends Model
{
    use SoftDeletes;

    public function scopeOffset($query, $offset)
    {
        if ($offset != null) {
            return $query->skip($offset);
        } else {
            return $query;
        }
    }
}
