<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccRoute extends Model
{
    use SoftDeletes;

    public function scopeBus($query){
        return $query->whereIn('service_id', config('dhiraagu.mtcc_bus_service'));
    }

    public function scopeFerry($query){
        return $query->whereIn('service_id', config('dhiraagu.mtcc_ferry_service'));
    }

    public function routeStops(){
        return $this->hasMany('App\MtccRouteStop', 'route_id', 'id');
    }

    public function routeStopMultipoints(){
        return $this->hasMany('App\MtccRouteStopMultipoint', 'route_id', 'id');
    }

    public function originServicePoint(){
        return $this->belongsTo('App\MtccServicePoint', 'origin_service_point_id', 'id');
    }

    public function destinationServicePoint(){
        return $this->belongsTo('App\MtccServicePoint', 'destination_service_point_id', 'id');
    }


}
