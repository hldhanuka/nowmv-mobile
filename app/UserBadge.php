<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBadge extends Model
{
    use SoftDeletes;

    protected $appends = ['share_link'];

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function ngo()
    {
        return $this->belongsTo('App\Ngo', 'ngo_id', 'id');
    }

    public function donation()
    {
        return $this->belongsTo('App\Donation', 'donation_id', 'id');
    }

    public function badge()
    {
        return $this->belongsTo('App\Badge', 'badge_id', 'id');
    }

    public function getShareLinkAttribute()
    {
        if ($this->attributes['type'] == "ngo") {
            $web_link = route("share-ngo-by-badge_code", ["ngo_id" => $this->attributes['ngo_id'], "badge_code" => $this->attributes['code']]);
        } else if ($this->attributes['type'] == "campaign") {
            $web_link = route("share-donation-by-badge_code", ["donation_id" => $this->attributes['donation_id'], "badge_code" => $this->attributes['code']]);
        } else {
            $web_link = route("share-badge", ["badge_id" => $this->attributes['badge_id'], "code" => $this->attributes['code']]);
        }

        return APIHelper::createFirebaseShareLink($web_link);
    }

}
