<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtccCalendarFixedHoliday extends Model
{
    use SoftDeletes;
}
