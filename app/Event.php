<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'is_user_favourite', 'share_link', 'is_my_event', 'can_invite', 'date_time', 'has_reminder'];

    protected $hidden = ['code'];

    protected $table = 'events';
    protected $fillable = ['name', 'date', 'time', 'type', 'description', 'image', 'longitude', 'latitude', 'status', 'created_at', 'updated_at'];

    public function scopeMyEvent($query)
    {
        return $query->where('created_by', APIHelper::loggedUser()->id);
    }

    public function scopePrivate($query)
    {
        return $query->where('type', 'private');
    }

    public function scopePublic($query)
    {
        return $query->where('type', 'public');
    }

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function tag()
    {
        return $this->belongsTo('App\EventTag', 'tag_id', 'id');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function authorizedUser()
    {
        return $this->belongsTo('App\User', 'authorized_by', 'id');
    }

    public function favourites()
    {
        return $this->hasMany('App\UserFavouriteEvent', 'event_id', 'id')->active();
    }

    public function userFavouriteEvents()
    {
        return $this->hasMany('App\UserFavouriteEvent', 'event_id', 'id');
    }

    public function eventInvitees()
    {
        return $this->hasMany('App\EventInvitee', 'event_id', 'id');
    }

    public function invitedToMe()
    {
        return $this->hasMany('App\EventInvitee', 'event_id', 'id')->where("invitee_id", APIHelper::loggedUser()->id)->count();
    }

    public function eventMessages()
    {
        return $this->hasMany('App\EventMessage', 'event_id', 'id');
    }

    public function reminderNotifications()
    {
        return $this->hasMany('App\ReminderNotification', 'relation_id', 'id')->where('type', 'event');
    }

    public function myReminderNotification()
    {
        return $this->reminderNotifications()->where('user_id', APIHelper::loggedUser()->id);
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function getIsUserFavouriteAttribute()
    {
        if (APIHelper::loggedUser() !== null) {
            return $this->userFavouriteEvents()->where("user_id", APIHelper::loggedUser()->id)->count();
        } else if (auth()->user() != null) {
            return $this->userFavouriteEvents()->where("user_id", auth()->user()->id)->count();
        } else {
            return 0;
        }

    }

    public function getShareLinkAttribute()
    {
        $web_link = route("share-event", ["event_id" => $this->attributes['id'], "code" => $this->attributes['code']]);
        return APIHelper::createFirebaseShareLink($web_link);
    }

    public function getIsMyEventAttribute()
    {
        if (APIHelper::loggedUser() != null) {
            // For API
            return ($this->attributes['created_by'] == APIHelper::loggedUser()->id) || ($this->invitedToMe() > 0);
        } else if (auth()->user() != null) {
            // For CMS
            return $this->attributes['created_by'] == auth()->user()->id;
        } else {
            return false;
        }

    }

    public function getCanInviteAttribute()
    {
        if (APIHelper::loggedUser() != null) {
            // For API
            $schedule_datetime = Carbon::parse($this->attributes['date'] . " " . $this->attributes['time']);
            return ($this->attributes['created_by'] == APIHelper::loggedUser()->id) && $schedule_datetime->isFuture();
        } else if (auth()->user() != null) {
            // For CMS
            return $this->attributes['created_by'] == auth()->user()->id;
        } else {
            return false;
        }

    }

    public function unreadMessage()
    {
        $user = APIHelper::loggedUser();
        $user_id = $user->id??null;
        return $this->eventMessages()->where("from", "!=", $user_id)->where("seen",0);
    }

    public function getHasReminderAttribute()
    {
        if (APIHelper::loggedUser() != null) {
            //For API
            $reminder_count = $this->myReminderNotification()->count();
            if ($reminder_count > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDateTimeAttribute()
    {
        return $this->attributes['date'] . " " . $this->attributes['time'];

    }

}
