<?php

namespace App;

use App\Helpers\APIHelper;
use App\Helpers\CMSHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meal extends Model
{
    use SoftDeletes;

    protected $appends = ['status_text', 'average_rating'];

    //Retrieve status
    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeReject($query)
    {
        return $query->where('status', 3);
    }

    public function featuredMeal()
    {
        return $this->hasOne('App\FeaturedMeal');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'id');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Merchant', 'merchant_id', 'id');
    }

    public function getStatusTextAttribute()
    {
        return CMSHelper::getStatusText($this->attributes['status']);
    }

    public function category()
    {
        return $this->belongsTo('App\MealCategory', 'category_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('App\MealReview');
    }

    public function ratings()
    {
        return $this->hasMany('App\MealRating');
    }

    public function getAverageRatingAttribute()
    {
        return round($this->ratings()->avg('rate'), 1);
    }

    public function myReviews()
    {

        if(APIHelper::loggedUser()!==null){
            return $this->hasMany('App\MealReview')->where('created_by', APIHelper::loggedUser()->id);
        } else if(auth()->user()!=null){
            return $this->hasMany('App\MealReview')->where('created_by', auth()->user()->id);
        } else {
            return [];
        }

    }


}
