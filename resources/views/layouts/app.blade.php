<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="32x32" href="{{ asset('/assets/img/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon-32x32.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- Canonical SEO -->
    <link rel="canonical" href="https://arimaclanka.com"/>


    <!--  Social tags      -->
    <meta name="keywords" content="arimac, dashboard">
    <meta name="description" content="Dhiraagu NOW MV">
    <meta itemprop="image" content="{{ asset('/assets/img/120x120.png') }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@teamarimac">
    <meta name="twitter:title" content="[Dhiraagu - NOW MV Dashboard] by Arimac Digital">

    <meta name="twitter:description" content="Dhiraagu NOW MV">
    <meta name="twitter:creator" content="@teamarimac">
    <meta name="twitter:image" content="{{ asset('/assets/img/120x120.png') }}">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="">
    <meta property="og:title" content="Dhiraagu - NOW MV Dashboard by Arimac Digital"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{ route('welcome') }}"/>
    <meta property="og:image" content="{{ asset('/assets/img/120x120.png') }}"/>
    <meta property="og:description" content="Dhiraagu - NOW MV Dashboard by Arimac Digital"/>
    <meta property="og:site_name" content="Dhiraagu - NOW MV"/>
    <title>
        Dhiraagu - Now MV Dashboard by Arimac Digital
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/ckeditor/custom-fonts/fonts.css') }}">
    <!-- CSS Files -->
    <link href="{{ asset('assets') }}/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{ asset('assets') }}/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet"/>

    <link href="{{ asset('select2/select2.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/image-uploader.min.css') }}" rel="stylesheet"/>

    <!--   Core JS Files   -->
    <script src="{{ asset('assets') }}/js/core/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/js/core/bootstrap.min.js"></script>
    <script src="{{ asset('select2/select2.min.js') }}"></script>
    <script src="{{ asset('js/image-uploader.min.js') }}"></script>
    @notifyCss

    {{-- Data table --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script>

    {{--sweetalert2--}}
    <link rel="stylesheet" href="{{ asset('sweet_alerts/sweetalert2.min.css') }}">
    <script src="{{ asset('sweet_alerts/sweetalert2.min.js') }}"></script>

    {{--jquery min--}}
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet"/>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</head>

<body class="{{ $class ?? '' }}">
<div class="wrapper">
    @auth
        @include('layouts.page_template.auth')
    @endauth
    @guest
        @include('layouts.page_template.guest')
    @endguest
</div>
<!--   Core JS Files   -->
<script src="{{ asset('assets') }}/js/core/popper.min.js"></script>
<script src="{{ asset('assets') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="{{ asset('assets') }}/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('assets') }}/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('assets') }}/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('assets') }}/demo/demo.js"></script>
@include('notify::messages')
@notifyJs
<script src="{{ asset('js/custom.js') }}"></script>
@stack('js')
</body>

</html>
