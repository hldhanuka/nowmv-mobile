<footer class="footer">
    <div class=" container-fluid ">
        <div class="d-flex justify-content-between">
            <div>
                <a href="{{ route('about') }}">About</a>
            </div>
            <div>
                <a style="margin-left: 177px" href="{{ route('privacy-policy') }}">Privacy & Policy</a>
            </div>
            <div id="copyright">
                &copy;
                <script>
                    document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                </script>, {{__(" Designed by")}}
                <a href="https://www.arimaclanka.com" target="_blank">{{__(" Arimac Digital")}}</a>
            </div>
        </div>
    </div>
</footer>
