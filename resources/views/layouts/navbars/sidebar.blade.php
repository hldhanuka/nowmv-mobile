<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            <img id="nav-bar-logo" src="{{ asset("assets/img/120x120.png") }}" alt="logo">
            {{ __(' Dhiraagu Now MV') }}
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="@if ($activePage == 'home') active @endif">
                <a href="{{ route('home') }}">
                    <i class="now-ui-icons design_app"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
                <hr>
            </li>
            {{--<li>--}}
            {{--<a data-toggle="collapse" href="#laravelExamples">--}}
            {{--<i class="fa "></i>--}}
            {{--<p>--}}
            {{--{{ __("Laravel Examples") }}--}}
            {{--<b class="caret"></b>--}}
            {{--</p>--}}
            {{--</a>--}}
            {{--<div class="collapse show" id="laravelExamples">--}}
            {{--<ul class="nav">--}}
            {{--<li class="@if ($activePage == 'profile') active @endif">--}}
            {{--<a href="{{ route('profile.edit') }}">--}}
            {{--<i class="now-ui-icons users_single-02"></i>--}}
            {{--<p> {{ __("User Profile") }} </p>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="@if ($activePage == 'users') active @endif">--}}
            {{--<a href="{{ route('user.index') }}">--}}
            {{--<i class="now-ui-icons design_bullet-list-67"></i>--}}
            {{--<p> {{ __("User Management") }} </p>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</li>--}}
            @can('ads.list')
                <li class="@if ($activePage == 'ads') active @endif">
                    <a href="{{ route('ads.index') }}">
                        <i class="fas fa-ad"></i>
                        <p>{{ __('Ads') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('interests.list')
                <li class=" @if ($activePage == 'interest') active @endif">
                    <a href="{{ route('interests.index') }}">
                        <i class="fab fa-gratipay"></i>
                        <p>{{ __('Interests') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('ngo_merchants.list')
                <li class="@if ($activePage == 'ngo_merchant') active @endif">
                    <a href="{{ route('ngo_merchants.index') }}">
                        <i class="fas fa-user-tie"></i>
                        <p>{{ __('NGO Merchants') }}</p>
                    </a>
                </li>
            @endcan
            @can('ngos.list')
                <li class="@if ($activePage == 'ngo') active @endif">
                    <a href="{{ route('ngos.index') }}">
                        <i class="fas fa-building"></i>
                        <p>{{ __('NGO') }}</p>
                    </a>
                </li>
            @endcan
            @can('donations.list')
                <li class="@if ($activePage == 'donation') active @endif">
                    <a href="{{ route('donations.index') }}">
                        <i class="fas fa-hand-holding-usd"></i>
                        <p>{{ __('Donations') }}</p>
                    </a>
                </li>
            @endcan
            @can('badges.list')
                <li class="@if ($activePage == 'badge') active @endif">
                    <a href="{{ route('badges.index') }}">
                        <i class="fas fa-certificate"></i>
                        <p>{{ __('Badges') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('schedule_types.list')
                <li class="@if ($activePage == 'calendar_schedule_type') active @endif">
                    <a href="{{ route('calendar_schedule_types.index') }}">
                        <i class="fas fa-calendar-day"></i>
                        <p>{{ __('Calendar Schedule Types') }}</p>
                    </a>
                </li>
            @endcan
            @can('event_tags.list')
                <li class="@if ($activePage == 'event_tag') active @endif">
                    <a href="{{ route('event_tags.index') }}">
                        <i class="far fa-calendar"></i>
                        <p>{{ __('Event Tags') }}</p>
                    </a>
                </li>
            @endcan
            @can('events.list')
                <li class="@if ($activePage == 'event') active @endif">
                    <a href="{{ route('events.index') }}">
                        <i class="fas fa-calendar-check"></i>
                        <p>{{ __('Events') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('news_agencies.list')
                <li class="@if ($activePage == 'news_agency') active @endif">
                    <a href="{{ route('news_agencies.index') }}">
                        <i class="far fa-newspaper"></i>
                        <p>{{ __('News Agencies') }}</p>
                    </a>
                </li>
            @endcan
            @if(auth()->user()->can('news_categories.list') || auth()->user()->hasRole('news_agent'))
                <li class="@if ($activePage == 'news_category') active @endif">
                    <a href="{{ route('news_categories.index') }}">
                        <i class="fas fa-newspaper"></i>
                        <p>{{ __('News Categories') }}</p>
                    </a>
                </li>
            @endif
            @can('news_tags.list')
                <li class="@if ($activePage == 'news_tag') active @endif">
                    <a href="{{ route('news_tags.index') }}">
                        <i class="fas fa-tag"></i>
                        <p>{{ __('News Tags') }}</p>
                    </a>
                </li>
            @endcan
            @if(auth()->user()->can('news.list') || auth()->user()->can('agent_news.list'))
                <li class="@if ($activePage == 'news') active @endif">
                    <a href="{{ route('news.index') }}">
                        <i class="far fa-newspaper"></i>
                        <p>{{ __('News') }}</p>
                    </a>
                </li>
                <hr>
            @endif
            @can('merchants.list')
                <li class="@if ($activePage == 'merchant') active @endif">
                    <a href="{{ route('merchants.index') }}">
                        <i class="fas fa-user-tie"></i>
                        <p>{{ __('Merchants') }}</p>
                    </a>
                </li>
            @endcan
            @can('restaurants.list')
                <li class="@if ($activePage == 'restaurant') active @endif">
                    <a href="{{ route('restaurants.index') }}">
                        <i class="fas fa-utensils"></i>
                        <p>{{ __('Restaurants') }}</p>
                    </a>
                </li>
            @endcan
            @can('restaurant_reviews.list')
                <li class="@if ($activePage == 'restaurant_review') active @endif">
                    <a href="{{ route('restaurant_reviews.index') }}">
                        <i class="fas fa-carrot"></i>
                        <p>{{ __('Restaurants Reviews') }}</p>
                    </a>
                </li>
            @endcan
            @can('meal_categories.list')
                <li class="@if ($activePage == 'meal_category') active @endif">
                    <a href="{{ route('meal_categories.index') }}">
                        <i class="fas fa-hotdog"></i>
                        <p>{{ __('Meal Categories') }}</p>
                    </a>
                </li>
            @endcan
            @can('meals.list')
                <li class=" @if ($activePage == 'meal') active @endif">
                    <a href="{{ route('meals.index') }}">
                        <i class="fas fa-hamburger"></i>
                        <p>{{ __('Meals') }}</p>
                    </a>
                </li>
            @endcan
            @can('meal_reviews.list')
                <li class="@if ($activePage == 'meal_review') active @endif">
                    <a href="{{ route('meal_reviews.index') }}">
                        <i class="fas fa-cheese"></i>
                        <p>{{ __('Meal Reviews') }}</p>
                    </a>
                </li>
            @endcan
            @can('recipes.list')
                <li class="@if ($activePage == 'recipe') active @endif">
                    <a href="{{ route('recipes.index') }}">
                        <i class="fas fa-list-alt"></i>
                        <p>{{ __('Recipes') }}</p>
                    </a>
                </li>
            @endcan
            @can('recipe_reviews.list')
                <li class="@if ($activePage == 'recipe_review') active @endif">
                    <a href="{{ route('recipe_reviews.index') }}">
                        <i class="fas fa-pizza-slice"></i>
                        <p>{{ __('Recipe Reviews') }}</p>
                    </a>
                </li>
            @endcan
            @if(auth()->user()->can('offer_categories.list') || auth()->user()->can('merchant_offer_category.list'))
                <li class="@if ($activePage == 'offer_category') active @endif">
                    <a href="{{ route('offer_categories.index') }}">
                        <i class="fas fa-percentage"></i>
                        <p>{{ __('Offer Categories') }}</p>
                    </a>
                </li>
            @endcan
            @if(auth()->user()->can('offers.list') || auth()->user()->can('merchant_offer.list'))
                <li class="@if ($activePage == 'offer') active @endif">
                    <a href="{{ route('offers.index') }}">
                        <i class="fas fa-percent"></i>
                        <p>{{ __('Offers') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            {{--            @can('ferry_locations.list')--}}
            {{--                <li class="@if ($activePage == 'ferry_location') active @endif">--}}
            {{--                    <a href="{{ route('ferry_locations.index') }}">--}}
            {{--                        <i class="fas fa-anchor"></i>--}}
            {{--                        <p>{{ __('Ferry Locations') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('schedule_types.list')--}}
            {{--                <li class="@if ($activePage == 'ferry_schedule_type') active @endif">--}}
            {{--                    <a href="{{ route('ferry_schedule_types.index') }}">--}}
            {{--                        <i class="fas fa-ship"></i>--}}
            {{--                        <p>{{ __('Ferry Schedule Types') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('ferry_routes.list')--}}
            {{--                <li class="@if ($activePage == 'ferry_route') active @endif">--}}
            {{--                    <a href="{{ route('ferry_routes.index') }}">--}}
            {{--                        <i class="fas fa-water"></i>--}}
            {{--                        <p>{{ __('Ferry Routes') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('ferry_route_schedules.list')--}}
            {{--                <li class="@if ($activePage == 'ferry_route_schedule') active @endif">--}}
            {{--                    <a href="{{ route('ferry_route_schedules.index') }}">--}}
            {{--                        <i class="fas fa-anchor"></i>--}}
            {{--                        <p>{{ __('Ferry Route Schedule') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('ferry_terminal_schedules.list')--}}
            {{--                <li class="@if ($activePage == 'ferry_terminal_schedule') active @endif">--}}
            {{--                    <a href="{{ route('ferry_terminal_schedules.index') }}">--}}
            {{--                        <i class="fas fa-water"></i>--}}
            {{--                        <p>{{ __('Ferry Terminal Schedule') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--                <hr>--}}
            {{--            @endcan--}}
            {{--            @can('schedule_types.list')--}}
            {{--                <li class="@if ($activePage == 'bus_schedule_type') active @endif">--}}
            {{--                    <a href="{{ route('bus_schedule_types.index') }}">--}}
            {{--                        <i class="fas fa-bus-alt"></i>--}}
            {{--                        <p>{{ __('Bus Schedule Types') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('bus_locations.list')--}}
            {{--                <li class="@if ($activePage == 'bus_location') active @endif">--}}
            {{--                    <a href="{{ route('bus_locations.index') }}">--}}
            {{--                        <i class="fas fa-sign"></i>--}}
            {{--                        <p>{{ __('Bus Locations') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('bus_routes.list')--}}
            {{--                <li class="@if ($activePage == 'bus_route') active @endif">--}}
            {{--                    <a href="{{ route('bus_routes.index') }}">--}}
            {{--                        <i class="fas fa-road"></i>--}}
            {{--                        <p>{{ __('Bus Routes') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('bus_route_schedules.list')--}}
            {{--                <li class="@if ($activePage == 'bus_route_schedule') active @endif">--}}
            {{--                    <a href="{{ route('bus_route_schedules.index') }}">--}}
            {{--                        <i class="fas fa-bus"></i>--}}
            {{--                        <p>{{ __('Bus Route Schedule') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            @endcan--}}
            {{--            @can('bus_halt_schedules.list')--}}
            {{--                <li class="@if ($activePage == 'bus_halt_schedule') active @endif">--}}
            {{--                    <a href="{{ route('bus_halt_schedules.index') }}">--}}
            {{--                        <i class="fas fa-shuttle-van"></i>--}}
            {{--                        <p>{{ __('Bus Halt Schedule') }}</p>--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--                <hr>--}}
            {{--            @endcan--}}
            @can('prayer_tips.list')
                <li class="@if ($activePage == 'prayer_tip') active @endif">
                    <a href="{{ route('prayer_tips.index') }}">
                        <i class="fas fa-pray"></i>
                        <p>{{ __('Prayer Tips') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('zakat_payments.list')
                <li class="@if ($activePage == 'zakat_payment') active @endif">
                    <a href="{{ route('zakat_payments.index') }}">
                        <i class="fab fa-cc-amazon-pay"></i>
                        <p>{{ __('Zakat Payments') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('support_teams.list')
                <li class="@if ($activePage == 'support_team') active @endif">
                    <a href="{{ route('support_teams.index') }}">
                        <i class="fas fa-users"></i>
                        <p>{{ __('Support Team') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('videos.list')
                <li class="@if ($activePage == 'video') active @endif">
                    <a href="{{ route('videos.index') }}">
                        <i class="fas fa-video"></i>
                        <p>{{ __('Videos') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('users.list')
                <li class="@if ($activePage == 'user') active @endif">
                    <a href="{{ route('users.index') }}">
                        <i class="fas fa-user"></i>
                        <p>{{ __('User') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
        <!--public images-->
            @can('public_images.list')
            <li class="@if ($activePage == 'public_images') active @endif">
                <a href="{{ route('public_images.index') }}">
                    <i class="fa fa-image"></i>
                    <p>{{ __('Public Images') }}</p>
                </a>
                <hr>
            </li>
            @endcan

            @can('settings.update')
                <li class="@if ($activePage == 'setting') active @endif">
                    <a href="{{ route('settings.index') }}">
                        <i class="fas fa-cogs"></i>
                        <p>{{ __('Settings') }}</p>
                    </a>
                </li>
                <hr>
            @endcan
            @can('push_notifications.create')
                <li class="@if ($activePage == 'push_notifications') active @endif">
                    <a href="{{ route('push_notifications.index') }}">
                        <i class="fas fa-envelope-square"></i>
                        <p>{{ __('Push Notifications') }}</p>
                    </a>
                </li>
                <hr>
            @endcan

<!--            imi games menu-->
            <li class="">
                <a class="imi-menu">
                    <i class="fas fa-gamepad"></i>
                    <p>IMI Games</p>
                </a>
<!--                sub menus-->
                <ul class="nav sidebar-sub">
                    <li>
                        <a href="{{ route('imi_games.promo-games') }}">
                            <p>Get promo games</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{route("imi_games.reward-list")}}">
                            <p>Reward List</p>
                        </a>
                    </li>
                </ul>
            </li>
            <hr>
        </ul>
    </div>
</div>

<style>
    .nav.sidebar-sub{
        margin-left: 35px;
        display: none;
    }
    .nav.sidebar-sub.active{
        display: block;
    }
    .nav.sidebar-sub li a{
        padding: 5px 10px;
        margin: 0 !important;
        border-bottom: 1px solid #ffffff29;
        border-radius: 0;
    }
</style>

<script>
    $( ".imi-menu" ).click(function() {
        $(".sidebar-sub").toggleClass( "active" );
    });
</script>
