
<style>
    #image_crop_modal{{ $id }}    {
        max-width: 2000px !important;
    }

    #sample_cropping_img{{ $id }}    {
        display: block;
        max-width: 100%;
    }

    #preview_cropping_img{{ $id }}    {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

<link rel="stylesheet" href="{{asset('css/cropper.css')}}"/>
<script src="{{asset('js/cropper.js')}}"></script>

<img class="gal-img prev_img" id="preview_img{{ $id }}"
     src="{{$current_image??asset('assets/img/dummy.jpg')}}" width="30%">
<input type="file" class="custom-file-input" id="custom-file-input{{ $id }}" name="{{ $name }}"
>
<input type="hidden" id="custom-file-input-url{{ $id }}" name="{{ $name }}_url"
       required>
<div class="modal fade" id="image_crop_modal{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true" onclick="return false;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img class="sample_cropping_img"
                                 id="sample_cropping_img{{ $id }}" src="{{asset('assets/img/dummy.jpg')}}">
                        </div>
                        <div class="col-md-4">
                            <div id="preview_cropping_img{{ $id }}"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        id="img_crop_cancel_button{{ $id }}">Cancel
                </button>
                <button type="button" class="btn btn-primary" id="img_crop_button{{ $id }}">Crop</button>
            </div>
        </div>
    </div>
</div>
<script>

    var ratio = {{$ratio}};

    function setRatio(ratio_input) {
        ratio = ratio_input;
    }

    $(document).ready(function () {
        $(document).on("click", "#preview_img{{ $id }}", function () {
            $("#custom-file-input{{ $id }}").click();
        });

        var sample_cropping_img = document.getElementById('sample_cropping_img{{ $id }}');
        var $cropping_modal = $('#image_crop_modal{{ $id }}');
        var cropper;
        $('#custom-file-input{{ $id }}').change(function (event) {
            var files = event.target.files;
            var done = function (url) {
                sample_cropping_img.src = url;

                $cropping_modal.modal({
                    backdrop: 'static'

                });

                $cropping_modal.modal('show');
                //done because need to have the cropping for the same file uploads again (required validation is removed from this input and added only to the url)
                $("#custom-file-input{{$id}}")[0].value = '';
            };

            if (files && files.length > 0) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0])
            }
        });

        $cropping_modal.on('shown.bs.modal', function () {
            cropper = new Cropper(sample_cropping_img, {
                aspectRatio: ratio,
                viewMode: 1,
                preview: '#preview_cropping_img{{ $id }}',
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $("#img_crop_button{{ $id }}").click(function () {
            canvas = cropper.getCroppedCanvas({
                minWidth: 256,
                minHeight: 256,
                maxWidth: 4096,
                maxHeight: 4096,
                fillColor: '#fff',
                imageSmoothingEnabled: true,
                imageSmoothingQuality: 'high',
                quality : '100'

            });

            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $("#img_crop_cancel_button{{ $id }}").prop("disabled", true);
                    $("#img_crop_button{{ $id }}").html("Uploading...");
                    $("#img_crop_button{{ $id }}").prop("disabled", true);
                    $.ajax({
                        url: "{{route('image_crop')}}",
                        data: {image: base64data, upload_path: '{{$upload_path}}'},
                        method: "post",
                        success: function (result) {
                            $cropping_modal.modal('hide');
                            if (result.success) {
                                $("#preview_img{{ $id }}").attr('src', result.data);
                                $("#custom-file-input-url{{ $id }}").val(result.data);
                                Swal.fire({
                                    title: 'Success',
                                    icon: 'success',
                                    text: result.message,
                                });
                            } else {
                                Swal.fire({
                                    title: 'Error',
                                    icon: 'error',
                                    text: result.message,
                                });
                            }
                            $("#img_crop_cancel_button{{ $id }}").attr("disabled", false);
                            $("#img_crop_button{{ $id }}").attr("disabled", false);
                            $("#img_crop_button{{ $id }}").html("Crop");
                        }
                    });
                }
            },'image/jpeg', 1)
        });
    });

</script>
