@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'User Profile',
    'activePage' => 'profile',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">{{__(" Edit Profile")}}</h5>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('profile.update', $user->id) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__("First Name")}}</label>
                                        <input type="text" name="first_name" class="form-control"
                                               value="{{ old('first_name', $user->first_name) }}">
                                        @include('alerts.feedback', ['field' => 'first_name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__("Last Name")}}</label>
                                        <input type="text" name="last_name" class="form-control"
                                               value="{{ old('last_name', $user->last_name) }}">
                                        @include('alerts.feedback', ['field' => 'last_name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__("Mobile")}}</label>
                                        <input type="text" name="mobile" class="form-control"
                                               value="{{ old('mobile', $user->mobile) }}">
                                        @include('alerts.feedback', ['field' => 'mobile'])
                                    </div>
                                </div>
                            </div>
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
                                    type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet"
                                  type="text/css"/>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Birthday")}}</label>
                                        <input id="datepicker" name="birthday" class="form-control"
                                               value="{{ old('birthday', $user->birthday) }}"/>
                                        <script>
                                            $('#datepicker').datepicker({format: 'yyyy-mm-dd'});
                                        </script>
                                        {{--<input type="text" id="date" name="date" class="form-control" value="{{ old('date') }}">--}}
                                        @include('alerts.feedback', ['field' => 'birthday'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Gender")}}</label><br>
                                        <input type="radio" name="gender"
                                               value="male" {{ $user->gender=="male"?"checked":"" }}> Male
                                        <input type="radio" name="gender"
                                               value="female" {{ $user->gender=="female"?"checked":"" }}> Female
                                        @include('alerts.feedback', ['field' => 'gender'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">{{__(" Email")}}</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email"
                                               value="{{ old('email', $user->email) }}">
                                        @include('alerts.feedback', ['field' => 'email'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">{{__(" Address")}}</label>
                                        <textarea type="text" name="address"
                                                  class="form-control">{{ old('address', $user->address) }}</textarea>
                                        @include('alerts.feedback', ['field' => 'address'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Save')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                    <div class="card-header">
                        <h5 class="title">{{__("Password")}}</h5>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                            @csrf
                            @method('put')
                            @include('alerts.success', ['key' => 'password_status'])
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>{{__(" Current Password")}}</label>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="old_password" placeholder="{{ __('Current Password') }}"
                                               type="password" required>
                                        @include('alerts.feedback', ['field' => 'old_password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>{{__(" New password")}}</label>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               placeholder="{{ __('New Password') }}" type="password" name="password"
                                               required>
                                        @include('alerts.feedback', ['field' => 'password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>{{__(" Confirm New Password")}}</label>
                                        <input class="form-control" placeholder="{{ __('Confirm New Password') }}"
                                               type="password" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit"
                                        class="btn btn-primary btn-round ">{{__('Change Password')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{asset('assets')}}/img/bg5.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <form id="profile_update" method="post" action="{{ route('profile.image.update') }}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="author form-group">
                                <a href="#">
                                    <img class="avatar border-gray prev_img" id="prev_img"
                                         src="{{$user->image!=null?$user->image:asset('uploads/profile/default.jpg')}}"
                                         alt="...">
                                    <input type="file" class="custom-file-input" id="custom-file-input" name="image">
                                    <h5 class="title">{{ $user->first_name }}</h5>
                                </a>
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                        </form>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#img_btn").click(function () {
                $(".hidden-file").click();
            });
            $("#img-file").change(function () {
                let fileName = $(this).val();
                $("#img_btn").val(fileName);
            })
        });
    </script>
@endsection
