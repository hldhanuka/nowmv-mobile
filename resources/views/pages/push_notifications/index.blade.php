@extends('layouts.app', [
    'namePage' => 'Push Notifications',
    'class' => 'sidebar-mini',
    'activePage' => 'push_notifications',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Send Push Notifications To All APP Users </h4>
                    </div>
                    <div class="card-body">
                        <form id="push_notification_form" method="post"
                              action="{{ route('push_notifications.update') }}">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="content">{{__(" Message")}}</label>
                                        <textarea type="text" name="message" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('message') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'message'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="type">{{__(" Type")}}</label>
                                        <select class="form-control valid" name="type" id="type">
                                            <option value="system" {{ old("type")=="system"?"selected":"" }}>Dhiraagu</option>
                                            <option value="weather" {{ old("type")=="weather"?"selected":"" }}>Weather</option>
                                            <option value="offer" {{ old("type")=="offer"?"selected":"" }}>Offer</option>
                                            <option value="travel" {{ old("type")=="travel"?"selected":"" }}>Travel</option>
                                            <option value="tip" {{ old("type")=="tip"?"selected":"" }}>Tips</option>
                                        </select>
                                        @include('alerts.feedback', ['field' => 'type'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="redirect_type">{{__(" Notification Redirect To")}}</label>
                                        <select class="form-control valid" name="redirect_type" id="redirect_type">
                                            <option selected value>Select an option....</option>
                                            <option value="home">Home Page</option>
                                            <option value="news_home">News Feed Home Page</option>
                                            <option value="news" {{ old("redirect_type")=="news"?"selected":"" }}>News Detail (with id)</option>
                                            <option value="travel_home">Travel Type List</option>
                                            <option value="travel_bus">Bus Home Page</option>
                                            <option value="travel_ferry">Ferry Home Page</option>
                                            <option value="travel_flight">Flight Home Page</option>
                                            <option value="offer_home">Offer All Tab</option>
                                            <option value="event_home">Event Home Page</option>
                                            <option value="event" {{ old("redirect_type")=="event"?"selected":"" }}>Event Detail (with id)</option>
                                            <option value="weather_home">Weather Home Page</option>
                                            <option value="food_home">Food Home Page</option>
                                            <option value="restaurant_home">Restaurant Home Page</option>
                                            <option value="restaurant" {{ old("redirect_type")=="restaurant"?"selected":"" }}>Restaurant Detail (with id)</option>
                                            <option value="donation_home">Donation Campaign Home Page</option>
                                            <option value="donation" {{ old("redirect_type")=="donation"?"selected":"" }}>Donation Campaign Detail Page (with id)</option>
                                            <option value="ngo" {{ old("redirect_type")=="ngo"?"selected":"" }}>NGO Detail Page (with id)</option>
                                            <option value="prayer_mate">Prayer Home Page</option>
                                            <option value="notifications">Notification List View</option>
                                            <option value="profile">Profile Update Page</option>
                                            <option value="my_badge">My Badge List View</option>
                                            <option value="donation_history">Donation History List</option>
                                            <option value="notification_setting">Notification Setting List View</option>
                                            <option value="personalize_setting">Personalize Setting List View</option>
                                            <option value="faq_setting">FAQ Page</option>
                                            <option value="contact_setting">Contact Page</option>
                                            <option value="tnc_setting">TnC Page</option>
                                            <option value="video">Video List View</option>
                                        </select>
                                        @include('alerts.feedback', ['field' => 'redirect_type'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group" id="_id">
                                        <label for="id">{{__(" Id")}}</label>
                                        <input type="number" min="0" name="id" class="form-control" value="{{ old('id') }}">
                                        @include('alerts.feedback', ['field' => 'id'])
                                    </div>
                                </div>
                            </div>
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-md-3 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="for_male">{{__(" Show this offer for male users?")}}</label><br>--}}
                            {{--                                        <input type="radio" id="for_male" name="for_male" value="1" checked> Yes--}}
                            {{--                                        <input type="radio" id="for_male" name="for_male" value="0"> No--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'for_male'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-md-3 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="for_female">{{__(" Show this offer for female users?")}}</label><br>--}}
                            {{--                                        <input type="radio" name="for_female" value="1" checked> Yes--}}
                            {{--                                        <input type="radio" name="for_female" value="0"> No--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'for_female'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-md-3 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="min_age">{{__(" Minimum age(years) limit to show the offer")}}</label>--}}
                            {{--                                        <input type="number" step="1" min="0" id="min_age" name="min_age" class="form-control" value="{{ old('min_age') }}">--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'min_age'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-md-3 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="max_age">{{__(" Maximum age(years) limit to show the offer")}}</label>--}}
                            {{--                                        <input type="number" step="1" min="0" id="max_age" name="max_age" class="form-control" value="{{ old('max_age') }}">--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'max_age'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <hr class="half-rule"/>
                            <button type="button" class="btn btn-primary btn-round"
                                    onclick="confirmSendMessage()">{{__('Submit')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {
            var req_types = ['news', 'event', 'restaurant', 'donation', 'ngo'];
            $('#redirect_type').change( function () {
                if($.inArray(this.value, req_types)>=0){
                    $('#_id').show();
                } else {
                    $('#_id').hide();
                }
            })
        })

        function confirmSendMessage() {
            Swal.fire({
                title: 'Are you sure',
                text: "What do you want to send push message to all APP users?",
                icon: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    // Press Ok
                    $("#push_notification_form").submit();
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    // Press No
                }
            })
        }
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\PushNotificationRequest', '#push_notification_form') !!}
@endsection
