@extends('layouts.app', [
    'namePage' => 'Ferry Terminal Schedule',
    'class' => 'sidebar-mini',
    'activePage' => 'ferry_terminal_schedule',
  ])
@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ferry_terminal_schedules.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Ferry Terminal Schedule</h4>
                    </div>
                    <div class="card-body">
                        <form id="ferry_terminal_schedule_create" method="post" action="{{ route('ferry_terminal_schedules.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ferry_route_id">{{__(" Ferry Route")}}</label>
                                        <select class="form-control" id="ferry_route_id" name="ferry_route_id" >
                                            @foreach($ferry_routes as $ferry_route)
                                                <option value="{{$ferry_route->id}}">{{$ferry_route->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ferry_route_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="start_location_id">{{__(" Starting Location")}}</label>
                                        <select class="form-control" id="start_location_id" name="start_location_id" >
                                            @foreach($locations as $location)
                                                <option value="{{$location["island_id"]}}">{{$location["island_name"]}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'start_location_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="terminal_id">{{__(" Terminal")}}</label>
                                        <select class="form-control" id="terminal_id" name="terminal_id" >
                                            @foreach($locations as $location)
                                                <option value="{{$location["island_id"]}}">{{$location["island_name"]}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'terminal_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1"> Active
                                        <input type="radio" name="status" value="0"> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Time")}}</label>
                                        <input id="timepicker" name="time" class="form-control" value="{{ old('time') }}"/>
                                        <script>
                                            $('#timepicker').timepicker({format: 'HH:MM'});
                                        </script>
                                        {{--                                        <input type="text"  id="time" name="time" class="form-control" value="{{ old('time') }}">--}}
                                        @include('alerts.feedback', ['field' => 'time'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\FerryTerminalScheduleCreateRequest', '#ferry_terminal_schedule_create') !!}
@endsection
