@extends('layouts.app', [
    'namePage' => 'Calendar Schedule Types',
    'class' => 'sidebar-mini',
    'activePage' => 'calendar_schedule_type',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('calendar_schedule_types.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Calendar Schedule Types</h4>
                    </div>
                    <div class="card-body">
                        {{--                        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
                        {{--                        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}
                        <form id="schedule_type_update" method="post"
                              action="{{ route('calendar_schedule_types.update', $schedule_type -> id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="name">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control"
                                               value="{{ old('name', $schedule_type->name) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="priority">{{__(" Priority")}}</label>
                                        <input type="text" name="priority" class="form-control"
                                               value="{{ old('priority',$schedule_type->priority) }}">
                                        @include('alerts.feedback', ['field' => 'priority'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__(" Dates")}}</label>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center item-raw-title">Dates</th>
                                                <th class="text-center item-raw-title">Titles</th>
                                                <!--for enabling the notification-->
                                                <th class="text-center item-raw-title">Notification</th>
                                                <th class="text-center">
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="schedule_table">
                                            @php
                                                $index = 0;
                                                $count = 0;
                                            @endphp

                                            @if(old('date')==null)
                                                @foreach(json_decode($schedule_type->date, true) as $date)

                                                    <tr>
                                                        <td class="text-center">
                                                            <i class="fas fa-arrows-alt-v"></i>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                                <input name="date[]" class="form-control datepicker"
                                                                       value="{{ $date["date"] }}">
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <input class="form-control" type="text" name="title[]"
                                                                   value="{{ $date["title"] }}">
                                                        </td>
                                                        <td class="text-center">
                                                            @if(array_key_exists("notification",$date))
                                                                <input type="hidden" name="notification[{{$count}}]"
                                                                       value="0">
                                                                <input class="form-control" type="checkbox"
                                                                       name="notification[{{$count}}]"
                                                                       value="1" {{ $date["notification"] == "true" ? ' checked' : '' }}>
                                                            @else
                                                                <input type="hidden" name="notification[{{$count}}]"
                                                                       value="0">
                                                                <input class="form-control" type="checkbox"
                                                                       name="notification[{{$count}}]" value="1"
                                                                       checked>

                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            <i class="fas fa-times-circle fa-2x text-danger"
                                                               onclick="removeMyRaw(this)"></i>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $count ++;
                                                    @endphp
                                                @endforeach
                                            @else

                                                @foreach(old('date') as $item)
                                                    <tr>
                                                        <td class="text-center">
                                                            <i class="fas fa-arrows-alt-v"></i>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                                <input name="date[]" class="form-control datepicker"
                                                                       value="{{ $item }}">
                                                            </div>
                                                            @if($errors->has('date.'.$index))
                                                                <span id="name-error"
                                                                      class="help-block error-help-block">{{$errors->first('date.'.$index)}}</span>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            <input class="form-control" type="text" name="title[]"
                                                                   value="{{ old('title')[$index] }}">
                                                            @if($errors->has('title.'.$index))
                                                                <span id="name-error"
                                                                      class="help-block error-help-block">{{$errors->first('title.'.$index)}}</span>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if (old('notification')[$index] == "1")
                                                                <input type="hidden" name="notification[{{$index}}]"
                                                                       value="0">
                                                                <input class="form-control" type="checkbox"
                                                                       name="notification[{{$index}}]" value="1"
                                                                       checked>

                                                            @else
                                                                <input type="hidden" name="notification[{{$index}}]"
                                                                       value="0">
                                                                <input class="form-control" type="checkbox"
                                                                       name="notification[{{$index}}]" value="1">
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            <i class="fas fa-times-circle fa-2x text-danger"
                                                               onclick="removeMyRaw(this)"></i>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $index++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        @include('alerts.feedback', ['field' => 'date'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <div class="row text-center">
                                    <div class="btn btn-primary ml-3 mb-3"
                                         onclick="addNewItemRaw('date','schedule_table', {{$count}})">
                                        <i class="fas fa-plus"></i>
                                        Add new
                                    </div>
                                    {{--                                    <p class="text-center ml-3">Add New</p>--}}
                                </div>
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#schedule_table").sortable();
            $("#schedule_table").disableSelection();
            $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
        });

        $('body').on('focus', ".datepicker", function () {
            $(this).datepicker({dateFormat: 'yy-mm-dd'});
        });

        function addNewItemRaw(a, b, count) {
            var counter = count;
            var string = ' name="notification[' + counter + ']"';
            let html = `<tr>
        <td class="text-center"><i class="fas fa-arrows-alt-v"></i></td>
        <td class="text-center">
            <div class="form-group">
                <input name="date[]" class="form-control datepicker">
            </div>
        </td>
        <td class="text-center"><input class="form-control" type="text" name="title[]"></td>
        <input type="hidden" ` + string + ` value="0">
        <td class="text-center"><input ` + string + ` class="form-control" type="checkbox" name="notification[]" value="1" checked></td>
        <td class="text-center"><i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i></td>
        </tr>`;
            $("#schedule_table").append(html);
            document.querySelector(".main-panel").scrollTo(0, document.querySelector(".card-body").scrollHeight);
        }

        function removeMyRaw(item) {
            $(item).closest("tr").remove();
        }
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\CalendarScheduleTypeUpdateRequest', '#schedule_type_update') !!}
@endsection
