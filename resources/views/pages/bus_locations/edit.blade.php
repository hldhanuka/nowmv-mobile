@extends('layouts.app', [
    'namePage' => 'Bus Locations',
    'class' => 'sidebar-mini',
    'activePage' => 'bus_location',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('bus_locations.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Bus Location</h4>
                    </div>
                    <div class="card-body">
                        <form id="bus_location_update" method="post" action="{{ route('bus_locations.update', $bus_location->id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name', $bus_location->name) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1" {{$bus_location->status==1?"checked":""}}> Active
                                        <input type="radio" name="status" value="0" {{$bus_location->status==0?"checked":""}}> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="type">{{__(" Type")}}</label>
                                        <select name="type" class="form-control" value="{{ old('type') }}">
                                            <option value="halt" {{$bus_location->type=="halt"?"selected":""}}>Halt</option>
                                            <option value="main" {{$bus_location->type=="main"?"selected":""}}>Main</option>
                                        </select>
                                        @include('alerts.feedback', ['field' => 'type'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\BusLocationUpdateRequest', '#bus_location_update') !!}
@endsection
