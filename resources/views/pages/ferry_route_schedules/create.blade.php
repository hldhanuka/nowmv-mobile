@extends('layouts.app', [
    'namePage' => 'Ferry Route Schedules',
    'class' => 'sidebar-mini',
    'activePage' => 'ferry_route_schedule',
  ])


@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ferry_route_schedules.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Ferry Route Schedule</h4>
                    </div>
                    <div class="card-body">
                        <form id="ferry_route_schedule_create" method="post" action="{{ route('ferry_route_schedules.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ferry_route_id">{{__(" Ferry Route")}}</label>
                                        <select class="form-control" id="ferry_route_id" name="ferry_route_id" >
                                            @foreach($ferry_routes as $ferry_route)
                                                <option value="{{$ferry_route->id}}">{{$ferry_route->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ferry_route_id'])
                                    </div>
                                </div>
                            </div>
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Starting Time")}}</label>
                                        <input id="timepicker" name="starting_time" class="form-control" value="{{ old('starting_time') }}"/>
                                        <script>
                                            $('#timepicker').timepicker({format: 'HH:MM'});
                                        </script>
                                        {{--                                        <input type="text"  id="time" name="time" class="form-control" value="{{ old('time') }}">--}}
                                        @include('alerts.feedback', ['field' => 'starting_time'])
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="schedule_types">{{__(" Schedule Types")}}</label>
                                        <select id="schedule_types" name="schedule_types[]" multiple="multiple">
                                            @foreach($schedule_types as $schedule_type)
                                                <option value="{{$schedule_type->id}}">{{$schedule_type->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'schedule_types'])
                                        <script>
                                            $(document).ready(function() {
                                                $('#schedule_types').select2();
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1"> Active
                                        <input type="radio" name="status" value="0"> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\FerryRouteScheduleCreateRequest', '#ferry_route_schedule_create') !!}
@endsection
