@extends('layouts.app', [
    'namePage' => 'Ferry Routes',
    'class' => 'sidebar-mini',
    'activePage' => 'ferry_route',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Ferry Route List</h4>
                        <div class="pull-right">
                            <a href="{{ route('ferry_routes.create') }}">
                                <button class="btn btn-primary">Create</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="ferry_routes_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Name</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('#ferry_routes_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('ferry_routes.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'from', name: 'from'},
                    {data: 'to', name: 'to'},
                    {data: 'type', name: 'type'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
