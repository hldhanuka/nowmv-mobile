@extends('layouts.app', [
    'namePage' => 'Ferry Routes',
    'class' => 'sidebar-mini',
    'activePage' => 'ferry_route',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ferry_routes.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Ferry Route</h4>
                    </div>
                    <div class="card-body">
                        <form id="ferry_route_create" method="post" action="{{ route('ferry_routes.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" From")}}</label>
                                        <select class="form-control" id="from" name="from" >
                                        {{--@foreach($locations as $location)--}}
                                        {{--    <option value="{{$location->id}}">{{$location->name}}</option>--}}
                                        {{--@endforeach--}}
                                            @foreach($locations as $location)
                                                <option value="{{$location["island_id"]}}">{{$location["island_name"]}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'from'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="restaurant_id">{{__(" To")}}</label>
                                        <select class="form-control" id="to" name="to" >
                                            {{--@foreach($locations as $location)--}}
                                            {{--    <option value="{{$location->id}}">{{$location->name}}</option>--}}
                                            {{--@endforeach--}}
                                            @foreach($locations as $location)
                                                <option value="{{$location["island_id"]}}">{{$location["island_name"]}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'to'])
                                    </div>
                                </div>
                            </div>
{{--                            <div class="row">--}}
{{--                                <div class="col-md-7 pr-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="type">{{__(" Type")}}</label>--}}
{{--                                        <select name="type" class="form-control" value="{{ old('type') }}">--}}
{{--                                            <option value="mtcc">MTCC</option>--}}
{{--                                            <option value="gmtl">GMTL</option>--}}
{{--                                        </select>--}}
{{--                                        @include('alerts.feedback', ['field' => 'type'])--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1"> Active
                                        <input type="radio" name="status" value="0"> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\FerryRouteCreateRequest', '#ferry_route_create') !!}
@endsection
