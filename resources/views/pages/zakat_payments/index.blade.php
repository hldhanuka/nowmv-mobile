@extends('layouts.app', [
    'namePage' => 'Zakat Payments',
    'class' => 'sidebar-mini',
    'activePage' => 'zakat_payment',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Zakat Payment List</h4>
                        <div class="pull-right">
                            <a href="{{ route('zakat_payments.csv') }}">
                                <button class="btn btn-primary">Report</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="zakat_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Mobile NO.</th>
                                <th>Name</th>
                                <th>ID Card NO.</th>
                                <th>Island</th>
                                <th>No. of Person</th>
                                <th>Sadagath Amount</th>
                                <th>Total Amount</th>
                                <th>Trxn Date</th>
                                <th>Order NO.</th>
                                <th>Response</th>
                                <th>TXN ID</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('#zakat_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('zakat_payments.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'mobile_number', name: 'mobile_number'},
                    {data: 'full_name', name: 'full_name'},
                    {data: 'id_number', name: 'id_number'},
                    {data: 'island_id', name: 'island_id'},
                    {data: 'no_of_person', name: 'no_of_person'},
                    {data: 'sadagath_amount', name: 'sadagath_amount'},
                    {data: 'total_amount', name: 'total_amount'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'payment_invoice_number', name: 'payment_invoice_number'},
                    {data: 'response_text', name: 'response_text'},
                    {data: 'transaction_id', name: 'transaction_id'},
                ]
            });
        });
    </script>
@endsection
