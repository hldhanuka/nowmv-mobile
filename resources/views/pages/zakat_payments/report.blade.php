@extends('layouts.app', [
    'namePage' => 'Zakat Payments',
    'class' => 'sidebar-mini',
    'activePage' => 'zakat_payment',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('zakat_payments.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Download CSV</h4>
                    </div>
                    <div class="card-body">
                        <form id="zakat_report" method="post" action="{{ route('zakat_payments.download-report') }}">
                            @csrf
                            @include('alerts.success')
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="from_date">{{__(" From Date")}}</label>
                                        <input id="from_date" name="from_date" class="form-control" value="{{ old('from_date') }}"/>
                                        <script>
                                            $('#from_date').datepicker({ format: 'yyyy-mm-dd' });
                                        </script>
                                        @include('alerts.feedback', ['field' => 'from_date'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="to_date">{{__(" To Date")}}</label>
                                        <input id="to_date" name="to_date" class="form-control" value="{{ old('to_date') }}"/>
                                        <script>
                                            $('#to_date').datepicker({ format: 'yyyy-mm-dd' });
                                        </script>
                                        @include('alerts.feedback', ['field' => 'to_date'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Download')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\ZakatDownloadCSVRequest', '#zakat_report') !!}
@endsection
