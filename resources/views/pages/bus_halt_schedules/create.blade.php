@extends('layouts.app', [
    'namePage' => 'Bus Halt Schedule',
    'class' => 'sidebar-mini',
    'activePage' => 'bus_halt_schedule',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('bus_halt_schedules.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Bus Halt Schedule</h4>
                    </div>
                    <div class="card-body">
                        <form id="bus_halt_schedule_create" method="post" action="{{ route('bus_halt_schedules.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="bus_route">{{__(" Bus Route")}}</label>
                                        <select class="form-control" id="bus_route" name="bus_route" >
                                            @foreach($bus_routes as $bus_route)
                                                <option value="{{$bus_route->id}}">{{$bus_route->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'bus_route'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="start_location">{{__(" Starting Location")}}</label>
                                        <select class="form-control" id="start_location" name="start_location" >
                                            @foreach($start_locations as $start_location)
                                                <option value="{{$start_location->id}}">{{$start_location->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'start_location'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="halt">{{__(" Halt")}}</label>
                                        <select class="form-control" id="halt" name="halt" >
                                            @foreach($bus_halts as $bus_halt)
                                                <option value="{{$bus_halt->id}}">{{$bus_halt->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'halt'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1"> Active
                                        <input type="radio" name="status" value="0"> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Time")}}</label>
                                        <input id="timepicker" name="time" class="form-control" value="{{ old('time') }}"/>
                                        <script>
                                            $('#timepicker').timepicker({format: 'HH:MM'});
                                        </script>
                                        {{--                                        <input type="text"  id="time" name="time" class="form-control" value="{{ old('time') }}">--}}
                                        @include('alerts.feedback', ['field' => 'time'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\BusHaltScheduleCreateRequest', '#bus_halt_schedule_create') !!}
@endsection
