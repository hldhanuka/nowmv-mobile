@extends('layouts.app', [
    'namePage' => 'Bus Halt Schedule',
    'class' => 'sidebar-mini',
    'activePage' => 'bus_halt_schedule',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Bus Halt Schedule List</h4>
                        <div class="pull-right">
                            <a href="{{ route('bus_halt_schedules.create') }}">
                                <button class="btn btn-primary">Create</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="bus_halt_schedules_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Bus Route</th>
                                <th>Start Location</th>
                                <th>Halt</th>
                                <th>Time</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('#bus_halt_schedules_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('bus_halt_schedules.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'bus_route', name: 'bus_route'},
                    {data: 'start_location', name: 'start_location'},
                    {data: 'halt', name: 'halt'},
                    {data: 'time', name: 'time'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
