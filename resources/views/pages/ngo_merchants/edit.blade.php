@extends('layouts.app', [
    'namePage' => 'NGO Merchants',
    'class' => 'sidebar-mini',
    'activePage' => 'ngo_merchant',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ngo_merchants.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Merchant</h4>
                    </div>
                    <div class="card-body">
                        <form id="ngo_merchant_update" method="post" action="{{ route('ngo_merchants.update', $ngo_merchant->id) }}">
                            @method("PUT")
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="name">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name', $ngo_merchant->name) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="key">{{__(" Key (Origination Number)")}}</label>
                                        <input type="text" name="key" class="form-control" value="{{ old('key', $ngo_merchant->key) }}">
                                        @include('alerts.feedback', ['field' => 'key'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\NgoMerchantUpdateRequest', '#ngo_merchant_update') !!}
@endsection
