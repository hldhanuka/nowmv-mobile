@extends('layouts.app', [
    'namePage' => 'News',
    'class' => 'sidebar-mini',
    'activePage' => 'news',
  ])

@section('content')


    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('news.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create News</h4>
                    </div>
                    <div class="card-body">
                        <form id="news_create" method="post" action="{{ route('news.store') }}"
                              enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Author")}}</label>
                                        <input type="text" name="author" class="form-control"
                                               value="{{ old('author') }}">
                                        @include('alerts.feedback', ['field' => 'author'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" Categories")}}</label>
                                        <select class="form-control" id="category_id" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'category_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="news_agency_id">{{__(" News Agency")}}</label>
                                        <select class="form-control" id="news_agency_id" name="news_agency_id">
                                            @if(auth()->user()->hasRole("admin"))
                                                @foreach($news_agencies as $news_agency)
                                                    <option value="{{$news_agency->id}}">{{$news_agency->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach(auth()->user()->agents()->get() as $news_agent)
                                                    <option
                                                        value="{{$news_agent->agency->id}}">{{$news_agent->agency->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'news_agency_id'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>1.5, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/news'])
                                        <p>Required Dimensions - Ratio 3:2 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required>
                                        <p>Required Dimensions - Ratio 3:2 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Video URL")}}</label>
                                        <input type="text" name="video" class="form-control" value="{{ old('video') }}">
                                        <p>Required URL - youtube or .mp4</p>
                                        @include('alerts.feedback', ['field' => 'video'])
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title" class="d-block">{{__(" OR Video File")}}</label>
                                        <div class='file-input'>
                                            <input type='file' name="video_file">
                                            <span class='button'>Upload Video</span>
                                            <span class='label' data-js-label>No file selected</span>
                                        </div>
                                        <p>Required Video Format - .mp4</p>
                                        @include('alerts.feedback', ['field' => 'video_file'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Video Thumbnail")}}</label>
                                        @include('includes.cropping_tool', ['id'=>2, 'name'=>'video_thumbnail', 'ratio'=>1.5, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'uploads/news'])
                                        <p>Required Dimensions - Ratio 3:2 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'video_thumbnail_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Video Thumbnail")}}</label>
                                        <img class="gal-img prev_img2" id="prev_img2" src="{{asset('assets/img/dummy1.jpg')}}">
                                        <input type="file" id="custom-file-input2" class="custom-file-input2"
                                               name="video_thumbnail">
                                        <p>Required Dimensions - Ratio 3:2 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'video_thumbnail'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Resource Link")}}</label>
                                        <input type="text" name="link" class="form-control" value="{{ old('link') }}">
                                        @include('alerts.feedback', ['field' => 'link'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Featured News")}}</label><br>
                                        <input type="radio" name="is_featured"
                                               value="1" {{ old('is_featured') == "1" ? 'checked' : '' }}> Yes
                                        <input type="radio" name="is_featured"
                                               value="0" {{ old('is_featured') == "0" ? 'checked' : '' }}> No
                                        @include('alerts.feedback', ['field' => 'is_featured'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="tags">{{__(" Tags")}}</label>
                                        <select id="tags" name="tags[]" multiple="multiple">
                                            @foreach($tags_list as $tag)
                                                <option
                                                    value="{{$tag->id}}" {{ in_array($tag->id, old('tags',[]))?"selected":"" }}>{{$tag->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'tags'])
                                        <script>
                                            $(document).ready(function () {
                                                $('#tags').select2();
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="is_english">{{__(" Is English")}}</label><br>
                                        <input type="radio" name="is_english" value="1"
                                               checked {{ old('is_english') == "1" ? 'checked' : '' }}> Yes
                                        <input type="radio" name="is_english"
                                               value="0" {{ old('is_english') == "0" ? 'checked' : '' }}> No
                                        @include('alerts.feedback', ['field' => 'is_english'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('content') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" HTML Content")}}</label>
                                        <textarea type="text" id="html_content" name="html_content" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('html_content') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'html_content'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--ckeditor--}}
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('html_content');
        });
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\NewsCreateRequest', '#news_create') !!}
@endsection
