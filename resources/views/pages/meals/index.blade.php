@extends('layouts.app', [
    'namePage' => 'Meals',
    'class' => 'sidebar-mini',
    'activePage' => 'meal',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Meal List</h4>
                        <div class="pull-right">
                            <a href="{{ route('meals.create') }}">
                                <button class="btn btn-primary">Create</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="meals_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Price</th>
                                <th>Restaurant</th>
                                <th>Merchant Name</th>
                                <th>Created By</th>
                                <th>Published At</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function changeStatus(item_id){
            Swal.fire({
                title: 'Change Status',
                text: "What do you want to do?",
                icon: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Approve',
                cancelButtonText: 'Reject'
            }).then((result) => {
                if (result.value) {
                    // Press Approve
                    sendAjax(item_id, 1);
                } else if(result.dismiss === Swal.DismissReason.cancel) {
                    // Press Reject
                    sendAjax(item_id, 3);
                }
            })
        }

        function sendAjax(item_id, status){
            $.ajax({
                url:"{{route('meals.change-status')}}",
                data:{id:item_id,status:status},
                method:"post",
                success: function (result) {
                    if(result.success){
                        Swal.fire({
                            title:'Success',
                            icon:'success',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire({
                            title:'Error',
                            icon:'error',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    }
                }
            });
        }

        $(document).ready(function () {

            $('#meals_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('meals.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'image', name: 'image', orderable: false, searchable: false},
                    {data: 'title', name: 'title'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'price', name: 'price'},
                    {data: 'restaurant_id', name: 'restaurant_id'},
                    {data: 'merchant_id', name: 'merchant_id'},
                    {data: 'created_user_email', name: 'created_user_email'},
                    {data: 'published_at', name: 'published_at'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
