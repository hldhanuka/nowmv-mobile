@extends('layouts.app', [
    'namePage' => 'Meals',
    'class' => 'sidebar-mini',
    'activePage' => 'meal',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('meals.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Meal</h4>
                    </div>
                    <div class="card-body">
                        <form id="meal_create" method="post" action="{{ route('meals.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="merchant_id">{{__(" Merchants")}}</label>
                                        <select class="form-control" id="merchant_id" name="merchant_id">
                                            @if(auth()->user()->hasRole("admin"))
                                                @foreach($merchants as $merchant)
                                                    <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach(auth()->user()->merchants()->get() as $merchant)
                                                    <option
                                                        value="{{$merchant->merchant->id}}">{{$merchant->merchant->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'merchant_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="restaurant_id">{{__(" Restaurants")}}</label>
                                        <select class="form-control" id="restaurant_id" name="restaurant_id" >
                                            @foreach($restaurants as $restaurant)
                                                <option value="{{$restaurant->id}}">{{$restaurant->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'restaurant_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" Categories")}}</label>
                                        <select class="form-control" id="category_id" name="category_id" >
                                            {{--Load from AJAX--}}
                                        </select>
                                        @include('alerts.feedback', ['field' => 'category_id'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>2, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/meals'])
                                        <p>Required Dimensions - Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image']  )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Price")}}</label>
                                        <input type="number" name="price" class="form-control" value="{{ old('price') }}">
                                        @include('alerts.feedback', ['field' => 'price'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Featured Meal")}}</label><br>
                                        <input type="radio" name="is_featured" value="1"> Yes
                                        <input type="radio" name="is_featured" value="0"> No
                                        @include('alerts.feedback', ['field' => 'is_featured'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control" style="border:1px solid #E3E3E3">{{ old('content') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\MealCreateRequest', '#meal_create') !!}

    <script>

      $(document).ready(function(){
        let restaurant_select = $("#restaurant_id");
        loadMealCategoriesByRestaurantId(restaurant_select.val());

        restaurant_select.change(function(){
          loadMealCategoriesByRestaurantId($(this).val());
        });
      });

      function loadMealCategoriesByRestaurantId(restaurant_id) {
        $.ajax({
          method: "GET",
          url: "{{ route('welcome') }}/load-meal-categories-by-restaurant-id/"+restaurant_id,
          success: function(data) {
            let html_content = "";
            for(let i=0;i<data.length;i++){
              html_content+=`<option value="`+data[i].id+`">`+data[i].title+`</option>`;
            }
            $("#category_id").html(html_content);
          }
        });
      }
    </script>
@endsection
