@extends('layouts.app', [
    'namePage' => 'Restaurants',
    'class' => 'sidebar-mini',
    'activePage' => 'restaurant',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('restaurants.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Restaurant</h4>
                    </div>
                    <div class="card-body">
                        <form id="restaurant_update" method="post" action="{{ route('restaurants.update', $restaurant->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Restaurant Name")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title', $restaurant->title) }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" Merchants")}}</label>
                                        <select class="form-control" id="merchant_id" name="merchant_id" >
                                            @if(auth()->user()->hasRole("admin"))
                                                @foreach($merchants as $merchant)
                                                    <option value="{{$merchant->id}}" {{$merchant->id==$restaurant->merchant_id?"selected":""}}>{{$merchant->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach(auth()->user()->merchants()->get() as $merchant)
                                                    <option
                                                        value="{{$merchant->merchant->id}}" {{$merchant->merchant->id==$restaurant->merchant_id?"selected":""}}>{{$merchant->merchant->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'merchant_id'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>2, 'current_image'=>$restaurant->image!=null?$restaurant->image:asset('assets/img/dummy.jpg'),'upload_path'=>'upload/restaurants'])
                                        <p>Required Dimensions - Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{$restaurant->image!=null?$restaurant->image:asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image']  )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group position-relative">
                                        <div class="input-wrap position-absolute">
                                            <input type="text" id="location_name" class="form-control frm-ctrl"
                                                   placeholder="Search Box">
                                        </div>
                                        {{--Map--}}
                                        <div id="map"
                                             style="border: 2px solid #3872ac;height: 400px;width: 100%;margin: 0;padding: 0;">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Longitude")}}</label>
                                        <input type="text" name="longitude" id="longitude" class="form-control" value="{{ old('longitude', $restaurant->longitude) }}" readonly>
                                        @include('alerts.feedback', ['field' => 'longitude'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Latitude")}}</label>
                                        <input type="text" name="latitude" id="latitude" class="form-control" value="{{ old('latitude', $restaurant->latitude) }}" readonly>
                                        @include('alerts.feedback', ['field' => 'latitude'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Location Name")}}</label>
                                        <input type="text" name="location_name" class="form-control" value="{{ old('location_name', $restaurant->location_name) }}">
                                        @include('alerts.feedback', ['field' => 'location_name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('content', $restaurant->content) }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\RestaurantUpdateRequest', '#restaurant_update') !!}

    {{--############################## Google map JS Start ##########################--}}
    <script
        src="{!! url('https://maps.google.com/maps/api/js?key='.config('app.google_map_key').'&libraries=places') !!}"></script>
    <script src="{{ asset('js/locationpicker.jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
            let default_latitude = '{{$restaurant->latitude}}';
            let default_longitude = '{{$restaurant->longitude}}';
            $("#latitude").val(default_latitude);
            $("#longitude").val(default_longitude);

            let male_latitude = 4.1754695;
            let male_longitude = 73.5093531;

            const bounds = new window.google.maps.LatLngBounds(
                new window.google.maps.LatLng(male_latitude,male_longitude),
            )
            $('#map').locationpicker({
                location: {
                    latitude: default_latitude,
                    longitude: default_longitude,
                },
                radius: 0,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    locationNameInput: $('#location_name')
                },
                enableAutocomplete: true,
                autocompleteOptions: {
                    bounds: bounds,
                },
                draggable: true,
                markerDraggable: true
            });
        });
    </script>
    {{--############################## Google map JS End ##########################--}}

@endsection
