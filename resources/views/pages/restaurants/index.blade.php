@extends('layouts.app', [
    'namePage' => 'Restaurants',
    'class' => 'sidebar-mini',
    'activePage' => 'restaurant',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Restaurants List</h4>
                        <div class="pull-right">
                            <a href="{{ route('restaurants.create') }}">
                                <button class="btn btn-primary">Create</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="restaurants_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Merchant Name</th>
                                <th>Status</th>
                                <th>Location</th>
                                <th>Created By</th>
                                <th>Published At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function changeStatus(item_id){
            Swal.fire({
                title: 'Change Status',
                text: "What do you want to do?",
                icon: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Approve',
                cancelButtonText: 'Reject'
            }).then((result) => {
                if (result.value) {
                    // Press Approve
                    sendAjax(item_id, 1);
                } else if(result.dismiss === Swal.DismissReason.cancel) {
                    // Press Reject
                    sendAjax(item_id, 3);
                }
            })
        }

        function sendAjax(item_id, status){
            $.ajax({
                url:"{{route('restaurants.change-status')}}",
                data:{id:item_id,status:status},
                method:"post",
                success: function (result) {
                    if(result.success){
                        Swal.fire({
                            title:'Success',
                            icon:'success',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire({
                            title:'Error',
                            icon:'error',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    }
                }
            });
        }

        $(document).ready(function () {

            $('#restaurants_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('restaurants.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'image', name: 'image', orderable: false, searchable: false},
                    {data: 'title', name: 'title'},
                    {data: 'merchant_id', name: 'merchant_id'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'location_name', name: 'location_name'},
                    {data: 'created_user_email', name: 'created_user_email'},
                    {data: 'published_at', name: 'published_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
