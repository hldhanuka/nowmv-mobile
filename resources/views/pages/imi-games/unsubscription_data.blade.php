@extends('layouts.app', [
    'namePage' => 'Unsubscription Data',
    'class' => 'sidebar-mini',
    'activePage' => 'page',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$name}} Unsubscription Data</h4>
                    </div>
                    <div class="card-body">
                        <div class="game-details-wrap">
                            <div class="row">
                                <div class="col-lg-12 mb-5">
                                    <form action="{{route("imi_games.game-unsubscription-index")}}" method="get" autocomplete="off">
                                        <input type="hidden" name="uuid" value="{{$uuid}}" id="uuid">
                                        <input type="hidden" name="name" value="{{$name}}" id="name">

                                        <div class="row mb-4">
                                            <div class="col-md-6 col-lg-5 col-xl-4">
                                                <div class="form-group">
                                                    <label for="title">Start Date</label>
                                                    <input 
                                                        type="text"
                                                        placeholder="Select date"
                                                        name="start_date"
                                                        class="form-control"
                                                        id="start_date" 
                                                        required
                                                    >
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-5 col-xl-4">
                                                <div class="form-group">
                                                    <label for="title">End Date</label>
                                                    <input 
                                                        type="text"
                                                        placeholder="Select date"
                                                        name="end_date"
                                                        class="form-control"
                                                        id="end_date" 
                                                        required
                                                >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary btn-round">Submit</button>
                                        </div>
                                    </form>
                                    <div class="table-responsive">
                                        <table class="table" id="subscription_data_daily">
                                            <thead class=" text-primary">
                                                <th>Date</th>
                                                <th>Count</th>
                                            </thead>
                                            <tbody>
                                                @foreach($unsubscription_data_daily as $unsubscription)
                                                    <tr>
                                                        <td>{{$unsubscription["unsubscribed_date"]}}</td>
                                                        <td>{{$unsubscription["unsubscription_count"]}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                var today = new Date();

                var today_date = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();

                //Date Picker
                $("#start_date").datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth:true,
                    changeYear:true,
                    numberOfMonths:[2,2],
                    maxDate: today_date
                });

                $('#end_date').datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth:true,
                    changeYear:true,
                    numberOfMonths:[2,2],
                    maxDate: today_date
                });

                // DataTables initialisation
                $('#subscription_data_daily').DataTable({
                    'processing': true,
                    "searching": false,
                    "lengthChange":false,
                    "pageLength": 10,
                    'serverSide': false,
                });
            });
        </script>
@endsection
