@extends('layouts.app', [
    'namePage' => 'Promo games',
    'class' => 'sidebar-mini',
    'activePage' => 'page',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Games</h4>
                    </div>
                    <div class="card-body">
                        <div class="game-details-wrap">

                            <div class="row justify-content-center">
                                <div class="col-12 mb-4">
                                    <h6 class="card-title w-100">Today Subscription Count : {{$subscription_data["today_subscription_count"]}}</h6>
                                    <h6 class="card-title w-100">Total Subscription Count : {{$subscription_data["total_subscription"]}}</h6>
                                    <h6 class="card-title w-100">Game play Count :  {{$subscription_data["today_gameplay"]}}</h6>
                                </div>

                                @foreach($games as $game)
                                    <div class="col-md-6 col-lg-4">
                                        <a href="{{(route('imi_games.game-details',$game))}}" class="game-detail-wrap">
                                            <div class="game-detail-img-wrap">
                                                <div class="game-detail-img">
                                                    <img src="{{$game["image"]}}" alt="image game"/>
                                                </div>
                                            </div>
                                            <div class="game-detail-text-wrap">
                                                <p class="name">{{$game["name"]}}</p>
                                                <p class="description">{{$game["description"]}}</p>
                                                <p class="type">{{$game["occurrence_type"]}}</p>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <style>
        .game-detail-wrap{
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            box-shadow: 0 1px 15px 1px #2727271a;
            padding: 15px 10px;
            margin-bottom: 25px;
        }
        .game-detail-wrap:hover{
            text-decoration: none;
        }
        .game-detail-img-wrap{
            position: relative;
            width: 200px;
        }
        .game-detail-img{
            height: 200px;
        }
        .game-detail-img-wrap img{
            width: 100%;
            height: 100%;
            object-fit: contain;
        }
        .game-detail-text-wrap{
            width: calc(100% - 200px);
            padding: 10px 0 10px 10px;
        }
        .game-detail-text-wrap .name{
            font-size: 17px;
            font-weight: 600;
            margin: 0 0 8px;
        }
        .game-detail-text-wrap .description{
            font-size: 14px;
            font-weight: 500;
            margin: 0 0 8px;
        }
        .game-detail-text-wrap .type{
            font-size: 14px;
            margin: 0;
        }
        @media(max-width: 575.98px){
            .game-detail-img-wrap{
                margin: 0 auto;
                width: 150px;
            }
            .game-detail-img{
                height: 150px;
            }
            .game-detail-text-wrap{
                text-align: center;
                width: 100%;
                padding: 12px;
            }
        }
    </style>
@endsection
