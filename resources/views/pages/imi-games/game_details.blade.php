@extends('layouts.app', [
    'namePage' => 'Game Details',
    'class' => 'sidebar-mini',
    'activePage' => '',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Game details</h4>
                    </div>
                    <div class="card-body">
                        <div class="game-details-wrap">

                            <div class="row game-single-detail">
                                <div class="col-sm-auto">
                                    <div class="img-wrap">
                                        <img src="{{$image}}" alt="image game"/>
                                    </div>
                                </div>
                                <div class="col-sm-auto">
                                    <div class="text-wrap">
                                        <p class="heading">{{$name}}</p>
                                        <p class="description">{{$description}}</p>
                                        <p class="para">{{$occurrence_type}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="btn-links">
                                        <a class="btn btn-primary btn-round" href="{{route("imi_games.game-subscription-index",["uuid"=>$uuid,"name"=>$name])}}">Subscription data</a>
                                        <a class="btn btn-primary btn-round" href="{{route("imi_games.game-unsubscription-index",["uuid"=>$uuid,"name"=>$name])}}">Unsubscription data</a>
                                        <a class="btn btn-primary btn-round" href="{{route("imi_games.revenue-index",["uuid"=>$uuid,"name"=>$name])}}">Revenue data</a>
                                        <a class="btn btn-primary btn-round" href="{{route("imi_games.leaderboard-index",["uuid"=>$uuid,"name"=>$name])}}">Leader Board</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
