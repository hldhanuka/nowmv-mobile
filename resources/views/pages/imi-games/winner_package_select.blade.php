@extends('layouts.app', [
    'namePage' => 'Winner Package Select',
    'class' => 'sidebar-mini',
    'activePage' => 'leader_board',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <form method="POST" action="{{route('imi_games.game-winner-send-package')}}">
                        <input type="hidden" class="form-control" name="game_uuid" value="{{$uuid}}"  readonly required>
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title">Select winner package</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Msisdn</label>
                                <input type="text" class="form-control" name="msisdn" value="{{$msisdn}}"  readonly required>

                            </div>

                            <div class="form-group">
                                <label for="title">Offer name</label>
                                <input type="text" class="form-control" name="offer_name" value="{{$offer_name}}"  readonly required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Package</label>
                                <select name="package_name" class="form-control" required>
                                    @foreach($eligible_bundle_list as $key => $eligible_bundle)
                                        <option value="{{$eligible_bundle['bundle_system_name']}}">{{$eligible_bundle['bundle_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
    </script>
@endsection
