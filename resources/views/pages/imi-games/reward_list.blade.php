@extends('layouts.app', [
    'namePage' => 'Reward List',
    'class' => 'sidebar-mini',
    'activePage' => 'leader_board',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Reward List</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12 mb-5 mt-5">
                            <div class="table-responsive">
                                <table class="table" id="leader_board">
                                    <thead class=" text-primary">
                                    <th>Date & Time</th>
                                    <th>Mobile No</th>
                                    <th>Rewarded Bundle Name</th>
                                    <th>Status</th>
                                    </thead>
                                    <tbody>



                                    @foreach($rewards_data as $reward_data)

                                        <tr>
                                            <td>{{$reward_data["time"]}}</td>
                                            <td>{{$reward_data["msisdn"]}}</td>
                                            <td>{{$reward_data["rewardedBundleName"]}}</td>
                                            <td>{{$reward_data["status"]}}</td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                            @if($current_page != 0)
                                <form method="GET" action="{{route('imi_games.reward-list')}}">
                                    <input type="hidden" name="page" value="{{$current_page-1}}" id="page">
                                    <button type="submit" class="btn btn-success">Back</button>
                                </form>
                            @endif

                            @if(!$is_last_page)
                                <form method="GET" action="{{route('imi_games.reward-list')}}">
                                    <input type="hidden" name="page" value="{{$current_page+1}}" id="page">
                                    <button type="submit" class="btn btn-success">Next</button>
                                </form>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('#leader_board').DataTable({
                "paging" : false,
                'processing': true,
                "searching": false,
                "lengthChange":false,
                'serverSide': false,
                "sorting": false,
                "aaSorting": [],
                "info": false,
            });
            $('#winners').dataTable();
        });
    </script>
@endsection
