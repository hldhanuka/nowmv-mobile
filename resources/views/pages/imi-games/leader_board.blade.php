@extends('layouts.app', [
    'namePage' => 'Leader Board',
    'class' => 'sidebar-mini',
    'activePage' => 'leader_board',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$name}} Leader Board</h4>
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{route('imi_games.leaderboard-index')}}" autocomplete="off">
                            <input type="hidden" name="uuid" value="{{$uuid}}" id="uuid">
                            <input type="hidden" name="name" value="{{$name}}" id="name">
                            <div class="row">
                                <div class="col-md-6 col-lg-5 col-xl-4">
                                    <div class="form-group">
                                        <label for="title">Start Date</label>
                                        <input
                                            type="text"
                                            placeholder="Select date"
                                            name="start_date"
                                            class="form-control"
                                            id="start_date" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-5 col-xl-4">
                                    <div class="form-group">
                                        <label for="title">End Date</label>
                                        <input
                                            type="text"
                                            placeholder="Select date"
                                            name="end_date"
                                            class="form-control"
                                            id="end_date" 
                                            required
                                        >
                                    </div>
                                </div>
                                @if ($is_reward_leaderboard_api_called && $reward_leaderboard_api_error)
                                    <div class="col-md-12 text-danger">
                                        {{ $reward_leaderboard_api_error }}
                                    </div>
                                @endif
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-round">Generate</button>
                                </div>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                        <div class="col-lg-12 mb-5 mt-5">
                            <h6 class="card-title w-100 mb-5">Generated Leader Board</h6>
                            <div class="table-responsive">
                                <table class="table" id="leader_board">
                                    <thead class=" text-primary">
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Leaderboard ID</th>
                                        <th>Game ID</th>
                                        <th>UUID</th>
                                        <th>Winners Count</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        @foreach($leader_boards as $leader_board)
                                            <tr>
                                                <td>{{$leader_board["id"]}}</td>
                                                <td>{{$leader_board["title"]}}</td>
                                                <td>{{$leader_board["start_date"]}}</td>
                                                <td>{{$leader_board["end_date"]}}</td>
                                                <td>{{$leader_board["leaderboard_id"]}}</td>
                                                <td>{{$leader_board["game_id"]}}</td>
                                                <td>{{$leader_board["uuid"]}}</td>
                                                <td>{{$leader_board["winnerCount"]}}</td>
                                                <td>
                                                    <button onclick="getWinnersData('{{$leader_board["uuid"]}}')" class="btn btn-warning" title="View"><i class="fas fa-eye"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                                @if($current_page != 0)
                                    <form method="GET" action="{{route('imi_games.leaderboard-index')}}">
                                        <input type="hidden" name="uuid" value="{{$uuid}}" >
                                        <input type="hidden" name="name" value="{{$name}}" >
                                        <input type="hidden" name="page" value="{{$current_page-1}}" id="page">
                                        <button type="submit" class="btn btn-success">Back</button>
                                    </form>
                                @endif

                                @if(!$is_last_page)
                                    <form method="GET" action="{{route('imi_games.leaderboard-index')}}">
                                        <input type="hidden" name="uuid" value="{{$uuid}}" >
                                        <input type="hidden" name="name" value="{{$name}}" >
                                        <input type="hidden" name="page" value="{{$current_page+1}}" id="page">
                                        <button type="submit" class="btn btn-success">Next</button>
                                    </form>
                                @endif
                            </div>

                            <div class="col-lg-12 mb-5">
                                <h6 class="card-title w-100 mb-5">Winners</h6>
                                <div class="table-responsive">
                                    <table class="table" id="winners">
                                        <thead class=" text-primary">
                                            <th>Mobile No</th>
                                            <th>Subscriber</th>
                                            <th>Rank</th>
                                            <th>Score</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script>
        $(document).ready(function () {
            var today = new Date();

            var today_date = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();

            //Start Date Picker
            $("#start_date").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth:true,
                changeYear:true,
                numberOfMonths:[2,2],
                minDate: today_date
            });

            //End Date Picker
            $('#end_date').datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth:true,
                changeYear:true,
                numberOfMonths:[2,2],
                minDate: today_date
            });

            // DataTables initialisation
            $('#leader_board').DataTable({
                "paging" : false,
                'processing': true,
                "searching": false,
                "lengthChange":false,
                "pageLength": 10,
                'serverSide': false,
                "info": false,
            });

            $('#winners').DataTable({
                "paging" : false,
                'processing': true,
                "searching": false,
                "lengthChange":false,
                "pageLength": 10,
                'serverSide': false,
                "info": false,
            });
        });

        function getWinnersData(uuid){
            $.ajax({
                type: 'get',
                url: "{{route('imi_games.get-winners-data')}}",
                data: {"uuid": uuid},

                success: function (data) {
                    loadWinnerTableData(data);
                }
            });
        }

        function loadWinnerTableData(data){
            var winner_table = $('#winners');
            var winner_table_tbody = winner_table.find('tbody');

            // to remove already appended table rows
            winner_table_tbody.empty();

            // append data to table
            $.each(JSON.parse(data), function(i, data){
                winner_table_tbody.append(
                    "<tr>" +
                        "<td>" + data.msisdn + "</td>" + 
                        "<td>" + data.subscriber + "</td>" +
                        "<td>" + data.rank + "</td>" +
                        "<td>" + data.score + "</td>" + 
                        "<td> <a href='{{route('imi_games.game-winner-send-package',["uuid"=>$uuid])}}&msisdn=" + data.msisdn + "' class='btn btn-warning' title='View'><i class='fas fa-eye'></i></a></td>" +
                    "</tr>"
                );
            });

            // set no data text on the table when data is empty
            if(winner_table_tbody.find('tr').length == 0) {
                winner_table_tbody.append('<td valign="top" colspan="5" class="dataTables_empty">No data available in table</td>');
            }
        }
    </script>
@endsection
