@extends('layouts.app', [
    'namePage' => 'Ferry Schedule Types',
    'class' => 'sidebar-mini',
    'activePage' => 'ferry_schedule_type',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ferry_schedule_types.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Ferry Schedule Types</h4>
                    </div>
                    <div class="card-body">
                        {{--                        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
                        {{--                        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}
                        <form id="schedule_type_create" method="post" action="{{ route('ferry_schedule_types.store') }}"
                              enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="name">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="priority">{{__(" Priority")}}</label>
                                        <input type="text" name="priority" class="form-control"
                                               value="{{ old('priority') }}">
                                        @include('alerts.feedback', ['field' => 'priority'])
                                    </div>
                                </div>
                            </div>
                            <link rel="stylesheet" href="{{asset('css/kendo.default-v2.min.css')}}">
                            <script src="{{asset('js/jquery.min.js')}}"></script>
                            <script src="{{asset('js/kendo.all.min.js')}}"></script>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__(" Dates")}}</label>
                                        <div id="example">
                                            <div class="demo-section k-content mb-2">
                                                <p>Schedule Dates</p>
                                                <div id="calendar"></div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="date_array" id="date_array">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p style="color: red;">***Select multiple dates by pressing the control button***</p>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function onMultipleDateChange() {
            $("#date_array").val(this._selectDates.map(x => formatDateString(x)));
            console.log(this._selectDates.map(x => formatDateString(x)));
            // console.log("Change :: " + (this.value()));
        }

        function formatDateString(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }

        $(document).ready(function() {
            // create Calendar from div HTML element
            $("#calendar").kendoCalendar({
                selectable: "multiple",
                weekNumber: true,
                change: onMultipleDateChange,
                format: "yyyy-mm-dd",
            });

        });
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\FerryScheduleTypeCreateRequest', '#schedule_type_create') !!}
@endsection
