@extends('layouts.app', [
    'namePage' => 'Schedule Types',
    'class' => 'sidebar-mini',
    'activePage' => 'schedule_type',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Schedule Type List</h4>
                        <div class="pull-right">
                            <a href="{{ route('schedule_types.create') }}">
                                <button class="btn btn-primary">Create</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="schedule_types_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Type</th>
                                <th>Priority</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function () {

        $('#schedule_types_table').dataTable({
            "scrollY": 500,
            processing: true,
            serverSide: true,
            ajax: "{!! route('schedule_types.load-data') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'status_text', name: 'status_text'},
                {data: 'type', name: 'type'},
                {data: 'priority', name: 'priority'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
