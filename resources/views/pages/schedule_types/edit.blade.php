@extends('layouts.app', [
    'namePage' => 'Schedule Types',
    'class' => 'sidebar-mini',
    'activePage' => 'schedule_type',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('schedule_types.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Schedule Types</h4>
                    </div>
                    <div class="card-body">
                        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
                        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                        <form id="schedule_type_update" method="post" action="{{ route('schedule_types.update', $schedule_type -> id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="name">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name', $schedule_type->name) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="type">{{__(" Type")}}</label>
                                        <select name="type" class="form-control" value="{{ old('type') }}">
                                            <option value="bus" {{ $schedule_type->type=="bus"?"selected":"" }}>bus</option>
                                            <option value="calendar" {{ $schedule_type->type=="calendar"?"selected":"" }}>calendar</option>
                                        </select>
                                        @include('alerts.feedback', ['field' => 'type'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="priority">{{__(" Priority")}}</label>
                                        <input type="text" name="priority" class="form-control" value="{{ old('priority',$schedule_type->priority) }}">
                                        @include('alerts.feedback', ['field' => 'priority'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__(" Dates")}}</label>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center item-raw-title">Dates</th>
                                                <th class="text-center item-raw-title">Titles</th>
                                                <th class="text-center">
                                                    <i class="fas fa-plus-circle fa-2x text-success" onclick="addNewItemRaw('date','schedule_table')"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="schedule_table">
                                            @foreach(json_decode($schedule_type->date, true) as $date)
                                            <tr>
                                                <td class="text-center">
                                                    <i class="fas fa-arrows-alt-v"></i>
                                                </td>
                                                <td class="text-center">
                                                    <div class="form-group">
                                                        <input name="date[]" class="form-control datepicker" value="{{ $date["date"] }}">
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text" name="title[]" value="{{ $date["title"] }}">
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @include('alerts.feedback', ['field' => 'date'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
      $(document).ready(function () {
        $("#schedule_table").sortable();
        $("#schedule_table").disableSelection();
        $('.datepicker').datepicker({ format: 'yyyy-mm-dd' });
      });

      // $('body').on('focus',".datepicker", function(){
      //   $(this).datepicker({ format: 'yyyy-mm-dd' });
      // });

      function addNewItemRaw(){
        let html = `<tr>
        <td class="text-center"><i class="fas fa-arrows-alt-v"></i></td>
        <td class="text-center">
            <div class="form-group">
                <input name="date[]" class="form-control datepicker">
            </div>
        </td>
        <td class="text-center"><input class="form-control" type="text" name="title[]"></td>
        <td class="text-center"><i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i></td>
        </tr>`;
        $("#schedule_table").append(html);
      }

      function removeMyRaw(item){
        $(item).closest("tr").remove();
      }
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\ScheduleTypeUpdateRequest', '#schedule_type_update') !!}
@endsection
