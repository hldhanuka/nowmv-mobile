@extends('layouts.app', [
    'namePage' => 'Offers',
    'class' => 'sidebar-mini',
    'activePage' => 'offer',
  ])

@section('content')

    <script src="{{asset('mobile_tags/jquery.tagsinput-revisited.js')}}"></script>
    <script>
        function showMobileDiv() {
            $('#mobile_numbers_div').show();
        }

        function showMsgDiv() {
            $('#push_message_div').show();
        }
    </script>
    <link rel="stylesheet" href="{{asset('mobile_tags/jquery.tagsinput-revisited.css')}}"/>

    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('offers.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div>{{$error}}</div>
                        @endforeach
                    @endif
                    <div class="card-header">
                        <h4 class="card-title"> Create Offers</h4>
                    </div>
                    <div class="card-body">
                        <form id="offer_create" method="post" action="{{ route('offers.store') }}"
                              enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" Categories")}}</label>
                                        <select class="form-control" id="category_id" name="category_id">
                                            @foreach($categories as $category)
                                                <option
                                                    value="{{$category->id}}" {{ old('category_id')==$category->id?"selected":"" }}>{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'category_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="merchant_id">{{__(" Merchants")}}</label>
                                        <select class="form-control" id="merchant_id" name="merchant_id">
                                            @if(auth()->user()->hasRole("admin"))
                                                @foreach($merchants as $merchant)
                                                    <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach(auth()->user()->merchants()->get() as $merchant)
                                                    <option
                                                        value="{{$merchant->merchant->id}}">{{$merchant->merchant->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'merchant_id'])
                                    </div>
                                </div>
                            </div>
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-md-7 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="title">{{__(" Amount")}}</label>--}}
                            {{--                                        <input type="number" name="amount" class="form-control"--}}
                            {{--                                               value="{{ old('amount') }}">--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'amount'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>1, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/offers'])
                                        <p>Required Dimensions - Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image']  )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="content">{{__(" Content")}}</label>
                                        <textarea type="text" id="content" name="content" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('content') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="is_push">{{__(" Do you want to send a push notification?")}}</label><br>
                                        <input type="radio" id="push_true" name="is_send_push_notification"
                                               value="1" {{ old('is_send_push_notification')=="1"?"checked":"" }}> Yes
                                        <input type="radio" id="push_false" name="is_send_push_notification" value="0"
                                            {{ old('is_send_push_notification')=="0" || empty(old('is_send_push_notification'))?"checked":"" }}>
                                        No
                                        @include('alerts.feedback', ['field' => 'is_push'])
                                    </div>
                                </div>
                            </div>

                            <div id="push_message_div" class="row" style="display:none">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="push_message">{{__(" Push Message")}}</label>
                                        <textarea type="text" id="push_message" name="push_message" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('push_message') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'push_message'])
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="is_target_offer">{{__(" Is a Target Offer?")}}</label><br>
                                        <input type="checkbox" id="is_target_offer" name="is_target_offer"
                                               value="1" {{ old('is_target_offer')=="1"?"checked":"" }}>
                                        @include('alerts.feedback', ['field' => 'is_target_offer'])
                                    </div>
                                </div>
                            </div>

                            <div id="mobile_numbers_div" class="row" style="display:none">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="mobile_numbers">{{__(" Mobile Numbers")}}</label><br>
                                        <div id="form">
                                            <input id="form-tags-1" name="mobile_numbers" type="text"
                                                   value="{{old('mobile_numbers')}}">
                                            @include('alerts.feedback', ['field' => 'mobile_numbers'])
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="mobile_numbers_csv">{{__(" Mobile Numbers CSV")}}</label><br>
                                        <img class="prev_img" style="width: 5%;"
                                             src="{{asset('assets/img/excel-icon.png')}}" onclick="getFile();">
                                        <input type="file" class="custom-file-input" id="mobile_numbers_csv"
                                               name="mobile_numbers_csv">
                                        @include('alerts.feedback', ['field' => 'mobile_numbers_csv'])
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="has_redeem">{{__(" Do you want to redeem?")}}</label><br>
                                        <input type="radio" id="has_redeem" name="has_redeem" value="1" checked> Yes
                                        <input type="radio" id="has_redeem" name="has_redeem" value="0"> No
                                        @include('alerts.feedback', ['field' => 'has_redeem'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="is_best">{{__(" Is this Best Offer?")}}</label><br>
                                        <input type="radio" id="is_best" name="is_best" value="1"> Yes
                                        <input type="radio" id="is_best" name="is_best" value="0" checked> No
                                        @include('alerts.feedback', ['field' => 'is_best'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 pr-1">
                                    <div class="form-group">
                                        <label for="for_male">{{__(" Show this offer for male users?")}}</label><br>
                                        <input type="radio" id="for_male" name="for_male" value="1" checked> Yes
                                        <input type="radio" id="for_male" name="for_male" value="0"> No
                                        @include('alerts.feedback', ['field' => 'for_male'])
                                    </div>
                                </div>
                                <div class="col-md-3 pr-1">
                                    <div class="form-group">
                                        <label for="for_female">{{__(" Show this offer for female users?")}}</label><br>
                                        <input type="radio" name="for_female" value="1" checked> Yes
                                        <input type="radio" name="for_female" value="0"> No
                                        @include('alerts.feedback', ['field' => 'for_female'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 pr-1">
                                    <div class="form-group">
                                        <label
                                            for="min_age">{{__(" Minimum age(years) limit to show the offer")}}</label>
                                        <input type="number" step="1" min="0" id="min_age" name="min_age"
                                               class="form-control" value="{{ old('min_age') }}">
                                        @include('alerts.feedback', ['field' => 'min_age'])
                                    </div>
                                </div>
                                <div class="col-md-3 pr-1">
                                    <div class="form-group">
                                        <label
                                            for="max_age">{{__(" Maximum age(years) limit to show the offer")}}</label>
                                        <input type="number" step="1" min="0" id="max_age" name="max_age"
                                               class="form-control" value="{{ old('max_age') }}">
                                        @include('alerts.feedback', ['field' => 'max_age'])
                                    </div>
                                </div>
                            </div>
                            {{--                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
                            {{--                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-md-7 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="start_datetime">{{__(" Offer Start Date and Time")}}</label>--}}
                            {{--                                        <input id="start_datetime" name="start_datetime" class="form-control" value="{{ old('start_datetime') }}"/>--}}
                            {{--                                        <script>--}}
                            {{--                                            $('#start_datetime').datetimepicker({ format: 'yyyy-mm-dd HH:MM:ss' });--}}
                            {{--                                        </script>--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'start_datetime'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-md-7 pr-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="end_datetime">{{__(" Offer End Date and Time")}}</label>--}}
                            {{--                                        <input id="end_datetime" name="end_datetime" class="form-control" value="{{ old('end_datetime') }}"/>--}}
                            {{--                                        <script>--}}
                            {{--                                            $('#end_datetime').datetimepicker({ format: 'yyyy-mm-dd HH:MM:ss' });--}}
                            {{--                                        </script>--}}
                            {{--                                        @include('alerts.feedback', ['field' => 'end_datetime'])--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        {{ old('is_target_offer')=="1"?"showMobileDiv();":""}}
        {{ old('is_send_push_notification')=="1"?"showMsgDiv();":""}}
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\OfferCreateRequest', '#offer_create') !!}


    <script>

        $(document).ready(function () {
            $('input[type="radio"]').click(function () {
                if ($(this).attr('id') == 'push_true') {
                    $('#push_message_div').show();
                } else if ($(this).attr('id') == 'push_false') {
                    $('#push_message_div').hide();
                }
            });

            $('#is_target_offer').change(function () {
                if (this.checked) {
                    $('#mobile_numbers_div').show();
                } else {
                    $('#mobile_numbers_div').hide();
                }
            });

            $(function () {
                $('#form-tags-1').tagsInput();
            });
        });

        function getFile() {
            $('#mobile_numbers_csv').click();
        }


    </script>
@endsection
