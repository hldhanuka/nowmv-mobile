@extends('layouts.app', [
    'namePage' => 'Recipe Reviews',
    'class' => 'sidebar-mini',
    'activePage' => 'recipe_review',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Recipe Reviews List</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="recipe_reviews_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Recipe Name</th>
                                <th>Comment</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('#recipe_reviews_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('recipe_reviews.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'recipe_id', name: 'recipe_id'},
                    {data: 'comment', name: 'comment'},
                    {data: 'created_user_email', name: 'created_user_email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
