@extends('layouts.app', [
    'namePage' => 'Offer Categories',
    'class' => 'sidebar-mini',
    'activePage' => 'offer_category',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Offer Category List</h4>
                        @if(@auth()->user()->hasPermissionTo('offer_categories.create'))
                            <div class="pull-right">
                                <a href="{{ route('offer_categories.create') }}">
                                    <button class="btn btn-primary">Create</button>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="offer_categories_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Authorized By</th>
                                <th>Published At</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function changeStatus(item_id) {
            Swal.fire({
                title: 'Change Status',
                text: "What do you want to do?",
                icon: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Approve',
                cancelButtonText: 'Reject'
            }).then((result) => {
                if (result.value) {
                    // Press Approve
                    sendAjax(item_id, 1);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    // Press Reject
                    sendAjax(item_id, 3);
                }
            })
        }

        function sendAjax(item_id, status) {
            $.ajax({
                url: "{{route('offer_categories.change-status')}}",
                data: {id: item_id, status: status},
                method: "post",
                success: function (result) {
                    if (result.success) {
                        Swal.fire({
                            title: 'Success',
                            icon: 'success',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire({
                            title: 'Error',
                            icon: 'error',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    }
                }
            });
        }

        $(document).ready(function () {

            $('#offer_categories_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('offer_categories.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'created_user_email', name: 'created_user_email'},
                    {data: 'authorized_user_email', name: 'authorized_user_email'},
                    {data: 'published_at', name: 'published_at'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

    </script>
@endsection
