@extends('layouts.app', [
    'namePage' => 'Restaurant Reviews',
    'class' => 'sidebar-mini',
    'activePage' => 'restaurant_review',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Restaurant Reviews List</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="restaurant_reviews_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Restaurant</th>
                                <th>Comment</th>
                                <th>Created By</th>
                                <th>Authorized By</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('#restaurant_reviews_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('restaurant_reviews.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'restaurant_id', name: 'restaurant_id'},
                    {data: 'comment', name: 'comment'},
                    {data: 'created_user_email', name: 'created_user_email'},
                    {data: 'authorized_user_email', name: 'authorized_user_email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
