@extends('layouts.app', [
    'namePage' => 'Public Images',
    'class' => 'sidebar-mini',
    'activePage' => 'public_images',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Public Images List</h4>
                        <div class="pull-left">
                            <form id="news_create" method="post" action=""
                                  enctype="multipart/form-data">
                                @csrf
                                @include('alerts.success')
                                {{--<div class="row">
                                    <div class="col-md-7 pr-2">
                                        <label class="d-block">Upload Ratio</label>
                                        <select id="ratio" onchange="getComboA(this)" class="form-control">
                                            <option value="0">Custom Ratio</option>
                                            <option value="1">1:1</option>
                                            <option value="2">2:1</option>
                                            <option value="3">3:1</option>
                                            <option value="4">4:1</option>
                                        </select>

                                        <div class="form-group mt-2">
                                            <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                            @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>0, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>('uploads/other_images')])
                                            @include('alerts.feedback', ['field' => 'image'])
                                            @include('alerts.feedback', ['field' => 'image_url'])
                                        </div>
                                    </div>
                                </div>--}}
                                <div class="row">
                                    <div class="col-md-7 pr-1">
                                        <div class="form-group">
                                            <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                            <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                            <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                            @include('alerts.feedback', ['field' => 'image']  )
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                                </div>
                                <hr class="half-rule"/>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="public_images_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Image</th>
                                <th>URL</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function copyToClipboard(url) {
            var dummy = document.createElement("textarea");
            document.body.appendChild(dummy);
            dummy.value = url;
            dummy.select();
            document.execCommand("copy");
            document.body.removeChild(dummy);
            Swal.fire({
                title: 'Success',
                icon: 'success',
                text: url + ' URL copy to clipboard',
            });
        }

        $(document).ready(function () {

            $('#preview_img1').width(400);
            $('#public_images_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('public_images.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'image', name: 'image', orderable: false, searchable: false},
                    {data: 'url', name: 'url'},
                    {data: 'created_user_email', name: 'created_user_email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

        function getComboA(selectObject) {
            var value = selectObject.value;
            setRatio(value);
        }
    </script>
@endsection
