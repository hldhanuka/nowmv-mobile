@extends('layouts.app', [
    'namePage' => 'Ads',
    'class' => 'sidebar-mini',
    'activePage' => 'ads',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ads.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Ad</h4>
                    </div>
                    <div class="card-body">
                        <form id="ad_update" method="post" action="{{ route('ads.update', $ad->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title', $ad->title) }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>2, 'current_image'=>$ad->image!=null?$ad->image:asset('assets/img/dummy.jpg'),'upload_path'=>'upload/ads'])
                                        <p>Required Dimensions - Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{$ad->image!=null?$ad->image:asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <div>
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center item-raw-title">Type</th>
                                                    <th class="text-center item-raw-title">Active Ads Count</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($ad_counts as $ad_count)
                                                    <tr>
                                                        <td class="text-center">{{ $ad_count["type"] }}</td>
                                                        <td class="text-center">{{ $ad_count["total"] }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="type">{{__(" Type")}}</label>
                                        <select name="type" class="form-control">
                                            <option value="food" {{ old('type', $ad->type)=="food"?"selected":"" }}>Food</option>
                                            <option value="news" {{ old('type', $ad->type)=="news"?"selected":"" }}>News</option>
                                            <option value="home" {{ old('type', $ad->type)=="home"?"selected":"" }}>Home</option>
                                            <option value="donation" {{ old('type', $ad->type)=="donation"?"selected":"" }}>Donation</option>
                                            <option value="offer" {{ old('type', $ad->type)=="offer"?"selected":"" }}>Offer</option>
                                            <option value="travel" {{ old('type', $ad->type)=="travel"?"selected":"" }}>Travel</option>
                                            <option value="weather" {{ old('type', $ad->type)=="weather"?"selected":"" }}>Weather</option>
                                        </select>
                                        @include('alerts.feedback', ['field' => 'type'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\AdUpdateRequest', '#ad_update') !!}
@endsection
