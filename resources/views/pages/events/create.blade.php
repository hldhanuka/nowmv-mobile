@extends('layouts.app', [
    'namePage' => 'Events',
    'class' => 'sidebar-mini',
    'activePage' => 'event',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('events.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Event</h4>
                    </div>
                    <div class="card-body">
                        <form id="event_create" method="post" action="{{ route('events.store') }}"
                              enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Event Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Date")}}</label>
                                        <input id="datepicker" name="date" class="form-control" value="{{ old('date') }}"/>
                                        <script>
                                            $('#datepicker').datepicker({ format: 'yyyy-mm-dd' });
                                        </script>
{{--                                        <input type="text" id="date" name="date" class="form-control" value="{{ old('date') }}">--}}
                                        @include('alerts.feedback', ['field' => 'date'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Time")}}</label>
                                        <input id="timepicker" name="time" class="form-control" value="{{ old('time') }}"/>
                                        <script>
                                            $('#timepicker').timepicker({format: 'HH:MM'});
                                        </script>
{{--                                        <input type="text"  id="time" name="time" class="form-control" value="{{ old('time') }}">--}}
                                        @include('alerts.feedback', ['field' => 'time'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>2, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/events'])
                                        <p>Required Dimensions - Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group position-relative">
                                        <div class="input-wrap position-absolute">
                                            <input type="text" id="location_name" class="form-control frm-ctrl"
                                                   placeholder="Search Box">
                                        </div>
                                        {{--Map--}}
                                        <div id="map"
                                             style="border: 2px solid #3872ac;height: 400px;width: 100%;margin: 0;padding: 0;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Longitude")}}</label>
                                        <input type="text" name="longitude" id="longitude" class="form-control" value="{{ old('longitude') }}" readonly>
                                        @include('alerts.feedback', ['field' => 'longitude'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Latitude")}}</label>
                                        <input type="text" name="latitude" id="latitude" class="form-control" value="{{ old('latitude') }}" readonly>
                                        @include('alerts.feedback', ['field' => 'latitude'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Location Name")}}</label>
                                        <input type="text" name="location_name" class="form-control" value="{{ old('location_name') }}">
                                        @include('alerts.feedback', ['field' => 'location_name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="tag">{{__(" Tag")}}</label>
                                        <select class="form-control" id="tag_id" name="tag_id" >
                                            @foreach($tags as $tag)
                                                <option value="{{$tag->id}}">{{$tag->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'tag_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Description")}}</label>
                                        <textarea type="text" name="description" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('description') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'description'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\EventCreateRequest', '#event_create') !!}

    {{--############################## Google map JS Start ##########################--}}
    <script
        src="{!! url('https://maps.google.com/maps/api/js?key='.config('app.google_map_key').'&libraries=places') !!}"></script>
    <script src="{{ asset('js/locationpicker.jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
            let default_latitude = 4.1754695;
            let default_longitude = 73.5093531;

            const bounds = new window.google.maps.LatLngBounds(
                new window.google.maps.LatLng(default_latitude,default_longitude),
            )

            $("#latitude").val(default_latitude);
            $("#longitude").val(default_longitude);
            $('#map').locationpicker({
                location: {
                    latitude: default_latitude,
                    longitude: default_longitude,
                },
                radius: 0,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    locationNameInput: $('#location_name')
                },
                enableAutocomplete: true,
                autocompleteOptions: {
                    bounds: bounds,
                },
                draggable: true,
                markerDraggable: true
            });
        });
    </script>
    {{--############################## Google map JS End ##########################--}}
@endsection
