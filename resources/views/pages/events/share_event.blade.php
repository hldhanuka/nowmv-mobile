<!doctype html>
<html lang="en">
<head>
    <title>Share Event - {{$share_event->name}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{$share_event->description}}">
    <meta property="og:title" content="{{$share_event->name}}" />
    <meta property="og:url" content="{{$url}}" />
    <meta property="description" content="{{$share_event->description}}">
    <meta property="og:image" content="{{$share_event->image}}">
    <meta property="og:type" content="website" />
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="row no-gutters">
    <div class="col-10 col-sm-10 offset-sm-1 offset-1">
        <div class="row">
            <div class="col-md-12 mt-5">
                <img src="{{$share_event->image}}" class="mx-auto d-block img-fluid" style="width: 100%;margin-bottom: 50px">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content_wrapper" style="float: left">
                            <p>{{$share_event->name}}</p>
                            <p>{{$share_event->date}},{{$share_event->time}}</p>
                            <p><img src="{{asset("images/location.png")}}">&nbsp{{$share_event->location_name}}</p>
                        </div>
                    </div>
                </div>
                <div class="description mt-5">
                    <p>{{$share_event->description}}</p>
                </div>
                <div class="row no-gutters justify-content-center">
                    <div class="col-lg-5 col-md-6 col-sm-6 col-10">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="download-text mb-3 text-center"><b>Download Dhiraagu Now MV APP</b></h4>
                            </div>
                            <div class="col-md-6">
                                <a href="https://apps.apple.com/vc/app/dhiraagu-pay/id1257628924">
                                    <img src="{{asset('images/app-store.png')}}" class="w-100 mb-3">
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a href="https://play.google.com/store/apps/details?id=mv.com.dhiraagu.dhiraagupay">
                                    <img src="{{asset('images/play-store.png')}}" class="w-100 mb-3">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
