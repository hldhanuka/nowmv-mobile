@extends('layouts.app', [
    'namePage' => 'Users',
    'class' => 'sidebar-mini',
    'activePage' => 'user',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> User List</h4>
                        <div class="pull-right">
                            <a href="{{ route('users.create') }}">
                                <button class="btn btn-primary">Create</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="users_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Image</th>
                                <th>First Name</th>
                                <th>Roles</th>
                                <th>Email</th>
                                <th>Mobile or Email</th>
                                <th>Gender</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function changeStatus(item_id){
            Swal.fire({
                title: 'Reset Password',
                text: "What do you want to do?",
                icon: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'reset password',
                cancelButtonText: 'Cancel'
            }).then((result) => {
                if (result.value) {
                    // Press Approve
                    sendAjax(item_id);
                }
            })
        }

        function sendAjax(item_id){
            $.ajax({
                url:"{{route('users.reset-password')}}",
                data:{id:item_id},
                method:"post",
                success: function (result) {
                    console.log(result);
                    console.log(result.success);
                    if(result.success){
                        Swal.fire({
                            title:'Success',
                            icon:'success',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire({
                            title:'Error',
                            icon:'error',
                            text: result.message,
                        }).then((result) => {
                            location.reload();
                        });
                    }
                }
            });
        }

        $(document).ready(function () {

            $('#users_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('users.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'image', name: 'image', orderable: false, searchable: false},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'roles', name: 'roles'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'gender', name: 'gender'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
