@extends('layouts.app', [
    'namePage' => 'Update User',
    'class' => 'sidebar-mini',
    'activePage' => 'user',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('users.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update User</h4>
                    </div>
                    <div class="card-body">
                        <label class="d-block" for="title">{{__(" Permissions")}}</label>
                        <form id="user_create" method="post" action="{{ route('users.update', $user->id) }}"
                              enctype="multipart/form-data" onsubmit="return validateForm()">
                            @csrf
                            @method('PUT')
                            @include('alerts.success')
                            <br>
                            <div class="row" id="custom_permissions_div">
                                {{-- ADS Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        ads
                                    @endslot
                                    @slot('title')
                                        ADS
                                    @endslot
                                    @slot('icon')
                                        fas fa-ad
                                    @endslot
                                @endcomponent

                                {{-- Interests Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        interests
                                    @endslot
                                    @slot('title')
                                        INTERESTS
                                    @endslot
                                    @slot('icon')
                                        fab fa-gratipay
                                    @endslot
                                @endcomponent

                                {{-- NGO Merchants Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        ngo_merchants
                                    @endslot
                                    @slot('title')
                                        NGO MERCHANTS
                                    @endslot
                                    @slot('icon')
                                        fas fa-user-tie
                                    @endslot
                                @endcomponent

                                {{-- NGO Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        ngos
                                    @endslot
                                    @slot('title')
                                        NGOS
                                    @endslot
                                    @slot('icon')
                                        fas fa-building
                                    @endslot
                                @endcomponent

                                {{-- Donations Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        donations
                                    @endslot
                                    @slot('title')
                                        DONATIONS
                                    @endslot
                                    @slot('icon')
                                        fas fa-hand-holding-usd
                                    @endslot
                                @endcomponent

                                {{-- Badges Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        badges
                                    @endslot
                                    @slot('title')
                                        BADGES
                                    @endslot
                                    @slot('icon')
                                        fas fa-certificate
                                    @endslot
                                @endcomponent

                                {{-- Schedule Types Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        schedule_types
                                    @endslot
                                    @slot('title')
                                        CALENDAR SCHEDULE TYPES
                                    @endslot
                                    @slot('icon')
                                        fas fa-calendar-day
                                    @endslot
                                @endcomponent

                                {{-- Event Tags Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        event_tags
                                    @endslot
                                    @slot('title')
                                        EVENT TAGS
                                    @endslot
                                    @slot('icon')
                                        far fa-calendar
                                    @endslot
                                @endcomponent

                                {{-- Events Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        events
                                    @endslot
                                    @slot('title')
                                        EVENTS
                                    @endslot
                                    @slot('icon')
                                        fas fa-calendar-check
                                    @endslot
                                @endcomponent

                                {{-- News Agencies Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        news_agencies
                                    @endslot
                                    @slot('title')
                                        NEWS AGENCIES
                                    @endslot
                                    @slot('icon')
                                        far fa-newspaper
                                    @endslot
                                @endcomponent

                                {{-- News Categories Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        news_categories
                                    @endslot
                                    @slot('title')
                                        NEWS CATEGORIES
                                    @endslot
                                    @slot('icon')
                                        fas fa-newspaper
                                    @endslot
                                @endcomponent

                                {{-- News Tags Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        news_tags
                                    @endslot
                                    @slot('title')
                                        NEWS TAGS
                                    @endslot
                                    @slot('icon')
                                        fas fa-tag
                                    @endslot
                                @endcomponent

                                {{-- News Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        news
                                    @endslot
                                    @slot('title')
                                        NEWS
                                    @endslot
                                    @slot('icon')
                                        far fa-newspaper
                                    @endslot
                                @endcomponent

                                {{-- Merchants Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        merchants
                                    @endslot
                                    @slot('title')
                                        MERCHANTS
                                    @endslot
                                    @slot('icon')
                                        fas fa-user-tie
                                    @endslot
                                @endcomponent

                                {{-- Restaurants Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        restaurants
                                    @endslot
                                    @slot('title')
                                        RESTAURANTS
                                    @endslot
                                    @slot('icon')
                                        fas fa-utensils
                                    @endslot
                                @endcomponent

                                {{-- Restaurants Reviews Permissions --}}
                                @component('components.permission', ['types' => ['list', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        restaurant_reviews
                                    @endslot
                                    @slot('title')
                                        RESTAURANT REVIEWS
                                    @endslot
                                    @slot('icon')
                                        fas fa-carrot
                                    @endslot
                                @endcomponent

                                {{-- Meal Categories Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        meal_categories
                                    @endslot
                                    @slot('title')
                                        MEAL CATEGORIES
                                    @endslot
                                    @slot('icon')
                                        fas fa-hotdog
                                    @endslot
                                @endcomponent

                                {{-- Meals Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        meals
                                    @endslot
                                    @slot('title')
                                        MEALS
                                    @endslot
                                    @slot('icon')
                                        fas fa-hamburger
                                    @endslot
                                @endcomponent

                                {{-- Meal Reviews Permissions --}}
                                @component('components.permission', ['types' => ['list', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        meal_reviews
                                    @endslot
                                    @slot('title')
                                        MEAL REVIEWS
                                    @endslot
                                    @slot('icon')
                                        fas fa-cheese
                                    @endslot
                                @endcomponent

                                {{-- Recipes Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        recipes
                                    @endslot
                                    @slot('title')
                                        RECIPES
                                    @endslot
                                    @slot('icon')
                                        fas fa-list-alt
                                    @endslot
                                @endcomponent

                                {{-- Recipe Reviews Permissions --}}
                                @component('components.permission', ['types' => ['list', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        recipe_reviews
                                    @endslot
                                    @slot('title')
                                        RECIPE REVIEWS
                                    @endslot
                                    @slot('icon')
                                        fas fa-pizza-slice
                                    @endslot
                                @endcomponent

                                {{-- Offer Categories Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        offer_categories
                                    @endslot
                                    @slot('title')
                                        OFFER CATEGORIES
                                    @endslot
                                    @slot('icon')
                                        fas fa-percentage
                                    @endslot
                                @endcomponent

                                {{-- Offers Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        offers
                                    @endslot
                                    @slot('title')
                                        OFFERS
                                    @endslot
                                    @slot('icon')
                                        fas fa-percent
                                    @endslot
                                @endcomponent

                                {{-- Prayer Tips Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        prayer_tips
                                    @endslot
                                    @slot('title')
                                        PRAYER TIPS
                                    @endslot
                                    @slot('icon')
                                        fas fa-pray
                                    @endslot
                                @endcomponent

                                {{-- Zakat Payments Permissions --}}
                                @component('components.permission', ['types' => ['list', 'csv'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        zakat_payments
                                    @endslot
                                    @slot('title')
                                        ZAKAT PAYMENTS
                                    @endslot
                                    @slot('icon')
                                        fab fa-cc-amazon-pay
                                    @endslot
                                @endcomponent

                                {{-- Support Team Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        support_teams
                                    @endslot
                                    @slot('title')
                                        SUPPORT TEAMS
                                    @endslot
                                    @slot('icon')
                                        fas fa-users
                                    @endslot
                                @endcomponent

                                {{-- Videos Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        videos
                                    @endslot
                                    @slot('title')
                                        VIDEOS
                                    @endslot
                                    @slot('icon')
                                        fas fa-video
                                    @endslot
                                @endcomponent

                                {{-- Users Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create', 'update', 'delete', 'approve'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        users
                                    @endslot
                                    @slot('title')
                                        USERS
                                    @endslot
                                    @slot('icon')
                                        fas fa-user
                                    @endslot
                                @endcomponent

                                {{-- Public Images Permissions --}}
                                @component('components.permission', ['types' => ['list', 'create','delete'], 'show_check_all' => true, 'permissions' => $permissions])
                                    @slot('permission_name')
                                        public_images
                                    @endslot
                                    @slot('title')
                                        PUBLIC IMAGES
                                    @endslot
                                    @slot('icon')
                                        fa fa-image
                                    @endslot
                                @endcomponent

                                {{-- Other Permissions --}}
                                <div class="col-sm-6 col-lg-3 d-flex">
                                    <div class="card" id="other_permissions">
                                        <div class="card-header">
                                            <i class="fas fa-cogs"></i>
                                            SETTINGS
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox"
                                                       id="settings_update"
                                                       name="settings[update]"
                                                       value="1" @if($permissions) {{ ( (old("settings.update", $permissions->contains("name","settings.update"))) ? 'checked' : '') }} @else {{ (! empty(old("settings.update")) ? 'checked' : '') }} @endif>
                                                Update
                                                <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                            </label>
                                        </div>
                                        <br>
                                        <div class="card-header">
                                            <i class="fas fa-envelope-square"></i>
                                            PUSH NOTIFICATIONS
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox"
                                                       id="push_notifications_create"
                                                       name="push_notifications[create]"
                                                       value="1" @if($permissions) {{ ( (old("push_notifications.create", $permissions->contains("name","push_notifications.create"))) ? 'checked' : '') }} @else {{ (! empty(old("push_notifications.create")) ? 'checked' : '') }} @endif>
                                                Create
                                                <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                            </label>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="news_agency_id_div">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="restaurant_id">{{__(" News Agencies")}}</label>
                                        <select class="form-control" id="news_agency_ids" name="news_agency_ids[]"
                                                multiple="multiple">
                                            @foreach($news_agencies as $news_agency)
                                                <option
                                                    value="{{$news_agency->id}}" {{ in_array($news_agency->id, Arr::pluck($news_agency_ids, "news_agency_id")) ? "selected":"" }} >{{$news_agency->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'news_agency_ids'])
                                        <script>
                                            $(document).ready(function () {
                                                $('#news_agency_ids').select2();
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="merchant_id_div">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="merchant_ids">{{__(" Merchants")}}</label>
                                        <select class="form-control" id="merchant_ids" name="merchant_ids[]"
                                                multiple="multiple">
                                            @foreach($merchants as $merchant)
                                                <option
                                                    value="{{$merchant->id}}" {{ in_array($merchant->id, Arr::pluck($merchant_ids, "merchant_id")) ? "selected":"" }} >{{$merchant->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'merchant_ids'])
                                        <script>
                                            $(document).ready(function () {
                                                $('#merchant_ids').select2();
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="ngo_merchant_id_div">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ngo_merchant_ids">{{__(" NGO Merchants")}}</label>
                                        <select class="form-control" id="ngo_merchant_ids" name="ngo_merchant_ids[]"
                                                multiple="multiple">
                                            @foreach($ngo_merchants as $ngo_merchant)
                                                <option
                                                    value="{{$ngo_merchant->id}}" {{ in_array($ngo_merchant->id, Arr::pluck($ngo_merchant_ids, "ngo_merchant_id")) ? "selected":"" }}>{{$ngo_merchant->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ngo_merchant_ids'])
                                        <script>
                                            $(document).ready(function () {
                                                $('#ngo_merchant_ids').select2();
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Profile Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img"
                                             src="{{$user->image!=null?$user->image:asset('uploads/profile/default.jpg')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" First Name")}}</label>
                                        <input type="text" name="first_name" class="form-control"
                                               value="{{ $user->first_name }}" disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Last Name")}}</label>
                                        <input type="text" name="last_name" class="form-control"
                                               value="{{ $user->last_name }}" disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Email")}}</label>
                                        <input type="email" name="email" class="form-control"
                                               value="{{ $user->email }}" disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Mobile")}}</label>
                                        <input type="text" name="mobile" class="form-control"
                                               value="{{ $user->mobile }}" disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Address")}}</label>
                                        <input type="text" name="address" class="form-control"
                                               value="{{ $user->address }}" disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Birthday")}}</label>
                                        <input id="datepicker" name="birthday" class="form-control"
                                               value="{{ $user->birthday }}" disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" Gender")}}</label>
                                        <select class="form-control" id="status" name="gender" disabled="disabled">
                                            <option value="male" {{ $user->gender == 'male' ? 'selected' : '' }} >Male
                                            </option>
                                            <option value="female" {{ $user->gender == 'female' ? 'selected' : '' }}>
                                                Female
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function validateForm() {
            if ($("#custom_permissions_div input[type=checkbox]:checked").length == 0) {
                Swal.fire({
                    title: 'Error',
                    text: "Select at least one permission",
                    confirmButtonText: 'OK'
                });
                return false
            }
        }

        $(document).ready(function () {
            let old_news_agency_ids = @json(old("news_agency_ids"));
            let old_merchant_ids = @json(old("merchant_ids"));
            let old_ngo_merchant_ids = @json(old("ngo_merchant_ids"));

            var $news_agency_ids = {!! $news_agency_ids !!};
            var $merchant_ids = {!! $merchant_ids !!};
            var $ngo_merchant_ids = {!! $ngo_merchant_ids !!};

            let news_agency_required = @json(old("news_categories")) || @json(old("news_tags")) || @json(old("news")) ||
            $news_agency_ids[0];

            let merchant_required = @json(old("restaurants")) || @json(old("restaurant_reviews")) || @json(old("meal_categories")) || @json(old("meals")) || @json(old("meal_reviews")) || @json(old("recipes")) || @json(old("recipe_reviews")) || @json(old("offer_categories")) || @json(old("offers")) ||
            $merchant_ids[0];

            let ngo_merchant_required = @json(old("ngos")) || @json(old("donations")) || @json(old("badges")) ||
            $ngo_merchant_ids[0];


            if ((ngo_merchant_required) && (news_agency_required) && (merchant_required)) {
                showAll();
            } else if ((news_agency_required) && (merchant_required)) {
                showNewsAgencyAndMerchant()
            } else if ((ngo_merchant_required) && (news_agency_required)) {
                showNewsAgencyAndNgoMerchant();
            } else if ((ngo_merchant_required) && (merchant_required)) {
                showMerchantAndNgoMerchant();
            } else if (ngo_merchant_required) {
                showCustomAndNgoMerchant();
            } else if (news_agency_required) {
                showCustomAndNewsAgency();
            } else if (merchant_required) {
                showCustomAndMerchant();
            } else {
                showCustom();
            }

            //Show and Hide Ngo Merchants Field
            $("#ngos_permissions input[type=checkbox], #donations_permissions input[type=checkbox], #badges_permissions input[type=checkbox]").on('change', function () {
                var count = 0;
                $("#ngos_permissions input[type=checkbox]:checked, #donations_permissions input[type=checkbox]:checked, #badges_permissions input[type=checkbox]:checked").each(function () {
                    count++
                });
                if (count > 0) {
                    $("#ngo_merchant_id_div").show();
                } else {
                    $("#ngo_merchant_id_div").hide();
                }
            });

            //Show and Hide News Agencies Field
            $("#news_categories_permissions input[type=checkbox], #news_tags_permissions input[type=checkbox], #news_permissions input[type=checkbox]").on('change', function () {
                var count = 0;
                $("#news_categories_permissions input[type=checkbox]:checked, #news_tags_permissions input[type=checkbox]:checked, #news_permissions input[type=checkbox]:checked").each(function () {
                    count++
                });
                if (count > 0) {
                    $("#news_agency_id_div").show();
                } else {
                    $("#news_agency_id_div").hide();
                }
            });

            //Show and Hide Merchants Field
            $("#restaurants_permissions input[type=checkbox], #restaurant_reviews_permissions input[type=checkbox], #meal_categories_permissions input[type=checkbox], #meals_permissions input[type=checkbox], #meal_reviews_permissions input[type=checkbox], #recipes_permissions input[type=checkbox], #recipe_reviews_permissions input[type=checkbox], #offer_categories_permissions input[type=checkbox], #offers_permissions input[type=checkbox]").on('change', function () {
                var count = 0;
                $("#restaurants_permissions input[type=checkbox]:checked, #restaurant_reviews_permissions input[type=checkbox]:checked, #meal_categories_permissions input[type=checkbox]:checked, #meals_permissions input[type=checkbox]:checked, #meal_reviews_permissions input[type=checkbox]:checked, #recipes_permissions input[type=checkbox]:checked, #recipe_reviews_permissions input[type=checkbox]:checked, #offer_categories_permissions input[type=checkbox]:checked, #offers_permissions input[type=checkbox]:checked").each(function () {
                    count++
                });
                if (count > 0) {
                    $("#merchant_id_div").show();
                } else {
                    $("#merchant_id_div").hide();
                }
            });

            function showCustom() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").hide();
                $("#merchant_id_div").hide();
                $("#news_agency_id_div").hide();
            }

            function showCustomAndMerchant() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").hide();
                $("#merchant_id_div").show();
                $("#news_agency_id_div").hide();
            }

            function showCustomAndNewsAgency() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").hide();
                $("#merchant_id_div").hide();
                $("#news_agency_id_div").show();
            }

            function showCustomAndNgoMerchant() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").show();
                $("#merchant_id_div").hide();
                $("#news_agency_id_div").hide();
            }

            function showNewsAgencyAndMerchant() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").hide();
                $("#merchant_id_div").show();
                $("#news_agency_id_div").show();
            }

            function showNewsAgencyAndNgoMerchant() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").show();
                $("#merchant_id_div").hide();
                $("#news_agency_id_div").show();
            }

            function showMerchantAndNgoMerchant() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").show();
                $("#merchant_id_div").show();
                $("#news_agency_id_div").hide();
            }

            function showAll() {
                $("#custom_permissions_div").show();
                $("#ngo_merchant_id_div").show();
                $("#merchant_id_div").show();
                $("#news_agency_id_div").show();
            }

        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\UserUpdateRequest', '#user_update') !!}
@endsection
