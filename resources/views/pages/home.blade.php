@extends('layouts.app', [
    'namePage' => 'Dashboard',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'home',
    'backgroundImage' => asset('assets/img/maldives-1.jpg'),
])

@section('content')

    @role('admin')
        <div class="panel-header panel-header-lg">
            <canvas id="topDashboardChart"></canvas>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-chart">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fas fa-percent"></i> Offer Payments</h4>

                        </div>
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="offerChart"></canvas>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="now-ui-icons ui-2_time-alarm"></i> Last 7 days
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-chart">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fas fa-hand-holding-usd"></i> Donation Payments</h4>
                        </div>
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="donationChart"></canvas>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="now-ui-icons ui-2_time-alarm"></i> Last 7 days
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="card card-chart">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fas fa-utensils"></i> Restaurant Ratings</h4>

                        </div>
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="restaurantChart"></canvas>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="fas fa-certificate"></i> Top 10 restaurants
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-chart">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fas fa-hamburger"></i> Meal Ratings</h4>
                        </div>
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="mealChart"></canvas>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="fas fa-certificate"></i> Top 10 meals
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @else
        <div class="panel-header"></div>
        <div class="content" style="min-height: calc(100vh - 300px);">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card card-chart">
                        <div class="card-header">
                            <div class="logo text-center">
                                <a href="{{ route('home') }}" class="simple-text logo-normal">
                                    <img id="" src="{{ asset("assets/img/120x120.png") }}" alt="logo">
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ auth()->user()->email }}</h5>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <a class="btn btn-primary col-md-4" href="{{ route('profile.edit') }}">{{ __("Edit profile") }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endrole
@endsection

@role('admin')
    @push('js')
        <script>
            let date_array = @json($date_array);
            let count_array = @json($count_array);
            let last_7_day_array = @json($last_7_day_array);
            let last_7_day_user_offers_array = @json($last_7_day_user_offers_array);
            let last_7_day_user_donations_array = @json($last_7_day_user_donations_array);
            let restaurants = @json($restaurants);
            let meals = @json($meals);

            $(document).ready(function () {
                // Javascript method's body can be found in assets/js/demos.js
                //demo.initDashboardPageCharts();

                let ctx0 = document.getElementById('topDashboardChart').getContext("2d");
                let chartColor = "#FFFFFF";
                let gradientFill0 = ctx0.createLinearGradient(0, 200, 0, 50);
                gradientFill0.addColorStop(0, "rgba(128, 182, 244, 0)");
                gradientFill0.addColorStop(1, "rgba(255, 255, 255, 0.24)");
                let topDashboardChart = new Chart(ctx0, {
                    type: 'line',
                    data: {
                        labels: date_array,
                        datasets: [{
                            label: "Data",
                            borderColor: chartColor,
                            pointBorderColor: chartColor,
                            pointBackgroundColor: "#1e3d60",
                            pointHoverBackgroundColor: "#1e3d60",
                            pointHoverBorderColor: chartColor,
                            pointBorderWidth: 1,
                            pointHoverRadius: 7,
                            pointHoverBorderWidth: 2,
                            pointRadius: 5,
                            fill: true,
                            backgroundColor: gradientFill0,
                            borderWidth: 2,
                            data: count_array
                        }]
                    },
                    options: {
                        layout: {
                            padding: {
                                left: 20,
                                right: 20,
                                top: 0,
                                bottom: 0
                            }
                        },
                        maintainAspectRatio: false,
                        tooltips: {
                            backgroundColor: '#fff',
                            titleFontColor: '#333',
                            bodyFontColor: '#666',
                            bodySpacing: 4,
                            xPadding: 12,
                            mode: "nearest",
                            intersect: 0,
                            position: "nearest"
                        },
                        legend: {
                            position: "bottom",
                            fillStyle: "#FFF",
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    fontColor: "rgba(255,255,255,0.4)",
                                    fontStyle: "bold",
                                    beginAtZero: true,
                                    maxTicksLimit: 5,
                                    padding: 10
                                },
                                gridLines: {
                                    drawTicks: true,
                                    drawBorder: false,
                                    display: true,
                                    color: "rgba(255,255,255,0.1)",
                                    zeroLineColor: "transparent"
                                }

                            }],
                            xAxes: [{
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    display: false,

                                },
                                ticks: {
                                    padding: 10,
                                    fontColor: "rgba(255,255,255,0.4)",
                                    fontStyle: "bold"
                                }
                            }]
                        }
                    }
                });


                let ctx1 = document.getElementById('offerChart').getContext("2d");
                let gradientFill1 = ctx1.createLinearGradient(0, 170, 0, 50);
                gradientFill1.addColorStop(0, "rgba(128, 182, 244, 0)");
                gradientFill1.addColorStop(1, "rgba(249, 99, 59, 0.40)");
                let offerChart = new Chart(ctx1, {
                    type: 'line',
                    responsive: true,
                    data: {
                        labels: last_7_day_array,
                        datasets: [{
                            label: "Payment count",
                            borderColor: "#f96332",
                            pointBorderColor: "#FFF",
                            pointBackgroundColor: "#f96332",
                            pointBorderWidth: 2,
                            pointHoverRadius: 4,
                            pointHoverBorderWidth: 1,
                            pointRadius: 4,
                            fill: true,
                            backgroundColor: gradientFill1,
                            borderWidth: 2,
                            data: last_7_day_user_offers_array
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: true
                        },
                        tooltips: {
                            bodySpacing: 4,
                            mode: "nearest",
                            intersect: 0,
                            position: "nearest",
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        responsive: 1,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        if (value % 1 === 0) {
                                            return value;
                                        }
                                    }
                                },
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }]
                        },
                        layout: {
                            padding: {left: 0, right: 0, top: 15, bottom: 15}
                        }
                    }
                });


                let ctx2 = document.getElementById('donationChart').getContext("2d");
                let gradientFill2 = ctx2.createLinearGradient(0, 170, 0, 50);
                gradientFill2.addColorStop(0, "rgba(128, 182, 244, 0)");
                gradientFill2.addColorStop(1, hexToRGB('#18ce0f', 0.4));
                let donationChart = new Chart(ctx2, {
                    type: 'line',
                    responsive: true,
                    data: {
                        labels: last_7_day_array,
                        datasets: [{
                            label: "Donation Count",
                            borderColor: "#18ce0f",
                            pointBorderColor: "#FFF",
                            pointBackgroundColor: "#18ce0f",
                            pointBorderWidth: 2,
                            pointHoverRadius: 4,
                            pointHoverBorderWidth: 1,
                            pointRadius: 4,
                            fill: true,
                            backgroundColor: gradientFill2,
                            borderWidth: 2,
                            data: last_7_day_user_donations_array
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: true
                        },
                        tooltips: {
                            bodySpacing: 4,
                            mode: "nearest",
                            intersect: 0,
                            position: "nearest",
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        if (value % 1 === 0) {
                                            return value;
                                        }
                                    }
                                },
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }]
                        },
                        layout: {
                            padding: {left: 0, right: 0, top: 15, bottom: 15}
                        }
                    }
                });


                let ctx3 = document.getElementById('restaurantChart').getContext("2d");
                let gradientFill3 = ctx3.createLinearGradient(0, 170, 0, 50);
                gradientFill3.addColorStop(0, "rgba(128, 182, 244, 0)");
                gradientFill3.addColorStop(1, "rgba(249, 99, 59, 0.40)");
                let restaurantChart = new Chart(ctx3, {
                    type: 'bar',
                    responsive: true,
                    data: {
                        labels: restaurants.map(a => a.title),
                        datasets: [{
                            label: "Rating count",
                            borderColor: "#66b3ff",
                            pointBorderColor: "#0059b3",
                            pointBackgroundColor: "#003366",
                            pointBorderWidth: 2,
                            pointHoverRadius: 4,
                            pointHoverBorderWidth: 1,
                            pointRadius: 4,
                            fill: true,
                            backgroundColor: gradientFill1,
                            borderWidth: 2,
                            data: restaurants.map(a => a.average_rating)
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: true
                        },
                        responsive: 1,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        if (value % 1 === 0) {
                                            return value;
                                        }
                                    }
                                },
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    callback: function(t) {
                                        var maxLabelLength = 3;
                                        if (t.length > maxLabelLength) return t.substr(0, maxLabelLength) + '...';
                                        else return t;
                                    }
                                },
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }]
                        },
                        tooltips: {
                            bodySpacing: 4,
                            mode: "point",
                            intersect: 0,
                            position: "nearest",
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10,
                            callbacks: {
                                title: function(t, d) {
                                    return d.labels[t[0].index];
                                }
                            }
                        },
                        layout: {
                            padding: {left: 0, right: 0, top: 15, bottom: 15}
                        }
                    }
                });

                let ctx4 = document.getElementById('mealChart').getContext("2d");
                let gradientFill4 = ctx4.createLinearGradient(0, 170, 0, 50);
                gradientFill4.addColorStop(0, "rgba(128, 182, 244, 0)");
                gradientFill4.addColorStop(1, hexToRGB('#18ce0f', 0.4));
                let mealChart = new Chart(ctx4, {
                    type: 'bar',
                    responsive: true,
                    data: {
                        labels: meals.map(a => a.title),
                        datasets: [{
                            label: "Rating Count",
                            borderColor: "#d24dff",
                            pointBorderColor: "#ac00e6",
                            pointBackgroundColor: "#730099",
                            pointBorderWidth: 2,
                            pointHoverRadius: 4,
                            pointHoverBorderWidth: 1,
                            pointRadius: 4,
                            fill: true,
                            backgroundColor: gradientFill2,
                            borderWidth: 2,
                            data: meals.map(a => a.average_rating)
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: true
                        },
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        if (value % 1 === 0) {
                                            return value;
                                        }
                                    }
                                },
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    callback: function(t) {
                                        var maxLabelLength = 3;
                                        if (t.length > maxLabelLength) return t.substr(0, maxLabelLength) + '...';
                                        else return t;
                                    }
                                },
                                gridLines: {
                                    zeroLineColor: "transparent",
                                    drawBorder: false
                                }
                            }]
                        },
                        tooltips: {
                            bodySpacing: 4,
                            mode: "point",
                            intersect: 0,
                            position: "nearest",
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10,
                            callbacks: {
                                title: function(t, d) {
                                    return d.labels[t[0].index];
                                }
                            }
                        },
                        layout: {
                            padding: {left: 0, right: 0, top: 15, bottom: 15}
                        }
                    }
                });
            });




        </script>
    @endpush
@endrole
