<!doctype html>
<html lang="en">
<head>
    <title>Share Badge - {{$share_badge->title}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{$share_badge->description}}">
    <meta property="og:title" content="{{$share_badge->title}}" />
    <meta property="og:url" content="{{$url}}" />
    <meta property="og:description" content="{{$share_badge->description}}">
    <meta property="og:image" content="{{$share_badge->image}}">
    <meta property="og:type" content="website" />
</head>
<body class="bg-image">
<div class="modal">
    <div class="modal-body">
        <img class="modal-icon" src="{{$share_badge->image}}" alt="Trophy"/>
        <div class="modal-header"><h6>Congratulations</h6></div>
        <div class="modal-subheader">{{$share_badge->description}}</div>
    </div>
</div>
<div class="row no-gutters justify-content-center">
    <div class="col-lg-5 col-md-6 col-sm-6 col-10">
        <div class="row">
            <div class="col-md-12">
                <h4 class="download-text mb-3 text-center"><b>Download Dhiraagu Now MV APP</b></h4>
            </div>
            <div class="col-md-6">
                <a href="https://apps.apple.com/vc/app/dhiraagu-pay/id1257628924">
                    <img src="{{asset('images/app-store.png')}}" class="w-100 mb-3">
                </a>
            </div>
            <div class="col-md-6">
                <a href="https://play.google.com/store/apps/details?id=mv.com.dhiraagu.dhiraagupay">
                    <img src="{{asset('images/play-store.png')}}" class="w-100 mb-3">
                </a>
            </div>
        </div>
    </div>
</div>
</body>
<style>
    /* Global
    ====================== */
    html {
        box-sizing: border-box;
        height: 100%;
    }

    }
    body {
        font-family: Helvetica, sans-serif !important;
    }

    body,
    .wrapper {
        min-height: 100vh;
    }

    /* Modal
    ====================== */
    .bg-image {
        display: flex;
        justify-content: center;
        align-items: center;
        background-size: cover;
    }

    .modal {
        width: 100%;
        max-width: 530px;
        margin: 20px;
        overflow: hidden;
        box-shadow: 0 60px 80px 0 rgba(0, 0, 0, .4);
    }

    .modal-body {
        padding: 6rem 2rem;
        background-color: #fff;
        background-image: url("{{asset("uploads/confetti.png")}}");
        background-repeat: no-repeat;
        background-size: cover;
    }

    .modal-icon {
        display: block;
        max-width: 207px;
        margin: 0 auto 65px;
    }

    .modal-header {
        margin-bottom: 10px;
        font-size: 50px;
        letter-spacing: 2px;
        text-align: center;
    }

    .modal-subheader {
        max-width: 350px;
        margin: 0 auto;
        font-size: 19px;
        line-height: 1.3;
        letter-spacing: 1.25px;
        text-align: center;
        color: black;
        font-weight: bold;
    }

    .modal-bottom {
        display: flex;
        flex-wrap: wrap;
        padding: 15px 55px;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
        background: #fff;
    }

    .modal-btn {
        margin: 10px auto;
    }

    /* Media Queries */
    @media (min-width: 456px) {
        .modal-top {
            padding: 69px 0 65px;
        }

        .modal-bottom {
            padding: 30px 60px;
        }

        .modal-btn {
            flex: 1;
            margin: 0;
        }
    }

</style>
</html>
