@extends('layouts.app', [
    'namePage' => 'Badges',
    'class' => 'sidebar-mini',
    'activePage' => 'badge',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('badges.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update Badge</h4>
                    </div>
                    <div class="card-body">
                        <form id="badge_update" method="post" action="{{ route('badges.update', $badge->id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <input name="donation_badge_id" type="hidden"
                                   value="{{ isset($donation_badge)?$donation_badge->id:'' }}">
                            <input name="ngo_badge_id" type="hidden" value="{{ isset($ngo_badge)?$ngo_badge->id:'' }}">
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control"
                                               value="{{ old('title', $badge->title) }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Description")}}</label>
                                        <input type="text" name="description" class="form-control"
                                               value="{{ old('description', $badge->description) }}">
                                        @include('alerts.feedback', ['field' => 'description'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>1, 'current_image'=>$badge->image!=null?$badge->image:asset('assets/img/dummy.jpg'),'upload_path'=>'upload/badges'])
                                        <p>Required Dimensions - Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{$badge->image!=null?$badge->image:asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="type">{{__(" Type")}}</label>
                                        <input type="text" id="type" name="type" class="form-control" readonly
                                               value="{{ old('type', $badge->type) }}">
                                        @include('alerts.feedback', ['field' => 'type'])
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="ngo_raw">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ngo_id">{{__(" NGO")}}</label>
                                        <select class="form-control" id="ngo_id" name="ngo_id">
                                            @foreach($ngos as $ngo)
                                                <option
                                                    value="{{$ngo->id}}" {{ isset($ngo_badge)&&$ngo->id==$ngo_badge->ngo_id?"selected":"" }}>{{$ngo->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ngo_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="donation_raw">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" Donation Campaign")}}</label>
                                        <select class="form-control" id="donation_id" name="donation_id">
                                            @foreach($donations as $donation)
                                                <option
                                                    value="{{$donation->id}}" {{isset($donation_badge)&&$donation->id==$donation_badge->donation_id?"selected":""}}>{{$donation->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'donation_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Winning Limit")}}</label>
                                        @php
                                            if(isset($ngo_badge)){
                                                $winning_limit = $ngo_badge->winning_limit;
                                            } else if(isset($donation_badge)){
                                                $winning_limit = $donation_badge->winning_limit;
                                            } else {
                                                $winning_limit = "";
                                            }
                                        @endphp
                                        <input type="text" name="winning_limit" class="form-control"
                                               value="{{ old('winning_limit', $winning_limit) }}">
                                        @include('alerts.feedback', ['field' => 'winning_limit'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\BadgeUpdateRequest', '#badge_update') !!}

    <script type="text/javascript">
        $(document).ready(function () {
            changeType();

            $(document).on("change", "#type", function () {
                changeType()
            });

            function changeType() {
                let type = $("#type").val();
                if (type === "ngo") {
                    $("#ngo_raw").show();
                    $("#donation_raw").hide();
                } else if (type === "donation") {
                    $("#donation_raw").show();
                    $("#ngo_raw").hide();
                }
            }

        });
    </script>
@endsection
