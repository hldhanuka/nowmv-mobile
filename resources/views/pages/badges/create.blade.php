@extends('layouts.app', [
    'namePage' => 'Badges',
    'class' => 'sidebar-mini',
    'activePage' => 'badge',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('badges.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Badge</h4>
                    </div>
                    <div class="card-body">
                        <form id="badge_create" method="post" action="{{ route('badges.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Description")}}</label>
                                        <input type="text" name="description" class="form-control" value="{{ old('description') }}">
                                        @include('alerts.feedback', ['field' => 'description'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>1, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/badges'])
                                        <p>Required Dimensions - Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="type">{{__(" Type")}}</label>
                                        <select class="form-control" id="type" name="type">
                                            <option value="ngo" {{ old('type')=="ngo"?"selected":"" }}>NGO</option>
                                            <option value="donation" {{ old('type')=="donation"?"selected":"" }}>Donation Campaign</option>
                                        </select>
                                        @include('alerts.feedback', ['field' => 'type'])
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="ngo_raw">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ngo_id">{{__(" NGO")}}</label>
                                        <select class="form-control" id="ngo_id" name="ngo_id" >
                                            @foreach($ngos as $ngo)
                                                <option value="{{$ngo->id}}" {{ old('ngo_id')==$ngo->id?"selected":"" }}>{{$ngo->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ngo_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="donation_raw">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="donation_id">{{__(" Donation Campaign")}}</label>
                                        <select class="form-control" id="donation_id" name="donation_id">
                                            @foreach($donations as $donation)
                                                <option value="{{$donation->id}}" {{ old('donation_id')==$donation->id?"selected":"" }}>{{$donation->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'donation_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Winning Limit")}}</label>
                                        <input type="text" name="winning_limit" class="form-control" value="{{ old('winning_limit') }}">
                                        @include('alerts.feedback', ['field' => 'winning_limit'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\BadgeCreateRequest', '#badge_create') !!}

    <script type="text/javascript">
        $(document).ready(function(){
            changeType();

            $(document).on("change","#type",function(){
                changeType()
            });

            function changeType(){
                let type = $("#type").val();
                if(type==="ngo"){
                    $("#ngo_raw").show();
                    $("#donation_raw").hide();
                } else if(type==="donation"){
                    $("#donation_raw").show();
                    $("#ngo_raw").hide();
                }
            }

        });
    </script>
@endsection
