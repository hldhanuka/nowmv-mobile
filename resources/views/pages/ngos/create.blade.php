@extends('layouts.app', [
    'namePage' => 'Ngo',
    'class' => 'sidebar-mini',
    'activePage' => 'ngo',
  ])
@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ngos.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title" id="test"> Create NGO </h4>
                    </div>
                    <div class="card-body">
                        <form id="ngo_create" method="post" action="{{ route('ngos.store') }}"
                              enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="merchant_id">{{__(" NGO Merchants")}}</label>
                                        <select class="form-control" id="ngo_merchant_id" name="ngo_merchant_id">
                                            @if(auth()->user()->hasRole("admin"))
                                                @foreach($ngo_merchants as $ngo_merchant)
                                                    <option
                                                        value="{{$ngo_merchant->id}}">{{$ngo_merchant->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach(auth()->user()->ngoMerchants()->get() as $ngo_merchant)
                                                    <option
                                                        value="{{$ngo_merchant->ngoMerchant->id}}">{{$ngo_merchant->ngoMerchant->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ngo_merchant_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Donation Maximum Limit")}}</label>
                                        <input type="number" name="max_limit" class="form-control"
                                               value="{{ old('max_limit') }}">
                                        @include('alerts.feedback', ['field' => 'max_limit'])
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="row">
                                 <div class="col-md-7 pr-1">
                                     <div class="form-group">
                                         <label class="d-block" for="title">{{__(" Upload Logo")}}</label>
                                         @include('includes.cropping_tool', ['id'=>1, 'name'=>'logo', 'ratio'=>1, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/ngos'])
                                         <p>Required Dimensions - Ratio 1:1 (width:height)</p>
                                         @include('alerts.feedback', ['field' => 'logo_url'])
                                     </div>
                                 </div>
                             </div>--}}
                            {{--<div id="img-upload" class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label
                                            for="images">{{__(" Slider Images (Drag & Drop or click the white space to browse)")}}</label>
                                        <div class="input-images-1" style="padding-top: .5rem;"></div>
                                        <p>Required Dimensions Ratio 3:2 (width:height) and Maximum Image Count is 3</p>
                                        @include('alerts.feedback', ['field' => 'images'])
                                        @include('alerts.feedback', ['field' => 'images.*'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Logo")}}</label>
                                        <img class="gal-img prev_img" id="prev_img"
                                             src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="logo"
                                               required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'logo'])
                                    </div>
                                </div>
                            </div>
                            <div id="img-upload" class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label
                                            for="images">{{__(" Slider Images (Drag & Drop or click the white space to browse)")}}</label>
                                        <div class="input-images-1" style="padding-top: .5rem;"></div>
                                        <p>Required Dimensions Ratio 3:2 (width:height) and Maximum Image Count is 3</p>
                                        @include('alerts.feedback', ['field' => 'images'])
                                        @include('alerts.feedback', ['field' => 'images.*'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Tag")}}</label>
                                        <input type="text" name="tag" class="form-control" value="{{ old('tag') }}">
                                        @include('alerts.feedback', ['field' => 'tag'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="content">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control"
                                                  style="border:1px solid #E3E3E3">{{ old('content') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1"> Active
                                        <input type="radio" name="status" value="0"> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                           {{-- <div id="multiple-images">
                                <input type="hidden" id="slider-url1" name="slider_url1" required>
                                <input type="hidden" id="slider-url2" name="slider_url2" required>
                                <input type="hidden" id="slider-url3" name="slider_url3" required>
                            </div>--}}
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <script>
        $('.input-images-1').imageUploader();
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\NgoCreateRequest', '#ngo_create') !!}



    {{--<script>
        let imgs = null;
        let index = null;
        $(document).ready(function () {
            $('body').on('DOMSubtreeModified', '.uploaded', function () {
                imgs = document.querySelectorAll('.uploaded img');

                if (imgs.length > 0) {
                    console.log(imgs[0].src);
                    $('#slider-url1').val(imgs[0].src);
                }
                if (imgs.length > 1) {
                    console.log(imgs[1].src);
                    $('#slider-url2').val(imgs[1].src);
                }
                if (imgs.length > 2) {
                    console.log(imgs[2].src);
                    $('#slider-url3').val(imgs[2].src);
                }

                console.log(imgs);
                if (imgs.length > 0) {
                    imgs[0].onclick = functionst(0);
                }
                if (imgs.length > 1) {
                    imgs[1].onclick = functionst(1);
                }
                if (imgs.length > 2) {
                    imgs[2].onclick = functionst(2);

                }

            });


        });

        function functionst(i) {
            return function () {
                let index = i + 2;
                let index_minus = i + 1;
                document.getElementById('multiple-images').innerHTML = '<div class="modal fade" id="image_crop_modal' + index + '" tabindex="-1" role="dialog" aria-labelledby="modalLabel"\n' +
                    '     aria-hidden="true" onclick="return false;">\n' +
                    '    <div class="modal-dialog modal-lg" role="document">\n' +
                    '        <div class="modal-content">\n' +
                    '            <div class="modal-header">\n' +
                    '                <h5 class="modal-title">Crop Image Before Upload</h5>\n' +
                    '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                    '                    <span aria-hidden="true">×</span>\n' +
                    '                </button>\n' +
                    '            </div>\n' +
                    '            <div class="modal-body">\n' +
                    '                <div class="img-container">\n' +
                    '                    <div class="row">\n' +
                    '                        <div class="col-md-8">\n' +
                    '                            <img class="sample_cropping_img"\n' +
                    '                                 id="sample_cropping_img' + index + '" src="">\n' +
                    '                        </div>\n' +
                    '                        <div class="col-md-4">\n' +
                    '                            <div id="preview_cropping_img' + index + '"></div>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '            <div class="modal-footer">\n' +
                    '                <button type="button" class="btn btn-secondary" data-dismiss="modal"\n' +
                    '                        id="img_crop_cancel_button' + i + '">Cancel\n' +
                    '                </button>\n' +
                    '                <button type="button" class="btn btn-primary" id="img_crop_button' + index + '">Crop</button>\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>';


                console.log('asdasd');


                let img_crop_modal = '#image_crop_modal' + index;
                let sample_img = 'sample_cropping_img' + index;
                var sample_cropping_img = document.getElementById(sample_img);
                var $cropping_modal = $(img_crop_modal);
                var cropper;


                sample_cropping_img.src = imgs[i].src;
                $cropping_modal.modal({
                    backdrop: 'static'

                });

                console.log(sample_cropping_img.src);


                $cropping_modal.modal('show');

                let prev_crop_img = '#preview_cropping_img' + index;

                $cropping_modal.on('shown.bs.modal', function () {
                    console.log('showing crop model');
                    cropper = new Cropper(sample_cropping_img, {
                        aspectRatio: 1.5,
                        viewMode: 1,
                        preview: prev_crop_img,
                        checkCrossOrigin: false,
                        checkOrientation: false
                    });
                    console.log('showing crop model2');


                }).on('hidden.bs.modal', function () {
                    console.log('hiding crop modal');

                    cropper.destroy();
                    cropper = null;
                });
                //alert(sample_cropping_img.src);
                let img_crop_button = '#img_crop_button' + index;

                $(img_crop_button).click(function () {
                    canvas = cropper.getCroppedCanvas({
                        minWidth: 256,
                        minHeight: 256,
                        maxWidth: 4096,
                        maxHeight: 4096,
                        fillColor: '#fff',
                        imageSmoothingEnabled: true,
                        imageSmoothingQuality: 'high',
                        quality: '100',

                    });

                    canvas.toBlob(function (blob) {
                        url = URL.createObjectURL(blob);
                        var reader = new FileReader();
                        reader.readAsDataURL(blob);
                        reader.onloadend = function () {
                            let img_crop_cancel_button = '#img_crop_cancel_buton' + index;
                            let img_crop_button = '#img_crop_button' + index;
                            var base64data = reader.result;
                            $(img_crop_cancel_button).prop("disabled", true);
                            $(img_crop_button).html("Uploading...");
                            $(img_crop_button).prop("disabled", true);
                            $.ajax({
                                url: "{{route('image_crop')}}",
                                data: {image: base64data, upload_path: 'test'},
                                method: "post",
                                success: function (result) {
                                    $cropping_modal.modal('hide');
                                    if (result.success) {
                                        img_prevs = document.querySelectorAll('.uploaded-image img');
                                        console.log(result.data);
                                        img_prevs[i].src = result.data;
                                        let file_input = '#slider-url' + index_minus;
                                        $(file_input).val(result.data);
                                        Swal.fire({
                                            title: 'Success',
                                            icon: 'success',
                                            text: result.message,
                                        });
                                    } else {
                                        Swal.fire({
                                            title: 'Error',
                                            icon: 'error',
                                            text: result.message,
                                        });
                                    }

                                    $(img_crop_cancel_button).attr("disabled", false);
                                    $(img_crop_button).attr("disabled", false);
                                    $(img_crop_button).html("Crop");
                                }
                            });
                        }
                    }, 'image/jpeg', 1)
                });

            }
        }
    </script>--}}
@endsection
