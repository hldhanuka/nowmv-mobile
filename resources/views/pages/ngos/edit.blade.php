@extends('layouts.app', [
    'namePage' => 'Ngo',
    'class' => 'sidebar-mini',
    'activePage' => 'ngo',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('ngos.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update NGOs</h4>
                    </div>
                    <div class="card-body">
                        <form id="ngo_update" method="post" action="{{ route('ngos.update', $ngo->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title', $ngo->title) }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="category_id">{{__(" NGO Merchants")}}</label>
                                        <select class="form-control" id="ngo_merchant_id" name="ngo_merchant_id" >
                                            @if(auth()->user()->hasRole("admin"))
                                                @foreach($ngo_merchants as $ngo_merchant)
                                                    <option value="{{$ngo_merchant->id}}" {{$ngo_merchant->id==$ngo->ngo_merchant_id?"selected":""}}>{{$ngo_merchant->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach(auth()->user()->ngoMerchants()->get() as $ngo_merchant)
                                                    <option
                                                        value="{{$ngo_merchant->ngoMerchant->id}}" {{$ngo_merchant->ngoMerchant->id==$ngo->ngo_merchant_id?"selected":""}}>{{$ngo_merchant->ngoMerchant->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ngo_merchant_id'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Donation Maximum Limit")}}</label>
                                        <input type="number" name="max_limit" class="form-control" value="{{ old('max_limit', $ngo->max_limit) }}">
                                        @include('alerts.feedback', ['field' => 'max_limit'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Logo")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'logo', 'ratio'=>1, 'current_image'=>$ngo->logo!=null?$ngo->logo:asset('assets/img/dummy.jpg'),'upload_path'=>'upload/ngos'])
                                        <p>Required Dimensions - Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'logo_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{$ngo->logo!=null?$ngo->logo:asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="logo" required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'logo'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="images">{{__(" Slider Images (Drag & Drop or click the white space to browse)")}}</label>
                                        <div class="input-images-1" style="padding-top: .5rem;"></div>
                                        <p>Required Dimensions Ratio 3:2 (width:height) and Maximum Image Count is 3</p>
                                        @include('alerts.feedback', ['field' => 'images'])
                                        @include('alerts.feedback', ['field' => 'images.*'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Tag")}}</label>
                                        <input type="text" name="tag" class="form-control" value="{{ old('tag', $ngo->tag) }}">
                                        @include('alerts.feedback', ['field' => 'tag'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control" style="border:1px solid #E3E3E3">{{ old('content', $ngo->content) }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Status")}}</label><br>
                                        <input type="radio" name="status" value="1" {{$ngo->status==1?"checked":""}}> Active
                                        <input type="radio" name="status" value="0" {{$ngo->status==0?"checked":""}}> Inactive
                                        @include('alerts.feedback', ['field' => 'status'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let ngo_images = @json($ngo->ngoImages);
        let preloaded = [];
        ngo_images.forEach(function(currentValue, index, arr){
            preloaded.push({id:currentValue.id,src:currentValue.image});
        });
        $('.input-images-1').imageUploader({
            preloaded: preloaded,
            imagesInputName: 'images',
            preloadedInputName: 'old'
        });
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\NgoUpdateRequest', '#ngo_update') !!}
@endsection
