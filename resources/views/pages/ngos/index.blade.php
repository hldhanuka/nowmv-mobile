@extends('layouts.app', [
    'namePage' => 'NGO',
    'class' => 'sidebar-mini',
    'activePage' => 'ngo',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> NGO List</h4>
                        @if(@auth()->user()->hasPermissionTo('ngos.create'))
                            <div class="pull-right">
                                <a href="{{ route('ngos.create') }}">
                                    <button class="btn btn-primary">Create</button>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="ngo_table">
                                <thead class=" text-primary">
                                <th>ID</th>
                                <th>Logo</th>
                                <th>Title</th>
                                <th>NGO Merchant</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                {{--Load data--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {

            $('#ngo_table').dataTable({
                "scrollY": 500,
                processing: true,
                serverSide: true,
                ajax: "{!! route('ngos.load-data') !!}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'logo', name: 'logo'},
                    {data: 'title', name: 'title'},
                    {data: 'ngo_merchant', name: 'ngo_merchant'},
                    {data: 'status_text', name: 'status_text'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

    </script>
@endsection
