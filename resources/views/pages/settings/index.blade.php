@extends('layouts.app', [
    'namePage' => 'Settings',
    'class' => 'sidebar-mini',
    'activePage' => 'setting',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Update Settings</h4>
                    </div>
                    <div class="card-body">
                        <form id="setting_update" method="post" action="{{ route('settings.update') }}">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-10 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" App Settings")}}</label>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center item-raw-title" width="10%">Flatform</th>
                                                <th class="text-center item-raw-title" width="20%">Latest Version</th>
                                                <th class="text-center item-raw-title" width="20%">Force Update</th>
                                                <th class="text-center item-raw-title" width="50%">Dhiraagu Pay URL</th>
                                            </tr>
                                            </thead>
                                            <tbody id="ingredients_table">
                                            <tr>
                                                <td class="text-center">
                                                    <i class="fab fa-apple fa-2x"></i>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text" name="ios_new_version"
                                                           value="{{ old('ios_new_version', $app_settings["ios"]["new_version"]) }}">
                                                    @include('alerts.feedback', ['field' => 'ios_new_version'])
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                               name="ios_update_required"
                                                               value="1" {{ $app_settings["ios"]["update_required"]==1?"checked":"" }}>
                                                        <label class="form-check-label" for="ios_update_required">
                                                            Required
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                               name="ios_update_required"
                                                               value="0" {{ $app_settings["ios"]["update_required"]==0?"checked":"" }}>
                                                        <label class="form-check-label" for="ios_update_required">
                                                            Not Required
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <input type="text" name="ios_dhiraagu_pay_url" class="form-control"
                                                           value="{{ old('ios_dhiraagu_pay_url', $ios_dhiraagu_pay_url) }}">
                                                    @include('alerts.feedback', ['field' => 'ios_dhiraagu_pay_urls'])
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <i class="fab fa-android fa-2x"></i>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text" name="android_new_version"
                                                           value="{{ old('android_new_version', $app_settings["android"]["new_version"]) }}">
                                                    @include('alerts.feedback', ['field' => 'android_new_version'])
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                               name="android_update_required"
                                                               value="1" {{ $app_settings["android"]["update_required"]==1?"checked":"" }}>
                                                        <label class="form-check-label" for="android_update_required">
                                                            Required
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                               name="android_update_required"
                                                               value="0" {{ $app_settings["android"]["update_required"]==0?"checked":"" }}>
                                                        <label class="form-check-label" for="android_update_required">
                                                            Not Required
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <input type="text" name="android_dhiraagu_pay_url" class="form-control"
                                                           value="{{ old('android_dhiraagu_pay_url', $android_dhiraagu_pay_url) }}">
                                                    @include('alerts.feedback', ['field' => 'android_dhiraagu_pay_urls'])
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        @include('alerts.feedback', ['field' => 'app_settings'])
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="priority">{{__(" Radius (km)")}}</label>
                                        <input type="text" name="radius" class="form-control"
                                               value="{{ old('radius', $radius) }}">
                                        @include('alerts.feedback', ['field' => 'radius'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="priority">{{__(" Greeting")}}</label>
                                        <input type="text" name="greeting" class="form-control"
                                               value="{{ old('greeting', $greeting) }}">
                                        @include('alerts.feedback', ['field' => 'greeting'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Is Fasting Enable")}}</label>
                                        <br>
                                        <input class="pr-1" type="radio"
                                               name="is_fasting_enable"
                                               value="1" {{ $is_fasting_enable==1?"checked":"" }}>
                                        <label class="form-check-label pr-1" for="is_fasting_enable">
                                            Enable
                                        </label>
                                        <br>
                                        <input class="pr-1" type="radio"
                                               name="is_fasting_enable"
                                               value="0" {{ $is_fasting_enable==0?"checked":"" }}>
                                        <label class="form-check-label pr-1" for="is_fasting_enable">
                                            Disable
                                        </label>
                                        @include('alerts.feedback', ['field' => 'is_fasting_enable'])
                                    </div>
                                </div>
                            </div>
                            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
                                    type="text/javascript"></script>
                            <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
                                  rel="stylesheet"
                                  type="text/css"/>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Fasting Start Date")}}</label>
                                        <input id="datepicker_start" name="fasting_start_date"
                                               class="form-control"
                                               value="{{ old('fasting_start_date', $fasting_start_date) }}"/>
                                        <script>
                                          $('#datepicker_start').datepicker({format: 'yyyy-mm-dd'});
                                        </script>
                                        @include('alerts.feedback', ['field' => 'fasting_start_date'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Fasting End Date")}}</label>
                                        <input id="datepicker_end" name="fasting_end_date"
                                               class="form-control"
                                               value="{{ old('fasting_end_date', $fasting_end_date) }}"/>
                                        <script>
                                          $('#datepicker_end').datepicker({format: 'yyyy-mm-dd'});
                                        </script>
                                        @include('alerts.feedback', ['field' => 'fasting_end_date'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Reset Password")}}</label>
                                        <input id="datepicker_end" name="reset_password" class="form-control"
                                               value="{{ old('reset_password', $reset_password) }}"/>
                                        @include('alerts.feedback', ['field' => 'reset_password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="display: none">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Ad Position")}}</label>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center item-raw-title">Type</th>
                                                <th class="text-center item-raw-title">Position</th>
                                            </tr>
                                            </thead>
                                            <tbody id="ingredients_table">
                                            <tr>
                                                <td class="text-center">
                                                    <p>Donation</p>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text"
                                                           name="donation_position"
                                                           value="{{ old('donation_position',$ad_position["donation_position"]) }}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <p>Food</p>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text"
                                                           name="food_position"
                                                           value="{{ old('food_position',$ad_position["food_position"]) }}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <p>News</p>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text"
                                                           name="news_position"
                                                           value="{{ old('news_position',$ad_position["news_position"]) }}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <p>Offer</p>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control" type="text"
                                                           name="offer_position"
                                                           value="{{ old('offer_position',$ad_position["offer_position"]) }}">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\SettingUpdateRequest', '#setting_update') !!}
@endsection
