@extends('layouts.app', [
    'namePage' => 'Videos',
    'class' => 'sidebar-mini',
    'activePage' => 'video',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('videos.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Video</h4>
                    </div>
                    <div class="card-body">
                        <form id="video_create" method="post" action="{{ route('videos.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Title")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Video URL")}}</label>
                                        <input type="text" name="url" class="form-control" value="{{ old('url') }}">
                                        <p>Required URL - youtube or .mp4</p>
                                        @include('alerts.feedback', ['field' => 'url'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title" class="d-block">{{__(" OR Video File")}}</label>
                                        <div class='file-input'>
                                            <input type='file' name="video_file">
                                            <span class='button'>Upload Video</span>
                                            <span class='label' data-js-label>No file selected</span>
                                        </div>
                                        <p>Required Video Format - .mp4</p>
                                        @include('alerts.feedback', ['field' => 'video_file'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Video Thumbnail")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'video_thumbnail', 'ratio'=>2, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/video'])
                                        <p>Required Dimensions - Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'video_thumbnail_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Video Thumbnail")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="video_thumbnail" required hidden>
                                        <p>Required Dimensions Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'video_thumbnail']  )
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\VideoCreateRequest', '#video_create') !!}
@endsection
