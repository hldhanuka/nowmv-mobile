@extends('layouts.app', [
    'namePage' => 'Donations',
    'class' => 'sidebar-mini',
    'activePage' => 'donation',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('donations.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Donation</h4>
                    </div>
                    <div class="card-body">
                        <form id="donation_create" method="post" action="{{ route('donations.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                    <div class="col-md-7 pr-1">
                                        <div class="form-group">
                                            <label for="title">{{__(" Title")}}</label>
                                            <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                                            @include('alerts.feedback', ['field' => 'title'])
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Donation Maximum Limit")}}</label>
                                        <input type="number" name="max_limit" class="form-control" value="{{ old('max_limit') }}">
                                        @include('alerts.feedback', ['field' => 'max_limit'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ngo_id">{{__(" NGO")}}</label>
                                        <select class="form-control" id="ngo_id" name="ngo_id">
                                            @foreach($ngos as $ngo)
                                                <option value="{{$ngo->id}}">{{$ngo->title}}</option>
                                            @endforeach
                                        </select>
                                        @include('alerts.feedback', ['field' => 'ngo_id'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>1.5, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/donations'])
                                        <p>Required Dimensions - Ratio 3:2 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 3:2 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Tag")}}</label>
                                        <input type="text" name="tag" class="form-control" value="{{ old('tag') }}">
                                        @include('alerts.feedback', ['field' => 'tag'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control" style="border:1px solid #E3E3E3">{{ old('content') }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\DonationCreateRequest', '#donation_create') !!}
@endsection
