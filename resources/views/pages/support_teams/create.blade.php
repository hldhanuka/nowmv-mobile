@extends('layouts.app', [
    'namePage' => 'Support Teams',
    'class' => 'sidebar-mini',
    'activePage' => 'support_team',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('support_teams.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Support Team</h4>
                    </div>
                    <div class="card-body">
                        <form id="support_team_create" method="post" action="{{ route('support_teams.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="name">{{__(" Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>1, 'current_image'=>asset('assets/img/dummy.jpg'),'upload_path'=>'upload/support_teams'])
                                        <p>Required Dimensions - Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image']  )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="email">{{__(" Email")}}</label>
                                        <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                                        @include('alerts.feedback', ['field' => 'email'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="phone">{{__(" Phone Number")}}</label>
                                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
                                        @include('alerts.feedback', ['field' => 'phone'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Location")}}</label>
                                        <input type="text" name="location" class="form-control" value="{{ old('location') }}">
                                        @include('alerts.feedback', ['field' => 'location'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Create')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\SupportTeamCreateRequest', '#support_team_create') !!}
@endsection
