@extends('layouts.app', [
    'namePage' => 'Recipes',
    'class' => 'sidebar-mini',
    'activePage' => 'recipe',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('recipes.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Create Recipe</h4>
                    </div>
                    <div class="card-body">
                        <form id="recipe_update" method="post" action="{{ route('recipes.update',$recipe->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Recipe Name")}}</label>
                                        <input type="text" name="title" class="form-control" value="{{ old('title',$recipe->title) }}">
                                        @include('alerts.feedback', ['field' => 'title'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'image', 'ratio'=>2, 'current_image'=>$recipe->image!=null?$recipe->image:asset('assets/img/dummy.jpg'),'upload_path'=>'upload/recipes'])
                                        <p>Required Dimensions - Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Image")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{$recipe->image!=null?$recipe->image:asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="image" required hidden>
                                        <p>Required Dimensions Ratio 2:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'image']  )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="video">{{__(" Video URL")}}</label>
                                        <input type="text" name="video" class="form-control" value="{{ old('video',$recipe->video) }}">
                                        <p>Required URL - youtube or .mp4</p>
                                        @include('alerts.feedback', ['field' => 'video'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title" class="d-block">{{__(" OR Video File")}}</label>
                                        <div class='file-input'>
                                            <input type='file' name="video_file">
                                            <span class='button'>Upload Video</span>
                                            <span class='label' data-js-label>No file selected</span>
                                        </div>
                                        <p>Required Video Format - .mp4</p>
                                        @include('alerts.feedback', ['field' => 'video_file'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="link">{{__(" Link")}}</label>
                                        <input type="text" name="link" class="form-control" value="{{ old('link',$recipe->link) }}">
                                        @include('alerts.feedback', ['field' => 'link'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="serving">{{__(" Serving")}}</label>
                                        <input type="text" name="serving" class="form-control" value="{{ old('serving',$recipe->serving) }}">
                                        @include('alerts.feedback', ['field' => 'serving'])
                                    </div>
                                </div>
                            </div>
{{--                            <div class="row">--}}
{{--                                <div class="col-md-7 pr-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="prepare_time">{{__(" Preparing Time")}}</label>--}}
{{--                                        <input type="text" name="prepare_time" class="form-control" value="{{ old('prepare_time',$recipe->prepare_time) }}">--}}
{{--                                        @include('alerts.feedback', ['field' => 'prepare_time'])--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="cook_time">{{__(" Cook Time")}}</label>
                                        <input type="text" name="cook_time" class="form-control" value="{{ old('cook_time',$recipe->cook_time) }}">
                                        @include('alerts.feedback', ['field' => 'cook_time'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="ingredients">{{__(" Ingredients")}}</label>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center item-raw-title">Items</th>
                                                <th class="text-center">
                                                    <i class="fas fa-plus-circle fa-2x text-success" onclick="addNewItemRaw('ingredients','ingredients_table')"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="ingredients_table">
                                            @if(old('ingredients')==null)
                                                @foreach(json_decode($recipe->ingredients,true) as $ingredient_item)
                                                <tr>
                                                    <td class="text-center">
                                                        <i class="fas fa-arrows-alt-v"></i>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control" type="text" name="ingredients[]" value="{{ $ingredient_item["item"] }}">
                                                    </td>
                                                    <td class="text-center">
                                                        <i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                                @php
                                                    $index = 0;
                                                @endphp
                                                @foreach(old('ingredients') as $item)
                                                    <tr>
                                                        <td class="text-center">
                                                            <i class="fas fa-arrows-alt-v"></i>
                                                        </td>
                                                        <td class="text-center">
                                                            <input class="form-control" type="text" name="ingredients[]" value="{{ $item }}">
                                                        </td>
                                                        <td class="text-center">
                                                            <i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $index++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        @include('alerts.feedback', ['field' => 'ingredients'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="directions">{{__(" Directions")}}</label>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center item-raw-title">Items</th>
                                                <th class="text-center">
                                                    <i class="fas fa-plus-circle fa-2x text-success" onclick="addNewItemRaw('directions','directions_table')"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="directions_table">
                                            @if(old('directions')==null)
                                                @foreach(json_decode($recipe->directions,true) as $direction_item)
                                                    <tr>
                                                        <td class="text-center">
                                                            <i class="fas fa-arrows-alt-v"></i>
                                                        </td>
                                                        <td class="text-center">
                                                            <input class="form-control" type="text" name="directions[]" value="{{ $direction_item["item"] }}">
                                                        </td>
                                                        <td class="text-center">
                                                            <i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                @php
                                                    $index = 0;
                                                @endphp
                                                @foreach(old('directions') as $item)
                                                    <tr>
                                                        <td class="text-center">
                                                            <i class="fas fa-arrows-alt-v"></i>
                                                        </td>
                                                        <td class="text-center">
                                                            <input class="form-control" type="text" name="directions[]" value="{{ $item }}">
                                                        </td>
                                                        <td class="text-center">
                                                            <i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $index++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        @include('alerts.feedback', ['field' => 'directions'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="content">{{__(" Content")}}</label>
                                        <textarea type="text" name="content" class="form-control" style="border:1px solid #E3E3E3">{{ old('content',$recipe->content) }}</textarea>
                                        @include('alerts.feedback', ['field' => 'content'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
      $(document).ready(function () {
        $("#ingredients_table").sortable();
        $("#ingredients_table").disableSelection();

        $("#directions_table").sortable();
        $("#directions_table").disableSelection();
      });

      function addNewItemRaw(field_name, table_body_id){
        let html = `<tr>
        <td class="text-center"><i class="fas fa-arrows-alt-v"></i></td>
        <td class="text-center"><input class="form-control" type="text" name="`+field_name+`[]"></td>
        <td class="text-center"><i class="fas fa-times-circle fa-2x text-danger" onclick="removeMyRaw(this)"></i></td>
        </tr>`;
        $("#"+table_body_id).append(html);
      }

      function removeMyRaw(item){
        $(item).closest("tr").remove();
      }
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\RecipeUpdateRequest', '#recipe_update') !!}
@endsection
