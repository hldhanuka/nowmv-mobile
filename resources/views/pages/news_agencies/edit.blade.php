@extends('layouts.app', [
    'namePage' => 'News Agencies',
    'class' => 'sidebar-mini',
    'activePage' => 'news_agency',
  ])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="pull-right">
                        <a href="{{ route('news_agencies.index') }}">
                            <button class="btn btn-dark" style="margin-right: 15px;">Back</button>
                        </a>
                    </div>
                    <div class="card-header">
                        <h4 class="card-title"> Update News Agency</h4>
                    </div>
                    <div class="card-body">
                        <form id="news_agency_update" method="post" action="{{ route('news_agencies.update', $news_agency->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success')
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="title">{{__(" Agency Name")}}</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name', $news_agency->name) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Logo")}}</label>
                                        @include('includes.cropping_tool', ['id'=>1, 'name'=>'logo', 'ratio'=>5, 'current_image'=>$news_agency->logo!=null?$news_agency->logo:asset('assets/img/dummy.jpg'),'upload_path'=>'upload/news_agencies'])
                                        <p>Required Dimensions - Ratio 5:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'logo_url'])
                                    </div>
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label class="d-block" for="title">{{__(" Upload Logo")}}</label>
                                        <img class="gal-img prev_img" id="prev_img" src="{{$news_agency->logo!=null?$news_agency->logo:asset('assets/img/dummy.jpg')}}">
                                        <input type="file" class="custom-file-input" id="custom-file-input" name="logo" required hidden>
                                        <p>Required Dimensions Ratio 1:1 (width:height)</p>
                                        @include('alerts.feedback', ['field' => 'logo'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">{{__('Update')}}</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CMS\NewsAgencyUpdateRequest', '#news_agency_update') !!}
@endsection
