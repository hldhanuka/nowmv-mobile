<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Privacy Policy</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="row no-gutters">
    <div class="col-10 col-sm-10 offset-sm-1 offset-1">
        <div class="row">
            <div class="col-md-12 mt-5">
                <img src="{{asset('assets/img/privacy.png')}}" class="mx-auto d-block img-fluid" style="width: 100px">
                <div class="description mt-5">
                    <p>
                        This Terms of Use (the “Terms”) governs your use of the NowMV (the “<b>Application</b>”), and is
                        an
                        agreement between You (“your” or “customer”) and DHIRAAGU (“we”, “our” or “us”). By using the
                        Application, you acknowledge and agree that you are bound by the terms herein and any amended
                        and varied terms at the sole discretion of DHIRAAGU. Any undertaking in this Agreement by either
                        party not to do any act or thing is understood to include an undertaking not to permit anyone
                        else to do that act or thing.
                    <ol>
                        <li><b>Our Service</b>
                            <ol>
                                <li>We will provide you with various content through the Application including but
                                    not limited to, news, articles, advertisements, promotions (including but not
                                    limited to coupons, offers or similar promotional content), schedules for local
                                    modes of transportation, and similar lifestyle content; and the service of
                                    acceptance and re-grant of donations to in-app registered NGOs (Non-Governmental
                                    Organisations) or foundations. For the purpose of these terms, any text, graphics,
                                    audio, visual (including still visual images) and/or audio-visual material,
                                    software, applications, data, database content or other multimedia content,
                                    information and material, including the metadata relating to any such content or
                                    other materials and works of authorship included in the Application, together
                                    constitutes the “<b>Content</b>”.
                                </li>
                                <li>
                                    We reserve the right to modify or discontinue the provision of any Content, the
                                    Application and/or features in the Application at any time without notice to you.
                                </li>
                            </ol>
                        </li>
                        <li><b>REGISTRATION</b>
                            <ol>
                                <li>
                                    You will be required to create an account to use the Application. By
                                    registering you warrant that (a) you are 18 years of age and have the capacity to
                                    agree to these Terms; and (b) all information provided to register your account are
                                    true, accurate, complete and up-to-date. You must promptly update any changes to the
                                    information provided.
                                </li>
                            </ol>
                        </li>
                        <li><b>GRANT OF LICENSE </b>
                            <ol>
                                <li>
                                    You will be required to create an account to use the Application. By
                                    registering you warrant that (a) you are 18 years of age and have the capacity to
                                    agree to these Terms; and (b) all information provided to register your account are
                                    true, accurate, complete and up-to-date. You must promptly update any changes to the
                                    information provided.
                                </li>
                                <li>
                                    Any attempt to disassemble, decompile, create derivative works of, reverse
                                    engineer, modify, sublicense, reproduce, copy, distribute or use for other purposes
                                    either the Application and/or Content is strictly prohibited.
                                </li>
                            </ol>
                        </li>
                        <li><b>INTELLECTUAL PROPERTY RIGHTS</b>
                            <ol>
                                <li>Intellectual Property Rights means patents, utility models, rights to
                                    inventions, copyrights, logos, moral rights, trademarks and service marks, business
                                    names and domain names, rights in get-up, rights in designs, rights in computer
                                    software, database rights, rights to use, confidential information, and all other
                                    intellectual property rights, whether registered or unregistered, which subsist or
                                    will subsist now or in the future in any part of the world (the “<b>IPR</b>”).
                                </li>
                                <li>
                                    You acknowledge and agree that DHIRAAGU its affiliated partners and/or
                                    third-parties retain ownership of all IPRs in the Application, the Content and any
                                    part thereof. Nothing in these terms shall operate as a transfer or license to you
                                    of title to and ownership of the Application, its Content, or any of the features of
                                    the Application.
                                </li>
                                <li>
                                    4.3. You agree not to do anything to limit, interfere with, or otherwise jeopardise
                                    in any manner such rights, title and interest and not to modify, reproduce,
                                    distribute, publish, license, or create derivative works from any such IPRs.
                                </li>
                            </ol>
                        </li>
                        <li><b>WARRANTIES</b>
                            <ol>
                                <li>
                                    Subject to periodic maintenance and support, we will use commercially
                                    reasonable efforts to ensure that you are able to use the Application without
                                    interruptions.
                                </li>
                                <li>
                                    You understand and agree that the Application and Content are provided on an
                                    “as is” and “as available” basis and we make no warranty that they will be
                                    up-to-date, accurate, complete, or will meet your requirements, be uninterrupted,
                                    timely, secure, or error-free.
                                </li>
                                <li>
                                    No advice, representations or information given by our employees, agents or
                                    contractors shall create a warranty unless expressly set out in these Terms.
                                </li>
                            </ol>
                        </li>
                        <li><b>THIRD PARTY CONTENT</b>
                            <ol>
                                <li>
                                    The Content are mainly provided by third parties. You agree that we are under
                                    no obligation to you or any other Party to monitor or filter any Content, although
                                    we may filter such Content at our discretion.
                                </li>
                                <li>
                                    We make no warranty or representation, whether express or implied, as to the
                                    accuracy, suitability, quality, completeness, legality or validity of such
                                    third-party Content. We acknowledge and agree that we will not be liable for any
                                    consequences due to your action or inaction in reliance of such Content and/or their
                                    accuracy.
                                </li>
                                <li>
                                    You acknowledge that the Application and Content may include certain components
                                    provided by third parties, and agree to be bound by the terms of those third parties
                                    if you use such components within the Application.
                                </li>
                            </ol>
                        </li>
                        <li><b>PREMIUM CONTENT, REDEEMABLE OFFERS AND DONATIONS</b>
                            <ol>
                                <li>
                                    If you wish to have access to our premium Content, you shall pay the fees at
                                    such rates determined by us from time to time, at our discretion.
                                </li>
                                <li>
                                    If you use any redeemable offers available through the Application, such offers
                                    should be valid at the time of payment, offers may not be used more than once, and
                                    will be subject to the terms specific to that offer.
                                </li>
                                <li>
                                    Any payments and/or donations made through the Application will be processed
                                    via Dhiraagu Pay and will be subject to terms and conditions of the Dhiraagu Pay
                                    service.
                                </li>
                                <li>
                                    The Donations made through the Application are complete and final charitable
                                    gifts to the registered NGOSs or Foundations.
                                </li>
                                <li>
                                    Any re-grants of the donations will be based on your submission through the
                                    Application less any applicable processing fees or charges payable as per the terms
                                    and conditions of Dhiraagu Pay.
                                </li>
                                <li>
                                    Dhiraagu reserves the right to establish minimum amounts and any other
                                    restrictions for any regrant. In the event, the NGO or foundation you selected
                                    doesn’t satisfy with our criteria or otherwise doesn’t accept or enable payment (due
                                    to suspicion of illegal activities, any inferior establishment or reputation,
                                    dissolution of the NGO or foundation, incompliance with laws and regulations,
                                    temporary suspension or any other such reason), Dhiraagu may select an alternative
                                    NGO or foundation registered in our Application to receive your donation.
                                </li>
                                <li>
                                    You will not be entitled to refunds for redeemable offers or any payments made
                                    through the Application.
                                </li>
                            </ol>
                        </li>
                        <li><b>SUSPENSION AND TERMINATION OF SERVICE </b>
                            <ol>
                                <li>
                                    We may in our sole discretion elect to indefinitely suspend, limit, terminate
                                    or block your use of the Application, either wholly or partially, without notice, in
                                    the event that:
                                    <ol type="a">
                                        <li>
                                            we become aware, or have reasonable grounds to suspect that you provided
                                            false, inaccurate, unverifiable information;
                                        </li>
                                        <li>
                                            identification or suspicion of any unauthorised, abusive, fraudulent or
                                            unlawful use or misuse of the Application and/or Content;
                                        </li>
                                        <li>
                                            you fail to follow the guidelines and/or instructions given by us, or
                                            breach any provisions herein;
                                        </li>
                                        <li>
                                            we have reasonable ground to believe that your use of the Service may
                                            create liability to you, us, and/or any third-party;
                                        </li>
                                        <li>
                                            any permit, license, authority or consent which we may require in order
                                            to carry out our obligations herein is refused, withdrawn, suspended, or
                                            terminated; or
                                        </li>
                                        <li>
                                            the relevant government authorities within the Republic of Maldives or
                                            elsewhere require us to suspend the Application for whatever reason.
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    Upon execution of any of our rights under clause 8.1 you may be barred from
                                    using the Application using your account, a different account, or unable to register
                                    a new account.
                                </li>
                                <li>
                                    None of the rights which either party have accrued as a result of these Terms,
                                    prior to the date of termination shall be lost or otherwise affected following
                                    termination.
                                </li>
                            </ol>
                        </li>
                        <li><b>LIABILITY AND INDEMNITY</b>
                            <ol>
                                <li>
                                    To the fullest extent permitted by the law of Maldives, we will not be liable
                                    to you, whether in contract, tort or otherwise in connection with the terms herein,
                                    or resulting from your access, use or reliance on the Application and/or its Content
                                    for: (i) any loss of profits, income, revenue, use of the service or anticipated
                                    savings, loss or corruption of data, loss of contract or opportunity or loss of
                                    goodwill; or (ii) any indirect economic, incidental, special, punitive or
                                    consequential loss of whatever nature, and whether or not reasonably foreseeable,
                                    reasonably contemplatable, or actually contemplated by the parties.
                                </li>
                                <li>
                                    If we shall be liable to you in contract, tort, under statute or otherwise
                                    under the terms herein, our aggregate liability to you for any event or related
                                    series of events shall be the lesser of: (a) the maximum daily Transaction Limit as
                                    set out in Dhiraagu Pay terms and conditions; or (b) the value of transactions
                                    processed by you through the Application, in the two (2) days preceding the date the
                                    claim arose.
                                </li>
                                <li>
                                    You agree to indemnify, defend and hold us harmless absolutely from and against
                                    all costs, losses, claims, damages and expenses of any kind whatsoever, whether
                                    foreseeable or not, that may be suffered by us as a result of your use of the
                                    Application (or anyone using the Application with your permission), which are
                                    brought or threatened against us by a third-party. This indemnity will survive the
                                    termination of these terms.
                                </li>
                                <li>
                                    You acknowledge and agree that Dhiraagu will not be responsible or liable, for
                                    any or all views, publications, transactions, conduct or activities, express or
                                    implied, by any third-parties, including but not limited to NGOs, Foundations,
                                    advertisers and/or external sites, and the risk of injury from such third-parties
                                    rests entirely with you.
                                </li>
                            </ol>
                        </li>
                        <li><b>PROTECTION OF PERSONAL INFORMATION</b>
                            <ol>
                                <li>
                                    Your Personal Data, includes but is not limited to: (i) any information
                                    collected through the Application and your use of the Application; (ii) your payment
                                    information; and (iii) our interactions with you through any medium including phone
                                    calls to our Call Centre. Such information may be collected from you and others or
                                    generated within our network when you or anyone else use the Application or our
                                    services.
                                </li>
                                <li>
                                    You acknowledge and expressly agree and authorise us to collect, process, use
                                    and disclose your Personal Data to third-parties in order to: (i) provide the
                                    Service; (ii) achieve lawful purposes including referencing, fraud detection and
                                    prevention, account management, billing, debt collection, investigating insurance
                                    claims, credit assessments, market research, customer profiling, product and service
                                    development, marketing and customer care; and (iii) for any reason required by law
                                    and for any other lawful purpose.
                                </li>
                                <li>
                                    We respect the privacy of your Personal Data and will not disclose any of your
                                    Personal Data except as permitted by the law and under the terms herein. We may
                                    retain your Personal Data for a reasonable period of time in a secure environment.
                                </li>
                                <li>
                                    You agree that we may contact any person or reference provided by you to
                                    verify the accuracy of your account details. You acknowledge that we or our agents,
                                    may from time to time contact you by post, telephone, in person, email or text
                                    message regarding details of promotions, competitions or our other products and
                                    services. You hereby expressly consent to such contacts. If you no longer wish to be
                                    contacted in such a manner, please notify us in writing.
                                </li>
                            </ol>
                        </li>
                        <li><b>RESOLVING DISPUTES AND COMPLAINTS</b>
                            <ol>
                                <li>
                                    If you have a complaint or dispute regarding our Service you may call the
                                    DHIRAAGU Call Centre on 123 or by send an email to: 123@dhiraagu.com.mv.
                                </li>
                                <li>
                                    In the event of any dispute between the parties relating to the construction
                                    of this Agreement or the rights, duties and obligations of the parties or any other
                                    matter arising out of or concerning the same the parties shall use their best
                                    endeavours to settle the matter by conciliation and negotiation. Where this fails
                                    such disputes shall be referred to the Civil Court of the Maldives.
                                </li>
                            </ol>
                        </li>
                        <li><b>MISCELLANEOUS PROVISIONS</b>
                            <ol>
                                <li>
                                    We reserve the right to amend these Terms at any time. If we make any
                                    amendments we will inform you of such changes either by posting the changes on our
                                    website at www.dhiraagu.com.mv, through the Application or by other means as we see
                                    fit. You agree that your continued use of the Application after any amendments to
                                    the Terms shall be evidence of your intention to be bound by such Terms as amended
                                    in accordance with this clause 12.1.
                                </li>
                                <li>
                                    This Agreement together with all documents which are referred to in these
                                    Terms represent the entire agreement between DHIRAAGU and you and they supersedes
                                    all prior representations or oral or written agreements between the Parties.
                                </li>
                                <li>
                                    The laws of the Republic of Maldives apply to these Terms with respect to
                                    construction, validity and performance.
                                </li>
                                <li>
                                    We will not be responsible for the failure to perform such duties arising
                                    under these Terms, during a force majeure event or where any event beyond our
                                    reasonable control occurs.
                                </li>
                                <li>
                                    If any part of these Terms becomes to any extent illegal, invalid or
                                    unenforceable, it shall to that extent be deemed to no longer form part of these
                                    Terms. This will not affect the legality, validity or enforceability of any of the
                                    remaining Terms.
                                </li>
                                <li>
                                    You may not assign or otherwise dispose of any of your rights or obligations
                                    under these Terms without our prior written consent. You agree that we may assign
                                    our rights and obligations under these Terms to an alternative provider if
                                    necessary.
                                </li>
                                <li>
                                    Failure by any party to exercise or enforce any right conferred by these Terms
                                    will not be deemed to be a waiver of any such right nor operate so as to bar the
                                    exercise or enforcement of such right or of any other right on any other occasion.
                                </li>
                                <li>
                                    Any notice required to be given under this Agreement must be in writing and
                                    delivered by hand or sent by pre-paid first-class post or courier or by fax to the,
                                    Dhivehi Raajjeyge Gulhun Plc at DHIRAAGU Head Office, Ameenee Magu, P.O Box 2082 and
                                    to Customer at address provided during your registration. Unless expressly agreed
                                    otherwise, any such notice shall be deemed to be served on the date delivered by
                                    hand, or if sent by fax the date on which a printed transmission report confirming
                                    receipt is received, or at the time of delivery, if delivered by hand, pre-paid
                                    first-class post or courier.
                                </li>
                            </ol>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<style>
    ol {
        list-style-type: none;
        counter-reset: item;
        margin: 0;
        padding: 0;
    }

    ol > li {
        display: table;
        counter-increment: item;
        margin-bottom: 0.6em;
    }

    ol > li:before {
        content: counters(item, ".") ". ";
        display: table-cell;
        padding-right: 0.6em;
    }

    li ol > li {
        margin: 0;
    }

    li ol > li:before {
        content: counters(item, ".") " ";
    }
</style>
