<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Frequently Asked Questions</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="row no-gutters faq">
    <div class="col-10 col-sm-10 offset-sm-1 offset-1">
        <div class="row">
            <div class="col-md-12 mt-5 mb-5">
                <img src="{{asset('assets/img/faq.png')}}" class="mx-auto d-block img-fluid faq-img">
                <div class="description mt-5">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link  freq_btn" type="button" data-toggle="collapse"
                                            data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        What is NowMV?

                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                        {{-- <div style="position: absolute;right: 0;right: -131px;right: -352px;top: 0;left: 1113px;" class="plus"></div>
                                         <div style="position: absolute;right: 0;right: -131px;right: -352px;top: 0;left: 1113px;" class="minus"><i class="fas fa-chevron-up"></i></div>--}}
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    NowMV is an app offering a wide array of localized lifestyle content ranging from
                                    travel, food to events and more.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                        How can I get NowMV?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Simply look for NowMV in the App store in the Android market. you can resgister
                                    either through your local mobile number or social media accounts (Facebook, Twitter
                                    and Google+)
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                        Do I have to buy the App?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    No. The App is free to donwload.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseFour" aria-expanded="false"
                                            aria-controls="collapseFour">
                                        How current is the information I see on the App?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    The information you see on the app is show in real-time, but make sure to refresh if
                                    you keep the app open for an extended period of time just to make sure that the
                                    information is still current.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFive">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseFive" aria-expanded="false"
                                            aria-controls="collapseFive">
                                        Do I have to Sign-up?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Yes. Sign-up is required, the information collected here will help us to provide a better in app experience.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSix">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseSix" aria-expanded="false"
                                            aria-controls="collapseSix">
                                        How do I personalize my homescreen?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Tap on your profile and go to the 'settings' page. On the menu, you will find the option to 'personalize homescreen'. Drag and re-arrange using the handle beside each section as you wish.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSeven">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseSeven" aria-expanded="false"
                                            aria-controls="collapseSeven">
                                        If I sign out, will my settings get cleared?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Yes, we recommend to be logged in at all times
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingEight">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseEight" aria-expanded="false"
                                            aria-controls="collapseEight">
                                        When do I receive badges?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Badges will be awarded according to your donations to a campaign
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingNine">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseNine" aria-expanded="false"
                                            aria-controls="collapseNine">
                                        How can I donate to NGOs?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    You can find the donation icon on the homepage footer. Select a campaign to proceed.
                                    Donations can be made via your DhiraaguPay account.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTen">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTen" aria-expanded="false"
                                            aria-controls="collapseTen">
                                        Will i be charged a commision for the donations I make?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    No. The amount donated will be directly transferred to the NGO's wallet without a
                                    commission.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingEleven">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseEleven" aria-expanded="false"
                                            aria-controls="collapseEleven">
                                        What if I am not registered on DhiraaguPay?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    If you are not registered on DhiraaguPay, unfortunately you will not be able to donate through the app. To make donations easily, we suggest to create a DhiraaguPay account.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwelve">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTwelve" aria-expanded="false"
                                            aria-controls="collapseTwelve">
                                        How does prayer tracking work?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Prayer tracking allows you to keep track of your prayers. It lets you mark off a prayer once you have completed it. If you have enabled prayer tracker from the settings, you should be able to see it on the homepage. You can mark the prayed status for attended prayers of the present day. If you have missed to make an entry for a previous day, tap on '>>' to view the monthly calendar where you can pick any date and mark the status on the banner provided below.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThirteen">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseThirteen" aria-expanded="false"
                                            aria-controls="collapseThirteen">
                                        How does the prayer reminder work?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Once you have selected your preferred setting, you will receiving alerts 5 minutes ahead of the prayer time.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFourteen">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseFourteen" aria-expanded="false"
                                            aria-controls="collapseFourteen">
                                        How can I view my prayer tracker logs?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    Go to the prayer page and swipe left. Here you will find a wheel which shows the calculated percentatge of prayers in a daily, weekly, and monthly view.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFifteen">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseFifteen" aria-expanded="false"
                                            aria-controls="collapseFifteen">
                                        What happens when I redeem an offer through the app?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    You can redeem offers using DhiraaguPay. Once you proceed to redeem, you will be asked to enter your DhiraaguPay number and the total bill amount. Upon confirmation, you will receive an SMS and a notification confirming the payment status.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSixteen">
                                <h2 class="mb-0">
                                    <button class="btn btn-link freq_btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseSixteen" aria-expanded="false"
                                            aria-controls="collapseSixteen">
                                        How does the nearby restaurants feature work?
                                        <span class="plus">
                                            <i class="fas fa-chevron-down" style="color: #7c7b7b"></i>
                                        </span>
                                        <span class="minus">
                                            <i class="fas fa-chevron-up" style="color: #7c7b7b"></i>
                                        </span>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    The restaurants you see in the 'nearby restaurants' columnn are the outlets nearest
                                    to your current location (if you have allowed the App to access your location)
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<style>
    .card-header {
        background: white;
    }

    .freq_btn {
        text-decoration: none;
        font-weight: 500;
        color: #0cb4c6;


    }

    .freq_btn:hover {
        text-decoration: none;
    }

    .card-body {
        background: #f7f8ff;
        color: #7c7b7b;
        font-weight: 300;
        font-weight: 200;
    }

    .accordion button.collapsed
    .minus {
        display: none;


    }

    button:not(.collapsed)
    .plus {
        display: none;
    }

    .faq .card-header .btn-link {
        width: 100%;
        text-align: left;
        position: relative;
    }

    .faq .card-header span {
        position: absolute;
        right: 0;
    }

    .faq-img {
        width: 100px;
    }

</style>
