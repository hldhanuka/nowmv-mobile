<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $news->title }}</title>
    <link rel="stylesheet" href="{{ asset('ckeditor/custom-fonts/fonts.css') }}">
</head>
<body style="padding: 5px">
{!! $news->html_content !!}
</body>
