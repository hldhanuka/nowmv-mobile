<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>About</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="row">
    <div class="col-12">
        <p class="text-right mt-2 mr-3"><a href="{{ route('privacy-policy') }}">Privacy & Policy</a></p>
    </div>
</div>
<div class="row no-gutters">
    <div class="col-10 col-sm-10 offset-sm-1 offset-1">
        <div class="row">
            <div class="col-md-12 mt-5">
                <img src="{{asset('assets/img/120x120.png')}}" class="mx-auto d-block img-fluid" style="width: 100px">
                <div class="description mt-5">
    <h2>1. <strong>Introduction</strong></h2>
    <p><strong>1.1&nbsp;App Overview</strong></p>
    <p>The purpose of this document is to present a detailed description of the &ldquo;NowMV Mobile Application&rdquo; for Dhiraagu. It will explain the features of the solution, the constraints under which it must operate and how the element will react to external stimuli. This document is intended for both the stakeholders and the developers of the system and will be proposed to Dhiraagu for its approval.</p>
    <p><strong>1.2&nbsp;Our Service</strong></p>
    <ol>
        <li>We will provide you with various content through the Application including but not limited to, news, articles, advertisements, promotions, schedules for local modes of transportation and similar lifestyle content. For the purpose of these terms, any text, graphics, audio, visual (including still visual images) and/or audio-visual material, software, applications, data, database content or other multimedia content, information and material, including the metadata relating to any such content or other materials and works of authorship included in the Application, together constitutes the&nbsp;<strong>&ldquo;Content&rdquo;</strong>.</li>
        <br>
        <li>We reserve the right to modify or discontinue the provision of any Content, the Application and/or features in the Application at any time without notice to you.</li>
    </ol>
    <h2>2. Functionality</h2>
    <h5>2.1 User Login</h5>
    <p><strong>2.1.1 Sign in</strong></p>
    <p>Once the user opens the application, the user would be landing on the first page of the application and it would display the below fields.</p>
    <ol>
        <li>Phone number</li>
        <li>Sign in</li>
        <li>Social media login buttons; through Google Account, Facebook &amp; Twitter </li>
        <li>Terms and conditions</li>
    </ol>
    <p>User would be facilitated to log in to the application via the mobile number or through social media logins.</p>
    <p>This would be a one-time login.</p>
    <p>If the user wishes to log in through mobile number, upon entering a valid mobile number; an OTP verification code would be sent to user&rsquo;s mobile number and the received code needs to be submitted in order to proceed. Dhiraagu APIs/services will be used to authenticate user and send verification code as an SMS. Once the user is authenticated, user would be directed to the Sign-Up page.</p>
    <p>If the mobile number is invalid, user would be shown with an appropriate error message as a pop-up.</p>
    <p>If the user wishes to log in through a social media platform, user could select one among Facebook and Google account. Once the user clicks on the social media type, it would ask user to provide their relevant social media account details (user name &amp; password). Upon successful authentication, user would be directed to the Sign -Up page.</p>
    <p><strong>2.1.2 Invalid OTP code</strong></p>
    <p>If the user is logged in through the mobile number and if the entered OTP code is invalid, an error message would be shown to the user and user would be requested to enter the mobile number again. Below items will be available when obtaining the OTP.</p>
    <ol>
        <li>OTP</li>
        <li>Resend</li>
        <li>Submit</li>
    </ol>
    <p><strong>2.1.3 Sign up</strong></p>
    <p>Upon successful login, the user will be directed to the Sign-up page with a registration form with following fields.</p>
    <ol>
        <li>First name</li>
        <li>Last name</li>
        <li>Email address</li>
        <li>Date of birth</li>
        <li>Address line 01</li>
        <li>Address line 02</li>
        <li>Gender</li>
        <li>Preferences</li>
    </ol>
    <p>If the user is logged in through a social media platform at the sign-up page, a mandatory field would exist to enter the mobile number. Once the mobile number is entered an OTP verification code would be sent to user&rsquo;s mobile number and upon successful submission of OTP code, user would be authenticated from the backend and would be directed to the home screen.</p>
    <p>All the above -mentioned fields are mandatory and will carry the necessary inline validations wherever applicable.</p>
    <h5>2.2 Home</h5>
    <p>The home page would consist of the following items.</p>
    <ol>
        <li>Dhiraagu logo on top left corner</li>
        <li>Top right corner (Donations icon, User profile, Notification icon; highlight the unread notifications)</li>
        <li>Universal search bar</li>
        <li>Prayer mate module with an icon</li>
        <li>Weather updates</li>
        <li>&ldquo;Whats on?&rdquo; title bar</li>
        <li>Body area displays latest news relevant to any category; in card view (Once clicked on a specific sponsorship; &ldquo;title would be displayed as sponsored&rdquo;, user would be directed to the relevant page)</li>
        <li>All sub business units (SBUs) would be displayed in a slider view which keeps the Home module stable and all other SBUs could be swiped from left to right. Upon selection of a SBU, the user would be taken to the respective screen as needed.</li>
    </ol>
    <h5>2.3 Donations</h5>
    <p>Once the user clicks on the donation&rsquo;s icon, user will be directed to the donations page. The donation page would display available donation categories/details in card view. Donations could be displayed in two separate pages as a list of campaigns and as a list of NGOs. Each display would have a small image related to that particular campaign, a title and a small description. Once the user clicks on a particular item (NGO/Campaign), user would be taken to the detailed view of that donation. After the detailed content, there would be a button labelled &ldquo;Donate&rdquo;. When the user clicks on the Donate button, user would be taken to the next page which requires the amount to donate (The technical feasibility of displaying the donation amount in a slider view, should be checked in detailed at the implementation stage.) This feature will be integrated with Dhiraagu&rsquo;s &lsquo;Dhiraagu Pay&rsquo;; payment gateway. Users would be notified upon the completion of a donation and user will be able to share the donation status on social media (Facebook and Google+).</p>
    <h5>2.4 User Profile</h5>
    <p>Once the user has created an account through the sign-up process, he/she will get to visit user profile page through the mobile app by clicking on the user profile icon which is on the top right corner of the mobile application&rsquo;s home screen. Once clicked this would display users profile picture, first name and the mobile number etc. This would show a button option labelled as &ldquo;Edit&rdquo; which would direct users to the profile edit page to make changes to below parameters.</p>
    <ol>
        <li>Username</li>
        <li>Profile picture</li>
        <li>Email address</li>
        <li>Mobile number</li>
        <li>Date of birth</li>
        <li>Address line 01</li>
        <li>Address line 02</li>
        <li>Gender</li>
        <li>Preference</li>
        <li>Logout option</li>
    </ol>
    <p>User should click on the &ldquo;Save&rdquo; button, to save the details.</p>
    <h5>2.5 Settings</h5>
    <p>The settings tab would allow users to change the language to English or Dhivehi. Once the user selects a language, user needs to confirm whether he/she needs to change the language and once clicked on the &ldquo;Confirm&rdquo; button, user would be redirected to the home screen with the language change. If the user clicks on the &ldquo;Cancel&rdquo; button, user will be user will be taken back to the language selection screen.</p>
    <h5>2.6 Log out</h5>
    <p>This option would allow user to log out from the application. Once clicked on the Log out option, user needs to confirm whether he/she needs to log out from the application. Once the user confirms that he/she needs to logout; the user would be redirected to the initial screen of the application to enter the mobile number.</p>
    <h5>2.7 FAQs</h5>
    <p>This section would list a series of FAQs and answers to helps users to use the application.</p>
    <h5>2.8 Notifications</h5>
    <p>This feature will list all the in - app notifications received. Notifications will be supported in both English &amp; Dhiveh.&nbsp;Unread notifications will be shown in red colour with unread number of messages. Notifications would cover;</p>
    <ul style="list-style-type: circle;">
        <li>Users would be notified after a successful transaction &ndash; In app + push notification both</li>
        <li>Notifications related with location-based weather updates &ndash; Push notification</li>
        <li>Users would be notified on special days &ndash; Push notifications</li>
        <li>Notify users on individual prayer times &ndash; Push notifications</li>
        <li>Notifications could be sent to users on reminding events &ndash; In app + push notification both</li>
        <li>Users would be notified on promotions &amp; offers &ndash; In app + push notification both</li>
        <li>Users would be notified on travel schedules(arrival/departure) including bus, flight &amp; ferry - Push notifications</li>
        <li>Email/SMS notifications related to transaction completion status for online payments (relevant email &amp; SMS gateways need to be provided by the client)</li>
        <li>Notify users on individual &amp; domestic flight schedules &ndash; Push notifications</li>
    </ul>
    <h5>2.9 Newsfeed</h5>
    <p>Newsfeed would display most popular local lifestyle news. Once the user clicks on an article, user would be directed to the relevant details page. Content for the newsfeed would be fed from the CMS. CMS would consist of separate sections for each content upload and each content that is uploaded from the CMS will be reviewed and approved by Dhiraagu Admin before publishing.</p>
    <p>Use of web crawlers could be suggested to fetch data to the newsfeed. This would be developed at Phase 02.</p>
    <p>Newsfeed would display location-based weather updates for users, daily tips etc. Articles would include a heading/title, date, images and a description. This module will be integrated with the calendar and users would be notified on special days.</p>
    <h5>2.10 Travel</h5>
    <p>This SBU would display time schedules of the three main transport ways in Maldives including Bus, Ferry &amp; Flight.</p>
    <p>Once clicked on the Bus sub unit, it would identify user&rsquo;s current location (registered location) and would display the nearby bus locations with routes, bus schedules like where it would stop next and the time that would take for the next bus stop; based on the responses obtained via the provided APIs.</p>
    <p>Domestic &amp; international; flight schedules and live schedule updates could be displayed under the Flight category.</p>
    <p>Ferry category would display the ferry arrival &amp; departure times based on the locations.</p>
    <p>APIs related to bus schedule is provided by the client and assume that APIs related to flight schedules, ferry &amp; locations will be shared by the client.</p>
    <p>This module would have a sub section named Travel news, to display travel tips. Content for this could be fed from the CMS.</p>
    <h5>2.11 Food</h5>
    <p>Once the user clicks on the food module, it would list the recipes posted by popular chefs/restaurants. Recipes could be displayed in the form of a video. Client is required to provide relevant YouTube links for the recipes and videos could be displayed within the app with all play back options in YouTube; start, pause, stop, fast forward and rewind. This module would have a separate section to display the health tips. A search option will be integrated in this module to search &amp; view nearby restaurants. These restaurants would be suggested based on the address provided in the user profile. User could add comments or reviews and rate the recipes and restaurants. Restaurants could add their menus, special offers and deals and could be boosted via promotional advertisements.</p>
    <h5>2.12 Prayer Mate</h5>
    <p>Users can view the prayer times based on the user&rsquo;s location through the Prayer Mate module. This could be added as an icon on the home page. Once the user clicks on the Prayer Mate module, the initial screen would display the main prayer times based on user&rsquo;s location. The top banner of this module would display the next latest prayer time and prayer type and the current location. If the user clicks on the location name in the banner, it would ask the user whether to change the location.</p>
    <p>This would have two options to auto locate user or else to set the location manually. If the user clicks on &ldquo;Locate Me&rdquo; which refers to the auto locate the user, user&rsquo;s location would be changed automatically and the prayer times would be displayed accordingly.</p>
    <p>If the user clicks on &ldquo;Select Manually&rdquo;, user would be directed to a page where user needs to type the location manually which will list out the suggested locations and thereby based on user&rsquo;s location, prayer time would be changed accordingly.</p>
    <p>Prayer mate would have five main sub categories for Prayer times, Personal tracker, Qibla Tracker &amp; Recite Quran, Good deeds in card view.</p>
    <p><strong>Prayer times</strong></p>
    <p>Once the user clicks on the prayer times category, user will be able to view all the prayer times of that particular day.</p>
    <p>Only in the month of Ramadan, the times would include an additional time tag to display &ldquo;Sahar End&rdquo; time.</p>
    <p>A clock icon would be displayed on top left of the banner and when user clicks on the clock icon, user will be able to view all the prayer times of that particular day. User can enable/disable the prayer alert feature. Also, individual notifications could be sent for users on reminding prayer times. Each user would be provided with a feature to track prayers and fasting.</p>
    <p>Personal prayer and fasting tracker could be displayed as two tabs.</p>
    <p><strong>Personal prayer</strong></p>
    <p>Once the user clicks on the personal prayer tab, a calendar would be displayed on the specific month. If the user is logged on 08<sup>th</sup> October, the calendar would display the month of October. User can swipe the calendar left &amp; right to view previous &amp; upcoming dates. User needs to tap on a date to mark his prayers. Once he taps on a specific date, the main five prayer types will be listed and an option to tick would be displayed in front of each. For example, if the user ticks in front of &ldquo;Fajr prayer&rdquo; it would be marked as prayed and the tick would be highlighted. If the user clicks on the tick icon again, the highlighted would be removed.</p>
    <p>A prayer type would be active only after its prayer time is passed. Hence the user can mark it if he actually prayed.</p>
    <p><strong>Fasting tracking</strong></p>
    <p>This feature would track the user&rsquo;s fasting in the month of Ramadhan. This feature will only be active during the month of Ramadhan and this should be disabled during other months through the backend.</p>
    <p>Once the user clicks on the Fasting tacking tab, similar to the prayer tracking module, a calendar would be displayed. Below the calendar a label would be displayed asking &ldquo;Are you fasting today?&rdquo; and two buttons with a &ldquo;tick button&rdquo; and a cancel icon with a cross button would be displayed. If the user clicks on the &ldquo;tick button&rdquo; it would be marked as user fasted on the particular day. If the user clicks on the cancel icon indicated as a &ldquo;cross button&rdquo; it would be marked as the user did not fast on the particular day. These two buttons can be displayed in two separate colours as green; if fasted (tick button) or else in red; if did not fast (cross button) to make the users more user friendly with the application.</p>
    <h5>2.13 Events</h5>
    <p>User could browse the events module by clicking on the calendar icon from the bottom navigation bar. Once the user clicks on that icon, user would be taken to the comprehensive event detail page. This page would display an event calendar, which shows all the upcoming events organized by Dhiraagu. Reminders could be set to notify users regarding the events. Events would be listed in card view with a small description for each. Users can view detailed description of events by clicking on an event. All details for events could be fed from the CMS.</p>
    <p>At phase 02, necessary account access would be given to the relevant event partners to upload their event details.</p>
    <p>This category would provide an option for the users to create their own events &amp; share it among their friends. The flow will be discussed with technical constraints at the development stage and a separate cost breakdown would be shared for this.</p>
    <h5>2.14 E &ndash; directory</h5>
    <p>This module is to display details of vendors who are on-board with the app. This will be an API integration of the existing database to the mobile application. Users would be able to search businesses by number/name. This module would be integrated with Google maps to show store locations along with contact details and other info.</p>
    <p>The app could be facilitated to search &amp; see nearby restaurants/stores based on user location; assuming that client provides us all the latitudes &amp; longitudes for the locations. Based on the user&rsquo;s registered location; nearby outlets would be visualized on a map.</p>
    <p>Note: The necessary details that need to be showcased under e-directory should be provided by the client</p>
    <h5>2.15 Offers</h5>
    <p>Offers module would show global &amp; targeted promotional offers. Promotions could be sent to particular users via micro interactions or by identifying relevant users from user tags. Offers would be listed in a card view, and detailed description could be viewed once clicked on a particular offer. A purchase &amp; redeem buttons would be displayed at the bottom of the description.</p>
    <p>The mechanism of purchasing or redeeming offers need to be specified by the client.</p>
    <p>Advertisements of offers would be shown in a slider where each banner item will last for 3 seconds and then the banner would switch to the other advert.</p>
    <h5>2.16 Support Local Videos</h5>
    <p>The app supports to play videos that are uploaded by Dhiraagu. Videos would be facilitated with all play back options like start, pause, stop, fast, forward, rewind etc. The user would see a title and a small description of the video on the app and once the user clicks on the link, we suggest to play the video via YouTube. This would be added through the CMS. Client needs to provide the YouTube link, video title and a description for relevant videos.</p>
    <p>We suggest to attach video through a YouTube link since it&rsquo;s more effective and does not harm to the app storage rather uploading a raw video through backend and displaying it in the app. Also, we suggest to keep a separate channel for these videos and will only be accessible to the admin so, that client could guarantee the visibility of the videos without being accessible to the public.</p>
    <h5>2.17 Ad Portal</h5>
    <p>The content management system would have a separate section to upload 3<sup>rd</sup> party advertisements. Any content uploaded needs to be approved by Dhiraagu before publishing in the app.</p>
    <h5>2.18 Payment Flexibility</h5>
    <p>Client would be responsible in providing all the payment gateways &amp; relevant details to Arimac team and Arimac will only be responsible for the payment integrations. Dhiraagu pay could be set as the default payment channel.</p>
    <p>Note: Necessary payment gateways and relevant details to be provided by the client</p>
    <h5>2.19 Help &amp; Support</h5>
    <p>This section is to display FAQs for users and would allow users to reach out to the support team to resolve queries.</p>
    <h5>2.20 Universal Search</h5>
    <p>This allows users to search any content in the application and could be placed at the top of the home screen. Certain 3<sup>rd</sup> party APIs will be required to integrate with the universal search. Arimac will request them separately at the development stage.</p>
    <h5>2.21 Calendar Module</h5>
    <p>This module is to add special calendar dates through the backend. Below features could be facilitated with this.</p>
    <ul style="list-style-type: circle;">
        <li>Create Calendar</li>
        <li>Update Calendar (add/alter/remove dates)</li>
        <li>Select target users / by default all</li>
        <li>Notify users</li>
    </ul>
    <p>&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
