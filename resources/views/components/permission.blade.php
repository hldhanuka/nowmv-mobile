<div class="col-sm-6 col-lg-3 d-flex">
    <div class="card" id="{{ $permission_name }}_permissions">
        <div class="card-header">
            <i class="{{ $icon }}"></i>
            {{ $title }}
        </div>
        <div class="card-body">
            <div class="form-check" {{ $show_check_all ? '' : 'hidden' }}>
                <label style="color: #f96332" class="form-check-label">
                    <input class="form-check-input" type="checkbox" id="{{ $permission_name }}_check_all">
                    Check All
                    <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                </label>
            </div>

            @foreach($types as $type)
                <div class="form-check">
                    @if($errors->any())
                        {{ $permissions = null}}
                    @endif
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" id="{{ $permission_name }}_{{ $type }}"
                               name="{{ $permission_name }}[{{ $type }}]"
                               value="1" @if($permissions) {{ ( (old("$permission_name.$type", $permissions->contains("name","$permission_name.$type"))) ? 'checked' : '') }} @else {{ (! empty(old("$permission_name.$type")) ? 'checked' : '') }} @endif>
                        {{ucwords($type)}}
                        <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                    </label>
                </div>
            @endforeach

            <br>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        //auto checkAll on load
        $('#{{ $permission_name }}_check_all').prop('checked', function () {
            var total = $('#{{ $permission_name }}_permissions input[type=checkbox]').length;
            var checked = $('#{{ $permission_name }}_permissions input[type=checkbox]:checked').length;

            if (total - 1 === checked) {
                this.checked = true;
            }
        });

        //checkAll function
        $('#{{ $permission_name }}_check_all').change(function () {
            if (this.checked) {
                $('#{{ $permission_name }}_permissions input[type=checkbox]').each(function () {
                    this.checked = true;
                });
            } else {
                $('#{{ $permission_name }}_permissions input[type=checkbox]').each(function () {
                    this.checked = false;
                });
            }
        });

        //handle other permission dependency on list permission
        $("#{{ $permission_name }}_permissions input[type=checkbox]").on('change', function () {
            if (this.id !== "{{ $permission_name }}_list" && this.id !== "{{ $permission_name }}_check_all" && this.checked) {
                $('#{{ $permission_name }}_permissions input[type=checkbox]').each(function () {
                    if (this.id === "{{ $permission_name }}_list") {
                        this.checked = true;
                    }
                });
            } else if (this.id === "{{ $permission_name }}_list" && this.checked === false) {
                $('#{{ $permission_name }}_permissions input[type=checkbox]').each(function () {
                    this.checked = false;
                });
            }
        });

        //checkAll auto uncheck
        $("#{{ $permission_name }}_permissions input[type=checkbox]").on('change', function () {
            var total = $('#{{ $permission_name }}_permissions input[type=checkbox]').length;
            var checked = $('#{{ $permission_name }}_permissions input[type=checkbox]:checked').length;

            if (total - 1 === checked) {
                if (!$('#{{ $permission_name }}_check_all').is(":checked")) {
                    $('#{{ $permission_name }}_check_all').prop('checked', true);
                } else {
                    $('#{{ $permission_name }}_check_all').prop('checked', false);
                }
            }
        });

    });
</script>
